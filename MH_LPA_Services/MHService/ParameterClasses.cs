﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace MHService_WCF
{
    public class ParameterClasses
    {
    }
    
    public class Parameters
    {
        public int p_line_product_rel_id { get; set; }
        public int p_user_id { get; set; }
        public int location_id { get; set; }
        public int p_question_id { get; set; }
        public string p_duration { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string oldPassword { get; set; }
        public string encryptedPassword { get; set; }
        public int p_region_id { get; set; }
        public int p_country_id { get; set; }
        public int p_rating     { get; set; }
        public string p_comments     { get; set; }
        public int p_line_id { get; set; }
        public string deviceID { get; set; }
        public int userID { get; set; }
        public int p_division_id { get; set; }
        public string p_audit_id { get; set; }
    }
    
    public class AuditResultParams
    {
        public int p_audit_plan_id { get; set; }
        public int p_line_product_rel_id { get; set; }
        public int p_audited_by_user_id { get; set; }
        public string p_audit_date { get; set; }
        public int p_shift_no	 { get; set; }
        public string p_json_AuditResults { get; set; }
        public int p_reviewed_by_user_id { get; set; }
        public int p_audit_id { get; set; }
        public int p_line_id { get; set; }
        public string p_part_number { get; set; }
        public string deviceID { get; set; }
        public int userID { get; set; }
        public string username { get; set; }
        public string password { get; set; }

    }

    public class ImageClass
    {
        public byte[] image_file { get; set; }
    }
    
    [Serializable] 
    public class AuditParameters
    {
        public AuditResultParams data { get; set; }
        public ImageClass imagelist { get; set; }
    }

    public class AnsResultsTableType
    {
        public string question_id { get; set; }
        public string answer { get; set; }
        public string remarks { get; set; }
        public string imageCount { get; set; }
        public byte[] image_file_name { get; set; }
    }

    public class AnsResultsTableObject
    {
        public AnsResultsTableType records { get; set; }
    }

    public class AnsReviewsTableType
    {
        public int loc_answer_id { get; set; }
        public string review_closed_on { get; set; }
        public int review_closed_status { get; set; }
        public string review_comments { get; set; }
        public string review_image_file_name { get; set; }
    }

    public class AnsReviewsTableObject
    {
        public AnsReviewsTableType records { get; set; }
    }

    public class multipleImageClass
    {
        public ICollection<byte[]> review_image_file_name { get; set; }
    }

    public class EmailDetails
    {
        public string toMailId { get; set; }
        public string ccMailId { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public string attachment { get; set; }
        public string fromMailId { get; set; }
    }

    public class ImageContent
    {
        public string filename { get; set; }
        public int question_id { get; set; }
        public int audit_id { get; set; }
        public string flag { get; set; }
        public string deviceID { get; set; }
        public int userID { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }

    public class OutputDetail
    {
        public int Error_code { get; set; }
        public string Error_msg { get; set; }
    }
}