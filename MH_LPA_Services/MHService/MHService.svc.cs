﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.Text;
using System.Web.Script.Serialization;

namespace MHService_WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MHService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select MHService.svc or MHService.svc.cs at the Solution Explorer and start debugging.
    public class MHService : IMHService
    {
        public void DoWork()
        {
        }

        public string GetQuestions(string userDetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            JSONHelper objjson = new JSONHelper();
            string output = string.Empty;
            List<AuditQuestions> objListAudit = new List<AuditQuestions>();
            try
            {
                //output = objComm.GetQuestionByLocation(1);
                //output = JsonConvert.Tostring(output);
                //output ="[{\"section_id\":1,\"section_name\":\"General Section\",\"section_display_sequence\":1,\"question_id\":1,\"question\":\"Are the aisles clear of any debris and oil spillages?","help_text":"All Aisles should be clear of protruding wires/waste oil/spare parts and tools","question_display_sequence":1},{"section_id":1,"section_name":"General Section","section_display_sequence":1,"question_id":2,"question":"Do you have all the information and knowledge to do your work today?","help_text":"Before you start the work make sure you have all the parts and information you need to do todays work","question_display_sequence":2},{"section_id":1,"section_name":"General Section","section_display_sequence":1,"question_id":10000,"question":"Can you read and write english?","question_display_sequence":3}]";
            }
            catch (Exception ex)
            {

            }

            return output;
        }

        public string GetMyQuestionList(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            Parameters objParam = new Parameters();
            JSONHelper objjson = new JSONHelper();
            string output = string.Empty;
            List<AuditQuestions> objListAudit = new List<AuditQuestions>();
            try
            {
               // objParam = objjson.JsonDeserialize<Parameters>(paramdetails);
                output = objComm.GetQuestionByLocation(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string GetMyTasks(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            List<AuditQuestions> objListAudit = new List<AuditQuestions>();
            try
            {
                //objParam = objjson.JsonDeserialize<Parameters>(paramdetails);
                //id = objParam.p_line_id;
                output = objComm.GetMyTasksById(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string GetAnswerHistory(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            List<AuditQuestions> objListAudit = new List<AuditQuestions>();
            try
            {
                //objParam = objjson.JsonDeserialize<Parameters>(paramdetails);
                //id = objParam.p_line_id;
                output = objComm.GetAnswerHistoryById(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string GetAuditScore(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            int id;
            Parameters objParam = new Parameters();
            JSONHelper objjson = new JSONHelper();
            string output = string.Empty;
            List<AuditQuestions> objListAudit = new List<AuditQuestions>();
            try
            {
                //objParam = objjson.JsonDeserialize<Parameters>(paramdetails);
                //id = objParam.p_line_product_rel_id;
                output = objComm.GetAuditScoreById(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string GetAuditPlan(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            Parameters objParam = new Parameters();
            JSONHelper objjson = new JSONHelper();
            string output = string.Empty;
            List<AuditQuestions> objListAudit = new List<AuditQuestions>();
            try
            {
                //objParam = objjson.JsonDeserialize<Parameters>(paramdetails);
                output = objComm.GetAuditPlanById(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string InsAuditResults(AuditResultParams paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.InsAuditResultsToDatabase(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string InsAuditResultsWithPart(AuditResultParams paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.InsAuditResultsWithPartToDatabase(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string GetReviewHistory(string paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            Parameters objParam = new Parameters();
            JSONHelper objjson = new JSONHelper();
            string output = string.Empty;
            List<AuditQuestions> objListAudit = new List<AuditQuestions>();
            try
            {
                objParam = objjson.JsonDeserialize<Parameters>(paramdetails);
                output = objComm.GetReviewHistoryById(objParam);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string InsAuditReview(AuditResultParams paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            JSONHelper objjson = new JSONHelper();
            string output = string.Empty;
            List<AuditQuestions> objListAudit = new List<AuditQuestions>();
            try
            {

                output = objComm.InsAuditReviewsToDatabase(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string GetQRCode(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.GetQRCodeFromDB(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public object DeserializeFromStream(MemoryStream stream)
        {
            IFormatter formatter = new BinaryFormatter();
            stream.Seek(0, SeekOrigin.Begin);
            object o = formatter.Deserialize(stream);
            return o;
        }

        public string PostImage(Stream stream)
        {
            
            csCommonFunctions objComm = new csCommonFunctions();
            ImageContent objImage=new ImageContent();
            string output = string.Empty;
            try
            {
                MultipartParser parser = new MultipartParser(stream);
                if (parser.Success)
                {
                    // Save the file
                    //SaveFile(parser.Filename, parser.ContentType, parser.FileContents);

                }
               // objComm.ParamLog("filename : " + parser.Filename);
               // objComm.ParamLog("ContentType : " + parser.ContentType);
               //string dirPath = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug\\", "");

                

                string filename = parser.Filename;
               // string filepath = ConfigurationManager.AppSettings["PublishedImageURL"].ToString();
                
               objImage.audit_id = Convert.ToInt32(filename.Substring(0, filename.IndexOf('_')));
               // objImage.filename = filepath + "/" + objImage.audit_id + "/" + parser.Filename + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
               objComm.ParamLog("audit_id : " + objImage.audit_id + " : " + filename);
                string pathString = ConfigurationManager.AppSettings["ImageURL"].ToString() + objImage.audit_id;
                System.IO.Directory.CreateDirectory(pathString);
                using (FileStream writer = new FileStream(pathString + "/" + parser.Filename + "." + System.Drawing.Imaging.ImageFormat.Jpeg, FileMode.OpenOrCreate))
                {
                    writer.Write(parser.FileContents, 0, parser.FileContents.Length);
                }
                //int len=filename.LastIndexOf('_') -filename.IndexOf('_') ;
                //objImage.user_id = Convert.ToInt32(filename.Substring(filename.IndexOf('_') + 1, len-1));
                //len=filename.Length- filename.LastIndexOf('_');
                //objImage.Line_part_rel_id = Convert.ToInt32(filename.Substring(filename.LastIndexOf('_') + 1, len - 1));
               // int len = (filename.Length - filename.IndexOf('_'));
               // objComm.ParamLog("length : " + len);

               //// string s = "100000100000100001";
                
               // int[] indexes = Enumerable.Range(0, filename.Length).Where(x => filename[x] == '_').ToArray();
               // objComm.ParamLog("indexes.Length : " + indexes.Length);
               // if (indexes.Length > 1)
               // {
               //     objComm.ParamLog("add" );
               //     objImage.question_id = Convert.ToInt32(filename.Substring(filename.IndexOf('_') + 1, ((filename.LastIndexOf('_')) - filename.IndexOf('_') - 1)));
               //     if(Convert.ToInt32(filename.Substring(filename.LastIndexOf('_') + 1,1))>0)
               //         objImage.flag = "add";
               //     else
               //         objImage.flag = "update";
               // }
               // else
               // {
               //     objComm.ParamLog("update");
               //     objImage.question_id = Convert.ToInt32(filename.Substring(filename.IndexOf('_') + 1, len-1));
               //     objImage.flag = "update";
               // }
               // objComm.ParamLog("question_id : " + objImage.question_id);
                //output=objComm.SaveImageToDatabase(objImage);
                output = "[{\"err_code\":0,\"err_message\":\"Inserted Successfully\"}]";
                return output;
            }
            catch (Exception ex)
            {
               
                objComm.ErrorLog(ex);
                return output;
            }
            
        }

        public string SaveImageForOpenTasks(Stream stream)
        {
            
            csCommonFunctions objComm = new csCommonFunctions();
            ImageContent objImage=new ImageContent();
            string output = string.Empty;
            try
            {
                MultipartParser parser = new MultipartParser(stream);
                if (parser.Success)
                {
                    // Save the file
                    //SaveFile(parser.Filename, parser.ContentType, parser.FileContents);

                }
                objComm.ParamLog("filename : " + parser.Filename);
                objComm.ParamLog("ContentType : " + parser.ContentType);
                string dirPath = AppDomain.CurrentDomain.BaseDirectory.Replace("\\bin\\Debug\\", "");
                string filename = parser.Filename;
                objImage.audit_id = Convert.ToInt32(filename.Substring(0, filename.IndexOf('_')));
                using (FileStream writer = new FileStream(dirPath + "images/"+objImage.audit_id+"/Task_" + parser.Filename + "." + System.Drawing.Imaging.ImageFormat.Jpeg, FileMode.OpenOrCreate))
                {
                    writer.Write(parser.FileContents, 0, parser.FileContents.Length);
                }

                
                string filepath = ConfigurationManager.AppSettings["PublishedImageURL"].ToString();
                objComm.ParamLog(filename);
                //objImage.audit_id = Convert.ToInt32(filename.Substring(0, parser.Filename.IndexOf('_')));
                objImage.filename = filepath + "/" + objImage.audit_id + "/Task_" + parser.Filename + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                objComm.ParamLog("audit_id : " + objImage.audit_id);
                string pathString = ConfigurationManager.AppSettings["ImageURL"].ToString() + objImage.audit_id;
                System.IO.Directory.CreateDirectory(pathString);
                int len = (filename.Length - filename.IndexOf('_'));
                objComm.ParamLog("length : " + len);
                objImage.question_id = Convert.ToInt32(filename.Substring(filename.IndexOf('_') + 1, len-1));
                objComm.ParamLog("question_id : " + objImage.question_id);
                output=objComm.SaveImageForTaskToDatabase(objImage);

                return output;
            }
            catch (Exception ex)
            {
               
                objComm.ErrorLog(ex);
                return output;
            }
            
        }

        public static byte[] ImageToByte(Image img)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            ImageConverter converter = new ImageConverter();
            try
            {
                objComm.ParamLog("inside");
                return (byte[])converter.ConvertTo(img, typeof(byte[]));
            }
            catch (Exception ex )
            {
                objComm.ErrorLog(ex);
                return (byte[])converter.ConvertTo(img, typeof(byte[]));
            }
           
        }

        public string GetUserDetails(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.GetUserDetailseFromDB(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string GetAllMyLines(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.GetAllMyLinesFromDB(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string GetRegionList(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.GetRegionListFromDB(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string GetCountryList(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.GetCountryListFromDB(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string GetLocationList(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.GetLocationListFromDB(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string getLineProductForLocation(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.getLineProductForLocationFromDB(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string GetAuditScoreSummary(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.GetAuditScoreSummaryFromDB(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string InsUserFeedback(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.InsUserFeedbackFromDB(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string InsProfileChangeReq(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.InsProfileChangeReqFromDB(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string UpdUserPassword(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.UpdUserPasswordFromDB(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string GetLatestAuditScoreSummary(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.GetLatestAuditScoreSummaryFromDB(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string GetLatestAuditScoreSummaryTotal(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.GetLatestAuditScoreSummaryTotalFromDB(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string GetAuditScoreSummaryTotal(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.GetAuditScoreSummaryTotalFromDB(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public void UploadFile(RemoteFileInfo request)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            FileStream targetStream = null;
            try
            {
                Stream sourceStream = request.FileByteStream;

                string uploadFolder = @"C:\Anamika\";

                string filePath = Path.Combine(uploadFolder, request.review_image_file_name);

                using (targetStream = new FileStream(filePath, FileMode.Create,
                                      FileAccess.Write, FileShare.None))
                {
                    //read from the input stream in 65000 byte chunks

                    const int bufferLen = 65000;
                    byte[] buffer = new byte[bufferLen];
                    int count = 0;
                    while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
                    {
                        // save to output stream
                        targetStream.Write(buffer, 0, count);
                    }
                    targetStream.Close();
                    sourceStream.Close();
                }
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

        }

        public MemoryStream CopyToMemory(Stream input)
        {
            // It won't matter if we throw an exception during this method;
            // we don't *really* need to dispose of the MemoryStream, and the
            // caller should dispose of the input stream
            MemoryStream ret = new MemoryStream();
            csCommonFunctions objComm = new csCommonFunctions();
            try
            {
                

                byte[] buffer = new byte[8192];
                int bytesRead;
                while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ret.Write(buffer, 0, bytesRead);
                }
                // Rewind ready for reading (typical scenario)
                ret.Position = 0;
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }
            
            return ret;
        }

        public string SendMail(ImageContent paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.SendMail(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string GetMyQuestionListForLine(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.GetMyQuestionListForLineFromDB(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }

        public string GetAuditInfoForLine(Parameters paramdetails)
        {
            csCommonFunctions objComm = new csCommonFunctions();
            string output = string.Empty;
            try
            {
                output = objComm.GetAuditInfoForLineFromDB(paramdetails);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }

            return output;
        }
    }
}
