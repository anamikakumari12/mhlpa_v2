﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Web.UI.DataVisualization.Charting;
using iTextSharp.text.html;
using System.Net;
using iTextSharp.tool.xml;

namespace MH_Logistics_Services
{
    public class csCommonFunctions
    {
        DataTable dtHeadingSession = new DataTable();
        int fontsize;
        string Language = string.Empty;
        OutputDetail objOutputDetail;
        #region Common Functions
        public OutputDetail checkLogin(Parameters param)
        {
            OutputDetail objOutput = new OutputDetail();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand("sp_checkAuthentication", sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@username", SqlDbType.Int).Value = param.username;
                sqlcmd.Parameters.Add("@password", SqlDbType.VarChar, -1).Value = param.encryptedPassword;
                sqlcmd.Parameters.Add("@userID", SqlDbType.VarChar, 1000).Value = param.userID;
                sqlcmd.Parameters.Add("@deviceID", SqlDbType.VarChar, 1000).Value = param.deviceID;
                sqlcmd.Parameters.Add("@l_err_code", SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add("@l_err_message", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.Error_code = Convert.ToInt32(sqlcmd.Parameters["@l_err_code"].Value);
                objOutput.Error_msg = Convert.ToString(sqlcmd.Parameters["@l_err_message"].Value);
                objOutput.Error_msg = "[{\"err_code\":" + objOutput.Error_code + ",\"err_message\":\"" + objOutput.Error_msg + "\"}]";

            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        /// <summary>
        /// Author : Anamika
        /// Date : June 6, 2017
        /// Desc : Saving exception in a file
        /// </summary>
        /// <param name="ex">Exception occured in different methods</param>
        public void ErrorLog(Exception ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;


            StreamWriter log;
            DateTime dateTime = DateTime.UtcNow.Date;
            string ErrorLogFile = Convert.ToString(ConfigurationManager.AppSettings["ErrorLogFile"]) + "_" + dateTime.ToString("dd_MM_yyyy") + ".txt";
            string ErrorFile = Convert.ToString(ConfigurationManager.AppSettings["ErrorFile"]) + "_" + dateTime.ToString("dd_MM_yyyy") + ".txt";

            if (!File.Exists(ErrorFile))
            {
                log = new StreamWriter(ErrorLogFile, true);
            }
            else
            {
                log = File.AppendText(ErrorLogFile);
            }

            // Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(message);
            // Close the stream:
            log.Close();
        }

        private void SendMail(EmailDetails objEmail)
        {
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            MailMessage email;
            SmtpClient smtpc;
            try
            {
                email = new MailMessage();
                if (objEmail.toMailId.Contains(";"))
                {
                    List<string> names = objEmail.toMailId.Split(';').ToList<string>();
                    for (int i = 0; i < names.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(names[i].TrimStart().TrimEnd()))
                            email.To.Add(names[i]);
                    }
                }
                else
                {
                    email.To.Add(objEmail.toMailId);
                }
                if (!string.IsNullOrEmpty(objEmail.ccMailId))
                    email.CC.Add(objEmail.ccMailId);
                email.Subject = objEmail.subject;
                email.Body = objEmail.body;
                email.IsBodyHtml = true;
                //email.From.Address = objEmail.fromMailId;
                if (!string.IsNullOrEmpty(objEmail.attachment))
                    email.Attachments.Add(new Attachment(objEmail.attachment));
                smtpc = new SmtpClient();
                smtpc.Send(email);
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
        }

        internal void ParamLog(string p)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message parameter : {0}", p);
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;


            StreamWriter log;
            DateTime dateTime = DateTime.UtcNow.Date;
            string ErrorLogFile = Convert.ToString(ConfigurationManager.AppSettings["ErrorLogFile"]) + "_" + dateTime.ToString("dd_MM_yyyy") + ".txt";
            string ErrorFile = Convert.ToString(ConfigurationManager.AppSettings["ErrorFile"]) + "_" + dateTime.ToString("dd_MM_yyyy") + ".txt";
            if (!File.Exists(ErrorFile))
            {
                log = new StreamWriter(ErrorLogFile, true);
            }
            else
            {
                log = File.AppendText(ErrorLogFile);
            }

            // Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(message);
            // Close the stream:
            log.Close();
        }

        public string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public static DataTable CreateDataTable<T>(IEnumerable<T> list)
        {
            Type type = typeof(T);
            var properties = type.GetProperties();

            DataTable dataTable = new DataTable();
            foreach (PropertyInfo info in properties)
            {
                dataTable.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }

            foreach (T entity in list)
            {
                object[] values = new object[properties.Length];
                for (int i = 0; i < properties.Length; i++)
                {
                    values[i] = properties[i].GetValue(entity);
                }

                dataTable.Rows.Add(values);
            }

            return dataTable;
        }

        private void convertPDF(string example_html, string filepath, string filename, DataSet dsImage)
        {
            try
            {
                using (FileStream fs = new FileStream(Path.Combine(filepath, "test.htm"), FileMode.Create))
                {
                    using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                    {
                        w.WriteLine(example_html);
                    }
                }

                GeneratePdfFromHtml(filepath, filename, dsImage);

            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
        }


        public void GeneratePdfFromHtml(string filepath, string filename, DataSet dsImage)
        {
            string outputFilename = Path.Combine(filepath, filename);
            string inputFilename = Path.Combine(filepath, "test.htm");

            using (var input = new FileStream(inputFilename, FileMode.Open))
            using (var output = new FileStream(outputFilename, FileMode.Create))
            {
                CreatePdf(filepath, filename, input, output, dsImage);
            }
        }

        public void CreatePdf(string filepath, string filename, Stream htmlInput, Stream pdfOutput, DataSet dsImage)
        {
            string[] images;
            string imageURL;
            Paragraph paragraph;
            try
            {
                using (var document = new Document(PageSize.A4, 30, 30, 30, 30))
                {
                    var writer = PdfWriter.GetInstance(document, pdfOutput);
                    var worker = XMLWorkerHelper.GetInstance();
                    TextReader tr = new StreamReader(htmlInput);
                    document.Open();
                    //document.NewPage();
                    worker.ParseXHtml(writer, document, htmlInput, null, Encoding.UTF8, new UnicodeFontFactory());
                    if (dsImage != null)
                    {
                        for (int j = 1; j < dsImage.Tables.Count; j++)
                        {
                            if (dsImage.Tables[j].Rows.Count > 0)
                            {
                                for (int i = 0; i < dsImage.Tables[j].Rows.Count; i++)
                                {
                                    if (!string.IsNullOrEmpty(Convert.ToString(dsImage.Tables[j].Rows[i]["image_file_name"])))
                                    {
                                        imageURL = string.Empty;
                                        paragraph = new Paragraph(Convert.ToString("Question : " + Convert.ToString(dsImage.Tables[j].Rows[i]["display_sequence"])));
                                        images = Convert.ToString(dsImage.Tables[j].Rows[i]["image_file_name"]).Split(';');
                                        document.Add(paragraph);
                                        for (int k = 0; k < images.Length; k++)
                                        {
                                            //imageURL = Convert.ToString(dsImage.Tables[j].Rows[i]["image_file_name"]);
                                            iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(images[k]);
                                            jpg.ScaleToFit(140f, 120f);
                                            jpg.SpacingBefore = 10f;
                                            jpg.SpacingAfter = 1f;
                                            jpg.Alignment = Element.ALIGN_LEFT;

                                            document.Add(jpg);
                                        }
                                    }

                                }
                            }
                        }
                    }
                    document.Close();
                }

            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
        }

        public class UnicodeFontFactory : FontFactoryImp
        {
            private static readonly string FontPath = ConfigurationManager.AppSettings["fontpath"].ToString();

            private readonly BaseFont _baseFont;

            public UnicodeFontFactory()
            {
                _baseFont = BaseFont.CreateFont(FontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            }

            public override Font GetFont(string fontname, string encoding, bool embedded, float size, int style, BaseColor color,
              bool cached)
            {
                return new Font(_baseFont, size, style, color);
            }
        }

        private string BindImages(DataSet dsImage)
        {
            string output = string.Empty;
            StringBuilder strHTMLBuilder = new StringBuilder();
            string imageURL = string.Empty;

            DataTable dtChart = new DataTable();
            DataSet ds = new DataSet();
            DataTable ChartData = new DataTable();
            try
            {
                strHTMLBuilder.Append("<table cellspacing='0'>");
                if (dsImage != null)
                {
                    for (int j = 1; j < dsImage.Tables.Count; j++)
                    {
                        if (dsImage.Tables[j].Rows.Count > 0)
                        {
                            for (int i = 0; i < dsImage.Tables[j].Rows.Count; i++)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(dsImage.Tables[j].Rows[i]["image_file_name"])))
                                {

                                    imageURL = Convert.ToString(dsImage.Tables[j].Rows[i]["image_file_name"]);
                                    if (!string.IsNullOrEmpty(imageURL))
                                    {
                                        strHTMLBuilder.Append("<tr >");
                                        strHTMLBuilder.Append("<td width='200px'>");
                                        strHTMLBuilder.Append(Convert.ToString("Question : " + Convert.ToString(dsImage.Tables[j].Rows[i]["display_sequence"])));
                                        strHTMLBuilder.Append("</td>");
                                        strHTMLBuilder.Append("<td width='800px'><img style='float:left; width:300px;' src='");
                                        strHTMLBuilder.Append(imageURL);
                                        strHTMLBuilder.Append("'/>");
                                        strHTMLBuilder.Append("</td>");
                                        strHTMLBuilder.Append("</tr>");
                                    }
                                }

                            }
                        }
                    }
                }


                strHTMLBuilder.Append("</table>");


                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return output;
        }

        protected string ExportDatatableToHtml(DataTable dt)
        {
            string Heading = Convert.ToString(dt.Rows[0]["section_name"]);
            StringBuilder strHTMLBuilder = new StringBuilder();
            string Htmltext = string.Empty;
            try
            {
                if (Language.ToLower() == "chinese" || Language.ToLower() == "thai")
                    fontsize = 7;
                else
                    fontsize = 9;
                //strHTMLBuilder.Append("<table cellspacing='0' cellpadding='0' style='border:1px black solid; border-collapse: collapse; border-spacing:0;'>");
                strHTMLBuilder.Append("<tr height='15px' style='font-family:Gotham-Book; font-size:" + (fontsize + 3) + "px; text-align:center; background-color:#048347; color:white; border-collapse: collapse;'>");
                //strHTMLBuilder.Append("<td></td>");
                strHTMLBuilder.Append("<td colspan='4'>");
                strHTMLBuilder.Append(Heading);
                strHTMLBuilder.Append("</td>");
                //strHTMLBuilder.Append("<td></td>");
                //strHTMLBuilder.Append("<td></td>");
                strHTMLBuilder.Append("<td></td>");
                strHTMLBuilder.Append("<td></td>");
                strHTMLBuilder.Append("<td></td>");
                strHTMLBuilder.Append("<td></td>");
                strHTMLBuilder.Append("</tr>");

                foreach (DataRow myRow in dt.Rows)
                {

                    strHTMLBuilder.Append("<tr style='background-color:white; height:30px'>");
                    foreach (DataColumn myColumn in dt.Columns)
                    {
                        if (myColumn.ColumnName.ToString() == "display_sequence")
                        {
                            strHTMLBuilder.Append("<td width='50px' style='border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:" + fontsize + "px;'>");
                            strHTMLBuilder.Append(myRow[myColumn.ColumnName].ToString());
                            strHTMLBuilder.Append("</td>");
                        }

                        else if (myColumn.ColumnName.ToString() == "question")
                        {
                            strHTMLBuilder.Append("<td width='570px'  style='border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:" + fontsize + "px;'>");
                            strHTMLBuilder.Append(myRow[myColumn.ColumnName].ToString());
                            strHTMLBuilder.Append("</td>");
                        }
                        else if (myColumn.ColumnName.ToString() == "answer")
                        {
                            if (myRow[myColumn.ColumnName].ToString() == "1")
                            {
                                strHTMLBuilder.Append("<td width='25px' style='background-color:red;  border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:" + fontsize + "px;'>");
                                strHTMLBuilder.Append("&#10008;");
                                strHTMLBuilder.Append("</td>");
                                strHTMLBuilder.Append("<td width='25px' style='border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:" + fontsize + "px;'>");
                                //strHTMLBuilder.Append("Yes");
                                strHTMLBuilder.Append("</td>");
                                if (myRow["na_flag"].ToString() == "Y")
                                {
                                    strHTMLBuilder.Append("<td width='25px' style='border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:" + fontsize + "px;'>");
                                    // strHTMLBuilder.Append("Not Applicable");
                                    strHTMLBuilder.Append("</td>");
                                }
                                else
                                {
                                    strHTMLBuilder.Append("<td width='25px'>");
                                    // strHTMLBuilder.Append("Not Applicable");
                                    strHTMLBuilder.Append("</td>");
                                }
                            }
                            else if (myRow[myColumn.ColumnName].ToString() == "0")
                            {
                                strHTMLBuilder.Append("<td width='25px' style=' border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:" + fontsize + "px;'>");
                                //strHTMLBuilder.Append("&#10004;");
                                strHTMLBuilder.Append("</td>");
                                strHTMLBuilder.Append("<td width='25px' style=' background-color:#048347; border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:" + fontsize + "px;'>");
                                strHTMLBuilder.Append("&#10004;");
                                strHTMLBuilder.Append("</td>");
                                if (myRow["na_flag"].ToString() == "Y")
                                {
                                    strHTMLBuilder.Append("<td width='25px' style='border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:" + fontsize + "px;'>");
                                    // strHTMLBuilder.Append("Not Applicable");
                                    strHTMLBuilder.Append("</td>");
                                }
                                else
                                {
                                    strHTMLBuilder.Append("<td width='25px'>");
                                    // strHTMLBuilder.Append("Not Applicable");
                                    strHTMLBuilder.Append("</td>");
                                }
                            }
                            else
                            {
                                strHTMLBuilder.Append("<td width='25px' style=' border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:" + fontsize + "px;'>");
                                //strHTMLBuilder.Append("&#10004;");
                                strHTMLBuilder.Append("</td>");
                                strHTMLBuilder.Append("<td width='25px' style=' border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:" + fontsize + "px;'>");
                                //strHTMLBuilder.Append("Yes");
                                strHTMLBuilder.Append("</td>");
                                if (myRow["na_flag"].ToString() == "Y")
                                {
                                    strHTMLBuilder.Append("<td width='25px' style='background-color:gray; border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:" + fontsize + "px;'>");
                                    strHTMLBuilder.Append("&#10004;");
                                    strHTMLBuilder.Append("</td>");
                                }
                                else
                                {
                                    strHTMLBuilder.Append("<td width='25px'>");
                                    // strHTMLBuilder.Append("Not Applicable");
                                    strHTMLBuilder.Append("</td>");
                                }

                            }

                        }
                        else if (myColumn.ColumnName.ToString() == "remarks")
                        {
                            strHTMLBuilder.Append("<td width='150px'  style='font-family:Gotham-Book; font-size:" + fontsize + "px;'>");
                            strHTMLBuilder.Append(myRow[myColumn.ColumnName]);
                            strHTMLBuilder.Append("</td>");
                        }
                    }
                    strHTMLBuilder.Append("<td width='70px' style=' border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:" + fontsize + "px;'>");
                    strHTMLBuilder.Append("");
                    strHTMLBuilder.Append("</td>");
                    strHTMLBuilder.Append("<td width='70px' style=' border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:" + fontsize + "px;'>");
                    strHTMLBuilder.Append("");
                    strHTMLBuilder.Append("</td>");
                    strHTMLBuilder.Append("</tr>");
                }

                //Close tags.  
                //strHTMLBuilder.Append("</table>");

                //strHTMLBuilder.Append("</html>");

                Htmltext = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return Htmltext;
        }

        private string firstpagedetails(DataTable dt)
        {
            string output = string.Empty;
            try
            {
                StringBuilder strHTMLBuilder = new StringBuilder();
                strHTMLBuilder.Append("<table border='1px' cellspacing='0'  style='font-family:Gotham-Book; font-size:medium; border-collapse: inherit;  border-spacing:0;'>");
                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td width='200px' style='font-weight:bold;'>");
                strHTMLBuilder.Append("Name of the auditor");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='225px'>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["conducted_by"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append("");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='200px' style='font-weight:bold;'>");
                strHTMLBuilder.Append("Date");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='225px'>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["conducted_on"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append("");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("<tr><td></td></tr>");
                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td style='font-weight:bold;'>");
                strHTMLBuilder.Append("Line Name");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["line_name"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append("");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='font-weight:bold;'>");
                strHTMLBuilder.Append("Part Number");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["product_code"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append("");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("<tr><td></td></tr>");
                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td style='font-weight:bold;'>");
                strHTMLBuilder.Append("Location");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["location_name"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append("");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='font-weight:bold;'>");
                strHTMLBuilder.Append("Score");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["Score"]) + "%");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append("");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");


                strHTMLBuilder.Append("</table>");

                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return output;
        }

        private string coverpage()
        {
            string output = string.Empty;
            try
            {
                StringBuilder strHTMLBuilder = new StringBuilder();
                strHTMLBuilder.Append("<table style='font-family:Gotham-Book; margin-top: 30%; margin-bottom: 300px; width:1000px;'>");
                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td style='text-align:center; font-size:45px; font-weight:bold'>");
                strHTMLBuilder.Append("LPA");
                strHTMLBuilder.Append("</td></tr ><tr><td style='text-align:center; font-size:25px; font-weight:bold'>");
                strHTMLBuilder.Append("conducted for");
                strHTMLBuilder.Append("</td></tr ><tr><td style='text-align:center; font-size:45px; font-weight:bold'>");
                strHTMLBuilder.Append("MHIN");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</table>");
                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return output;
        }

        private string heading()
        {
            string output = string.Empty;
            StringBuilder strHTMLBuilder = new StringBuilder();
            string imageURL = string.Empty;
            try
            {
                imageURL = Convert.ToString(ConfigurationManager.AppSettings["MHLogo"]);
                strHTMLBuilder.Append("<table>");
                strHTMLBuilder.Append("<tr ><td width='800px'></td>");
                strHTMLBuilder.Append("<td width='200px'><img style='float:left; width:110px;' src='");
                strHTMLBuilder.Append(imageURL);
                strHTMLBuilder.Append("'/>");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</table>");
                strHTMLBuilder.Append("<table border='1px' cellspacing='0' style='font-family:Gotham-Book; font-size:22px; font-weight:bold'>");

                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td width='1000px' style='text-align:center; background-color:#048347; color:white;'> LAYERED PROCESS AUDIT REPORT</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</table>");
                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return output;
        }

        private string BindchartData(DataTable dataTable, int audit_id)
        {
            string output = string.Empty;
            StringBuilder strHTMLBuilder = new StringBuilder();
            string imageURL = string.Empty;

            DataTable dtChart = new DataTable();
            DataSet ds = new DataSet();
            DataTable ChartData = new DataTable();
            try
            {
                ds = GetAuditScore(dataTable, audit_id);
                dtChart = ds.Tables[0];
                imageURL = Convert.ToString(ConfigurationManager.AppSettings["ChartImageFile"]);
                //dtChart = (DataTable)Session["ChartData"];

                strHTMLBuilder.Append("<table cellspacing='0'>");

                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td width='350px'></td>");
                strHTMLBuilder.Append("<td width='800px'><img style='float:right; width:300px;' src='");
                strHTMLBuilder.Append(imageURL);
                strHTMLBuilder.Append("'/>");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</table>");
                //strHTMLBuilder.Append("<tr >");
                // strHTMLBuilder.Append("<td width='1000px' style='font-family:Gotham-Book; font-size:10px; text-align:left;'>");
                strHTMLBuilder.Append("<table cellspacing='0' style='font-family:Gotham-Book; font-size:10px;  border: 1px solid darkgray;'>");
                int i = 0;
                foreach (DataRow myRow in dtChart.Rows)
                {
                    if (i % 2 == 0)
                        strHTMLBuilder.Append("<tr >");

                    strHTMLBuilder.Append("<td width='100px' style=' border: 1px solid gray; border-collapse: collapse;'> ");
                    strHTMLBuilder.Append(Convert.ToString(myRow["section_abbr"]));
                    strHTMLBuilder.Append("</td>");
                    strHTMLBuilder.Append("<td width='400px' style=' border: 1px solid gray; border-collapse: collapse;'>");
                    strHTMLBuilder.Append(Convert.ToString(myRow["section_name"]));
                    strHTMLBuilder.Append("</td>");
                    if (i % 2 != 0)
                        strHTMLBuilder.Append("</tr>");
                    i++;
                }
                strHTMLBuilder.Append("</table>");
                //strHTMLBuilder.Append("</td>");
                //strHTMLBuilder.Append("</tr>");
                //strHTMLBuilder.Append("</table>");

                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return output;
        }

        private void Bindchart(DataTable dataTable, int audit_id)
        {
            DataTable dtChart = new DataTable();
            DataSet ds = new DataSet();
            DataTable ChartData = new DataTable();
            try
            {
                ds = GetAuditScore(dataTable, audit_id);
                dtChart = ds.Tables[0];
                ChartData = dtChart;
                if (ChartData.Rows.Count > 0)
                {
                    string[] XPointMember1 = new string[ChartData.Rows.Count];
                    int[] YPointMember1 = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember1[count] = ChartData.Rows[count]["section_abbr"].ToString();
                        //storing values for Y Axis  
                        YPointMember1[count] = Convert.ToInt32(ChartData.Rows[count]["avg_score"]);
                        //ChartPerSection.Series[0].Name = column.ColumnName;

                    }
                    Chart Chart2 = new Chart();
                    Series series1 = new Series();
                    Chart2.Series.Add(series1);
                    Chart2.Series[0].Points.DataBindXY(XPointMember1, YPointMember1);
                    Chart2.Series[0].ChartType = SeriesChartType.Radar;
                    Chart2.Series[0]["RadarDrawingStyle"] = "Area";
                    Chart2.Series[0]["AreaDrawingStyle"] = "Polygon";
                    Chart2.Series[0].Color = System.Drawing.Color.DarkGreen;
                    Title title = new System.Web.UI.DataVisualization.Charting.Title();
                    Chart2.Titles.Add(title);
                    Chart2.Titles[0].Text = "Audit Score";
                    Chart2.Series[0].IsValueShownAsLabel = true;
                    ChartArea ch1 = new ChartArea();
                    Chart2.ChartAreas.Add(ch1);
                    Chart2.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    Chart2.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                    Chart2.ChartAreas[0].AxisX.Interval = 1;
                    Chart2.ChartAreas[0].AxisY.Interval = 25;
                    Chart2.Series[0].LabelFormat = "{0} %";
                    Chart2.Series[0].LabelForeColor = System.Drawing.Color.DarkRed;
                    Chart2.Series[0].LabelBorderWidth = 10;
                    Chart2.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                    Chart2.ChartAreas[0].AxisY.Maximum = 100;
                    Chart2.SaveImage(Convert.ToString(ConfigurationManager.AppSettings["ChartImageFile"]), ChartImageFormat.Png);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

        }

        internal DataSet GetAuditScore(DataTable dt, int audit_id)
        {
            DataSet dsOutput = new DataSet();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                if (dt.Rows.Count > 0)
                {
                    sqlcmd = new SqlCommand("GetAuditScoreSummaryWeb", sqlconn);
                    sqlconn.Open();
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.Add("@p_region_id", SqlDbType.Int).Value = Convert.ToInt32(dt.Rows[0]["region_id"]);
                    sqlcmd.Parameters.Add("@p_country_id", SqlDbType.Int).Value = Convert.ToInt32(dt.Rows[0]["country_id"]);
                    sqlcmd.Parameters.Add("@p_location_id", SqlDbType.Int).Value = Convert.ToInt32(dt.Rows[0]["location_id"]);
                    sqlcmd.Parameters.Add("@p_line_id", SqlDbType.Int).Value = Convert.ToInt32(dt.Rows[0]["line_id"]);
                    sqlcmd.Parameters.Add("@p_audit_id", SqlDbType.Int).Value = audit_id;
                    sqlda = new SqlDataAdapter(sqlcmd);
                    sqlda.Fill(dsOutput);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dsOutput;
        }

        private string newheading(DataTable dt)
        {
            string output = string.Empty;
            StringBuilder strHTMLBuilder = new StringBuilder();
            string imageURL = string.Empty;
            DataTable dtHeading = new DataTable();
            try
            {
                dtHeading = GetHeading(Convert.ToInt32(dt.Rows[0]["Document_no"]));
                dtHeadingSession = dtHeading;
                Language = Convert.ToString(dtHeading.Rows[0]["Language"]);
                imageURL = Convert.ToString(ConfigurationManager.AppSettings["MHLogo"]);
                strHTMLBuilder.Append("<table cellspacing='0' style='border: 1px solid darkgray; font-family:Gotham-Book; border-collapse:collapse;'>");

                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td colspan='5' width='850px' style='text-align:center; font-size:14px; font-weight:bold; color:black;'>");
                //strHTMLBuilder.Append("Layered Process Audit (LPA)");
                strHTMLBuilder.Append(Convert.ToString(dtHeading.Rows[0]["LPA"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='150px'><img style='float:left; width:80px;' src='");
                strHTMLBuilder.Append(imageURL);
                strHTMLBuilder.Append("'/>");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                //strHTMLBuilder.Append("</table>");
                //strHTMLBuilder.Append("<table style='border: 1px solid gray; border-collapse:collapse; border-spacing:0;'>");
                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td width='150px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append(Convert.ToString(dtHeading.Rows[0]["Plant"]));
                //strHTMLBuilder.Append("0.1)Plant");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='200px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["location_name"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='100px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                //strHTMLBuilder.Append("0.3)Name:");
                strHTMLBuilder.Append(Convert.ToString(dtHeading.Rows[0]["Name"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='250px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["conducted_by"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='150px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                // strHTMLBuilder.Append("0.5)Personal No.:");
                strHTMLBuilder.Append(Convert.ToString(dtHeading.Rows[0]["Person"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='150px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["username"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");

                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                //strHTMLBuilder.Append("0.2) Current product<br/>/part-no.");
                strHTMLBuilder.Append(Convert.ToString(dtHeading.Rows[0]["Part"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["line_name"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                // strHTMLBuilder.Append("0.4)Date:");
                strHTMLBuilder.Append(Convert.ToString(dtHeading.Rows[0]["Date"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["conducted_on"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                //  strHTMLBuilder.Append("0.6)Shift:");
                strHTMLBuilder.Append(Convert.ToString(dtHeading.Rows[0]["Shift"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["no_of_shifts"]));
                strHTMLBuilder.Append("</td>");

                strHTMLBuilder.Append("</tr>");

                strHTMLBuilder.Append("</table>");

                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return output;
        }

        private DataTable GetHeading(int audit_id)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand("get_text_for_audit_report", sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@p_audit_id", SqlDbType.Int).Value = audit_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
                if (dtOutput.Rows.Count > 0)
                {
                    dtOutput = GenerateTransposedTable(dtOutput);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        private string questionheading()
        {
            StringBuilder strHTMLBuilder = new StringBuilder();
            string Htmltext = string.Empty;
            try
            {
                if (Language.ToLower() == "chinese" || Language.ToLower() == "thai")
                    fontsize = 8;
                else
                    fontsize = 10;
                strHTMLBuilder.Append("<table style='border-collapse: collapse; border: 1px solid darkgray;'>");
                strHTMLBuilder.Append("<tr height='20px' style='background-color:gray;'>");
                strHTMLBuilder.Append("<td width='50px' style='font-family:Gotham-Book; font-size:" + fontsize + "px; border: 1px solid gray; border-collapse: collapse;'>");
                //strHTMLBuilder.Append("Item #");
                strHTMLBuilder.Append(Convert.ToString(dtHeadingSession.Rows[0]["Item"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='560px' style='border: 1px solid darkgray; border-collapse: collapse; font-family:Gotham-Book; font-size:" + fontsize + "px; text-align:center;'>");
                //strHTMLBuilder.Append("Checklist");
                strHTMLBuilder.Append(Convert.ToString(dtHeadingSession.Rows[0]["Checklist"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='90px' colspan='3' style='border: 1px solid darkgray; border-collapse: collapse; font-family:Gotham-Book; font-size:" + fontsize + "px; text-align:center;'>");
                strHTMLBuilder.Append(Convert.ToString(dtHeadingSession.Rows[0]["Evaluation"]));
                strHTMLBuilder.Append("<br/><table  style='border-collapse: collapse; border: 1px solid black;'><tr><td style='border: 1px solid black; border-collapse: collapse; font-family:Gotham-Book; font-size:" + (fontsize - 1) + "px; text-align:center;'>");
                //No
                strHTMLBuilder.Append(Convert.ToString(dtHeadingSession.Rows[0]["No"]));
                strHTMLBuilder.Append("</td><td style='border: 1px solid black; border-collapse: collapse; font-family:Gotham-Book; font-size:" + (fontsize - 1) + "px; text-align:center;'>");
                //Yes
                strHTMLBuilder.Append(Convert.ToString(dtHeadingSession.Rows[0]["Yes"]));
                strHTMLBuilder.Append("</td><td style='border: 1px solid black; border-collapse: collapse; font-family:Gotham-Book; font-size:" + (fontsize - 1) + "px; text-align:center;'>");
                //NA
                strHTMLBuilder.Append(Convert.ToString(dtHeadingSession.Rows[0]["NA"]));
                strHTMLBuilder.Append("</td></tr></table>");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='150px' style='border: 1px solid darkgray; border-collapse: collapse; font-family:Gotham-Book; font-size:" + fontsize + "px; text-align:center;'>");
                //strHTMLBuilder.Append("Findings");
                strHTMLBuilder.Append(Convert.ToString(dtHeadingSession.Rows[0]["Findings"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='70px' style='border: 1px solid darkgray; border-collapse: collapse; font-family:Gotham-Book; font-size:" + fontsize + "px; text-align:center; '>");
                // strHTMLBuilder.Append("Resp.");
                strHTMLBuilder.Append(Convert.ToString(dtHeadingSession.Rows[0]["Resp"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='70px' style='border: 1px solid darkgray; border-collapse: collapse; font-family:Gotham-Book; font-size:" + fontsize + "px; text-align:center;'>");
                //strHTMLBuilder.Append("Actual Date Closed");
                strHTMLBuilder.Append(Convert.ToString(dtHeadingSession.Rows[0]["Closed_Date"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                //strHTMLBuilder.Append("</table>");
                Htmltext = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return Htmltext;
        }

        private string getLastRow(DataTable dt)
        {
            StringBuilder strHTMLBuilder = new StringBuilder();
            string strHTML = string.Empty;
            try
            {
                //strHTMLBuilder.Append("<table border='1px' cellspacing='0'>");

                //strHTMLBuilder.Append("<tr >");
                //strHTMLBuilder.Append("<td colspan='2' style='font-family:Gotham-Book; font-size:12px; text-align:center;'>");
                //strHTMLBuilder.Append("</td>");
                //strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("<tr style='border: 1px solid gray; border-collapse: collapse;'>");
                strHTMLBuilder.Append("<td colspan='2' width='500px' style='border: 1px solid gray; border-collapse: collapse; font-family:Gotham-Book; font-size:12px; text-align:right;'>");
                // strHTMLBuilder.Append("Result:");
                strHTMLBuilder.Append(Convert.ToString(dtHeadingSession.Rows[0]["Result"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td colspan='6' width='500px' style='border: 1px solid gray; border-collapse: collapse; font-family:Gotham-Book; font-size:12px; text-align:left;'>");
                if (dt != null)
                    if (dt.Rows.Count > 0)
                        strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["Score"]) + "%");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                // strHTMLBuilder.Append("</table>");
                strHTML = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return strHTML;
        }

        private DataTable GenerateTransposedTable(DataTable inputTable)
        {
            DataTable outputTable = new DataTable();

            // Add columns by looping rows

            // Header row's first column is same as in inputTable
            outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());

            // Header row's second column onwards, 'inputTable's first column taken
            foreach (DataRow inRow in inputTable.Rows)
            {
                string newColName = (inRow[0].ToString()).Replace(",", "");
                outputTable.Columns.Add(newColName);
            }

            // Add rows by looping columns        
            for (int rCount = 1; rCount <= inputTable.Columns.Count - 1; rCount++)
            {
                DataRow newRow = outputTable.NewRow();

                // First column is inputTable's Header row's second column
                newRow[0] = inputTable.Columns[rCount].ColumnName.ToString().Replace(",", "");
                for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                {
                    string colValue = inputTable.Rows[cCount][rCount].ToString().Replace(",", "");
                    newRow[cCount + 1] = colValue;
                }
                outputTable.Rows.Add(newRow);
            }

            return outputTable;
        }

        #endregion


        /// <summary>
        /// Author : Anamika Kumari
        /// Date : June 6, 2017
        /// Desc : Getting the questions list based on location id from database
        /// </summary>
        /// <param name="location_id">User's location id</param>
        /// <returns></returns>
        public string GetQuestionByLocation(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("Select dbo.GetMyQuestionList(" + paramdetails.location_id + ")", connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = objOutputDetail.Error_msg;
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        public string GetMyTasksById(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("Select dbo.GetMyTasks(" + paramdetails.p_line_id + ")", connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = objOutputDetail.Error_msg;
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_line_product_rel_id">Line Product relation Id</param>
        /// <returns></returns>
        public string GetAnswerHistoryById(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("Select dbo.GetAnswerHistory(" + paramdetails.p_line_id + ")", connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = objOutputDetail.Error_msg;
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_line_product_rel_id">Line Product relation Id</param>
        /// <returns></returns>
        public string GetAuditScoreById(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());

            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("Select dbo.GetAuditScore(" + paramdetails.p_line_id + ", '" + paramdetails.p_duration + "')", connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = objOutputDetail.Error_msg;
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_line_product_rel_id">Line Product relation Id</param>
        /// <param name="p_user_id"> User Id</param>
        /// <returns></returns>
        public string GetAuditPlanById(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            string p_location_id;
            string line_id;
            string user_id;
            string query;
            string login = string.Empty;
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    if (paramdetails.location_id != 0)
                        p_location_id = Convert.ToString(paramdetails.location_id);
                    else
                        p_location_id = "null";
                    if (paramdetails.p_user_id != 0 && Convert.ToString(paramdetails.p_user_id) != "")
                        user_id = Convert.ToString(paramdetails.p_user_id);
                    else
                        user_id = "null";
                    if (paramdetails.p_line_id != 0 && Convert.ToString(paramdetails.p_line_id) != "")
                        line_id = Convert.ToString(paramdetails.p_line_id);
                    else
                        line_id = "null";
                    query = "Select dbo.GetAuditPlan(" + p_location_id + "," + line_id + "," + user_id + ")";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = objOutputDetail.Error_msg;
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        public string InsAuditResultsToDatabase(AuditResultParams objParam)
        {
            string Output = string.Empty;

            DataSet dsOutput = new DataSet();
            SqlDataAdapter sqlda = null;
            DataTable dtAnswer = new DataTable();
            List<AnsResultsTableType> objAnsResults = new List<AnsResultsTableType>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());

            try
            {
                objOutputDetail = new OutputDetail();
                Parameters paramdetails = new Parameters();
                paramdetails.username = objParam.username;
                paramdetails.encryptedPassword = Encrypt(objParam.password);
                paramdetails.userID = objParam.userID;
                paramdetails.deviceID = objParam.deviceID;
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    JavaScriptSerializer ser = new JavaScriptSerializer();
                    objAnsResults = ser.Deserialize<List<AnsResultsTableType>>(objParam.p_json_AuditResults);
                    dtAnswer = CreateDataTable<AnsResultsTableType>(objAnsResults);
                    dtAnswer.Columns.Remove("image_file_name");
                    dtAnswer.Columns.Add("image_file_name");
                    dtAnswer.Columns.Remove("imageCount");
                    ParamLog(Convert.ToString(dtAnswer.Rows.Count));
                    ParamLog(objParam.p_json_AuditResults);

                    ParamLog(Convert.ToString("p_audit_plan_id : " + objParam.p_audit_plan_id + ", p_line_product_rel_id: " + objParam.p_line_product_rel_id + ",  p_audited_by_user_id" + objParam.p_audited_by_user_id + ", p_shift_no" + objParam.p_shift_no));
                    for (int i = 0; i < dtAnswer.Rows.Count; i++)
                    {
                        ParamLog(Convert.ToString("question_id : " + dtAnswer.Rows[i]["question_id"] + "-- answer: " + dtAnswer.Rows[i]["answer"] + ",  "));
                    }

                    ParamLog("Line_id : " + objParam.p_line_id + "; Part_Rel_id : " + objParam.p_line_product_rel_id);
                    connection.Open();
                    SqlCommand command = new SqlCommand("InsAuditResults", connection);
                    command.CommandType = CommandType.StoredProcedure;

                    ParamLog("InsAuditResults");
                    command.Parameters.Add("@p_audit_plan_id", SqlDbType.Int).Value = objParam.p_audit_plan_id;
                    //command.Parameters.Add("p_line_product_rel_id", SqlDbType.Int).Value = objParam.p_line_product_rel_id;
                    command.Parameters.Add("@p_audited_by_user_id", SqlDbType.Int).Value = objParam.p_audited_by_user_id;
                    command.Parameters.Add("@p_shift_no", SqlDbType.Int).Value = objParam.p_shift_no;
                    command.Parameters.AddWithValue("@p_AuditResults", dtAnswer);
                    sqlda = new SqlDataAdapter(command);
                    sqlda.Fill(dsOutput);
                    if (dsOutput != null)
                    {
                        if (dsOutput.Tables[0].Rows.Count > 0)
                        {
                            Output = Convert.ToString(dsOutput.Tables[0].Rows[0][0]);
                        }
                    }
                }
                else
                {
                    Output = objOutputDetail.Error_msg;
                }
                //Output = Convert.ToString(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                Output = "";
                ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return Output;
        }

        private DataSet GetAnswers(int audit_id)
        {
            DataSet dsOutput = new DataSet();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand("getAuditResultsForReport", sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@p_audit_id", SqlDbType.Int).Value = audit_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dsOutput);
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dsOutput;
        }

        internal string GetReviewHistoryById(Parameters objParam)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            try
            {
                objOutputDetail = new OutputDetail();
                objParam.encryptedPassword = Encrypt(objParam.password);
                objOutputDetail = checkLogin(objParam);
                if (objOutputDetail.Error_code == 0)
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("Select dbo.GetReviewHistory(" + objParam.p_line_id + "," + +objParam.p_question_id + ")", connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        internal string InsAuditReviewsToDatabase(AuditResultParams objParam)
        {
            string Output = string.Empty;
            DataTable dtOutput = new DataTable();
            DataTable dtAnswer = new DataTable();
            List<AnsReviewsTableType> objAnsResults = new List<AnsReviewsTableType>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            try
            {
                objOutputDetail = new OutputDetail();
                Parameters paramdetails = new Parameters();
                paramdetails.username = objParam.username;
                paramdetails.encryptedPassword = Encrypt(objParam.password);
                paramdetails.userID = objParam.userID;
                paramdetails.deviceID = objParam.deviceID;
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    JavaScriptSerializer ser = new JavaScriptSerializer();
                    objAnsResults = ser.Deserialize<List<AnsReviewsTableType>>(objParam.p_json_AuditResults);
                    dtAnswer = CreateDataTable<AnsReviewsTableType>(objAnsResults);

                    if (!dtAnswer.Columns.Contains("audit_id"))
                    {
                        dtAnswer.Columns.Add("audit_id");
                    }
                    for (int i = 0; i < dtAnswer.Rows.Count; i++)
                    {
                        dtAnswer.Rows[i]["audit_id"] = objParam.p_audit_id;
                    }
                    using (var bulkCopy = new SqlBulkCopy(connection.ConnectionString, SqlBulkCopyOptions.KeepIdentity))
                    {
                        // my DataTable column names match my SQL Column names, so I simply made this loop. However if your column names don't match, just pass in which datatable name matches the SQL column name in Column Mappings
                        foreach (DataColumn col in dtAnswer.Columns)
                        {
                            bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                        }

                        bulkCopy.BulkCopyTimeout = 600;
                        bulkCopy.DestinationTableName = "temp_AnsReviewsTableType";
                        bulkCopy.WriteToServer(dtAnswer);
                    }
                    connection.Open();


                    SqlCommand command = new SqlCommand("InsAuditReviews", connection);
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add("@p_audit_id", SqlDbType.Int).Value = objParam.p_audit_id;
                    //command.Parameters.Add("@p_line_product_rel_id", SqlDbType.Int).Value = objParam.p_line_product_rel_id;
                    command.Parameters.Add("@p_reviewed_by_user_id", SqlDbType.Int).Value = objParam.p_reviewed_by_user_id;

                    //command.Parameters.AddWithValue("@p_AuditReview", dtAnswer);

                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return Output;
        }

        internal string GetQRCodeFromDB(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            string line_id;
            string query;
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {

                    if (paramdetails.p_line_id != 0 && Convert.ToString(paramdetails.p_line_id) != "")
                        line_id = Convert.ToString(paramdetails.p_line_id);
                    else
                        line_id = "null";
                    query = "Select dbo.GetQRDetails(" + line_id + ")";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        internal string GetUserDetailseFromDB(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            string query;
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    query = "Select dbo.UserLogin('" + paramdetails.username + "', '" + Encrypt(paramdetails.password) + "')";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        internal string GetAllMyLinesFromDB(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            string query;
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    query = "Select dbo.GetAllMyLines(" + paramdetails.location_id + ")";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        internal string GetRegionListFromDB(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            string query;
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    query = "Select dbo.GetRegionList(" + paramdetails.p_user_id + ")";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        internal string GetCountryListFromDB(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            string query;
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    query = "Select dbo.GetCountryList(" + paramdetails.p_user_id + ", " + paramdetails.p_region_id + ")";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        internal string GetLocationListFromDB(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            string query;
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    query = "Select dbo.GetLocationList(" + paramdetails.p_user_id + ", " + paramdetails.p_country_id + ")";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        internal string getLineProductForLocationFromDB(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            string query;
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    query = "Select dbo.getLineProductForLocation(" + paramdetails.location_id + ", " + paramdetails.p_user_id + ")";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        internal string GetAuditScoreSummaryFromDB(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            string query;
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    query = "Select dbo.GetAuditScoreSummary(" + paramdetails.p_division_id + ", " + paramdetails.p_region_id + ", " + paramdetails.p_country_id + ", " + paramdetails.location_id + ", " + paramdetails.p_line_id + ")";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        internal string InsUserFeedbackFromDB(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("InsUserFeedback", connection);
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add("@p_user_id", SqlDbType.Int).Value = paramdetails.p_user_id;
                    command.Parameters.Add("@p_rating", SqlDbType.Int).Value = paramdetails.p_rating;
                    command.Parameters.Add("@p_comments", SqlDbType.VarChar, 100).Value = paramdetails.p_comments;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return Output;
        }

        internal string InsProfileChangeReqFromDB(Parameters paramdetails)
        {
            string Output = string.Empty;
            DataSet dsOutput = new DataSet();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter da;
            try
            {
                //objOutputDetail = new OutputDetail();
                //paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                //objOutputDetail = checkLogin(paramdetails);
                //if (objOutputDetail.Error_code == 0)
                //{
                    connection.Open();
                    SqlCommand command = new SqlCommand("InsProfileChangeReq", connection);
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add("@p_user_id", SqlDbType.Int).Value = paramdetails.p_user_id;
                    command.Parameters.Add("@p_username", SqlDbType.VarChar, 100).Value = paramdetails.username;
                    command.Parameters.Add("@p_comments", SqlDbType.VarChar, 100).Value = paramdetails.p_comments;
                    da = new SqlDataAdapter(command);
                    da.Fill(dsOutput);
                    if (dsOutput.Tables.Count > 0)
                    {
                        if (dsOutput.Tables[0] != null)
                        {
                            if (dsOutput.Tables[0].Rows.Count > 0)
                            {
                                Output = Convert.ToString(dsOutput.Tables[0].Rows[0][0]);
                            }
                        }
                        if (dsOutput.Tables[1] != null)
                        {
                            if (dsOutput.Tables[1].Rows.Count > 0)
                            {
                                EmailDetails objEmail = new EmailDetails();
                                objEmail.toMailId = Convert.ToString(dsOutput.Tables[1].Rows[0]["to_email"]);
                                objEmail.subject = Convert.ToString(dsOutput.Tables[1].Rows[0]["Sub"]);
                                objEmail.body = Convert.ToString(dsOutput.Tables[1].Rows[0]["content"]);
                                objEmail.fromMailId = Convert.ToString(dsOutput.Tables[1].Rows[0]["from_email"]);
                                SendMail(objEmail);
                            }
                        }
                    }
                    Output = Convert.ToString(command.ExecuteScalar());
                //}
                //else
                //{
                //    Output = Convert.ToString(objOutputDetail.Error_msg);
                //}
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return Output;
        }

        internal string UpdUserPasswordFromDB(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.oldPassword);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("UpdUserPassword", connection);
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add("@p_user_id", SqlDbType.Int).Value = paramdetails.p_user_id;
                    command.Parameters.Add("@PASSWORD", SqlDbType.VarChar, -1).Value = Encrypt(paramdetails.password);
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return Output;
        }

        internal string GetLatestAuditScoreSummaryFromDB(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            string query;
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(paramdetails.p_region_id)))
                        paramdetails.p_region_id = 0;
                    if (string.IsNullOrEmpty(Convert.ToString(paramdetails.p_country_id)))
                        paramdetails.p_country_id = 0;
                    if (string.IsNullOrEmpty(Convert.ToString(paramdetails.location_id)))
                        paramdetails.location_id = 0;
                    if (string.IsNullOrEmpty(Convert.ToString(paramdetails.p_line_id)))
                        paramdetails.p_line_id = 0;
                    query = "Select dbo.GetLatestAuditScoreSummary("+paramdetails.p_division_id+", " + paramdetails.p_region_id + "," + paramdetails.p_country_id + "," + paramdetails.location_id + "," + paramdetails.p_line_id + ")";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        internal string GetLatestAuditScoreSummaryTotalFromDB(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            string query;
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(paramdetails.p_region_id)))
                        paramdetails.p_region_id = 0;
                    if (string.IsNullOrEmpty(Convert.ToString(paramdetails.p_country_id)))
                        paramdetails.p_country_id = 0;
                    if (string.IsNullOrEmpty(Convert.ToString(paramdetails.location_id)))
                        paramdetails.location_id = 0;
                    if (string.IsNullOrEmpty(Convert.ToString(paramdetails.p_line_id)))
                        paramdetails.p_line_id = 0;
                    query = "Select dbo.GetLatestAuditScoreSummaryTotal(" + paramdetails.p_division_id + ", " + paramdetails.p_region_id + "," + paramdetails.p_country_id + "," + paramdetails.location_id + "," + paramdetails.p_line_id + ")";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        internal string GetAuditScoreSummaryTotalFromDB(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            string query;
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(paramdetails.p_region_id)))
                        paramdetails.p_region_id = 0;
                    if (string.IsNullOrEmpty(Convert.ToString(paramdetails.p_country_id)))
                        paramdetails.p_country_id = 0;
                    if (string.IsNullOrEmpty(Convert.ToString(paramdetails.location_id)))
                        paramdetails.location_id = 0;
                    if (string.IsNullOrEmpty(Convert.ToString(paramdetails.p_line_product_rel_id)))
                        paramdetails.p_line_id = 0;
                    query = "Select dbo.GetAuditScoreSummaryTotal(" + paramdetails.p_division_id + ", " + paramdetails.p_region_id + "," + paramdetails.p_country_id + "," + paramdetails.location_id + "," + paramdetails.p_line_id + ")";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        internal string SaveImageToDatabase(ImageContent objImage)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());

            DataSet dsOutput = new DataSet();
            SqlDataAdapter sqlda = null;
            try
            {
                objOutputDetail = new OutputDetail();
                Parameters paramdetails = new Parameters();
                paramdetails.username = objImage.username;
                paramdetails.encryptedPassword = Encrypt(objImage.password);
                paramdetails.userID = objImage.userID;
                paramdetails.deviceID = objImage.deviceID;
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("sp_saveImageFile", connection);
                    command.CommandType = CommandType.StoredProcedure;


                    command.Parameters.Add("@p_audit_id", SqlDbType.Int).Value = objImage.audit_id;
                    command.Parameters.Add("@p_question_id", SqlDbType.Int).Value = objImage.question_id;
                    command.Parameters.Add("@p_filename", SqlDbType.VarChar, -1).Value = objImage.filename;
                    command.Parameters.Add("@p_flag", SqlDbType.VarChar, 20).Value = objImage.flag;
                    sqlda = new SqlDataAdapter(command);
                    sqlda.Fill(dsOutput);
                    if (dsOutput != null)
                    {
                        if (dsOutput.Tables[0].Rows.Count > 0)
                        {
                            Output = Convert.ToString(dsOutput.Tables[0].Rows[0][0]);
                            ParamLog("Output : " + Output);
                        }
                    }
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }

                //Output = Convert.ToString(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return Output;
        }

        internal string SendMail(ImageContent paramdetails)
        {
            string output = string.Empty;
            string mail_id = string.Empty;
            int audit_id = Convert.ToInt32(paramdetails.audit_id);
            ParamLog("Email(audit_id) : " + Convert.ToString(paramdetails.audit_id));
            DataSet dsAnswers = new DataSet();
            DataSet dsImage = new DataSet();
            string html = string.Empty;
            string filename = string.Empty;
            string filepath = string.Empty;
            string Location = string.Empty;
            string Line = string.Empty;
            try
            {
                objOutputDetail = new OutputDetail();
                //Parameters paramdetail = new Parameters();
                //paramdetail.username = paramdetails.username;
                //paramdetail.encryptedPassword = Encrypt(paramdetails.password);
                //paramdetail.userID = paramdetails.userID;
                //paramdetail.deviceID = paramdetails.deviceID;
                //objOutputDetail = checkLogin(paramdetail);
                objOutputDetail.Error_code = 0;
                if (objOutputDetail.Error_code == 0)
                {
                    dsAnswers = GetAnswers(audit_id);
                    if (dsAnswers != null)
                    {
                        dsImage = dsAnswers.Copy();
                        html = "<html ><head></head><body>";
                        //html += coverpage();
                        //html += heading();
                        for (int i = 0; i < dsAnswers.Tables.Count; i++)
                        {
                            if (i == 0)
                            {
                                //html += firstpagedetails(dsAnswers.Tables[0]);
                                html += newheading(dsAnswers.Tables[0]);
                                html += questionheading();
                                Location = Convert.ToString(dsAnswers.Tables[0].Rows[0]["location_name"]);
                                Line = Convert.ToString(dsAnswers.Tables[0].Rows[0]["line_name"]);
                                mail_id = Convert.ToString(dsAnswers.Tables[0].Rows[0]["distribution_list"]);
                                ParamLog("Email(mailId) : " + Convert.ToString(mail_id));
                            }
                            else
                            {
                                dsAnswers.Tables[i].Columns.Remove("image_file_name");
                                //dsAnswers.Tables[i].Columns.Remove("display_sequence");
                                //dsAnswers.Tables[i].Columns.Remove("location_name");
                                //dsAnswers.Tables[i].Columns.Remove("line_name");
                                //dsAnswers.Tables[i].Columns.Remove("product_name");
                                html += ExportDatatableToHtml(dsAnswers.Tables[i]);
                            }
                        }
                        html += getLastRow(dsAnswers.Tables[0]);
                        StringBuilder strHTMLBuilder = new StringBuilder();
                        strHTMLBuilder.Append("</table>");
                        html += strHTMLBuilder.ToString();

                        strHTMLBuilder = new StringBuilder();
                        strHTMLBuilder.Append("<div style='height:160px;'></div>");
                        html += strHTMLBuilder.ToString();
                        html += newheading(dsAnswers.Tables[0]);
                        filename = "Audit_Result_" + Convert.ToString(dsAnswers.Tables[0].Rows[0]["Document_no"]) + ".pdf";
                        filepath = ConfigurationManager.AppSettings["PDF_Folder"].ToString();


                        Bindchart(dsAnswers.Tables[0], audit_id);
                        html += BindchartData(dsAnswers.Tables[0], audit_id);
                        //html += BindImages(dsImage);
                        html += "</body></html>";

                        convertPDF(html, filepath, filename, dsImage);
                        //Session["filename"] = Convert.ToString(filepath + filename);
                        ParamLog("Email(mailId1) : " + Convert.ToString(mail_id));
                        if (!string.IsNullOrEmpty(mail_id))
                        {
                            ParamLog("Email(ReportTomailId) : " + Convert.ToString(mail_id));
                            EmailDetails objEmail = new EmailDetails();
                            objEmail.toMailId = mail_id;
                            objEmail.subject = "LPA Audit Report for " + Location + "-" + Line;
                            objEmail.body = "Layered Process Audit for " + Location + "-" + Line + " is completed and audit report is attached.  This audit report was also sent to Central Processing Team.";
                            objEmail.attachment = Convert.ToString(filepath + filename);
                            SendMail(objEmail);
                        }
                        output = "[{\"err_code\":0,\"err_message\":\"Mail is sent successfully\"}]";
                    }
                }
                else
                {
                    output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return output;
        }

        internal string SaveImageForTaskToDatabase(ImageContent objImage)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            DataSet dsOutput = new DataSet();
            try
            {
                objOutputDetail = new OutputDetail();
                //Parameters paramdetails = new Parameters();
                //paramdetails.username = objImage.username;
                //paramdetails.encryptedPassword = Encrypt(objImage.password);
                //paramdetails.userID = objImage.userID;
                //paramdetails.deviceID = objImage.deviceID;
                //objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("sp_saveImageForTasks", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@p_audit_id", SqlDbType.Int).Value = objImage.audit_id;
                    command.Parameters.Add("@p_question_id", SqlDbType.Int).Value = objImage.question_id;
                    command.Parameters.Add("@p_filename", SqlDbType.VarChar, -1).Value = objImage.filename;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }

            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return Output;
        }

        internal string InsAuditResultsWithPartToDatabase(AuditResultParams objParam)
        {
            string Output = string.Empty;

            DataSet dsOutput = new DataSet();
            SqlDataAdapter sqlda = null;
            DataTable dtAnswer = new DataTable();
            List<AnsResultsTableType> objAnsResults = new List<AnsResultsTableType>();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());

            try
            {
                objOutputDetail = new OutputDetail();
                Parameters paramdetails = new Parameters();
                paramdetails.username = objParam.username;
                paramdetails.encryptedPassword = Encrypt(objParam.password);
                paramdetails.userID = objParam.userID;
                paramdetails.deviceID = objParam.deviceID;
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    JavaScriptSerializer ser = new JavaScriptSerializer();
                    objAnsResults = ser.Deserialize<List<AnsResultsTableType>>(objParam.p_json_AuditResults);
                    dtAnswer = CreateDataTable<AnsResultsTableType>(objAnsResults);
                    dtAnswer.Columns.Remove("image_file_name");
                    dtAnswer.Columns.Add("image_file_name");
                    int audit_id = GetMaxAuditDAL();
                    ParamLog("Next audit id : " + Convert.ToString(audit_id));

                    ParamLog(Convert.ToString(dtAnswer.Rows.Count));
                    ParamLog(objParam.p_json_AuditResults);
                    string filepath = ConfigurationManager.AppSettings["PublishedImageURL"].ToString();
                    filepath = filepath + "/" + Convert.ToString(audit_id) + "/";
                    //+ parser.Filename + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                    string filename = string.Empty;
                    ParamLog(Convert.ToString("p_audit_plan_id : " + objParam.p_audit_plan_id + ", p_line_product_rel_id: " + objParam.p_line_product_rel_id + ",  p_audited_by_user_id" + objParam.p_audited_by_user_id + ", p_shift_no" + objParam.p_shift_no));
                    for (int i = 0; i < dtAnswer.Rows.Count; i++)
                    {
                        filename = string.Empty;
                        if (Convert.ToInt32(dtAnswer.Rows[i]["imageCount"]) > 1)
                        {
                            for (int j = 0; j < Convert.ToInt32(dtAnswer.Rows[i]["imageCount"]); j++)
                            {
                                if (String.IsNullOrEmpty(filename))
                                    filename = filepath + Convert.ToString(audit_id) + "_" + Convert.ToString(dtAnswer.Rows[i]["question_id"]) + "_" + Convert.ToString(j) + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                else
                                    filename = filename + ";" + filepath + Convert.ToString(audit_id) + "_" + Convert.ToString(dtAnswer.Rows[i]["question_id"]) + "_" + Convert.ToString(j) + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                            }
                        }
                        else if (Convert.ToInt32(dtAnswer.Rows[i]["imageCount"]) == 1)
                        {
                            filename = filepath + Convert.ToString(audit_id) + "_" + Convert.ToString(dtAnswer.Rows[i]["question_id"]) + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                        }
                        dtAnswer.Rows[i]["image_file_name"] = filename;
                        ParamLog(Convert.ToString("question_id : " + dtAnswer.Rows[i]["question_id"] + "-- answer: " + dtAnswer.Rows[i]["answer"] + ",  "));
                    }

                    ParamLog("Line_id : " + objParam.p_line_id + "; Part_Rel_id : " + objParam.p_line_product_rel_id);
                    //connection.Open();
                    //string query = "SELECT ISNULL(MAX(audit_id),1)+1 FROM mh_lpa_local_answers";
                    dtAnswer.Columns.Remove("imageCount");
                    ParamLog("Line_id : " + objParam.p_line_id + "; Part_Number : " + objParam.p_part_number);
                    connection.Open();
                    SqlCommand command = new SqlCommand("InsAuditResults_New", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    ParamLog("InsAuditResults_New");
                    command.Parameters.Add("@p_audit_id", SqlDbType.Int).Value = audit_id;
                    command.Parameters.Add("@p_audit_plan_id", SqlDbType.Int).Value = objParam.p_audit_plan_id;
                    command.Parameters.Add("@p_line_id", SqlDbType.Int).Value = objParam.p_line_id;
                    //command.Parameters.Add("@p_part_number", SqlDbType.VarChar, 100).Value = objParam.p_part_number;
                    command.Parameters.Add("@p_audited_by_user_id", SqlDbType.Int).Value = objParam.p_audited_by_user_id;
                    //command.Parameters.Add("@p_audit_date", SqlDbType.DateTime).Value = Convert.ToDateTime(objParam.p_audit_date);
                    command.Parameters.Add("@p_shift_no", SqlDbType.Int).Value = objParam.p_shift_no;
                    command.Parameters.AddWithValue("@p_AuditResults", dtAnswer);
                    sqlda = new SqlDataAdapter(command);
                    sqlda.Fill(dsOutput);
                    if (dsOutput != null)
                    {
                        if (dsOutput.Tables[0].Rows.Count > 0)
                        {
                            Output = Convert.ToString(dsOutput.Tables[0].Rows[0][0]);
                        }
                    }
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
                //Output = Convert.ToString(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                Output = "";
                ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return Output;
        }

        public int GetMaxAuditDAL()
        {
            int p_audit_id = 0;
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand("GetMaxAuditId", sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@p_audit_id", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                p_audit_id = Convert.ToInt32(sqlcmd.Parameters["@p_audit_id"].Value);

            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return p_audit_id;
        }

        internal string GetMyQuestionListForLineFromDB(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("Select dbo.GetMyQuestionList_new(" + paramdetails.p_line_id + ")", connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }

        internal string GetDivisionListFromDB(Parameters paramdetails)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            string query;
            try
            {
                objOutputDetail = new OutputDetail();
                paramdetails.encryptedPassword = Encrypt(paramdetails.password);
                objOutputDetail = checkLogin(paramdetails);
                if (objOutputDetail.Error_code == 0)
                {
                    query = "Select dbo.GetDivisionList()";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    command.CommandType = CommandType.Text;
                    Output = Convert.ToString(command.ExecuteScalar());
                }
                else
                {
                    Output = Convert.ToString(objOutputDetail.Error_msg);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return Output;
        }


        #region Encryption

        public string Decrypt(string cipherText)
        {
            try
            {
                string EncryptionKey = "1234256wqhgtdfqwsydtrwsd";
                byte[] cipherBytes = Convert.FromBase64String(cipherText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
            }
            catch (Exception)
            {
                //objCom.ErrorLog(ex);
            }
            return cipherText;
        }

        public string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text  
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it  
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits  
                //for each byte  
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }  

        public string Encrypt(string key, string data)
        {
            string encData = null;
            byte[][] keys = GetHashKeys(key);

            try
            {
                encData = EncryptStringToBytes_Aes(data, keys[0], keys[1]);
            }
            catch (CryptographicException) { }
            catch (ArgumentNullException) { }

            return encData;
        }

        public string Decrypt(string key, string data)
        {
            string decData = null;
            byte[][] keys = GetHashKeys(key);

            try
            {
                decData = DecryptStringFromBytes_Aes(data, keys[0], keys[1]);
            }
            catch (CryptographicException) { }
            catch (ArgumentNullException) { }

            return decData;
        }

        private byte[][] GetHashKeys(string key)
        {
            byte[][] result = new byte[2][];
            Encoding enc = Encoding.UTF8;

            SHA256 sha2 = new SHA256CryptoServiceProvider();

            byte[] rawKey = enc.GetBytes(key);
            byte[] rawIV = enc.GetBytes(key);

            byte[] hashKey = sha2.ComputeHash(rawKey);
            byte[] hashIV = sha2.ComputeHash(rawIV);

            Array.Resize(ref hashIV, 16);

            result[0] = rawKey;
            result[1] = rawIV;

            return result;
        }

        //source: https://msdn.microsoft.com/de-de/library/system.security.cryptography.aes(v=vs.110).aspx
        private static string EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            byte[] encrypted;

            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt =
                            new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(encrypted);
        }

        //source: https://msdn.microsoft.com/de-de/library/system.security.cryptography.aes(v=vs.110).aspx
        private static string DecryptStringFromBytes_Aes(string cipherTextString, byte[] Key, byte[] IV)
        {
            byte[] cipherText = Convert.FromBase64String(cipherTextString);

            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            string plaintext = null;

            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt =
                            new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }

        string EncryptOrDecrypt(string text, string key)//Your Key as parameter
        {
            var result = new StringBuilder();

            for (int c = 0; c < text.Length; c++)
                result.Append((char)((uint)text[c] ^ (uint)key[c % key.Length]));

            return result.ToString();
        }

        private string Decrypt256(string text)
        {
            // AesCryptoServiceProvider
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;
            aes.KeySize = 256;
            //aes.IV = Encoding.UTF8.GetBytes(AesIV256);
            //aes.Key = Encoding.UTF8.GetBytes(AesKey256);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            // Convert Base64 strings to byte array
            byte[] src = System.Convert.FromBase64String(text);

            // decryption
            using (ICryptoTransform decrypt = aes.CreateDecryptor())
            {
                byte[] dest = decrypt.TransformFinalBlock(src, 0, src.Length);
                return Encoding.Unicode.GetString(dest);
            }
        }
        #endregion
    }
}