﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;

namespace MH_Logistics_Services
{
    public class StreamExtensions
    {
        csCommonFunctions objComm = new csCommonFunctions();
        public void SerializeTo<T>(T o, Stream stream)
        {
            try
            {
                new BinaryFormatter().Serialize(stream, o);  // serialize o not typeof(T)
            }
            catch (Exception ex)
            {

                objComm.ErrorLog(ex);
            }
            
        }

        public T Deserialize<T>(Stream stream)
        {
            stream.Position = 0;
                return (T)new BinaryFormatter().Deserialize(stream);
        }
    }
}