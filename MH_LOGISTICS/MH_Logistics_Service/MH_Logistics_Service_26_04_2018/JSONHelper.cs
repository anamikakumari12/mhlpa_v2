﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
/// <summary>
/// JSON Serialization and Deserialization Assistant Class
/// </summary>
public class JSONHelper
{
    /// <summary>
    /// JSON Serialization
    /// </summary>
    public string JsonSerializer<T>(T t)
    {
        DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
        MemoryStream ms = new MemoryStream();
        ser.WriteObject(ms, t);
        string jsonString = Encoding.UTF8.GetString(ms.ToArray());
        ms.Close();
        return jsonString;
    }

    /// <summary>
    /// JSON Deserialization
    /// </summary>
    public T JsonDeserialize<T>(string jsonString)
    {
        DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
        MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
        T obj = (T)ser.ReadObject(ms);
        return obj;
    }

    //public T CreateFromJsonStream<T>(Stream stream)
    //{
    //    JSONHelper serializer = new JSONHelper();
    //    T data;
    //    using (StreamReader streamReader = new StreamReader(stream))
    //    {
  
    //        data = (T)serializer.JsonSerializer<T>(streamReader, typeof(T));
    //    }
    //    return data;
    //}

    //private object JsonSerializer<T>(StreamReader streamReader, Type type)
    //{
    //    DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
    //    //MemoryStream ms = new MemoryStream(streamReader);
    //    T obj = (T)ser.ReadObject(ms);
    //    return obj;
    //}

    //public T CreateFromJsonString<T>(String json)
    //{
    //    T data;
    //    using (MemoryStream stream = new MemoryStream(System.Text.Encoding.Default.GetBytes(json)))
    //    {
    //        data = CreateFromJsonStream<T>(stream);
    //    }
    //    return data;
    //}

    //public T CreateFromJsonFile<T>(String fileName)
    //{
    //    T data;
    //    using (FileStream fileStream = new FileStream(fileName, FileMode.Open))
    //    {
    //        data = CreateFromJsonStream<T>(fileStream);
    //    }
    //    return data;
    //}
}