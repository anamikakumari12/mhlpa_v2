
USE [MH_LOGISTICS]
GO
/****** Object:  User [sa]    Script Date: 7/11/2018 3:07:31 PM ******/
CREATE USER [sa] FOR LOGIN [sa] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [sa]
GO
USE [MH_LOGISTICS]
GO
/****** Object:  Sequence [dbo].[audit_id_s]    Script Date: 7/11/2018 3:07:32 PM ******/
CREATE SEQUENCE [dbo].[audit_id_s] 
 AS [bigint]
 START WITH 20000
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
/****** Object:  UserDefinedTableType [dbo].[AnsResultsTableType]    Script Date: 7/11/2018 3:07:32 PM ******/
CREATE TYPE [dbo].[AnsResultsTableType] AS TABLE(
	[question_id] [int] NULL,
	[answer] [int] NULL,
	[remarks] [nvarchar](1000) NULL,
	[image_file_name] [varchar](1000) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[AnsReviewsTableType]    Script Date: 7/11/2018 3:07:34 PM ******/
CREATE TYPE [dbo].[AnsReviewsTableType] AS TABLE(
	[loc_answer_id] [int] NULL,
	[review_closed_on] [varchar](50) NULL,
	[review_closed_status] [int] NULL,
	[review_comments] [nvarchar](1000) NULL,
	[review_image_file_name] [varchar](1000) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[AuditPlanInterfaceTableType]    Script Date: 7/11/2018 3:07:36 PM ******/
CREATE TYPE [dbo].[AuditPlanInterfaceTableType] AS TABLE(
	[location_id] [int] NULL,
	[line_id] [int] NULL,
	[line_name] [varchar](100) NULL,
	[year] [int] NULL,
	[wk1] [int] NULL,
	[wk2] [int] NULL,
	[wk3] [int] NULL,
	[wk4] [int] NULL,
	[wk5] [int] NULL,
	[wk6] [int] NULL,
	[wk7] [int] NULL,
	[wk8] [int] NULL,
	[wk9] [int] NULL,
	[wk10] [int] NULL,
	[wk11] [int] NULL,
	[wk12] [int] NULL,
	[wk13] [int] NULL,
	[wk14] [int] NULL,
	[wk15] [int] NULL,
	[wk16] [int] NULL,
	[wk17] [int] NULL,
	[wk18] [int] NULL,
	[wk19] [int] NULL,
	[wk20] [int] NULL,
	[wk21] [int] NULL,
	[wk22] [int] NULL,
	[wk23] [int] NULL,
	[wk24] [int] NULL,
	[wk25] [int] NULL,
	[wk26] [int] NULL,
	[wk27] [int] NULL,
	[wk28] [int] NULL,
	[wk29] [int] NULL,
	[wk30] [int] NULL,
	[wk31] [int] NULL,
	[wk32] [int] NULL,
	[wk33] [int] NULL,
	[wk34] [int] NULL,
	[wk35] [int] NULL,
	[wk36] [int] NULL,
	[wk37] [int] NULL,
	[wk38] [int] NULL,
	[wk39] [int] NULL,
	[wk40] [int] NULL,
	[wk41] [int] NULL,
	[wk42] [int] NULL,
	[wk43] [int] NULL,
	[wk44] [int] NULL,
	[wk45] [int] NULL,
	[wk46] [int] NULL,
	[wk47] [int] NULL,
	[wk48] [int] NULL,
	[wk49] [int] NULL,
	[wk50] [int] NULL,
	[wk51] [int] NULL,
	[wk52] [int] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[AuditPlanInviteTableType]    Script Date: 7/11/2018 3:08:01 PM ******/
CREATE TYPE [dbo].[AuditPlanInviteTableType] AS TABLE(
	[email_id] [varchar](1000) NULL,
	[start_date] [date] NULL,
	[end_date] [date] NULL,
	[line_name] [varchar](100) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[GroupMembersTableType]    Script Date: 7/11/2018 3:08:03 PM ******/
CREATE TYPE [dbo].[GroupMembersTableType] AS TABLE(
	[user_id] [int] NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[PartsTableType]    Script Date: 7/11/2018 3:08:03 PM ******/
CREATE TYPE [dbo].[PartsTableType] AS TABLE(
	[region] [varchar](100) NULL,
	[country] [varchar](100) NULL,
	[location] [varchar](100) NULL,
	[lineName] [varchar](100) NULL,
	[PartNumber] [varchar](max) NULL
)
GO
/****** Object:  StoredProcedure [dbo].[AuditResultsForDownload]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AuditResultsForDownload](@p_region_id   int,
		@p_country_id   int,
		@p_location_id   int,
		@p_line_id   int,
        @l_st_date    DATE,
        @l_end_date    DATE
        )
AS
 SET NOCOUNT ON;
 
BEGIN

IF @p_region_id IS NULL OR @p_region_id = 0 
  SET @p_region_id = NULL;
 ELSE
  SET @p_region_id = @p_region_id;
 IF @p_country_id IS NULL OR @p_country_id = 0 
  SET @p_country_id = NULL;
 ELSE
  SET @p_country_id = @p_country_id;
 IF @p_location_id IS NULL OR @p_location_id = 0 
  SET @p_location_id = NULL;
 ELSE
  SET @p_location_id = @p_location_id;
 IF @p_line_id IS NULL OR @p_line_id = 0 
  SET @p_line_id = NULL;
 ELSE
  SET @p_line_id = @p_line_id;


	SELECT audit_date,
		  region_name,
		  country_name,
		  location_name,
		  lm.line_name,
		  CASE [q1_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q1,
		  CASE [q2_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q2,
		  CASE [q3_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q3,
		  CASE [q4_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q4,
		  CASE [q5_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q5,
		  CASE [q6_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q6,
		  CASE [q7_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q7,
		  CASE [q8_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q8,
		  CASE [q9_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q9,
		  CASE [q10_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q10,
		  CASE [q1_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END Q1,
		  CASE [q12_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q12,
		  CASE [q13_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q13,
		  CASE [q14_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q14,
		  CASE [q15_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q15,
		  CASE [q16_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q16,
		  CASE [q17_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q17,
		  CASE [q18_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q18,
		  CASE [q19_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q19,
		  CASE [q10_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q20,
		  CASE [q2_answer]
			WHEN 0 THEN 'YES'
			WHEN 1 THEN 'NO'
			ELSE 'N/A'
		  END q2
	  FROM .[dbo].[mh_lpa_score_summary] s,
			mh_lpa_region_master rm,
			mh_lpa_country_master cm,
			mh_lpa_location_master loc,
			mh_lpa_line_master lm
	WHERE s.region_id = rm.region_id
	AND   s.country_id = cm.country_id
	AND   s.location_id = loc.location_id
	AND   s.line_id =lm.line_id
	AND   s.region_id = ISNULL(@p_region_id, s.region_id)
	AND   s.country_id = ISNULL(@p_country_id, s.country_id)
	AND   s.location_id = ISNULL(@p_location_id, s.location_id)
	AND   s.line_id = ISNULL(@p_line_id, s.line_id)
	AND   s.audit_date BETWEEN @l_st_date AND @l_end_date;

END;






GO
/****** Object:  StoredProcedure [dbo].[DelAuditPlan]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DelAuditPlan](@p_audit_plan_id			int,
								@l_err_code				int OUTPUT,
								@l_err_message			varchar(100) OUTPUT
								)
AS
	SET NOCOUNT ON;
	
BEGIN

/* This procedure needs date overlap validation still */

	
	SET @l_err_code = 0;
	SET @l_err_message = 'Initializing';
	
	IF @p_audit_plan_id IS NOT NULL AND @p_audit_plan_id != 0 
	
		BEGIN
	
			DELETE FROM mh_lpa_audit_plan
			WHERE audit_plan_id = @p_audit_plan_id;
			
			SET @l_err_code = 0;
			SET @l_err_message = 'Deleted Successfully';

		END;
		
	ELSE
		BEGIN
			SET @l_err_code = 100;
			SET @l_err_message = 'Unable to find Plan';
		END;

END




GO
/****** Object:  StoredProcedure [dbo].[DelAuditPlan_byDate]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[DelAuditPlan_byDate](@p_line_id   int,
        @l_st_date    DATE,
        @l_end_date    DATE,
        @l_del_invite_to_list  VARCHAR(1000) OUTPUT,
        @l_err_code    int OUTPUT,
        @l_err_message   varchar(100) OUTPUT
        )
AS
 SET NOCOUNT ON;
 
BEGIN
/* This procedure needs date overlap validation still */
 DECLARE  @l_auditor_id   INT;
 DECLARE  @l_audit_plan_id   INT;
 
 SET @l_err_code = 0;
 SET @l_err_message = 'Initializing';
 
 SELECT @l_audit_plan_id = audit_plan_id,
   @l_auditor_id = to_be_audited_by_user_id
 FROM   mh_lpa_audit_plan p
 WHERE  planned_date = @l_st_date
 AND    planned_date_end = @l_end_date
 AND    p.line_id = @p_line_id;
 
 IF @l_audit_plan_id IS NOT NULL
 BEGIN
  EXEC dbo.[DelAuditPlan] @l_audit_plan_id, @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
 END;
 
 IF @l_auditor_id IS NOT NULL
  SELECT @l_del_invite_to_list = email_id
  FROM   mh_lpa_user_master
  WHERE  user_id = @l_auditor_id;
END









GO
/****** Object:  StoredProcedure [dbo].[DelLine]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DelLine](		@p_line_id				int,
								@p_end_date				date,
								@l_err_code				int OUTPUT,
								@l_err_message			varchar(100) OUTPUT

								)
AS
	SET NOCOUNT ON;
	
BEGIN

	SET @l_err_code = 0;
	SET @l_err_message = 'Initializing';

	IF @p_line_id IS NOT NULL AND @p_line_id != 0
	BEGIN

		UPDATE mh_lpa_line_master
		SET    end_date = @p_end_date
		WHERE   line_id = @p_line_id;

		SET @l_err_code = 0;
		SET @l_err_message = 'Deleted Successfully';
			
	END	
	ELSE
		BEGIN
			SET @l_err_code = 100;
			SET @l_err_message = 'Unable to locate Line';
		END;

	
END





GO
/****** Object:  StoredProcedure [dbo].[DelRole]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DelRole](@p_role_id		int,

								@l_err_code				int OUTPUT,
								@l_err_message			varchar(100) OUTPUT)
As
BEGIN
	--DECLARE @l_err_code		INT;
	--DECLARE @l_err_message	VARCHAR(100);
	DECLARE @l_count		INT;

	SET @l_err_code = 0;
	SET @l_err_message = 'Initializing';
	
	SELECT @l_count = count(*)
	FROM   mh_lpa_user_master
	WHERE  role_id = @p_role_id;

	IF @l_count > 0 
	BEGIN
		SET @l_err_code = 100;
		SET @l_err_message = 'This role is already in use';
	END;
	ELSE
	BEGIN
		DELETE FROM mh_lpa_role_master
		WHERE  role_id = @p_role_id;
		SET @l_err_code = 0;
		SET @l_err_message = 'Role Deleted Successfully';
	END;

	SELECT * FROM (select @l_err_code err_code, @l_err_message err_message) a
END;



GO
/****** Object:  StoredProcedure [dbo].[get_st_end_date]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[get_st_end_date](@p_year  VARCHAR(4),
      @p_week_no    INT,
      @l_start_date   DATE OUTPUT,
      @l_end_date    DATE OUTPUT)
AS
BEGIN
 SET @l_start_date = DATEADD(day, 1, DATEADD(wk, DATEDIFF(wk, 6, '1/1/' + @p_year) + (@p_week_no-1), 6));
 SET @l_end_date = DATEADD(day, -1, DATEADD(wk, DATEDIFF(wk, 5, '1/1/' + @p_year) + (@p_week_no-1), 5));
END;



GO
/****** Object:  StoredProcedure [dbo].[get_text_for_audit_report]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_text_for_audit_report](@p_audit_id		INT)
AS
 SET NOCOUNT ON;
 
BEGIN
	DECLARE @l_language		VARCHAR(50);
	DECLARE @p_location_id INT
	SELECT top 1 @p_location_id=location_id FROM   mh_lpa_local_answers WHERE audit_id=@p_audit_id
	Print @p_location_id
	SELECT @l_language = UPPER(COALESCE(default_language,'ENGLISH'))
	FROM   mh_lpa_location_master
	WHERE  location_id = @p_location_id;
	
	-- SET @l_language = 'CHINESE';
	
	SELECT 
		'Language' AS Col_heading,
		@l_language  AS Col_Value
	UNION 
	SELECT 
		'LPA' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
			
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'TITLE'
	UNION 
	SELECT 
		'Plant' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'LINE'
	UNION 
	SELECT 
	'Part' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'PART'
	UNION 
	SELECT 
			'Name' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'NAME'
	UNION 
	SELECT 
			'Date' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'DATE'
	UNION 
	SELECT 
			'Person' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'PERSON'
	UNION 
	SELECT 
			'Shift' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'SHIFT'
	UNION 
	SELECT 
			'Item' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'ITEM'
	UNION 
	SELECT 
			'Checklist' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'CHECKLIST'
	UNION 
	SELECT 
			'Evaluation' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'EVALUATION'
	UNION 
	SELECT 
			'No' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'NOK'
	UNION 
	SELECT 
			'Yes' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'OK'
	UNION 
	SELECT 
			'NA' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'NA'
	UNION 
	SELECT 
			'Findings' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'FINDINGS'
	UNION 
	SELECT 
			'Resp' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'RESP'
	UNION 
	SELECT 
			'Closed_Date' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'CLOSED_DATE'
	UNION 
	SELECT 
			'Result' AS Col_heading,
			CASE @l_language 
				WHEN 'GERMAN' THEN german
				WHEN 'SPANISH' THEN SPANISH
				WHEN 'FRENCH' THEN FRENCH
				WHEN 'CZECH' THEN CZECH
				WHEN 'BOSNIAN' THEN BOSNIAN
				WHEN 'CHINESE' THEN CHINESE
				WHEN 'KOREAN' THEN KOREAN
				WHEN 'THAI' THEN THAI
				WHEN 'PORTUGUESE' THEN PORTUGUESE
				ELSE ENGLISH
			END AS Col_Value
	FROM  mh_lpa_language_master
	WHERE  UPPER(Code) = 'RESULT'
	;

END;



GO
/****** Object:  StoredProcedure [dbo].[getAllMasters]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec getAllMasters 6
CREATE PROCEDURE [dbo].[getAllMasters](@p_user_id		INT=null)
AS
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_location_id 			INT;
	DECLARE @l_global_admin			VARCHAR(1);
	DECLARE @l_local_admin			VARCHAR(1);


	SELECT @l_global_admin = ISNULL(global_admin_flag,'N'),
		@l_local_admin = ISNULL(site_admin_flag,'N'),
		@l_location_id = location_id
	FROM   mh_lpa_user_master
	WHERE  user_id = @p_user_id;
	
	SELECT * FROM mh_lpa_region_master
	WHERE   (@l_global_admin = 'Y' OR
				region_id in (select region_id from mh_lpa_line_master where location_id = @l_location_id) 
				)
	ORDER BY region_name;

	SELECT * FROM mh_lpa_country_master
	WHERE   (@l_global_admin = 'Y' OR
				country_id in (select country_id from mh_lpa_line_master where location_id = @l_location_id) 
				)
	ORDER BY Country_name;

	SELECT region_id,
			country_id,
			location_code, 
			location_id,
			location_name,
			timezone_code,
			timezone_desc, 
			no_of_shifts,
			FORMAT(shift1_start_time,'HH:mm'),
			FORMAT(shift1_end_time,'HH:mm'),
			FORMAT(shift2_start_time,'HH:mm'),
			FORMAT(shift2_end_time,'HH:mm'),
			FORMAT(shift3_start_time,'HH:mm'),
			FORMAT(shift3_end_time,'HH:mm')
	 FROM mh_lpa_location_master
	WHERE   (@l_global_admin = 'Y' OR location_id = @l_location_id)
	ORDER BY location_name;


	SELECT * FROM mh_lpa_line_master
	WHERE   ISNULL(end_date, DATEADD(day,1,getdate())) >= getdate()
	AND     (@l_global_admin = 'Y' OR location_id = @l_location_id)
	ORDER BY line_name; 



	SELECT * FROM [dbo].[mh_lpa_role_master]
	ORDER BY rolename;

	SELECT * from mh_lpa_group_master;

	SELECT * FROM mh_lpa_user_master
	WHERE   (@l_global_admin = 'Y' OR location_id = @l_location_id)
	ORDER BY username; 

	SELECT * FROM mh_lpa_section_master
	ORDER BY section_id;
	

END;





GO
/****** Object:  StoredProcedure [dbo].[getAuditPerformanceForDownload]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getAuditPerformanceForDownload](@p_region_id   int,
		@p_country_id   int,
		@p_location_id   int,
		@p_line_id   int,
        @l_st_date    DATE,
        @l_end_date    DATE
        )
AS
 SET NOCOUNT ON;
 
BEGIN

IF @p_region_id IS NULL OR @p_region_id = 0 
  SET @p_region_id = NULL;
 ELSE
  SET @p_region_id = @p_region_id;
 IF @p_country_id IS NULL OR @p_country_id = 0 
  SET @p_country_id = NULL;
 ELSE
  SET @p_country_id = @p_country_id;
 IF @p_location_id IS NULL OR @p_location_id = 0 
  SET @p_location_id = NULL;
 ELSE
  SET @p_location_id = @p_location_id;
 IF @p_line_id IS NULL OR @p_line_id = 0 
  SET @p_line_id = NULL;
 ELSE
  SET @p_line_id = @p_line_id;


SELECT  region_name,
   country_name,
   location_name,
   line_name,
   dt,
   SUM(cy_audit_planned) no_of_audits_planned,
   SUM(cy_audit_performed) no_of_audits_performed
 FROM (
  SELECT 
     rm.region_name,
     cm.country_name,
     loc.location_name,
	 l.line_name,
                    count(*) cy_audit_planned,
                    0 cy_audit_performed,
     0 ly_audit_planned,
     0 ly_audit_performed,
                    CONVERT(DATE, '01-'+FORMAT(planned_date,'MM-yyyy'),105) dt
        FROM    mh_lpa_audit_plan p,
    mh_lpa_line_master l,
    mh_lpa_location_master loc,
    mh_lpa_country_master cm,
    mh_lpa_region_master rm
        WHERE   p.line_id = l.line_id
  AND     l.region_id = rm.region_id
        AND     l.country_id = cm.country_id
        AND     l.location_id = loc.location_id
        -- AND     planned_date BETWEEN DATEADD(year,-1,getdate()) AND getdate()
		AND   l.region_id = ISNULL(@p_region_id, l.region_id)
	AND   l.country_id = ISNULL(@p_country_id, l.country_id)
	AND   l.location_id = ISNULL(@p_location_id, l.location_id)
	AND   l.line_id = ISNULL(@p_line_id, l.line_id)
	AND   planned_date BETWEEN @l_st_date AND @l_end_date
        GROUP BY  rm.region_name,
     cm.country_name,
     loc.location_name,
     l.line_name,
     CONVERT(DATE, '01-'+FORMAT(planned_date,'MM-yyyy'),105)
        UNION
        SELECT 
       rm.region_name,
     cm.country_name,
     loc.location_name,
	 lm.line_name,
                    0 cy_audit_planned,
                    count(*) cy_audit_performed,
     0 ly_audit_planned,
     0 ly_audit_performed,
                    CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105) dt
        FROM    mh_lpa_score_summary l,
    mh_lpa_line_master lm,
    mh_lpa_location_master loc,
    mh_lpa_country_master cm,
    mh_lpa_region_master rm
        WHERE   l.line_id = lm.line_id
  AND     l.region_id = rm.region_id
        AND     l.country_id = cm.country_id
        AND     l.location_id = loc.location_id
        -- AND     audit_date BETWEEN DATEADD(year,-1,getdate()) AND getdate()
		AND   l.region_id = ISNULL(@p_region_id, l.region_id)
	AND   l.country_id = ISNULL(@p_country_id, l.country_id)
	AND   l.location_id = ISNULL(@p_location_id, l.location_id)
	AND   l.line_id = ISNULL(@p_line_id, l.line_id)
	AND   audit_date BETWEEN @l_st_date AND @l_end_date
        GROUP BY   rm.region_name,
     cm.country_name,
     loc.location_name,
     lm.line_name,CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105)
  ) tmp
 GROUP BY region_name,
   country_name,
   location_name,
   line_name,
   dt;

END;



GO
/****** Object:  StoredProcedure [dbo].[getAuditResultsForReport]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec getAuditResultsForReport 20011

CREATE PROCEDURE [dbo].[getAuditResultsForReport](@p_audit_id         INT)
AS
BEGIN
       SELECT DISTINCT audit_id Document_no, 
                     FORMAT(Convert(date,audit_date),'dd-MMM-yyyy') conducted_on,
                     lme.line_name,
                     lm.location_name,
                     dbo.getEmployeeName(la.audited_by_user_id) conducted_by,
                     dbo.getAuditScorebyAudit(audit_id) Score,
                     CONCAT(lme.distribution_list,'; lpa@mann-hummel.com; anamika@knstek.com; ',mhu.email_id) distribution_list ,
					 --'anamika@knstek.com' distribution_list ,
                     lme.line_id,
                     lm.location_id,
                     lm.country_id,
                     lm.region_id,
      (select username from mh_lpa_user_master where user_id=la.audited_by_user_id) username,
      -- lm.no_of_shifts
      la.shift_No no_of_shifts
       FROM   mh_lpa_local_answers la,
                     mh_lpa_location_master lm,
                     mh_lpa_line_master lme,
                     mh_lpa_user_master mhu
       WHERE la.audit_id = @p_audit_id
       AND   la.location_id = lm.location_id
       AND   la.line_id = lme.line_id
       AND      mhu.user_id=la.audited_by_user_id;


       SELECT 
					-- sm.section_name,
					 dbo.get_translated_text_for_section(la.location_id,sm.section_id) section_name,
                     lq.display_sequence,
                     -- qm.question,
					 dbo.get_translated_text_for_question(la.location_id,qm.question_id) question,
                     la.answer,
                     la.remarks,
                     la.image_file_name,
					 qm.na_flag
       FROM    mh_lpa_local_answers la,
                     mh_lpa_local_questions lq,
                     mh_lpa_question_master qm,
                     mh_lpa_section_master sm
       WHERE la.question_id = qm.question_id
       AND   la.question_id = lq.question_id
       AND   la.location_id = lq.location_id
       AND   qm.section_id = sm.section_id
       AND   la.audit_id = @p_audit_id
       AND   sm.display_sequence = 1
       UNION
       SELECT 
					-- sm.section_name,
					 dbo.get_translated_text_for_section(la.location_id,sm.section_id) section_name,
                     lq.display_sequence,
                     qm.local_question,
                     la.answer,
                     la.remarks,
                     la.image_file_name,
					 qm.na_flag
       FROM    mh_lpa_local_answers la,
                     mh_lpa_local_questions lq,
                     mh_lpa_local_question_master qm,
                     mh_lpa_section_master sm
       WHERE la.question_id = qm.local_question_id
       AND   la.question_id = lq.question_id
       AND   la.location_id = lq.location_id
       AND   qm.section_id = sm.section_id
       AND   la.audit_id = @p_audit_id
       AND   sm.display_sequence = 1;

       SELECT 
					-- sm.section_name,
					 dbo.get_translated_text_for_section(la.location_id,sm.section_id) section_name,
                     lq.display_sequence,
                    -- qm.question,
					 dbo.get_translated_text_for_question(la.location_id,qm.question_id) question,
                      la.answer,
                     la.remarks,
                     la.image_file_name,
					 qm.na_flag
       FROM    mh_lpa_local_answers la,
                     mh_lpa_local_questions lq,
                     mh_lpa_question_master qm,
                     mh_lpa_section_master sm
       WHERE la.question_id = qm.question_id
       AND   la.question_id = lq.question_id
       AND   la.location_id = lq.location_id
       AND   qm.section_id = sm.section_id
       AND   la.audit_id = @p_audit_id
       AND   sm.display_sequence = 2
       UNION
       SELECT 
					-- sm.section_name,
					 dbo.get_translated_text_for_section(la.location_id,sm.section_id) section_name,
                     lq.display_sequence,
                     qm.local_question,
                     la.answer,
                     la.remarks,
                     la.image_file_name,
					 qm.na_flag
       FROM    mh_lpa_local_answers la,
                     mh_lpa_local_questions lq,
                     mh_lpa_local_question_master qm,
                     mh_lpa_section_master sm
       WHERE la.question_id = qm.local_question_id
       AND   la.question_id = lq.question_id
       AND   la.location_id = lq.location_id
       AND   qm.section_id = sm.section_id
       AND   la.audit_id = @p_audit_id
       AND   sm.display_sequence = 2;

       SELECT 
					-- sm.section_name,
					 dbo.get_translated_text_for_section(la.location_id,sm.section_id) section_name,
                     lq.display_sequence,
                    -- qm.question,
					 dbo.get_translated_text_for_question(la.location_id,qm.question_id) question,
                      la.answer,
                     la.remarks,
                     la.image_file_name,
					 qm.na_flag
       FROM    mh_lpa_local_answers la,
                     mh_lpa_local_questions lq,
                     mh_lpa_question_master qm,
                     mh_lpa_section_master sm
       WHERE la.question_id = qm.question_id
       AND   la.question_id = lq.question_id
       AND   la.location_id = lq.location_id
       AND   qm.section_id = sm.section_id
       AND   la.audit_id = @p_audit_id
       AND   sm.display_sequence = 3
       UNION
       SELECT 
					-- sm.section_name,
					 dbo.get_translated_text_for_section(la.location_id,sm.section_id) section_name,
                     lq.display_sequence,
                     qm.local_question,
                     la.answer,
                     la.remarks,
                     la.image_file_name,
					 qm.na_flag
       FROM    mh_lpa_local_answers la,
                     mh_lpa_local_questions lq,
                     mh_lpa_local_question_master qm,
                     mh_lpa_section_master sm
       WHERE la.question_id = qm.local_question_id
       AND   la.question_id = lq.question_id
       AND   la.location_id = lq.location_id
       AND   qm.section_id = sm.section_id
       AND   la.audit_id = @p_audit_id
       AND   sm.display_sequence = 3;

       SELECT 
					-- sm.section_name,
					 dbo.get_translated_text_for_section(la.location_id,sm.section_id) section_name,
                     lq.display_sequence,
                    -- qm.question,
					 dbo.get_translated_text_for_question(la.location_id,qm.question_id) question,
                      la.answer,
                     la.remarks,
                     la.image_file_name,
					 qm.na_flag
       FROM    mh_lpa_local_answers la,
                     mh_lpa_local_questions lq,
                     mh_lpa_question_master qm,
                     mh_lpa_section_master sm
       WHERE la.question_id = qm.question_id
       AND   la.question_id = lq.question_id
       AND   la.location_id = lq.location_id
       AND   qm.section_id = sm.section_id
       AND   la.audit_id = @p_audit_id
       AND   sm.display_sequence = 4
       UNION
       SELECT 
					-- sm.section_name,
					 dbo.get_translated_text_for_section(la.location_id,sm.section_id) section_name,
                     lq.display_sequence,
                     qm.local_question,
                     la.answer,
                     la.remarks,
                     la.image_file_name,
					 qm.na_flag
       FROM    mh_lpa_local_answers la,
                     mh_lpa_local_questions lq,
                     mh_lpa_local_question_master qm,
                     mh_lpa_section_master sm
       WHERE la.question_id = qm.local_question_id
       AND   la.question_id = lq.question_id
       AND   la.location_id = lq.location_id
       AND   qm.section_id = sm.section_id
       AND   la.audit_id = @p_audit_id
       AND   sm.display_sequence = 4;

       SELECT 
					-- sm.section_name,
					 dbo.get_translated_text_for_section(la.location_id,sm.section_id) section_name,
                     lq.display_sequence,
                    -- qm.question,
					 dbo.get_translated_text_for_question(la.location_id,qm.question_id) question,
                      la.answer,
                     la.remarks,
                     la.image_file_name,
					 qm.na_flag
       FROM    mh_lpa_local_answers la,
                     mh_lpa_local_questions lq,
                     mh_lpa_question_master qm,
                     mh_lpa_section_master sm
       WHERE la.question_id = qm.question_id
       AND   la.question_id = lq.question_id
       AND   la.location_id = lq.location_id
       AND   qm.section_id = sm.section_id
       AND   la.audit_id = @p_audit_id
       AND   sm.display_sequence = 5
       UNION
       SELECT 
					-- sm.section_name,
					 dbo.get_translated_text_for_section(la.location_id,sm.section_id) section_name,
                     lq.display_sequence,
                     qm.local_question,
                     la.answer,
                     la.remarks,
                     la.image_file_name,
					 qm.na_flag
       FROM    mh_lpa_local_answers la,
                     mh_lpa_local_questions lq,
                     mh_lpa_local_question_master qm,
                     mh_lpa_section_master sm
       WHERE la.question_id = qm.local_question_id
       AND   la.question_id = lq.question_id
       AND   la.location_id = lq.location_id
       AND   qm.section_id = sm.section_id
       AND   la.audit_id = @p_audit_id
       AND   sm.display_sequence = 5;

       SELECT 
					-- sm.section_name,
					 dbo.get_translated_text_for_section(la.location_id,sm.section_id) section_name,
                     lq.display_sequence,
                    -- qm.question,
					 dbo.get_translated_text_for_question(la.location_id,qm.question_id) question,
                      la.answer,
                     la.remarks,
                     la.image_file_name,
					 qm.na_flag
       FROM    mh_lpa_local_answers la,
                     mh_lpa_local_questions lq,
                     mh_lpa_question_master qm,
                     mh_lpa_section_master sm
       WHERE la.question_id = qm.question_id
       AND   la.question_id = lq.question_id
       AND   la.location_id = lq.location_id
       AND   qm.section_id = sm.section_id
       AND   la.audit_id = @p_audit_id
       AND   sm.display_sequence = 6
       UNION
       SELECT 
					-- sm.section_name,
					 dbo.get_translated_text_for_section(la.location_id,sm.section_id) section_name,
                     lq.display_sequence,
                     qm.local_question,
                     la.answer,
                     la.remarks,
                     la.image_file_name,
					 qm.na_flag
       FROM    mh_lpa_local_answers la,
                     mh_lpa_local_questions lq,
                     mh_lpa_local_question_master qm,
                     mh_lpa_section_master sm
       WHERE la.question_id = qm.local_question_id
       AND   la.question_id = lq.question_id
       AND   la.location_id = lq.location_id
       AND   qm.section_id = sm.section_id
       AND   la.audit_id = @p_audit_id
       AND   sm.display_sequence = 6;
END;


GO
/****** Object:  StoredProcedure [dbo].[GetAuditReviews]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAuditReviews](@p_line_id int)  
 
AS  
BEGIN  


   SELECT loc_answer_id,
		audit_id,
		section_id,
		section_name,
		section_display_sequence,
		question_id,
		question,
		help_text,
		question_display_sequence,
		remarks,
		image_file_name,
		review_comments
	FROM (
		SELECT  la.loc_answer_id,
				la.audit_id,
				sm.section_id,
				sm.section_name,
				sm.display_sequence  section_display_sequence,
				qm.question_id,
				qm.question,
				qm.help_text,
				lq.display_sequence  question_display_sequence,
				la.remarks,
				la.image_file_name,
				la.review_comments
		FROM    mh_lpa_local_answers la,
				mh_lpa_question_master qm,
				mh_lpa_local_questions lq,
				mh_lpa_section_master sm
		WHERE  la.answer = 1
		AND    la.audit_id = (select max(audit_id) 
								from mh_lpa_local_answers la1
								where la1.line_id = @p_line_id)
		AND    la.question_id = qm.question_id
		AND    la.question_id < 10000
		AND    la.location_id = lq.location_id
		AND    lq.question_id = qm.question_id
		AND    lq.section_id = sm.section_id
		AND    ISNULL(la.review_closed_status,0) = 0
		UNION
		SELECT  la.loc_answer_id,
				la.audit_id,
				sm.section_id,
				sm.section_name,
				sm.display_sequence  section_display_sequence,
				qm.local_question_id,
				qm.local_question,
				qm.local_help_text,
				lq.display_sequence  question_display_sequence,
				la.remarks,
				la.image_file_name,
				la.review_comments
		FROM    mh_lpa_local_answers la,
				mh_lpa_local_question_master qm,
				mh_lpa_local_questions lq,
				mh_lpa_section_master sm
		WHERE  la.answer = 1
		AND    la.audit_id = (select max(audit_id) 
								from mh_lpa_local_answers la1
								where la1.line_id = @p_line_id)
		AND    la.question_id = qm.local_question_id
		AND    la.question_id >= 10000
		AND    lq.question_id = qm.local_question_id
		AND    la.location_id = lq.location_id
		AND    lq.section_id = sm.section_id
		AND    ISNULL(la.review_closed_status,0) = 0
		 ) tmp
	ORDER BY section_display_sequence, question_display_sequence
    
END





GO
/****** Object:  StoredProcedure [dbo].[GetAuditScoreSummaryWeb]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec GetAuditScoreSummaryWeb 0,0,0,0
CREATE PROCEDURE [dbo].[GetAuditScoreSummaryWeb](
								@p_region_id		INT=null,
								@p_country_id		INT=null,
								@p_location_id		INT=null,
								@p_line_id int=null,
								@p_audit_id	int=null )  
AS  
BEGIN  
   SELECt sec_id,
			section_name,
			section_abbr,
			avg_score,
			tot_answers
	FROM (
			SELECT Sec_id,
					section_name,
					section_abbr,
					CAST(AVG( CAST(yes_answer AS FLOAT)/CAST(tot_answers AS FLOAT)*100) as numeric(10,2) ) avg_score,
					CAST(AVG(tot_answers) as numeric(10,2) ) tot_answers
			FROM (
				SELECT la.audit_id,
						la.audit_date,
						la.Shift_No,
						sm.section_id as sec_id,
						CASE 
						WHEN CHARINDEX('A. ',section_name)>0 THEN REPLACE(sm.section_name,'A. ','') 
						WHEN CHARINDEX('B. ',section_name)>0 THEN REPLACE(sm.section_name,'B. ','') 
						WHEN CHARINDEX('C. ',section_name)>0 THEN REPLACE(sm.section_name,'C. ','') 
						WHEN CHARINDEX('D. ',section_name)>0 THEN REPLACE(sm.section_name,'D. ','') 
						WHEN CHARINDEX('E. ',section_name)>0 THEN REPLACE(sm.section_name,'E. ','') 
						ELSE section_name
						END section_name,
						CASE
							WHEN section_name = 'General Topics' THEN 'GT'
							WHEN section_name = 'A. People Development' THEN 'A'
							WHEN section_name = 'B. Safe, Organized, Clean Work Area' THEN 'B'
							WHEN section_name = 'C. Robust Processes and Equipment' THEN 'C'
							WHEN section_name = 'D. Standardized Work' THEN 'D'
							WHEN section_name = 'E. Rapid Problem Solving / 8D' THEN 'E'
							ELSE 'OTHERS'
							END section_abbr,
						SUM(CASE 
							WHEN answer = 0 THEN 1
							ELSE 0
							END) yes_answer,
						SUM(CASE	
							WHEN answer = 0 THEN 1
							WHEN answer = 1 THEN 1
							ELSE 0
							END) tot_answers
				FROM    mh_lpa_local_answers la,
						mh_lpa_section_master sm
				WHERE  la.section_id = sm.section_id
			--	AND   la.section_id != 1
			--	AND    audit_id = dbo.GetLowScoreAuditIdbyDate(line_product_rel_id,audit_date)
				AND   answer != 2
				AND    ( 
						(ISNULL(@p_line_id,0) != 0 AND la.line_id = @p_line_id) OR
						ISNULL(@p_line_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_region_id,0) != 0 AND la.region_id = @p_region_id) OR
						ISNULL(@p_region_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_country_id,0) != 0 AND la.country_id = @p_country_id) OR
						ISNULL(@p_country_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_location_id,0) != 0 AND la.location_id = @p_location_id) OR
						ISNULL(@p_location_id,0) = 0
						)
				--AND    FORMAT(la.audit_date,'MM-YYYY') = FORMAT(CURRENT_TIMESTAMP,'MM-YYYY')
				AND   la.audit_id = ISNULL(@p_audit_id, la.audit_id)
				GROUP BY la.audit_id,
						la.audit_date,
						la.Shift_No,
						sm.section_id,
						section_name
				HAVING SUM(CASE	
							WHEN answer = 0 THEN 1
							WHEN answer = 1 THEN 1
							ELSE 0
							END) != 0
				) tmp	
		GROUP BY 
			sec_id, section_name, section_abbr
			) t	
		ORDER BY sec_id
		
END





GO
/****** Object:  StoredProcedure [dbo].[getCountryId]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[getCountryId](@p_country_code	varchar(10),
							@p_country_name		varchar(100),
							@l_country_id		INT		output	)
AS  
BEGIN
	DECLARE @l_count 			INT;
	
	SELECT @l_count = count(*)  
	FROM   mh_lpa_country_master
	WHERE  UPPER(ISNULL(country_code,'*')) = UPPER(ISNULL(@p_country_code,'*'))
	AND    UPPER(country_name) = UPPER(@p_country_name);
	
	IF @l_count = 0 
		exec Inscountry NULL, @p_country_code, @p_country_name;

	
	SELECT @l_country_id	= country_id 
	FROM   mh_lpa_country_master
	WHERE  UPPER(ISNULL(country_code,'*')) = UPPER(ISNULL(@p_country_code,'*'))
	AND    UPPER(country_name) = UPPER(@p_country_name);
		
END






GO
/****** Object:  StoredProcedure [dbo].[GetCountryListforWeb]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetCountryListforWeb](@p_user_id INT,
									@p_region_id int)
AS
BEGIN

	SELECT DISTINCT cm.country_id,
		country_code,
		country_name
	FROM   mh_lpa_country_master cm,
	mh_lpa_location_master lm
	WHERE cm.country_id = lm.country_id
	AND   lm.region_id = @p_region_id
	ORDER BY country_name;

END;






GO
/****** Object:  StoredProcedure [dbo].[getDataforLPAResultsReport]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [getDataforLPAResultsReport] null,null,null,null,'09-10-2017'
CREATE PROCEDURE [dbo].[getDataforLPAResultsReport](@p_region_id         INT=null,
                                                                                        @p_country_id           INT=null,
                                                                                        @p_location_id          INT=null,
                                                                                        @p_line_id                      INT=null,
                                                                                        @p_date                         DATE)
AS
BEGIN
 
        DECLARE @l_region_id            INT;
        DECLARE @l_country_id           INT;
        DECLARE @l_location_id          INT;
        DECLARE @l_line_id              INT;
 
        IF @p_region_id IS NULL OR @p_region_id = 0 
                SET @l_region_id = NULL;
        ELSE
                SET @l_region_id = @p_region_id;
 
        IF @p_country_id IS NULL OR @p_country_id = 0 
                SET @l_country_id = NULL;
        ELSE
                SET @l_country_id = @p_country_id;
 
        IF @p_location_id IS NULL OR @p_location_id = 0 
                SET @l_location_id = NULL;
        ELSE
                SET @l_location_id = @p_location_id;
 
        IF @p_line_id IS NULL OR @p_line_id = 0 
                SET @l_line_id = NULL;
        ELSE
                SET @l_line_id = @p_line_id;
 
 
        SELECT FORMAT(audit_date,'MON-YYYY') audit_period,
                        AVG(total_score) average_score,
                        1 previous_score,
CONVERT(DATE,'01-'+(FORMAT(audit_date,'mm-YYYY')), 105)
        FROM    mh_lpa_score_summary
        WHERE   region_id = ISNULL(@l_region_id, region_id)
        AND     country_id = ISNULL(@l_country_id, country_id)
        AND     location_id = ISNULL(@l_location_id, location_id)
        AND     line_id = ISNULL(@l_line_id, line_id)
        AND     audit_date BETWEEN DATEADD(year,-2,@p_date) AND @p_date
        GROUP BY FORMAT(audit_date,'MON-YYYY'), CONVERT(DATE,'01-'+(FORMAT(audit_date,'mm-YYYY')), 105)
		ORDER BY CONVERT(DATE,'01-'+(FORMAT(audit_date,'mm-YYYY')), 105)

		--order BY CONVERT(DATE,(FORMAT(audit_date,'01-MON-YYYY')))
END






GO
/****** Object:  StoredProcedure [dbo].[GetDistList]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetDistList](@p_location_id			int=null,
								@p_list_type				VARCHAR(100),
								@p_user_id					int=null,
								@l_err_code					int OUTPUT,
								@l_err_message				varchar(100) OUTPUT
								)
AS
	SET NOCOUNT ON;
	
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_email_list		VARCHAR(1000);
	DECLARE @l_owner_email_id		VARCHAR(100);
	DECLARE @l_send_note_to_owner	VARCHAR(1);

	SET @l_err_code = 0;
	SET @l_err_message = 'Initializing';

	IF @p_location_id IS NOT NULL AND @p_location_id != 0 AND @p_list_type IN ('At Audit Plan','At Audit Completion','At New Line Addition')
	BEGIN
/*
		SELECT  email_list
		FROM   mh_lpa_distribution_list
		WHERE location_id = @p_location_id
		AND   UPPER(@p_list_type) = UPPER(list_type)
		AND   ISNULL(send_notification_to_owner,'N') = 'N'
		UNION
		SELECT  email_list+' ;'+ email_id
		FROM   mh_lpa_distribution_list d, mh_lpa_user_master u
		WHERE location_id = @p_location_id
		AND   UPPER(@p_list_type) = UPPER(list_type)
		AND   ISNULL(send_notification_to_owner,'N') = 'Y'
		AND   user_id = @p_user_id
		;

*/

		SELECT  @l_email_list = email_list, @l_send_note_to_owner = send_notification_to_owner
		FROM   mh_lpa_distribution_list
		WHERE location_id = @p_location_id
		AND   UPPER(@p_list_type) = UPPER(list_type);

/*
		IF ISNULL(@l_send_note_to_owner,'N') = 'Y'
		BEGIN
			NULL 

		END;
*/

		SELECT @l_email_list

		SET @l_err_code = 0;
		SET @l_err_message = 'Initializing';

	END;
	ELSE
		BEGIN
			SET @l_err_code = 100;
			SET @l_err_message = 'Unable to find list';
		END;

END




GO
/****** Object:  StoredProcedure [dbo].[GetEmployeeListforWeb]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetEmployeeListforWeb](
@p_location_id
int)
AS
BEGIN

SELECT um.user_id, um.emp_full_name
FROM   [mh_lpa_user_master] um
WHERE um.location_id = @p_location_id
AND   ISNULL(um.end_date, DATEADD(day,1,getdate())) >= getdate()
ORDER BY um.emp_full_name;

END;




GO
/****** Object:  StoredProcedure [dbo].[getGroupId]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getGroupId](@p_location_name		varchar(100),
				@p_group_name		varchar(100),
				@l_group_id		int output)
 
AS  
BEGIN
	DECLARE @l_count 		INT;
	DECLARE @l_location_id		INT;
	
	EXEC getLocationId NULL, @p_location_name, @l_location_id = @l_location_id OUTPUT;

	SELECT @l_count = count(*)  
	FROM   mh_lpa_group_master
	WHERE  UPPER(ISNULL(group_name,'*')) = UPPER(ISNULL(@p_group_name,'*'))
	AND    location_id = @l_location_id;
	
	IF @l_count = 0 
		exec InsGroup  NULL, @p_location_name, @p_group_name;

	
	SELECT @l_group_id	= group_id 
	FROM   mh_lpa_group_master
	WHERE  UPPER(ISNULL(group_name,'*')) = UPPER(ISNULL(@p_group_name,'*'))
	AND    location_id = @l_location_id;

END





GO
/****** Object:  StoredProcedure [dbo].[GetGroupList]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetGroupList](
					@p_location_name		VARCHAR(100),
					@p_location_id			int
								)
AS
	
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_group_id		INT;
	DECLARE @l_location_id		INT;
	
	IF @p_location_id IS NULL OR @p_location_id = 0 
		EXEC getLocationId NULL, @p_location_name, @l_location_id = @l_location_id OUTPUT;
	ELSE
		SET @l_location_id = @p_location_id;
	
	SELECT group_id, group_name
	FROM   mh_lpa_group_master
	WHERE  location_id = @l_location_id;
	

END





GO
/****** Object:  StoredProcedure [dbo].[GetGroupMemberList]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetGroupMemberList](
					@p_location_name		VARCHAR(100)=NULL,
					@p_location_id			int=0,
					@p_group_name			VARCHAR(100)=NULL,
					@p_group_id				int=0
								)
AS
	
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_group_id			INT;
	DECLARE @l_location_id		INT;
	
	IF @p_location_id IS NULL OR @p_location_id = 0 
		EXEC getLocationId NULL, @p_location_name, @l_location_id = @l_location_id OUTPUT;
	ELSE
		SET @l_location_id = @p_location_id;

	IF @p_group_id IS NULL OR @p_group_id = 0 
		BEGIN
			IF @p_group_name IS NULL 
				SET @l_group_id = NULL;
			ELSE
				EXEC getGroupId NULL, @p_location_name, @p_group_name, @l_group_id = @l_group_id OUTPUT;
		END;
	ELSE
		SET @l_group_id = @p_group_id;
	
	SELECT group_member_id, g.group_id, g.group_name, u.user_id,
			u.username, u.emp_full_name
	FROM   mh_lpa_user_master u,
			mh_lpa_group_members gm,
			mh_lpa_group_master g
	WHERE  gm.location_id = @l_location_id
	AND    gm.group_id = ISNULL(@l_group_id, gm.group_id)
	AND    gm.group_id = g.group_id
	AND    gm.user_id = u.user_id;
	


	SELECT gm.group_id, g.group_name,
			stuff(( select ',' + u.emp_full_name 
							from mh_lpa_user_master u,
								 mh_lpa_group_members gm1
							where gm1.user_id = u.user_id
							and   gm1.group_id = gm.group_id
							for xml path('')
							),1,1,'') as emp_full_name
	FROM   mh_lpa_group_members gm,
			mh_lpa_group_master g
	WHERE  gm.location_id = @l_location_id
	AND    gm.group_id = ISNULL(@l_group_id, gm.group_id)
	AND    gm.group_id = g.group_id
	GROUP BY gm.group_id, g.group_name;

END







GO
/****** Object:  StoredProcedure [dbo].[getLineId]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getLineId](  
        @p_region_name   VARCHAR(100),
        @p_region_id   INT,
        @p_country_name   VARCHAR(100),
        @p_country_id   INT,
        @p_location_name  VARCHAR(100),
        @p_location_id   INT,
        @p_line_name   VARCHAR(100),
        @l_line_id    INT OUTPUT)
 
AS  
BEGIN
 DECLARE @l_count    INT
 DECLARE @l_region_id  INT
 DECLARE @l_country_id  INT
 DECLARE @l_location_id  INT
 DECLARE @err_code   INT, @err_msg VARCHAR(100)
 DECLARE @l_region_name  VARCHAR(100)
 DECLARE @l_country_name  VARCHAR(100)
 DECLARE @l_location_name VARCHAR(100)
 

 IF @p_region_id IS NOT NULL AND @p_region_id != 0
  BEGIN
   SET @l_region_id = @p_region_id;
   IF @p_region_name IS NULL OR @p_region_name = ''
    SELECT @l_region_name = region_name
    FROM   mh_lpa_region_master 
    WHERE  region_id = @l_region_id;
   ELSE
    SET @l_region_name = @p_region_name;
  END;
 ELSE
  EXEC getRegionId  NULL, @p_region_name, @l_region_id = @l_region_id OUTPUT;
 
 IF @p_country_id IS NOT NULL  AND @p_country_id != 0
  BEGIN
   SET @l_country_id = @p_country_id;
   IF @p_country_name IS NULL OR @p_country_name = ''
    SELECT @l_country_name = country_name
    FROM   mh_lpa_country_master 
    WHERE  country_id = @l_country_id;
   ELSE
    SET @l_country_name = @p_country_name;
  END;
 ELSE
  EXEC getCountryId  NULL, @p_country_name, @l_country_id = @l_country_id OUTPUT;
 
 IF @p_location_id IS NOT NULL  AND @p_location_id != 0
  BEGIN
   SET @l_location_id = @p_location_id;
   IF @p_location_name IS NULL OR @p_location_name = ''
    SELECT @l_location_name = location_name
    FROM   mh_lpa_location_master 
    WHERE  location_id = @l_location_id;
   ELSE
    SET @l_location_name = @p_location_name;
  END;
 ELSE
  EXEC getLocationId NULL, @p_location_name, @l_location_id = @l_location_id OUTPUT;
 
 
 SELECT @l_count = count(*)  
 FROM   mh_lpa_line_master
 WHERE  UPPER(line_name) = UPPER(@p_line_name)
 AND    location_id = @l_location_id
 AND    country_id = @l_country_id
 AND    region_id = @l_region_id
 AND    ISNULL(end_date, DATEADD(day,1,getdate())) >= getdate();
 
 IF @l_count = 0 
  EXEC InsLine NULL, @l_region_name,
    @l_country_name,
    @l_location_name,
    NULL,
    @p_line_name,
    NULL,
    @err_code OUTPUT,
    @err_msg OUTPUT
 
 SELECT @l_line_id = line_id 
 FROM   mh_lpa_line_master
 WHERE  UPPER(line_name) = UPPER(@p_line_name)
 AND    location_id = @l_location_id
 AND    country_id = @l_country_id
 AND    region_id = @l_region_id
 AND    ISNULL(end_date, DATEADD(day,1,getdate())) >= getdate();
 
 
  RETURN @l_line_id;
 
END



GO
/****** Object:  StoredProcedure [dbo].[getLineListforEdit]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getLineListforEdit](@p_user_id		INT)
AS
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_location_id 			INT;
	DECLARE @l_global_admin			VARCHAR(1);
	DECLARE @l_local_admin			VARCHAR(1);


	SELECT @l_global_admin = ISNULL(global_admin_flag,'N'),
		@l_local_admin = ISNULL(site_admin_flag,'N'),
		@l_location_id = location_id
	FROM   mh_lpa_user_master
	WHERE  user_id = @p_user_id;
	
	SELECT  
			rm.region_id,
			rm.region_name,
			cm.country_id,
			cm.country_name,
			lm.location_id,
			lm.location_name,
			lne.line_id,
			lne.line_name,
			lne.line_code,
			lne.distribution_list
	FROM   mh_lpa_region_master rm,
			mh_lpa_country_master cm,
			mh_lpa_location_master lm,
			mh_lpa_line_master lne
	WHERE  lne.region_id = rm.region_id
	AND    lne.country_id = cm.country_id
	AND    lne.location_id = lm.location_id
	AND		(
			(ISNULL(@l_local_admin,'N') = 'Y' AND lne.location_id = @l_location_id) OR
			(ISNULL(@l_global_admin,'N') = 'Y')
			)
	AND    ISNULL(lne.end_date, DATEADD(day,1,getdate())) >= getdate()
	ORDER BY rm.region_name,
			cm.country_name,
			lm.location_name,
			lne.line_name;

END;




GO
/****** Object:  StoredProcedure [dbo].[GetLineListforWeb]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetLineListforWeb](
@p_location_id
int)
AS
BEGIN

SELECT line_id,
line_code,
line_name
FROM   mh_lpa_line_master lm
WHERE lm.location_id = @p_location_id
AND   ISNULL(lm.end_date, DATEADD(day,1,getdate())) >= getdate()
ORDER BY line_name;

END;




GO
/****** Object:  StoredProcedure [dbo].[getLineLocListforEdit]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec getLineLocListforEdit 6
CREATE PROCEDURE [dbo].[getLineLocListforEdit](@p_user_id		INT)
AS
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_location_id 			INT;
	DECLARE @l_global_admin			VARCHAR(1);
	DECLARE @l_local_admin			VARCHAR(1);


	SELECT @l_global_admin = ISNULL(global_admin_flag,'N'),
		@l_local_admin = ISNULL(site_admin_flag,'N'),
		@l_location_id = location_id
	FROM   mh_lpa_user_master
	WHERE  user_id = @p_user_id;
	print @l_global_admin
	print @l_local_admin
	print @l_location_id
	/*
	SELECT  rel.line_product_rel_id,
			rel.region_id,
			rm.region_name,
			rel.country_id,
			cm.country_name,
			rel.location_id,
			lm.location_name,
			rel.line_id,
			lne.line_name,
			lne.line_code,
			rel.product_id,
			pm.product_code,
			pm.product_name
	FROM   mh_lpa_line_product_relationship  rel,
			mh_lpa_region_master rm,
			mh_lpa_country_master cm,
			mh_lpa_location_master lm,
			mh_lpa_product_master pm,
			mh_lpa_line_master lne
	WHERE  rel.region_id = rm.region_id
	AND    rel.country_id = cm.country_id
	AND    rel.location_id = lm.location_id
	AND    rel.product_id = pm.product_id
	AND    rel.line_id = lne.line_id
	AND		(
			(ISNULL(@l_local_admin,'N') = 'Y' AND rel.location_id = @l_location_id) OR
			(ISNULL(@l_global_admin,'N') = 'Y')
			)
*/
	SELECT  rm.region_id,
			rm.region_name,
			cm.country_id,
			cm.country_name,
			lm.location_id,
			lm.location_name,
			lne.line_id,
			lne.line_name,
			lne.line_code
	FROM   mh_lpa_region_master rm,
			mh_lpa_country_master cm,
			mh_lpa_location_master lm,
			mh_lpa_line_master lne
	WHERE  lne.region_id = rm.region_id
	AND    lne.country_id = cm.country_id
	AND    lne.location_id = lm.location_id
	AND		(
			(ISNULL(@l_local_admin,'N') = 'Y' AND lm.location_id = @l_location_id) OR
			(ISNULL(@l_global_admin,'N') = 'Y')
			)
	AND   lne.line_id IN (select rel.line_id from mh_lpa_line_product_relationship rel where rel.line_id =  lne.line_id
						  and ISNULL(end_date, DateAdd(day, 1, getdate())) > getdate() 
						  ) 
	and ISNULL(end_date , dateadd(day,1,getdate())) > getdate()	  
	ORDER BY line_name;

END;



--SELECT  rm.region_id,
--			rm.region_name,
--			cm.country_id,
--			cm.country_name,
--			lm.location_id,
--			lm.location_name,
--			lne.line_id,
--			lne.line_name,
--			lne.line_code,
--			dbo.getPartsForLine(lne.line_id) product_code
--	FROM   mh_lpa_region_master rm,
--			mh_lpa_country_master cm,
--			mh_lpa_location_master lm,
--			mh_lpa_line_master lne
--	WHERE  lne.region_id = rm.region_id
--	AND    lne.country_id = cm.country_id
--	AND    lne.location_id = lm.location_id
--	AND lm.location_id = 1
--	--AND		(
--	--		(ISNULL(@l_local_admin,'N') = 'Y' AND lm.location_id = 1) OR
--	--		(ISNULL(@l_global_admin,'N') = 'Y')
--	--		)
--	AND   lne.line_id IN (select rel.line_id from mh_lpa_line_product_relationship rel where rel.line_id =  lne.line_id
--						  and ISNULL(end_date, DateAdd(day, 1, getdate())) > getdate() 
--						  ) 
						  
--	ORDER BY line_name;


	--select * from mh_lpa_line_master where location_id=1

	
	
	
	
	



GO
/****** Object:  StoredProcedure [dbo].[getListForEmailReminder]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getListForEmailReminder]
AS
	SET NOCOUNT ON;
	
BEGIN

	SELECT  
			um.email_id,
			loc.location_name Subject,
			FORMAT(p.planned_date,'DD-MM-YYYY')+ FORMAT(p.planned_date_end,'DD-MM-YYYY') body,
			um.user_id,
			um.emp_full_name,
			loc.location_name,
			lm.line_name,
			p.planned_date, p.planned_date_end
	FROM  mh_lpa_audit_plan p,
		mh_lpa_line_master lm,
		mh_lpa_location_master loc,
		mh_lpa_user_master um
	WHERE  p.planned_date BETWEEN DATEADD(day,2,getdate()) AND DATEADD(day,10,getdate())
	AND    p.to_be_audited_by_user_id = um.user_id
	AND    p.line_id = lm.line_id
	AND    p.location_id = loc.location_id;

END;



GO
/****** Object:  StoredProcedure [dbo].[getLocationId]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getLocationId](@p_location_code		varchar(10),
							@p_location_name		varchar(100),
							@l_location_id			INT		output	)

AS  
BEGIN
	DECLARE @l_count 			INT;
	
	SELECT @l_count = count(*)  
	FROM   mh_lpa_location_master
	WHERE  UPPER(ISNULL(location_code,'*')) = UPPER(ISNULL(@p_location_code,'*'))
	AND    UPPER(location_name) = UPPER(@p_location_name);
	
	IF @l_count = 0 
		-- exec Inslocation NULL, @p_location_code, @p_location_name;
		Set @l_location_id = 0;
	ELSE
		SELECT @l_location_id	= location_id 
		FROM   mh_lpa_location_master
		WHERE  UPPER(ISNULL(location_code,'*')) = UPPER(ISNULL(@p_location_code,'*'))
		AND    UPPER(location_name) = UPPER(@p_location_name);
		
END




GO
/****** Object:  StoredProcedure [dbo].[GetLocationListforWeb]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetLocationListforWeb](@p_user_id INT,
									@p_country_id int)
AS
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_location_id 			INT;
	DECLARE @l_global_admin			VARCHAR(1);
	DECLARE @l_local_admin			VARCHAR(1);


	SELECT @l_global_admin = ISNULL(global_admin_flag,'N'),
		@l_local_admin = ISNULL(site_admin_flag,'N'),
		@l_location_id = location_id
	FROM   mh_lpa_user_master
	WHERE  user_id = @p_user_id;

	SELECT DISTINCT loc.location_id,
		location_code,
		location_name,
		[dbo].[Getshift](loc.location_id) as Shift_no
		/*
		CASE 
			WHEN CAST('1900-01-01 ' + FORMAT(getdate(),'HH:mm:ss')+' '+loc.timezone_code as datetimeoffset) 
					between  CAST('1900-01-01 ' + FORMAT(CAST(loc.shift1_start_time as datetime),'HH:mm')+':00 '+loc.timezone_code as datetimeoffset )
							and  CAST('1900-01-01 ' + FORMAT(CAST(loc.shift1_end_time as datetime),'HH:mm')+':00 '+loc.timezone_code as datetimeoffset ) THEN 1
			WHEN CAST('1900-01-01 ' + FORMAT(getdate(),'HH:mm:ss')+' '+loc.timezone_code as datetimeoffset) 
					between  CAST('1900-01-01 ' + FORMAT(CAST(loc.shift2_start_time as datetime),'HH:mm')+':00 '+loc.timezone_code as datetimeoffset )
							and  CAST('1900-01-01 ' + FORMAT(CAST(loc.shift2_end_time as datetime),'HH:mm')+':00 '+loc.timezone_code as datetimeoffset ) THEN 2
			WHEN CAST('1900-01-01 ' + FORMAT(getdate(),'HH:mm:ss')+' '+loc.timezone_code as datetimeoffset) 
					between  CAST('1900-01-01 ' + FORMAT(CAST(loc.shift3_start_time as datetime),'HH:mm')+':00 '+loc.timezone_code as datetimeoffset )
							and  CAST('1900-01-01 ' + FORMAT(CAST(loc.shift3_end_time as datetime),'HH:mm')+':00 '+loc.timezone_code as datetimeoffset ) THEN 3
			ELSE 1
		END as Shift_no
		*/
	FROM   mh_lpa_location_master loc
/*		   mh_lpa_line_master lm
	WHERE loc.location_id = lm.location_id
	AND   lm.country_id = @p_country_id;
	*/
	WHERE loc.country_id = @p_country_id
	AND  ( @l_global_admin = 'Y' OR loc.location_id = @l_location_id)
	ORDER BY location_name ;

END;




GO
/****** Object:  StoredProcedure [dbo].[getLocListforEdit]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getLocListforEdit](@p_user_id		INT)
AS
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_location_id 			INT;
	DECLARE @l_global_admin			VARCHAR(1);
	DECLARE @l_local_admin			VARCHAR(1);


	SELECT @l_global_admin = ISNULL(global_admin_flag,'N'),
		@l_local_admin = ISNULL(site_admin_flag,'N'),
		@l_location_id = location_id
	FROM   mh_lpa_user_master
	WHERE  user_id = @p_user_id;
	
	SELECT  rm.region_id,
			rm.region_name,
			cm.country_id,
			cm.country_name,
			lm.location_id,
			lm.location_name,
			no_of_shifts,
			shift1_start_time,
			shift1_end_time,
			shift2_start_time,
			shift2_end_time,
			shift3_start_time,
			shift3_end_time,
			timezone_desc
	FROM   mh_lpa_region_master rm,
			mh_lpa_country_master cm,
			mh_lpa_location_master lm
	WHERE  lm.region_id = rm.region_id
	AND    lm.country_id = cm.country_id
	AND		(
			(ISNULL(@l_local_admin,'N') = 'Y' AND lm.location_id = @l_location_id) OR
			(ISNULL(@l_global_admin,'N') = 'Y')
			)
	ORDER BY location_name;


END;



GO
/****** Object:  StoredProcedure [dbo].[GetMaxAuditId]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[GetMaxAuditId]
(
@p_audit_id INT OUTPUT
)
AS
BEGIN 
	Select @p_audit_id=NEXT VALUE FOR dbo.audit_id_s
	--MAX(audit_id)+1 from mh_lpa_local_answers
END



GO
/****** Object:  StoredProcedure [dbo].[getQuestionListforEdit]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getQuestionListforEdit](@p_user_id		INT)
AS
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_location_id 			INT;
	DECLARE @l_global_admin			VARCHAR(1);
	DECLARE @l_local_admin			VARCHAR(1);


	SELECT @l_global_admin = ISNULL(global_admin_flag,'N'),
		@l_local_admin = ISNULL(site_admin_flag,'N'),
		@l_location_id = location_id
	FROM   mh_lpa_user_master
	WHERE  user_id = @p_user_id;

	SELECT section_id,
		section_name,
		section_display_sequence,
		question_id,
		question,
		na_flag,
		help_text,
		how_to_check,
		question_display_sequence,
		edit_allowed_flag
	FROM (
		SELECT  sm.section_id,
			sm.section_name,
			sm.display_sequence  section_display_sequence,
			qm.question_id,
			qm.question,
			qm.na_flag,
			qm.help_text,
			qm.how_to_check,
			lq.display_sequence  question_display_sequence,
			'N'		edit_allowed_flag
		FROM    mh_lpa_question_master qm,
			mh_lpa_local_questions lq,
			mh_lpa_section_master sm
		WHERE  lq.location_id = @l_location_id
		AND    lq.question_id < 10000
		AND    lq.question_id = qm.question_id
		AND    lq.section_id = sm.section_id
		AND    @l_local_admin = 'Y'
		UNION
		SELECT  sm.section_id,
			sm.section_name,
			sm.display_sequence  section_display_sequence,
			qm.local_question_id question_id,
			qm.local_question    question,
			qm.na_flag,
			qm.local_help_text   help_text,
			qm.how_to_check,
			lq.display_sequence  question_display_sequence,
			'Y'		edit_allowed_flag
		FROM    mh_lpa_local_question_master qm,
			mh_lpa_local_questions lq,
			mh_lpa_section_master sm
		WHERE  lq.location_id = @l_location_id
		AND    lq.question_id = qm.local_question_id
		AND    lq.section_id = sm.section_id
		AND    @l_local_admin = 'Y'
		AND    lq.location_id = @l_location_id
		UNION
		SELECT  sm.section_id,
			sm.section_name,
			sm.display_sequence  section_display_sequence,
			qm.question_id,
			qm.question,
			qm.na_flag,
			qm.help_text,
			qm.how_to_check,
			qm.display_sequence  question_display_sequence,
			'Y'		edit_allowed_flag
		FROM    mh_lpa_question_master qm,
			mh_lpa_section_master sm
		WHERE  qm.section_id = sm.section_id
		AND    @l_local_admin = 'N' 
		AND    @l_global_admin = 'Y'
		) tmp
	ORDER BY section_display_sequence, question_display_sequence;
 

END;





GO
/****** Object:  StoredProcedure [dbo].[getRegionId]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[getRegionId](@p_region_code		varchar(10),
							@p_region_name		varchar(100),
							@l_region_id		int output)
 
AS  
BEGIN
	DECLARE @l_count 			INT;

	SELECT @l_count = count(*)  
	FROM   mh_lpa_region_master
	WHERE  UPPER(ISNULL(region_code,'*')) = UPPER(ISNULL(@p_region_code,'*'))
	AND    UPPER(region_name) = UPPER(@p_region_name);
	
	IF @l_count = 0 
		exec InsRegion  NULL, @p_region_code, @p_region_name;

	
	SELECT @l_region_id	= region_id 
	FROM   mh_lpa_region_master
	WHERE  UPPER(ISNULL(region_code,'*')) = UPPER(ISNULL(@p_region_code,'*'))
	AND    UPPER(region_name) = UPPER(@p_region_name);

END






GO
/****** Object:  StoredProcedure [dbo].[GetReviewHistoryForWeb]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetReviewHistoryForWeb](
									@p_line_id int,
									@p_question_id			int)  
AS
BEGIN
	SELECT  la.review_closed_on,
				CASE WHEN la.review_closed_status=0 THEN 'Close'
				ELSE 'Open' End review_closed_status,
				ISNULL(la.review_comments,'Comments Not Entered') review_comments
	FROM    mh_lpa_local_answers la
	WHERE  1=1 -- la.audit_date BETWEEN DATEADD(month, -1, CURRENT_TIMESTAMP)  AND CURRENT_TIMESTAMP
	AND    la.line_id = @p_line_id
	AND    la.question_id = ISNULL(@p_question_id,la.question_id)
	AND    la.review_comments IS NOT NULL
	ORDER BY la.review_closed_on DESC
END





GO
/****** Object:  StoredProcedure [dbo].[GetRoleList]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetRoleList]
AS  

BEGIN  

 SELECT role_id,

    rolename,

no_of_audits_required,

frequency

    FROM  mh_lpa_role_master

END






GO
/****** Object:  StoredProcedure [dbo].[getSectionId]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getSectionId](
							@p_section_name		varchar(100),
							@l_section_id			INT		output	)

AS  
BEGIN
	SELECT @l_section_id	= section_id 
	FROM   mh_lpa_section_master
	WHERE  UPPER(section_name) = UPPER(@p_section_name);
		
END






GO
/****** Object:  StoredProcedure [dbo].[getUserListforEdit]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[getUserListforEdit](@p_user_id		INT)
AS
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_location_id 			INT;
	DECLARE @l_global_admin			VARCHAR(1);
	DECLARE @l_local_admin			VARCHAR(1);


	SELECT @l_global_admin = ISNULL(global_admin_flag,'N'),
		@l_local_admin = ISNULL(site_admin_flag,'N'),
		@l_location_id = location_id
	FROM   mh_lpa_user_master
	WHERE  user_id = @p_user_id;

	SELECT user_id,
			username,
			emp_full_name,
			emp_first_name,
			emp_last_name,
			display_name_flag,
			email_id,
			um.location_id,
			lm.location_name,
			role,
			rm.role_id,
			global_admin_flag,
			site_admin_flag,
			um.start_date,
			um.end_date,
			um.passwd,
			(select case  when global_admin_flag='Y' then 'Global'
			 when site_admin_flag='Y' then 'Local' end ) as 'admin_flag'
	FROM  mh_lpa_user_master um
	join [dbo].[mh_lpa_location_master] lm on um.location_id=lm.location_id
	join [dbo].[mh_lpa_role_master] rm on rm.role_id=um.role_id
	WHERE  (
			(@l_local_admin = 'Y' AND um.location_id = @l_location_id) OR
			(@l_global_admin = 'Y')
			)
			AND (um.end_date is null OR um.end_date>=GETDATE())
			order by emp_last_name, emp_first_name

END;





GO
/****** Object:  StoredProcedure [dbo].[InsAuditPlan]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsAuditPlan](@p_audit_plan_id   int,
        @p_user_id    int, /* Auditor's User Id */ 
        @p_planned_date   date,
        @p_planned_date_end  date,
        @p_line_id    int,
        @l_invite_to_list  varchar(max) OUTPUT,
        @l_err_code    int OUTPUT,
        @l_err_message   varchar(100) OUTPUT
        )
AS
 SET NOCOUNT ON;
 
BEGIN
/* This procedure needs date overlap validation still */
 DECLARE @l_send_note_to_owner VARCHAR(10);
 DECLARE @l_location_id   INT;
 DECLARE @l_count    INT;
 DECLARE @l_count1    INT;
 
 SET @l_err_code = 0;
 SET @l_err_message = 'Initializing';
 SELECT @l_location_id = location_id
 FROM   mh_lpa_line_master
 WHERE  line_id = @p_line_id;
 SELECT @l_invite_to_list = email_id
 FROM   mh_lpa_user_master
 WHERE  user_id = @p_user_id;
 
 IF @p_audit_plan_id IS NOT NULL AND @p_audit_plan_id != 0 
 
  BEGIN
 
   UPDATE mh_lpa_audit_plan
   SET    to_be_audited_by_user_id = @p_user_id,
     planned_date = @p_planned_date,
     planned_date_end = @p_planned_date_end,
     line_id = @p_line_id,
     location_id = @l_location_id
   WHERE audit_plan_id = @p_audit_plan_id;
   
   SET @l_err_code = 0;
   SET @l_err_message = 'Updated Successfully';
  END;  
 ELSE
  BEGIN
   SELECT @l_count = count(*)
   FROM   mh_lpa_audit_plan
   WHERE  FORMAT(ISNULL(planned_date,getdate()),'dd-MM-yyyy') = FORMAT(@p_planned_date,'dd-MM-yyyy')
   AND    FORMAT(ISNULL(planned_date_end,getdate()),'dd-MM-yyyy') = FORMAT(@p_planned_date_end,'dd-MM-yyyy')
   AND    line_id = @p_line_id;
   IF @l_count > 0
   BEGIN
    SELECT @l_count1 = count(*)
    FROM   mh_lpa_audit_plan
    WHERE  to_be_audited_by_user_id = @p_user_id
    AND    FORMAT(ISNULL(planned_date,getdate()),'dd-MM-yyyy') = FORMAT(@p_planned_date,'dd-MM-yyyy')
    AND    FORMAT(ISNULL(planned_date_end,getdate()),'dd-MM-yyyy') = FORMAT(@p_planned_date_end,'dd-MM-yyyy')
    AND    line_id = @p_line_id
    AND    location_id = @l_location_id;
   
    IF @l_count1 = 0 
     UPDATE mh_lpa_audit_plan
     SET    to_be_audited_by_user_id = @p_user_id
     WHERE  FORMAT(ISNULL(planned_date,getdate()),'dd-MM-yyyy') = FORMAT(@p_planned_date,'dd-MM-yyyy')
     AND    FORMAT(ISNULL(planned_date_end,getdate()),'dd-MM-yyyy') = FORMAT(@p_planned_date_end,'dd-MM-yyyy')
     AND    line_id = @p_line_id
     AND    location_id = @l_location_id;
    ELSE
     SET @l_invite_to_list = NULL;
   END;
   ELSE
    INSERT INTO mh_lpa_audit_plan
    (
     to_be_audited_by_user_id,
     planned_date,
     planned_date_end,
     line_id,
     location_id
    ) VALUES (
     @p_user_id,
     @p_planned_date,
     @p_planned_date_end,
     @p_line_id,
     @l_location_id
    );
 
   
   SET @l_err_code = 0;
   SET @l_err_message = 'Inserted Successfully';
  END;
END



GO
/****** Object:  StoredProcedure [dbo].[insAuditPlanInterface]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE  PROCEDURE [dbo].[insAuditPlanInterface](@p_audit_plan_interface_table AuditPlanInterfaceTableType READONLY)
AS
BEGIN
-- DECLARE @l_audit_invite  AuditPlanInviteTableType;
 DECLARE @l_year  INT;
 DECLARE @l_emp_id INT;
 DECLARE @l_line_id INT;
 DECLARE @l_location_id   INT;
 DECLARE @l_line_name   VARCHAR(100);
 DECLARE @l_invite_to_list  varchar(max);
 DECLARE @l_del_invite_to_list varchar(max);
 DECLARE @l_err_code    int;
 DECLARE @l_err_message   varchar(100);
 DECLARE @l_yr     VARCHAR(4);
 DECLARE @l_start_date   DATE;
 DECLARE @l_end_date    DATE;
 DECLARE @l_week     INT;
  DECLARE @l_timezone_code varchar(100);
  DECLARE @l_time_offset	FLOAT;
  DECLARE @l_location_name	VARCHAR(100);

 DECLARE @l_wk1 INT, @l_wk2 INT, @l_wk3 INT, @l_wk4 INT, @l_wk5 INT, 
         @l_wk6 INT, @l_wk7 INT, @l_wk8 INT, @l_wk9 INT, @l_wk10 INT,
   @l_wk11 INT, @l_wk12 INT, @l_wk13 INT, @l_wk14 INT, @l_wk15 INT,
   @l_wk16 INT, @l_wk17 INT, @l_wk18 INT, @l_wk19 INT, @l_wk20 INT,
   @l_wk21 INT, @l_wk22 INT, @l_wk23 INT, @l_wk24 INT, @l_wk25 INT,
   @l_wk26 INT, @l_wk27 INT, @l_wk28 INT, @l_wk29 INT, @l_wk30 INT,
   @l_wk31 INT, @l_wk32 INT, @l_wk33 INT, @l_wk34 INT, @l_wk35 INT,
   @l_wk36 INT, @l_wk37 INT, @l_wk38 INT, @l_wk39 INT, @l_wk40 INT,
   @l_wk41 INT, @l_wk42 INT, @l_wk43 INT, @l_wk44 INT, @l_wk45 INT,
   @l_wk46 INT, @l_wk47 INT, @l_wk48 INT, @l_wk49 INT, @l_wk50 INT,
   @l_wk51 INT, @l_wk52 INT;
   
   DECLARE @l_orig_wk1 INT, @l_orig_wk2 INT, @l_orig_wk3 INT, @l_orig_wk4 INT, @l_orig_wk5 INT, 
         @l_orig_wk6 INT, @l_orig_wk7 INT, @l_orig_wk8 INT, @l_orig_wk9 INT, @l_orig_wk10 INT,
   @l_orig_wk11 INT, @l_orig_wk12 INT, @l_orig_wk13 INT, @l_orig_wk14 INT, @l_orig_wk15 INT,
   @l_orig_wk16 INT, @l_orig_wk17 INT, @l_orig_wk18 INT, @l_orig_wk19 INT, @l_orig_wk20 INT,
   @l_orig_wk21 INT, @l_orig_wk22 INT, @l_orig_wk23 INT, @l_orig_wk24 INT, @l_orig_wk25 INT,
   @l_orig_wk26 INT, @l_orig_wk27 INT, @l_orig_wk28 INT, @l_orig_wk29 INT, @l_orig_wk30 INT,
   @l_orig_wk31 INT, @l_orig_wk32 INT, @l_orig_wk33 INT, @l_orig_wk34 INT, @l_orig_wk35 INT,
   @l_orig_wk36 INT, @l_orig_wk37 INT, @l_orig_wk38 INT, @l_orig_wk39 INT, @l_orig_wk40 INT,
   @l_orig_wk41 INT, @l_orig_wk42 INT, @l_orig_wk43 INT, @l_orig_wk44 INT, @l_orig_wk45 INT,
   @l_orig_wk46 INT, @l_orig_wk47 INT, @l_orig_wk48 INT, @l_orig_wk49 INT, @l_orig_wk50 INT,
   @l_orig_wk51 INT, @l_orig_wk52 INT;

 DECLARE l_audit_plan_cur CURSOR FOR SELECT * FROM @p_audit_plan_interface_table;
 
 SELECT TOP 1 @l_year = year,
  @l_location_id = location_id,
  @l_line_id = line_id,
  @l_line_name = line_name
 FROM  @p_audit_plan_interface_table;
 
 
 DELETE FROM mh_lpa_audit_plan_interface
 WHERE  year = @l_year
 AND    location_id = @l_location_id;
 
 INSERT INTO mh_lpa_audit_plan_interface
 SELECT *
 FROM   @p_audit_plan_interface_table;
 
 
 DELETE FROM  mh_lpa_AuditPlan_Del_Invite
 WHERE  year = @l_year
 AND    location_id = @l_location_id;
 
 DELETE FROM  mh_lpa_AuditPlanInvite
 WHERE  year = @l_year
 AND    location_id = @l_location_id;
  
 
 SET @l_yr = CAST(@l_year AS VARCHAR);
 
 /*
  * Now for each cell from the above table, create or update the plan in the mh_lpa_audit_plan table
  */
  
 OPEN l_audit_plan_cur
 FETCH NEXT FROM l_audit_plan_cur INTO @l_location_id,@l_line_id,@l_line_name,@l_year,
           @l_wk1, @l_wk2, @l_wk3, @l_wk4, @l_wk5, 
           @l_wk6, @l_wk7, @l_wk8, @l_wk9, @l_wk10,
           @l_wk11, @l_wk12, @l_wk13, @l_wk14, @l_wk15,
           @l_wk16, @l_wk17, @l_wk18, @l_wk19, @l_wk20,
           @l_wk21, @l_wk22, @l_wk23, @l_wk24, @l_wk25,
           @l_wk26, @l_wk27, @l_wk28, @l_wk29, @l_wk30,
           @l_wk31, @l_wk32, @l_wk33, @l_wk34, @l_wk35,
           @l_wk36, @l_wk37, @l_wk38, @l_wk39, @l_wk40,
           @l_wk41, @l_wk42, @l_wk43, @l_wk44, @l_wk45,
           @l_wk46, @l_wk47, @l_wk48, @l_wk49, @l_wk50,
           @l_wk51, @l_wk52;
 WHILE @@FETCH_STATUS = 0 
 BEGIN
 
	SET @l_orig_wk1 = 0; SET @l_orig_wk2 = 0; SET @l_orig_wk3 = 0; SET @l_orig_wk4 = 0; SET @l_orig_wk5 = 0; 
	SET @l_orig_wk6 = 0; SET @l_orig_wk7 = 0; SET @l_orig_wk8 = 0; SET @l_orig_wk9 = 0; SET @l_orig_wk10 = 0; 
	
	SET @l_orig_wk11 = 0; SET @l_orig_wk12 = 0; SET @l_orig_wk13 = 0; SET @l_orig_wk14 = 0; SET @l_orig_wk15 = 0; 
	SET @l_orig_wk16 = 0; SET @l_orig_wk17 = 0; SET @l_orig_wk18 = 0; SET @l_orig_wk19 = 0; SET @l_orig_wk20 = 0; 
	
	SET @l_orig_wk21 = 0; SET @l_orig_wk22 = 0; SET @l_orig_wk23 = 0; SET @l_orig_wk24 = 0; SET @l_orig_wk25 = 0; 
	SET @l_orig_wk26 = 0; SET @l_orig_wk27 = 0; SET @l_orig_wk28 = 0; SET @l_orig_wk29 = 0; SET @l_orig_wk30 = 0; 

	SET @l_orig_wk31 = 0; SET @l_orig_wk32 = 0; SET @l_orig_wk33 = 0; SET @l_orig_wk34 = 0; SET @l_orig_wk35 = 0; 
	SET @l_orig_wk36 = 0; SET @l_orig_wk37 = 0; SET @l_orig_wk38 = 0; SET @l_orig_wk39 = 0; SET @l_orig_wk40 = 0; 

	SET @l_orig_wk41 = 0; SET @l_orig_wk42 = 0; SET @l_orig_wk43 = 0; SET @l_orig_wk44 = 0; SET @l_orig_wk45 = 0; 
	SET @l_orig_wk46 = 0; SET @l_orig_wk47 = 0; SET @l_orig_wk48 = 0; SET @l_orig_wk49 = 0; SET @l_orig_wk50 = 0; 

	SET @l_orig_wk51 = 0; SET @l_orig_wk52 = 0; 

	/*  SET @l_week = 1;
  SET @l_start_date = DATEADD(day, 1, DATEADD(wk, DATEDIFF(wk, 6, '1/1/' + @l_yr) + (@l_week-1), 6));
  SET @l_end_date = DATEADD(day, -1, DATEADD(wk, DATEDIFF(wk, 5, '1/1/' + @l_yr) + (@l_week-1), 5));
*/
  
  SELECT @l_timezone_code = timezone_code,
		@l_location_name = location_name
  FROM   mh_lpa_location_master
  WHERE  location_id = @l_location_id;
  
  

  EXEC dbo.get_st_end_date @l_yr, 1,@l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk1 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
   
  IF @l_wk1 = 0 OR @l_wk1 != @l_orig_wk1
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk1 IS NOT NULL AND @l_wk1 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk1, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 2,  @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;   
	SELECT @l_orig_wk2 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk2 = 0 OR @l_wk2 != @l_orig_wk2
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk2 IS NOT NULL AND @l_wk2 != 0
  BEGIN
   EXEC dbo.InsAuditPlan NULL, @l_wk2, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 3,  @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk3 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk3 = 0 OR @l_wk3 != @l_orig_wk3
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk3 IS NOT NULL AND @l_wk3 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk3, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 4,  @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk4 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk4 = 0 OR @l_wk4 != @l_orig_wk4
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk4 IS NOT NULL AND @l_wk4 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk4, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 5,  @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk5 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk5 = 0 OR @l_wk5 != @l_orig_wk5
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk5 IS NOT NULL AND @l_wk5 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk5, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 6,  @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk6 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk6 = 0 OR @l_wk6 != @l_orig_wk6
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk6 IS NOT NULL AND @l_wk6 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk6, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 7,  @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk7 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk7 = 0 OR @l_wk7 != @l_orig_wk7
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk7 IS NOT NULL AND @l_wk7 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk7, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 8,  @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk8 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk8 = 0 OR @l_wk8 != @l_orig_wk8
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk8 IS NOT NULL AND @l_wk8 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk8, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 9,  @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk9 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk9 = 0 OR @l_wk9 != @l_orig_wk9
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk9 IS NOT NULL AND @l_wk9 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk9, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 10, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk10 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk10 = 0 OR @l_wk10 != @l_orig_wk10
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk10 IS NOT NULL AND @l_wk10 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk10, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 11, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk11 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk11 = 0 OR @l_wk11 != @l_orig_wk11
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk11 IS NOT NULL AND @l_wk11 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk11, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 12, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk12 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk12 = 0 OR @l_wk12 != @l_orig_wk12
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk12 IS NOT NULL AND @l_wk12 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk12, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 13, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk13 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk13 = 0 OR @l_wk13 != @l_orig_wk13
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk13 IS NOT NULL AND @l_wk13 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk13, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 14, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk14 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk14 = 0 OR @l_wk14 != @l_orig_wk14
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk14 IS NOT NULL AND @l_wk14 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk14, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 15, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk15 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk15 = 0 OR @l_wk15 != @l_orig_wk15
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk15 IS NOT NULL AND @l_wk15 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk15, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 16, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk16 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
   
  IF @l_wk16 = 0 OR @l_wk16 != @l_orig_wk16
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk16 IS NOT NULL AND @l_wk16 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk16, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 17, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk17 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
   
  IF @l_wk17 = 0 OR @l_wk17 != @l_orig_wk17
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk17 IS NOT NULL AND @l_wk17 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk17, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 18, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk18 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
   
  IF @l_wk18 = 0 OR @l_wk18 != @l_orig_wk18
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk18 IS NOT NULL AND @l_wk18 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk18, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 19, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk19 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
   
  IF @l_wk19 = 0 OR @l_wk19 != @l_orig_wk19
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk19 IS NOT NULL AND @l_wk19 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk19, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  /* Week 20 to 29 */
  
  
  EXEC dbo.get_st_end_date @l_yr, 20, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk20 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk20 = 0 OR @l_wk20 != @l_orig_wk20
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk20 IS NOT NULL AND @l_wk20 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk20, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 21, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk21 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk21 = 0 OR @l_wk21 != @l_orig_wk21
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk21 IS NOT NULL AND @l_wk21 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk21, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 22, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk22 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk22 = 0 OR @l_wk22 != @l_orig_wk22
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk22 IS NOT NULL AND @l_wk22 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk22, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 23, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk23 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk23 = 0 OR @l_wk23 != @l_orig_wk23
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk23 IS NOT NULL AND @l_wk23 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk23, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 24, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk24 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk24 = 0 OR @l_wk24 != @l_orig_wk24
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk24 IS NOT NULL AND @l_wk24 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk24, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 25, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk25 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk25 = 0 OR @l_wk25 != @l_orig_wk25
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk25 IS NOT NULL AND @l_wk25 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk25, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 26, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk26 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk26 = 0 OR @l_wk26 != @l_orig_wk26
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk26 IS NOT NULL AND @l_wk26 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk26, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 27, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk27 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk27 = 0 OR @l_wk27 != @l_orig_wk27
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk27 IS NOT NULL AND @l_wk27 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk27, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 28, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk28 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk28 = 0 OR @l_wk28 != @l_orig_wk28
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk28 IS NOT NULL AND @l_wk28 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk28, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 29, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk29 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk29 = 0 OR @l_wk29 != @l_orig_wk29
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk29 IS NOT NULL AND @l_wk29 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk29, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  

  /* Week 30 to 39 */
  
  
  EXEC dbo.get_st_end_date @l_yr, 30, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk30 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk30 = 0 OR @l_wk30 != @l_orig_wk30
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk30 IS NOT NULL AND @l_wk30 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk30, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 31, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk31 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk31 = 0 OR @l_wk31 != @l_orig_wk31
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk31 IS NOT NULL AND @l_wk31 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk31, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 32, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk32 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk32 = 0 OR @l_wk32 != @l_orig_wk32
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk32 IS NOT NULL AND @l_wk32 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk32, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 33, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk33 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk33 = 0 OR @l_wk33 != @l_orig_wk33
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk33 IS NOT NULL AND @l_wk33 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk33, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 34, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk34 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk34 = 0 OR @l_wk34 != @l_orig_wk34
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;

  IF @l_wk34 IS NOT NULL AND @l_wk34 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk34, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 35, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk35 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk35 = 0 OR @l_wk35 != @l_orig_wk35
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk35 IS NOT NULL AND @l_wk35 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk35, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 36, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk36 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk36 = 0 OR @l_wk36 != @l_orig_wk36
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk36 IS NOT NULL AND @l_wk36 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk36, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 37, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk37 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk37 = 0 OR @l_wk37 != @l_orig_wk37
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk37 IS NOT NULL AND @l_wk37 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk37, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 38, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk38 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk38 = 0 OR @l_wk38 != @l_orig_wk38
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk38 IS NOT NULL AND @l_wk38 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk38, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 39, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk39 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk39 = 0 OR @l_wk39 != @l_orig_wk39
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk39 IS NOT NULL AND @l_wk39 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk39, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  

  
  /* Week 40 to 49 */
  
  
  EXEC dbo.get_st_end_date @l_yr, 40, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk40 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk40 = 0 OR @l_wk40 != @l_orig_wk40
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk40 IS NOT NULL AND @l_wk40 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk40, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;
 
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 41, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk41 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk41 = 0 OR @l_wk41 != @l_orig_wk41
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk41 IS NOT NULL AND @l_wk41 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk41, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 42, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk42 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk42 = 0 OR @l_wk42 != @l_orig_wk42
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk42 IS NOT NULL AND @l_wk42 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk42, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 43, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk43 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk43 = 0 OR @l_wk43 != @l_orig_wk43
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk43 IS NOT NULL AND @l_wk43 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk43, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 44, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk44 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk44 = 0 OR @l_wk44 != @l_orig_wk44
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk44 IS NOT NULL AND @l_wk44 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk44, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 45, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk45 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk45 = 0 OR @l_wk45 != @l_orig_wk45
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk45 IS NOT NULL AND @l_wk45 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk45, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 46, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk46 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk46 = 0 OR @l_wk46 != @l_orig_wk46
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk46 IS NOT NULL AND @l_wk46 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk46, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 47, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk47 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk47 = 0 OR @l_wk47 != @l_orig_wk47
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk47 IS NOT NULL AND @l_wk47 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk47, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 48, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk48 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk48 = 0 OR @l_wk48 != @l_orig_wk48
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk48 IS NOT NULL AND @l_wk48 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk48, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 49, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk49 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk49 = 0 OR @l_wk49 != @l_orig_wk49
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk49 IS NOT NULL AND @l_wk49 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk49, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  /* Week 50 to 52 */
  
  
  EXEC dbo.get_st_end_date @l_yr, 50, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk50 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk50 = 0 OR @l_wk50 != @l_orig_wk50
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk50 IS NOT NULL AND @l_wk50 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk50, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 51, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
    SELECT @l_orig_wk51 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk51 = 0 OR @l_wk51 != @l_orig_wk51
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk51 IS NOT NULL AND @l_wk51 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk51, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  EXEC dbo.get_st_end_date @l_yr, 52, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk52 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk52 = 0 OR @l_wk52 != @l_orig_wk52
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;

  IF @l_wk52 IS NOT NULL AND @l_wk52 != 0
  BEGIN
-- select * from (select @l_wk52 wk52, @l_start_date st_date, @l_end_date end_date, @l_line_id line_id ) tmp
   EXEC dbo.InsAuditPlan NULL, @l_wk52, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  
  END;

  
  FETCH NEXT FROM l_audit_plan_cur INTO @l_location_id,@l_line_id,@l_line_name,@l_year,
           @l_wk1, @l_wk2, @l_wk3, @l_wk4, @l_wk5, 
           @l_wk6, @l_wk7, @l_wk8, @l_wk9, @l_wk10,
           @l_wk11, @l_wk12, @l_wk13, @l_wk14, @l_wk15,
           @l_wk16, @l_wk17, @l_wk18, @l_wk19, @l_wk20,
           @l_wk21, @l_wk22, @l_wk23, @l_wk24, @l_wk25,
           @l_wk26, @l_wk27, @l_wk28, @l_wk29, @l_wk30,
           @l_wk31, @l_wk32, @l_wk33, @l_wk34, @l_wk35,
           @l_wk36, @l_wk37, @l_wk38, @l_wk39, @l_wk40,
           @l_wk41, @l_wk42, @l_wk43, @l_wk44, @l_wk45,
           @l_wk46, @l_wk47, @l_wk48, @l_wk49, @l_wk50,
           @l_wk51, @l_wk52;
 END;

 CLOSE l_audit_plan_cur;
 DEALLOCATE l_audit_plan_cur;
 --SELECT * from (select @l_err_code err_code, @l_err_message err_message) a;
 --select  location_id,year,email_id, Convert(VARCHAR(100),start_date) +' 08:00:00.000' start_date,
 --Convert(VARCHAR(100),start_date) +' 09:00:00.000' end_date,
 --line_name  from mh_lpa_AuditPlanInvite where location_id=@l_location_id and year=@l_year


	-- SET  @l_time_offset	= CAST(REPLACE(@l_timezone_code,':','.') as FLOAT);
	-- SET  @l_time_offset = @l_time_offset * -1.0;
 	SET  @l_timezone_code	= REPLACE(@l_timezone_code,':','.');
	SET  @l_time_offset	= CAST(REPLACE(@l_timezone_code,'.30','.50') as FLOAT);
	SET  @l_time_offset = @l_time_offset * -60.0;
	-- SET  @l_time_offset = @l_time_offset * 60.0;

  select  i.location_id,
		@l_location_name location_name,
		year,
		email_id,
		Convert(VARCHAR(100),start_date) start_date,
		Convert(VARCHAR(100),end_date) end_date,
		FORMAT(DATEADD(minute,@l_time_offset, CAST(FORMAT(start_date,'yyyy-MM-dd'+' 08:00:00') as datetime) ),'yyyyMMddTHHmmssZ') start_date_formatted,
		FORMAT(DATEADD(minute,@l_time_offset, CAST(FORMAT(start_date,'yyyy-MM-dd'+' 09:00:00') as datetime) ),'yyyyMMddTHHmmssZ') end_date_formatted,
--		TODATETIMEOFFSET(CAST(start_date as datetime), @l_timezone_code) end_date,
--		Convert(VARCHAR(100),start_date) +' 08:00:00.000' start_date,
--		Convert(VARCHAR(100),start_date) +' 09:00:00.000' end_date,
 line_name  
 from mh_lpa_AuditPlanInvite i
 where i.location_id=@l_location_id and year=@l_year



 SELECT location_id, @l_location_name location_name, year,email_id, 
 		Convert(VARCHAR(100),start_date) start_date,
		Convert(VARCHAR(100),end_date) end_date,
		FORMAT(DATEADD(minute,@l_time_offset, CAST(FORMAT(start_date,'yyyy-MM-dd'+' 08:00:00') as datetime) ),'yyyyMMddTHHmmssZ') start_date_formatted,
		FORMAT(DATEADD(minute,@l_time_offset, CAST(FORMAT(start_date,'yyyy-MM-dd'+' 09:00:00') as datetime) ),'yyyyMMddTHHmmssZ') end_date_formatted,
line_name 
 from mh_lpa_AuditPlan_Del_Invite where location_id=@l_location_id and year=@l_year





END;





GO
/****** Object:  StoredProcedure [dbo].[InsAuditResults_date]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  Table [dbo].[mh_lpa_language_master]    Script Date: 4/9/2018 4:45:42 PM ******
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_language_master](
	[code] [varchar](100) NULL,
	[english] [nvarchar](1000) NULL,
	[german] [nvarchar](1000) NULL,
	[french] [nvarchar](1000) NULL,
	[spanish] [nvarchar](1000) NULL,
	[czech] [nvarchar](1000) NULL,
	[chinese] [nvarchar](1000) NULL,
	[korean] [nvarchar](1000) NULL,
	[thai] [nvarchar](1000) NULL,
	[bosnian] [nvarchar](1000) NULL,
	[portuguese] [nvarchar](1000) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
****** Object:  Table [dbo].[mh_lpa_Question_language_master]    Script Date: 4/9/2018 4:45:43 PM ******
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mh_lpa_Question_language_master](
	[Question_id] [int] NULL,
	[English] [nvarchar](1000) NULL,
	[German] [nvarchar](1000) NULL,
	[French] [nvarchar](1000) NULL,
	[Spanish] [nvarchar](1000) NULL,
	[czech] [nvarchar](1000) NULL,
	[Chinese] [nvarchar](1000) NULL,
	[Korean] [nvarchar](1000) NULL,
	[Thai] [nvarchar](1000) NULL,
	[Bosnian] [nvarchar](1000) NULL,
	[Portuguese] [nvarchar](1000) NULL
) ON [PRIMARY]

GO
****** Object:  Table [dbo].[mh_lpa_Section_language_master]    Script Date: 4/9/2018 4:45:43 PM ******
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mh_lpa_Section_language_master](
	[section_id] [int] NULL,
	[English] [nvarchar](1000) NULL,
	[German] [nvarchar](1000) NULL,
	[French] [nvarchar](1000) NULL,
	[Spanish] [nvarchar](1000) NULL,
	[czech] [nvarchar](1000) NULL,
	[Chinese] [nvarchar](1000) NULL,
	[Korean] [nvarchar](1000) NULL,
	[Thai] [nvarchar](1000) NULL,
	[Bosnian] [nvarchar](1000) NULL,
	[Portuguese] [nvarchar](1000) NULL
) ON [PRIMARY]

GO
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'Title', N'Layered Process Audit (LPA)', N'Layered Process Audit (LPA)', N'AUDIT DE POSTE DE TRAVAIL', N'Auditoría Escalonada de Proceso Layered Process Audit (LPA)', N'Layered Process Audit (LPA)', N'Layered Process Audit (LPA) 分层审核', N'Layered Process Audit (LPA)', N'Layered Process Audit (LPA) ', N'Slojeviti audit procesa (LPA)', N'Auditoria de Processo Escalonada (LPA)')
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'Line', N'0.1) Plant / Line (name / no.)', N'0.1) Werk / Linie (Name / Nr.)', N'0.1) Usine / UP /  Ligne (nom / n°)', N'0.1) Planta / Línea (nombre / Nº)', N'0.1) Závod / Linka (název / č.)', N'0.1) Plant工厂 / Line产线 (name / no.)', N'0.1) Plant / Line (name / no.)', N'0.1) Plant / Line (name / no.)', N'0.1) Lokacija / Linija (Ime / Br.)', N'0.1) Planta / Linha (nome / nº.)')
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'Part', N'0.2) Current product / part-no.', N'0.2) Aktuelles Produkt / Teilenummer', N'0.2) produit actuel / référence pièces', N'0.2) Producto/ Nº de parte', N'0.2) Aktuální výrobek / č. dílu', N'0.2) Current product 产品/ part-no.零件号', N'0.2) Current product / part-no.', N'0.2) Current product / part-no.', N'0.2) Aktuelni proizvod / Šifra dijela', N'0.2) Produto atual / nº - peça')
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'Name', N'0.3) Name: ', N'0.3) Name: ', N'0.3) Nom: ', N'0.3) Nombre del Auditor:', N'0.3) Jméno: ', N'0.3) Name名字: ', N'0.3) Name: ', N'0.3) Name ', N'0.3) Ime: ', N'0.3) Nome: ')
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'Date', N'0.4) Date:(Day, Month)', N'0.4) Datum:(Tag, Monat)', N'0.4) Date:(jour, Mois)', N'0.4) Fecha:(Día, Mes)', N'0.4) Datum:(den, měsíc)', N'0.4) Date日期:(Day, Month)', N'0.4) Date:(Day, Month)', N'0.4) Date:(Day, Month)', N'0.4) Datum: (Dan, Mjesec)', N'0.4) Data:(Dia, Ano)')
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'Person', N'0.5 )      Personal No.:', N'0.5)   Personal Nr.:', N'0.5 )      Identifiant', N'0.5) Legajo Nº:', N'0.5 )   Osobní číslo:', N'0.5 )   Personal No. 个人序号:', N'0.5 ) Personal No.:', N'0.5 )      Personal No.:', N'0.5)   Personalni  br.:', N'0.5 )      Nº pessoal:')
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'Shift', N'0.6) Shift:', N'0.6) Schicht:', N'0.6) Equipe', N'0.6) Shift:', N'0.6) Směna:', N'0.6) Shift班次:', N'0.6) Shift:', N'0.6) Shift :', N'0.6) Smjena:', N'0.6) Turno:')
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'Item', N'Item #', N'Pos.', N'Item #', N'Item #', N'Položkač.', N'Item #序号', N'Item #', N'Item #', N'Poz.', N'Item #')
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'Checklist', N'Checklist', N'Checkliste', N'Liste de vérification', N'Checklist', N'Kontrolní seznam', N'Checklist检查清单', N'Checklist', N'Checklist หัวข้อการตรวจ', N'Checklista', N'Lista de verificação')
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'Evaluation', N'Evaluation', N'Bewertung', N'Evaluation', N'Evaluación', N'Vyhodnocení', N'Evaluation评价', N'Evaluation', N'Evaluation ผล', N'Ocjena', N'Avaliação')
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'OK', N'OK', N'OK', N'OK', N'OK', N'OK', N'OK', N'OK', N'OK', N'OK', N'OK')
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'NOK', N'NOK', N'NOK', N'NOK', N'NOK', N'NOK', N'NOK', N'NOK', N'NOK', N'NOK', N'NOK')
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'NA', N'NA', N'NA', N'NA', N'NA', N'NA', N'NA', N'NA', N'NA', N'NA', N'NA')
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'Findings', N'Findings', N'Feststellungen', N'Ecarts / actions correctives', N'Evidencias', N'Zjištění', N'Findings审核发现', N'Findings', N'Findings รายละเอียดที่ตรวจพบ', N'Rezultati', N'Resultados')
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'Resp', N'Resp. ', N'Verantw.', N'Resp. ', N'Responsable', N'Zodpovědný', N'Resp. 负责人', N'Resp. ', N'Resp. ผู้รับผิดชอบ', N'Odgovoran', N'Resp. ')
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'Closed_date', N'Actual Date Closed', N'Datum abgeschl.', N'Date de cloture', N'Fecha cerrado', N'Skut. datum uzavření', N'Actual Date Closed实际关闭日期', N'Actual Date Closed', N'Actual Date Closed', N'Datum završetka', N'Data Atual Fechado')
INSERT [dbo].[mh_lpa_language_master] ([code], [english], [german], [french], [spanish], [czech], [chinese], [korean], [thai], [bosnian], [portuguese]) VALUES (N'Result', N'Result', N'Ergebnis', N'Résultat ', N'Resultado:', N'Výsledek: ', N'Result: ', N'Result: ', N'Result: ', N'Rezultat ', N'Resultado: ')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (1, N'Are all actions of previous layered audits closed or documented on line RPS?', N'Sind alle Maßnahmen aus vorangegangenen LPA abgeschlossen oder im Line RPS dokumentiert?', N'Les actions correctives des précédents audits sont-ils cloturées ou documentées dans le line RPS ?', N'¿Están todas las acciones correctivas de auditorías escalonadas previas cerradas ó registradas en el RPS de línea?', N'Jsou všechny činnosti předešlých LPA uzavřeny anebo zdokumentovány v linkových RPS?', N'Are all actions of previous layered audits closed or documented on line RPS? 上次分层审核的问题项是否关闭或者已经在现场RPS上文件化?', N'지난 LPA 심사의 미비한 사항에 대한 모든 개선 조치가 이루어 지고 있는가? RPS 보드판에 문서화 되어 있는가?', N'Are all actions of previous layered audits closed or documented on line RPS? ให้แน่ใจว่าปัญหาที่เคยตรวจเจอในการaudit ครั้งก่อนได้รับการแก้ไข รวมถึงได้มีการระบุ ติดตามใน RPS ?', N'Jesu li zatvorene sve mjere iz prethodnog LPA ili da li su dokumentovane na linijskom  RPS-u?', N' Todas as ações de auditorias escalonadas anteriores estão  fechadas ou  documentadas na RPS (Solução Rápida de Problemas) da linha?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (2, N'Is the qualification matrix and the validation sheet correctly filled and properly used?', N'Ist die Qualifikationsmatrix und die Trainingsdokumentation vorhanden und werden sie richtig angewendet? ', N'Est-ce que la matrice de qualification est correctement complétées. Les évaluations sont enregistrés dans l application ILUO.', N'¿Se utiliza la matriz de polivalencia en forma adecuada?', N'Jsou Kvalifikační matice a formulář Hodnocení dělníka na pracovišti správně vyplněny a správně používány?', N'Is the qualification matrix and the validation sheet correctly filled and properly used?  人员能力矩阵表是否被正确填写并正确使用？', N'Qualification matirx(자격자인증 메트릭스)와 Vaildation sheet(검증시트) 는 정해진 일정에 정확한 프로세스에 따라 평가되고 업데이트 되는가?', N'Is the qualification matrix and the validation sheet correctly filled and properly used?    ให้แน่ใจว่าตารางคุณสมบัติและการประเมินพนักงานถูกต้องและถูกใช้งานในไลน์？', N'Da li  matrica kvalifikacija i obrazac za validaciju postoje, jesu li ispravno popunjeni i da li se propisno koriste?', N'A matriz de qualificação e a folha de validação estão corretamente  preenchidas e usadas apropriadamente?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (9, N'Are floor markings used in the right way and not damaged? ', N'Sind die Bodenmarkierungen richtig angewendet und nicht beschädigt? ', N'Est-ce que les marquages au sol sont conformes au standard et non endommagés? ', N'¿Se utilizan adecuadamente las líneas/marcas del piso y no están dañadas?', N'Je značení na podlaze používáno správným způsobem a není poškozené? ', N'Are floor markings used in the right way and not damaged?  地上的标识是否正确使用没有被损坏？', N'안전장치 작동 확인이 TPM계획에 포함되고 매일 시업전 점검 되는가? ', N'Are floor markings used in the right way and not damaged? ให้แน่ใจว่ามีการตีเส้นระบุตำแหน่งที่ถูกต้องและไม่ชำรุด？', N'Da li se markiranja po podu ispravno koriste  i jesu li oštećena? ', N'As marcações no piso são usadas da forma certa e não estão danificadas?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (19, N'Are packaging instructions available and followed?', N'Sind Verpackungsvorschriften vorhanden und werden sie beachtet?', N'Est-ce que l instruction d emballage est disponible et appliquée ?', N'¿Se cumple con las instrucciones/planes de embalaje?', N'Jsou k dispozici a dodržovány pokyny pro balení?', N'Are packaging instructions available and followed?  包装作业指导书是否在现场并按照要求执行？', N'포장 작업 지침을 지키고 있는가?  (생산 차종과 포장 사양 일치 확인)', N'Are packaging instructions available and followed? ให้แน่ใจว่าได้มีการปฏิบัติตามมาตรฐานการบรรจุ จัดเก็บงาน？', N'Da li postoje propisi pakovanja i  poštuju li se?', N'As instruções de embalagem estão disponíveis e são seguidas?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (3, N'Are all employees in the work area trained properly to perform the required activities?', N'Sind alle Mitarbeiter im Arbeitsbereich richtig geschult, um die erforderlichen Tätigkeiten auszuführen?', N'Est-ce que les employés du poste de travail sont correctement formés pour faire leur travail ? Le nom de l opérateur est noté sur la Matrice de Qualification pour fabriquer le produit en cours avec le niveau de compétence (L accompagné d un référent, U ou O). L évaluation est conforme à la réalité / produit en cours de fabrication. La fiche de validation est conforme à la réalité / produit en cours de fabrication.', N'¿Están todos los operarios del área de trabajo entrenados adecuadamente para realizar las tareas requeridas?', N'Jsou všichni zaměstnanci na pracovišti správně zaškoleni na vykonávání požadovaných činností?', N'Are all employees in the work area trained properly to perform the required activities? 岗位的员工是否经过了培训并按照要求操作？', N'작업구역의 모든 작업자들은 시업 전 정해진 스트레칭과 안전에 관한 교육이 되었는가?', N'Are all employees in the work area trained properly to perform the required activities? ให้แน่ใจว่าพนักงานที่ปฏิบัติงานในสถานีงานนั้นๆได้รับการเทรนอย่างเพียงพอตามความต้องการของแต่ละกิจกรรมงาน？', N'Da li su svi zaposlenici u  radnom području odgovarajuće obučeni da sprovode potrebne aktivnosti?', N'Todos os colaboradores da área de trabalho são treinados adequadamente para realizar as atividades necessárias?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (4, N'Do operators understand the quality criteria for their workstation (regarding supplier parts, manufacturing process at the station and the result)?', N'Verstehen die Werker die Qualitätskriterien für Ihren Arbeitsbereich?  (bzgl. Anforderungen an Einzelteile/Komponenten, die genutzten Herstellprozesse und die Ergebnisse)?', N'Est-ce que les opérateurs connaissent les critères qualité pour fabriquer le produit (concernant les composants, le process de fabrication et le résultat? Demander à l opérateur ce qu il doit contrôler comme critère et vérifier en comparant aux informations de l instruction de travail visuelle.', N'¿Los operarios comprenden los criterios de calidad para su estación de trabajo (componentes comprados, proceso de fabricación y producto terminado)?', N'Znají operátoři kritéria kvality svého pracoviště (pokud jde o dodávané díly, výrobní procesy na pracovišti a výsledky)?', N'Do operators understand the quality criteria for their workstation (regarding supplier parts, manufacturing process at the station and the result)?  操作工是否了解其从事岗位的产品质量特性(包括供应商零件,装配过程以及装配完的产品)?', N'작업자들은 해당 작업 구역에서 본인의 작업내용과 품질및 안전문제 발생시 대응방법에 대하여 시업 전 충분히 숙지하고 이해하고 있는가?  ', N'Do operators understand the quality criteria for their workstation (regarding supplier parts, manufacturing process at the station and the result)? ให้แน่ใจว่าพนักงานเข้าใจจุดตรวจสอบที่สำคัญในสถานีงานนั้นๆ（ชิ้นส่วนจากซัพพลายเออร์, การผลิตและประกอบในสถานีงานนั้น）', N'Da li zaposlenici  shvataju kriterije kvaliteta  njihovog radnog područja (vezano za zahtjeve za pojedinačne dijelove/komponente, korištene procese proizvodnje i rezultate)?', N' Os operadores entendem os critérios de qualidade para a sua estação de trabalho (com relação às peças do fornecedor, ao processo de fabricação na estação e ao resultado)?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (23, N'Is the Line RPS used properly and up-to-date?', N'Wird Line RPS ordentlich/organisiert genutzt und ist er aktuell ?', N'Est-ce que le line RPS est correctement utilisée et à jour ?', N'¿Se utiliza adecuadamente el RPS de línea y está actualizado?', N'Jsou linkové RPS používány správně a jsou aktualizovány? Má linka vlastní Line RPS?', N'Is the Line RPS used properly and up-to-date? 在线RPS是否正确使用并及时更新？', N'라인 RPS 가  제대로 사용되고 있고 최신인가? ', N'Is the Line RPS used properly and up-to-date? ได้มีการใช้ RPS เป็นประจำ และข้อมูลได้มีการอัพเดท เสมอ？', N'Da li se linijiski RPS uredno/organizovano koristi i da li je aktuelan ?', N'O RPS da Linha é usado e  atualizado adequadamente?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (5, N'Are the workplaces clean and orderly? Does the workplace have sufficient lighting? ', N'Sind die Arbeitsplätze sauber und ordentlich/organsiert? Ist der Arbeitsplatz ausreichend beleuchtet? ', N'Est-ce que l espace de travail est propre ? Pas de saleté, de composants par terre, pas de cable au sol ?  Est-ce que l espace de travail est suffisamment éclairé? ', N'¿Están los puestos de trabajo limpios y ordenados?', N'Jsou pracovní místa čisté a uklizeny? Má pracoviště dostatečné osvětlení? ', N'Are the workplaces clean and orderly? Does the workplace have sufficient lighting?  工作区域是否干净有序？工作区域的照明是否充分？', N'작업장 바닥및 장비의 청소 상태는 양호한가? 작업장의 조도는 충분한가? ', N'Are the workplaces clean and orderly? Does the workplace have sufficient lighting?  ให้แน่ใจว่าพื้นที่ทำงานสะอาดเป็นระเบียบ และมีแสงสว่างที่เพียงพอ？', N'Jesu li  radna mjesta čista i uredna/organizovana? Je li radno mjesto  dovoljno osvjetljeno ? ', N'Os locais de trabalho estão limpos e arrumados? O local de trabalho tem iluminação suficiente?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (6, N'Is there a place defined for everything? Is everything on its place?', N'Ist für jede Sache ein Platz definiert?  Ist jede Sache an dem vorgesehen Platz?', N'Est-ce que l espace de travail est rangé ? Y a-t-il une place définie pour chaque chose? Est-ce que tout est à sa place? Il n y a pas de chose personnel sur le poste de travail.', N'¿Todo tiene un lugar definido, y todo está en su lugar?', N'Je pro všechno definováno příslušné místo? Je všechno na svém místě?', N'Is there a place defined for everything? Is everything on its place? 是否每个物品都定义了摆放位置？物品是否放在了正确的位置？', N'작업현장에 비치된 모든 물품의 사용방법등이 명기 및 가시화되어 있고 정확한 위치에 비치 되었는가? ', N'Is there a place defined for everything? Is everything on its place? ให้แน่ใจว่าพื้นที่ทำงานได้มีการระบุตำแหน่งต่างๆ และสิ่งของต้องวางอยู่ในตำแหน่งพื้นที่ที่กำหนด？', N'Je li za svaku stvar definisano određeno mjesto? Je li se svaka stvar nalazi na određenom mjestu?', N'Existe um lugar definido para tudo? Está tudo em seu lugar?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (7, N'Are all boxes and containers properly identified and clean?', N'Sind alle Boxen und Container (Verpackungen) richtig gekennzeichnet und sind sie sauber?', N'Est-ce que tous les emballages (fournisseur, client) sont propres et bien identifiés ?', N'¿Están todas las cajas y contenedores adecuadamente identificados?', N'Jsou všechny boxy a zásobníky správně identifikovány a čisté?', N'Are all boxes and containers properly identified and clean? 是否所有的箱子和容器都被正确的标识并保持干净？', N'모든 자재및 제품 용기들이 정확히 식별표에 의해 식별되고 정위치에 놓여 있는가? 모든 용기는 청결한가?  (다른 랙 위에 적재 및 혼입 여부 확인)', N'Are all boxes and containers properly identified and clean? กล่องเหลือง แดง ที่อยู่ในไลน์ต้องมีการระบุบ่งชี้ที่ชัดเจน และสะอาด？', N'Da li su sve kutije i kontejneri (Pakovanja) ispravno označeni i  jesu li čisti?', N'Todas as caixas e contenedores estão devidamente identificados e limpos?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (8, N'Is the FIFO principle applied for all materials. Are maximum stock levels in the line and in the racks not exceeded?', N'Wird das FIFO Prinzip für alle Materialien angewendet? Sind die maximalen Lagermengen an der Linie und in den Regalen nicht überschritten?', N'Est-ce que les Kanban sont correctement utilisés ( trains + ligne de fabrication); Vérifier qu il y  a le bon nombre de Kanban.', N'¿Se aplica el principio FIFO en todos los materiales? ¿Los niveles máximos de almacenamiento en la línea y los contenedores no están excedidos?', N'Je princip FIFO dodržován pro všechny materiály? Nejsou maximální hladiny zásob na lince a v regálech překročeny?', N'Is the FIFO principle applied for all materials. Are maximum stock levels in the line and in the racks not exceeded? 是否所有的原料都采用先进先出？在线和货架上的库存量是否超出最大库存要求？', N'모든 자재및 제품들에 대한 정확한 선입선출이 이루어지는가? 최소 및 최대 적재량을 표시하고 지켜지고 있는가?  ', N'Is the FIFO principle applied for all materials. Are maximum stock levels in the line and in the racks not exceeded? ให้แน่ใจว่ามีการควบคุม FIFO เข้าก่อนออกก่อนของชิ้นส่วน？ต้องไม่มีงานล้น หรือเกินระดับสต็อกสูงสุดที่ควบคุม？', N'Da li se FIFO princip primjenjuje na sve materijale? Da li su prekoračene maksimalne skladišne količine na liniji i u stalažama?', N'O princípio FIFO (Primeiro que Entra, Primeiro que Sai) é aplicado para todos os materiais? Os níveis máximos de estoque na linha e nos racks não são excedidos?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (11, N'TPM: Are defined maintenance activities executed according to the plan? Is this documented?', N'TPM: Werden festgelegte Wartungsarbeiten gem. den vorgegebenen Plänen durchgeführt? Ist das dokumentiert worden?', N'TPM: Est-ce que les actions de maintenance définies sont réalisés en accord avec le plan ? Est-ce que c est documenté (vérifier que le document de suivi des actions TPM est complété et que les délais sont respectés) ?', N'TPM: ¿Las actividades de mantenimiento son realizadas de acuerdo al plan? ¿Están estas actividades registradas?', N'TPM: Jsou definované činnosti údržby vykonávány v souladě s plánem? Je tato činnost zdokumentována?', N'TPM: Are defined maintenance activities executed according to the plan? Is this documented? 设备维护：定义的设备维护是否按照计划执行？是否被文件化？', N'TPM: 유지보수 활동이 문서화 되어 있는가? 계획에 따라 실행되고 있는가?모든 장비및 점검 항목들은 쉽게 점검이 가능하도록 시각화 되어 있는가? (예. 게이지상 Max,Min 표시)', N'TPM: Are defined maintenance activities executed according to the plan? Is this documented? ให้แน่ใจว่าTPM ได้มีการตรวจเช็คและติดตามอย่างสม่ำเสมอ รวมถึงการจัดเก็บเอกสารที่ดี？', N'TPM: Da li su definisane aktivnosti održavanja izvršene u skladu sa planom? Da li su iste i dokumentovane?', N'TPM: As atividades de manutenção definidas são executadas de acordo com o plano? Ele é documentado?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (10, N'Are the safety rules followed? Is personal protection equipment listed on the work instruction and properly used? Is safety equipment working (not disabled) and part of the maintenance plan (TPM)?', N'Werden die Sicherheitsregeln beachtet? Werden persönliche Schutzaus-rüstungen richtig genutzt ? Funktionieren die Sicherheitseinrichtungen korrekt (nicht außer Betrieb genommen) und sind diese Inhalt der Wartungspläne (TPM)?', N'Est-ce que les règles de sécurité sont connues (demander à l opérateur et comparer à l instruction de travail visuelle) et appliquées ? Est-ce que les équipements de protection individuelles sont listés dans l instruction de travail et sont correctement portés ? Les équipements de sécurité fonctionnent et font partie du plan de Maintenance (rechercher sur la ligne s il y a des risques pour la sécurité du personnel)?', N'¿Se cumplen las reglas de seguridad? ¿Se utilizan adecuadamente los elementos personales de protección? ¿Están en funcionamiento (no inhabilitados) los equipamientos de seguridad y están incluídos en los planes de mantenimiento (TPM)?', N'Jsou dodržovány zásady bezpečnosti? Jsou speciální osobní ochranné prostředky uvedeny v pracovním postupu a vizuální návodce a správně používány? Fungují bezpečnostní zařízení (např. nejsou deaktivovány? není klíč v ovládacím panelu?) a jsou součástí plánu údržby TPM?', N'Are the safety rules followed? Is personal protection equipment listed on the work instruction and properly used? Is safety equipment working (not disabled) and part of the maintenance plan (TPM)? 是否遵守安全制度？个人防护用品是否按照指导书正确使用？安全设备使用（未用）是否列为设备维护的一部分？', N'요구되는 개인 보호장비가 작업표준서에 명시되고 시각화 되어 있는가? 작업자는 개인보호 장비를 착용하고 있는가? ', N'Are the safety rules followed? Is personal protection equipment listed on the work instruction and properly used? Is safety equipment working (not disabled) and part of the maintenance plan (TPM)? ให้แน่ใจว่าได้มีการปฏิบัติตามกฏความปลอดภัย？อุปกรณ์ที่เกี่ยวข้องกับความปลอดภัยต้องอยู่ในสภาพที่พร้อมใช้งานและต้องอยู่ในแผนงานการตรวจเช็คของช่าง (PM)？', N'Da li se slijede sigurnosna pravila? Da li se lična zaštitna oprema adekvatno koristi ? Da li je sigurnosna  oprema  ispravna (nije pokvarena/isključena) i da li je dio plana održavanja (TPM)?', N'As normas de segurança são seguidas? Os equipamentos de proteção individual (EPIs) estão listados na instrução de trabalho e são usados apropriadamente? Os equipamentos de segurança estão funcionando (não desativados) e fazem parte do plano de manutenção (TPM).  ')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (12, N'Are test equipment, poka yoke devices, gages and fixtures verified? Is the result documented?', N'Werden Prüfeinrichtungen, Poka Yoke Einrichtungen, Lehren und Vorrichtungen überprüft? Sind die Ergebnisse dokumentiert?', N'Est-ce que les équipements sont testés, les poka yoke, les maquettes et calibres de contrôle sont vérifiées? Est-ce que le résultat est documenté? Vérifier que l instruction de démarrage est correctement complété par rapport à ces critères.', N'¿Son verificados los equipos de ensayo, dispositivos poka yoke, calibres pasa no-pasa? ¿Estas verificaciones están documentadas?', N'Jsou testovací zařízení, zařízení poka yoke, měřící a upínací zařízení ověřovány? Jsou výsledky dokumentovány? (např. kalibr. známky, možnost neshodného uložení do Poka Yoke)', N'Are test equipment, poka yoke devices, gages and fixtures verified? Is the result documented? 检查设备，防错设备，检具和工装是否有效？结果是否文件化？', N'테스트 장비, 포카요케, Error Proofing 장치에 대한 검증절차가 문서화 되어있고 매 시프트마다 불량마스터에 의한 검증을 하는가? 게이지 및 지그는 검증 절차가 문서화 되어있고 정기적인 교정(calibration)이 되었는가? ', N'Are test equipment, poka yoke devices, gages and fixtures verified? Is the result documented? ให้แน่ใจว่ามีการบันทึกการตรวจสอบอุปการณ์ PoKayoke, เกจ ฟิกเจอร์？', N'Da li se ispitni uređaji, poka yoke uređaji, kontrolnici i oprema provjeravaju?  Da li se dokumentuju rezultati?', N'Os equipamentos de teste, dispositivos poka yoke, medidores e acessórios são verificados? O resultado é documentado?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (13, N'Was the startup for this part or this shift according to standard (the sheet filled-out, corrective actions documented)', N'Ist die Produktionsfreigabecheckliste für dieses Teil oder diese Schicht vollständig, Maßnahmen definiert und abgeschlossen, falls erforderlich? ', N'L instruction de démarrage en production est correctement complétée. En cas d écart, les actions menées sont renseignées dans le document.', N'¿Se completa la hoja de Lanzamiento de la Producción y las acciones correctivas están definidas y son concluidas a tiempo?', N'Bylo na začátku směny nebo po odstávce provedeno uvolnění pracoviště v souladu s Pokyny pro zahájení výroby?  Protokol o uvolnění vyplněný, nápravné činnosti zdokumentovány?', N'Was the startup for this part or this shift according to standard (startup sheet filled-out, corrective actions documented)  产品或者班次的生产启动是否执行？（填写生产启动检查表，纠正措施文件化）', N'스타트업 체크시트가 부품 혹은 교대변경 시 표준에 따라 작성되는가? (Nok 시  정의 된 대응 계획에 따라 시정조치사항 기록 문서화 확인) ', N'Was the startup for this part or this shift according to standard (startup sheet filled-out, corrective actions documented) ตรวจดูว่าได้มีการปฏิบัติตามมาตรฐานการเริ่มงาน (startup) เช่น กรอกเอกสารถูกต้อง บันทึกการแก้ไข(ถ้ามี)', N'Da li je checklista za odobrenje procesa proizvodnje za ovaj dio ili ovu smjenu potpuna, jesu li definisane i  zatvorene  mjere, ukoliko su neophodne?  ', N'O startup para essa peça ou esse turno estava de acordo com a norma (folha de startup preenchida, ações corretivas documentadas)')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (14, N'Are startup parts properly identified and in the defined location?', N'Sind Produktionsfreigabemuster ordentlich gekennzeichnet und liegen diese am festgelegten Platz vor?', N'Est-ce que la première pièce bonne de l équipe est dans le bac bleu sur la ligne. Elle est identifiée avec : la date, l heure de fabrication, le nom de l opérateur et son équipe.', N'¿Está adecuadamente identificada la Primer Pieza y ubicada en un lugar definido?', N'Jsou uvolňovací kusy (1. kusy) správně označeny a nacházejí se na definovaném místě? Odpovídá 1. kus výkresové dokumentaci?', N'Are startup parts properly identified and in the defined location?  生产启动零件是否标识并放在指定的区域？', N'초중종물은 정해진 위치에 있고 초,중,종 체크시트가 정확한 프로세스에 따라 업데이트 되는가? ', N'Are startup parts properly identified and in the defined location? ต้องแน่ใจว่างานตัวแรก (start up part) ได้มีการระบุและเก็บในตำแหน่งที่ระบุ？', N'Jesu li uzorci za odobrenje proizvodnje  uredno obilježeni i nalaze li se na definisanom mjestu? ', N'As peças de startup são identificadas apropriadamente e no local definido?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (15, N'Do the conditions in the line comply to the requirements in the Standardized Work Chart? Are operators following standardized work chart / visual work instruction?', N'Sind die Bedingungen in der Linie so wie in der standardisierten Arbeitskarte beschrieben?  Beachten die Werker die standardisierten Arbeitskarte und bildliche Arbeitsanweisung?', N'Est-ce que l implantation de la ligne est en accord avec le mode opératoire standard? Est-ce que les opérateurs suivent le mode opératoire standard et l instruction de travail visuelle ?', N'¿Los operarios cumplen con el Plan de trabajo estándar y las Instrucciones visuales de trabajo?', N'Odpovídají podmínky na lince požadavkům Standardizovaného schéma práce? Dodržují pracovníci toto schéma / vizuální návodky pro práci?', N'Do the conditions in the line comply to the requirements in the Standardized Work Chart? Are operators following standardized work chart / visual work instruction?  生产线是否符合标准流程图？员工是否按照标准作业指导书工作？', N'표준업무 차트가 현재의 작업 조건을 반영하였는가? 작업자는 표준업무 차트에 따라 업무를 수행하고 있는가? (임의의 작업자를 선정하여 실제 작업 방법과 비교)', N'Do the conditions in the line comply to the requirements in the Standardized Work Chart? Are operators following standardized work chart / visual work instruction? ให้แน่ใจว่าพื้นที่ทำงานเป็นไปตามงานมาตรฐาน รวมถึงพนักงานได้ปฏิบัติตามงานมาตรฐานที่กำหนด？', N'Da li  uvjeti na liniji odgovaraju opisu na standardnoj radnoj karti ? Da li operateri slijede standardnu radnu kartu i vizuelna radna uputstva?', N'As condições na linha estão em conformidade com os requisitos do Gráfico de Trabalho Padronizado? Os operadores estão seguindo o gráfico de trabalho padronizado/instrução de trabalho visual?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (16, N'Is the real cycle time according to the standardized work chart? Is the line well balanced? Is the no. of operators acc. to Standardized Work Chart?', N'Ist die tatsächliche Zykluszeit gemäß der standardisierten Arbeitskarte?  Ist die Linie gut ausgetaktet? Entspricht die Anzahl Werker der Vorgabe auf der standardisierten Arbeitskarte?', N'Est-ce que le temps de cycle est en accord avec le mode opératoire standard ? Est ce que le nombre d opérateur sur la ligne est en accord avec le mode opératoire standard?', N'¿Está el tiempo de ciclo real de acuerdo con el Plan de trabajo estándar? ¿Está la línea bien balanceada?', N'Je skutečná doba cyklu v souladu se Standardizovaným schéma práce? Je linka správně vyvážená? Je počet pracovníků v souladu se Standardizovaným schéma práce?  Odpovídají údaje v prac. postupu a stand. schéma práce skutečnosti?', N'Is the real cycle time according to the standardized work chart? Is the line well balanced? Is the no. of operators acc. to Standardized Work Chart? 产品生产周期是否和标准流程图一致？生产线各工位是否协调？操作工人数否符合标准作业流程要求?', N'시각적 지침서는 쉽게 따라 할 수 있도록 작업 단계별로 명확하게 작성 되었으며, 충분한 설명이 있는가?', N'Is the real cycle time according to the standardized work chart? Is the line well balanced? Is the no. of operators acc. to Standardized Work Chart? เช็คเวลาในการทำงานว่าตรงตามงานมาตรฐานหรือไม? จำนวนพนักงานต้องตรงตามที่ระบุ?', N'Da li je stvarno ciklusno vrijeme u skladu sa standardnom radnom kartom? Da li je takt linije odgovarajući? Da li je broj operatera u skladu sa propisom na standardnoj radnoj karti ?', N'O tempo de ciclo real está de acordo com o Gráfico de Trabalho Padronizado ? A linha está bem equilibrada? O número de operadores está de acordo com o Gráfico de Trabalho Padronizado ?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (18, N'Are defined boundary samples or a catalog available for all quality checks and used according the visual work instruction?', N'Sind Grenzmuster zur Verfügung und werden diese gemäß der bildlichen Arbeitsanweisung / Anweisung für Sichtprüfung verwendet?', N'Est-ce qu une défauthèque (pièces ou papiers)  sont disponibles pour les contrôles qualité et utilisé en accord avec l instruction de travail visuelle?', N'¿Se disponde de muestras límite y son utilizadas de acuerdo a las Instrucciones visuales de trabajo?', N'Jsou definovány hraniční vzorky nebo je k dispozici katalog pro všechny kontroly kvality a používá se v souladu s vizuálními pracovními pokyny?', N'Are defined boundary samples or a catalog available for all quality checks and used according the visual work instruction? 现场是否制作了边界样件或者目录以供质量检查，这些是否有作业指导书？', N'정의 된 한도견본샘플, 양품마스터샘플 또는 카달로그가 비치되어 있고, 모든 품질확인사항이 목록화 되어 있으며, 시각적 작업 지침서에 따라 사용되는가? ', N'Are defined boundary samples or a catalog available for all quality checks and used according the visual work instruction? ให้แน่ใจว่าได้มีการจัดเตรียม ตัวอย่างงานดี งานเสีย สำหรับการตรวจเช็คที่สะดวก(master OK and NG)？', N'Da li su granični uzorci na raspolaganju i da li se isti shodno vizuelnim radnim uputstvima  koriste za vizuelnu kontrolu ?', N'Estão definidas as amostras limite ou um catálogo está disponível para todas as verificações da qualidade e é usado de acordo com a instrução de trabalho visual?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (17, N'Is the reaction plan known by the operator / Line leader (when and how to react with failures, how to react with breakdowns or how to react when identifying a problem during startup)?', N'Ist der Reaktionsplan bei den Werkern bekannt (wann und wie ist zu reagieren bei Fehlern, wie muss reagiert werden bei Maschinenausfall oder wie muss reagiert werden bei Problemen während der Anfahrphase)? ', N'Est-ce que le plan de réaction de l instruction de démarrage est connu des opérateurs et des GAP leader (Quand et comment réagir en cas de défaillance, comment réagir en cas de panne et comment réagir en cas de problème au démarrage?', N'¿Se cumplen con los planes de reacción (cuándo y cómo reaccionar ante fallas, cómo reaccionar ante paradas de la línea ó cómo reaccionar cuando se identifica un problema en el lanzamiento de la producción)?', N'Zná operátor / line leader reakční, eskalační plán a hranice zásahu (kdy a jak reagovat na chyby, jak reagovat na poruchy nebo jak reagovat při identifikaci problému při spuštění)?', N'Is the reaction plan known by the operator / Line leader (when and how to react with failures, how to react with breakdowns or how to react when identifying a problem during startup)? 操作工/班长是否清楚反应计划？(出现问题的时，什么时候？怎样处理？停机怎样处理？当检查生产启动时发现问题如何处理？)', N'불량발생 경우: 불량 발생 시 정식절차에 따라 보고 하는가?  동일문제 2회 발생시 즉시 보고하고 작업을 중단 시키는가? 이에 대한 기록이 라인에 유지되는가? ', N'Is the reaction plan known by the operator / Line leader (when and how to react with failures, how to react with breakdowns or how to react when identifying a problem during startup)? ให้แน่ใจว่าพนักงานในไลน์ เข้าใจ และปฏิบัติตามมาตรฐานในกรณีทีมีปัญหา เช่น ด้านคุณภาพ เครื่องจักรหยุด เป็นต้น', N'Da li je plan reakcije operaterima poznat  (kada i kako reagovati kad nastane greška, kako reagovati kod zastoja mašine ili kako se treba reagovati kada se javi problem prilikom puštanja linije u rad )? ', N' O plano de reação é conhecido pelo operador/ Líder da linha (quando e como reagir a falhas, como reagir a paradas ou como reagir quando identificando um problema durante o startup)?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (20, N'Are nonconforming or suspect products identified and placed in a designated area? Scrap parts in red bins; Suspect parts in yellow bins.', N'Werden nicht  konforme oder suspekte Teile erkannt/gekennzeichnet und an einem festgelegten Platz abgelegt? Ausschuss teile in roten Behältern; suspekte Teile in gelben Boxen)?', N'Les opérateurs savent comment gérer les rebuts : * Rebuts dans les bacs rouge * Retouche dans les bacs jaune', N'¿Se identifican las piezas no conformes ó productos sospechosos y son ubicados en un lugar específico? Piezas de scrap en contenendores rojos; piezas sospechosas en contenendores amarillos.', N'Jsou neshodné anebo podezřelé produkty označeny a umístěny do určeného prostoru? Zmetky do červených přepravek, podezřelé díly do žlutých?  Odpovídá počet NOK dílů Sběrné kartě chyb?', N'Are nonconforming or suspect products identified and placed in a designated area? Scrap parts in red bins; Suspect parts in yellow bins. 是否所有的不合格品和可疑品都被标识并且放置在指定的区域？报废产品放红箱，可疑产品放黄箱。', N'부적합품과 의심품은 각각의 지정된 Red bin과 Yellow Bin 에 격리되어 있는가? 불량테그는 부착되어 있는가? ', N'Are nonconforming or suspect products identified and placed in a designated area? Scrap parts in red bins; Suspect parts in yellow bins.ให้แน่ใจว่างานที่คาดว่าจะมีปัญหา หรืองานที่มีปัญหา ได้มีการคัดแยกและมีป้ายระบุที่ชัดเจน รวมถึงการระบุพื้นที่ที่ชัดเจน。', N'Da li su neskladni ili sumnjivi dijelovi prepoznati/obilježeni  i premješteni na određeno mjesto? Škart dijelovi u crvenim gajbama; sumnjivi dijelovi u žutim gajbama.', N'Os produtos não conforme e suspeitos são identificados e colocados em uma área designada? Sucata em recipientes vermelhos; peças suspeitas em recipientes amarelos.')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (25, N'Is there a visualization of the performance of the line available? Is Line RPS used when the target is not reached (Downtime, Scrap, etc.)', N'Wird die Leistung der Linie an der Linie visualisiert ? Wird ein Linien RPS eingesetzt wenn die Zielvorgaben nicht erreicht werden (Linienstillstand, Ausschuss etc.)?', N'Est-ce que la performance de la ligne est disponible et visible (KPI) ? Est-ce que le tableau de marche est correctement  utilisé?', N'¿Hay disponible una visualización del rendimiento / performance de la línea? ¿Se utiliza el RPS de línea cuando el objetivo no es alcanzado (paradas, scrap, etc)', N'Je na lince k dispozici vizualizace výkonu (Tabulka výkonů/Hodinové sledování)? Používá se linkové RPS, pokud není cíl plněn (prostoje, zmetky atd.)?', N'Is there a visualization of the performance of the line available? Is Line RPS used when the target is not reached (Downtime, Scrap, etc.) 在线上是否有可视化的业绩？当指标没有完成时是否使用了在线RPS？（停机时间，报废率等）', N'라인의 운영성과가 시각화 되어 매일 정확히 기록되고 있는가? 모든 부적합사항에 대해 라인 RPS가 작성되고 개선 되었는가? 비가동,스크랩 등 ', N'Is there a visualization of the performance of the line available? Is Line RPS used when the target is not reached (Downtime, Scrap, etc.) ได้มีการแสดงผลของการผลิตที่มองเห็นได้ชัดเจนไหม? ในกรณีที่ผลผลิตไม่ได้ตามเป้าหมาย ได้มีการนำ RPS มาใช้หรือไม่?', N'Da li je učinak (performanse) linije vizualiziraju na liniji ? Da li se kod nedostignuća cilja  koristi linijski RPS (zastoj, škart, itd.)?', N'Existe uma visualização disponível do desempenho  da linha? A Linha RPS é usada quando a meta não e atingida (tempo ocioso,sucata, etc.)')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (21, N'In case of rework: is rework executed according to the standard (outside of the line, workstation according to standard, material re-entering the line at same place it was sorted out) ', N'Wird Nacharbeit gemäß den Standardvorgaben ausgeführt (außerhalb der Produktionszelle, Nacharbeitsplatz gemäß Standard, Material fließt wieder an der Stelle in der Linie ein, an der es den Prozess verlassen hat)?', N'En cas de retouche, est ce que les retouches sont effectuées en accord avec le standard de travail (en dehors de la ligne, la station de retouche est en accord avec le standard, les pièces retouchées retournent au même endroit qu elles sont sorties, les opérateurs savent comment gérer les retouches) In case of rework: is rework executed according to the standard (outside of the line, workstation according to standard, material re-entering the line at same place it was sorted out) ', N'En caso de retrabajo: ¿es el retrabajo realizado de acuerdo al estándar (fuera de la línea, estación de retrabajo según el estándar, las piezas re-ingresan a la línea al mismo lugar en el que fué retirada)', N'V případě demontáže nebo povolené opravy: vykonává se demontáž/oprava v souladu prac. návodkou a mimo linku?', N'In case of rework: is rework executed according to the standard (outside of the line, workstation according to standard, material re-entering the line at same place it was sorted out) 对于返工：返工是否按照标准执行(离线返工)? 根据标准工作区域，材料重新进入其被挑选出的区域。', N'리워크의 경우: LOT 불량이 인지되면 즉각 보고 하고 작업은 바로 중단 되었는가? 불량Tag 를 부착후 격리장소로 이동 하였는가? 이에대한 기록은 라인에서 유지 되는가? ', N'In case of rework: is rework executed according to the standard (outside of the line, workstation according to standard, material re-entering the line at same place it was sorted out) ในกรณีงาน rework จะต้องมีการกำหนดมาตรฐานการซ่อมงาน พื้นที่ ที่ชัดเจน รวมถึงในกรณีที่ชิ้นส่วนที่สามารถนำกลับมาใช้ได้อีกต้องมีการควบคุม ตรวจสอบ。', N'Da li se dorada vrši  prema standardu  (izvan proizvodne linije, mjesto za doradu u skladu sa standardom,  materijal se vraća ponovo u liniju na istom mjesto gdje je i odstranjen) ?', N'No caso de retrabalho: o retrabalho é executado de acordo com a norma (fora da linha, estação de trabalho de acordo com a norma, reentrada de material na linha no mesmo lugar que foi classificado/separado).')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (22, N'In case of any deviation to the standard (e.g. regarding product, process, maintenance etc.): is there a valid, written and approved deviation permit and are all conditions in this permit applied?', N'Im Falle von jeglicher Abweichung zum Standard (z.B. bezogen auf Produkte, Prozess, Wartung etc.): liegt eine gültige, schriftliche und genehmigte Abweicherlaubnis vor und sind alle Bedingungen die in der Abweicherlaubnis beschrieben sind eingeführt?', N'En cas d écart avec le standard (par ex produit, process, maintenance, ect), y a t il une deviation permit valide, écrite, approuvée et appliquée?', N'En el caso de algún desvío del estándar (producto, proceso, mantenimiento, etc.): ¿Hay un Permiso de desvío válido, escrito y aprobado y se cumplen todas las condiciones del Permiso de desvío?', N'V případě jakékoliv neshody/odchylky od normy (pokud jde o produkt, proces, údržbu atd.): existuje platná, písemná a schválená odchylka a jsou všechny podmínky téhle odchylky dodržovány?', N'In case of any deviation to the standard (e.g. regarding product, process, maintenance etc.): is there a valid, written and approved deviation permit and are all conditions in this permit applied? 与标准有偏差的情况（譬如：产品，工艺，维护保养等）：有无有效的书面批准偏离许可，并在本文件中提出了偏离的情况？', N'표준 특채의 경우(제품,공정,보전 등): 생산팀장, 공장장, CFO 및 MD의 승인에 따라 이루어 졌는가? 이에대한 기록은 라인에 유지 되는가? ', N'In case of any deviation to the standard (e.g. regarding product, process, maintenance etc.): is there a valid, written and approved deviation permit and are all conditions in this permit applied? ในกรณีที่งานไม่เป็นไปตามข้อกำหนด (งานไม่ได้ตามขนาด ขบวนการผบลิตหรือเครื่องจักรผิดปรกติ) จะต้องมีการขออนุญาตขอใช้เป็นกรณีพิเศษ ？', N'U slučaju bilo kakvog odstupanja od standarda (npr. vezano za proizvod, proces, održavanje itd.): postoji li validna, pismena i odobrena dozvola za odstupanje i da li su ispunjeni svi uvjeti koji su opisani u dozvoli za odstupanje?', N'No caso de algum desvio à norma (por ex.: com relação ao produto, processo, manutenção, etc.): existe uma autorização de desvio escrita, aprovada e válida e todas as condições da autorização são aplicadas?')
INSERT [dbo].[mh_lpa_Question_language_master] ([Question_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (24, N'Are there current customer complaints at the line / for this product? Are the operators aware of the complaint and the corrective action? Is an 8D available for every complaint and up-to-date?', N'Gibt es aktuell Kundenreklamationen an der Line / für das Produkt? Kennen die Werker die Reklamation und die Abstellmaßnahmen ? Ist ein 8D zu jeder Reklamation vorhanden und aktuell?', N'Y a-t-il des réclamations clients sur la ligne / au produit actuel ? Est ce que les opérateurs connaissent les réclamation et les actions correctives ? Y a t il un 8D  à jour pour chaque réclamation client ?', N'¿Existen actualmente reclamos de clientes en la línea / para este producto? ¿Están los operarios informados del reclamo y la acciones correctivas? ¿Hay disponible un 8D para cada reclamo y está actualizado?', N'Jsou na lince / pro tento produkt aktuální reklamace od zákazníka? Jsou operátoři informovaní o dané reklamaci a nápravném opatření? Je 8D k dispozici pro každou reklamaci a aktuální?', N'Are there current customer complaints at the line / for this product? Are the operators aware of the complaint and the corrective action? Is an 8D available for every complaint and up-to-date? 在该线上或该产品是否有当前的客户抱怨信息/操作工是否了解抱怨和相应的纠正措施？每个抱怨都有8D报告吗？报告有没有及时更新？', N'해당 라인/해당 제품의 최근 고객불만에 관한 기록이 있는가? 작업자가 고객불만과 시정조치에 대해 인지하고 있는가? 모든 불만에 대해 8D 및 라인 RPS가 작성 유지 되는가?  ', N'Are there current customer complaints at the line / for this product? Are the operators aware of the complaint and the corrective action? Is an 8D available for every complaint and up-to-date?ได้มีข้อร้องเรียนจากลูกค้าไหม？พนักงานได้รับการสือสารเรื่องข้อร้องเรียนจากลูกค้าไหม?รวมถึงได้มีการนำ 8D มาใช้หรือไม่?', N'Ima li na liniji aktuelnih reklamacija kupaca / za proizvod? Jesu li zaposlenici upoznati sa reklamacijom i korektivnim mjerama ? Da li za svaku reklamaciju postoji 8D i je li aktuelan?', N' Existem reclamações atuais do cliente na linha/para este produto?  Os operadores estão conscientes da reclamação e a ação corretiva? Um 8D está disponível para cada reclamação e está atualizado?')
INSERT [dbo].[mh_lpa_Section_language_master] ([section_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (1, N'General Topics', N'Allgemein', N'Généralités', N'Temas Generales', N'Všeobecné', N'General Topics 通常话题', N'General Topics', N'General Topics หัวข้อทั่วไป', N'Općenito', N'Tópicos gerais')
INSERT [dbo].[mh_lpa_Section_language_master] ([section_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (2, N'A: People Development', N' A: Entwicklung der Mitarbeiter', N' A: Développement du personnel', N' A: Desarrollo del Personal', N' A: Rozvoj lidských zdrojů', N' A: People Development 人员', N' A: People Development', N' A: People Development  การพัฒนาบุคลากร', N' A:Razvoj zaposlenika', N' A: Desenvolvimento Pessoal')
INSERT [dbo].[mh_lpa_Section_language_master] ([section_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (3, N'B: Safe, Organized, Clean Work Area', N'B: sichere, ordentliche und saubere Arbeitsumgebung', N'B: Espace de travail sécurisé, organisé et propre', N'B: Área de trabajo Segura, Organizada y Limpia', N'B: Bezpečné, organizované, čisté pracoviště', N'B: Safe, Organized, Clean Work Area 安全，有序，清洁的工作场所', N'B: Safe, Organized, Clean Work Area', N'B: Safe, Organized, Clean Work Area  ความปลอดภัย ความสะอาด ของพื้นที่ทำงาน', N'B: Sigurno, uredno i čisto radno okruženje ', N'B:  Área de Trabalho Limpa, Organizada e Segura')
INSERT [dbo].[mh_lpa_Section_language_master] ([section_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (4, N'C: Robust Processes and Equipment', N'C: Robuste Prozesse und Maschinen', N'C: Equipements et process robustes', N'C: Equipamientos y Procesos Robustos', N'C: Robustní procesy a zařízení', N'C: Robust Processes and Equipment 工艺和设备', N'C: Robust Processes and Equipment', N'C: Robust Processes and Equipment ขบวนการผลิตและเครื่องจักร', N'C: Sigurni procesi i mašine', N'C: Processos Robustos e Equipamentos')
INSERT [dbo].[mh_lpa_Section_language_master] ([section_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (5, N'D: Standardized Work  ', N'D: Standardabläufe', N'D: Travail standard', N'D: Trabajo Estándar', N'D: Standardizovaná práce ', N'D: Standardized Work  标准作业', N'D: Standardized Work  ', N'D: Standardized Work  มาตรฐานการทำงาน', N'D: Standardni tokovi', N'D:  Trabalho Padronizado')
INSERT [dbo].[mh_lpa_Section_language_master] ([section_id], [English], [German], [French], [Spanish], [czech], [Chinese], [Korean], [Thai], [Bosnian], [Portuguese]) VALUES (6, N'E: Rapid Problem Solving / 8D', N'E: Schnelle Problemlösung / 8D', N'E: Rapid Problem Solving / 8D', N'E: Rápida Solución de Problemas / 8D', N'E: Rychlé řešení problémů (Rapid Problem Solving) / 8D', N'E: Rapid Problem Solving / 8D 快速问题解决？/8D', N'E: Rapid Problem Solving / 8D', N'E: Rapid Problem Solving / 8D การแก้ไขปัญหาอย่างทันทีเมื่อมีปัญหา/8D', N'E: Brzo rješavanje problema / 8D', N'E: Solução Rápida de Problemas/ 8D ')


GO
*/

---Don't run below untill Bakta sir approves. Below query is for Date and time field in Audit Perform


CREATE PROCEDURE [dbo].[InsAuditResults_date](@p_audit_id int,
@p_audit_plan_id   int,
			@p_line_id			int,
			@p_audited_by_user_id  int,
			@p_audit_date		datetime,
			@p_shift_no			int,
			@p_AuditResults		AnsResultsTableType READONLY
        )
AS
 SET NOCOUNT ON;
 
BEGIN
 DECLARE @l_audit_id  int;
 DECLARE @l_err_code  int;
 DECLARE @l_err_message  varchar(100);
 DECLARE @l_region_id INT;
 DECLARE @l_country_id INT;
 DECLARE @l_location_id INT;
 DECLARE @l_line_id  INT;
 DECLARE @l_count  INT;
 DECLARE @l_result_count  INT;
 DECLARE @l_send_note_to_owner VARCHAR(10);
 DECLARE @l_invite_to_list VARCHAR(max);
 DECLARE @l_mail_to_list VARCHAR(max);
 DECLARE @l_audit_date DateTime
 DECLARE @l_region_name   VARCHAR(100);
 DECLARE @l_country_name  VARCHAR(100);
 DECLARE @l_location_name  VARCHAR(100);
 DECLARE @l_line_name   VARCHAR(100);
 

	/*  SET @p_audit_date=GETDATE(); */
	IF @p_audit_date IS NULL
		SET @l_audit_date=GETDATE(); 
	ELSE
		SET @l_audit_date=@p_audit_date; 



	IF @p_line_id IS NOT NULL AND @p_line_id != 0 
		SELECT @l_region_name = rm.region_name,
			@l_country_name = cm.country_name,
			@l_location_name = loc.location_name,
			@l_line_name = lm.line_name,
			@l_region_id = rm.region_id,
			@l_country_id = cm.country_id,
			@l_location_id = loc.location_id,
			@l_line_id = lm.line_id
		FROM  mh_lpa_line_master lm,
			mh_lpa_region_master rm,
			mh_lpa_country_master cm,
			mh_lpa_location_master loc
		WHERE lm.line_id = @p_line_id
		AND   lm.region_id = rm.region_id
		AND   lm.country_id = cm.country_id
		and lm.location_id = loc.location_id;


 
	SET @l_err_code = 0;
	SET @l_err_message = 'Initializing';
	SELECT  @l_mail_to_list = distribution_list , @l_send_note_to_owner = 'Y'
	FROM   mh_lpa_line_master
	WHERE line_id = @l_line_id;


	IF ISNULL(@l_send_note_to_owner,'N') = 'Y' 
	BEGIN
		SELECT @l_invite_to_list = email_id
		FROM   mh_lpa_user_master
		WHERE  user_id = @p_audited_by_user_id;

		IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
			SET  @l_mail_to_list =  @l_mail_to_list+' ; '+@l_invite_to_list;
	END;


	BEGIN TRANSACTION New_audit
--  SELECT @l_audit_id = ISNULL(MAX(audit_id),1)+1  
--  FROM   mh_lpa_local_answers;
		--SELECT @l_audit_id = NEXT VALUE FOR dbo.audit_id_s;
		SELECT @l_audit_id =@p_audit_id
		delete from temp_AnsResultsTableType where audit_id = @l_audit_id;
  
		insert into temp_AnsResultsTableType(audit_id,question_id, answer, remarks, image_file_name, location_id,  audit_date)
		select  @l_audit_id, question_id, answer, remarks, image_file_name, @l_location_id, @l_audit_date
		from @p_AuditResults;
		SELECT @l_count = count(*)
		FROM   temp_AnsResultsTableType
		WHERE  question_id < 3
		AND    audit_id = @l_audit_id
		AND    location_id = @l_location_id;
  
		IF @l_count < 3 
  
			INSERT INTO mh_lpa_local_answers
				(audit_id,
				audit_plan_id,
				audited_by_user_id,
				region_id,
				country_id,
				location_id,
				line_id,
				audit_date,
				Shift_No,
				question_id,
				section_id,
				answer,
				remarks,
				image_file_name)
			SELECT @l_audit_id,
				@p_audit_plan_id,
				@p_audited_by_user_id,
				@l_region_id,
				@l_country_id,
				@l_location_id,
				@l_line_id,
				@l_audit_date,
				@p_shift_no,
				q.question_id,   q.section_id,
				answer, remarks, image_file_name
			FROM temp_AnsResultsTableType r,
				mh_lpa_local_questions q
			WHERE q.question_id = r.question_id
			AND   q.location_id = @l_location_id
			AND   q.location_id = r.location_id
			AND   r.audit_id = @l_audit_id;
		ELSE
			INSERT INTO mh_lpa_local_answers
				(audit_id,
				audit_plan_id,
				audited_by_user_id,
				region_id,
				country_id,
				location_id,
				line_id,
				audit_date,
				Shift_No,
				question_id,
				section_id,
				answer,
				remarks,
				image_file_name)
			SELECT @l_audit_id,
				@p_audit_plan_id,
				@p_audited_by_user_id,
				@l_region_id,
				@l_country_id,
				@l_location_id,
				@l_line_id,
				@l_audit_date,
				@p_shift_no,
				q.question_id,
				q.section_id,
				r.question_id, remarks, image_file_name
			FROM temp_AnsResultsTableType r,
				mh_lpa_local_questions q
			WHERE q.question_id = r.answer
			AND   q.location_id = @l_location_id
			AND   q.location_id = r.location_id
			AND   r.audit_id = @l_audit_id;  
  
	COMMIT TRANSACTION New_audit;


	DECLARE @return_value int
	EXEC @return_value = [dbo].[insScoreSummary]
		@p_audit_id = @l_audit_id;

	SELECT @l_result_count = count(*)
	FROM   mh_lpa_local_answers
	WHERE  audit_id = @l_audit_id;

	IF @l_result_count > 20
	BEGIN
		SET @l_err_code = 0;
		SET @l_err_message = 'Inserted Successfully';
	END;
	ELSE
	BEGIN
		SET @l_err_code = 401;
		SET @l_err_message = 'Unable to insert Audit Data - check log for errors';
	END;


	SELECT *  FROM (SELECT @l_err_code err_code, @l_err_message err_message, @l_audit_id audit_id) a FOR JSON AUTO

	Select @l_mail_to_list  mail_to_list, @l_audit_id audit_id;

	SELECT *
	FROM temp_AnsResultsTableType r
	WHERE r.location_id = @l_location_id
	AND   r.audit_id = @l_audit_id;  
 
END




GO
/****** Object:  StoredProcedure [dbo].[InsAuditResults_New]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[InsAuditResults_New](@p_audit_id int,
@p_audit_plan_id   int,
		@p_line_id			int,
		@p_audited_by_user_id  int,
		--@p_audit_date		datetime,
		@p_shift_no			int,
		@p_AuditResults		AnsResultsTableType READONLY
        )
AS
 SET NOCOUNT ON;
 
BEGIN

	EXEC [dbo].[InsAuditResults_date] 
			@p_audit_id,
			@p_audit_plan_id,
			@p_line_id,
			@p_audited_by_user_id,
			NULL,		-- @p_audit_date		datetime,
			@p_shift_no,
			@p_AuditResults ;
			
END




GO
/****** Object:  StoredProcedure [dbo].[InsAuditReviews]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DROP PROCEDURE [dbo].[InsAuditReviews]
 CREATE PROCEDURE [dbo].[InsAuditReviews](@p_audit_id				int,
								@p_reviewed_by_user_id		int
								-- @p_AuditReview				AnsReviewsTableType READONLY
								)
AS
	SET NOCOUNT ON;
	
BEGIN
	DECLARE @l_err_code		int;
	DECLARE @l_err_message		varchar(100);
	
	SET @l_err_code = 0;
	SET @l_err_message = 'Initializing';

	
	UPDATE mh_lpa_local_answers 
	SET review_user_id = 17,
		review_closed_on = CONVERT(DATE,p.review_closed_on,21),
		review_closed_status = p.review_closed_status,
		review_comments	= p.review_comments,
		review_image_file_name = p.review_image_file_name
	FROM [temp_AnsReviewsTableType] p
	WHERE  mh_lpa_local_answers.loc_answer_id = p.loc_answer_id;

	DELETE FROM [temp_AnsReviewsTableType] WHERE audit_id = @p_audit_id;
			
	SET @l_err_code = 0;
	SET @l_err_message = 'Inserted Successfully';

	SELECT * FROM (SELECT @l_err_code err_code, @l_err_message err_message) a FOR JSON AUTO

END





GO
/****** Object:  StoredProcedure [dbo].[InsCountry]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsCountry](		@p_country_id			int,
								@p_country_code			VARCHAR(10),
								@p_country_name			VARCHAR(100)
								)
AS
	SET NOCOUNT ON;
	
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_country_id		INT;
	DECLARE @l_location_id		INT;
	DECLARE @l_line_id			INT;
	DECLARE @l_product_id		INT;
	
	
	IF @p_country_id IS NOT NULL AND @p_country_id != 0
			UPDATE mh_lpa_country_master
			SET country_code = @p_country_code,
				country_name = @p_country_name
			WHERE country_id = @p_country_id;
	ELSE
		BEGIN	
			SELECT @l_count = count(*)  
			FROM   mh_lpa_country_master
			WHERE  UPPER(ISNULL(country_code,'*')) = UPPER(ISNULL(@p_country_code,'*'))
			AND    UPPER(country_name) = UPPER(@p_country_name);
	
		IF @l_count = 0 
			BEGIN
				IF @p_country_name IS NOT NULL AND @p_country_name != '' 
					INSERT INTO mh_lpa_country_master (country_code, country_name) VALUES (@p_country_code, @p_country_name);
			END;
		END;

	
END





GO
/****** Object:  StoredProcedure [dbo].[InsDistList]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsDistList](@p_location_id			int=null,
								@p_list_type				VARCHAR(100),
								@p_email_list				VARCHAR(max),
								@p_send_to_owner			VARCHAR(1)=null,
								@l_err_code				int OUTPUT,
								@l_err_message			varchar(100) OUTPUT
								)
AS
	SET NOCOUNT ON;
	
BEGIN
	DECLARE @l_count 			INT;

	SET @l_err_code = 0;
	SET @l_err_message = 'Initializing';

	IF @p_location_id IS NOT NULL AND @p_list_type IN ('At Audit Plan','At Audit Completion','At New Line Addition')
	BEGIN
			UPDATE mh_lpa_distribution_list
			SET 
				email_list = @p_email_list,
				send_notification_to_owner = @p_send_to_owner
			WHERE location_id = @p_location_id
			AND   UPPER(@p_list_type) = UPPER(list_type);

			SET @l_err_code = 0;
			SET @l_err_message = 'Updated Successfully';
			
		END;
	ELSE
		BEGIN	
			SET @l_err_code = 100;
			SET @l_err_message = 'Unable to find List';
		END;

	
END




GO
/****** Object:  StoredProcedure [dbo].[InsGroup]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsGroup](		@p_group_id			int,
					@p_location_name		VARCHAR(100),
					@p_group_name			VARCHAR(100)
								)
AS
	SET NOCOUNT ON;
	
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_group_id		INT;
	DECLARE @l_location_id		INT;
	
	EXEC getLocationId NULL, @p_location_name, @l_location_id = @l_location_id OUTPUT;
	
	IF @p_group_id IS NOT NULL AND @p_group_id != 0 
			UPDATE mh_lpa_group_master
			SET  location_id = @l_location_id,
				group_name = @p_group_name
			WHERE group_id = @p_group_id;
	ELSE
		BEGIN	
			SELECT @l_count = count(*)  
			FROM   mh_lpa_group_master
			WHERE  UPPER(ISNULL(group_name,'*')) = UPPER(ISNULL(@p_group_name,'*'))
			AND    location_id = @l_location_id;
	
		IF @l_count = 0 
			BEGIN
				IF @p_group_name IS NOT NULL AND @p_group_name != '' 
					INSERT INTO mh_lpa_group_master (location_id, group_name) VALUES (@l_location_id, @p_group_name);
			END;
		END;

	
END






GO
/****** Object:  StoredProcedure [dbo].[InsGroupMembers]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsGroupMembers](@p_location_name		VARCHAR(100),
								@p_group_name			VARCHAR(100),
								@p_group_members		GroupMembersTableType READONLY
								)
AS
	SET NOCOUNT ON;
	
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_group_id		INT;
	DECLARE @l_location_id		INT;
	
	EXEC getLocationId NULL, @p_location_name, @l_location_id = @l_location_id OUTPUT;
	EXEC getGroupId @p_location_name, @p_group_name, @l_group_id = @l_group_id OUTPUT;

	DELETE FROM mh_lpa_group_members
	WHERE  location_id = @l_location_id
	AND    group_id = @l_group_id;

	INSERT INTO mh_lpa_group_members(
		location_id,
		group_id,
		user_id)
	SELECT @l_location_id, @l_group_id, user_id
	FROM   @p_group_members;
	
END






GO
/****** Object:  StoredProcedure [dbo].[InsLine]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsLine](  @p_line_id    int,
        @p_region_name   VARCHAR(100),
        @p_country_name   VARCHAR(100),
        @p_location_name  VARCHAR(100),
        @p_line_code   varchar(100)= NULL,
        @p_line_name   VARCHAR(100),
        @p_distribution_list   VARCHAR(max),
        @l_err_code    int OUTPUT,
        @l_err_message   varchar(100) OUTPUT
        )
AS
 SET NOCOUNT ON;
 
BEGIN
 DECLARE @l_count    INT;
 DECLARE @l_region_id  INT;
 DECLARE @l_country_id  INT;
 DECLARE @l_location_id  INT;
 DECLARE @l_line_id   INT;
 
 EXEC getRegionId  NULL, @p_region_name, @l_region_id = @l_region_id OUTPUT;
 EXEC getCountryId  NULL, @p_country_name, @l_country_id = @l_country_id OUTPUT;
 EXEC getLocationId NULL, @p_location_name, @l_location_id = @l_location_id OUTPUT;
 SET @l_err_code = 0;
 SET @l_err_message = 'Initializing';
/*
 SELECT  @l_mail_to_list = email_list
 FROM   mh_lpa_distribution_list
 WHERE location_id = 0
 AND   UPPER(list_type) = UPPER('At New Line Addition');
*/
 IF @p_line_id IS NOT NULL AND @p_line_id != 0
 BEGIN
  UPDATE mh_lpa_line_master
  SET    location_id = @l_location_id,
    country_id = @l_country_id,
    region_id = @l_region_id,
    line_code = @p_line_code,
    line_name = @p_line_name,
    distribution_list = @p_distribution_list
  WHERE   line_id = @p_line_id;
  SET @l_err_code = 0;
   SET @l_err_message = 'Updated Successfully';
   
 END 
 ELSE
  BEGIN
 
   SELECT @l_count = count(*)  
   FROM   mh_lpa_line_master
   WHERE  UPPER(line_name) = UPPER(@p_line_name)
   AND    location_id = @l_location_id
   AND    country_id = @l_country_id
   AND    region_id = @l_region_id;
 
   IF @l_count = 0 AND @p_line_name IS NOT NULL AND @p_line_name != ''
   BEGIN
    INSERT INTO mh_lpa_line_master (
      location_id,
      country_id,
      region_id,
      line_code,
      line_name,
      distribution_list
     ) VALUES (
      @l_location_id,
      @l_country_id,
      @l_region_id,
      @p_line_code,
      @p_line_name,
      @p_distribution_list
     );
    SET @l_err_code = 0;
    SET @l_err_message = 'Inserted Successfully';
   END
   ELSE
   BEGIN
    SELECT @l_count = count(*)  
    FROM   mh_lpa_line_master
    WHERE  UPPER(line_name) = UPPER(@p_line_name)
    AND    location_id = @l_location_id
    AND    country_id = @l_country_id
    AND    region_id = @l_region_id
    AND    end_date IS NOT NULL;
    IF @l_count > 0
     UPDATE mh_lpa_line_master
     SET    end_date = NULL, 
       distribution_list = @p_distribution_list
     WHERE  UPPER(line_name) = UPPER(@p_line_name)
     AND    location_id = @l_location_id
     AND    country_id = @l_country_id
     AND    region_id = @l_region_id
     AND    end_date IS NOT NULL;

   END;
  END;
 
END



GO
/****** Object:  StoredProcedure [dbo].[InsLocalQuestion]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsLocalQuestion]( @p_local_question_id int,
        @p_location_name  VARCHAR(100),
        @p_section_name   VARCHAR(100),
        @p_question    VARCHAR(1000),
		@p_na_flag				VARCHAR(1),
        @p_help_text   VARCHAR(1000),
        @p_how_to_check   VARCHAR(1000),
        @p_display_seq   int,
        @l_err_code    int OUTPUT,
        @l_err_message   varchar(100) OUTPUT
        )
AS
 SET NOCOUNT ON;
 
BEGIN
 DECLARE @l_count    INT;
 DECLARE @l_section_id   INT;
 DECLARE @l_location_id   INT;
 DECLARE @l_old_seq_no  INT;
 DECLARE @l_old_seq_no_lq  INT;
 DECLARE @l_local_question_id INT;
 
 SET @l_location_id=Convert(INT,@p_location_name)
  
 EXEC getSectionId  @p_section_name, @l_section_id = @l_section_id OUTPUT;
 --EXEC getLocationId NULL, @p_location_name, @l_location_id = @l_location_id OUTPUT;
IF @l_section_id IS NULL 
	BEGIN
		SET @l_err_code = 2001;
		SET @l_err_message = 'Invalid Section';
	END;
ELSE
	BEGIN

		IF @p_local_question_id IS NOT NULL AND @p_local_question_id != 0		-- its an update of an exising question
			BEGIN
				SET @l_local_question_id = @p_local_question_id

				/*
				 * Get the old display sequence from  local_question_master 
				 */
				IF @p_local_question_id > 10000
					SELECT @l_old_seq_no = display_sequence
					FROM   mh_lpa_local_question_master
					WHERE local_question_id = @p_local_question_id
					AND   location_id = @l_location_id;
				ELSE
					SET @l_old_seq_no = @p_display_seq;

				/*
				 * Get the old display sequence from  local_questions 
				 */
				SELECT @l_old_seq_no_lq = display_sequence
				FROM   mh_lpa_local_questions
				WHERE  question_id = @p_local_question_id
				AND    location_id = @l_location_id;

				/* update all the changes in the local_question_master */
				UPDATE mh_lpa_local_question_master
				SET display_sequence = @p_display_seq,
					local_question = @p_question,
					local_help_text = @p_help_text,
					how_to_check = @p_how_to_check,
					section_id = @l_section_id,
					na_flag = @p_na_flag
				WHERE local_question_id = @p_local_question_id
				AND   location_id = @l_location_id;
				
				/* update the display sequence in the local_questions */
				UPDATE mh_lpa_local_questions
				SET display_sequence = @p_display_seq,
					section_id = @l_section_id
				WHERE question_id = @p_local_question_id
				AND   location_id = @l_location_id;
			END;
		ELSE	-- IF @p_local_question_id IS NOT NULL AND @p_local_question_id != 0 (Inserting a New Local Question)
			BEGIN
				SELECT @l_count = count(*)  
				FROM   mh_lpa_local_question_master
				WHERE  UPPER(local_question) = UPPER(@p_question);
	   
				IF @l_count = 0 
				BEGIN
					SET @l_old_seq_no_lq = @p_display_seq;
					INSERT INTO mh_lpa_local_question_master (
						location_id,
						section_id,
						local_question,
						na_flag,
						local_help_text,
						how_to_check,
						display_sequence
					) VALUES (
						@l_location_id,
						@l_section_id,
						@p_question,
						@p_na_flag,
						@p_help_text,
						@p_how_to_check,
						@p_display_seq
					);
						
					SELECT @l_local_question_id = local_question_id
					FROM   mh_lpa_local_question_master
					WHERE  location_id = @l_location_id
					AND    section_id = @l_section_id
					AND    UPPER(local_question) = UPPER(@p_question);

					UPDATE mh_lpa_local_questions
					SET    display_sequence = display_sequence+1
					WHERE  display_sequence >= @p_display_seq
					AND    location_id = @l_location_id;

					INSERT INTO mh_lpa_local_questions(
						question_id,
						location_id,
						section_id,
						display_sequence
					)VALUES (
						@l_local_question_id,
						@l_location_id,
						@l_section_id,
						@p_display_seq
					);
					
	       		END;	-- IF @l_count = 0 
	
			END;	-- IF @p_local_question_id IS NOT NULL AND @p_local_question_id != 0

		/*
		 * Insert or update is done in the local_question_master and local_questions
		 * Now we need to adjust the display sequence for
		 *			Existing questions
		 */
			 
		IF @l_old_seq_no < @p_display_seq
			UPDATE mh_lpa_local_question_master
			SET    display_sequence = display_sequence-1
			WHERE  local_question_id != @l_local_question_id
			AND    location_id = @l_location_id
			AND    display_sequence BETWEEN @l_old_seq_no AND @p_display_seq;
		ELSE
			IF @l_old_seq_no > @p_display_seq
				UPDATE mh_lpa_local_question_master
				SET    display_sequence = display_sequence+1
				WHERE  local_question_id != @l_local_question_id
				AND    location_id = @l_location_id
				AND    display_sequence BETWEEN @p_display_seq AND @l_old_seq_no ;
			 

		IF @l_old_seq_no_lq < @p_display_seq
			UPDATE mh_lpa_local_questions
			SET    display_sequence = display_sequence-1
			WHERE  question_id != @l_local_question_id
			AND    location_id = @l_location_id
			AND    display_sequence BETWEEN @l_old_seq_no_lq AND @p_display_seq;
		ELSE
			IF @l_old_seq_no_lq > @p_display_seq
				UPDATE mh_lpa_local_questions
				SET    display_sequence = display_sequence+1
				WHERE  question_id != @l_local_question_id
				AND    location_id = @l_location_id
				AND    display_sequence BETWEEN  @p_display_seq AND @l_old_seq_no_lq ;

		SET @l_err_code = 0;
		SET @l_err_message = 'Question saved Successfully';

	END;
END;




GO
/****** Object:  StoredProcedure [dbo].[InsLocation]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsLocation](@p_location_id			int=null,
								@p_region_name				VARCHAR(100),
								@p_country_name				VARCHAR(100),
								@p_location_code			VARCHAR(10)=null,
								@p_location_name			VARCHAR(100),
								@p_timezone_code			VARCHAR(100),
								@p_timezone_desc			VARCHAR(100),
								@p_no_of_shifts				int,
								@p_shift1_st_time			time(0),
								@p_shift1_end_time			time(0),
								@p_shift2_st_time			time(0),
								@p_shift2_end_time			time(0),
								@p_shift3_st_time			time(0),
								@p_shift3_end_time			time(0),
								@l_err_code				int OUTPUT,
								@l_err_message			varchar(100) OUTPUT
								)
AS
	SET NOCOUNT ON;
	
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_location_id		INT;
	DECLARE @l_country_id		INT;
	DECLARE @l_region_id		INT;
	DECLARE @l_line_id			INT;
	DECLARE @l_product_id		INT;
	

	EXEC getRegionId  NULL, @p_region_name, @l_region_id = @l_region_id OUTPUT;
	EXEC getCountryId  NULL, @p_country_name, @l_country_id = @l_country_id OUTPUT;

	SET @l_err_code = 0;
	SET @l_err_message = 'Initializing';

	IF @p_location_id IS NOT NULL AND @p_location_id != 0 
	BEGIN
			UPDATE mh_lpa_location_master
			SET 
				region_id = @l_region_id,
				country_id = @l_country_id,
				location_code = @p_location_code,
				location_name = @p_location_name,
				timezone_code = @p_timezone_code,
				timezone_desc = @p_timezone_desc,
				no_of_shifts = @p_no_of_shifts,
				/*
				shift1_start_time = CAST((FORMAT(@p_shift1_st_time,'hh:mm') + ':00 ' +  @p_timezone_code) as datetimeoffset),
				shift1_end_time = CAST(( FORMAT(@p_shift1_end_time,'hh:mm') + ':00 ' +  @p_timezone_code) as datetimeoffset),
				shift2_start_time = CAST(( FORMAT(@p_shift2_st_time,'hh:mm') + ':00 ' +  @p_timezone_code) as datetimeoffset),
				shift2_end_time = CAST(( FORMAT(@p_shift2_end_time,'hh:mm') + ':00 ' +  @p_timezone_code) as datetimeoffset),
				shift3_start_time = CAST(( FORMAT(@p_shift3_st_time,'hh:mm') + ':00 ' +  @p_timezone_code) as datetimeoffset),
				shift3_end_time = CAST(( FORMAT(@p_shift3_end_time,'hh:mm') + ':00 ' +  @p_timezone_code) as datetimeoffset)
				*/
				shift1_start_time = CAST(@p_shift1_st_time as datetimeoffset),
				shift1_end_time = CAST(@p_shift1_end_time as datetimeoffset),
				shift2_start_time = CAST(@p_shift2_st_time as datetimeoffset),
				shift2_end_time = CAST(@p_shift2_end_time as datetimeoffset),
				shift3_start_time = CAST(@p_shift3_st_time as datetimeoffset),
				shift3_end_time = CAST(@p_shift3_end_time as datetimeoffset)
				WHERE location_id = @p_location_id;

			SET @l_err_code = 0;
			SET @l_err_message = 'Updated Successfully';
			
		END;
	ELSE
		BEGIN	
			SELECT @l_count = count(*)  
			FROM   mh_lpa_location_master
			WHERE  UPPER(ISNULL(location_code,'*')) = UPPER(ISNULL(@p_location_code,'*'))
			AND    UPPER(location_name) = UPPER(@p_location_name)
			AND    region_id = @l_region_id
			AND    country_id = @l_country_id;
	
		IF @l_count = 0 
			BEGIN
				IF @p_location_name IS NOT NULL AND @p_location_name != '' 
					BEGIN
						INSERT INTO mh_lpa_location_master (
							region_id,
							country_id,
							location_code, 
							location_name,
							timezone_code,
							timezone_desc, 
							no_of_shifts,
							shift1_start_time,
							shift1_end_time,
							shift2_start_time,
							shift2_end_time,
							shift3_start_time,
							shift3_end_time
						) VALUES (
							@l_region_id,
							@l_country_id,
							@p_location_code, 
							@p_location_name,
							@p_timezone_code,
							@p_timezone_desc,
							@p_no_of_shifts,
							CAST(@p_shift1_st_time as datetimeoffset),
							CAST(@p_shift1_end_time as datetimeoffset),
							CAST(@p_shift2_st_time as datetimeoffset),
							CAST(@p_shift2_end_time as datetimeoffset),
							CAST(@p_shift3_st_time as datetimeoffset),
							CAST(@p_shift3_end_time as datetimeoffset)
							/*
							CAST(FORMAT(@p_shift1_st_time,'hh:mm') + ':00 ' +  @p_timezone_code as datetimeoffset),
							CAST(FORMAT(@p_shift1_end_time,'hh:mm') + ':00 ' +  @p_timezone_code as datetimeoffset),
							CAST(FORMAT(@p_shift2_st_time,'hh:mm') + ':00 ' +  @p_timezone_code as datetimeoffset),
							CAST(FORMAT(@p_shift2_end_time,'hh:mm') + ':00 ' +  @p_timezone_code as datetimeoffset),
							CAST(FORMAT(@p_shift3_st_time,'hh:mm') + ':00 ' +  @p_timezone_code as datetimeoffset),
							CAST(FORMAT(@p_shift3_end_time,'hh:mm') + ':00 ' +  @p_timezone_code as datetimeoffset)
							*/
						);

						SELECT @l_location_id = location_id
						FROM   mh_lpa_location_master
						WHERE  location_name = @p_location_name;

						insert into mh_lpa_local_questions
						select question_id, @l_location_id, section_id, display_sequence
						from mh_lpa_question_master	

/*	
						DELETE FROM mh_lpa_distribution_list
						WHERE  location_id = @l_location_id;

						INSERT INTO mh_lpa_distribution_list (location_id,list_type) VALUES (@l_location_id, 'At Audit Plan');
						INSERT INTO mh_lpa_distribution_list (location_id,list_type) VALUES (@l_location_id, 'At Audit Completion');
*/
						SET @l_err_code = 0;
			SET @l_err_message = 'Inserted Successfully';
					END;
			END;
		END;

	
END




GO
/****** Object:  StoredProcedure [dbo].[InsProfileChangeReq]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsProfileChangeReq](@p_user_id				int,
								@p_username				VARCHAR(100),
								@p_comments				VARCHAR(100)							
								)
AS
	SET NOCOUNT ON;
	
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_user_id			INT;
	DECLARE @l_err_code			INT;
	DECLARE @l_err_message		VARCHAR(100);
	
	SET @l_err_code = 0;
	SET @l_err_message = 'Initializing';
	
	IF @p_user_id IS NOT NULL AND @p_user_id != 0
		BEGIN
			INSERT INTO mh_lpa_request_profile_changes (user_id, username, request_date, comments ) VALUES 
						(@p_user_id, @p_username, CURRENT_TIMESTAMP, @p_comments);
			SET @l_err_code = 0;
			SET @l_err_message = 'Inserted Successfully';
		END;
	ELSE
		BEGIN

			SELECT @l_user_id = user_id
			FROM   mh_lpa_user_master
			WHERE  UPPER(Username) = UPPER(@p_username);

			IF @l_user_id IS NOT NULL AND @l_user_id != 0
			BEGIN
				INSERT INTO mh_lpa_request_profile_changes (user_id, username, request_date, comments ) VALUES 
							(@l_user_id, @p_username, CURRENT_TIMESTAMP, @p_comments);
				SET @l_err_code = 0;
				SET @l_err_message = 'Inserted Successfully';
			END;

		END;

	SELECT * FROM (SELECT @l_err_code err_code, @l_err_message err_message) a FOR JSON AUTO;

	SELECT [dbo].[get_local_admin_email](u.user_id) to_email,
    		u.email_id from_email,
    		'New Reqest for Profile Change' Sub,
    		'User: '+u.emp_full_name+' reqests changes to his/her profile: '+@p_comments content
	FROM mh_lpa_user_master u
	WHERE   u.user_id = @l_user_id;

	UPDATE mh_lpa_request_profile_changes
	SET    notification_sent_flag = 1
	WHERE  notification_sent_flag IS NULL 
	AND    user_id = @l_user_id
	AND    comments = @p_comments;
  
END





GO
/****** Object:  StoredProcedure [dbo].[InsQuestion]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsQuestion](	@p_question_id			int,
								@p_section_name			VARCHAR(100),
								@p_question				VARCHAR(1000),
								@p_na_flag				VARCHAR(1),
								@p_help_text			VARCHAR(1000),
								@p_how_to_check			VARCHAR(1000),
								@p_display_seq			int,
								@p_location_id			int,
								@l_err_code				int OUTPUT,
								@l_err_message			varchar(100) OUTPUT
								)
AS
	SET NOCOUNT ON;
	
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_section_id 		INT;
	DECLARE @l_question_id		INT;
	DECLARE @l_old_seq_no		INT;
	DECLARE @l_old_seq_no_lq		INT;
		
	EXEC getSectionId  @p_section_name, @l_section_id = @l_section_id OUTPUT;

	IF @l_section_id IS NULL 
		BEGIN
			SET @l_err_code = 2001;
			SET @l_err_message = 'Invalid Section';
		END;
	ELSE
		BEGIN

			/* havent done any validation for uniqueness of the display sequence */
			
			IF @p_question_id IS NOT NULL AND @p_question_id != 0 
			BEGIN

				SELECT @l_old_seq_no = display_sequence
				FROM   mh_lpa_question_master
				WHERE question_id = @p_question_id;
				
				IF @l_old_seq_no < @p_display_seq
					UPDATE mh_lpa_question_master
					SET    display_sequence = display_sequence-1
					WHERE  question_id != @p_question_id
					AND    display_sequence BETWEEN @l_old_seq_no AND @p_display_seq;
				ELSE
					IF @l_old_seq_no > @p_display_seq
						UPDATE mh_lpa_question_master
						SET    display_sequence = display_sequence+1
						WHERE  question_id != @p_question_id
						AND    display_sequence BETWEEN @p_display_seq AND @l_old_seq_no ;
								
				UPDATE mh_lpa_question_master
				SET display_sequence = @p_display_seq,
					question = @p_question,
					help_text = @p_help_text,
					how_to_check = @p_how_to_check,
					section_id = @l_section_id,
					na_flag = @p_na_flag
				WHERE question_id = @p_question_id;
				
				SET @l_err_code = 0;
				SET @l_err_message = 'Updated Successfully';
				
			END;
			ELSE
				BEGIN	
					SELECT @l_count = count(*)  
					FROM   mh_lpa_question_master
					WHERE  UPPER(question) = UPPER(@p_question);
			
				IF @l_count = 0 
					IF @p_question IS NOT NULL AND @p_question != '' 
					BEGIN
						INSERT INTO mh_lpa_question_master (
							section_id,
							question,
							na_flag,
							help_text,
							how_to_check,
							display_sequence
						) VALUES (
							@l_section_id,
							@p_question,
							@p_na_flag,
							@p_help_text,
							@p_how_to_check,
							@p_display_seq
						);

						SELECT @l_question_id = question_id
						FROM   mh_lpa_question_master
						WHERE  UPPER(question) = UPPER(@p_question);

						INSERT INTO mh_lpa_local_questions(
							question_id,
							location_id,
							section_id,
							display_sequence
						)
						SELECT @l_question_id, location_id, @l_section_id, MAX(display_sequence)+1
						FROM   mh_lpa_local_questions
						GROUP BY location_id;


						SET @l_err_code = 0;
						SET @l_err_message = 'Inserted Successfully';
					END;
				END;
		END;
	
END





GO
/****** Object:  StoredProcedure [dbo].[InsRegion]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsRegion](		@p_region_id			int,
								@p_region_code			VARCHAR(10),
								@p_region_name			VARCHAR(100)
								)
AS
	SET NOCOUNT ON;
	
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_region_id		INT;
	DECLARE @l_country_id		INT;
	DECLARE @l_location_id		INT;
	DECLARE @l_line_id			INT;
	DECLARE @l_product_id		INT;
	
	
	IF @p_region_id IS NOT NULL AND @p_region_id != 0 
			UPDATE mh_lpa_region_master
			SET region_code = @p_region_code,
				region_name = @p_region_name
			WHERE region_id = @p_region_id;
	ELSE
		BEGIN	
			SELECT @l_count = count(*)  
			FROM   mh_lpa_region_master
			WHERE  UPPER(ISNULL(region_code,'*')) = UPPER(ISNULL(@p_region_code,'*'))
			AND    UPPER(region_name) = UPPER(@p_region_name);
	
		IF @l_count = 0 
			BEGIN
				IF @p_region_name IS NOT NULL AND @p_region_name != '' 
					INSERT INTO mh_lpa_region_master (region_code, region_name) VALUES (@p_region_code, @p_region_name);
			END;
		END;

	
END





GO
/****** Object:  StoredProcedure [dbo].[InsRole]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsRole](		@p_role_id				int,
								@p_rolename				VARCHAR(100),
								@p_no_of_audits_req		int,
								@p_frequency			VARCHAR(30),
								@l_err_code				int OUTPUT,
								@l_err_message			varchar(100) OUTPUT								
								)
AS
	SET NOCOUNT ON;
	
BEGIN
	DECLARE @l_count 			INT;
	
	SET @l_err_code = 0;
	SET @l_err_message = 'Initializing';
	
	IF @p_role_id IS NOT NULL AND @p_role_id != 0
		BEGIN
			UPDATE mh_lpa_role_master
			SET rolename = @p_rolename,
				no_of_audits_required = @p_no_of_audits_req,
				frequency = @p_frequency
			WHERE role_id = @p_role_id;

			SET @l_err_code = 0;
			SET @l_err_message = 'Role Updated Successfully';

		END;

	ELSE
		BEGIN	
			SELECT @l_count = count(*)  
			FROM   mh_lpa_role_master
			WHERE  UPPER(ISNULL(rolename,'*')) = UPPER(ISNULL(@p_rolename,'*'));
	
		IF @l_count = 0 
			BEGIN
				IF @p_rolename IS NOT NULL AND @p_rolename != '' 
					BEGIN
					INSERT INTO mh_lpa_role_master (rolename, no_of_audits_required,frequency ) VALUES (@p_rolename, @p_no_of_audits_req, @p_frequency);
					SET @l_err_code = 0;
					SET @l_err_message = 'Role Created Successfully';
					END;
			END;
		ELSE
		BEGIN
			SET @l_err_code = 0;
			SET @l_err_message = 'No Changes in Role or Role Exists already';

		END;
		END;

	
END






GO
/****** Object:  StoredProcedure [dbo].[insScoreSummary]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- drop PROCEDURE insScoreSummary
-- delete from mh_lpa_score_summary
CREATE PROCEDURE [dbo].[insScoreSummary](@p_audit_id		INT=null)
AS
BEGIN

	DECLARE @l_yes_ans	INT;
	DECLARE @l_tot_ans	INT;
	DECLARE @l_audit_id	INT;

	IF @p_audit_id IS NOT NULL AND @p_audit_id != 0 
	BEGIN

		DELETE FROM mh_lpa_score_summary
		WHERE audit_id = @p_audit_id;

		INSERT INTO mh_lpa_score_summary
		(audit_id,
		audit_date,
		region_id,
		country_id,
		location_id,
		line_id,
		update_complete,
		sec1_yes_count,
		sec1_tot_count,
		sec2_yes_count,
		sec2_tot_count,
		sec3_yes_count,
		sec3_tot_count,
		sec4_yes_count,
		sec4_tot_count,
		sec5_yes_count,
		sec5_tot_count,
		sec6_yes_count,
		sec6_tot_count,
		total_yes,
		total_no,
		total_na
		) 
		SELECT audit_id,
				audit_date,
				la.region_id,
				la.country_id,
				la.location_id,
				lm.line_id,
				1,
				SUM(CASE 
					WHEN section_id = 1 and answer = 0 THEN 1		-- Answer is Yes
					ELSE 0
					END ) sec1_yes_count,
				SUM(CASE 
					WHEN section_id = 1 and answer IN (0,1) THEN 1		-- Count of Section 1
					ELSE 0
					END ) sec1_tot_count,
				SUM(CASE 
					WHEN section_id = 2 and answer = 0 THEN 1		-- Answer is Yes
					ELSE 0
					END ) sec2_yes_count,
				SUM(CASE 
					WHEN section_id = 2 and answer IN (0,1) THEN 1		-- Count of Section 1
					ELSE 0
					END ) sec2_tot_count,					
				SUM(CASE 
					WHEN section_id = 3 and answer = 0 THEN 1		-- Answer is Yes
					ELSE 0
					END ) sec3_yes_count,
				SUM(CASE 
					WHEN section_id = 3 and answer IN (0,1) THEN 1		-- Count of Section 1
					ELSE 0
					END ) sec3_tot_count,				
				SUM(CASE 
					WHEN section_id = 4 and answer = 0 THEN 1		-- Answer is Yes
					ELSE 0
					END ) sec4_yes_count,
				SUM(CASE 
					WHEN section_id = 4 and answer IN (0,1) THEN 1		-- Count of Section 1
					ELSE 0
					END ) sec4_tot_count,				
				SUM(CASE 
					WHEN section_id = 5 and answer = 0 THEN 1		-- Answer is Yes
					ELSE 0
					END ) sec5_yes_count,
				SUM(CASE 
					WHEN section_id = 5 and answer IN (0,1) THEN 1		-- Count of Section 1
					ELSE 0
					END ) sec5_tot_count,				
				SUM(CASE 
					WHEN section_id = 6 and answer = 0 THEN 1		-- Answer is Yes
					ELSE 0
					END ) sec6_yes_count,
				SUM(CASE 
					WHEN section_id = 6 and answer IN (0,1) THEN 1		-- Count of Section 1
					ELSE 0
					END ) sec6_tot_count,					
				SUM(CASE 
					WHEN answer = 2 THEN 0		-- Not applicable
					WHEN answer = 1 THEN 0		-- Answer is No
					ELSE 1
					END ) total_yes,
				SUM(CASE 
					WHEN answer = 2 THEN 0		-- Not applicable
					WHEN answer = 0 THEN 0		-- Answer is Yes
					ELSE 1
					END ) total_no,
				SUM(CASE 
					WHEN answer = 1 THEN 0		-- Answer is No
					WHEN answer = 0 THEN 0		-- Answer is Yes
					ELSE 1
					END ) total_na
		FROM   mh_lpa_local_answers la,
				mh_lpa_line_master lm
		WHERE  la.line_id = lm.line_id
		AND    la.audit_id = @p_audit_id
		GROUP BY audit_id,
				audit_date,
				la.region_id,
				la.country_id,
				la.location_id,
				lm.line_id;
	
	END;
	ELSE

		INSERT INTO mh_lpa_score_summary
		(audit_id,
		audit_date,
		region_id,
		country_id,
		location_id,
		line_id,
		update_complete,
		sec1_yes_count,
		sec1_tot_count,
		sec2_yes_count,
		sec2_tot_count,
		sec3_yes_count,
		sec3_tot_count,
		sec4_yes_count,
		sec4_tot_count,
		sec5_yes_count,
		sec5_tot_count,
		sec6_yes_count,
		sec6_tot_count,
		total_yes,
		total_no,
		total_na
		) 
		SELECT audit_id,
				audit_date,
				la.region_id,
				la.country_id,
				la.location_id,
				lm.line_id,
				1,
				SUM(CASE 
					WHEN section_id = 1 and answer = 0 THEN 1		-- Answer is Yes
					ELSE 0
					END ) sec1_yes_count,
				SUM(CASE 
					WHEN section_id = 1 and answer IN (0,1) THEN 1		-- Count of Section 1
					ELSE 0
					END ) sec1_tot_count,
				SUM(CASE 
					WHEN section_id = 2 and answer = 0 THEN 1		-- Answer is Yes
					ELSE 0
					END ) sec2_yes_count,
				SUM(CASE 
					WHEN section_id = 2 and answer IN (0,1) THEN 1		-- Count of Section 1
					ELSE 0
					END ) sec2_tot_count,					
				SUM(CASE 
					WHEN section_id = 3 and answer = 0 THEN 1		-- Answer is Yes
					ELSE 0
					END ) sec3_yes_count,
				SUM(CASE 
					WHEN section_id = 3 and answer IN (0,1) THEN 1		-- Count of Section 1
					ELSE 0
					END ) sec3_tot_count,				
				SUM(CASE 
					WHEN section_id = 4 and answer = 0 THEN 1		-- Answer is Yes
					ELSE 0
					END ) sec4_yes_count,
				SUM(CASE 
					WHEN section_id = 4 and answer IN (0,1) THEN 1		-- Count of Section 1
					ELSE 0
					END ) sec4_tot_count,				
				SUM(CASE 
					WHEN section_id = 5 and answer = 0 THEN 1		-- Answer is Yes
					ELSE 0
					END ) sec5_yes_count,
				SUM(CASE 
					WHEN section_id = 5 and answer IN (0,1) THEN 1		-- Count of Section 1
					ELSE 0
					END ) sec5_tot_count,				
				SUM(CASE 
					WHEN section_id = 6 and answer = 0 THEN 1		-- Answer is Yes
					ELSE 0
					END ) sec6_yes_count,
				SUM(CASE 
					WHEN section_id = 6 and answer IN (0,1) THEN 1		-- Count of Section 1
					ELSE 0
					END ) sec6_tot_count,					
				SUM(CASE 
					WHEN answer = 2 THEN 0		-- Not applicable
					WHEN answer = 1 THEN 0		-- Answer is No
					ELSE 1
					END ) total_yes,
				SUM(CASE 
					WHEN answer = 2 THEN 0		-- Not applicable
					WHEN answer = 0 THEN 0		-- Answer is Yes
					ELSE 1
					END ) total_no,
				SUM(CASE 
					WHEN answer = 1 THEN 0		-- Answer is No
					WHEN answer = 0 THEN 0		-- Answer is Yes
					ELSE 1
					END ) total_na
		FROM   mh_lpa_local_answers la,
				mh_lpa_line_master lm
		WHERE  la.line_id = lm.line_id
		AND    la.audit_id	NOT IN (select audit_id from mh_lpa_score_summary)
		GROUP BY audit_id,
				audit_date,
				la.region_id,
				la.country_id,
				la.location_id,
				lm.line_id;

				
		UPDATE mh_lpa_score_summary 
		SET q1_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 1 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;

		UPDATE mh_lpa_score_summary 
		SET q2_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 2 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q3_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 3 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q4_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 4 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q5_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 5 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q6_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 6 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q7_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 7 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q8_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 8 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q9_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 9 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q10_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 10 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		

		
		UPDATE mh_lpa_score_summary 
		SET q11_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 11 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;

		UPDATE mh_lpa_score_summary 
		SET q12_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 12 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q13_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 13 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q14_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 14 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q15_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 15 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q16_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 16 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q17_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 17 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q18_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 18 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q19_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 19 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q20_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 20 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;


		UPDATE mh_lpa_score_summary 
		SET q21_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 21 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q22_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 22 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q23_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 23 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q24_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 24 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		UPDATE mh_lpa_score_summary 
		SET q25_answer = la.answer 
		FROM mh_lpa_local_answers la 
		WHERE la.audit_id = mh_lpa_score_summary.audit_id 
		AND question_id = 25 
		AND ISNULL(mh_lpa_score_summary.update_complete,1) = 1;
		
		
		UPDATE mh_lpa_score_summary
		SET    sec1_score	= CAST((CAST(sec1_yes_count as float)/cast(sec1_tot_count as float)) as numeric(10,2))
		WHERE  ISNULL(update_complete,1) = 1
		AND    sec1_tot_count > 0;

		UPDATE mh_lpa_score_summary
		SET    sec2_score	= CAST((CAST(sec2_yes_count as float)/cast(sec2_tot_count as float)) as numeric(10,2))
		WHERE  ISNULL(update_complete,1) = 1
		AND    sec2_tot_count > 0 ;

		UPDATE mh_lpa_score_summary
		SET    sec3_score	= CAST((CAST(sec3_yes_count as float)/cast(sec3_tot_count as float)) as numeric(10,2))
		WHERE  ISNULL(update_complete,1) = 1
		AND    sec3_tot_count > 0;

		UPDATE mh_lpa_score_summary
		SET    sec4_score	= CAST((CAST(sec4_yes_count as float)/cast(sec4_tot_count as float)) as numeric(10,2))
		WHERE  ISNULL(update_complete,1) = 1
		AND    sec4_tot_count > 0;

		UPDATE mh_lpa_score_summary
		SET    sec5_score	= CAST((CAST(sec5_yes_count as float)/cast(sec5_tot_count as float)) as numeric(10,2))
		WHERE  ISNULL(update_complete,1) = 1
		AND    sec5_tot_count > 0;


		UPDATE mh_lpa_score_summary
		SET    sec6_score	= CAST((CAST(sec6_yes_count as float)/cast(sec6_tot_count as float)) as numeric(10,2))
		WHERE  ISNULL(update_complete,1) = 1
		AND    sec6_tot_count > 0;

		UPDATE mh_lpa_score_summary
		SET    total_score	=  CAST((CAST(total_yes as float)/cast((total_yes+total_no) as float)) as numeric(10,2)),
				update_complete = 0
		WHERE  ISNULL(update_complete,1) = 1
		AND    (total_yes+total_no) > 0;



		
END;













GO
/****** Object:  StoredProcedure [dbo].[InsSection]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsSection](	@p_section_id			int,
								@p_section_name			VARCHAR(100),
								@p_display_seq			int
								)
AS
	SET NOCOUNT ON;
	
BEGIN
	DECLARE @l_count 			INT;

	/* havent done any validation for uniqueness of the display sequence */
	
	IF @p_section_id IS NOT NULL AND @p_section_id != 0
			UPDATE mh_lpa_section_master
			SET display_sequence = @p_display_seq,
				section_name = @p_section_name
			WHERE section_id = @p_section_id;
	ELSE
		BEGIN	
			SELECT @l_count = count(*)  
			FROM   mh_lpa_section_master
			WHERE  UPPER(section_name) = UPPER(@p_section_name);
	
		IF @l_count = 0 
			BEGIN
				IF @p_section_name IS NOT NULL AND @p_section_name != '' 
					INSERT INTO mh_lpa_section_master (section_name, display_sequence) VALUES (@p_section_name, @p_display_seq);
			END;
		END;

	
END





GO
/****** Object:  StoredProcedure [dbo].[InsUserDetails]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsUserDetails]
(
                @p_user_id                INT,
                @USERNAME             VARCHAR(100),
                -- @FULLNAME                VARCHAR(1000),
                @p_first_name                VARCHAR(100),
                @p_last_name                VARCHAR(100),
                @PASSWORD                VARCHAR(1000),
                @LOCATION_ID     INT,
                @PRIVACY              VARCHAR(1),
                @ROLE                       VARCHAR(100),
                @STARTDATE                  DATE,
                @ENDDATE                    DATE,
                @EMAILID                   VARCHAR(100)=null,
                @SITEADMINFLAG           VARCHAR(1),
                @GLOBALADMINFLAG    VARCHAR(1),
    @l_err_code    int OUTPUT,
    @l_err_message   varchar(100) OUTPUT  
)
AS
BEGIN
 
--exec InsUserDetails 1, 'ab', 'abc', 'kns',1,'Y','Manager','06-01-2017','06-25-2017',null,'N','Y','Insert'
                -- SET NOCOUNT ON added to prevent extra result sets from
                -- interfering with SELECT statements.
                SET NOCOUNT ON;
 
 
 DECLARE @l_role_id INT;
 DECLARE @l_region_id INT;
 DECLARE @l_country_id INT;
 DECLARE @l_end_date  DATE;
 DECLARE @l_user_id  INT;
 DECLARE @l_count  INT;
 DECLARE @l_dup_count  INT;
  SET @l_err_code = 0;
 SET @l_err_message = 'Initializing';

 IF @ENDDATE = '01-01-0001' 
  set @l_end_date = NULL;
 ELSE
  set @l_end_date = @ENDDATE
 
 SELECT @l_role_id = role_id
 FROM   mh_lpa_role_master
 WHERE  upper(rolename) = UPPER(@ROLE);
 SELECT distinct @l_region_id = region_id,
  @l_country_id = country_id
 FROM  mh_lpa_location_master
 WHERE location_id = @LOCATION_ID;
 
 IF @p_user_id IS NOT NULL AND @p_user_id != 0
 BEGIN
  SET @l_user_id = @p_user_id;
 END;
 ELSE
 BEGIN
  IF @USERNAME IS NOT NULL
   SELECT @l_count = count(*)
   FROM   mh_lpa_user_master
   WHERE  upper(username) = upper(@USERNAME);
  IF @l_count > 0 
   SELECT @l_user_id = user_id
   FROM   mh_lpa_user_master
   WHERE  upper(username) = upper(@USERNAME);
  ELSE
   SET @l_user_id = 0;
 END;
SELECT @l_dup_count = count(*)
FROM   mh_lpa_user_master
WHERE  user_id != @l_user_id
AND    upper(username) = upper(@USERNAME);
IF @l_dup_count > 0 
BEGIN
 SET @l_err_code = 200;
 SET @l_err_message = 'Username exists in the system';
END;
ELSE
BEGIN
 IF @l_user_id IS NOT NULL AND @l_user_id != 0
 BEGIN
 
  UPDATE mh_lpa_user_master
  SET   username = @USERNAME,
  passwd = @PASSWORD,
  emp_full_name = @p_first_name+' '+@p_last_name,
  emp_first_name = @p_first_name,
  emp_last_name = @p_last_name,
  display_name_flag = @PRIVACY,
  email_id = @EMAILID,
  region_id = @l_region_id,
  country_id = @l_country_id,
  location_id = @LOCATION_ID,
  role = @ROLE,
  role_id = @l_role_id,
  global_admin_flag = @GLOBALADMINFLAG,
  site_admin_flag = @SITEADMINFLAG,
  start_date = @STARTDATE,
  end_Date = @l_end_date
  WHERE user_id = @l_user_id
 
  SET @l_err_code = 0;
  SET @l_err_message = 'Updated Successfully';
 END;
 ELSE
 BEGIN
  INSERT INTO [dbo].[mh_lpa_user_master]
  ([username]
  ,[passwd]
  ,[emp_full_name]
  ,[emp_first_name]
  ,[emp_last_name]
  ,[display_name_flag]
  ,[email_id]
  , region_id
  , country_id
  ,[location_id]
  ,[role]
  ,role_id
  ,[global_admin_flag]
  ,[site_admin_flag]
  ,[start_date]
  ,[end_date])
  VALUES
  (@USERNAME
  ,@PASSWORD
  ,@p_first_name+' '+@p_last_name
  ,@p_first_name
  ,@p_last_name
  ,@PRIVACY
  ,@EMAILID
  , @l_region_id
  , @l_country_id
  ,@LOCATION_ID
  ,@ROLE
  ,@l_role_id
  ,@GLOBALADMINFLAG
  ,@SITEADMINFLAG
  ,@STARTDATE
  ,@l_end_date);
  
  SET @l_err_code = 0;
  SET @l_err_message = 'Inserted Successfully';
 END;
END;
           
END




GO
/****** Object:  StoredProcedure [dbo].[InsUserFeedback]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[InsUserFeedback](@p_user_id				int,
								@p_rating				int,
								@p_comments				VARCHAR(100)							
								)
AS
	SET NOCOUNT ON;
	
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_err_code			INT;
	DECLARE @l_err_message		VARCHAR(100);
	
	SET @l_err_code = 0;
	SET @l_err_message = 'Initializing';
	
	IF @p_user_id IS NOT NULL AND @p_user_id != 0
		BEGIN
			INSERT INTO mh_lpa_user_feedback (user_id, rating,review_date, review_comments ) VALUES (@p_user_id, @p_rating,CURRENT_TIMESTAMP, @p_comments);
			SET @l_err_code = 0;
			SET @l_err_message = 'Inserted Successfully';
		END;

	SELECT * FROM (SELECT @l_err_code err_code, @l_err_message err_message) a FOR JSON AUTO

	
END





GO
/****** Object:  StoredProcedure [dbo].[ResetUserPassword]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec ResetUserPassword 'anamika','ehesurhjrj'
CREATE PROCEDURE [dbo].[ResetUserPassword]
(
                @USERNAME               	VARCHAR(100),
                @PASSWORD               	VARCHAR(1000)
)
AS
BEGIN

	DECLARE  @l_err_code 		INT;
	DECLARE  @l_err_message 	VARCHAR(100);
 
--exec InsUserDetails 1, 'ab', 'abc', 'kns',1,'Y','Manager','06-01-2017','06-25-2017',null,'N','Y','Insert'
                -- SET NOCOUNT ON added to prevent extra result sets from
                -- interfering with SELECT statements.
                SET NOCOUNT ON;
 
 	IF @USERNAME IS NOT NULL AND @USERNAME != ''
 
		UPDATE mh_lpa_user_master
		SET   passwd = @PASSWORD
		WHERE username = @USERNAME
		SET @l_err_code = 0;
		SET @l_err_message = 'Password Reset Successfully';
		SELECT email_id,(SELECT * FROM (SELECT @l_err_code err_code, @l_err_message err_message) a FOR JSON AUTO) FROM mh_lpa_user_master WHERE username = @USERNAME AND passwd = @PASSWORD

	
END






GO
/****** Object:  StoredProcedure [dbo].[sp_getAllTasks]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_getAllTasks null,null,null,null,'2','12-10-2017','11-11-2017'
CREATE PROCEDURE [dbo].[sp_getAllTasks](
	@p_region_id  INT=null,
    @p_country_id  INT=null,
    @p_location_id  INT=null,
	@p_line_id		INT = null,
	@p_closed_status VARCHAR(10),
	@p_from_date VARCHAR(100),
	@p_to_date VARCHAR(100) 
) 
 
AS 
BEGIN 
 IF @p_region_id IS NULL OR @p_region_id = 0 
  SET @p_region_id = NULL;
 IF @p_country_id IS NULL OR @p_country_id = 0 
  SET @p_country_id = NULL;
 IF @p_location_id IS NULL OR @p_location_id = 0 
  SET @p_location_id = NULL;
 IF @p_line_id IS NULL OR @p_line_id = 0 
  SET @p_line_id = NULL;

   SELECT  region_name,
                     country_name,
                     location_name,
                     line_name,
                     audit_date,
                     audit_id,
                     section_id,
                     section_name,
                     section_display_sequence,
                     question_id,
                     question,
                     question_display_sequence,
                     answer,
                     remarks,
                     review_closed_status,
                     review_comments,
					image_file_name,
					review_image_file_name
       FROM (
              SELECT  rm.region_name,
                           cm.country_name,
                           lm.location_name,
                           linem.line_name,
                           la.audit_date,
                           la.audit_id,
                           sm.section_id,
                           sm.section_name,
                           sm.display_sequence  section_display_sequence,
                           qm.question_id,
                           qm.question,
                           lq.display_sequence  question_display_sequence,
                           la.answer,
                           la.remarks,
                           la.review_closed_status,
                           la.review_comments,
						   la.image_file_name,
						   la.review_image_file_name
              FROM    mh_lpa_local_answers la,
                           mh_lpa_question_master qm,
                           mh_lpa_local_questions lq,
                           mh_lpa_section_master sm,
                           mh_lpa_region_master rm,
                           mh_lpa_country_master cm,
                           mh_lpa_location_master lm,
                           mh_lpa_line_master linem
              WHERE  la.answer = 1
              AND    la.region_id=rm.region_id
              AND la.country_id=cm.country_id
              AND la.location_id=lm.location_id
              AND la.line_id=linem.line_id
              AND    la.question_id = qm.question_id
              AND    la.question_id < 10000
              AND    la.location_id = lq.location_id
              AND    lq.question_id = qm.question_id
              AND    lq.section_id = sm.section_id
			  AND    la.region_id = ISNULL(@p_region_id, la.region_id)
			  AND    la.country_id = ISNULL(@p_country_id, la.country_id)
			AND     la.location_id = ISNULL(@p_location_id, la.location_id)
			AND     la.line_id = ISNULL(@p_line_id, la.line_id)
     AND    (
(ISNULL(@p_closed_status,'3') ='3' AND la.review_closed_status IS NULL) OR
(@p_closed_status = '0' AND la.review_closed_status =0) OR
(@p_closed_status = '1' AND la.review_closed_status =1) OR
(@p_closed_status = '2')
    )
--	AND la.audit_date BETWEEN @p_from_date AND @p_to_date
AND Convert(Date,la.audit_date,103) BETWEEN Convert(Date,Convert(Varchar(100), @p_from_date),103) AND Convert(Date,Convert(Varchar(100), @p_to_date),103)
              UNION
              SELECT  rm.region_name,
                           cm.country_name,
                           lm.location_name,
                           linem.line_name,
                           la.audit_date,
                           la.audit_id,
                           sm.section_id,
                           sm.section_name,
                           sm.display_sequence  section_display_sequence,
                           qm.local_question_id question_id,
                           qm.local_question question,
                           lq.display_sequence  question_display_sequence,
                           la.answer,
                           la.remarks,
                           la.review_closed_status,
                           la.review_comments,
						   la.image_file_name,
						   la.review_image_file_name
              FROM    mh_lpa_local_answers la,
                           mh_lpa_local_question_master qm,
                           mh_lpa_local_questions lq,
                           mh_lpa_section_master sm,
                           mh_lpa_region_master rm,
                           mh_lpa_country_master cm,
                           mh_lpa_location_master lm,
                           mh_lpa_line_master linem
              WHERE  la.answer = 1
              AND    la.region_id=rm.region_id
              AND la.country_id=cm.country_id
              AND la.location_id=lm.location_id
              AND la.line_id=linem.line_id
              AND    la.question_id = qm.local_question_id
              AND    la.question_id >= 10000
              AND    lq.question_id = qm.local_question_id
              AND    la.location_id = lq.location_id
              AND    lq.section_id = sm.section_id
			  AND    la.region_id = ISNULL(@p_region_id, la.region_id)
			  AND    la.country_id = ISNULL(@p_country_id, la.country_id)
			AND     la.location_id = ISNULL(@p_location_id, la.location_id)
			AND     la.line_id = ISNULL(@p_line_id, la.line_id)
     AND    (
(ISNULL(@p_closed_status,'3') ='3' AND la.review_closed_status IS NULL) OR
(@p_closed_status = '0' AND la.review_closed_status =0) OR
(@p_closed_status = '1' AND la.review_closed_status =1) OR
(@p_closed_status = '2')
    )
	--AND la.audit_date BETWEEN @p_from_date AND @p_to_date
AND Convert(Date,la.audit_date,103) BETWEEN Convert(Date,Convert(Varchar(100), @p_from_date),103) AND Convert(Date,Convert(Varchar(100), @p_to_date),103)
--              AND    ISNULL(CONVERT(VARCHAR(10),la.review_closed_status),'0') in (SELECT item from dbo.SplitString(@p_closed_status,','))
              ) tmp
       ORDER BY section_display_sequence, question_display_sequence
   
END
 



GO
/****** Object:  StoredProcedure [dbo].[SP_GetAuditPlan]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GetAuditPlan](@p_location_id int)   
AS  
BEGIN  

		SELECT pln.audit_plan_id,
				FORMAT(pln.planned_date,'yyyy/MM/dd HH:mm:ss') planned_date, 
				FORMAT(pln.planned_date_end,'yyyy/MM/dd HH:mm:ss') planned_date_end, 
				dbo.getAuditStatus(audit_plan_id)	Audit_Status,
				pln.to_be_audited_by_user_id,
				pln.line_id,
				lm.line_name,
				dbo.getEmployeeName(pln.to_be_audited_by_user_id) emp_name
		FROM    mh_lpa_audit_plan pln,
				mh_lpa_line_master lm
		WHERE  pln.planned_date BETWEEN DATEADD(year, -1, CURRENT_TIMESTAMP)  AND DATEADD(year, 1, CURRENT_TIMESTAMP)
		AND    pln.line_id = lm.line_id
		AND    lm.location_id = ISNULL(@p_location_id, lm.location_id)
		ORDER BY planned_date DESC
END





GO
/****** Object:  StoredProcedure [dbo].[sp_getAuditPlan_Interface]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_getAuditPlan_Interface 22,2018
CREATE PROCEDURE [dbo].[sp_getAuditPlan_Interface](@p_location_id  int,
     @p_year    int)
AS
BEGIN
 SELECT int.location_id
      ,int.line_id
      ,lm.line_name
      ,[year]
      ,[wk1]
      ,[wk2]
      ,[wk3]
      ,[wk4]
      ,[wk5]
      ,[wk6]
      ,[wk7]
      ,[wk8]
      ,[wk9]
      ,[wk10]
      ,[wk11]
      ,[wk12]
      ,[wk13]
      ,[wk14]
      ,[wk15]
      ,[wk16]
      ,[wk17]
      ,[wk18]
      ,[wk19]
      ,[wk20]
      ,[wk21]
      ,[wk22]
      ,[wk23]
      ,[wk24]
      ,[wk25]
      ,[wk26]
      ,[wk27]
      ,[wk28]
      ,[wk29]
      ,[wk30]
      ,[wk31]
      ,[wk32]
      ,[wk33]
      ,[wk34]
      ,[wk35]
      ,[wk36]
      ,[wk37]
      ,[wk38]
      ,[wk39]
      ,[wk40]
      ,[wk41]
      ,[wk42]
      ,[wk43]
      ,[wk44]
      ,[wk45]
      ,[wk46]
      ,[wk47]
      ,[wk48]
      ,[wk49]
      ,[wk50]
      ,[wk51]
      ,[wk52] 
 FROM   mh_lpa_audit_plan_interface int,
   mh_lpa_line_master lm
 WHERE  int.location_id = @p_location_id
 AND    year = @p_year
 AND   int.line_id = lm.line_id
 AND  isnull(end_date, dateadd(day, 1,getdate())) > getdate()
 UNION
 SELECT [location_id]
      ,[line_id]
      ,[line_name]
   ,@p_year
      , 0 wk1
      , 0 wk2
      , 0 wk3
      , 0 wk4
      , 0 wk5
      , 0 wk6
      , 0 wk7
      , 0 wk8
      , 0 wk9
      , 0 wk10
      , 0 wk11
      , 0 wk12
      , 0 wk13
      , 0 wk14
      , 0 wk15
      , 0 wk16
      , 0 wk17
      , 0 wk18
      , 0 wk19
      , 0 wk20
      , 0 wk21
      , 0 wk22
      , 0 wk23
      , 0 wk24
      , 0 wk25
      , 0 wk26
      , 0 wk27
      , 0 wk28
      , 0 wk29
      , 0 wk30
      , 0 wk31
      , 0 wk32
      , 0 wk33
      , 0 wk34
      , 0 wk35
      , 0 wk36
      , 0 wk37
      , 0 wk38
      , 0 wk39
      , 0 wk40
      , 0 wk41
      , 0 wk42
      , 0 wk43
      , 0 wk44
      , 0 wk45
      , 0 wk46
      , 0 wk47
      , 0 wk48
      , 0 wk49
      , 0 wk50
      , 0 wk51
      , 0 wk52 
 FROM  mh_lpa_line_master lm
 WHERE lm.line_id NOT IN (select line_id from mh_lpa_audit_plan_interface
       WHERE  location_id = @p_location_id
       AND    year = @p_year)
 AND  lm.location_id = @p_location_id
 AND  isnull(end_date, dateadd(day, 1,getdate())) > getdate()
 order by line_name
END;



GO
/****** Object:  StoredProcedure [dbo].[sp_getAuditReportPaths]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec sp_getAuditReportPaths null,null,null,null,'12-10-2017','11-11-2017'
CREATE PROCEDURE [dbo].[sp_getAuditReportPaths](
	@p_region_id  INT=null,
    @p_country_id  INT=null,
    @p_location_id  INT=null,
	@p_line_id		INT = null,
	@p_from_date VARCHAR(100),
	@p_to_date VARCHAR(100) 
)
AS 
BEGIN 
 IF @p_region_id IS NULL OR @p_region_id = 0 
  SET @p_region_id = NULL
 IF @p_country_id IS NULL OR @p_country_id = 0 
  SET @p_country_id = NULL
 IF @p_location_id IS NULL OR @p_location_id = 0 
  SET @p_location_id = NULL
 IF @p_line_id IS NULL OR @p_line_id = 0 
  SET @p_line_id = NULL

              SELECT DISTINCT  rm.region_name,
                           cm.country_name,
                           lm.location_name,
                           linem.line_name,
                           CONCAT(la.audit_date,'(UTC)') audit_date,
                           la.audit_id
              FROM    mh_lpa_local_answers la,
                           mh_lpa_region_master rm,
                           mh_lpa_country_master cm,
                           mh_lpa_location_master lm,
                           mh_lpa_line_master linem
              WHERE   la.region_id=rm.region_id
              AND la.country_id=cm.country_id
              AND la.location_id=lm.location_id
              AND la.line_id=linem.line_id
           
			  AND    la.region_id = ISNULL(@p_region_id, la.region_id)
			  AND    la.country_id = ISNULL(@p_country_id, la.country_id)
			AND     la.location_id = ISNULL(@p_location_id, la.location_id)
			AND     la.line_id = ISNULL(@p_line_id, la.line_id)
    
AND Convert(Date,la.audit_date,103) BETWEEN Convert(Date,Convert(Varchar(100), @p_from_date),103) 
	AND Convert(Date,Convert(Varchar(100), @p_to_date),103)
   ORDER BY audit_id  DESC         
END
 



GO
/****** Object:  StoredProcedure [dbo].[sp_getDistributionList]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_getDistributionList]
(
@GLOBALADMINFLAG VARCHAR(2)=null,
@SITEADMINFLAG VARCHAR(2)=null,
@p_location_id INT
)
AS
BEGIN
	IF(UPPER(@GLOBALADMINFLAG)='Y')
	BEGIN
		SELECT * FROM [dbo].[mh_lpa_distribution_list] WHERE LIST_TYPE='At New Line Addition'
	END
	ELSE IF(UPPER(@SITEADMINFLAG)='Y')
	BEGIN
		SELECT * FROM [dbo].[mh_lpa_distribution_list] WHERE LOCATION_ID=@p_location_id
	END
END




GO
/****** Object:  StoredProcedure [dbo].[sp_getDropDownDetailsForReport]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_getDropDownDetailsForReport 0,0,0,0,null

CREATE PROCEDURE [dbo].[sp_getDropDownDetailsForReport]
(
@p_region_id INT=null,
@p_country_id INT =null,
@p_location_id INT = NULL,
@p_line_id INT=NULL,
@p_Current_field VARCHAR(100)=NULL
)
AS
BEGIN
 DECLARE @l_region_id INT;
 DECLARE @l_country_id INT;
 DECLARE @l_location_id INT;
 DECLARE @l_line_id INT;


 IF @p_region_id IS NOT NULL AND @p_region_id != 0 
SET @l_region_id = @p_region_id;
 ELSE
SET @l_region_id = NULL;

 IF @p_country_id IS NOT NULL AND @p_country_id != 0 
SET @l_country_id = @p_country_id;

 ELSE
SET @l_country_id = NULL;

 IF @p_location_id IS NOT NULL AND @p_location_id != 0 
SET @l_location_id = @p_location_id;
 ELSE
SET @l_location_id = NULL;

 IF @p_line_id IS NOT NULL AND @p_line_id != 0 
SET @l_line_id = @p_line_id;
 ELSE
SET @l_line_id = NULL;

SELECT distinct rm.region_id,region_name
FROM   mh_lpa_location_master loc,
mh_lpa_country_master cm,
mh_lpa_region_master rm,
mh_lpa_line_master lm
WHERE  lm.location_id = loc.location_id
AND    lm.country_id = cm.country_id
AND    lm.region_id = rm.region_id
AND    lm.region_id = ISNULL(@l_region_id, lm.region_id)
AND    lm.country_id = ISNULL(@l_country_id, lm.country_id)
AND    lm.location_id = ISNULL(@l_location_id, lm.location_id)
AND    lm.line_id = ISNULL(@l_line_id , lm.line_id)
ORDER BY region_name;


SELECT distinct cm.country_id, country_name
FROM   mh_lpa_location_master loc,
mh_lpa_country_master cm,
mh_lpa_region_master rm,
mh_lpa_line_master lm
WHERE  lm.location_id = loc.location_id
AND    lm.country_id = cm.country_id
AND    lm.region_id = rm.region_id
AND    lm.region_id = ISNULL(@l_region_id, lm.region_id)
AND    lm.country_id = ISNULL(@l_country_id, lm.country_id)
AND    lm.location_id = ISNULL(@l_location_id, lm.location_id)
AND    lm.line_id = ISNULL(@l_line_id , lm.line_id)
ORDER BY country_name;

SELECT distinct loc.location_id, location_name
FROM   mh_lpa_location_master loc,
mh_lpa_country_master cm,
mh_lpa_region_master rm,
mh_lpa_line_master lm
WHERE  lm.location_id = loc.location_id
AND    lm.country_id = cm.country_id
AND    lm.region_id = rm.region_id
AND    lm.region_id = ISNULL(@l_region_id, lm.region_id)
AND    lm.country_id = ISNULL(@l_country_id, lm.country_id)
AND    lm.location_id = ISNULL(@l_location_id, lm.location_id)
AND    lm.line_id = ISNULL(@l_line_id , lm.line_id)
ORDER BY location_name;

SELECT distinct lm.line_id, line_name
FROM   mh_lpa_location_master loc,
mh_lpa_country_master cm,
mh_lpa_region_master rm,
mh_lpa_line_master lm
WHERE  lm.location_id = loc.location_id
AND    lm.country_id = cm.country_id
AND    lm.region_id = rm.region_id
AND    lm.region_id = ISNULL(@l_region_id, lm.region_id)
AND    lm.country_id = ISNULL(@l_country_id, lm.country_id)
AND    lm.location_id = ISNULL(@l_location_id, lm.location_id)
AND    lm.line_id = ISNULL(@l_line_id , lm.line_id)
ORDER BY line_name;

-- IF(UPPER(@p_Current_field)=UPPER('Region'))
-- BEGIN
--SELECT region_name, rm.region_id
--FROM   mh_lpa_location_master loc,
--mh_lpa_country_master cm,
--mh_lpa_region_master rm,
--mh_lpa_line_master lm
--WHERE  lm.location_id = loc.location_id
--AND    lm.country_id = cm.country_id
--AND    lm.region_id = rm.region_id
--AND    lm.region_id = ISNULL(@l_region_id, lm.region_id)
--AND    lm.country_id = ISNULL(@l_country_id, lm.country_id)
--AND    lm.location_id = ISNULL(@l_location_id, lm.location_id)
--AND    lm.line_id = ISNULL(@l_line_id , lm.line_id);
-- END
-- ELSE IF(UPPER(@p_Current_field)=UPPER('Country'))
-- BEGIN
--SELECT country_name, lm.country_id
--FROM   mh_lpa_location_master loc,
--mh_lpa_country_master cm,
--mh_lpa_region_master rm,
--mh_lpa_line_master lm
--WHERE  lm.location_id = loc.location_id
--AND    lm.country_id = cm.country_id
--AND    lm.region_id = rm.region_id
--AND    lm.region_id = ISNULL(@l_region_id, lm.region_id)
--AND    lm.country_id = ISNULL(@l_country_id, lm.country_id)
--AND    lm.location_id = ISNULL(@l_location_id, lm.location_id)
--AND    lm.line_id = ISNULL(@l_line_id , lm.line_id);
-- END
-- ELSE IF(UPPER(@p_Current_field)=UPPER('Location'))
-- BEGIN
--SELECT location_name, loc.location_id
--FROM   mh_lpa_location_master loc,
--mh_lpa_country_master cm,
--mh_lpa_region_master rm,
--mh_lpa_line_master lm
--WHERE  lm.location_id = loc.location_id
--AND    lm.country_id = cm.country_id
--AND    lm.region_id = rm.region_id
--AND    lm.region_id = ISNULL(@l_region_id, lm.region_id)
--AND    lm.country_id = ISNULL(@l_country_id, lm.country_id)
--AND    lm.location_id = ISNULL(@l_location_id, lm.location_id)
--AND    lm.line_id = ISNULL(@l_line_id , lm.line_id);
-- END
-- ELSE IF(UPPER(@p_Current_field)=UPPER('Line'))
-- BEGIN
--SELECT line_name,lm.line_id
--FROM   mh_lpa_location_master loc,
--mh_lpa_country_master cm,
--mh_lpa_region_master rm,
--mh_lpa_line_master lm
--WHERE  lm.location_id = loc.location_id
--AND    lm.country_id = cm.country_id
--AND    lm.region_id = rm.region_id
--AND    lm.region_id = ISNULL(@l_region_id, lm.region_id)
--AND    lm.country_id = ISNULL(@l_country_id, lm.country_id)
--AND    lm.location_id = ISNULL(@l_location_id, lm.location_id)
--AND    lm.line_id = ISNULL(@l_line_id , lm.line_id);
-- END

END



GO
/****** Object:  StoredProcedure [dbo].[sp_getEmailList]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_getEmailList](@p_location_id		INT)
AS
BEGIN
	DECLARE @l_count 			INT;
	DECLARE @l_location_id 			INT;
	DECLARE @l_global_admin			VARCHAR(1);
	DECLARE @l_local_admin			VARCHAR(1);


	SELECT distinct email_id
	FROM   mh_lpa_user_master
	WHERE location_id = @p_location_id;


END;



GO
/****** Object:  StoredProcedure [dbo].[sp_getLoginInfo]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_getLoginInfo](
@user_name VARCHAR(100),
@PASSWORD VARCHAR(100)
)
AS
BEGIN
SELECT user_id
,username
,passwd
,emp_full_name
,display_name_flag
,email_id
,location_id
,(select location_name from [dbo].[mh_lpa_location_master] where location_id=um.location_id) location_name
,role
,(select role_id from [dbo].[mh_lpa_role_master] where rolename=um.role) role_id
,global_admin_flag
,site_admin_flag
,start_date
,end_date
,region_id
,country_id
 FROM [dbo].[mh_lpa_user_master] um where username=@user_name AND passwd=@PASSWORD
 AND  ISNULL(end_date, getdate())  >= getdate()
END






GO
/****** Object:  StoredProcedure [dbo].[sp_getLPAPlanVsActuals]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  procedure [dbo].[sp_getLPAPlanVsActuals](@p_region_id          INT=null,
                                                                           @p_country_id        INT=null,
                                                                           @p_location_id              INT=null,
                                                                           @p_line_id                  INT=null,
                                                                           @p_date                           DATE)
AS
BEGIN
 
	DECLARE @l_region_id       INT;
	DECLARE @l_country_id             INT;
	DECLARE @l_location_id            INT;
	DECLARE @l_line_id         INT;
	DECLARE @i					INT;
	DECLARE @l_count				INT;
	DECLARE @l_date				DATE;

	DECLARE @l_tempTable TABLE
	(
	cy_audit_planned	INT,
	cy_audit_performed	INT,
	ly_audit_planned	INT,
	ly_audit_performed	INT,
	audit_date			DATE
	);
 
       IF @p_region_id IS NULL OR @p_region_id = 0 
              SET @l_region_id = NULL;
       ELSE
              SET @l_region_id = @p_region_id;
 
       IF @p_country_id IS NULL OR @p_country_id = 0 
              SET @l_country_id = NULL;
       ELSE
              SET @l_country_id = @p_country_id;
 
       IF @p_location_id IS NULL OR @p_location_id = 0 
              SET @l_location_id = NULL;
       ELSE
              SET @l_location_id = @p_location_id;
 
       IF @p_line_id IS NULL OR @p_line_id = 0 
              SET @l_line_id = NULL;
       ELSE
              SET @l_line_id = @p_line_id;



		INSERT INTO @l_tempTable
		SELECT 
                    count(*) cy_audit_planned,
                    0 cy_audit_performed,
					0 ly_audit_planned,
					0 ly_audit_performed,
                    CONVERT(DATE, '01-'+FORMAT(planned_date,'MM-yyyy'),105) dt
        FROM    mh_lpa_audit_plan p,
				mh_lpa_line_master l
        WHERE   p.line_id = l.line_id
		AND     l.region_id = ISNULL(@l_region_id, l.region_id)
        AND     l.country_id = ISNULL(@l_country_id, l.country_id)
        AND     l.location_id = ISNULL(@l_location_id, l.location_id)
        AND     l.line_id = ISNULL(@l_line_id, l.line_id)
        AND     planned_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date
        GROUP BY CONVERT(DATE, '01-'+FORMAT(planned_date,'MM-yyyy'),105)
        UNION
        SELECT 
                    0 cy_audit_planned,
                    count(*) cy_audit_performed,
					0 ly_audit_planned,
					0 ly_audit_performed,
                    CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105) dt
        FROM    mh_lpa_score_summary
        WHERE   region_id = ISNULL(@l_region_id, region_id)
        AND     country_id = ISNULL(@l_country_id, country_id)
        AND     location_id = ISNULL(@l_location_id, location_id)
        AND     line_id = ISNULL(@l_line_id, line_id)
        AND     audit_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date
        GROUP BY CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105)
		UNION
        SELECT 
					0 cy_audit_planned,
					0 cy_audit_performed,
                    count(*) ly_audit_planned,
                    0 ly_audit_performed,
                    CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,planned_date),'MM-yyyy'),105) dt
        FROM    mh_lpa_audit_plan p,
				mh_lpa_line_master l
        WHERE   p.line_id = l.line_id
		AND     l.region_id = ISNULL(@l_region_id, l.region_id)
        AND     l.country_id = ISNULL(@l_country_id, l.country_id)
        AND     l.location_id = ISNULL(@l_location_id, l.location_id)
        AND     l.line_id = ISNULL(@l_line_id, l.line_id)
        AND     planned_date BETWEEN DATEADD(year,-2,@p_date)  AND  DATEADD(year,-1,@p_date)
        GROUP BY CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,planned_date),'MM-yyyy'),105)
        UNION
        SELECT 
					0 cy_audit_planned,
					0 cy_audit_performed,
                    0 ly_audit_planned,
                    count(*) ly_audit_performed,
                    CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,audit_date),'MM-yyyy'),105) dt
        FROM    mh_lpa_score_summary
        WHERE   region_id = ISNULL(@l_region_id, region_id)
        AND     country_id = ISNULL(@l_country_id, country_id)
        AND     location_id = ISNULL(@l_location_id, location_id)
        AND     line_id = ISNULL(@l_line_id, line_id)
        AND     audit_date BETWEEN DATEADD(year,-2,@p_date) AND  DATEADD(year,-1,@p_date)
        GROUP BY CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,audit_date),'MM-yyyy'),105);


	SET @i = 0;
	WHILE @i < 13
	BEGIN
		SET @l_count = 0;
		SET @l_date = CONVERT(DATE, '01-'+FORMAT(DATEADD(month,-@i,@p_date),'MM-yyyy'),105);
		SELECT @l_count = count(*)
		FROM   @l_tempTable
		WHERE  audit_date = @l_date;

		IF @l_count = 0 
			INSERT INTO @l_tempTable VALUES(0,0,0,0, @l_date);
			
		SET @i = @i+1; 
	END;

	SELECT tmp1.audit_period, tmp1.dt,
			CASE 
				WHEN tmp1.cy_audit_planned = 0 THEN 0
				ELSE ROUND(CAST(CAST(tmp1.cy_audit_performed AS NUMERIC(10,2))/CAST(tmp1.cy_audit_planned AS NUMERIC(10,2)) AS NUMERIC(10,2))*100,0)
			END  cy_perform_rate,
			CASE 
				WHEN tmp1.ly_audit_planned = 0 THEN 0
				ELSE ROUND(CAST(CAST(tmp1.ly_audit_performed AS NUMERIC(10,2))/CAST(tmp1.ly_audit_planned AS NUMERIC(10,2)) AS NUMERIC(10,2))*100,0)
			END  ly_perform_rate,
			cy_audit_planned,
			cy_audit_performed
	FROM (select 
				FORMAT(audit_date,'MMM-yyyy') audit_period,
				audit_date dt,
				SUM(cy_audit_planned) cy_audit_planned,
				SUM(cy_audit_performed) cy_audit_performed,
				SUM(ly_audit_planned) ly_audit_planned,
				SUM(ly_audit_performed) ly_audit_performed
		FROM @l_tempTable
		GROUP BY FORMAT(audit_date,'MMM-yyyy'),
				audit_date) tmp1
	ORDER BY dt;




 
/*
	SELECT tmp1.audit_period, tmp1.dt,
			CASE 
				WHEN tmp1.cy_audit_planned = 0 THEN 0
				ELSE ROUND(CAST(CAST(tmp1.cy_audit_performed AS NUMERIC(10,2))/CAST(tmp1.cy_audit_planned AS NUMERIC(10,2)) AS NUMERIC(10,2))*100,0)
			END  cy_perform_rate,
			CASE 
				WHEN tmp1.ly_audit_planned = 0 THEN 0
				ELSE ROUND(CAST(CAST(tmp1.ly_audit_performed AS NUMERIC(10,2))/CAST(tmp1.ly_audit_planned AS NUMERIC(10,2)) AS NUMERIC(10,2))*100,0)
			END  ly_perform_rate
	FROM
	(
       SELECT audit_period,
              SUM(cy_audit_planned) cy_audit_planned,
              SUM(cy_audit_performed) cy_audit_performed,
              SUM(ly_audit_planned) ly_audit_planned,
              SUM(ly_audit_performed) ly_audit_performed,
              dt
       FROM (
              SELECT FORMAT(planned_date,'MMM-yyyy') audit_period,
                           count(*) cy_audit_planned,
                           0 cy_audit_performed,
						   0 ly_audit_planned,
						   0 ly_audit_performed,
                           CONVERT(DATE, '01-'+FORMAT(planned_date,'MM-yyyy'),105) dt
              FROM    mh_lpa_audit_plan p,
						mh_lpa_line_master l
              WHERE   p.line_id = l.line_id
			  AND     l.region_id = ISNULL(@l_region_id, l.region_id)
              AND     l.country_id = ISNULL(@l_country_id, l.country_id)
              AND     l.location_id = ISNULL(@l_location_id, l.location_id)
              AND     l.line_id = ISNULL(@l_line_id, l.line_id)
              AND     planned_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date
              GROUP BY FORMAT(planned_date,'MMM-yyyy'), CONVERT(DATE, '01-'+FORMAT(planned_date,'MM-yyyy'),105)
              UNION
              SELECT FORMAT(audit_date,'MMM-yyyy') audit_period,
                           0 cy_audit_planned,
                           count(*) cy_audit_performed,
						   0 ly_audit_planned,
						   0 ly_audit_performed,
                           CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105) dt
              FROM    mh_lpa_score_summary
              WHERE   region_id = ISNULL(@l_region_id, region_id)
              AND     country_id = ISNULL(@l_country_id, country_id)
              AND     location_id = ISNULL(@l_location_id, location_id)
              AND     line_id = ISNULL(@l_line_id, line_id)
              AND     audit_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date
              GROUP BY FORMAT(audit_date,'MMM-yyyy'), CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105)
			  UNION
              SELECT FORMAT(DATEADD(year,1,planned_date),'MMM-yyyy') audit_period,
						   0 cy_audit_planned,
						   0 cy_audit_performed,
                           count(*) ly_audit_planned,
                           0 ly_audit_performed,
                           CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,planned_date),'MM-yyyy'),105) dt
              FROM    mh_lpa_audit_plan p,
						mh_lpa_line_master l
              WHERE   p.line_id = l.line_id
			  AND     l.region_id = ISNULL(@l_region_id, l.region_id)
              AND     l.country_id = ISNULL(@l_country_id, l.country_id)
              AND     l.location_id = ISNULL(@l_location_id, l.location_id)
              AND     l.line_id = ISNULL(@l_line_id, l.line_id)
              AND     planned_date BETWEEN DATEADD(year,-2,@p_date)  AND  DATEADD(year,-1,@p_date)
              GROUP BY FORMAT(DATEADD(year,1,planned_date),'MMM-yyyy'), CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,planned_date),'MM-yyyy'),105)
              UNION
              SELECT FORMAT(DATEADD(year,1,audit_date),'MMM-yyyy') audit_period,
						   0 cy_audit_planned,
						   0 cy_audit_performed,
                           0 ly_audit_planned,
                           count(*) ly_audit_performed,
                           CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,audit_date),'MM-yyyy'),105) dt
              FROM    mh_lpa_score_summary
              WHERE   region_id = ISNULL(@l_region_id, region_id)
              AND     country_id = ISNULL(@l_country_id, country_id)
              AND     location_id = ISNULL(@l_location_id, location_id)
              AND     line_id = ISNULL(@l_line_id, line_id)
              AND     audit_date BETWEEN DATEADD(year,-2,@p_date) AND  DATEADD(year,-1,@p_date)
              GROUP BY FORMAT(DATEADD(year,1,audit_date),'MMM-yyyy'), CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,audit_date),'MM-yyyy'),105)
              ) tmp
       GROUP BY audit_period, dt
	) tmp1
       ORDER BY tmp1.dt
  
 */
 
 
END


-- ========================================================



GO
/****** Object:  StoredProcedure [dbo].[sp_getLPAResultByLine]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_getLPAResultByLine](@p_region_id  INT=null,
           @p_country_id  INT=null,
           @p_location_id  INT=null,
           @p_line_id   INT=null,
           @p_date    DATE)
AS
BEGIN
DECLARE @l_region_id  INT;
 DECLARE @l_country_id  INT;
 DECLARE @l_location_id  INT;
 DECLARE @l_line_id  INT;
 IF @p_region_id IS NULL OR @p_region_id = 0 
  SET @l_region_id = NULL;
 ELSE
  SET @l_region_id = @p_region_id;
 IF @p_country_id IS NULL OR @p_country_id = 0 
  SET @l_country_id = NULL;
 ELSE
  SET @l_country_id = @p_country_id;
 IF @p_location_id IS NULL OR @p_location_id = 0 
  SET @l_location_id = NULL;
 ELSE
  SET @l_location_id = @p_location_id;
 IF @p_line_id IS NULL OR @p_line_id = 0 
  SET @l_line_id = NULL;
 ELSE
  SET @l_line_id = @p_line_id;


 SELECT  TOP(10) line_name,
				average_score
 FROM   (
SELECT  TOP(10) lm.line_name,
   AVG(total_score) average_score
 FROM    mh_lpa_score_summary s,
   mh_lpa_line_master lm
 WHERE   s.region_id = ISNULL(@l_region_id, s.region_id)
 AND     s.country_id = ISNULL(@l_country_id, s.country_id)
 AND     s.location_id = ISNULL(@l_location_id, s.location_id)
 AND     s.line_id = ISNULL(@l_line_id, s.line_id)
 AND     s.line_id = lm.line_id
 AND     audit_date BETWEEN DATEADD(year,-2,@p_date) AND @p_date
 GROUP BY lm.line_name
 --HAVING  AVG(total_score) < 0.6
 ORDER BY average_score asc) t

END




GO
/****** Object:  StoredProcedure [dbo].[sp_getLPAResultByQuestion]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_getLPAResultByQuestion](@p_region_id		INT=null,
											@p_country_id		INT=null,
											@p_location_id		INT=null,
											@p_line_id			INT=null,
											@p_date				DATE)
AS
BEGIN

DECLARE @l_region_id		INT;
	DECLARE @l_country_id		INT;
	DECLARE @l_location_id		INT;
	DECLARE @l_line_id		INT;
	DECLARE @l_count		INT;

	IF @p_region_id IS NULL OR @p_region_id = 0 
		SET @l_region_id = NULL;
	ELSE
		SET @l_region_id = @p_region_id;

	IF @p_country_id IS NULL OR @p_country_id = 0 
		SET @l_country_id = NULL;
	ELSE
		SET @l_country_id = @p_country_id;

	IF @p_location_id IS NULL OR @p_location_id = 0 
		SET @l_location_id = NULL;
	ELSE
		SET @l_location_id = @p_location_id;

	IF @p_line_id IS NULL OR @p_line_id = 0 
		SET @l_line_id = NULL;
	ELSE
		SET @l_line_id = @p_line_id;

	SELECT @l_count = count(*)
	FROM    mh_lpa_score_summary
	WHERE  1=1
	AND     region_id = ISNULL(@l_region_id, region_id)
	AND     country_id = ISNULL(@l_country_id, country_id)
	AND     location_id = ISNULL(@l_location_id, location_id)
	AND     line_id = ISNULL(@l_line_id, line_id)
	AND     audit_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date;

	IF @l_count = 0
		select qm.question_id, qm.question, 0 score
		FROM   mh_lpa_question_master qm
	ELSE
SELECT unpvt.question_id, qm.question,  ISNULL(score,0) score, qm.section_id
FROM (
		SELECT
		CASE 
			WHEN q1_tot_count = 0 THEN 0.0
			ELSE CAST(ROUND (q1_yes_count/CAST(q1_tot_count as numeric(10,5)),2 ) AS numeric(10,5))
		END as q0,
		CASE 
			WHEN q2_tot_count = 0  THEN 0.0
			ELSE CAST((q2_yes_count/CAST(q2_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q1,
		CASE 
			WHEN q3_tot_count = 0  THEN 0.0
			ELSE CAST((q3_yes_count/CAST(q3_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q2,
		CASE 
			WHEN q4_tot_count = 0  THEN 0.0
			ELSE CAST((q4_yes_count/CAST(q4_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q3,
		CASE 
			WHEN q5_tot_count = 0  THEN 0.0
			ELSE CAST((q5_yes_count/CAST(q5_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q4,
		CASE 
			WHEN q6_tot_count = 0  THEN 0.0
			ELSE CAST((q6_yes_count/CAST(q6_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q5,
		CASE 
			WHEN q7_tot_count = 0  THEN 0.0
			ELSE CAST((q7_yes_count/CAST(q7_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q6,
		CASE 
			WHEN q8_tot_count = 0  THEN 0.0
			ELSE CAST((q8_yes_count/CAST(q8_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q7,
		CASE 
			WHEN q9_tot_count = 0  THEN 0.0
			ELSE CAST((q9_yes_count/CAST(q9_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q8,
		CASE 
			WHEN q10_tot_count = 0  THEN 0.0
			ELSE CAST((q10_yes_count/CAST(q10_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q9,
		CASE 
			WHEN q11_tot_count = 0  THEN 0.0
			ELSE CAST((q11_yes_count/CAST(q11_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q10,
		CASE 
			WHEN q12_tot_count = 0  THEN 0.0
			ELSE CAST((q12_yes_count/CAST(q12_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q11,
		CASE 
			WHEN q13_tot_count = 0  THEN 0.0
			ELSE CAST((q13_yes_count/CAST(q13_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q12,
		CASE 
			WHEN q14_tot_count = 0  THEN 0.0
			ELSE CAST((q14_yes_count/CAST(q14_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q13,
		CASE 
			WHEN q15_tot_count = 0  THEN 0.0
			ELSE CAST((q15_yes_count/CAST(q15_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q14,
		CASE 
			WHEN q16_tot_count = 0  THEN 0.0
			ELSE CAST((q16_yes_count/CAST(q16_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q15,
		CASE 
			WHEN q17_tot_count = 0  THEN 0.0
			ELSE CAST((q17_yes_count/CAST(q17_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q16,
		CASE 
			WHEN q18_tot_count = 0  THEN 0.0
			ELSE CAST((q18_yes_count/CAST(q18_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q17,
		CASE 
			WHEN q19_tot_count = 0  THEN 0.0
			ELSE CAST((q19_yes_count/CAST(q19_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q18,
		CASE 
			WHEN q20_tot_count = 0  THEN 0.0
			ELSE CAST((q20_yes_count/CAST(q20_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q19,
		CASE 
			WHEN q21_tot_count = 0  THEN 0.0
			ELSE CAST((q21_yes_count/CAST(q21_tot_count as numeric(10,5) ) ) AS numeric(10,5))
		END as q20
		--INTO #TEMP1
	FROM
		(SELECT
				SUM(
					CASE 
						WHEN q1_answer = 0 THEN 1
						ELSE 0
					END) as q1_yes_count,
				SUM(
					CASE 
						WHEN q1_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q1_tot_count,
		--
				SUM(
					CASE 
						WHEN q2_answer = 0 THEN 1
						ELSE 0
					END) as q2_yes_count,
				SUM(
					CASE 
						WHEN q2_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q2_tot_count,
		--
				SUM(
					CASE 
						WHEN q3_answer = 0 THEN 1
						ELSE 0
					END) as q3_yes_count,
				SUM(
					CASE 
						WHEN q3_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q3_tot_count,
		--
				SUM(
					CASE 
						WHEN q4_answer = 0 THEN 1
						ELSE 0
					END) as q4_yes_count,
				SUM(
					CASE 
						WHEN q4_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q4_tot_count,
		--
				SUM(
					CASE 
						WHEN q5_answer = 0 THEN 1
						ELSE 0
					END) as q5_yes_count,
				SUM(
					CASE 
						WHEN q5_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q5_tot_count,
		--
				SUM(
					CASE 
						WHEN q6_answer = 0 THEN 1
						ELSE 0
					END) as q6_yes_count,
				SUM(
					CASE 
						WHEN q6_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q6_tot_count,
		--
				SUM(
					CASE 
						WHEN q7_answer = 0 THEN 1
						ELSE 0
					END) as q7_yes_count,
				SUM(
					CASE 
						WHEN q7_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q7_tot_count,
		--
				SUM(
					CASE 
						WHEN q8_answer = 0 THEN 1
						ELSE 0
					END) as q8_yes_count,
				SUM(
					CASE 
						WHEN q8_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q8_tot_count,
		--
				SUM(
					CASE 
						WHEN q9_answer = 0 THEN 1
						ELSE 0
					END) as q9_yes_count,
				SUM(
					CASE 
						WHEN q9_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q9_tot_count,
		--
				SUM(
					CASE 
						WHEN q10_answer = 0 THEN 1
						ELSE 0
					END) as q10_yes_count,
				SUM(
					CASE 
						WHEN q10_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q10_tot_count,
		--
				SUM(
					CASE 
						WHEN q11_answer = 0 THEN 1
						ELSE 0
					END) as q11_yes_count,
				SUM(
					CASE 
						WHEN q11_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q11_tot_count,
		--
				SUM(
					CASE 
						WHEN q12_answer = 0 THEN 1
						ELSE 0
					END) as q12_yes_count,
				SUM(
					CASE 
						WHEN q12_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q12_tot_count,
		--
				SUM(
					CASE 
						WHEN q13_answer = 0 THEN 1
						ELSE 0
					END) as q13_yes_count,
				SUM(
					CASE 
						WHEN q13_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q13_tot_count,
		--
				SUM(
					CASE 
						WHEN q14_answer = 0 THEN 1
						ELSE 0
					END) as q14_yes_count,
				SUM(
					CASE 
						WHEN q14_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q14_tot_count,
		--
				SUM(
					CASE 
						WHEN q15_answer = 0 THEN 1
						ELSE 0
					END) as q15_yes_count,
				SUM(
					CASE 
						WHEN q15_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q15_tot_count,
		--
				SUM(
					CASE 
						WHEN q16_answer = 0 THEN 1
						ELSE 0
					END) as q16_yes_count,
				SUM(
					CASE 
						WHEN q16_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q16_tot_count,
		--
				SUM(
					CASE 
						WHEN q17_answer = 0 THEN 1
						ELSE 0
					END) as q17_yes_count,
				SUM(
					CASE 
						WHEN q17_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q17_tot_count,
		--
				SUM(
					CASE 
						WHEN q18_answer = 0 THEN 1
						ELSE 0
					END) as q18_yes_count,
				SUM(
					CASE 
						WHEN q18_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q18_tot_count,
		--
				SUM(
					CASE 
						WHEN q19_answer = 0 THEN 1
						ELSE 0
					END) as q19_yes_count,
				SUM(
					CASE 
						WHEN q19_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q19_tot_count,
		--
				SUM(
					CASE 
						WHEN q20_answer = 0 THEN 1
						ELSE 0
					END) as q20_yes_count,
				SUM(
					CASE 
						WHEN q20_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q20_tot_count,
		--
				SUM(
					CASE 
						WHEN q21_answer = 0 THEN 1
						ELSE 0
					END) as q21_yes_count,
				SUM(
					CASE 
						WHEN q21_answer IN( 0,1) THEN 1
						ELSE 0
					END) as q21_tot_count
		--
		FROM    mh_lpa_score_summary
		WHERE  1=1
		AND     region_id = ISNULL(@l_region_id, region_id)
		AND     country_id = ISNULL(@l_country_id, country_id)
		AND     location_id = ISNULL(@l_location_id, location_id)
		AND     line_id = ISNULL(@l_line_id, line_id)
		AND     audit_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date 
		)  tmp ) t
	UNPIVOT
	(score FOR question_id IN 
		(
			q0,q1,q2,q3,q4,q5,
			q6,q7,q8,q9,q10,
			q11,q12,q13,q14,q15,
			q16,q17,q18,q19,q20
		)
	)AS unpvt,
	mh_lpa_question_master qm
	WHERE CAST(REPLACE(unpvt.question_id,'q','') as INT)+1 = qm.question_id
	order by qm.section_id, qm.question_id;



END






GO
/****** Object:  StoredProcedure [dbo].[sp_getLPAResultMonthly]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_getLPAResultMonthly](@p_region_id          INT=null,
                                                                           @p_country_id        INT=null,
                                                                           @p_location_id              INT=null,
                                                                           @p_line_id                  INT=null,
                                                                           @p_date                           DATE)
AS
BEGIN
 
	   DECLARE @l_region_id       INT;
       DECLARE @l_country_id             INT;
       DECLARE @l_location_id            INT;
       DECLARE @l_line_id         INT;
	   DECLARE @i					INT;
	   DECLARE @l_count				INT;
	   DECLARE @l_date				DATE;

	   DECLARE @l_tempTable TABLE
	   (
		avg_score		NUMERIC(10,5),
		prev_score		NUMERIC(10,5),
		audit_date		DATE
	   );
 
       IF @p_region_id IS NULL OR @p_region_id = 0 
              SET @l_region_id = NULL;
       ELSE
              SET @l_region_id = @p_region_id;
 
       IF @p_country_id IS NULL OR @p_country_id = 0 
              SET @l_country_id = NULL;
       ELSE
              SET @l_country_id = @p_country_id;
 
       IF @p_location_id IS NULL OR @p_location_id = 0 
              SET @l_location_id = NULL;
       ELSE
              SET @l_location_id = @p_location_id;
 
       IF @p_line_id IS NULL OR @p_line_id = 0 
              SET @l_line_id = NULL;
       ELSE
              SET @l_line_id = @p_line_id;

		INSERT INTO @l_tempTable
		SELECT 
                    AVG(total_score) average_score,
                    CAST(0.0 as numeric(10,5)) previous_score,
                    CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105) a
        FROM    mh_lpa_score_summary
        WHERE   region_id = ISNULL(@l_region_id, region_id)
        AND     country_id = ISNULL(@l_country_id, country_id)
        AND     location_id = ISNULL(@l_location_id, location_id)
        AND     line_id = ISNULL(@l_line_id, line_id)
        AND     audit_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date
        GROUP BY FORMAT(audit_date,'MMM-yyyy'), CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105)
        UNION
        SELECT 
                    0.0 average_score,
                    AVG(CAST(total_score as numeric(10,5)) )   previous_score,
                    CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,audit_date),'MM-yyyy'),105) a
        FROM    mh_lpa_score_summary
        WHERE   region_id = ISNULL(@l_region_id, region_id)
        AND     country_id = ISNULL(@l_country_id, country_id)
        AND     location_id = ISNULL(@l_location_id, location_id)
        AND     line_id = ISNULL(@l_line_id, line_id)
        AND     audit_date BETWEEN DATEADD(year,-2,@p_date) AND  DATEADD(year,-1,@p_date)
        GROUP BY FORMAT(DATEADD(year,1,audit_date),'MMM-yyyy'), CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,audit_date),'MM-yyyy'),105);

		SET @i = 0;
		WHILE @i < 13
		BEGIN
			SET @l_count = 0;
			SET @l_date = CONVERT(DATE, '01-'+FORMAT(DATEADD(month,-@i,@p_date),'MM-yyyy'),105);
			SELECT @l_count = count(*)
			FROM   @l_tempTable
			WHERE  audit_date = @l_date;

			IF @l_count = 0 
				INSERT INTO @l_tempTable VALUES(0,0, @l_date);

			SET @i = @i+1; 
		END;

		SELECT * FROM (select SUM(avg_score) average_score, 
							SUM(prev_score) previous_score, 
							FORMAT(audit_date,'MMM-yyyy') audit_period,
							audit_date a	
						FROM @l_tempTable
						GROUP BY FORMAT(audit_date,'MMM-yyyy'),
							audit_date) t
						ORDER BY a;
/* 
       SELECT audit_period,
              SUM(average_score) average_score,
              SUM(previous_score) previous_score,
              a
       FROM (
              SELECT FORMAT(audit_date,'MMM-yyyy') audit_period,
                           AVG(total_score) average_score,
                           CAST(0.0 as numeric(10,5)) previous_score,
                           CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105) a
              FROM    mh_lpa_score_summary
              WHERE   region_id = ISNULL(@l_region_id, region_id)
              AND     country_id = ISNULL(@l_country_id, country_id)
              AND     location_id = ISNULL(@l_location_id, location_id)
              AND     line_id = ISNULL(@l_line_id, line_id)
              AND     audit_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date
              GROUP BY FORMAT(audit_date,'MMM-yyyy'), CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105)
              UNION
              SELECT FORMAT(DATEADD(year,1,audit_date),'MMM-yyyy') audit_period,
                           0.0 average_score,
                           AVG(CAST(total_score as numeric(10,5)) )   previous_score,
                           CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,audit_date),'MM-yyyy'),105) a
              FROM    mh_lpa_score_summary
              WHERE   region_id = ISNULL(@l_region_id, region_id)
              AND     country_id = ISNULL(@l_country_id, country_id)
              AND     location_id = ISNULL(@l_location_id, location_id)
              AND     line_id = ISNULL(@l_line_id, line_id)
              AND     audit_date BETWEEN DATEADD(year,-2,@p_date) AND  DATEADD(year,-1,@p_date)
              GROUP BY FORMAT(DATEADD(year,1,audit_date),'MMM-yyyy'), CONVERT(DATE, '01-'+FORMAT(DATEADD(year,+1,audit_date),'MM-yyyy'),105)
              ) tmp
       GROUP BY audit_period, a
       ORDER BY a
       
*/
 
 
END



GO
/****** Object:  StoredProcedure [dbo].[sp_getLPAResultPerSection]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_getLPAResultPerSection null,null,null,null,'01-01-2017'
CREATE procedure [dbo].[sp_getLPAResultPerSection](@p_region_id		INT=null,
											@p_country_id		INT=null,
											@p_location_id		INT=null,
											@p_line_id			INT=null,
											@p_date				DATE)
AS
BEGIN

DECLARE @l_region_id		INT;
	DECLARE @l_country_id		INT;
	DECLARE @l_location_id		INT;
	DECLARE @l_line_id		INT;

	IF @p_region_id IS NULL OR @p_region_id = 0 
		SET @l_region_id = NULL;
	ELSE
		SET @l_region_id = @p_region_id;

	IF @p_country_id IS NULL OR @p_country_id = 0 
		SET @l_country_id = NULL;
	ELSE
		SET @l_country_id = @p_country_id;

	IF @p_location_id IS NULL OR @p_location_id = 0 
		SET @l_location_id = NULL;
	ELSE
		SET @l_location_id = @p_location_id;

	IF @p_line_id IS NULL OR @p_line_id = 0 
		SET @l_line_id = NULL;
	ELSE
		SET @l_line_id = @p_line_id;

	SELECT
			ROUND(AVG(ISNULL(sec1_score,0)),2) Section1_score,
			ROUND(AVG(ISNULL(sec2_score,0)),2) Section2_score,
			ROUND(AVG(ISNULL(sec3_score,0)),2) Section3_score,
			ROUND(AVG(ISNULL(sec4_score,0)),2) Section4_score,
			ROUND(AVG(ISNULL(sec5_score,0)),2) Section5_score,
			ROUND(AVG(ISNULL(sec6_score,0)),2) Section6_score
		INTO #TEMP1
	FROM    mh_lpa_score_summary
	WHERE   region_id = ISNULL(@l_region_id, region_id)
	AND     country_id = ISNULL(@l_country_id, country_id)
	AND     location_id = ISNULL(@l_location_id, location_id)
	AND     line_id = ISNULL(@l_line_id, line_id)
	AND     audit_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date;
	--select * from #TEMP1
	SELECT Section_number, 
	CASE 
						WHEN CHARINDEX('A. ',section_name)>0 THEN REPLACE(sm.section_name,'A. ','') 
						WHEN CHARINDEX('B. ',section_name)>0 THEN REPLACE(sm.section_name,'B. ','') 
						WHEN CHARINDEX('C. ',section_name)>0 THEN REPLACE(sm.section_name,'C. ','') 
						WHEN CHARINDEX('D. ',section_name)>0 THEN REPLACE(sm.section_name,'D. ','') 
						WHEN CHARINDEX('E. ',section_name)>0 THEN REPLACE(sm.section_name,'E. ','') 
						ELSE section_name
						END section_name, ISNULL(Score,0) Score, Sec_name_abbr
	FROM (
		SELECT 1 Section_number, Section1_score Score, 'GT' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 2 Section_number, Section2_score Score, 'A' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 3 Section_number, Section3_score Score, 'B' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 4 Section_number, Section4_score Score, 'C' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 5 Section_number, Section5_score Score, 'D' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 6 Section_number, Section6_score Score, 'E' as Sec_name_abbr
		from #TEMP1
/*		SELECT 2 Section_number, Section2_score Score, 'PD' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 3 Section_number, Section3_score Score, 'SOCWA' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 4 Section_number, Section4_score Score, 'RPE' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 5 Section_number, Section5_score Score, 'SW' as Sec_name_abbr
		from #TEMP1
		UNION ALL
		SELECT 6 Section_number, Section6_score Score, 'RPS' as Sec_name_abbr
		from #TEMP1
*/
	) tmp, mh_lpa_section_master sm
	WHERE tmp.Section_number = sm.section_id
	ORDER BY section_id

END






GO
/****** Object:  StoredProcedure [dbo].[sp_getLPAResultPerSectionMonthly]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_getLPAResultPerSectionMonthly](@p_region_id		INT=null,
											@p_country_id		INT=null,
											@p_location_id		INT=null,
											@p_line_id			INT=null,
											@p_date				DATE)
AS
BEGIN

	DECLARE @l_region_id		INT;
	DECLARE @l_country_id		INT;
	DECLARE @l_location_id		INT;
	DECLARE @l_line_id		INT;
	DECLARE @i					INT;
	DECLARE @l_count				INT;
	DECLARE @l_date				DATE;

	DECLARE @l_tempTable TABLE
	(
	audit_date		DATE,
	sec1_score		NUMERIC(10,5),
	sec2_score		NUMERIC(10,5),
	sec3_score		NUMERIC(10,5),
	sec4_score		NUMERIC(10,5),
	sec5_score		NUMERIC(10,5),
	sec6_score		NUMERIC(10,5)
	);

	IF @p_region_id IS NULL OR @p_region_id = 0 
		SET @l_region_id = NULL;
	ELSE
		SET @l_region_id = @p_region_id;

	IF @p_country_id IS NULL OR @p_country_id = 0 
		SET @l_country_id = NULL;
	ELSE
		SET @l_country_id = @p_country_id;

	IF @p_location_id IS NULL OR @p_location_id = 0 
		SET @l_location_id = NULL;
	ELSE
		SET @l_location_id = @p_location_id;

	IF @p_line_id IS NULL OR @p_line_id = 0 
		SET @l_line_id = NULL;
	ELSE
		SET @l_line_id = @p_line_id;

	INSERT INTO @l_tempTable
	SELECT -- FORMAT(audit_date,'MMM-yyyy') audit_period,
			CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105),
			ISNULL(AVG(sec1_score),0.00) 'GP',
			ISNULL(AVG(sec2_score),0.00) 'PD',
			ISNULL(AVG(sec3_score),0.00) 'SOCWA',
			ISNULL(AVG(sec4_score),0.00) 'RPE',
			ISNULL(AVG(sec5_score),0.00) 'SW',
			ISNULL(AVG(sec6_score),0.00) 'RPS'
	FROM    mh_lpa_score_summary
	WHERE   region_id = ISNULL(@l_region_id, region_id)
	AND     country_id = ISNULL(@l_country_id, country_id)
	AND     location_id = ISNULL(@l_location_id, location_id)
	AND     line_id = ISNULL(@l_line_id, line_id)
	AND     audit_date BETWEEN DATEADD(year,-1,@p_date) AND @p_date
	-- 		  AND     FORMAT(audit_date,'MMM-yyyy') = FORMAT(@p_date,'MMM-yyyy')
	GROUP BY CONVERT(DATE, '01-'+FORMAT(audit_date,'MM-yyyy'),105);


	SET @i = 0;
	WHILE @i < 13
	BEGIN
		SET @l_count = 0;
		SET @l_date = CONVERT(DATE, '01-'+FORMAT(DATEADD(month,-@i,@p_date),'MM-yyyy'),105);
		SELECT @l_count = count(*)
		FROM   @l_tempTable
		WHERE  audit_date = @l_date;

		IF @l_count = 0 
			INSERT INTO @l_tempTable VALUES(@l_date,0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

		SET @i = @i+1; 
	END;

	SELECT * FROM (select 
							FORMAT(audit_date,'MMM-yyyy') audit_period,
							audit_date,
							ISNULL(sec1_score,0)		'GT',
/*							sec2_score		'PD',
							sec3_score		'SOCWA',
							sec4_score		'RPE',
							sec5_score		'SW',
							sec6_score		'RPS'
							*/
							ISNULL(sec2_score,0)		'A',
							ISNULL(sec3_score,0)		'B',
							ISNULL(sec4_score,0)		'C',
							ISNULL(sec5_score,0)		'D',
							ISNULL(sec6_score,0)		'E'
					FROM @l_tempTable) t
					ORDER BY audit_date;


	
END





GO
/****** Object:  StoredProcedure [dbo].[SP_GetMyQuestionListForWeb ]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GetMyQuestionListForWeb ](@p_location_id int)  

AS  
BEGIN  

   SELECT section_id,
		section_name,
		section_display_sequence,
		question_id,
		question,
		na_flag,
		max_no_of_pics_allowed,
		help_text,
		question_display_sequence,
		how_to_check
		FROM (
				SELECT  sm.section_id,
						sm.section_name,
						sm.display_sequence  section_display_sequence,
						qm.question_id,
						qm.question,
						qm.na_flag,
						max_no_of_pics_allowed,
						qm.help_text,
						lq.display_sequence  question_display_sequence,
						qm.how_to_check
				FROM    mh_lpa_question_master qm,
						mh_lpa_local_questions lq,
						mh_lpa_section_master sm
				WHERE  lq.location_id = @p_location_id
				AND    lq.question_id < 10000
				AND    lq.question_id = qm.question_id
				AND    lq.section_id = sm.section_id
				UNION
				SELECT  sm.section_id,
						sm.section_name,
						sm.display_sequence  section_display_sequence,
						qm.local_question_id,
						qm.local_question,
						qm.na_flag,
						max_no_of_pics_allowed,
						qm.local_help_text,
						lq.display_sequence  question_display_sequence,
						qm.how_to_check
				FROM    mh_lpa_local_question_master qm,
						mh_lpa_local_questions lq,
						mh_lpa_section_master sm
				WHERE  lq.location_id = @p_location_id
				AND    lq.question_id = qm.local_question_id
				AND    lq.section_id = sm.section_id
				 ) tmp
		ORDER BY question_display_sequence
         
END






GO
/****** Object:  StoredProcedure [dbo].[sp_GetQuestionsAnswers]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_GetQuestionsAnswers]
(
@audit_id INT
)
AS
BEGIN

declare @sec int
select @sec=Max(section_id) from mh_lpa_local_answers

SELECT audit_id Document_no, 
			audit_date conducted_on,
			dbo.getEmployeeName(la.audited_by_user_id) conducted_by,
			dbo.getAuditScorebyAudit(audit_id) Score
	FROM   mh_lpa_local_answers la,
			mh_lpa_location_master lm
	WHERE la.audit_id = @audit_id
	AND   la.location_id = lm.location_id;


while(@sec>=1)
BEGIN

	SELECT
	msec.section_name,
	mq.display_sequence, 
	mq.question,  
	mh.answer,
	mh.remarks,
	mreg.region_name,
	mc.country_name,
	mloc.location_name,
	mline.line_name
	from mh_lpa_local_answers mh join [dbo].[mh_lpa_question_master] mq on mh.question_id=mq.question_id
	join [dbo].[mh_lpa_section_master] msec on msec.section_id=mh.section_id
	join [dbo].[mh_lpa_region_master] mreg on mreg.region_id=mh.region_id
	join [dbo].[mh_lpa_country_master] mc on mc.country_id=mh.country_id
	join [dbo].[mh_lpa_location_master] mloc on mloc.location_id=mh.location_id
	join [dbo].[mh_lpa_line_master] mline on mh.line_id=mline.line_id
	where audit_id=@audit_id
	AND mh.section_id=@sec
	
	SET @sec-=1
END

END










GO
/****** Object:  StoredProcedure [dbo].[sp_insLineProduct_bulk]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- drop PROCEDURE sp_insLineProduct_bulk
CREATE PROCEDURE [dbo].[sp_insLineProduct_bulk](@p_add_edit_flag		VARCHAR(10),
												@p_parts_table		PartsTableType READONLY
) AS

BEGIN

	DECLARE @l_region_name VARCHAR(100), @l_country_name VARCHAR(100), 
			@l_location_name VARCHAR(100), @l_line_name VARCHAR(100), 
			@l_part_number VARCHAR(100);
	DECLARE @l_err_code INT, @l_err_message VARCHAR(1000);
	DECLARE @l_count	INT;
	DECLARE @l_line_id	INT;
	
	SELECT @l_line_id = line_id
	FROM   @p_parts_table p, 
			mh_lpa_line_master lm, 
			mh_lpa_location_master loc
	WHERE  loc.location_id = lm.location_id
	AND    UPPER(p.location) = UPPER(loc.location_name)
	AND    UPPER(p.linename) = UPPER(lm.line_name);

	IF @l_line_id IS NOT NULL AND UPPER(@p_add_edit_flag) <> 'ADD'
	BEGIN
			EXEC dbo.DelPart @l_line_id, NULL, @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
	END;


	DECLARE l_line_part_cur CURSOR FOR
							SELECT region,country,location,linename,
							LTRIM(RTRIM(m.n.value('.[1]','varchar(8000)'))) AS PartNumber
							FROM
							(
							SELECT region,country,location,linename,
									CAST('<XMLRoot><RowData>' + REPLACE(PartNumber,',','</RowData><RowData>') + '</RowData></XMLRoot>' AS XML) AS x
							FROM   @p_parts_table
							)t
							CROSS APPLY x.nodes('/XMLRoot/RowData')m(n)

	OPEN l_line_part_cur
	FETCH NEXT FROM l_line_part_cur INTO @l_region_name, @l_country_name, @l_location_name, @l_line_name, @l_part_number;

	-- SELECT @l_count = 
	WHILE @@FETCH_STATUS = 0 
	BEGIN

		SELECT @l_count = count(*) 
		FROM mh_lpa_location_master loc,
			 mh_lpa_country_master cm,
			 mh_lpa_region_master rm,
			 mh_lpa_line_master lm
		where UPPER(loc.location_name) = UPPER(@l_location_name)
		AND   UPPER(cm.country_name) = UPPER(@l_country_name)
		AND   UPPER(rm.region_name) = UPPER(@l_region_name)
		AND   UPPER(lm.line_name) = UPPER(@l_line_name)
		AND   lm.location_id = loc.location_id
		ANd   lm.country_id = cm.country_id
		AND   lm.region_id = rm.region_id
		IF @l_count = 0 
		BEGIN
			SET @l_err_code = 100;
			SET @l_err_message = 'Region/Country/Location/Line Combination Does not exist';
			BREAK;
		END;
		ELSE
		BEGIN
			EXEC dbo.InsLineProduct NULL, @l_region_name, @l_country_name, @l_location_name, @l_line_name, @l_part_number, NULL,NULL, @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
			FETCH NEXT FROM l_line_part_cur INTO @l_region_name, @l_country_name, @l_location_name, @l_line_name, @l_part_number;
		END;
	END;


	CLOSE l_line_part_cur;
	DEALLOCATE l_line_part_cur;

	SELECT * from (select @l_err_code err_code, @l_err_message err_message) a;
/*

SELECT EmployeeID,
LTRIM(RTRIM(m.n.value('.[1]','varchar(8000)'))) AS Certs
FROM
(
SELECT EmployeeID,CAST('<XMLRoot><RowData>' + REPLACE(Certs,',','</RowData><RowData>') + '</RowData></XMLRoot>' AS XML) AS x
FROM   @t
)t
CROSS APPLY x.nodes('/XMLRoot/RowData')m(n)
*/
END;



GO
/****** Object:  StoredProcedure [dbo].[sp_saveImageFile]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_saveImageFile]
(
	@p_audit_id		int,
	@p_question_id	int,
	@p_filename		VARCHAR(MAX),
	@p_flag			VARCHAR(20)
)
AS 
BEGIN
		DECLARE @l_err_code		int;
		DECLARE @l_err_message		varchar(100);
		DECLARE @l_mail_to_list VARCHAR(max);
		DECLARE @l_send_note_to_owner VARCHAR(10);

		SELECT  @l_mail_to_list = distribution_list	, @l_send_note_to_owner = 'Y'
	FROM   mh_lpa_line_master
	WHERE line_id =(SELECT distinct line_id 
						FROM [dbo].[mh_lpa_local_answers] 
							WHERE audit_id=@p_audit_id)
		
		SET @l_err_code = 0;
		SET @l_err_message = 'Initializing';
		if(LOWER(@p_flag)='add')
		BEGIN
		IF EXISTS(SELECT image_file_name FROM [dbo].[mh_lpa_local_answers] where audit_id=@p_audit_id AND question_id=@p_question_id)
		UPDATE [dbo].[mh_lpa_local_answers]
		SET image_file_name=image_file_name+';'+@p_filename
		where audit_id=@p_audit_id
		AND question_id=@p_question_id
		ELSE
		UPDATE [dbo].[mh_lpa_local_answers]
		SET image_file_name=@p_filename
		where audit_id=@p_audit_id
		AND question_id=@p_question_id
		END
		else
		UPDATE [dbo].[mh_lpa_local_answers]
		SET image_file_name=@p_filename
		where audit_id=@p_audit_id
		AND question_id=@p_question_id

		SET @l_err_code = 0;
		SET @l_err_message = 'Inserted Successfully';

		SELECT * FROM (SELECT @l_err_code err_code, @l_err_message err_message) a FOR JSON AUTO

		Select @l_mail_to_list mail_to_list, @p_audit_id audit_id
	
END


--[mh_lpa_local_answers] where audit_id=56








GO
/****** Object:  StoredProcedure [dbo].[sp_saveImageForTasks]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_saveImageForTasks]
(
	@p_audit_id		int,
	@p_question_id	int,
	@p_filename		VARCHAR(MAX)
)
AS 
BEGIN
		DECLARE @l_err_code		int;
		DECLARE @l_err_message		varchar(100);
		DECLARE @l_mail_to_list VARCHAR(max);
		DECLARE @l_send_note_to_owner VARCHAR(10);

		
		SET @l_err_code = 0;
		SET @l_err_message = 'Initializing';

		UPDATE [dbo].[mh_lpa_local_answers]
		SET review_image_file_name=@p_filename
		where audit_id=@p_audit_id
		AND question_id=@p_question_id

		SET @l_err_code = 0;
		SET @l_err_message = 'Inserted Successfully';

		SELECT * FROM (SELECT @l_err_code err_code, @l_err_message err_message) a FOR JSON AUTO

	
END


--[mh_lpa_local_answers] where audit_id=62



GO
/****** Object:  StoredProcedure [dbo].[UpdUserPassword]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UpdUserPassword]
(
                @p_user_id               	INT,
                @PASSWORD               	VARCHAR(1000)
)
AS
BEGIN

	DECLARE  @l_err_code 		INT;
	DECLARE  @l_err_message 	VARCHAR(100);
 
--exec InsUserDetails 1, 'ab', 'abc', 'kns',1,'Y','Manager','06-01-2017','06-25-2017',null,'N','Y','Insert'
                -- SET NOCOUNT ON added to prevent extra result sets from
                -- interfering with SELECT statements.
                SET NOCOUNT ON;
 
 	IF @p_user_id IS NOT NULL AND @p_user_id != 0
 
		UPDATE mh_lpa_user_master
		SET   passwd = @PASSWORD
		WHERE user_id = @p_user_id

	SET @l_err_code = 0;
	SET @l_err_message = 'Password Reset Successfully';

	SELECT * FROM (SELECT @l_err_code err_code, @l_err_message err_message) a FOR JSON AUTO

	
END






GO
/****** Object:  StoredProcedure [dbo].[UserLoginforWeb]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[UserLoginforWeb](@p_username   	varchar(100),
										@p_passwd	varchar(1000))
AS 
BEGIN 
 
	SELECT um.user_id,
		um.role,
		um.role_id,
		um.email_id,
		um.emp_full_name,
		um.emp_last_name,
		um.emp_first_name,
		um.display_name_flag,
		um.global_admin_flag,
		um.site_admin_flag,
			um.region_id,
			rm.region_name,
			um.country_id,
			cm.country_name,
			lm.location_id,
			lm.location_name
		FROM   mh_lpa_user_master um,
				mh_lpa_location_master lm,
				mh_lpa_country_master cm,
				mh_lpa_region_master rm
		WHERE um.location_id = lm.location_id
		AND   um.region_id = rm.region_id
		AND   um.country_id = cm.country_id
	AND   um.username = @p_username
	AND   um.passwd = @p_passwd
	AND (um.end_date is null OR end_date>=GETDATE())
	ORDER BY lm.location_name, um.emp_last_name, um.emp_first_name, um.user_id;
	
 
END;





GO
/****** Object:  UserDefinedFunction [dbo].[get_local_admin_email]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[get_local_admin_email](@p_user_id INT)
RETURNS VARCHAR(1000)
AS
BEGIN
 DECLARE @l_count    INT;
 DECLARE @l_location_id   INT;
 DECLARE @l_email   VARCHAR(1000);
 
 SELECT @l_location_id = location_id
 FROM   mh_lpa_user_master 
 WHERE  user_id = @p_user_id;
 SELECT @l_email = COALESCE(@l_email + '; ', '')+ email_id
 FROM   mh_lpa_user_master 
 WHERE  location_id = @l_location_id
 AND    COALESCE(site_admin_flag,'N') = 'Y';
 
 RETURN(@l_email);
 
END;



GO
/****** Object:  UserDefinedFunction [dbo].[get_translated_text_for_question]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[get_translated_text_for_question](@p_location_id        INT,
                                            @p_question_id            INT)
RETURNS nvarchar(1000) AS
 
BEGIN
    DECLARE @l_language        VARCHAR(50);
    DECLARE @l_question        NVARCHAR(1000);
    
    SELECT @l_language = UPPER(COALESCE(default_language,'ENGLISH'))
    FROM   mh_lpa_location_master
    WHERE  location_id = @p_location_id;
    
    -- SET @l_language = 'CHINESE';
    
--    RETURN(
        SELECT @l_question =
                CASE @l_language
                    WHEN 'GERMAN' THEN german
                    WHEN 'SPANISH' THEN SPANISH
                    WHEN 'FRENCH' THEN FRENCH
                    WHEN 'CZECH' THEN CZECH
                    WHEN 'BOSNIAN' THEN BOSNIAN
                    WHEN 'CHINESE' THEN CHINESE
                    WHEN 'KOREAN' THEN KOREAN
                    WHEN 'THAI' THEN THAI
                    WHEN 'PORTUGUESE' THEN PORTUGUESE
                    ELSE ENGLISH
                END
        FROM  mh_lpa_Question_language_master
        WHERE  question_id = @p_question_id;

        IF @l_question IS NULL
            SELECT @l_question = question
            FROM   mh_lpa_question_master
            WHERE  question_id = @p_question_id;

        RETURN @l_question;

END;



GO
/****** Object:  UserDefinedFunction [dbo].[get_translated_text_for_section]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE FUNCTION [dbo].[get_translated_text_for_section](@p_location_id        INT,
                                            @p_section_id            INT)
RETURNS nvarchar(1000) AS
 
BEGIN
    DECLARE @l_language        VARCHAR(50);
    DECLARE @l_question        NVARCHAR(1000);
    
    SELECT @l_language = UPPER(COALESCE(default_language,'ENGLISH'))
    FROM   mh_lpa_location_master
    WHERE  location_id = @p_location_id;
    
    -- SET @l_language = 'CHINESE';
    
--    RETURN(
        SELECT @l_question =
                CASE @l_language
                    WHEN 'GERMAN' THEN german
                    WHEN 'SPANISH' THEN SPANISH
                    WHEN 'FRENCH' THEN FRENCH
                    WHEN 'CZECH' THEN CZECH
                    WHEN 'BOSNIAN' THEN BOSNIAN
                    WHEN 'CHINESE' THEN CHINESE
                    WHEN 'KOREAN' THEN KOREAN
                    WHEN 'THAI' THEN THAI
                    WHEN 'PORTUGUESE' THEN PORTUGUESE
                    ELSE ENGLISH
                END
        FROM  mh_lpa_Section_language_master
        WHERE  section_id = @p_section_id;


        IF @l_question IS NULL
            SELECT @l_question = section_name
            FROM   mh_lpa_section_master
            WHERE  section_id = @p_section_id;

        RETURN @l_question;

END;



GO
/****** Object:  UserDefinedFunction [dbo].[GetAllMyLines]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetAllMyLines](@p_location_id 		int)  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
   SELECT 
			line_name
	FROM   
			mh_lpa_line_master lm
	WHERE  lm.location_id = @p_location_id
	FOR JSON AUTO)  
END






GO
/****** Object:  UserDefinedFunction [dbo].[GetAnswerHistory]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetAnswerHistory](@p_line_id int)  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
    SELECT audit_id,
   audit_date,
   order_by,
   Shift_No,
   question_id,
   answer,
   Score,
   Total
 FROM
    (
  SELECT  la.audit_id,
    FORMAT(la.audit_date,'dd MMM') audit_date,
    la.audit_date order_by,
    la.Shift_No,
    la.question_id,
    la.answer,
    CAST(dbo.GetAuditScorebyAudit(la.audit_id) as numeric(10,2) ) as Score,
    CAST(dbo.GetTotalAnswers(la.audit_id) as INT) as Total
  FROM    mh_lpa_local_answers la
  WHERE  la.audit_date BETWEEN DATEADD(year, -1, CURRENT_TIMESTAMP)  AND CURRENT_TIMESTAMP
  AND    la.line_id = @p_line_id
  ) tmp
  ORDER BY order_by DESC
 FOR JSON AUTO)  
END



GO
/****** Object:  UserDefinedFunction [dbo].[GetAuditPlan]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetAuditPlan](@p_location_id 		int,
							@p_line_id 	int,
							@p_user_id				int)  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
   SELECT audit_plan_id,
   			planned_date, 
			planned_date_end,
			Audit_status,
			location_name,
			line_name,
			Emp_name
	FROM (
		SELECT pln.audit_plan_id,
				FORMAT(pln.planned_date,'dd-MM-yyyy') planned_date, 
				FORMAT(pln.planned_date_end,'dd-MM-yyyy') planned_date_end, 
				loc.location_name,
				lm.line_name,
				dbo.getAuditStatus(audit_plan_id)	Audit_status,
				dbo.getEmployeeName(pln.to_be_audited_by_user_id) AS emp_name
		FROM    mh_lpa_audit_plan pln,
				mh_lpa_line_master lm,
				mh_lpa_location_master loc
		WHERE  pln.planned_date BETWEEN DATEADD(year, -1, CURRENT_TIMESTAMP)  AND DATEADD(year, 1, CURRENT_TIMESTAMP)
		AND    pln.location_id = ISNULL(@p_location_id, pln.location_id)
		AND    pln.line_id = ISNULL(@p_line_id, pln.line_id)
		AND    pln.to_be_audited_by_user_id = ISNULL(@p_user_id, pln.to_be_audited_by_user_id)
		AND    pln.location_id = loc.location_id
		AND    pln.line_id = lm.line_id) tmp
		ORDER BY planned_date DESC 
	FOR JSON AUTO)  
END




GO
/****** Object:  UserDefinedFunction [dbo].[GetAuditScore]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetAuditScore](@p_line_id int,
								@p_duration		varchar(1) )  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
	SELECT audit_id,
			audit_date,
			Shift_No,
			score
	FROM (
		SELECT la.audit_id,
				FORMAT(audit_date,'dd MMM') audit_date,
				la.Shift_No,
				dbo.GetAuditScorebyAudit(audit_id) score  
		FROM    mh_lpa_local_answers la
		WHERE  1= 1
		AND    answer != 2
		AND    la.line_id = @p_line_id
		AND    (
				(@p_duration = 'Y' AND la.audit_date BETWEEN DATEADD(year, -1, CURRENT_TIMESTAMP)  AND CURRENT_TIMESTAMP) OR 
				(@p_duration = 'M' AND la.audit_date BETWEEN DATEADD(month, -1, CURRENT_TIMESTAMP)  AND CURRENT_TIMESTAMP) OR
				(@p_duration = 'W' AND la.audit_date BETWEEN DATEADD(week, -1, CURRENT_TIMESTAMP)  AND CURRENT_TIMESTAMP)
				)
		GROUP BY la.audit_id,
				la.audit_date,
				la.Shift_No) tmp		
		ORDER BY audit_date DESC
		FOR JSON AUTO)  
END






GO
/****** Object:  UserDefinedFunction [dbo].[GetAuditScorebyAudit]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetAuditScorebyAudit](@p_audit_id int)  
 RETURNS FLOAT  
AS  
BEGIN  

	DECLARE @l_total_answers int;
	DECLARE @l_yes_answers	int;
	DECLARE @l_out			FLOAT;

	SELECT @l_out = CAST(CAST((total_score * 100) as FLOAT) as numeric(10,2))
	FROM  mh_lpa_score_summary
	WHERE audit_id = @p_audit_id;
/*
	SELECT @l_yes_answers = count(*)
	FROM   mh_lpa_local_answers
	WHERE  audit_id = @p_audit_id
	-- AND    section_id != 1
	AND    answer = 0;

	SELECT @l_total_answers = count(*)
	FROM   mh_lpa_local_answers
	WHERE  audit_id = @p_audit_id
	-- AND    section_id != 1
	AND    answer in (0,1);
	
	IF @l_total_answers > 0
		SET  @l_out = ROUND(CAST(@l_yes_answers AS FLOAT)/CAST(@l_total_answers AS FLOAT)*100,2) 
	ELSE
		SET @l_out = 0.0
*/
	RETURN @l_out
END





GO
/****** Object:  UserDefinedFunction [dbo].[GetAuditScorebyAuditDate]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE FUNCTION [dbo].[GetAuditScorebyAuditDate](@p_line_id int,
									@p_audit_date			DATE)  
 RETURNS FLOAT  
AS  
BEGIN  

	DECLARE @l_total_answers int;
	DECLARE @l_yes_answers	int;
	DECLARE @l_audit_id		int;
	DECLARE @l_audit_score			FLOAT;


	SELECT @l_audit_score = MIN(dbo.GetAuditScorebyAudit(audit_id)  )
	FROM   mh_lpa_local_answers
	WHERE  line_id = @p_line_id
	AND    FORMAT(audit_date,'dd-MM-YYYY') = FORMAT(@p_audit_date,'dd-MM-YYYY');
	
	RETURN 	@l_audit_score

END





GO
/****** Object:  UserDefinedFunction [dbo].[GetAuditScoreSummary]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetAuditScoreSummary](
								@p_region_id		INT,
								@p_country_id		INT,
								@p_location_id		INT,
								@p_line_id int )  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  

	DECLARE @l_line_id		INT;
	DECLARE @l_audit_id		INT;
	DECLARE @l_region_id	INT;
	DECLARE @l_country_id	INT;
	DECLARE @l_location_id	INT;


	IF @p_line_id IS NOT NULL AND @p_line_id != 0 
		BEGIN
			SET @l_line_id = @p_line_id;
		END;
	ELSE
		BEGIN
			SET @l_line_id = NULL;
		END;
		
	IF @p_region_id IS NOT NULL AND @p_region_id != 0 
		SET @l_region_id = @p_region_id;
	ELSE
		SET @l_region_id = NULL;
		
	IF @p_country_id IS NOT NULL AND @p_country_id != 0 
		SET @l_country_id = @p_country_id;
	ELSE
		SET @l_country_id = NULL;
		
	IF @p_location_id IS NOT NULL AND @p_location_id != 0 
		SET @l_location_id = @p_location_id;
	ELSE
		SET @l_location_id = NULL;
		
	RETURN(
	SELECT sec_id, avg_score, section_name, section_abbr
	FROM (
		SELECT  Sec_id, 
				avg_score,
				section_name,
				CASE
							WHEN section_id = 1 THEN 'GP'
							WHEN section_id = 2 THEN 'PD'
							WHEN section_id = 3 THEN 'SOCWA'
							WHEN section_id = 4 THEN 'RPE'
							WHEN section_id = 5 THEN 'SW'
							WHEN section_id = 6 THEN 'RPS'
							ELSE 'OTHERS'
/*
							WHEN section_name = 'General Topics' THEN 'GP'
							WHEN section_name = 'People Development' THEN 'PD'
							WHEN section_name = 'Safe, Organized, Clean Work Area' THEN 'SOCWA'
							WHEN section_name = 'Robust Processes and Equipment' THEN 'RPE'
							WHEN section_name = 'Standardized Work' THEN 'SW'
							WHEN section_name = 'Rapid Problem Solving / 8D' THEN 'RPS'
							ELSE 'OTHERS'
*/
				END section_abbr
		FROM (
			SELECT 1 as Sec_id, 
					AVG(sec1_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    sec1_score IS NOT NULL
			UNION
			SELECT 2 as Sec_id, 
					AVG(sec2_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    sec2_score IS NOT NULL
			UNION
			SELECT 3 as Sec_id, 
					AVG(sec3_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    sec3_score IS NOT NULL
			UNION
			SELECT 4 as Sec_id, 
					AVG(sec4_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    sec4_score IS NOT NULL
			UNION
			SELECT 5 as Sec_id, 
					AVG(sec5_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    sec5_score IS NOT NULL
			UNION
			SELECT 6 as Sec_id, 
					AVG(sec6_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    sec6_score IS NOT NULL
			) t,
			mh_lpa_section_master sm
			WHERE t.sec_id = sm.section_id
		) tmp
		FOR JSON AUTO)



/*
   RETURN (
   SELECt sec_id,
			section_name,
			section_abbr,
			avg_score,
			tot_answers
	FROM (
			SELECT Sec_id,
					section_name,
					section_abbr,
					CAST(AVG( CAST(yes_answer AS FLOAT)/CAST(tot_answers AS FLOAT)*100) as numeric(10,2) ) avg_score,
					CAST(AVG(tot_answers) as numeric(10,2) ) tot_answers
			FROM (
				SELECT la.audit_id,
						la.audit_date,
						la.Shift_No,
						sm.section_id as sec_id,
						sm.section_name,
						CASE
							WHEN section_name = 'General Topics' THEN 'GP'
							WHEN section_name = 'People Development' THEN 'PD'
							WHEN section_name = 'Safe, Organized, Clean Work Area' THEN 'SOCWA'
							WHEN section_name = 'Robust Processes and Equipment' THEN 'RPE'
							WHEN section_name = 'Standardized Work' THEN 'SW'
							WHEN section_name = 'Rapid Problem Solving / 8D' THEN 'RPS'
							ELSE 'OTHERS'
							END section_abbr,
						SUM(CASE 
							WHEN answer = 0 THEN 1
							ELSE 0
							END) yes_answer,
						SUM(CASE	
							WHEN answer = 0 THEN 1
							WHEN answer = 1 THEN 1
							ELSE 0
							END) tot_answers
				FROM    mh_lpa_local_answers la,
						mh_lpa_section_master sm
				WHERE  la.section_id = sm.section_id
			--	AND   la.section_id != 1
			--	AND    audit_id = dbo.GetLowScoreAuditIdbyDate(line_product_rel_id,audit_date)
				AND   answer != 2
				AND    ( 
						(ISNULL(@p_line_product_rel_id,0) != 0 AND la.line_product_rel_id = @p_line_product_rel_id) OR
						ISNULL(@p_line_product_rel_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_region_id,0) != 0 AND la.region_id = @p_region_id) OR
						ISNULL(@p_region_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_country_id,0) != 0 AND la.country_id = @p_country_id) OR
						ISNULL(@p_country_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_location_id,0) != 0 AND la.location_id = @p_location_id) OR
						ISNULL(@p_location_id,0) = 0
						)
				AND    FORMAT(la.audit_date,'MM-YYYY') = FORMAT(CURRENT_TIMESTAMP,'MM-YYYY')
				GROUP BY la.audit_id,
						la.audit_date,
						la.Shift_No,
						sm.section_id,
						section_name
				HAVING SUM(CASE	
							WHEN answer = 0 THEN 1
							WHEN answer = 1 THEN 1
							ELSE 0
							END) != 0
				) tmp	
		GROUP BY 
			sec_id, section_name, section_abbr
			) t	
		FOR JSON AUTO)  
*/
END






GO
/****** Object:  UserDefinedFunction [dbo].[GetAuditScoreSummaryTotal]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetAuditScoreSummaryTotal](
								@p_region_id		INT,
								@p_country_id		INT,
								@p_location_id		INT,
								@p_line_id int )  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  

	DECLARE @l_line_id		INT;
	DECLARE @l_audit_id		INT;
	DECLARE @l_region_id	INT;
	DECLARE @l_country_id	INT;
	DECLARE @l_location_id	INT;


	IF @p_line_id IS NOT NULL AND @p_line_id != 0 
		BEGIN
			SET @l_line_id = @p_line_id;
		END;
	ELSE
		BEGIN
			SET @l_line_id = NULL;
		END;
		
	IF @p_region_id IS NOT NULL AND @p_region_id != 0 
		SET @l_region_id = @p_region_id;
	ELSE
		SET @l_region_id = NULL;
		
	IF @p_country_id IS NOT NULL AND @p_country_id != 0 
		SET @l_country_id = @p_country_id;
	ELSE
		SET @l_country_id = NULL;
		
	IF @p_location_id IS NOT NULL AND @p_location_id != 0 
		SET @l_location_id = @p_location_id;
	ELSE
		SET @l_location_id = NULL;
		

	RETURN(
		SELECT avg_score
		FROM
			(SELECT AVG(total_score)*100 avg_score
			FROM   mh_lpa_score_summary
			WHERE  line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			) t
		FOR JSON AUTO)

/*
   RETURN (
   SELECt avg_score
   	FROM (
			SELECT 
					CAST(AVG( CAST(yes_answer AS FLOAT)/CAST(tot_answers AS FLOAT)*100) as numeric(10,2) ) avg_score
			FROM (
				SELECT la.audit_id,
						la.audit_date,
						la.Shift_No,
						SUM(CASE 
							WHEN answer = 0 THEN 1
							ELSE 0
							END) yes_answer,
						SUM(CASE	
							WHEN answer = 0 THEN 1
							WHEN answer = 1 THEN 1
							ELSE 0
							END) tot_answers
				FROM    mh_lpa_local_answers la
				WHERE  1=1
			--	and    audit_id = dbo.GetLowScoreAuditIdbyDate(line_product_rel_id,audit_date)
				AND   section_id != 1
				AND   answer != 2
				AND    ( 
						(ISNULL(@p_line_product_rel_id,0) != 0 AND la.line_product_rel_id = @p_line_product_rel_id) OR
						ISNULL(@p_line_product_rel_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_region_id,0) != 0 AND la.region_id = @p_region_id) OR
						ISNULL(@p_region_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_country_id,0) != 0 AND la.country_id = @p_country_id) OR
						ISNULL(@p_country_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_location_id,0) != 0 AND la.location_id = @p_location_id) OR
						ISNULL(@p_location_id,0) = 0
						)
				AND    FORMAT(la.audit_date,'MM-YYYY') = FORMAT(CURRENT_TIMESTAMP,'MM-YYYY')
				GROUP BY la.audit_id,
						la.audit_date,
						la.Shift_No
				HAVING SUM(CASE	
							WHEN answer = 0 THEN 1
							WHEN answer = 1 THEN 1
							ELSE 0
							END) != 0
				) tmp	
			) t	
		FOR JSON AUTO)  

*/
END





GO
/****** Object:  UserDefinedFunction [dbo].[getAuditStatus]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getAuditStatus](@p_audit_plan_id int)
RETURNS INT
AS
BEGIN
 DECLARE @l_location_id  INT;
 DECLARE @l_count   INT;
 DECLARE @l_audit_start_date  DATE;
 DECLARE @l_audit_end_date  DATE;
 DECLARE @l_audit_status   INT;
 DECLARE @l_line_id    INT;
 SELECT @l_line_id = line_id,
   @l_location_id = location_id,
   @l_audit_start_Date = planned_date,
   @l_audit_end_date = planned_date_end
 FROM   mh_lpa_audit_plan p
 WHERE  audit_plan_id = @p_audit_plan_id;
 SELECT @l_count = count(*)
 FROM   mh_lpa_local_answers
 WHERE  location_id = @l_location_id
 AND    line_id = @l_line_id
 AND    CAST(audit_date as date) BETWEEN @l_audit_start_Date AND @l_audit_end_date;
 IF @l_count > 0
  SET @l_audit_status =  1; /* Completed */
 ELSE IF @l_audit_end_date < GETDATE()
   SET @l_audit_status = 0; /* Missed */
  ELSE
   SET @l_audit_status = 2; /* Planned */
 RETURN @l_audit_status;
END;



GO
/****** Object:  UserDefinedFunction [dbo].[GetCountryList]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetCountryList](@p_user_id INT,
								@p_region_id int)
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
   SELECT country_id,
   			country_code, 
		 	country_name
	FROM (
		SELECT distinct cm.country_id,
			country_code,
			country_name
		FROM   mh_lpa_country_master cm,
		mh_lpa_line_master lm
		WHERE cm.country_id = lm.country_id
		AND   lm.region_id = @p_region_id
		) tmp
		order by country_name
	FOR JSON AUTO)  
END






GO
/****** Object:  UserDefinedFunction [dbo].[getEmployeeName]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getEmployeeName](@p_user_id int)  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
		SELECT CASE
					-- WHEN display_name_flag = 'N' THEN emp_first_name + ' ' + emp_last_name
					WHEN display_name_flag = 'N' THEN emp_full_name
					ELSE 'User'
					END Emp_name
		FROM    mh_lpa_user_master usr
		WHERE   usr.user_id = @p_user_id
		)  
END





GO
/****** Object:  UserDefinedFunction [dbo].[getEmployeeName_NoMask]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getEmployeeName_NoMask](@p_user_id int)  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
		SELECT emp_full_name
		FROM    mh_lpa_user_master usr
		WHERE   usr.user_id = @p_user_id
		)  
END



GO
/****** Object:  UserDefinedFunction [dbo].[getGroupName]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getGroupName](@p_group_id int)  
 RETURNS NVARCHAR(1000)  
AS  
BEGIN  
   RETURN (
		SELECT group_name
		FROM    mh_lpa_group_master usr
		WHERE   usr.group_id = @p_group_id
		)  
END




GO
/****** Object:  UserDefinedFunction [dbo].[GetLatestAuditScoreSummary]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetLatestAuditScoreSummary](
								@p_region_id		INT,
								@p_country_id		INT,
								@p_location_id		INT,
								@p_line_id int )  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  

	DECLARE @l_line_id		INT;
	DECLARE @l_audit_id		INT;
	DECLARE @l_region_id	INT;
	DECLARE @l_country_id	INT;
	DECLARE @l_location_id	INT;


	IF @p_line_id IS NOT NULL AND @p_line_id != 0 
		BEGIN
			SET @l_line_id = @p_line_id;

			SELECT @l_audit_id = MAX(audit_id) 
			FROM   mh_lpa_local_answers
			WHERE line_id = @l_line_id;
		END;
	ELSE
		BEGIN
			SET @l_line_id = NULL;
			SET @l_audit_id = NULL;
		END;
		
	IF @p_region_id IS NOT NULL AND @p_region_id != 0 
		SET @l_region_id = @p_region_id;
	ELSE
		SET @l_region_id = NULL;
		
	IF @p_country_id IS NOT NULL AND @p_country_id != 0 
		SET @l_country_id = @p_country_id;
	ELSE
		SET @l_country_id = NULL;
		
	IF @p_location_id IS NOT NULL AND @p_location_id != 0 
		SET @l_location_id = @p_location_id;
	ELSE
		SET @l_location_id = NULL;
		
	RETURN(
	SELECT sec_id, avg_score, section_name, section_abbr
	FROM (
		SELECT  Sec_id, 
				avg_score,
				section_name,
				CASE
							WHEN section_id = 1 THEN 'GP'
							WHEN section_id = 2 THEN 'PD'
							WHEN section_id = 3 THEN 'SOCWA'
							WHEN section_id = 4 THEN 'RPE'
							WHEN section_id = 5 THEN 'SW'
							WHEN section_id = 6 THEN 'RPS'
							ELSE 'OTHERS'
/*
							WHEN section_name = 'General Topics' THEN 'GP'
							WHEN section_name = 'People Development' THEN 'PD'
							WHEN section_name = 'Safe, Organized, Clean Work Area' THEN 'SOCWA'
							WHEN section_name = 'Robust Processes and Equipment' THEN 'RPE'
							WHEN section_name = 'Standardized Work' THEN 'SW'
							WHEN section_name = 'Rapid Problem Solving / 8D' THEN 'RPS'
							ELSE 'OTHERS'
*/
				END section_abbr
		FROM (
			SELECT 1 as Sec_id, 
					AVG(sec1_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  audit_id = ISNULL(@l_audit_id, audit_id)
			AND    line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    sec1_score IS NOT NULL
			UNION
			SELECT 2 as Sec_id, 
					AVG(sec2_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  audit_id = ISNULL(@l_audit_id, audit_id)
			AND    line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    sec2_score IS NOT NULL
			UNION
			SELECT 3 as Sec_id, 
					AVG(sec3_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  audit_id = ISNULL(@l_audit_id, audit_id)
			AND    line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    sec3_score IS NOT NULL
			UNION
			SELECT 4 as Sec_id, 
					AVG(sec4_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  audit_id = ISNULL(@l_audit_id, audit_id)
			AND    line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    sec4_score IS NOT NULL
			UNION
			SELECT 5 as Sec_id, 
					AVG(sec5_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  audit_id = ISNULL(@l_audit_id, audit_id)
			AND    line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    sec5_score IS NOT NULL
			UNION
			SELECT 6 as Sec_id, 
					AVG(sec6_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  audit_id = ISNULL(@l_audit_id, audit_id)
			AND    line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    sec6_score IS NOT NULL
			) t,
			mh_lpa_section_master sm
			WHERE t.sec_id = sm.section_id
		) tmp
		FOR JSON AUTO)
/*
   RETURN (
   SELECt sec_id,
			section_name,
			section_abbr,
			avg_score,
			tot_answers
	FROM (
			SELECT Sec_id,
					section_name,
					section_abbr,
					CAST(AVG( CAST(yes_answer AS FLOAT)/CAST(tot_answers AS FLOAT)*100) as numeric(10,2) ) avg_score,
					CAST(AVG(tot_answers) as numeric(10,2) ) tot_answers
			FROM (
				SELECT la.audit_id,
						la.audit_date,
						la.Shift_No,
						sm.section_id as sec_id,
						sm.section_name,
						CASE
							WHEN section_name = 'General Topics' THEN 'GP'
							WHEN section_name = 'People Development' THEN 'PD'
							WHEN section_name = 'Safe, Organized, Clean Work Area' THEN 'SOCWA'
							WHEN section_name = 'Robust Processes and Equipment' THEN 'RPE'
							WHEN section_name = 'Standardized Work' THEN 'SW'
							WHEN section_name = 'Rapid Problem Solving / 8D' THEN 'RPS'
							ELSE 'OTHERS'
							END section_abbr,
						SUM(CASE 
							WHEN answer = 0 THEN 1
							ELSE 0
							END) yes_answer,
						SUM(CASE	
							WHEN answer = 0 THEN 1
							WHEN answer = 1 THEN 1
							ELSE 0
							END) tot_answers
				FROM    mh_lpa_local_answers la,
						mh_lpa_section_master sm
				WHERE  la.section_id = sm.section_id
				--and    la.section_id != 1
--				AND    audit_id = (select max(audit_id) from mh_lpa_local_answers where line_product_rel_id = @p_line_product_rel_id)
				AND   answer != 2
				AND    ( 
						(ISNULL(@p_line_id,0) != 0 AND  audit_id = (select max(audit_id) from mh_lpa_local_answers loc, mh_lpa_line_product_relationship rel 
																				where loc.line_product_rel_id = rel.line_product_rel_id
																				and   rel.line_id = @p_line_id)) OR
						ISNULL(@p_line_id,0) = 0
						)
*
				AND    ( 
						(ISNULL(@p_line_product_rel_id,0) != 0 AND la.line_product_rel_id = @p_line_product_rel_id) OR
						ISNULL(@p_line_product_rel_id,0) = 0
						)
*
				AND    ( 
						(ISNULL(@p_region_id,0) != 0 AND la.region_id = @p_region_id) OR
						ISNULL(@p_region_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_country_id,0) != 0 AND la.country_id = @p_country_id) OR
						ISNULL(@p_country_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_location_id,0) != 0 AND la.location_id = @p_location_id) OR
						ISNULL(@p_location_id,0) = 0
						)
				-- AND    FORMAT(la.audit_date,'MM-YYYY') = FORMAT(CURRENT_TIMESTAMP,'MM-YYYY')
				GROUP BY la.audit_id,
						la.audit_date,
						la.Shift_No,
						sm.section_id,
						section_name
				HAVING SUM(CASE	
							WHEN answer = 0 THEN 1
							WHEN answer = 1 THEN 1
							ELSE 0
							END) != 0
				) tmp	
		GROUP BY 
			sec_id, section_name, section_abbr
			) t	
		FOR JSON AUTO) 
		
*/ 
END






GO
/****** Object:  UserDefinedFunction [dbo].[GetLatestAuditScoreSummaryTotal]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetLatestAuditScoreSummaryTotal](
								@p_region_id		INT,
								@p_country_id		INT,
								@p_location_id		INT,
								@p_line_id int )  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  

	DECLARE @l_line_id		INT;
	DECLARE @l_audit_id		INT;
	DECLARE @l_region_id	INT;
	DECLARE @l_country_id	INT;
	DECLARE @l_location_id	INT;


	IF @p_line_id IS NOT NULL AND @p_line_id != 0 
		BEGIN
			SET @l_line_id = @p_line_id;

			SELECT @l_audit_id = MAX(audit_id) 
			FROM   mh_lpa_local_answers
			WHERE line_id = @l_line_id;
		END;
	ELSE
		BEGIN
			SET @l_line_id = NULL;
			SET @l_audit_id = NULL;
		END;
		
	IF @p_region_id IS NOT NULL AND @p_region_id != 0 
		SET @l_region_id = @p_region_id;
	ELSE
		SET @l_region_id = NULL;
		
	IF @p_country_id IS NOT NULL AND @p_country_id != 0 
		SET @l_country_id = @p_country_id;
	ELSE
		SET @l_country_id = NULL;
		
	IF @p_location_id IS NOT NULL AND @p_location_id != 0 
		SET @l_location_id = @p_location_id;
	ELSE
		SET @l_location_id = NULL;
		

	RETURN(
		SELECT avg_score
		FROM
			(SELECT AVG(total_score)*100 avg_score
			FROM   mh_lpa_score_summary
			WHERE  audit_id = ISNULL(@l_audit_id, audit_id)
			AND    line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			) t
		FOR JSON AUTO)
/*

   RETURN (
   SELECT 
			avg_score
	FROM (
			SELECT 
					CAST(AVG( CAST(yes_answer AS FLOAT)/CAST(tot_answers AS FLOAT)*100) as numeric(10,2) ) avg_score
			FROM (
				SELECT la.audit_id,
						la.audit_date,
						la.Shift_No,
						SUM(CASE 
							WHEN answer = 0 THEN 1
							ELSE 0
							END) yes_answer,
						SUM(CASE	
							WHEN answer = 0 THEN 1
							WHEN answer = 1 THEN 1
							ELSE 0
							END) tot_answers
				FROM    mh_lpa_local_answers la, mh_lpa_line_product_relationship rel
				WHERE  1=1 
				AND    rel.line_id = @p_line_id
				AND    rel.line_product_rel_id = la.line_product_rel_id
				AND    ( 
						(ISNULL(@p_line_id,0) != 0 AND  audit_id = (select max(audit_id) 
																	from mh_lpa_local_answers l1, mh_lpa_line_product_relationship rel1
																	where line_id = @p_line_id
																	and   rel1.line_product_rel_id = l1.line_product_rel_id)) OR
						ISNULL(@p_line_id,0) = 0
						)
--				and    la.section_id != 1
				AND   answer != 2
*				AND    ( 
						(ISNULL(@p_line_product_rel_id,0) != 0 AND la.line_product_rel_id = @p_line_product_rel_id) OR
						ISNULL(@p_line_product_rel_id,0) = 0
						)
*
				AND    ( 
						(ISNULL(@p_region_id,0) != 0 AND la.region_id = @p_region_id) OR
						ISNULL(@p_region_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_country_id,0) != 0 AND la.country_id = @p_country_id) OR
						ISNULL(@p_country_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_location_id,0) != 0 AND la.location_id = @p_location_id) OR
						ISNULL(@p_location_id,0) = 0
						)
				-- AND    FORMAT(la.audit_date,'MM-YYYY') = FORMAT(CURRENT_TIMESTAMP,'MM-YYYY')
				GROUP BY la.audit_id,
						la.audit_date,
						la.Shift_No
				HAVING SUM(CASE	
							WHEN answer = 0 THEN 1
							WHEN answer = 1 THEN 1
							ELSE 0
							END) != 0
				) tmp	
			) t	
		FOR JSON AUTO)  

*/
END





GO
/****** Object:  UserDefinedFunction [dbo].[getLineProductForLocation]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[getLineProductForLocation](@p_location_id   int,
         @p_user_id    int)  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN 
   RETURN (
   SELECT 
      line_name, 
   line_id
 FROM mh_lpa_line_master 
 WHERE location_id = @p_location_id 
and ISNULL(end_date , dateadd(day,1,getdate())) > getdate()	  
 order by line_name
 FOR JSON AUTO)  
/* 
   RETURN (
   SELECT line_product_rel_id,
      line_name, 
   line_id,
    product_code,
   product_name,
   Shift_no
 FROM (
  SELECT rel.line_product_rel_id,
    -- line_name+'('+product_code+' - '+product_name+')' line_name,
    l.line_id line_id,
    line_name line_name,
    product_code,
    product_name,
    dbo.getShift(rel.location_id) as Shift_no
    *
    CASE 
     WHEN CAST('1900-01-01 ' + FORMAT(getdate(),'HH:mm:ss')+' '+loc.timezone_code as datetimeoffset) 
       between  CAST('1900-01-01 ' + FORMAT(loc.shift1_start_time,'HH:mm')+':00 '+loc.timezone_code as datetimeoffset )
         and  CAST('1900-01-01 ' + FORMAT(loc.shift1_end_time,'HH:mm')+':00 '+loc.timezone_code as datetimeoffset ) THEN 1
     WHEN CAST('1900-01-01 ' + FORMAT(getdate(),'HH:mm:ss')+' '+loc.timezone_code as datetimeoffset) 
       between  CAST('1900-01-01 ' + FORMAT(loc.shift2_start_time,'HH:mm')+':00 '+loc.timezone_code as datetimeoffset )
         and  CAST('1900-01-01 ' + FORMAT(loc.shift2_end_time,'HH:mm')+':00 '+loc.timezone_code as datetimeoffset ) THEN 2
     WHEN CAST('1900-01-01 ' + FORMAT(getdate(),'HH:mm:ss')+' '+loc.timezone_code as datetimeoffset) 
       between  CAST('1900-01-01 ' + FORMAT(loc.shift3_start_time,'HH:mm')+':00 '+loc.timezone_code as datetimeoffset )
         and  CAST('1900-01-01 ' + FORMAT(loc.shift3_end_time,'HH:mm')+':00 '+loc.timezone_code as datetimeoffset ) THEN 3
     ELSE 1
    END as Shift_no
    *
  FROM   mh_lpa_line_product_relationship rel,
   mh_lpa_line_master l,
   mh_lpa_product_master p,
   mh_lpa_location_master loc
  WHERE  rel.line_id = l.line_id
  AND    rel.location_id = loc.location_id
  AND    rel.product_id = p.product_id
  AND    rel.location_id = @p_location_id
  ) tmp
 FOR JSON AUTO)  
*/
END

GO
/****** Object:  UserDefinedFunction [dbo].[GetLocationList]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetLocationList](@p_user_id INT,
								@p_country_id int)
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
   SELECT location_id,
   			location_code, 
		 	location_name
	FROM (
		SELECT distinct cm.location_id,
			location_code,
			location_name
		FROM   mh_lpa_location_master cm,
			mh_lpa_line_master lm
		WHERE cm.location_id = lm.location_id
		AND   lm.country_id = @p_country_id
		
		) tmp

		order by location_name
	FOR JSON AUTO)  
END





GO
/****** Object:  UserDefinedFunction [dbo].[GetLowScoreAuditIdbyDate]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetLowScoreAuditIdbyDate](@p_line_id int,
									@p_audit_date			DATE)  
 RETURNS FLOAT  
AS  
BEGIN  

	DECLARE @l_total_answers int;
	DECLARE @l_yes_answers	int;
	DECLARE @l_audit_id		int;
	DECLARE @l_audit_score			FLOAT;
	DECLARE @l_count		int;

	SELECT @l_count = count(*)
	FROM   mh_lpa_local_answers
	WHERE  line_id = @p_line_id
	AND    FORMAT(audit_date,'dd-MM-YYYY') = FORMAT(@p_audit_date,'dd-MM-YYYY');

	IF @l_count < 40
		SELECT TOP 1 @l_audit_id = audit_id
		FROM   mh_lpa_local_answers
		WHERE  line_id = @p_line_id
		AND    FORMAT(audit_date,'dd-MM-YYYY') = FORMAT(@p_audit_date,'dd-MM-YYYY')
	ELSE

		SELECT TOP 1 @l_audit_id = audit_id
		FROM   mh_lpa_local_answers
		WHERE  line_id = @p_line_id
		AND    FORMAT(audit_date,'dd-MM-YYYY') = FORMAT(@p_audit_date,'dd-MM-YYYY')
		AND    dbo.GetAuditScorebyAudit(audit_id) = dbo.GetAuditScorebyAuditDate(line_id,audit_date);
	
	RETURN 	@l_audit_id

END





GO
/****** Object:  UserDefinedFunction [dbo].[GetMyQuestionList]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetMyQuestionList](@p_location_id int)  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
   SELECT section_id,
		section_name,
		section_display_sequence,
		question_id,
		question,
		na_flag,
		max_no_of_pics_allowed,
		help_text_full,
		help_text,
		how_to_check,
		question_display_sequence
		FROM (
				SELECT  sm.section_id,
						sm.section_name,
						sm.display_sequence  section_display_sequence,
						qm.question_id,
						qm.question,
						ISNULL(qm.na_flag,'N') na_flag,
						max_no_of_pics_allowed,
						'What Should be in Place? \n\n'+qm.help_text+'\n\n\n How to check it? \n\n'+qm.how_to_check help_text_full,
						qm.help_text,
						qm.how_to_check,
						lq.display_sequence  question_display_sequence
				FROM    mh_lpa_question_master qm,
						mh_lpa_local_questions lq,
						mh_lpa_section_master sm
				WHERE  lq.location_id = @p_location_id
				AND    lq.question_id < 10000
				AND    lq.question_id = qm.question_id
				AND    qm.section_id = sm.section_id
				UNION
				SELECT  sm.section_id,
						sm.section_name,
						sm.display_sequence  section_display_sequence,
						qm.local_question_id,
						qm.local_question,
						ISNULL(qm.na_flag,'N') na_flag,
						max_no_of_pics_allowed,
						'What Should be in Place? \n\n'+qm.local_help_text+'\n\n\nHow to check it? \n\n'+qm.how_to_check help_text_full,
						qm.local_help_text,
						qm.how_to_check,
						lq.display_sequence  question_display_sequence
				FROM    mh_lpa_local_question_master qm,
						mh_lpa_local_questions lq,
						mh_lpa_section_master sm
				WHERE  lq.location_id = @p_location_id
				AND    lq.question_id = qm.local_question_id
				AND    lq.section_id = sm.section_id
				 ) tmp
		ORDER BY question_display_sequence
        FOR JSON AUTO)  
END






GO
/****** Object:  UserDefinedFunction [dbo].[GetMyQuestionList_new]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetMyQuestionList_new](@p_line_id int)  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
   SELECT section_id,
		section_name,
		section_display_sequence,
		question_id,
		question,
		dbo.IsCommentsExists(@p_line_id, question_id) comment_available,
		na_flag,
		max_no_of_pics_allowed,
		help_text_full,
		help_text,
		how_to_check,
		question_display_sequence
		FROM (
				SELECT  sm.section_id,
						sm.section_name,
						sm.display_sequence  section_display_sequence,
						qm.question_id,
						qm.question,
						ISNULL(qm.na_flag,'N') na_flag,
						max_no_of_pics_allowed,
						'What Should be in Place? \n\n'+qm.help_text+'\n\n\n How to check it? \n\n'+qm.how_to_check help_text_full,
						qm.help_text,
						qm.how_to_check,
						lq.display_sequence  question_display_sequence
				FROM    mh_lpa_question_master qm,
						mh_lpa_local_questions lq,
						mh_lpa_section_master sm,
						mh_lpa_line_master lm
				WHERE  lm.line_id = @p_line_id
				AND    lq.location_id = lm.location_id
				AND    lq.question_id < 10000
				AND    lq.question_id = qm.question_id
				AND    qm.section_id = sm.section_id
				UNION
				SELECT  sm.section_id,
						sm.section_name,
						sm.display_sequence  section_display_sequence,
						qm.local_question_id,
						qm.local_question,
						ISNULL(qm.na_flag,'N') na_flag,
						max_no_of_pics_allowed,
						'What Should be in Place? \n\n'+qm.local_help_text+'\n\n\nHow to check it? \n\n'+qm.how_to_check help_text_full,
						qm.local_help_text,
						qm.how_to_check,
						lq.display_sequence  question_display_sequence
				FROM    mh_lpa_local_question_master qm,
						mh_lpa_local_questions lq,
						mh_lpa_section_master sm,
						mh_lpa_line_master lm
				WHERE  lm.line_id = @p_line_id
				AND    lq.location_id = lm.location_id
				AND    lq.question_id = qm.local_question_id
				AND    lq.section_id = sm.section_id
				 ) tmp
		ORDER BY question_display_sequence
        FOR JSON AUTO)  
END






GO
/****** Object:  UserDefinedFunction [dbo].[GetMyTasks]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[GetMyTasks](@p_line_id int)  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
   SELECT loc_answer_id,
		audit_id,
		Shift_No,
		section_id,
		section_name,
		section_display_sequence,
		question_id,
		question,
		help_text,
		question_display_sequence,
		remarks,
		image_file_name
	FROM (
		SELECT  la.loc_answer_id,
				la.audit_id,
				la.Shift_No,
				sm.section_id,
				sm.section_name,
				sm.display_sequence  section_display_sequence,
				qm.question_id,
				qm.question,
				qm.help_text,
				lq.display_sequence  question_display_sequence,
				la.remarks,
				la.image_file_name
		FROM    mh_lpa_local_answers la,
				mh_lpa_question_master qm,
				mh_lpa_local_questions lq,
				mh_lpa_section_master sm
		WHERE  la.answer = 1
		AND    la.audit_id = (select max(audit_id) from mh_lpa_local_answers loc 
							where loc.line_id = @p_line_id)
		AND    la.question_id = qm.question_id
		AND    la.question_id < 10000
		AND    lq.question_id = qm.question_id
		AND    lq.location_id = la.location_id
		AND    lq.section_id = sm.section_id
		AND    ISNULL(la.review_closed_status,0) = 0
		UNION
		SELECT  la.loc_answer_id,
				la.audit_id,
				la.Shift_No,
				sm.section_id,
				sm.section_name,
				sm.display_sequence  section_display_sequence,
				qm.local_question_id,
				qm.local_question,
				qm.local_help_text,
				lq.display_sequence  question_display_sequence,
				la.remarks,
				la.image_file_name
		FROM    mh_lpa_local_answers la,
				mh_lpa_local_question_master qm,
				mh_lpa_local_questions lq,
				mh_lpa_section_master sm
		WHERE  la.answer = 1
		AND    la.audit_id = (select max(audit_id) 
							from mh_lpa_local_answers la1 
							where line_id = @p_line_id)
		AND    la.question_id = qm.local_question_id
		AND    la.question_id >= 10000
		AND    lq.question_id = qm.local_question_id
		AND    lq.location_id = la.location_id
		AND    lq.section_id = sm.section_id
		AND    ISNULL(la.review_closed_status,0) = 0
		 ) tmp
	ORDER BY section_display_sequence, question_display_sequence
    FOR JSON AUTO)  
END




GO
/****** Object:  UserDefinedFunction [dbo].[GetQRDetails]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetQRDetails](@p_line_id int)  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
   SELECT 
			region_name,
			region_id,
			country_name,
			country_id,
			location_name,
			line_name,
			location_id,
			line_id,
			shift_no
   FROM (
   		SELECT DISTINCT 
				rm.region_name,
				rm.region_id,
				cm.country_name,
				cm.country_id,
		   		loc.location_name,
				lm.line_name,
				lm.location_id,
				lm.line_id,
				dbo.getShift(loc.location_id) Shift_no
		FROM mh_lpa_line_master lm,
			mh_lpa_location_master loc,
			mh_lpa_country_master cm,
			mh_lpa_region_master rm
		WHERE lm.line_id = @p_line_id
		AND   lm.location_id = loc.location_id
		AND   lm.country_id = cm.country_id
		AND   lm.region_id = rm.region_id
		and ISNULL(lm.end_date , dateadd(day,1,getdate())) > getdate()	  

		) tmp
    FOR JSON AUTO)  
END















GO
/****** Object:  UserDefinedFunction [dbo].[GetRegionList]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetRegionList](@p_user_id INT)
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
		SELECT region_id,
			region_code,
			region_name
		FROM   mh_lpa_region_master
		order by region_name
	FOR JSON AUTO)  
END





GO
/****** Object:  UserDefinedFunction [dbo].[GetReviewHistory]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetReviewHistory](@p_line_id int,
        @p_question_id   int)  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
  SELECT  
    FORMAT(la.audit_date,'dd-MMM-yyyy') audit_date,
    la.remarks,
    ISNULL(la.image_file_name,' ') image_file_name,
    ISNULL(FORMAT(la.review_closed_on,'dd-MMM-yyyy'),' ') review_closed_on,
    ISNULL(la.review_closed_status,' ') review_closed_status,
    ISNULL(la.review_comments,' ')   review_comments,
    ISNULL(la.review_image_file_name,' ') review_image_file_name
  FROM    mh_lpa_local_answers la
  WHERE  la.audit_date BETWEEN DATEADD(month, -1, CURRENT_TIMESTAMP)  AND CURRENT_TIMESTAMP
  AND    la.line_id = @p_line_id
  AND    la.question_id = ISNULL(@p_question_id,la.question_id)
  AND la.answer=1
  ORDER BY la.audit_date DESC
  FOR JSON AUTO)  
END



GO
/****** Object:  UserDefinedFunction [dbo].[Getshift]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Getshift](@p_location_id  int)
RETURNS INT  
AS  
BEGIN  
 DECLARE @l_timezone_code varchar(100);
 DECLARE @l_s1_start   datetimeoffset;
 DECLARE @l_s1_end   datetimeoffset;
 DECLARE @l_s2_start   datetimeoffset;
 DECLARE @l_s2_end   datetimeoffset;
 DECLARE @l_s3_start   datetimeoffset;
 DECLARE @l_s3_end   datetimeoffset;
 DECLARE @l_cur_time   datetimeoffset;
 DECLARE @l_shift   int;
/*
 WHEN CAST('1900-01-01 ' + FORMAT(getdate(),'HH:mm:ss')+' '+loc.timezone_code as datetimeoffset) 
  between  CAST('1900-01-01 ' + FORMAT(CAST(loc.shift1_start_time as datetime),'HH:mm')+':00 '+loc.timezone_code as datetimeoffset )
    and  CAST('1900-01-01 ' + FORMAT(CAST(loc.shift1_end_time as datetime),'HH:mm')+':00 '+loc.timezone_code as datetimeoffset ) THEN 1
WHEN CAST('1900-01-01 ' + FORMAT(getdate(),'HH:mm:ss')+' '+loc.timezone_code as datetimeoffset) 
  between  CAST('1900-01-01 ' + FORMAT(CAST(loc.shift2_start_time as datetime),'HH:mm')+':00 '+loc.timezone_code as datetimeoffset )
    and  CAST('1900-01-01 ' + FORMAT(CAST(loc.shift2_end_time as datetime),'HH:mm')+':00 '+loc.timezone_code as datetimeoffset ) THEN 2
WHEN CAST('1900-01-01 ' + FORMAT(getdate(),'HH:mm:ss')+' '+loc.timezone_code as datetimeoffset) 
  between  CAST('1900-01-01 ' + FORMAT(CAST(loc.shift3_start_time as datetime),'HH:mm')+':00 '+loc.timezone_code as datetimeoffset )
    and  CAST('1900-01-01 ' + FORMAT(CAST(loc.shift3_end_time as datetime),'HH:mm')+':00 '+loc.timezone_code as datetimeoffset ) THEN 3
*/
 SELECT @l_timezone_code = timezone_code,
   @l_s1_start = TODATETIMEOFFSET(CAST(shift1_start_time as datetime),timezone_code),
   @l_s1_end = TODATETIMEOFFSET(CAST(shift1_end_time as datetime),timezone_code),
   @l_s2_start = TODATETIMEOFFSET(CAST(shift2_start_time as datetime),timezone_code),
   @l_s2_end = TODATETIMEOFFSET(CAST(shift2_end_time as datetime),timezone_code),
   @l_s3_start = TODATETIMEOFFSET(CAST(shift3_start_time as datetime),timezone_code),
   @l_s3_end = TODATETIMEOFFSET(CAST(shift3_end_time as datetime),timezone_code)
 FROM   mh_lpa_location_master
 WHERE  location_id = @p_location_id;
 -- SELECT  @l_cur_time = TODATETIMEOFFSET(CAST(FORMAT(getdate(),'HH:mm:ss') as datetime), @l_timezone_code);
-- SELECT  @l_cur_time = SYSDATETIMEOFFSET();
-- SELECT  @l_cur_time = SWITCHOFFSET(getdate(), @l_timezone_code);
 SELECT  @l_cur_time = SWITCHOFFSET(CAST(FORMAT(getdate(),'HH:mm:ss') as datetime), @l_timezone_code);

 IF FORMAT(@l_cur_time,'yyyy') = '1899' 
		SET @l_cur_time = DATEADD(day, 1, @l_cur_time)

 IF @l_s1_start > @l_s1_end
  SET @l_s1_end = DATEADD(dd,1,@l_s1_end)
 IF @l_s2_start > @l_s2_end
  SET @l_s2_end = DATEADD(dd,1,@l_s2_end)
 IF @l_s3_start > @l_s3_end
  SET @l_s3_end = DATEADD(dd,1,@l_s3_end)
 
 IF @l_cur_time BETWEEN @l_s1_start AND @l_s1_end
  SET @l_shift = 1
 ELSE
  IF  @l_cur_time BETWEEN @l_s2_start AND @l_s2_end
   SET @l_shift = 2
  ELSE
  BEGIN
	IF @l_s3_start > @l_s3_end
		SET @l_s3_end = DATEADD(day,1,@l_s3_end);
	IF  @l_cur_time BETWEEN @l_s3_start AND @l_s3_end
		SET @l_shift = 3
	ELSE
		SET @l_shift = 1
  END;
 RETURN @l_shift
END;




GO
/****** Object:  UserDefinedFunction [dbo].[GetTotalAnswers]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetTotalAnswers](@p_audit_id int)  
 RETURNS FLOAT  
AS  
BEGIN  

	DECLARE @l_total_answers int;

	SELECT @l_total_answers = count(*)
	FROM   mh_lpa_local_answers
	WHERE  audit_id = @p_audit_id
	AND    answer in (0,1);
	
	RETURN 	CAST(@l_total_answers AS FLOAT)
END





GO
/****** Object:  UserDefinedFunction [dbo].[IsCommentsExists]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[IsCommentsExists](@p_line_id int,
								@p_question_id		INT)  
 RETURNS INT  
AS  
BEGIN  
	DECLARE @l_count		INT;
	DECLARE @l_answer		INT;

	SET @l_count = 0;

	SELECT @l_count = 1
	FROM   mh_lpa_local_answers
	WHERE  question_id = @p_question_id
	AND    audit_id = (select max(audit_id) from mh_lpa_local_answers where line_id = @p_line_id)
	AND    answer = 1;

	RETURN @l_count;

END;


GO
/****** Object:  UserDefinedFunction [dbo].[UserLogin]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[UserLogin](@p_username      varchar(100),
								@p_passwd  varchar(1000))
RETURNS NVARCHAR(MAX) 
AS
BEGIN 

	DECLARE @l_count		INT;
	DECLARE @l_err_code		INT;
	DECLARE @l_err_message	VARCHAR(100);

	SELECT @l_count = count(*)
	FROM   mh_lpa_user_master um
	WHERE um.username = @p_username
	AND   um.passwd = @p_passwd	;


	IF @l_count > 0 
	BEGIN

		SET @l_err_code = 0;
		SET @l_err_message = 'Login Successfull';

		RETURN (
			SELECT user_id,
				role,
				role_id,
				email_id,
				emp_full_name,
				display_name_flag,
				global_admin_flag,
				site_admin_flag,
				region_id,
				region_name,
				country_id,
				country_name,
				location_id,
				location_name,
				err_code,
				err_message
			FROM(
				SELECT um.user_id,
					um.role,
					um.role_id,
					um.email_id,
					um.emp_full_name,
					um.display_name_flag,
					um.global_admin_flag,
					um.site_admin_flag,
					um.region_id,
					rm.region_name,
					um.country_id,
					cm.country_name,
					lm.location_id,
					lm.location_name,
					@l_err_code err_code,
					@l_err_message err_message
				FROM   mh_lpa_user_master um,
						mh_lpa_location_master lm,
						mh_lpa_country_master cm,
						mh_lpa_region_master rm
				WHERE um.location_id = lm.location_id
				AND   um.region_id = rm.region_id
				AND   um.country_id = cm.country_id
				AND   um.username = @p_username
				AND   um.passwd = @p_passwd		
				AND (um.end_date is null OR end_date>=GETDATE())
			) t
				FOR JSON AUTO) 
	END;
	ELSE
	BEGIN

		SET @l_err_code = 2000;
		SET @l_err_message = 'Username or Password is incorrect';

		RETURN(SELECT * FROM (SELECT @l_err_code err_code, @l_err_message err_message) a FOR JSON AUTO)

	END;
	RETURN NULL;
END;
 




GO
/****** Object:  Table [dbo].[mh_lpa_audit_plan]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_audit_plan](
	[audit_plan_id] [int] IDENTITY(100,1) NOT NULL,
	[to_be_audited_by_user_id] [int] NULL,
	[planned_date] [datetime] NULL,
	[shift_no] [int] NULL,
	[line_product_rel_id] [int] NULL,
	[notification_sent_flag] [varchar](1) NULL,
	[to_be_audited_by_group_id] [int] NULL,
	[planned_date_end] [datetime] NULL,
	[line_id] [int] NULL,
	[location_id] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_audit_plan_interface]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_audit_plan_interface](
	[location_id] [int] NULL,
	[line_id] [int] NULL,
	[line_name] [varchar](100) NULL,
	[year] [int] NULL,
	[wk1] [int] NULL,
	[wk2] [int] NULL,
	[wk3] [int] NULL,
	[wk4] [int] NULL,
	[wk5] [int] NULL,
	[wk6] [int] NULL,
	[wk7] [int] NULL,
	[wk8] [int] NULL,
	[wk9] [int] NULL,
	[wk10] [int] NULL,
	[wk11] [int] NULL,
	[wk12] [int] NULL,
	[wk13] [int] NULL,
	[wk14] [int] NULL,
	[wk15] [int] NULL,
	[wk16] [int] NULL,
	[wk17] [int] NULL,
	[wk18] [int] NULL,
	[wk19] [int] NULL,
	[wk20] [int] NULL,
	[wk21] [int] NULL,
	[wk22] [int] NULL,
	[wk23] [int] NULL,
	[wk24] [int] NULL,
	[wk25] [int] NULL,
	[wk26] [int] NULL,
	[wk27] [int] NULL,
	[wk28] [int] NULL,
	[wk29] [int] NULL,
	[wk30] [int] NULL,
	[wk31] [int] NULL,
	[wk32] [int] NULL,
	[wk33] [int] NULL,
	[wk34] [int] NULL,
	[wk35] [int] NULL,
	[wk36] [int] NULL,
	[wk37] [int] NULL,
	[wk38] [int] NULL,
	[wk39] [int] NULL,
	[wk40] [int] NULL,
	[wk41] [int] NULL,
	[wk42] [int] NULL,
	[wk43] [int] NULL,
	[wk44] [int] NULL,
	[wk45] [int] NULL,
	[wk46] [int] NULL,
	[wk47] [int] NULL,
	[wk48] [int] NULL,
	[wk49] [int] NULL,
	[wk50] [int] NULL,
	[wk51] [int] NULL,
	[wk52] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_AuditPlan_Del_Invite]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_AuditPlan_Del_Invite](
	[location_id] [int] NULL,
	[year] [int] NULL,
	[email_id] [varchar](1000) NULL,
	[start_date] [date] NULL,
	[end_date] [date] NULL,
	[line_name] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_AuditPlanInvite]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_AuditPlanInvite](
	[location_id] [int] NULL,
	[year] [int] NULL,
	[email_id] [varchar](1000) NULL,
	[start_date] [date] NULL,
	[end_date] [date] NULL,
	[line_name] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_country_master]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_country_master](
	[country_id] [int] IDENTITY(1,1) NOT NULL,
	[region_id] [int] NULL,
	[country_code] [varchar](10) NULL,
	[country_name] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_distribution_list]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_distribution_list](
	[dist_list_id] [int] IDENTITY(100,1) NOT NULL,
	[location_id] [int] NULL,
	[list_type] [varchar](50) NULL,
	[email_list] [varchar](max) NULL,
	[send_notification_to_owner] [varchar](1) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_group_master]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_group_master](
	[group_id] [int] IDENTITY(1,1) NOT NULL,
	[location_id] [int] NULL,
	[group_name] [varchar](100) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_group_members]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mh_lpa_group_members](
	[group_member_id] [int] IDENTITY(1,1) NOT NULL,
	[location_id] [int] NULL,
	[group_id] [int] NULL,
	[user_id] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mh_lpa_language_master]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_language_master](
	[code] [varchar](100) NULL,
	[english] [nvarchar](1000) NULL,
	[german] [nvarchar](1000) NULL,
	[french] [nvarchar](1000) NULL,
	[spanish] [nvarchar](1000) NULL,
	[czech] [nvarchar](1000) NULL,
	[chinese] [nvarchar](1000) NULL,
	[korean] [nvarchar](1000) NULL,
	[thai] [nvarchar](1000) NULL,
	[bosnian] [nvarchar](1000) NULL,
	[portuguese] [nvarchar](1000) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_line_master]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_line_master](
	[line_id] [int] IDENTITY(1,1) NOT NULL,
	[location_id] [int] NULL,
	[country_id] [int] NULL,
	[region_id] [int] NULL,
	[line_code] [varchar](100) NULL,
	[line_name] [varchar](100) NULL,
	[start_date] [date] NULL,
	[end_date] [date] NULL,
	[distribution_list] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_local_answers]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_local_answers](
	[loc_answer_id] [int] IDENTITY(1,1) NOT NULL,
	[audit_id] [int] NULL,
	[audit_plan_id] [int] NULL,
	[audited_by_user_id] [int] NULL,
	[audit_date] [datetime] NULL,
	[Shift_No] [int] NULL,
	[question_id] [int] NULL,
	[answer] [int] NULL,
	[remarks] [nvarchar](1000) NULL,
	[image_file_name] [varchar](1000) NULL,
	[review_user_id] [int] NULL,
	[review_closed_on] [datetime] NULL,
	[review_closed_status] [int] NULL,
	[review_comments] [nvarchar](1000) NULL,
	[review_image_file_name] [varchar](1000) NULL,
	[notification_sent_flag] [varchar](1) NULL,
	[region_id] [int] NULL,
	[country_id] [int] NULL,
	[location_id] [int] NULL,
	[section_id] [int] NULL,
	[line_id] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_local_question_master]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_local_question_master](
	[local_question_id] [int] IDENTITY(10000,1) NOT NULL,
	[location_id] [int] NULL,
	[section_id] [int] NULL,
	[local_question] [varchar](1000) NULL,
	[local_help_text] [varchar](1000) NULL,
	[display_sequence] [int] NULL,
	[how_to_check] [varchar](1000) NULL,
	[na_flag] [varchar](1) NULL,
	[max_no_of_pics_allowed] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_local_questions]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mh_lpa_local_questions](
	[question_id] [int] NULL,
	[location_id] [int] NULL,
	[section_id] [int] NULL,
	[display_sequence] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mh_lpa_location_master]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_location_master](
	[location_id] [int] IDENTITY(1,1) NOT NULL,
	[country_id] [int] NULL,
	[region_id] [int] NULL,
	[location_code] [varchar](10) NULL,
	[location_name] [varchar](100) NULL,
	[shift1_start_time] [time](0) NULL,
	[shift1_end_time] [time](0) NULL,
	[shift2_start_time] [time](0) NULL,
	[shift2_end_time] [time](0) NULL,
	[shift3_start_time] [time](0) NULL,
	[shift3_end_time] [time](0) NULL,
	[no_of_shifts] [int] NULL,
	[timezone_code] [varchar](100) NULL,
	[timezone_desc] [varchar](100) NULL,
	[default_language] [varchar](30) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_Question_language_master]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mh_lpa_Question_language_master](
	[Question_id] [int] NULL,
	[English] [nvarchar](1000) NULL,
	[German] [nvarchar](1000) NULL,
	[French] [nvarchar](1000) NULL,
	[Spanish] [nvarchar](1000) NULL,
	[czech] [nvarchar](1000) NULL,
	[Chinese] [nvarchar](1000) NULL,
	[Korean] [nvarchar](1000) NULL,
	[Thai] [nvarchar](1000) NULL,
	[Bosnian] [nvarchar](1000) NULL,
	[Portuguese] [nvarchar](1000) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mh_lpa_question_master]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_question_master](
	[question_id] [int] IDENTITY(1,1) NOT NULL,
	[section_id] [int] NULL,
	[question] [varchar](1000) NULL,
	[help_text] [varchar](1000) NULL,
	[display_sequence] [int] NULL,
	[how_to_check] [varchar](1000) NULL,
	[na_flag] [varchar](1) NULL,
	[max_no_of_pics_allowed] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_region_master]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_region_master](
	[region_id] [int] IDENTITY(1,1) NOT NULL,
	[region_code] [varchar](10) NULL,
	[region_name] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_ReminderMail_Info]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_ReminderMail_Info](
	[ID] [int] NULL,
	[ToMailIDs] [varchar](max) NULL,
	[CcMailIDs] [varchar](max) NULL,
	[Subject] [varchar](max) NULL,
	[Body] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_request_profile_changes]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_request_profile_changes](
	[user_id] [int] NULL,
	[request_date] [datetime] NULL,
	[comments] [varchar](1000) NULL,
	[notification_sent_flag] [int] NULL,
	[username] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_role_master]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_role_master](
	[role_id] [int] IDENTITY(1,1) NOT NULL,
	[rolename] [varchar](100) NULL,
	[no_of_audits_required] [int] NULL,
	[frequency] [varchar](30) NULL,
	[start_date] [date] NULL,
	[end_date] [date] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_score_summary]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mh_lpa_score_summary](
	[audit_id] [int] NULL,
	[audit_date] [date] NULL,
	[region_id] [int] NULL,
	[country_id] [int] NULL,
	[location_id] [int] NULL,
	[line_id] [int] NULL,
	[sec1_yes_count] [int] NULL,
	[sec1_tot_count] [int] NULL,
	[sec1_score] [float] NULL,
	[sec2_yes_count] [int] NULL,
	[sec2_tot_count] [int] NULL,
	[sec2_score] [float] NULL,
	[sec3_yes_count] [int] NULL,
	[sec3_tot_count] [int] NULL,
	[sec3_score] [float] NULL,
	[sec4_yes_count] [int] NULL,
	[sec4_tot_count] [int] NULL,
	[sec4_score] [float] NULL,
	[sec5_yes_count] [int] NULL,
	[sec5_tot_count] [int] NULL,
	[sec5_score] [float] NULL,
	[sec6_yes_count] [int] NULL,
	[sec6_tot_count] [int] NULL,
	[sec6_score] [float] NULL,
	[total_yes] [int] NULL,
	[total_no] [int] NULL,
	[total_na] [int] NULL,
	[total_score] [float] NULL,
	[q1_answer] [int] NULL,
	[q2_answer] [int] NULL,
	[q3_answer] [int] NULL,
	[q4_answer] [int] NULL,
	[q5_answer] [int] NULL,
	[q6_answer] [int] NULL,
	[q7_answer] [int] NULL,
	[q8_answer] [int] NULL,
	[q9_answer] [int] NULL,
	[q10_answer] [int] NULL,
	[q11_answer] [int] NULL,
	[q12_answer] [int] NULL,
	[q13_answer] [int] NULL,
	[q14_answer] [int] NULL,
	[q15_answer] [int] NULL,
	[q16_answer] [int] NULL,
	[q17_answer] [int] NULL,
	[q18_answer] [int] NULL,
	[q19_answer] [int] NULL,
	[q20_answer] [int] NULL,
	[q21_answer] [int] NULL,
	[q22_answer] [int] NULL,
	[q23_answer] [int] NULL,
	[q24_answer] [int] NULL,
	[q25_answer] [int] NULL,
	[q26_answer] [int] NULL,
	[q27_answer] [int] NULL,
	[q28_answer] [int] NULL,
	[q29_answer] [int] NULL,
	[q30_answer] [int] NULL,
	[update_complete] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mh_lpa_Section_language_master]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mh_lpa_Section_language_master](
	[section_id] [int] NULL,
	[English] [nvarchar](1000) NULL,
	[German] [nvarchar](1000) NULL,
	[French] [nvarchar](1000) NULL,
	[Spanish] [nvarchar](1000) NULL,
	[czech] [nvarchar](1000) NULL,
	[Chinese] [nvarchar](1000) NULL,
	[Korean] [nvarchar](1000) NULL,
	[Thai] [nvarchar](1000) NULL,
	[Bosnian] [nvarchar](1000) NULL,
	[Portuguese] [nvarchar](1000) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[mh_lpa_section_master]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_section_master](
	[section_id] [int] IDENTITY(1,1) NOT NULL,
	[section_name] [varchar](100) NULL,
	[display_sequence] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_user_feedback]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_user_feedback](
	[user_id] [int] NULL,
	[rating] [int] NULL,
	[review_date] [datetime] NULL,
	[review_comments] [varchar](1000) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[mh_lpa_user_master]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[mh_lpa_user_master](
	[user_id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](100) NULL,
	[passwd] [varchar](1000) NULL,
	[emp_full_name] [varchar](1000) NULL,
	[display_name_flag] [varchar](1) NULL,
	[email_id] [varchar](100) NULL,
	[location_id] [int] NULL,
	[role] [varchar](100) NULL,
	[global_admin_flag] [varchar](1) NULL,
	[site_admin_flag] [varchar](1) NULL,
	[start_date] [date] NULL,
	[end_date] [date] NULL,
	[role_id] [int] NULL,
	[region_id] [int] NULL,
	[country_id] [int] NULL,
	[emp_first_name] [varchar](100) NULL,
	[emp_last_name] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp_AnsResultsTableType]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_AnsResultsTableType](
	[question_id] [int] NULL,
	[answer] [int] NULL,
	[remarks] [nvarchar](1000) NULL,
	[image_file_name] [varchar](1000) NULL,
	[audit_id] [int] NULL,
	[location_id] [int] NULL,
	[audit_date] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[temp_AnsReviewsTableType]    Script Date: 7/11/2018 3:08:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[temp_AnsReviewsTableType](
	[audit_id] [int] NULL,
	[loc_answer_id] [int] NULL,
	[review_closed_on] [varchar](50) NULL,
	[review_closed_status] [int] NULL,
	[review_comments] [nvarchar](1000) NULL,
	[review_image_file_name] [varchar](1000) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [master]
GO
ALTER DATABASE [MH_LOGISTICS] SET  READ_WRITE 
GO
