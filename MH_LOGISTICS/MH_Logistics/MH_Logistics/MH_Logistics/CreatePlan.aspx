﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreatePlan.aspx.cs" Inherits="MH_Logistics.CreatePlan" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <script src="js/datatables_export_lib.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <link href="css/datatables_lib_export.min.css" rel="stylesheet" />
    <link href="css/dataTables.bootstrap.css" rel="stylesheet" />
    <script src="js/DatatblefixedColumns.js"></script>
    <script src="https://cdn.datatables.net/scroller/1.4.4/js/dataTables.scroller.min.js"></script>
    <style>
        #MainContent_CreateplanGridWrapper
        {
            float: left;
        }

        #MainContent_CreateplanGridWrapper, #MainContent_CreateplanGridPanelHeader, #MainContent_CreateplanGridPanelHeaderContent, #MainContent_CreateplanGridPanelItemContent, #MainContent_CreateplanGridPanelItem
        {
            width: 100% !important;
        }

            #MainContent_CreateplanGridWrapper th, #MainContent_CreateplanGridWrapper td
            {
                padding: 5px 15px;
                font-size: 13px;
            }

        .ma_table
        {
            margin: 10px 0 0 0;
            width: 100%;
            float: left;
        }

        #MainContent_CreateplanGridPanelHeaderContent
        {
            background: none !important;
        }

        #MainContent_CreateplanGridPanelHeader, #MainContent_CreateplanGridPanelHeaderContentFreeze
        {
            background: #008244 !important;
        }

        #MainContent_CreateplanGridFreeze
        {
            background: #008244;
            color: #ffffff;
        }

        .DTFC_Clone
        {
            background: #008244;
            color: #ffffff;
        }

        div.DTFC_LeftHeadWrapper table, div.DTFC_LeftFootWrapper table, div.DTFC_RightHeadWrapper table, div.DTFC_RightFootWrapper table, table.DTFC_Cloned tr.even
        {
            background: #008244;
            color: #ffffff;
            padding: 0 5px;
        }

        div.DTFC_LeftHeadWrapper table, div.DTFC_LeftFootWrapper table, div.DTFC_RightHeadWrapper table, div.DTFC_RightFootWrapper table, table.DTFC_Cloned tr.odd
        {
            background: #008244;
            color: #ffffff;
            padding: 0 5px;
        }

        table.dataTable thead .sorting
        {
            border: none;
        }

        th, td
        {
            white-space: nowrap;
        }

        table.DTFC_Cloned thead, table.DTFC_Cloned tfoot
        {
            background-color: white;
        }

        div.DTFC_Blocker
        {
            background-color: white;
        }

        div.DTFC_LeftWrapper table.dataTable, div.DTFC_RightWrapper table.dataTable
        {
            margin-bottom: 0;
            z-index: 2;
        }

            div.DTFC_LeftWrapper table.dataTable.no-footer, div.DTFC_RightWrapper table.dataTable.no-footer
            {
                border-bottom: none;
            }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            //gridviewScroll();
            debugger;
            //Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            var head_content = $('#MainContent_CreateplanGrid tr:first').html();
            $('#MainContent_CreateplanGrid').prepend('<thead></thead>')
            $('#MainContent_CreateplanGrid thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_CreateplanGrid tbody tr:first').hide();

            var table = $('#MainContent_CreateplanGrid').dataTable({
                scrollY: '500px',
                scrollX: '100%',
                sScrollXInner: "100%",
                paging: false,
                sorting: false,
                fixedColumns: {
                    leftColumns: 1

                },
                columnDefs: [
                  { "width": "200px", "targets": 0 },
                ],
                // "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]]
            });
            //$('#MainContent_CreateplanGrid').scroll(5);
            //var left = $('#MainContent_CreateplanGrid').scrollLeft;
            //$('#MainContent_CreateplanGrid').scrollLeft = 1000;
        });

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>

    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Audit</a>
                        </li>
                        <li class="current">Create Plan</li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div>
        <!-- main content start-->

        <div class="md_main">

            <div class="row">
                <div>
                    <div class="clearfix"></div>
                    <div class="filter_panel">
                        <div>
                            <div class="col-md-4 filter_label" style="width: 14%">
                                <p>Region* : </p>
                            </div>
                            <div class="col-md-4" style="width: 20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-4 filter_label" style="width: 13%">
                                <p>Country* : </p>
                            </div>
                            <div class="col-md-4" style="width: 20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-4 filter_label" style="width: 13%">
                                <p>Location* : </p>
                            </div>
                            <div class="col-md-4" style="width: 20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </div>
                        <div>
                            <div class="col-md-4 filter_label" style="width: 14%">
                                <p>Year* : </p>
                            </div>
                            <div class="col-md-4" style="width: 20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlYear" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                            </div>
                            <div class="col-md-4 filter_label" style="width: 66%">
                                <div id="save">

                                    <asp:Button runat="server" CssClass="btn-add" ID="btnSave" Text="SAVE" OnClick="btnSave_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix" style="height: 5px;"></div>




                    <div class="result_panel_1">
                        <%--<asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:csMHLPA %>" SelectCommand="select user_id, emp_full_name from mh_lpa_user_master" ID="SqlDataSource1" runat="server"></asp:SqlDataSource>--%>
                        <asp:GridView ID="CreateplanGrid" runat="server" AutoGenerateColumns="false" Width="100%" OnRowDataBound="OnRowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="Zone" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <%-- <asp:HiddenField ID="LbllineNoId" runat="server" Value='<%# Eval("line_id") %>'/>--%>
                                        <asp:Label ID="LbllineNoId" runat="server" Text='<%# Eval("line_id") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="LblLineNo" runat="server" Text='<%# Eval("line_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 1" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers1" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 2" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers2" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 3" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers3" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 4" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers4" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 5" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers5" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 6" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers6" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 7" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers7" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 8" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers8" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 9" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers9" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 10" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers10" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 11" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers11" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 12" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers12" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 13" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers13" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 14" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers14" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 15" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers15" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 16" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers16" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 17" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers17" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 18" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers18" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 19" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers19" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 20" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers20" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 21" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers21" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 22" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers22" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 23" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers23" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 24" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers24" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 25" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers25" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 26" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers26" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 27" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers27" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 28" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers28" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 29" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers29" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 30" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers30" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 31" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers31" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 32" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers32" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 33" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers33" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 34" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers34" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 35" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers35" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 36" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers36" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 37" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers37" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 38" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers38" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 39" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers39" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 40" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers40" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 41" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers41" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 42" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers42" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 43" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers43" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 44" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers44" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 45" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers45" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 46" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers46" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 47" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers47" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 48" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers48" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 49" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers49" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 50" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers50" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 51" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers51" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Week 52" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlusers52" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <%--</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="ddlRegion" />
            <asp:PostBackTrigger ControlID="ddlCountry" />
            <asp:PostBackTrigger ControlID="ddlRegion" />
        </Triggers>
</asp:UpdatePanel>--%>
</asp:Content>
