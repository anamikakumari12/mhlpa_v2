﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MH_Logistics_BusinessLogic;
using MH_Logistics_BusinessObject;

namespace MH_Logistics
{
    public partial class MyTask : System.Web.UI.Page
    {
        #region GlobalDeclaration

        CommonBL objComBL;
        CommonFunctions objCom = new CommonFunctions();
        PlanBO objPlanBO;
        PlanBL objPlanBL;
        DataSet dsDropDownData;
        UsersBO objUserBO;
        DataTable dtData;
        DataTable dtReview = new DataTable();
        DashboardBO objDashBO;
        DashboardBL objDashBL;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }

                if (!IsPostBack)
                {
                    //GetMasterDropdown();
                    objDashBO = new DashboardBO();
                    LoadFilterDropDowns(objDashBO);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnNext_Click(object sender, EventArgs e)        
        {
            int rowcount;
            try
            {
                if (Session["dtReview"] != null)
                {
                    dtReview = (DataTable)Session["dtReview"];
                }
                if (!string.IsNullOrEmpty(rdbStatus.SelectedValue))
                {
                    if (Convert.ToInt32(dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["RowCount"]) == Convert.ToInt32(Session["RowCount"]))
                    {
                        dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["review_closed_status"] = rdbStatus.SelectedValue;
                        dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["review_comments"] = txtComment.Text;
                        if (uplImage.PostedFile != null && uplImage.PostedFile.ContentLength > 0)
                        {
                            string str = "Task_"+Convert.ToString(lblAuditId.Value) + "_" + Convert.ToString(Session["question_id"]);
                            string filename = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(lblAuditId.Value) + "/" + str + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                            string filenametodb = ConfigurationManager.AppSettings["PublishedImageURL"].ToString() + "/" + Convert.ToString(lblAuditId.Value) + "/" + str + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                            string pathString = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(lblAuditId.Value);
                            if (!Directory.Exists(pathString))
                            {
                                Directory.CreateDirectory(pathString);
                            }
                            if (!string.IsNullOrEmpty(filename))
                            {
                                uplImage.PostedFile.SaveAs(filename);
                                dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["review_image_file_name"] = filenametodb;
                            }
                        }
                        Session["dtReview"] = dtReview;

                    }
                }

                dtData = new DataTable();
                rowcount = Convert.ToInt32(Session["RowCount"]) + 1;
                dtData = (DataTable)Session["dtAnswers"];
                if (dtData.Rows.Count > rowcount)
                {
                    lblSection.Text = Convert.ToString(dtData.Rows[rowcount]["section_name"]);
                    lblHelp.Text = Convert.ToString(dtData.Rows[rowcount]["help_text"]);
                   // lblQuestion.Text = Convert.ToString(dtData.Rows[rowcount]["question"]);
                    lblQuestion.Text = Convert.ToString(dtData.Rows[rowcount]["question_display_sequence"]) + ". " + Convert.ToString(dtData.Rows[rowcount]["question"]);
                    rdbAnswer.SelectedValue = "No";
                    txtRemarks.Text = Convert.ToString(dtData.Rows[rowcount]["remarks"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(dtData.Rows[rowcount]["image_file_name"])))
                    {
                        string[] imageArray = Convert.ToString(dtData.Rows[rowcount]["image_file_name"]).Split(';');
                        for (int i = 0; i < imageArray.Length; i++)
                        {
                            Image img = new Image();
                            img.ID = "img" + i;
                            img.ImageUrl = Convert.ToString(imageArray[i]);
                            img.Width = 50;
                            img.Height = 50;
                            img.Attributes["style"] = "cursor: pointer";
                            img.Attributes["onclick"] = "myFunction(this);";
                            img.Attributes["data-toggle"] = "modal";
                            img.Attributes["data-target"] = "#myModal1";
                            pnlImages.Controls.Add(img);
                        }
                        //myImg.Visible = true;
                        //myImg.ImageUrl = Convert.ToString(dtData.Rows[rowcount]["image_file_name"]);
                    }
                    //else
                    //    myImg.Visible = false;
                    
                    txtComment.Text = Convert.ToString(dtData.Rows[rowcount]["review_comments"]);
                    Session["RowCount"] = rowcount;

                    if (Convert.ToInt32(dtReview.Rows[rowcount]["RowCount"]) == Convert.ToInt32(Session["RowCount"]))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["review_closed_status"])))
                        {
                            rdbStatus.SelectedValue = Convert.ToString(dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["review_closed_status"]);
                            txtComment.Text = Convert.ToString(dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["review_comments"]);
                        }
                        else
                        {
                            rdbStatus.ClearSelection();
                            txtComment.Text = Convert.ToString(dtData.Rows[rowcount]["review_comments"]);
                        }
                    }
                    else
                    {
                        rdbStatus.ClearSelection();
                        txtComment.Text = "";
                    }

                        if (rowcount == 0)
                    {
                        btnSubmit.Visible = true;
                        btnNext.Visible = true;
                        btnPrevious.Visible = false;
                    }
                    else
                    //if (dtData.Rows.Count - rowcount == 1)
                    {
                        int retUnanswered = ValidateAnswers(dtReview);
                        if (retUnanswered == 0)
                        {
                            btnSubmit.Visible = true;
                        }
                        else
                        {
                            btnSubmit.Visible = true;
                        }
                        //if (retUnanswered == 1 && dtData.Rows.Count - rowcount == 1)
                        //{
                        //    btnSubmit.Visible = true;
                        //}

                        btnPrevious.Visible = true;
                        if (dtData.Rows.Count - rowcount == 1)
                            btnNext.Visible = false;
                        else
                            btnNext.Visible = true;
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            int rowcount;
            try
            {
                if (Session["dtReview"] != null)
                {
                    dtReview = (DataTable)Session["dtReview"];
                }
                if (!string.IsNullOrEmpty(rdbStatus.SelectedValue))
                {
                    if (Convert.ToInt32(dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["RowCount"]) == Convert.ToInt32(Session["RowCount"]))
                    {
                        dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["review_closed_status"] = rdbStatus.SelectedValue;
                        dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["review_comments"] = txtComment.Text;
                        if (uplImage.PostedFile != null && uplImage.PostedFile.ContentLength > 0)
                        {
                            string str = "Task_" + Convert.ToString(lblAuditId.Value) + "_" + Convert.ToString(Session["question_id"]);
                            string filename = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(lblAuditId.Value) + "/" + str + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                            string filenametodb = ConfigurationManager.AppSettings["PublishedImageURL"].ToString() + "/" + Convert.ToString(lblAuditId.Value) + "/" + str + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                            string pathString = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(lblAuditId.Value);
                            if (!Directory.Exists(pathString))
                            {
                                Directory.CreateDirectory(pathString);
                            }
                            if (!string.IsNullOrEmpty(filename))
                            {
                                uplImage.PostedFile.SaveAs(filename);
                                dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["review_image_file_name"] = filenametodb;
                            }
                        }
                        Session["dtReview"] = dtReview;
                    }
                }
                dtData = new DataTable();
                rowcount = Convert.ToInt32(Session["RowCount"]) - 1;
                dtData = (DataTable)Session["dtAnswers"];
                if (dtData.Rows.Count >= rowcount)
                {
                    lblSection.Text = Convert.ToString(dtData.Rows[rowcount]["section_name"]);
                    lblHelp.Text = Convert.ToString(dtData.Rows[rowcount]["help_text"]);
                   // lblQuestion.Text = Convert.ToString(dtData.Rows[rowcount]["question"]);
                    lblQuestion.Text = Convert.ToString(dtData.Rows[rowcount]["question_display_sequence"]) + ". " + Convert.ToString(dtData.Rows[rowcount]["question"]);
                    rdbAnswer.SelectedValue = "No";
                    txtRemarks.Text = Convert.ToString(dtData.Rows[rowcount]["remarks"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(dtData.Rows[rowcount]["image_file_name"])))
                    {
                        string[] imageArray = Convert.ToString(dtData.Rows[rowcount]["image_file_name"]).Split(';');
                        for (int i = 0; i < imageArray.Length; i++)
                        {
                            Image img = new Image();
                            img.ID = "img" + i;
                            img.ImageUrl = Convert.ToString(imageArray[i]);
                            img.Width = 50;
                            img.Height = 50;
                            img.Attributes["style"] = "cursor: pointer";
                            img.Attributes["onclick"] = "myFunction(this);";
                            img.Attributes["data-toggle"] = "modal";
                            img.Attributes["data-target"] = "#myModal1";
                            pnlImages.Controls.Add(img);
                        }
                        //myImg.Visible = true;
                        //myImg.ImageUrl = Convert.ToString(dtData.Rows[rowcount]["image_file_name"]);
                    }
                    //else
                    //    myImg.Visible = false;

                    //myImg.ImageUrl = Convert.ToString(dtData.Rows[rowcount]["image_file_name"]);
                    txtComment.Text = Convert.ToString(dtData.Rows[rowcount]["review_comments"]);
                    Session["RowCount"] = rowcount;

                    if (Convert.ToInt32(dtReview.Rows[rowcount]["RowCount"]) == Convert.ToInt32(Session["RowCount"]))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["review_closed_status"])))
                        {
                            rdbStatus.SelectedValue = Convert.ToString(dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["review_closed_status"]);
                            txtComment.Text = Convert.ToString(dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["review_comments"]);

                        }
                        else
                        {
                            rdbStatus.ClearSelection();
                            txtComment.Text = Convert.ToString(dtData.Rows[rowcount]["review_comments"]);
                        }
                    }
                    else
                    {
                        rdbStatus.ClearSelection();
                        txtComment.Text = "";
                    }
                    //if (dtData.Rows.Count - rowcount == 1)
                    //{
                    //    btnSubmit.Visible = true;
                    //    btnNext.Visible = false;
                    //    btnPrevious.Visible = true;
                    //}
                    //else 
                    if (rowcount == 0)
                    {
                        btnSubmit.Visible = true;
                        btnNext.Visible = true;
                        btnPrevious.Visible = false;
                    }
                    else
                    {
                        int retUnanswered = ValidateAnswers(dtReview);
                        if (retUnanswered == 0)
                        {
                            btnSubmit.Visible = true;
                        }
                        else
                        {
                            btnSubmit.Visible = true;
                        }
                        //if (retUnanswered == 1 && dtData.Rows.Count - rowcount == 1)
                        //{
                        //    btnSubmit.Visible = true;
                        //}

                        btnPrevious.Visible = true;
                        if (dtData.Rows.Count - rowcount == 1)
                            btnNext.Visible = false;
                        else
                            btnNext.Visible = true;
                    }

                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            bool anyanswered = false;
            int retUnanswered = 0;
            try
            {
                if (Session["dtReview"] != null)
                {
                    dtReview = (DataTable)Session["dtReview"];
                }
                for (int i = 0; i < dtReview.Rows.Count; i++)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(dtReview.Rows[i]["review_closed_status"])))
                    {
                        anyanswered = true;
                    }
                }
                if (!string.IsNullOrEmpty(rdbStatus.SelectedValue))
                    {
                        if (Convert.ToInt32(dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["RowCount"]) == Convert.ToInt32(Session["RowCount"]))
                        {
                            dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["review_closed_status"] = rdbStatus.SelectedValue;
                            dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["review_comments"] = txtComment.Text;
                            if (uplImage.PostedFile != null && uplImage.PostedFile.ContentLength > 0)
                            {
                                string str = "Task_" + Convert.ToString(lblAuditId.Value) + "_" + Convert.ToString(Session["question_id"]);
                                string filename = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(lblAuditId.Value) + "/" + str + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                string filenametodb = ConfigurationManager.AppSettings["PublishedImageURL"].ToString() + "/" + Convert.ToString(lblAuditId.Value) + "/" + str + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                string pathString = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(lblAuditId.Value);
                                if (!Directory.Exists(pathString))
                                {
                                    Directory.CreateDirectory(pathString);
                                }
                                if (!string.IsNullOrEmpty(filename))
                                {
                                    uplImage.PostedFile.SaveAs(filename);
                                    dtReview.Rows[Convert.ToInt32(Session["RowCount"])]["review_image_file_name"] = filenametodb;
                                }
                            }
                            Session["dtReview"] = dtReview;
                        }
                        
                    }
                //retUnanswered = ValidateAnswers(dtReview);
                //if (retUnanswered == 0)
                if (anyanswered)
                {
                    objPlanBO = new PlanBO();
                    objPlanBL = new PlanBL();
                    objPlanBO.p_audit_id = Convert.ToInt32(lblAuditId.Value);
                    //objPlanBO.p_line_product_rel_id = Convert.ToInt32(ddlPartName.SelectedValue);
                    objPlanBO.p_reviewed_by_user_id = Convert.ToInt32(Session["UserId"]);
                    objPlanBO = objPlanBL.SaveAuditReviewsBL(objPlanBO, dtReview);
                    if (!string.IsNullOrEmpty(objPlanBO.error_msg))
                    {
                        pnlQuestion.Visible = false;
                        GetMasterDropdown();
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('" + objPlanBO.error_msg + "');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Plesae try again.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('At least one question should be reviewed. Please check and try again.');", true);
                }
                
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            BindData();
        }

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                //objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                //objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Region";
                // LoadFilterDropDowns(objDashBO);


                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        //ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        //ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        //ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    //DataTable dtLineProduct = new DataTable();
                    objUserBO = new UsersBO();
                    objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                    objComBL = new CommonBL();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLineProduct.Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dtLineProduct;
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        //BindPart();
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            int region;
            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                //if (objDashBO.country_id == 0)
                //{
                    //objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                //}
                //objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Country";
                // LoadFilterDropDowns(objDashBO);

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        //ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        //if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        //{
                            //ddlRegion.SelectedValue = Convert.ToString(region);
                        //}
                        //else
                        //{
                        //    ddlRegion.SelectedIndex = 1;
                        //}
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        //ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        ddlCountry.SelectedValue = Convert.ToString(objDashBO.country_id);
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        //ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                   // DataTable dtLineProduct = new DataTable();
                    objUserBO = new UsersBO();
                    objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                    objComBL = new CommonBL();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLineProduct.Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dtLineProduct;
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        //BindPart();
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            int region, country;
            try
            {
                objDashBO = new DashboardBO();
                //region = Convert.ToInt32(ddlRegion.SelectedValue);
                //country = Convert.ToInt32(ddlCountry.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //if (objDashBO.location_id == 0)
                //{
                    //objDashBO.country_id = country;
                    //objDashBO.region_id = region;
                //}
                objDashBO.selection_flag = "Location";

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        //ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        //if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        //{
                            //ddlRegion.SelectedValue = Convert.ToString(region);
                        //}
                        //else
                        //{
                        //    ddlRegion.SelectedIndex = 1;
                        //}
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        //ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        //if (ddlCountry.Items.FindByValue(Convert.ToString(country)) != null && country != 0)
                        //{
                            //ddlCountry.SelectedValue = Convert.ToString(country);
                        //}
                        //else
                        //{
                        //    ddlCountry.SelectedIndex = 1;
                        //}
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                       // ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        ddlLocation.SelectedValue = Convert.ToString(objDashBO.location_id);
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    dtLineProduct = new DataTable();
                    objUserBO = new UsersBO();
                    objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                    objComBL = new CommonBL();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLineProduct.Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dtLineProduct;
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        //BindPart();
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLineName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();

                objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                //if (objDashBO.line_id == 0)
                //{
                //    objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                //    objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                //    objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //}
                objDashBO.selection_flag = "Line";
                // LoadFilterDropDowns(objDashBO);

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        //ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        //ddlRegion.SelectedIndex = 1;
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        //ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        //ddlCountry.SelectedIndex = 1;
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        //ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        //ddlLocation.SelectedIndex = 1;
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    DataTable dtLineProduct = new DataTable();
                    objUserBO = new UsersBO();
                    objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                    objComBL = new CommonBL();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLineProduct.Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dtLineProduct;
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        //BindPart();
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        protected void rdbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtResult = new DataTable();
            int i;
            try
            {
                if (Session["dtReview"] != null)
                {
                    dtResult = (DataTable)Session["dtReview"];
                    if (Convert.ToInt32(dtResult.Rows[Convert.ToInt32(Session["RowCount"])]["RowCount"]) == Convert.ToInt32(Session["RowCount"]))
                    {
                        dtResult.Rows[Convert.ToInt32(Session["RowCount"])]["review_closed_status"] = rdbStatus.SelectedValue;
                        Session["dtReview"] = dtResult;

                    }
                    dtResult = (DataTable)Session["dtReview"];
                    dtData = new DataTable();
                    //rowcount = Convert.ToInt32(Session["RowCount"]) + 1;
                    dtData = (DataTable)Session["dtAnswers"];
                    if (!string.IsNullOrEmpty(Convert.ToString(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"])))
                    {
                        string[] imageArray = Convert.ToString(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]).Split(';');
                        for (int j = 0; j < imageArray.Length; j++)
                        {
                            Image img = new Image();
                            img.ID = "img" + j;
                            img.ImageUrl = Convert.ToString(imageArray[j]);
                            img.Width = 50;
                            img.Height = 50;
                            img.Attributes["style"] = "cursor: pointer";
                            img.Attributes["onclick"] = "myFunction(this);";
                            img.Attributes["data-toggle"] = "modal";
                            img.Attributes["data-target"] = "#myModal1";
                            pnlImages.Controls.Add(img);
                        }
                        //myImg.Visible = true;
                        //myImg.ImageUrl = Convert.ToString(dtData.Rows[rowcount]["image_file_name"]);
                    }

                    i = ValidateAnswers(dtResult);
                    if (i == 0)
                    {
                        btnSubmit.Visible = true;
                    }
                    else
                    {
                        btnSubmit.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                LoadFilterDropDowns(objDashBO);
                pnlQuestion.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        //protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtLineProduct = new DataTable();
        //    DataTable dtCountry = new DataTable();
        //    DataTable dtLocation = new DataTable();
        //    try
        //    {
        //        dsDropDownData = new DataSet();
        //        objUserBO = new UsersBO();
        //        objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
        //        objComBL = new CommonBL();

        //        objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
        //        dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

        //        if (dtCountry.Rows.Count > 0)
        //        {
        //            ddlCountry.DataSource = dtCountry;
        //            ddlCountry.DataTextField = "country_name";
        //            ddlCountry.DataValueField = "country_id";
        //            ddlCountry.DataBind();
        //            objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
        //        }
        //        else
        //        {
        //            ddlCountry.Items.Clear();
        //            ddlCountry.DataSource = null;
        //            ddlCountry.DataBind();
        //        }

        //        dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
        //        if (dtLocation.Rows.Count > 0)
        //        {
        //            ddlLocation.DataSource = dtLocation;
        //            ddlLocation.DataTextField = "location_name";
        //            ddlLocation.DataValueField = "location_id";
        //            ddlLocation.DataBind();
        //            objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //        }
        //        else
        //        {
        //            ddlLocation.Items.Clear();
        //            ddlLocation.DataSource = null;
        //            ddlLocation.DataBind();
        //        }

        //        dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
        //        if (dtLineProduct.Rows.Count > 0)
        //        {
        //            ddlLineName.DataSource = dtLineProduct;
        //            ddlLineName.DataTextField = "line_name";
        //            ddlLineName.DataValueField = "line_id";
        //            ddlLineName.DataBind();
        //            //BindPart();
        //        }
        //        else
        //        {
        //            ddlLineName.Items.Clear();
        //            ddlLineName.DataSource = null;
        //            ddlLineName.DataBind();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtLineProduct = new DataTable();
        //    DataTable dtCountry = new DataTable();
        //    DataTable dtLocation = new DataTable();
        //    try
        //    {
        //        dsDropDownData = new DataSet();
        //        objUserBO = new UsersBO();
        //        objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
        //        objComBL = new CommonBL();

        //        objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
        //        dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
        //        if (dtLocation.Rows.Count > 0)
        //        {
        //            ddlLocation.DataSource = dtLocation;
        //            ddlLocation.DataTextField = "location_name";
        //            ddlLocation.DataValueField = "location_id";
        //            ddlLocation.DataBind();
        //        }
        //        else
        //        {
        //            ddlLocation.Items.Clear();
        //            ddlLocation.DataSource = null;
        //            ddlLocation.DataBind();
        //        }
        //        objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //        dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
        //        if (dtLineProduct.Rows.Count > 0)
        //        {
        //            ddlLineName.DataSource = dtLineProduct;
        //            ddlLineName.DataTextField = "line_name";
        //            ddlLineName.DataValueField = "line_id";
        //            ddlLineName.DataBind();
        //            //BindPart();
        //        }
        //        else
        //        {
        //            ddlLineName.Items.Clear();
        //            ddlLineName.DataSource = null;
        //            ddlLineName.DataBind();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtLineProduct = new DataTable();
        //    try
        //    {
        //        objUserBO = new UsersBO();
        //        objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
        //        objComBL = new CommonBL();
        //        objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //        dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
        //        if (dtLineProduct.Rows.Count > 0)
        //        {
        //            ddlLineName.DataSource = dtLineProduct;
        //            ddlLineName.DataTextField = "line_name";
        //            ddlLineName.DataValueField = "line_id";
        //            ddlLineName.DataBind();
        //            //BindPart();
        //        }
        //        else
        //        {
        //            ddlLineName.Items.Clear();
        //            ddlLineName.DataSource = null;
        //            ddlLineName.DataBind();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlLineName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    BindPart();
        //}

        #endregion

        #region Methods

        //public void BindPart()
        //{
        //    DataTable dtPart = new DataTable();
        //    DataRow[] dr;
        //    DataTable dtLine = new DataTable();
        //    objUserBO = new UsersBO();
        //    objComBL = new CommonBL();
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(ddlLineName.SelectedValue))
        //        {
        //            objUserBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
        //            dtPart = objComBL.GetPartBasedOnLineBL(objUserBO);
        //            if (dtPart.Rows.Count > 0)
        //            {
        //                ddlPartName.DataSource = dtPart;
        //                ddlPartName.DataTextField = "Part_code";
        //                ddlPartName.DataValueField = "line_product_rel_id";
        //                ddlPartName.DataBind();
        //            }
        //            else
        //            {
        //                ddlPartName.Items.Clear();
        //                ddlPartName.DataSource = null;
        //                ddlPartName.DataBind();
        //            }
        //        }
        //        else
        //        {
        //            ddlPartName.Items.Clear();
        //            ddlPartName.DataSource = null;
        //            ddlPartName.DataBind();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }

        //}

        private void GetMasterDropdown()
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);

                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ddlRegion.SelectedValue)))
                {
                    objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                    dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                    if (dtCountry.Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dtCountry;
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ddlCountry.SelectedValue)))
                {
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                    dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                    if (dtLocation.Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dtLocation;
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ddlLocation.SelectedValue)))
                {
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLineProduct.Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dtLineProduct;
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        //BindPart();
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
                else
                {
                    ddlLineName.Items.Clear();
                    ddlLineName.DataSource = null;
                    ddlLineName.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void BindData()
        {
            DataTable dtQuestions;
            DataTable dtSec1;
            int UserId;
            try
            {
                dtReview = new DataTable();
                dtReview.Columns.Add("loc_answer_id", typeof(int));
                dtReview.Columns.Add("review_closed_on", typeof(string));
                dtReview.Columns.Add("review_closed_status", typeof(int));
                dtReview.Columns.Add("review_comments", typeof(string));
                dtReview.Columns.Add("review_image_file_name", typeof(string));
                dtReview.Columns.Add("RowCount", typeof(int));

                UserId = Convert.ToInt32(Session["UserId"]);
                objPlanBO = new PlanBO();
                objPlanBL = new PlanBL();
                dtQuestions = new DataTable();
                dtSec1 = new DataTable();
                objPlanBO.p_user_id = UserId;
                objPlanBO.p_line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                dtQuestions = objPlanBL.getMyTaskBL(objPlanBO);
                Session["dtAnswers"] = dtQuestions;
                if (dtQuestions != null)
                {
                    if (dtQuestions.Rows.Count > 0)
                    {
                        lblResult.Text = "";
                        lblSection.Text = Convert.ToString(dtQuestions.Rows[0]["section_name"]);
                        lblHelp.Text = Convert.ToString(dtQuestions.Rows[0]["help_text"]);
                        lblQuestion.Text = Convert.ToString(dtQuestions.Rows[0]["question_display_sequence"])+". "+Convert.ToString(dtQuestions.Rows[0]["question"]);
                        rdbAnswer.SelectedValue = "No";
                        txtRemarks.Text = Convert.ToString(dtQuestions.Rows[0]["remarks"]);
                        lblAuditId.Value = Convert.ToString(dtQuestions.Rows[0]["audit_id"]);
                     //   imageresource.ImageUrl = Convert.ToString(dtQuestions.Rows[0]["image_file_name"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(dtQuestions.Rows[0]["image_file_name"])))
                        {
                            string[] imageArray = Convert.ToString(dtQuestions.Rows[0]["image_file_name"]).Split(';');
                            for (int i = 0; i < imageArray.Length; i++)
                            {
                                Image img = new Image();
                                img.ID = "img" + i;
                                img.ImageUrl = Convert.ToString(imageArray[i]);
                                img.Width = 50;
                                img.Height = 50;
                                img.Attributes["style"] = "cursor: pointer";
                                img.Attributes["onclick"] = "myFunction(this);";
                                img.Attributes["data-toggle"] = "modal";
                                img.Attributes["data-target"] = "#myModal1";
                                pnlImages.Controls.Add(img);
                            }
                            //myImg.Visible = true;
                            //myImg.ImageUrl = Convert.ToString(dtQuestions.Rows[0]["image_file_name"]);
                        }
                        //else
                        //    myImg.Visible = false;
                        rdbStatus.ClearSelection();
                        txtComment.Text = Convert.ToString(dtQuestions.Rows[0]["review_comments"]);
                        Session["RowCount"] = 0;

                        pnlQuestion.Visible = true;
                        DataRow row;
                        for (int i = 0; i < dtQuestions.Rows.Count; i++)
                        {
                            row = dtReview.NewRow();
                            row["RowCount"] = i;
                            row["loc_answer_id"] = Convert.ToInt32(dtQuestions.Rows[i]["loc_answer_id"]);
                            dtReview.Rows.Add(row);
                        }
                        Session["dtReview"] = dtReview;
                        pnlQuestion.Visible = true;
                        if (dtQuestions.Rows.Count == 1)
                        {
                            btnSubmit.Visible = true;
                            btnNext.Visible = false;
                        }
                    }
                    else
                    {
                        lblResult.Text = "There is no open task.";
                        pnlQuestion.Visible = false;
                    }
                }
                else
                {
                    lblResult.Text = "There is no open task.";
                    pnlQuestion.Visible = false;
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private int ValidateAnswers(DataTable dtReviewResult)
        {
            int retVal = 0;
            try
            {
                for (int i = 0; i < dtReviewResult.Rows.Count; i++)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(dtReviewResult.Rows[i]["review_closed_status"])))
                        retVal++;
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
                retVal++;
            }
            return retVal;
        }


        private void LoadFilterDropDowns(DashboardBO objDashBO)
        {
            try
            {
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        //ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        //ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        //ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    DataTable dtLineProduct = new DataTable();
                    objUserBO = new UsersBO();
                    objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                    objComBL = new CommonBL();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLineProduct.Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dtLineProduct;
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        //BindPart();
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                    //if (dsDropDownData.Tables[3].Rows.Count > 0)
                    //{
                    //    ddlLineName.DataSource = dsDropDownData.Tables[3];
                    //    ddlLineName.DataTextField = "line_name";
                    //    ddlLineName.DataValueField = "line_id";
                    //    ddlLineName.DataBind();
                    //    //ddlLineName.Items.Insert(0, new ListItem("All", "0"));

                    //}
                    //else
                    //{
                    //    ddlLineName.Items.Clear();
                    //    ddlLineName.DataSource = null;
                    //    ddlLineName.DataBind();
                    //}
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        #endregion


    }
}