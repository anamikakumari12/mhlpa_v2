﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using PdfSharp.Drawing;
using System.Drawing.Imaging;
using System.IO.Compression;
using System.Drawing;
using System.Globalization;

using MH_Logistics_BusinessLogic;
using MH_Logistics_BusinessObject;

namespace MH_Logistics
{
    public partial class Audit : System.Web.UI.Page
    {
        #region GlobalDeclaration

        CommonBL objComBL;
        CommonFunctions objCom = new CommonFunctions();
        DataSet dsDropDownData;
        UsersBO objUserBO;
        DataTable dtData;
        PlanBO objPlanBO;
        PlanBL objPlanBL;
        DataTable dtAnswer = new DataTable();
        DataTable dtImageBind;
        DataTable imagetable = new DataTable();
        DashboardBO objDashBO;
        DashboardBL objDashBL;
        #endregion

        #region Events

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : when the page loads, it loads all the dropdowns by calling method LoadFilterDropDowns, then it checks for user admin type, if it is global admin, date and time field should be visible and shift field should be editable
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }

                if (!IsPostBack)
                {
                   // GetMasterDropdown();
                    // GetAuditId();
                    objDashBO = new DashboardBO();
                    LoadFilterDropDowns(objDashBO);
                    if (Convert.ToString(Session["GlobalAdminFlag"]) == "Y")
                    {
                        divdatetime.Visible = true;
                        txtNumber.Enabled = true;
                        txtDate.Text = String.Format("{0:dd-MM-yyyy}", DateTime.Now);
                        txtTime.Text = "00:00";
                    }
                    else
                    {
                        txtDate.Text = String.Format("{0:dd-MM-yyyy}", DateTime.Now);
                        txtTime.Text = "00:00";
                        divdatetime.Visible = false;
                        txtNumber.Enabled = false;
                    }
                }
                else
                {
                    ButtonPanel();
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : On click of next button, it will store all the details of cuurent question in dtAnswer datatable and load the panel with next question details from dtData. It saves the images on the server. The colour of question icon in the question panel is changed here.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNext_Click(object sender, EventArgs e)
        {
            int rowcount;
            lblImageResult.Text = "";
            imgAddImage.Visible = false;
            try
            {
                if (Session["dtAnswer"] != null)
                {
                    dtAnswer = (DataTable)Session["dtAnswer"];
                }
                dtData = new DataTable();

                dtData = (DataTable)Session["dtQuestions"];
                if (!string.IsNullOrEmpty(rdbAnswer.SelectedValue))
                {
                    if (Convert.ToInt32(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["RowCount"]) == Convert.ToInt32(Session["RowCount"]))
                    {
                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["answer"] = rdbAnswer.SelectedValue;
                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["remarks"] = txtComment.Text;
                        if (uplImage.PostedFile != null && uplImage.PostedFile.ContentLength > 0)
                        {

                            string str = String.Empty;
                            string half = String.Empty;

                            int[] nam = new int[Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"])];
                            int[] finalnum = new int[Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"])];
                            string[] filenamestr = new string[Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"])];
                            imagetable.Columns.Add("name", typeof(string));
                            string[] names = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]).Split(';');
                            if (!String.IsNullOrEmpty(Convert.ToString(names[0])))
                            {
                                if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) > 1)
                                {
                                    half = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]) + "/" + Convert.ToString(Session["NextAuditId"]) + "_" + Convert.ToString(Session["question_id"]);
                                    objCom.TestLog("Max_Length : " + Convert.ToString(filenamestr.Length));
                                    for (int i = 0; i < filenamestr.Length; i++)
                                    {
                                        if (i == 0)
                                            filenamestr[i] = half + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                        else
                                            filenamestr[i] = half + "_" + i + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                        objCom.TestLog("filenamestr : " + Convert.ToString(i) + " : " + filenamestr[i]);
                                    }

                                    //string[] listDiff = filenamestr.Except(names).ToArray();

                                    //var listDiff = filenamestr.Where(i => !names.Contains(i));
                                    //foreach (string s in listDiff)
                                    //{
                                    //    str = s;
                                    //    break;
                                    //}
                                    string[] listDiff = filenamestr;
                                    int numIdx;
                                    for (int j = 0; j < names.Length; j++)
                                    {
                                        for (int i = 0; i < filenamestr.Length; i++)
                                        {

                                            if (filenamestr[i].Replace(ConfigurationManager.AppSettings["ImageURL"].ToString(), ConfigurationManager.AppSettings["PublishedImageURL"].ToString()) == names[j])
                                            {
                                                numIdx = Array.IndexOf(listDiff, filenamestr[i]);
                                                objCom.TestLog("numIdx : " + numIdx);
                                                List<string> tmp = new List<string>(listDiff);
                                                tmp.Remove(filenamestr[i]);
                                                //tmp.RemoveAt(numIdx);
                                                listDiff = tmp.ToArray();
                                            }
                                        }
                                    }
                                    for (int i = 0; i < listDiff.Length; i++)
                                    {
                                        str = listDiff[i];
                                        break;
                                    }
                                    //str = listDiff[0];
                                    objCom.TestLog("str : " + listDiff.Length + " : " + str + " : " + listDiff[0]);
                                    //if (names.Length==1)
                                    //    str = half + "_" + Convert.ToString(names.Length);

                                    //for (int i = 0; i < names.Length; i++)
                                    //{
                                    //    if (names[i].Substring(0, names[i].IndexOf('.')) == half) nam[0] = 0;
                                    //    else if (names[i].Substring(0, names[i].IndexOf('.')) == half + "_" + i) nam[i] = i;
                                    //}
                                    //for (int i = 0; i < Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]); i++)
                                    //{
                                    //    if (nam.Length > i)
                                    //    {
                                    //        if (!(nam[i] == i)) finalnum[i] = i;
                                    //        else finalnum[i] = -i;
                                    //    }
                                    //    else
                                    //    {
                                    //        finalnum[i] = i;
                                    //    }
                                    //}

                                    //str = Convert.ToString(Session["NextAuditId"]) + "_" + Convert.ToString(Session["question_id"]) + "_" + Convert.ToString(filenamestr[nam.Max()]);
                                }
                                else
                                {
                                    str = Convert.ToString(Session["NextAuditId"]) + "_" + Convert.ToString(Session["question_id"]);
                                }
                            }
                            else
                            {
                                str = Convert.ToString(Session["NextAuditId"]) + "_" + Convert.ToString(Session["question_id"]);
                            }
                            string filename = string.Empty;
                            string filenametodb = string.Empty;
                            if (str.Contains("." + System.Drawing.Imaging.ImageFormat.Jpeg))
                            {
                                filename = str;
                                filenametodb = filename.Replace(ConfigurationManager.AppSettings["ImageURL"].ToString(), ConfigurationManager.AppSettings["PublishedImageURL"].ToString());
                            }
                            else
                            {
                                filename = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]) + "/" + str + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                filenametodb = ConfigurationManager.AppSettings["PublishedImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]) + "/" + str + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                            }
                            objCom.TestLog("image_file_name : " + filename);
                            string pathString = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]);
                            if (!Directory.Exists(pathString))
                            {
                                Directory.CreateDirectory(pathString);
                            }
                            if (!string.IsNullOrEmpty(filename))
                            {
                                uplImage.PostedFile.SaveAs(filename);
                                if (String.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"])))
                                {
                                    dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = filenametodb;
                                    dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Server.MapPath(uplImage.FileName);
                                }
                                else
                                {
                                    for (int i = 0; i < names.Length; i++)
                                        imagetable.Rows.Add(new object[] { names[i] });
                                    if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) > 1)
                                    {
                                        if (imagetable.Rows.Count <= Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]))
                                        {
                                            dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]) + ";" + filenametodb;
                                            dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"]) + ";" + Server.MapPath(uplImage.FileName);
                                            //dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path" + Convert.ToString(imagetable.Rows.Count)] = Server.MapPath(uplImage.FileName);
                                        }
                                    }
                                    else if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) == 1)
                                    {
                                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = filenametodb;
                                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Server.MapPath(uplImage.FileName);
                                    }
                                    objCom.TestLog("imagefile : " + Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]));
                                    objCom.TestLog("localimagefile : " + Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"]));
                                }

                            }
                        }

                        Session["dtAnswer"] = dtAnswer;
                    }
                }


                rowcount = Convert.ToInt32(Session["RowCount"]) + 1;
                if (dtData.Rows.Count > rowcount)
                {
                    lblSection.Text = Convert.ToString(dtData.Rows[rowcount]["section_name"]);
                    string helptooltip = "What should be in place?" + Environment.NewLine + Convert.ToString(dtData.Rows[rowcount]["help_text"]) + Environment.NewLine + Environment.NewLine + "How to check?" + Environment.NewLine + Convert.ToString(dtData.Rows[rowcount]["how_to_check"]);
                    //imgHelp.ToolTip = Convert.ToString(dtData.Rows[rowcount]["help_text"]);
                    imgHelp.ToolTip = helptooltip;

                    lblQuestion.Text = Convert.ToInt32(dtData.Rows[rowcount]["question_display_sequence"]) + ". " + Convert.ToString(dtData.Rows[rowcount]["question"]);
                    SetNotApplicableVisibility(Convert.ToString(dtData.Rows[rowcount]["na_flag"]));
                    Session["RowCount"] = rowcount;
                    Session["question_id"] = Convert.ToInt32(dtData.Rows[rowcount]["question_id"]);
                    if (Convert.ToInt32(dtAnswer.Rows[rowcount]["RowCount"]) == Convert.ToInt32(Session["RowCount"]))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["answer"])))
                        {
                            rdbAnswer.SelectedValue = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["answer"]);
                            txtComment.Text = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["remarks"]);
                            //uplImage.PostedFile.FileName = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]);
                            if (rdbAnswer.SelectedValue == "1")
                            {
                                //txtComment.Visible = true;
                                txtComment.Attributes["style"] = "width:80%; display:block;";
                                uplImage.Attributes["style"] = "width:50%; display:block;";
                                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", " visibleComment();", true);
                                if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"])))
                                {
                                    dtImageBind = new DataTable();
                                    dtImageBind.Columns.Add("Image_path");
                                    dtImageBind.Columns.Add("DBImage_path");
                                    string[] image_file_name = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]).Split(';');
                                    string[] Image_path = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"]).Split(';');
                                    DataRow dr = dtImageBind.NewRow();

                                    //dr["Image_path"] = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"]);
                                    //dtImageBind.Rows.Add(dr);
                                    for (int i = 0; i < Convert.ToInt32(Session["max_no_of_pics_allowed"]); i++)
                                    {
                                        if (Image_path.Length > i)
                                        {
                                            if (!String.IsNullOrEmpty(Image_path[i]))
                                            {
                                                dr = dtImageBind.NewRow();
                                                dr["Image_path"] = Convert.ToString(Image_path[i]);
                                                dr["DBImage_path"] = Convert.ToString(image_file_name[i]);
                                                dtImageBind.Rows.Add(dr);
                                            }
                                        }
                                    }
                                    //dr["Image_path"] = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"]);
                                    //dtImageBind.Rows.Add(dr);
                                    //for (int i = 1; i < Convert.ToInt32(Session["max_no_of_pics_allowed"]); i++)
                                    //{
                                    //    if (!String.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path" + i])))
                                    //    {
                                    //        dr = dtImageBind.NewRow();
                                    //        dr["Image_path"] = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path" + i]);
                                    //        dtImageBind.Rows.Add(dr);
                                    //    }
                                    //}
                                    grdImages.DataSource = dtImageBind;
                                    grdImages.DataBind();
                                }
                                else
                                {
                                    grdImages.DataSource = null;
                                    grdImages.DataBind();
                                }
                                if (Convert.ToInt32(dtData.Rows[rowcount]["max_no_of_pics_allowed"]) > 1 && dtImageBind.Rows.Count < Convert.ToInt32(dtData.Rows[rowcount]["max_no_of_pics_allowed"]))
                                {
                                    imgAddImage.Visible = true;
                                    uplImage.Enabled = true;
                                    lblImageResult.Text = "";
                                }
                                else if (Convert.ToInt32(dtData.Rows[rowcount]["max_no_of_pics_allowed"]) == 1)
                                {
                                    imgAddImage.Visible = false;
                                    uplImage.Enabled = true;
                                    lblImageResult.Text = "";
                                }
                                else
                                {
                                    uplImage.Enabled = false;
                                    imgAddImage.Visible = false;
                                    lblImageResult.Text = "You have reached the maximum number of images. Please delete and image if you want to add different image.";
                                }
                            }
                            else
                            {
                                txtComment.Attributes["style"] = "width:80%; display:none;";
                                uplImage.Attributes["style"] = "width:50%; display:none;";
                                grdImages.DataSource = null;
                                grdImages.DataBind();
                            }
                        }
                        else
                        {
                            rdbAnswer.ClearSelection();
                            txtComment.Attributes["style"] = "width:80%; display:none;";
                            uplImage.Attributes["style"] = "width:50%; display:none;";
                            txtComment.Text = "";
                            grdImages.DataSource = null;
                            grdImages.DataBind();
                        }
                    }
                    else
                    {
                        rdbAnswer.ClearSelection();
                        txtComment.Attributes["style"] = "width:80%; display:none;";
                        uplImage.Attributes["style"] = "width:50%; display:none;";
                        txtComment.Text = "";
                        grdImages.DataSource = null;
                        grdImages.DataBind();
                    }

                    if (rowcount == 0)
                    {
                        btnSubmit.Visible = false;
                        btnNext.Visible = true;
                        btnPrevious.Visible = false;
                    }
                    else
                    //if (dtData.Rows.Count - rowcount == 1)
                    {
                        int retUnanswered = ValidateAnswers(dtAnswer);
                        if (retUnanswered == 0)
                        {
                            btnSubmit.Visible = true;
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                        }
                        if (retUnanswered == 1 && dtData.Rows.Count - rowcount == 1)
                        {
                            btnSubmit.Visible = true;
                        }

                        btnPrevious.Visible = true;
                        if (dtData.Rows.Count - rowcount == 1)
                            btnNext.Visible = false;
                        else
                            btnNext.Visible = true;
                    }
                    //else
                    //{
                    //    btnSubmit.Visible = false;
                    //    btnNext.Visible = true;
                    //    btnPrevious.Visible = true;
                    //}
                    //if (dtData.Rows.Count - rowcount == 1)
                    //{
                    //    btnSubmit.Visible = true;
                    //    btnNext.Visible = false;
                    //    btnPrevious.Visible = true;
                    //}
                    //else if (rowcount == 0)
                    //{
                    //    btnNext.Visible = true;
                    //    btnPrevious.Visible = false;
                    //}
                    //else
                    //    btnPrevious.Visible = true;
                }
                ColorChange();
                BindReapeter(Convert.ToInt32(Session["question_id"]));
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : On click of previous button, it will store all the details of cuurent question in dtAnswer datatable and load the panel with previous question details from dtData. It saves the images on the server. The colour of question icon in the question panel is changed here.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            int rowcount;
            lblImageResult.Text = "";
            imgAddImage.Visible = false;
            try
            {
                if (Session["dtAnswer"] != null)
                {
                    dtAnswer = (DataTable)Session["dtAnswer"];
                }
                dtData = new DataTable();

                dtData = (DataTable)Session["dtQuestions"];
                if (!string.IsNullOrEmpty(rdbAnswer.SelectedValue))
                {
                    if (Convert.ToInt32(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["RowCount"]) == Convert.ToInt32(Session["RowCount"]))
                    {
                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["answer"] = rdbAnswer.SelectedValue;
                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["remarks"] = txtComment.Text;
                        if (uplImage.PostedFile != null && uplImage.PostedFile.ContentLength > 0)
                        {

                            string str = String.Empty;
                            string half = String.Empty;

                            int[] nam = new int[Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"])];
                            int[] finalnum = new int[Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"])];
                            string[] filenamestr = new string[Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"])];
                            imagetable.Columns.Add("name", typeof(string));
                            string[] names = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]).Split(';');
                            if (!String.IsNullOrEmpty(Convert.ToString(names[0])))
                            {
                                if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) > 1)
                                {
                                    half = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]) + "/" + Convert.ToString(Session["NextAuditId"]) + "_" + Convert.ToString(Session["question_id"]);
                                    for (int i = 0; i < filenamestr.Length; i++)
                                    {
                                        if (i == 0)
                                            filenamestr[i] = half + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                        else
                                            filenamestr[i] = half + "_" + i + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                    }

                                    //var listDiff = filenamestr.Except(names);
                                    //var listDiff = filenamestr.Where(i => !names.Contains(i));
                                    //foreach (string s in listDiff)
                                    //{
                                    //    str = s;
                                    //    break;
                                    //}
                                    string[] listDiff = filenamestr;
                                    int numIdx;
                                    for (int j = 0; j < names.Length; j++)
                                    {
                                        for (int i = 0; i < filenamestr.Length; i++)
                                        {

                                            if (filenamestr[i].Replace(ConfigurationManager.AppSettings["ImageURL"].ToString(), ConfigurationManager.AppSettings["PublishedImageURL"].ToString()) == names[j])
                                            {
                                                numIdx = Array.IndexOf(listDiff, filenamestr[i]);
                                                objCom.TestLog("numIdx : " + numIdx);
                                                List<string> tmp = new List<string>(listDiff);
                                                tmp.Remove(filenamestr[i]);
                                                //tmp.RemoveAt(numIdx);
                                                listDiff = tmp.ToArray();
                                            }
                                        }
                                    }
                                    for (int i = 0; i < listDiff.Length; i++)
                                    {
                                        str = listDiff[i];
                                        break;
                                    }

                                }
                                else
                                {
                                    str = Convert.ToString(Session["NextAuditId"]) + "_" + Convert.ToString(Session["question_id"]);
                                }
                            }
                            else
                            {
                                str = Convert.ToString(Session["NextAuditId"]) + "_" + Convert.ToString(Session["question_id"]);
                            }
                            string filename = string.Empty;
                            string filenametodb = string.Empty;
                            if (str.Contains("." + System.Drawing.Imaging.ImageFormat.Jpeg))
                            {
                                filename = str;
                                filenametodb = filename.Replace(ConfigurationManager.AppSettings["ImageURL"].ToString(), ConfigurationManager.AppSettings["PublishedImageURL"].ToString());
                            }
                            else
                            {
                                filename = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]) + "/" + str + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                filenametodb = ConfigurationManager.AppSettings["PublishedImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]) + "/" + str + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                            }

                            string pathString = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]);
                            if (!Directory.Exists(pathString))
                            {
                                Directory.CreateDirectory(pathString);
                            }
                            if (!string.IsNullOrEmpty(filename))
                            {
                                uplImage.PostedFile.SaveAs(filename);
                                if (String.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"])))
                                {
                                    dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = filenametodb;
                                    dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Server.MapPath(uplImage.FileName);
                                }
                                else
                                {
                                    for (int i = 0; i < names.Length; i++)
                                        imagetable.Rows.Add(new object[] { names[i] });
                                    if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) > 1)
                                    {
                                        if (imagetable.Rows.Count <= Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]))
                                        {
                                            dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]) + ";" + filenametodb;
                                            dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"]) + ";" + Server.MapPath(uplImage.FileName);
                                            //dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path" + Convert.ToString(imagetable.Rows.Count)] = Server.MapPath(uplImage.FileName);
                                        }
                                    }
                                    else if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) == 1)
                                    {
                                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = filenametodb;
                                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Server.MapPath(uplImage.FileName);
                                    }
                                }

                            }
                        }
                        Session["dtAnswer"] = dtAnswer;
                    }
                }


                //dtData = new DataTable();
                rowcount = Convert.ToInt32(Session["RowCount"]) - 1;
                // dtData = (DataTable)Session["dtQuestions"];
                if (dtData.Rows.Count >= rowcount)
                {
                    lblSection.Text = Convert.ToString(dtData.Rows[rowcount]["section_name"]);
                    string helptooltip = "What should be in place?" + Environment.NewLine + Convert.ToString(dtData.Rows[rowcount]["help_text"]) + Environment.NewLine + "How to check?" + Environment.NewLine + Convert.ToString(dtData.Rows[rowcount]["how_to_check"]);
                    //imgHelp.ToolTip = Convert.ToString(dtData.Rows[rowcount]["help_text"]);
                    imgHelp.ToolTip = helptooltip;
                    lblQuestion.Text = Convert.ToInt32(dtData.Rows[rowcount]["question_display_sequence"]) + ". " + Convert.ToString(dtData.Rows[rowcount]["question"]);
                    SetNotApplicableVisibility(Convert.ToString(dtData.Rows[rowcount]["na_flag"]));
                    Session["RowCount"] = rowcount;
                    Session["question_id"] = Convert.ToInt32(dtData.Rows[rowcount]["question_id"]);
                    if (Convert.ToInt32(dtAnswer.Rows[rowcount]["RowCount"]) == Convert.ToInt32(Session["RowCount"]))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["answer"])))
                        {
                            rdbAnswer.SelectedValue = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["answer"]);
                            txtComment.Text = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["remarks"]);
                            if (rdbAnswer.SelectedValue == "1")
                            {
                                txtComment.Attributes["style"] = "width:80%; display:block;";
                                uplImage.Attributes["style"] = "width:50%; display:block;";
                                if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"])))
                                {
                                    dtImageBind = new DataTable();
                                    dtImageBind.Columns.Add("Image_path");
                                    dtImageBind.Columns.Add("DBImage_path");
                                    string[] image_file_name = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]).Split(';');
                                    string[] Image_path = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"]).Split(';');
                                    DataRow dr = dtImageBind.NewRow();

                                    //dr["Image_path"] = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"]);
                                    //dtImageBind.Rows.Add(dr);
                                    for (int i = 0; i < Convert.ToInt32(Session["max_no_of_pics_allowed"]); i++)
                                    {
                                        if (Image_path.Length > i)
                                        {
                                            if (!String.IsNullOrEmpty(Image_path[i]))
                                            {
                                                dr = dtImageBind.NewRow();
                                                dr["Image_path"] = Convert.ToString(Image_path[i]);
                                                dr["DBImage_path"] = Convert.ToString(image_file_name[i]);
                                                dtImageBind.Rows.Add(dr);
                                            }
                                        }
                                    }
                                    grdImages.DataSource = dtImageBind;
                                    grdImages.DataBind();
                                }
                                else
                                {
                                    grdImages.DataSource = null;
                                    grdImages.DataBind();
                                }

                                if (Convert.ToInt32(dtData.Rows[rowcount]["max_no_of_pics_allowed"]) > 1 && dtImageBind.Rows.Count < Convert.ToInt32(dtData.Rows[rowcount]["max_no_of_pics_allowed"]))
                                {
                                    imgAddImage.Visible = true;
                                    uplImage.Enabled = true;
                                    lblImageResult.Text = "";
                                }
                                else if (Convert.ToInt32(dtData.Rows[rowcount]["max_no_of_pics_allowed"]) == 1)
                                {
                                    imgAddImage.Visible = false;
                                    uplImage.Enabled = true;
                                    lblImageResult.Text = "";
                                }
                                else
                                {
                                    uplImage.Enabled = false;

                                    imgAddImage.Visible = false;
                                    lblImageResult.Text = "You have reached the maximum number of images. Please delete and image if you want to add different image.";
                                }
                            }
                            else
                            {
                                txtComment.Attributes["style"] = "width:80%; display:none;";
                                uplImage.Attributes["style"] = "width:50%; display:none;";
                                grdImages.DataSource = null;
                                grdImages.DataBind();
                            }
                        }
                        else
                        {
                            rdbAnswer.ClearSelection();
                            txtComment.Attributes["style"] = "width:80%; display:none;";
                            uplImage.Attributes["style"] = "width:50%; display:none;";
                            txtComment.Text = "";
                            grdImages.DataSource = null;
                            grdImages.DataBind();
                        }
                    }
                    else
                    {
                        rdbAnswer.ClearSelection();
                        txtComment.Attributes["style"] = "width:80%; display:none;";
                        uplImage.Attributes["style"] = "width:50%; display:none;";
                        txtComment.Text = "";
                        grdImages.DataSource = null;
                        grdImages.DataBind();
                    }
                    if (rowcount == 0)
                    {
                        btnSubmit.Visible = false;
                        btnNext.Visible = true;
                        btnPrevious.Visible = false;
                    }
                    else
                    {
                        int retUnanswered = ValidateAnswers(dtAnswer);
                        if (retUnanswered == 0)
                        {
                            btnSubmit.Visible = true;
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                        }
                        if (retUnanswered == 1 && dtData.Rows.Count - rowcount == 1)
                        {
                            btnSubmit.Visible = true;
                        }

                        btnPrevious.Visible = true;
                        if (dtData.Rows.Count - rowcount == 1)
                            btnNext.Visible = false;
                        else
                            btnNext.Visible = true;

                    }
                    //else
                    //{
                    //    btnSubmit.Visible = false;
                    //    btnNext.Visible = true;
                    //    btnPrevious.Visible = true;
                    //}
                }
                ColorChange();
                BindReapeter(Convert.ToInt32(Session["question_id"]));
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : it stores the question detail in dtAnswer datatable, saves images on server and calls SaveAuditBL method to save audit details to database. It then calls AuditResult.aspx page to send audit report to respective users, to show graph and option to download the audit report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            lblImageResult.Text = "";
            imgAddImage.Visible = false;
            int retUnanswered = 0;
            try
            {
                objPlanBO = new PlanBO();
                objPlanBL = new PlanBL();
                if (Session["dtAnswer"] != null)
                {
                    dtAnswer = (DataTable)Session["dtAnswer"];
                }
                dtData = new DataTable();

                dtData = (DataTable)Session["dtQuestions"];
                if (!string.IsNullOrEmpty(Convert.ToString(rdbAnswer.SelectedValue)))
                {
                    if (Convert.ToInt32(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["RowCount"]) == Convert.ToInt32(Session["RowCount"]))
                    {
                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["answer"] = rdbAnswer.SelectedValue;
                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["remarks"] = txtComment.Text;
                        if (uplImage.PostedFile != null && uplImage.PostedFile.ContentLength > 0)
                        {
                            string str = String.Empty;
                            string half = String.Empty;

                            int[] nam = new int[Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"])];
                            int[] finalnum = new int[Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"])];
                            string[] filenamestr = new string[Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"])];
                            imagetable.Columns.Add("name", typeof(string));
                            string[] names = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]).Split(';');
                            if (!String.IsNullOrEmpty(Convert.ToString(names[0])))
                            {
                                if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) > 1)
                                {
                                    half = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]) + "/" + Convert.ToString(Session["NextAuditId"]) + "_" + Convert.ToString(Session["question_id"]);
                                    for (int i = 0; i < filenamestr.Length; i++)
                                    {
                                        if (i == 0)
                                            filenamestr[i] = half + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                        else
                                            filenamestr[i] = half + "_" + i + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                    }

                                    string[] listDiff = filenamestr;
                                    int numIdx;
                                    for (int j = 0; j < names.Length; j++)
                                    {
                                        for (int i = 0; i < filenamestr.Length; i++)
                                        {

                                            if (filenamestr[i].Replace(ConfigurationManager.AppSettings["ImageURL"].ToString(), ConfigurationManager.AppSettings["PublishedImageURL"].ToString()) == names[j])
                                            {
                                                numIdx = Array.IndexOf(listDiff, filenamestr[i]);
                                                objCom.TestLog("numIdx : " + numIdx);
                                                List<string> tmp = new List<string>(listDiff);
                                                tmp.Remove(filenamestr[i]);
                                                //tmp.RemoveAt(numIdx);
                                                listDiff = tmp.ToArray();
                                            }
                                        }
                                    }
                                    for (int i = 0; i < listDiff.Length; i++)
                                    {
                                        str = listDiff[i];
                                        break;
                                    }

                                }
                                else
                                {
                                    str = Convert.ToString(Session["NextAuditId"]) + "_" + Convert.ToString(Session["question_id"]);
                                }
                            }
                            else
                            {
                                str = Convert.ToString(Session["NextAuditId"]) + "_" + Convert.ToString(Session["question_id"]);
                            }
                            string filename = string.Empty;
                            string filenametodb = string.Empty;
                            if (str.Contains("." + System.Drawing.Imaging.ImageFormat.Jpeg))
                            {
                                filename = str;
                                filenametodb = filename.Replace(ConfigurationManager.AppSettings["ImageURL"].ToString(), ConfigurationManager.AppSettings["PublishedImageURL"].ToString());
                            }
                            else
                            {
                                filename = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]) + "/" + str + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                filenametodb = ConfigurationManager.AppSettings["PublishedImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]) + "/" + str + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                            }

                            string pathString = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]);
                            if (!Directory.Exists(pathString))
                            {
                                Directory.CreateDirectory(pathString);
                            }
                            if (!string.IsNullOrEmpty(filename))
                            {
                                uplImage.PostedFile.SaveAs(filename);
                                if (String.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"])))
                                {
                                    dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = filenametodb;
                                    dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Server.MapPath(uplImage.FileName);
                                }
                                else
                                {
                                    for (int i = 0; i < names.Length; i++)
                                        imagetable.Rows.Add(new object[] { names[i] });
                                    if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) > 1)
                                    {
                                        if (imagetable.Rows.Count <= Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]))
                                        {
                                            dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]) + ";" + filenametodb;
                                            dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"]) + ";" + Server.MapPath(uplImage.FileName);
                                            //dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path" + Convert.ToString(imagetable.Rows.Count)] = Server.MapPath(uplImage.FileName);
                                        }
                                    }
                                    else if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) == 1)
                                    {
                                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = filenametodb;
                                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Server.MapPath(uplImage.FileName);
                                    }
                                }

                            }
                        }
                        Session["dtAnswer"] = dtAnswer;
                    }
                }
                retUnanswered = ValidateAnswers(dtAnswer);
                if (retUnanswered == 0)
                {
                    objPlanBO.p_user_id = Convert.ToInt32(Session["UserId"]);
                    //objPlanBO.p_line_product_rel_id = Convert.ToInt32(ddlPartName.SelectedValue);
                    objPlanBO.p_line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                    //objPlanBO.part_number = Convert.ToString(Page.Request.Form["txtPartNo"]);
                    objPlanBO.p_shift_no = Convert.ToInt32(txtNumber.Text);
                    objPlanBO.p_audit_id = Convert.ToInt32(Session["NextAuditId"]);
                    string UIpattern = "dd-MM-yyyy";
                    string DBPattern = "MM-dd-yyyy";
                    DateTime parsedDate;
                    if (DateTime.TryParseExact(txtDate.Text, UIpattern, null, DateTimeStyles.None, out parsedDate))
                    {
                        string reportdate = parsedDate.ToString(DBPattern);
                        // objPlanBO.p_planned_start_date = Convert.ToDateTime(plannedstartdate);
                        // objDashBO.reportdate = DateTime.Parse(reportdate);
                        objPlanBO.audit_datetime = DateTime.ParseExact(string.Concat(reportdate, " ", txtTime.Text), "MM-dd-yyyy HH:mm", null);
                    }

                    objPlanBO.GlobalAdminFlag = Convert.ToString(Session["GlobalAdminFlag"]);
                    objPlanBO = objPlanBL.SaveAuditBL(objPlanBO, dtAnswer);
                    pnlQuestion.Visible = false;
                    pnlQNumbers.Visible = false;
                    GetMasterDropdown();
                    clearSession();

                    if (objPlanBO.error_code == 0)
                    {
                        Session["objPlanBO"] = objPlanBO;
                        Response.Redirect("AuditResult.aspx");
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('" + objPlanBO.error_msg + "');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('All questions should be answered before submitting. Please see the question panel and answer the unaswered questions.');", true);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : It will store all the dropdown values in sessions and call BindData method to bind question panel. it calls ButtonPanel to display all the question number as the question number can be dynamic. It calls GetAuditId function to fetch the audit id from database for the current audit.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {

                clearSession();
                Session["chart_region"] = Convert.ToString(ddlRegion.SelectedValue);
                Session["chart_country"] = Convert.ToString(ddlCountry.SelectedValue);
                Session["chart_location"] = Convert.ToString(ddlLocation.SelectedValue);
                Session["chart_line"] = Convert.ToString(ddlLineName.SelectedValue);
                //Response.Redirect("AuditResult.aspx");

                pnlQuestion.Visible = true;
                pnlQNumbers.Visible = true;
                Session["pdf_Location"] = Convert.ToString(ddlLocation.SelectedItem);
                Session["pdf_Line"] = Convert.ToString(ddlPartName.SelectedItem);
                //Session["pdf_Part"] = Convert.ToString(ddlLocation.SelectedItem);
                objPlanBO = new PlanBO();
                objPlanBL = new PlanBL();
                objPlanBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objPlanBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                objPlanBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                objPlanBO = objPlanBL.CheckLocationForAuditBL(objPlanBO);
                if (objPlanBO.error_code == 1)
                {
                    BindData();
                    ButtonPanel();
                    GetAuditId();
                }
                else
                {
                    pnlQuestions.Visible = false;
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Selected Region, Country and Location mapping is not correct. Please check and try again. ');", true);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : /// Desc : On click of question button, it will store all the details of cuurent question in dtAnswer datatable and load the panel with selected question details from dtData by calling LoadQuestion method. It saves the images on the server. The colour of question icon in the question panel is changed here by ColorChange method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _text_Click(object sender, EventArgs e)
        {
            try
            {
                lblImageResult.Text = "";
                imgAddImage.Visible = false;
                int name;
                System.Web.UI.WebControls.Button b = sender as System.Web.UI.WebControls.Button;
                if (rdbAnswer.SelectedValue == "1")
                {
                    if (string.IsNullOrEmpty(Convert.ToString(txtComment.Text)))
                    {
                        txtComment.Attributes["style"] = "width:80%; display:block; border-color:red;";
                        uplImage.Attributes["style"] = "width:50%; display:block;";
                    }
                    else
                    {
                        txtComment.Attributes["style"] = "width:80%; display:block; border-color:'';";
                        uplImage.Attributes["style"] = "width:50%; display:block;";
                        if (b != null)
                        {
                            name = Convert.ToInt32(b.ID);
                            LoadQuestion(name - 1);
                        }
                        ColorChange();
                    }
                }
                else
                {
                    if (b != null)
                    {
                        name = Convert.ToInt32(b.ID);
                        LoadQuestion(name - 1);
                    }
                    ColorChange();
                }
                BindReapeter(Convert.ToInt32(Session["question_id"]));

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : Based on selection of Region value, country, location, line will be loaded accordingly and it will load the part by BindPart method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objUserBO = new UsersBO();
                objComBL = new CommonBL();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                //objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                //objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Region";
                // LoadFilterDropDowns(objDashBO);


                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        //ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        //ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        //ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                        dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                        objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                        DataRow drselect = (from DataRow dr in dtLocation.Rows
                                            where (int)dr["location_id"] == objUserBO.location_id
                                            select dr).FirstOrDefault();
                        txtNumber.Text = Convert.ToString(drselect["Shift_no"]);
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    //DataTable dtLineProduct = new DataTable();
                    objUserBO = new UsersBO();
                    objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                    // objComBL = new CommonBL();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLineProduct.Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dtLineProduct;
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        //BindPart();
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
                //BindPart();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : Based on selection of country value, region, location, line will be loaded accordingly and it will load the part by BindPart method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            int region;
            try
            {
                objDashBO = new DashboardBO();
                objUserBO = new UsersBO();
                objComBL = new CommonBL();
                region = Convert.ToInt32(ddlRegion.SelectedValue);
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                //if (objDashBO.country_id == 0)
                //{
                //objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                //}
                //objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Country";
                // LoadFilterDropDowns(objDashBO);

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        //ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        //if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        //{
                        //ddlRegion.SelectedValue = Convert.ToString(region);
                        //}
                        //else
                        //{
                        //    ddlRegion.SelectedIndex = 1;
                        //}
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        //ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        ddlCountry.SelectedValue = Convert.ToString(objDashBO.country_id);
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                        dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                        objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);

                        DataRow drselect = (from DataRow dr in dtLocation.Rows
                                            where (int)dr["location_id"] == objUserBO.location_id
                                            select dr).FirstOrDefault();
                        txtNumber.Text = Convert.ToString(drselect["Shift_no"]);
                        //ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    //DataTable dtLineProduct = new DataTable();
                    objUserBO = new UsersBO();
                    objUserBO.UserId = Convert.ToInt32(Session["UserId"]);

                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLineProduct.Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dtLineProduct;
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        //BindPart();
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
                //BindPart();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : Based on selection of location value, country, region, line will be loaded accordingly and it will load the part by BindPart method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            //DataTable dtLocation = new DataTable();
            int region, country;
            try
            {
                objDashBO = new DashboardBO();
                objUserBO = new UsersBO();
                objComBL = new CommonBL();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                //region = Convert.ToInt32(ddlRegion.SelectedValue);
                //country = Convert.ToInt32(ddlCountry.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //if (objDashBO.location_id == 0)
                //{
                //objDashBO.country_id = country;
                //objDashBO.region_id = region;
                //}
                objDashBO.selection_flag = "Location";

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        //ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        //if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        //{
                        //ddlRegion.SelectedValue = Convert.ToString(region);
                        //}
                        //else
                        //{
                        //    ddlRegion.SelectedIndex = 1;
                        //}
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        //ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        //if (ddlCountry.Items.FindByValue(Convert.ToString(country)) != null && country != 0)
                        //{
                        //ddlCountry.SelectedValue = Convert.ToString(country);
                        //}
                        //else
                        //{
                        //    ddlCountry.SelectedIndex = 1;
                        //}
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        // ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        ddlLocation.SelectedValue = Convert.ToString(objDashBO.location_id);

                        objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                        //dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                        objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                        txtNumber.Text = Convert.ToString(objComBL.getShiftBL(Convert.ToInt32(ddlLocation.SelectedValue)));
                        //DataRow drselect = (from DataRow dr in dtLocation.Rows
                        //                    where (int)dr["location_id"] == objUserBO.location_id
                        //                    select dr).FirstOrDefault();
                        //txtNumber.Text = Convert.ToString(drselect["Shift_no"]);
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    // DataTable dtLineProduct = new DataTable();
                    objUserBO = new UsersBO();
                    objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLineProduct.Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dtLineProduct;
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        //BindPart();
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
                //BindPart();

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : Based on selection of line value, country, region, location will be loaded accordingly and it will load the part by BindPart method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLineName_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objComBL = new CommonBL();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                //if (objDashBO.line_id == 0)
                //{
                //    objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                //    objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                //    objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //}
                objDashBO.selection_flag = "Line";
                // LoadFilterDropDowns(objDashBO);

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        //ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        //ddlRegion.SelectedIndex = 1;
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        //ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        //ddlCountry.SelectedIndex = 1;
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        //ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        //ddlLocation.SelectedIndex = 1;
                        objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                        dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                        objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);

                        DataRow drselect = (from DataRow dr in dtLocation.Rows
                                            where (int)dr["location_id"] == objUserBO.location_id
                                            select dr).FirstOrDefault();
                        txtNumber.Text = Convert.ToString(drselect["Shift_no"]);
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.SelectedValue = Convert.ToString(objDashBO.line_id);
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                    //BindPart();
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : On selection of radio button for answer, the comment panel and image panel should be visible or not and move to next question with the color change in question panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rdbAnswer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(rdbAnswer.SelectedValue) == 0 || Convert.ToInt32(rdbAnswer.SelectedValue) == 2)
                {
                    txtComment.Attributes["style"] = "width:80%; display:none;";
                    uplImage.Attributes["style"] = "width:50%; display:none;";
                    btnNext_Click(null, null);
                }
                else if (Convert.ToInt32(rdbAnswer.SelectedValue) == 1)
                {
                    txtComment.Attributes["style"] = "width:80%; display:block;";
                    uplImage.Attributes["style"] = "width:50%; display:block;";
                    dtData = new DataTable();
                    dtData = (DataTable)Session["dtQuestions"];

                    if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) > 1)
                    {
                        if (dtImageBind != null)
                        {
                            if (dtImageBind.Rows.Count < Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]))
                            {
                                imgAddImage.Visible = true;
                                uplImage.Enabled = true;
                                lblImageResult.Text = "";
                            }
                        }
                        else
                        {
                            imgAddImage.Visible = true;
                            uplImage.Enabled = true;
                            lblImageResult.Text = "";
                        }


                    }
                    else if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) == 1)
                    {
                        imgAddImage.Visible = false;
                        uplImage.Enabled = true;
                        lblImageResult.Text = "";
                    }
                    else
                    {
                        uplImage.Enabled = false;
                        imgAddImage.Visible = false;
                        lblImageResult.Text = "You have reached the maximum number of images. Please delete and image if you want to add different image.";
                    }




                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : On click of delete icon for image, it delete the image path from the datatable dtAnswer and display the rest of it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdImages_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                if (Session["dtAnswer"] != null)
                {
                    dtAnswer = (DataTable)Session["dtAnswer"];
                }
                Label lblPath = (Label)grdImages.Rows[e.RowIndex].FindControl("lblPath");
                string Image_path = lblPath.Text;
                HiddenField hdnPath = (HiddenField)grdImages.Rows[e.RowIndex].FindControl("hdnPath");
                string DBImage_path = hdnPath.Value;

                if (!string.IsNullOrEmpty(rdbAnswer.SelectedValue))
                {
                    if (Convert.ToInt32(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["RowCount"]) == Convert.ToInt32(Session["RowCount"]))
                    {
                        string[] image_file_name = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]).Split(';');
                        string[] physical_path = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"]).Split(';');

                        for (int i = 0; i < physical_path.Length; i++)
                        {
                            if (physical_path[i] == Image_path)
                                physical_path[i] = null;
                            if (image_file_name[i] == DBImage_path)
                                image_file_name[i] = null;
                        }
                        Image_path = physical_path[0];
                        DBImage_path = image_file_name[0];
                        for (int i = 1; i < physical_path.Length; i++)
                        {
                            if (!String.IsNullOrEmpty(physical_path[i]))
                            {
                                Image_path += ";" + physical_path[i];
                            }
                            if (!String.IsNullOrEmpty(image_file_name[i]))
                                DBImage_path += ";" + image_file_name[i];
                        }
                        if (DBImage_path.Substring(0, 1) == ";")
                            dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = DBImage_path.Substring(1, DBImage_path.Length - 1);
                        else
                            dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = DBImage_path;
                        if (Image_path.Substring(0, 1) == ";")
                            dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Image_path.Substring(1, Image_path.Length - 1);
                        else
                            dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Image_path;

                        Session["dtAnswer"] = dtAnswer;
                    }
                }
                if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"])))
                {
                    dtImageBind = new DataTable();
                    dtImageBind.Columns.Add("Image_path");
                    dtImageBind.Columns.Add("DBImage_path");
                    string[] image_file_name1 = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]).Split(';');
                    string[] Image_path1 = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"]).Split(';');
                    DataRow dr = dtImageBind.NewRow();
                    for (int i = 0; i < Convert.ToInt32(Session["max_no_of_pics_allowed"]); i++)
                    {
                        if (Image_path1.Length > i)
                        {
                            if (!String.IsNullOrEmpty(Image_path1[i]))
                            {
                                dr = dtImageBind.NewRow();
                                dr["Image_path"] = Convert.ToString(Image_path1[i]);
                                dr["DBImage_path"] = Convert.ToString(image_file_name1[i]);
                                dtImageBind.Rows.Add(dr);
                            }
                        }
                    }
                    grdImages.DataSource = dtImageBind;
                    grdImages.DataBind();
                }
                else
                {
                    grdImages.DataSource = null;
                    grdImages.DataBind();
                }
                ColorChange();

                dtData = new DataTable();
                dtData = (DataTable)Session["dtQuestions"];

                if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) > 1)
                {
                    if (dtImageBind != null)
                    {
                        if (dtImageBind.Rows.Count < Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]))
                        {
                            imgAddImage.Visible = true;
                            uplImage.Enabled = true;
                            lblImageResult.Text = "";
                        }
                    }
                    else
                    {
                        imgAddImage.Visible = true;
                        uplImage.Enabled = true;
                        lblImageResult.Text = "";
                    }


                }
                else if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) == 1)
                {
                    imgAddImage.Visible = false;
                    uplImage.Enabled = true;
                    lblImageResult.Text = "";
                }
                else
                {
                    uplImage.Enabled = false;
                    imgAddImage.Visible = false;
                    lblImageResult.Text = "You have reached the maximum number of images. Please delete and image if you want to add different image.";
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        //public override void VerifyRenderingInServerForm(System.Web.UI.Control control)
        //{

        //}

        

        //protected void rdbAnswer_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (rdbAnswer.SelectedValue == "1")
        //        {
        //            txtComment.Visible = true;
        //        }
        //        else
        //        {
        //            txtComment.Visible = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        

        //protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtLineProduct = new DataTable();
        //    DataTable dtCountry = new DataTable();
        //    DataTable dtLocation = new DataTable();
        //    try
        //    {
        //        dsDropDownData = new DataSet();
        //        objUserBO = new UsersBO();
        //        objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
        //        objComBL = new CommonBL();

        //        objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
        //        dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

        //        if (dtCountry.Rows.Count > 0)
        //        {
        //            ddlCountry.DataSource = dtCountry;
        //            ddlCountry.DataTextField = "country_name";
        //            ddlCountry.DataValueField = "country_id";
        //            ddlCountry.DataBind();
        //            objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
        //        }
        //        else
        //        {
        //            ddlCountry.Items.Clear();
        //            ddlCountry.DataSource = null;
        //            ddlCountry.DataBind();
        //        }

        //        dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
        //        if (dtLocation.Rows.Count > 0)
        //        {
        //            ddlLocation.DataSource = dtLocation;
        //            ddlLocation.DataTextField = "location_name";
        //            ddlLocation.DataValueField = "location_id";
        //            ddlLocation.DataBind();
        //            objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //            DataRow drselect = (from DataRow dr in dtLocation.Rows
        //                                where (int)dr["location_id"] == objUserBO.location_id
        //                                select dr).FirstOrDefault();
        //            txtNumber.Text = Convert.ToString(drselect["Shift_no"]);
        //        }
        //        else
        //        {
        //            ddlLocation.Items.Clear();
        //            ddlLocation.DataSource = null;
        //            ddlLocation.DataBind();
        //            txtNumber.Text = "";
        //        }

        //        dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
        //        if (dtLineProduct.Rows.Count > 0)
        //        {
        //            ddlLineName.DataSource = dtLineProduct;
        //            ddlLineName.DataTextField = "line_name";
        //            ddlLineName.DataValueField = "line_id";
        //            ddlLineName.DataBind();
        //            Session["dtLine"] = dtLineProduct;
        //            BindPart();
        //        }
        //        else
        //        {
        //            ddlLineName.Items.Clear();
        //            ddlLineName.DataSource = null;
        //            ddlLineName.DataBind();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtLineProduct = new DataTable();
        //    DataTable dtCountry = new DataTable();
        //    DataTable dtLocation = new DataTable();
        //    try
        //    {
        //        dsDropDownData = new DataSet();
        //        objUserBO = new UsersBO();
        //        objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
        //        objComBL = new CommonBL();

        //        objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
        //        dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
        //        Session["dtLocation"] = dtLocation;
        //        if (dtLocation.Rows.Count > 0)
        //        {
        //            ddlLocation.DataSource = dtLocation;
        //            ddlLocation.DataTextField = "location_name";
        //            ddlLocation.DataValueField = "location_id";
        //            ddlLocation.DataBind();
        //            objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //            DataRow drselect = (from DataRow dr in dtLocation.Rows
        //                                where (int)dr["location_id"] == objUserBO.location_id
        //                                select dr).FirstOrDefault();
        //            txtNumber.Text = Convert.ToString(drselect["Shift_no"]);
        //        }
        //        else
        //        {
        //            ddlLocation.Items.Clear();
        //            ddlLocation.DataSource = null;
        //            ddlLocation.DataBind();
        //            txtNumber.Text = "";
        //        }
        //        objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //        dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
        //        if (dtLineProduct.Rows.Count > 0)
        //        {
        //            ddlLineName.DataSource = dtLineProduct;
        //            ddlLineName.DataTextField = "line_name";
        //            ddlLineName.DataValueField = "line_id";
        //            ddlLineName.DataBind();
        //            Session["dtLine"] = dtLineProduct;
        //            BindPart();
        //        }
        //        else
        //        {
        //            ddlLineName.Items.Clear();
        //            ddlLineName.DataSource = null;
        //            ddlLineName.DataBind();
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtLineProduct = new DataTable();
        //    DataTable dtLocation = new DataTable();
        //    try
        //    {
        //        objUserBO = new UsersBO();
        //        objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
        //        objComBL = new CommonBL();
        //        dtLocation = (DataTable)Session["dtLocation"];
        //        objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //        DataRow drselect = (from DataRow dr in dtLocation.Rows
        //                            where (int)dr["location_id"] == objUserBO.location_id
        //                            select dr).FirstOrDefault();
        //        txtNumber.Text = Convert.ToString(drselect["Shift_no"]);
        //        dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
        //        if (dtLineProduct.Rows.Count > 0)
        //        {
        //            ddlLineName.DataSource = dtLineProduct;
        //            ddlLineName.DataTextField = "line_name";
        //            ddlLineName.DataValueField = "line_id";
        //            ddlLineName.DataBind();
        //            Session["dtLine"] = dtLineProduct;

        //        }
        //        else
        //        {
        //            ddlLineName.Items.Clear();
        //            ddlLineName.DataSource = null;
        //            ddlLineName.DataBind();
        //        }
        //        BindPart();
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlLineName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    BindPart();
        //}

        #endregion

        #region Methods
        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : It calls GetMaxAuditId method to fetch the new audit id from database and stores in NextAuditId session.
        /// </summary>
        private void GetAuditId()
        {
            try
            {
                objPlanBL = new PlanBL();
                objPlanBO = new PlanBO();
                objPlanBO = objPlanBL.GetMaxAuditId();
                Session["NextAuditId"] = objPlanBO.p_audit_id;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : It returns the number of questions which are not answered and based on result "Submit" button will be visible.
        /// </summary>
        /// <param name="dtAnswerResult"></param>
        /// <returns></returns>
        private int ValidateAnswers(DataTable dtAnswerResult)
        {
            int retVal = 0;
            try
            {
                for (int i = 0; i < dtAnswerResult.Rows.Count; i++)
                {
                    if (string.IsNullOrEmpty(Convert.ToString(dtAnswerResult.Rows[i]["answer"])))
                        retVal++;
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
                retVal++;
            }
            return retVal;
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : It clears all the session used in this page
        /// </summary>
        private void clearSession()
        {
            Session["dtAnswer"] = null;
            Session["dtQuestions"] = null;
            Session["RowCount"] = null;
            Session["part"] = null;
            dtAnswer = new DataTable();
            dtData = new DataTable();
            var allButtons = DivQ1.Controls.OfType<Button>().ToList();
            foreach (Control item in allButtons)
            {
                if (item != null)
                    DivQ1.Controls.Remove(item);
            }
            allButtons = DivQ2.Controls.OfType<Button>().ToList();
            foreach (Control item in allButtons)
            {
                if (item != null)
                    DivQ2.Controls.Remove(item);
            }
            allButtons = DivQ3.Controls.OfType<Button>().ToList();
            foreach (Control item in allButtons)
            {
                if (item != null)
                    DivQ3.Controls.Remove(item);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : It calls GetAllMasterBL method with userid as parameter from CommonFunctionBL class to fetch region.country, location and line data and it bind to respective dropdowns.(Not In use as requirement might get changed)
        /// </summary>
        private void GetMasterDropdown()
        {
            DataTable dtLine = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();

            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);

                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ddlRegion.SelectedValue)))
                {
                    objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                    dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                    if (dtCountry.Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dtCountry;
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ddlCountry.SelectedValue)))
                {
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                    dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                    Session["dtLocation"] = dtLocation;
                    if (dtLocation.Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dtLocation;
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ddlLocation.SelectedValue)))
                {
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    DataRow drselect = (from DataRow dr in dtLocation.Rows
                                        where (int)dr["location_id"] == objUserBO.location_id
                                        select dr).FirstOrDefault();
                    txtNumber.Text = Convert.ToString(drselect["Shift_no"]);
                    dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLine.Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dtLine;
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        Session["dtLine"] = dtLine;
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
                else
                {
                    ddlLineName.Items.Clear();
                    ddlLineName.DataSource = null;
                    ddlLineName.DataBind();
                }
                //BindPart();

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : It calls GetPartBasedOnLineBL method and bind the part with part textbox.
        /// </summary>
        public void BindPart()
        {
            Session["part"] = null;
            DataTable dtPart = new DataTable();
            DataRow[] dr;
            DataTable dtLine = new DataTable();
            objUserBO = new UsersBO();
            objComBL = new CommonBL();
            try
            {
                //dtLine = (DataTable)Session["dtLine"];
                //dr = dtLine.Select("line_name='" + Convert.ToString(ddlLineName.SelectedItem)+"'");
                //dtPart = dtLine.Clone();
                //foreach (DataRow row in dr)
                //{
                //    dtPart.Rows.Add(row.ItemArray);
                //}
                //if (dtPart.Rows.Count > 0)
                //{
                //    ddlPartName.DataSource = dtPart;
                //    ddlPartName.DataTextField = "product_name";
                //    ddlPartName.DataValueField = "line_product_rel_id";
                //}
                //else
                //    ddlPartName.DataSource = null;
                //ddlPartName.DataBind();
                if (!string.IsNullOrEmpty(Convert.ToString(ddlLineName.SelectedValue)))
                {
                    objUserBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                    dtPart = objComBL.GetPartBasedOnLineBL(objUserBO);
                    if (dtPart.Rows.Count > 0)
                    {
                        Session["Parts"] = dtPart;
                        ddlPartName.DataSource = dtPart;
                        ddlPartName.DataTextField = "Part_code";
                        ddlPartName.DataValueField = "line_product_rel_id";
                        ddlPartName.DataBind();
                    }
                    else
                    {
                        ddlPartName.Items.Clear();
                        ddlPartName.DataSource = null;
                        ddlPartName.DataBind();
                    }
                }
                else
                {
                    ddlPartName.Items.Clear();
                    ddlPartName.DataSource = null;
                    ddlPartName.DataBind();
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : This method is used to bind question information to question panel. It calls BindReapeter for binding alll the comments for the particular question
        /// </summary>
        private void BindData()
        {
            DataTable dtQuestions;
            DataTable dtSec1;
            try
            {
                dtAnswer = new DataTable();
                dtAnswer.Columns.Add("question_id", typeof(int));
                dtAnswer.Columns.Add("answer", typeof(int));
                dtAnswer.Columns.Add("RowCount", typeof(int));
                dtAnswer.Columns.Add("remarks", typeof(string));
                dtAnswer.Columns.Add("image_file_name", typeof(string));
                dtAnswer.Columns.Add("display_seq", typeof(int));
                dtAnswer.Columns.Add("Image_path", typeof(string));


                objPlanBO = new PlanBO();
                objPlanBL = new PlanBL();
                dtQuestions = new DataTable();
                dtSec1 = new DataTable();
                objPlanBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objPlanBO.p_line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                dtQuestions = objPlanBL.GetQuestionsForAuditBL(objPlanBO);
                int maxValue = (int)dtQuestions.Compute("MAX([max_no_of_pics_allowed])", "");
                Session["max_no_of_pics_allowed"] = maxValue;
                //for (int i = 1; i < maxValue; i++)
                //{
                //    dtAnswer.Columns.Add("Image_path" + i, typeof(string));
                //}
                Session["dtQuestions"] = dtQuestions;
                if (dtQuestions != null)
                {
                    if (dtQuestions.Rows.Count > 0)
                    {
                        lblResult.Text = "";
                        pnlQuestions.Visible = true;
                        lblSection.Text = Convert.ToString(dtQuestions.Rows[0]["section_name"]);
                        string helptooltip = "What should be in place?" + Environment.NewLine + Convert.ToString(dtQuestions.Rows[0]["help_text"]) + Environment.NewLine + "How to check?" + Environment.NewLine + Convert.ToString(dtQuestions.Rows[0]["how_to_check"]);
                        //imgHelp.ToolTip = Convert.ToString(dtData.Rows[rowcount]["help_text"]);
                        imgHelp.ToolTip = helptooltip;
                        lblQuestion.Text = Convert.ToInt32(dtQuestions.Rows[0]["question_display_sequence"]) + ". " + Convert.ToString(dtQuestions.Rows[0]["question"]);
                        SetNotApplicableVisibility(Convert.ToString(dtQuestions.Rows[0]["na_flag"]));

                        Session["RowCount"] = 0;
                        Session["question_id"] = Convert.ToInt32(dtQuestions.Rows[0]["question_id"]);
                        DataRow row;
                        for (int i = 0; i < dtQuestions.Rows.Count; i++)
                        {
                            row = dtAnswer.NewRow();
                            row["RowCount"] = i;
                            row["question_id"] = Convert.ToInt32(dtQuestions.Rows[i]["question_id"]);
                            row["display_seq"] = Convert.ToInt32(dtQuestions.Rows[i]["question_display_sequence"]);
                            dtAnswer.Rows.Add(row);
                        }
                        Session["dtAnswer"] = dtAnswer;
                    }
                    else
                    {
                        pnlQuestions.Visible = false;
                        lblResult.Text = "There are no questions for selection.";
                    }
                }
                else
                {
                    pnlQuestions.Visible = false;
                    lblResult.Text = "There are no questions for selection.";
                }
                BindReapeter(Convert.ToInt32(Session["question_id"]));
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }


        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : it checks if NA flag is true then show the NOt applicable option else it does not.
        /// </summary>
        /// <param name="p">Y for NA applicable, else for NA not applicable</param>
        private void SetNotApplicableVisibility(string p)
        {
            try
            {
                if (Convert.ToString(p) == "Y")
                {
                    if (rdbAnswer.Items.Count == 2)
                    {
                        System.Web.UI.WebControls.ListItem lst = new System.Web.UI.WebControls.ListItem("Not Applicable", "2");
                        rdbAnswer.Items.Add(lst);
                        rdbAnswer.DataBind();
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(rdbAnswer.Items[2])))
                    {
                        rdbAnswer.Items.RemoveAt(2);
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        
        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : it loads all the question button in question panel.
        /// </summary>
        public void ButtonPanel()
        {
            try
            {
                if (Session["dtAnswer"] != null)
                {
                    dtAnswer = (DataTable)Session["dtAnswer"];
                }

                for (int i = 1; i <= dtAnswer.Rows.Count; i++)
                {

                    System.Web.UI.WebControls.Button _text = new System.Web.UI.WebControls.Button();
                    _text.Visible = true;
                    //_text.Text = i.ToString();
                    _text.Text = Convert.ToString(dtAnswer.Rows[i - 1]["display_seq"]);
                    _text.ID = i.ToString();
                    if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[i - 1]["answer"])))
                    {
                        if (Convert.ToInt32(dtAnswer.Rows[i - 1]["answer"]) == 1)
                            _text.CssClass = "btn-NoAnswered";
                        else if (Convert.ToInt32(dtAnswer.Rows[i - 1]["answer"]) == 2)
                            _text.CssClass = "btn-NAAnswered";
                        else if (Convert.ToInt32(dtAnswer.Rows[i - 1]["answer"]) == 0)
                            _text.CssClass = "btn-YesAnswered";
                        //if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[i - 1]["image_file_name"])))
                        //    _text.CssClass = "btn-NoAnswered";
                        //else if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[i - 1]["answer"])))
                        //    _text.CssClass = "btn-YesAnswered";
                        else
                            _text.CssClass = "btn-default";

                    }
                    else
                    {
                        _text.CssClass = "btn-default";
                    }
                    if (i % 3 == 1)
                    {
                        DivQ1.Controls.Add(_text);
                    }
                    else if (i % 3 == 2)
                    {
                        DivQ2.Controls.Add(_text);
                    }
                    else if (i % 3 == 0)
                    {
                        DivQ3.Controls.Add(_text);
                    }
                    _text.Click += new EventHandler(_text_Click);
                    PostBackTrigger trigger = new PostBackTrigger();
                    trigger.ControlID = _text.ID;
                    updQuestion.Triggers.Add(trigger);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : It stores the answer details to dtAnswer datatable and load the question vpassed in parameter
        /// </summary>
        /// <param name="Qnum">question number</param>
        public void LoadQuestion(int Qnum)
        {
            try
            {
                if (Session["dtAnswer"] != null)
                {
                    dtAnswer = (DataTable)Session["dtAnswer"];
                }
                dtData = new DataTable();

                dtData = (DataTable)Session["dtQuestions"];
                if (!string.IsNullOrEmpty(rdbAnswer.SelectedValue))
                {
                    if (Convert.ToInt32(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["RowCount"]) == Convert.ToInt32(Session["RowCount"]))
                    {
                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["answer"] = rdbAnswer.SelectedValue;
                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["remarks"] = txtComment.Text;
                        if (uplImage.PostedFile != null && uplImage.PostedFile.ContentLength > 0)
                        {
                            string str = String.Empty;
                            string half = String.Empty;

                            int[] nam = new int[Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"])];
                            int[] finalnum = new int[Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"])];
                            string[] filenamestr = new string[Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"])];
                            imagetable.Columns.Add("name", typeof(string));
                            string[] names = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]).Split(';');
                            if (!String.IsNullOrEmpty(Convert.ToString(names[0])))
                            {
                                if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) > 1)
                                {
                                    half = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]) + "/" + Convert.ToString(Session["NextAuditId"]) + "_" + Convert.ToString(Session["question_id"]);
                                    for (int i = 0; i < filenamestr.Length; i++)
                                    {
                                        if (i == 0)
                                            filenamestr[i] = half + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                        else
                                            filenamestr[i] = half + "_" + i + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                    }

                                    string[] listDiff = filenamestr;
                                    int numIdx;
                                    for (int j = 0; j < names.Length; j++)
                                    {
                                        for (int i = 0; i < filenamestr.Length; i++)
                                        {

                                            if (filenamestr[i].Replace(ConfigurationManager.AppSettings["ImageURL"].ToString(), ConfigurationManager.AppSettings["PublishedImageURL"].ToString()) == names[j])
                                            {
                                                numIdx = Array.IndexOf(listDiff, filenamestr[i]);
                                                objCom.TestLog("numIdx : " + numIdx);
                                                List<string> tmp = new List<string>(listDiff);
                                                tmp.Remove(filenamestr[i]);
                                                
                                                //tmp.RemoveAt(numIdx);
                                                listDiff = tmp.ToArray();
                                            }
                                        }
                                    }
                                    for (int i = 0; i < listDiff.Length; i++)
                                    {
                                        str = listDiff[i];
                                        break;
                                    }

                                }
                                else
                                {
                                    str = Convert.ToString(Session["NextAuditId"]) + "_" + Convert.ToString(Session["question_id"]);
                                }
                            }
                            else
                            {
                                str = Convert.ToString(Session["NextAuditId"]) + "_" + Convert.ToString(Session["question_id"]);
                            }
                            string filename = string.Empty;
                            string filenametodb = string.Empty;
                            if (str.Contains("." + System.Drawing.Imaging.ImageFormat.Jpeg))
                            {
                                filename = str;
                                filenametodb = filename.Replace(ConfigurationManager.AppSettings["ImageURL"].ToString(), ConfigurationManager.AppSettings["PublishedImageURL"].ToString());
                            }
                            else
                            {
                                filename = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]) + "/" + str + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                filenametodb = ConfigurationManager.AppSettings["PublishedImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]) + "/" + str + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                            }

                            string pathString = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]);
                            if (!Directory.Exists(pathString))
                            {
                                Directory.CreateDirectory(pathString);
                            }
                            if (!string.IsNullOrEmpty(filename))
                            {
                                uplImage.PostedFile.SaveAs(filename);
                                if (String.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"])))
                                {
                                    dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = filenametodb;
                                    dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Server.MapPath(uplImage.FileName);
                                }
                                else
                                {
                                    for (int i = 0; i < names.Length; i++)
                                        imagetable.Rows.Add(new object[] { names[i] });
                                    if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) > 1)
                                    {
                                        if (imagetable.Rows.Count <= Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]))
                                        {
                                            dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]) + ";" + filenametodb;
                                            dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"]) + ";" + Server.MapPath(uplImage.FileName);
                                            //dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path" + Convert.ToString(imagetable.Rows.Count)] = Server.MapPath(uplImage.FileName);
                                        }
                                    }
                                    else if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) == 1)
                                    {
                                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = filenametodb;
                                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Server.MapPath(uplImage.FileName);
                                    }
                                }

                            }
                        }
                        Session["dtAnswer"] = dtAnswer;
                    }
                }

                //dtData = new DataTable();
                //dtData = (DataTable)Session["dtQuestions"];
                if (dtData.Rows.Count > Qnum)
                {
                    lblSection.Text = Convert.ToString(dtData.Rows[Qnum]["section_name"]);
                    string helptooltip = "What should be in place?" + Environment.NewLine + Convert.ToString(dtData.Rows[Qnum]["help_text"]) + Environment.NewLine + "How to check?" + Environment.NewLine + Convert.ToString(dtData.Rows[Qnum]["how_to_check"]);
                    //imgHelp.ToolTip = Convert.ToString(dtData.Rows[rowcount]["help_text"]);
                    imgHelp.ToolTip = helptooltip;
                    lblQuestion.Text = Convert.ToInt32(dtData.Rows[Qnum]["question_display_sequence"]) + ". " + Convert.ToString(dtData.Rows[Qnum]["question"]);
                    SetNotApplicableVisibility(Convert.ToString(dtData.Rows[Qnum]["na_flag"]));
                    Session["RowCount"] = Qnum;
                    Session["question_id"] = Convert.ToInt32(dtData.Rows[Qnum]["question_id"]);
                    if (Convert.ToInt32(dtAnswer.Rows[Qnum]["RowCount"]) == Convert.ToInt32(Session["RowCount"]))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["answer"])))
                        {
                            rdbAnswer.SelectedValue = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["answer"]);
                            txtComment.Text = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["remarks"]);
                            if (rdbAnswer.SelectedValue == "1")
                            {
                                // txtComment.Visible = true;
                                txtComment.Attributes["style"] = "width:80%; display:block;";
                                uplImage.Attributes["style"] = "width:50%; display:block;";

                                if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"])))
                                {
                                    dtImageBind = new DataTable();
                                    dtImageBind.Columns.Add("Image_path");
                                    dtImageBind.Columns.Add("DBImage_path");
                                    string[] image_file_name = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]).Split(';');
                                    string[] Image_path = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"]).Split(';');
                                    DataRow dr = dtImageBind.NewRow();

                                    //dr["Image_path"] = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"]);
                                    //dtImageBind.Rows.Add(dr);
                                    for (int i = 0; i < Convert.ToInt32(Session["max_no_of_pics_allowed"]); i++)
                                    {
                                        if (Image_path.Length > i)
                                        {
                                            if (!String.IsNullOrEmpty(Image_path[i]))
                                            {
                                                dr = dtImageBind.NewRow();
                                                dr["Image_path"] = Convert.ToString(Image_path[i]);
                                                dr["DBImage_path"] = Convert.ToString(image_file_name[i]);
                                                dtImageBind.Rows.Add(dr);
                                            }
                                        }
                                    }
                                    grdImages.DataSource = dtImageBind;
                                    grdImages.DataBind();
                                }
                                else
                                {
                                    grdImages.DataSource = null;
                                    grdImages.DataBind();
                                }
                                if (Convert.ToInt32(dtData.Rows[Qnum]["max_no_of_pics_allowed"]) > 1 && dtImageBind.Rows.Count < Convert.ToInt32(dtData.Rows[Qnum]["max_no_of_pics_allowed"]))
                                {
                                    imgAddImage.Visible = true;
                                    uplImage.Enabled = true;
                                    lblImageResult.Text = "";
                                }
                                else if (Convert.ToInt32(dtData.Rows[Qnum]["max_no_of_pics_allowed"]) == 1)
                                {
                                    imgAddImage.Visible = false;
                                    uplImage.Enabled = true;
                                    lblImageResult.Text = "";
                                }
                                else
                                {
                                    uplImage.Enabled = false;
                                    imgAddImage.Visible = false;
                                    lblImageResult.Text = "You have reached the maximum number of images. Please delete and image if you want to add different image.";
                                }
                            }
                            else
                            {
                                txtComment.Attributes["style"] = "width:80%; display:none;";
                                uplImage.Attributes["style"] = "width:50%; display:none;";
                                grdImages.DataSource = null;
                                grdImages.DataBind();
                            }
                        }
                        else
                        {
                            rdbAnswer.ClearSelection();
                            txtComment.Attributes["style"] = "width:80%; display:none;";
                            uplImage.Attributes["style"] = "width:50%; display:none;";
                            txtComment.Text = "";
                            grdImages.DataSource = null;
                            grdImages.DataBind();
                        }
                    }
                    else
                    {
                        rdbAnswer.ClearSelection();
                        txtComment.Attributes["style"] = "width:80%; display:none;";
                        uplImage.Attributes["style"] = "width:50%; display:none;";
                        txtComment.Text = "";
                        grdImages.DataSource = null;
                        grdImages.DataBind();
                    }

                    if (Qnum == 0)
                    {
                        btnSubmit.Visible = false;
                        btnNext.Visible = true;
                        btnPrevious.Visible = false;
                    }
                    else
                    {
                        int retUnanswered = ValidateAnswers(dtAnswer);
                        if (retUnanswered == 0)
                        {
                            btnSubmit.Visible = true;
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                        }
                        if (retUnanswered == 1 && dtData.Rows.Count - Qnum == 1)
                        {
                            btnSubmit.Visible = true;
                        }

                        btnPrevious.Visible = true;
                        if (dtData.Rows.Count - Qnum == 1)
                            btnNext.Visible = false;
                        else
                            btnNext.Visible = true;
                    }
                    //else
                    //{
                    //    btnSubmit.Visible = false;
                    //    btnNext.Visible = true;
                    //    btnPrevious.Visible = true;
                    //}
                }


            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : Based on selection of answers the question button css is being changed here.
        /// </summary>
        public void ColorChange()
        {
            try
            {
                if (Session["dtAnswer"] != null)
                {
                    dtAnswer = (DataTable)Session["dtAnswer"];
                }

                for (int i = 1; i <= dtAnswer.Rows.Count; i++)
                {

                    System.Web.UI.WebControls.Button _text = (System.Web.UI.WebControls.Button)DivQ1.FindControl(Convert.ToString(i));
                    System.Web.UI.WebControls.Button _text1 = (System.Web.UI.WebControls.Button)DivQ2.FindControl(Convert.ToString(i));
                    System.Web.UI.WebControls.Button _text2 = (System.Web.UI.WebControls.Button)DivQ3.FindControl(Convert.ToString(i));
                    if (_text != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[i - 1]["answer"])))
                        {
                            if (Convert.ToInt32(dtAnswer.Rows[i - 1]["answer"]) == 1)
                                _text.CssClass = "btn-NoAnswered";
                            else if (Convert.ToInt32(dtAnswer.Rows[i - 1]["answer"]) == 2)
                                _text.CssClass = "btn-NAAnswered";
                            else if (Convert.ToInt32(dtAnswer.Rows[i - 1]["answer"]) == 0)
                                _text.CssClass = "btn-YesAnswered";
                            //if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[i - 1]["image_file_name"])))
                            //    _text.CssClass = "btn-NoAnswered";
                            //else if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[i - 1]["answer"])))
                            //    _text.CssClass = "btn-YesAnswered";
                            else
                                _text.CssClass = "btn-default";
                        }
                        else
                        {
                            _text.CssClass = "btn-default";
                        }
                    }
                    else if (_text1 != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[i - 1]["answer"])))
                        {
                            if (Convert.ToInt32(dtAnswer.Rows[i - 1]["answer"]) == 1)
                                _text.CssClass = "btn-NoAnswered";
                            else if (Convert.ToInt32(dtAnswer.Rows[i - 1]["answer"]) == 2)
                                _text.CssClass = "btn-NAAnswered";
                            else if (Convert.ToInt32(dtAnswer.Rows[i - 1]["answer"]) == 0)
                                _text.CssClass = "btn-YesAnswered";
                            //if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[i - 1]["image_file_name"])))
                            //    _text.CssClass = "btn-NoAnswered";
                            //else if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[i - 1]["answer"])))
                            //    _text.CssClass = "btn-YesAnswered";
                            else
                                _text.CssClass = "btn-default";
                        }
                        else
                        {
                            _text1.CssClass = "btn-default";
                        }
                    }
                    else if (_text2 != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[i - 1]["answer"])))
                        {
                            if (Convert.ToInt32(dtAnswer.Rows[i - 1]["answer"]) == 1)
                                _text.CssClass = "btn-NoAnswered";
                            else if (Convert.ToInt32(dtAnswer.Rows[i - 1]["answer"]) == 2)
                                _text.CssClass = "btn-NAAnswered";
                            else if (Convert.ToInt32(dtAnswer.Rows[i - 1]["answer"]) == 0)
                                _text.CssClass = "btn-YesAnswered";
                            //if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[i - 1]["image_file_name"])))
                            //    _text.CssClass = "btn-NoAnswered";
                            //else if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[i - 1]["answer"])))
                            //    _text.CssClass = "btn-YesAnswered";
                            else
                                _text.CssClass = "btn-default";
                        }
                        else
                        {
                            _text2.CssClass = "btn-default";
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : It calls GetCommentsBL methoda to fetch all the comments for question_id passed in parameter. It binds the comments to repeater.
        /// </summary>
        /// <param name="question_id"></param>
        public void BindReapeter(int question_id)
        {
            int line_id = 0;
            DataTable dtComments;
            int CommentExists;
            try
            {
                objPlanBO = new PlanBO();
                objPlanBL = new PlanBL();
                dtComments = new DataTable();
                line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objPlanBO.p_line_id = line_id;
                objPlanBO.question_id = question_id;
                CommentExists = objPlanBL.IsCommentsExistsBL(objPlanBO);
                if (CommentExists == 1)
                {
                    dtComments = objPlanBL.GetCommentsBL(objPlanBO);
                    if (dtComments != null)
                    {
                        imgComment.Attributes["style"] = "float: right; vertical-align: top; height: 55px; width: 50px; display:block;";
                        if (dtComments.Rows.Count > 0)
                        {
                            lblComments.Text = "";
                            rptComments.Visible = true;
                            rptComments.DataSource = dtComments;
                            rptComments.DataBind();
                        }
                        else
                        {
                            rptComments.Visible = false;
                            lblComments.Text = "No History found";
                        }
                    }
                    else
                    {
                        rptComments.Visible = false;
                        lblComments.Text = "No History found";
                    }
                }
                else
                {
                    imgComment.Attributes["style"] = "display:none;";
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : 
        /// </summary>
        /// <param name="strpath"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethod]
        public static string SetDownloadPath(string strpath)
        {
            Page objp = new Page();
            objp.Session["Print"] = strpath;
            return strpath;
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : It accepts the character entered in Part textbox and load related part from Part session value.
        /// </summary>
        /// <param name="Name">text entered in part textbox</param>
        /// <returns></returns>
        [WebMethod]
        public static List<string> GetPartNumbers(string Name)
        {
            List<string> empResult = new List<string>();
            DataTable dtParts = new DataTable();
            dtParts = (DataTable)HttpContext.Current.Session["Parts"];
            if (dtParts.Rows.Count > 0)
            {
                DataRow[] dr = dtParts.Select("Part_code like '" + Name + "%'");
                foreach (DataRow dr1 in dr)
                {
                    empResult.Add(Convert.ToString(dr1["Part_code"]));
                }
            }
            return empResult;
            //return Convert.ToString(HttpContext.Current.Session["json"]);
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : Addimage button is visible for some question, when click on this button, image will be saved to server and bind the image grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgAddImage_Click(object sender, ImageClickEventArgs e)
        {
            int rowcount;

            try
            {
                if (Session["dtAnswer"] != null)
                {
                    dtAnswer = (DataTable)Session["dtAnswer"];
                }
                dtData = new DataTable();
                dtData = (DataTable)Session["dtQuestions"];
                if (!string.IsNullOrEmpty(rdbAnswer.SelectedValue))
                {
                    if (Convert.ToInt32(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["RowCount"]) == Convert.ToInt32(Session["RowCount"]))
                    {
                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["answer"] = rdbAnswer.SelectedValue;
                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["remarks"] = txtComment.Text;
                        if (uplImage.PostedFile != null && uplImage.PostedFile.ContentLength > 0)
                        {

                            string str = String.Empty;
                            string half = String.Empty;

                            int[] nam = new int[Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"])];
                            int[] finalnum = new int[Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"])];
                            string[] filenamestr = new string[Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"])];
                            imagetable.Columns.Add("name", typeof(string));
                            string[] names = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]).Split(';');
                            if (!String.IsNullOrEmpty(Convert.ToString(names[0])))
                            {
                                if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) > 1)
                                {
                                    half = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]) + "/" + Convert.ToString(Session["NextAuditId"]) + "_" + Convert.ToString(Session["question_id"]);
                                    for (int i = 0; i < filenamestr.Length; i++)
                                    {
                                        if (i == 0)
                                            filenamestr[i] = half + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                        else
                                            filenamestr[i] = half + "_" + i + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                    }
                                    objCom.TestLog("names : " + names.Length + " : " + Convert.ToString(names[0]));
                                    //var listDiff = filenamestr.Except(names);
                                    //var listDiff = filenamestr.Where(i => !names.Contains(i)); 
                                    string[] listDiff = filenamestr;
                                    int numIdx;
                                    for (int j = 0; j < names.Length; j++)
                                    {
                                        objCom.TestLog("j : " + j + " : " + names[j]);
                                        for (int i = 0; i < filenamestr.Length; i++)
                                        {
                                            objCom.TestLog("i : " + i + " : " + filenamestr[i]);
                                            if (filenamestr[i].Replace(ConfigurationManager.AppSettings["ImageURL"].ToString(), ConfigurationManager.AppSettings["PublishedImageURL"].ToString()) == names[j])
                                            {
                                                numIdx = Array.IndexOf(listDiff, filenamestr[i]);
                                                objCom.TestLog("numIdx : " + numIdx);
                                                List<string> tmp = new List<string>(listDiff);
                                                tmp.Remove(filenamestr[i]);
                                                //tmp.RemoveAt(numIdx);
                                                listDiff = tmp.ToArray();
                                                objCom.TestLog("inside loop : " + listDiff.Length + " : " + listDiff[0]);
                                            }
                                        }
                                    }
                                    for (int i = 0; i < listDiff.Length; i++)
                                    {
                                        str = listDiff[i];
                                        break;
                                    }
                                    objCom.TestLog("str : " + listDiff.Length + " : " + str + " : " + listDiff[0]);
                                    //foreach (string s in listDiff)
                                    //{
                                    //    str = s;
                                    //    break;
                                    //}
                                }
                                else
                                {
                                    str = Convert.ToString(Session["NextAuditId"]) + "_" + Convert.ToString(Session["question_id"]);
                                }
                            }
                            else
                            {
                                str = Convert.ToString(Session["NextAuditId"]) + "_" + Convert.ToString(Session["question_id"]);
                            }
                            string filename = string.Empty;
                            string filenametodb = string.Empty;
                            if (str.Contains("." + System.Drawing.Imaging.ImageFormat.Jpeg))
                            {
                                filename = str;
                                filenametodb = filename.Replace(ConfigurationManager.AppSettings["ImageURL"].ToString(), ConfigurationManager.AppSettings["PublishedImageURL"].ToString());
                            }
                            else
                            {
                                filename = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]) + "/" + str + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                                filenametodb = ConfigurationManager.AppSettings["PublishedImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]) + "/" + str + "." + System.Drawing.Imaging.ImageFormat.Jpeg;
                            }

                            string pathString = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/" + Convert.ToString(Session["NextAuditId"]);
                            if (!Directory.Exists(pathString))
                            {
                                Directory.CreateDirectory(pathString);
                            }
                            if (!string.IsNullOrEmpty(filename))
                            {
                                uplImage.PostedFile.SaveAs(filename);
                                if (String.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"])))
                                {
                                    dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = filenametodb;
                                    dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Server.MapPath(uplImage.FileName);
                                }
                                else
                                {
                                    for (int i = 0; i < names.Length; i++)
                                        imagetable.Rows.Add(new object[] { names[i] });
                                    if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) > 1)
                                    {
                                        if (imagetable.Rows.Count <= Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]))
                                        {
                                            dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]) + ";" + filenametodb;
                                            dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"]) + ";" + Server.MapPath(uplImage.FileName);
                                            //dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path" + Convert.ToString(imagetable.Rows.Count)] = Server.MapPath(uplImage.FileName);
                                        }
                                    }
                                    else if (Convert.ToInt32(dtData.Rows[Convert.ToInt32(Session["RowCount"])]["max_no_of_pics_allowed"]) == 1)
                                    {
                                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"] = filenametodb;
                                        dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"] = Server.MapPath(uplImage.FileName);
                                    }
                                }

                            }
                        }
                        Session["dtAnswer"] = dtAnswer;
                    }
                }


                rowcount = Convert.ToInt32(Session["RowCount"]);
                if (dtData.Rows.Count > rowcount)
                {
                    lblSection.Text = Convert.ToString(dtData.Rows[rowcount]["section_name"]);
                    string helptooltip = "What should be in place?" + Environment.NewLine + Convert.ToString(dtData.Rows[rowcount]["help_text"]) + Environment.NewLine + Environment.NewLine + "How to check?" + Environment.NewLine + Convert.ToString(dtData.Rows[rowcount]["how_to_check"]);
                    //imgHelp.ToolTip = Convert.ToString(dtData.Rows[rowcount]["help_text"]);
                    imgHelp.ToolTip = helptooltip;

                    lblQuestion.Text = Convert.ToInt32(dtData.Rows[rowcount]["question_display_sequence"]) + ". " + Convert.ToString(dtData.Rows[rowcount]["question"]);
                    SetNotApplicableVisibility(Convert.ToString(dtData.Rows[rowcount]["na_flag"]));
                    Session["RowCount"] = rowcount;
                    Session["question_id"] = Convert.ToInt32(dtData.Rows[rowcount]["question_id"]);
                    if (Convert.ToInt32(dtAnswer.Rows[rowcount]["RowCount"]) == Convert.ToInt32(Session["RowCount"]))
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["answer"])))
                        {
                            rdbAnswer.SelectedValue = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["answer"]);
                            txtComment.Text = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["remarks"]);
                            //uplImage.PostedFile.FileName = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]);
                            if (rdbAnswer.SelectedValue == "1")
                            {
                                //txtComment.Visible = true;
                                txtComment.Attributes["style"] = "width:80%; display:block;";
                                uplImage.Attributes["style"] = "width:50%; display:block;";
                                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", " visibleComment();", true);
                                if (!string.IsNullOrEmpty(Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"])))
                                {
                                    dtImageBind = new DataTable();
                                    dtImageBind.Columns.Add("Image_path");
                                    dtImageBind.Columns.Add("DBImage_path");
                                    string[] image_file_name = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["image_file_name"]).Split(';');
                                    string[] Image_path = Convert.ToString(dtAnswer.Rows[Convert.ToInt32(Session["RowCount"])]["Image_path"]).Split(';');
                                    DataRow dr = dtImageBind.NewRow();
                                    for (int i = 0; i < Convert.ToInt32(Session["max_no_of_pics_allowed"]); i++)
                                    {
                                        if (Image_path.Length > i)
                                        {
                                            if (!String.IsNullOrEmpty(Image_path[i]))
                                            {
                                                dr = dtImageBind.NewRow();
                                                dr["Image_path"] = Convert.ToString(Image_path[i]);
                                                dr["DBImage_path"] = Convert.ToString(image_file_name[i]);
                                                dtImageBind.Rows.Add(dr);
                                            }
                                        }
                                    }
                                    grdImages.DataSource = dtImageBind;
                                    grdImages.DataBind();
                                }
                                else
                                {
                                    grdImages.DataSource = null;
                                    grdImages.DataBind();
                                }
                                if (Convert.ToInt32(dtData.Rows[rowcount]["max_no_of_pics_allowed"]) > 1 && dtImageBind.Rows.Count < Convert.ToInt32(dtData.Rows[rowcount]["max_no_of_pics_allowed"]))
                                {
                                    imgAddImage.Visible = true;
                                    uplImage.Enabled = true;
                                    lblImageResult.Text = "";
                                }
                                else if (Convert.ToInt32(dtData.Rows[rowcount]["max_no_of_pics_allowed"]) == 1)
                                {
                                    imgAddImage.Visible = false;
                                    uplImage.Enabled = true;
                                    lblImageResult.Text = "";
                                }
                                else
                                {
                                    uplImage.Enabled = false;
                                    imgAddImage.Visible = false;
                                    lblImageResult.Text = "You have reached the maximum number of images. Please delete and image if you want to add different image.";
                                }
                            }
                            else
                            {
                                txtComment.Attributes["style"] = "width:80%; display:none;";
                                uplImage.Attributes["style"] = "width:50%; display:none;";
                                grdImages.DataSource = null;
                                grdImages.DataBind();
                            }
                        }
                        else
                        {
                            rdbAnswer.ClearSelection();
                            txtComment.Attributes["style"] = "width:80%; display:none;";
                            uplImage.Attributes["style"] = "width:50%; display:none;";
                            txtComment.Text = "";
                            grdImages.DataSource = null;
                            grdImages.DataBind();
                        }
                    }
                    else
                    {
                        rdbAnswer.ClearSelection();
                        txtComment.Attributes["style"] = "width:80%; display:none;";
                        uplImage.Attributes["style"] = "width:50%; display:none;";
                        txtComment.Text = "";
                        grdImages.DataSource = null;
                        grdImages.DataBind();
                    }

                    if (rowcount == 0)
                    {
                        btnSubmit.Visible = false;
                        btnNext.Visible = true;
                        btnPrevious.Visible = false;
                    }
                    else
                    //if (dtData.Rows.Count - rowcount == 1)
                    {
                        int retUnanswered = ValidateAnswers(dtAnswer);
                        if (retUnanswered == 0)
                        {
                            btnSubmit.Visible = true;
                        }
                        else
                        {
                            btnSubmit.Visible = false;
                        }
                        if (retUnanswered == 1 && dtData.Rows.Count - rowcount == 1)
                        {
                            btnSubmit.Visible = true;
                        }

                        btnPrevious.Visible = true;
                        if (dtData.Rows.Count - rowcount == 1)
                            btnNext.Visible = false;
                        else
                            btnNext.Visible = true;
                    }
                }
                ColorChange();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc :  It calls getFilterDataBLfrom DashboardBL class, it binds all the dropdown on screen and be default loaded the user's location which are stored in session.
        /// </summary>
        /// <param name="objDashBO"></param>
        private void LoadFilterDropDowns(DashboardBO objDashBO)
        {
            try
            {
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        //ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        //ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        //ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    DataTable dtLineProduct = new DataTable();
                    DataTable dtLocation = new DataTable();
                    objUserBO = new UsersBO();
                    objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                    objComBL = new CommonBL();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                    dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                    //if (dtLineProduct.Rows.Count > 0)
                    //{
                    //    ddlLineName.DataSource = dtLineProduct;
                    //    ddlLineName.DataTextField = "line_name";
                    //    ddlLineName.DataValueField = "line_id";
                    //    ddlLineName.DataBind();
                    //    //BindPart();
                    //}
                    //else
                    //{
                    //    ddlLineName.Items.Clear();
                    //    ddlLineName.DataSource = null;
                    //    ddlLineName.DataBind();
                    //}
                    if (!string.IsNullOrEmpty(Convert.ToString(ddlLocation.SelectedValue)))
                    {
                        objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                        DataRow drselect = (from DataRow dr in dtLocation.Rows
                                            where (int)dr["location_id"] == objUserBO.location_id
                                            select dr).FirstOrDefault();
                        txtNumber.Text = Convert.ToString(drselect["Shift_no"]);
                        //dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
                        if (dtLineProduct.Rows.Count > 0)
                        {
                            ddlLineName.DataSource = dtLineProduct;
                            ddlLineName.DataTextField = "line_name";
                            ddlLineName.DataValueField = "line_id";
                            ddlLineName.DataBind();
                            Session["dtLine"] = dtLineProduct;
                        }
                        else
                        {
                            ddlLineName.Items.Clear();
                            ddlLineName.DataSource = null;
                            ddlLineName.DataBind();
                        }
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                    //BindPart();
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 09 Oct 2017
        /// Desc : On click of clear button question panel, question number panel are not visible and all the dropdown values are reset by calling LoadFilterDropDowns method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                LoadFilterDropDowns(objDashBO);
                pnlQuestion.Visible = false;
                pnlQNumbers.Visible = false;
                if (Convert.ToString(Session["GlobalAdminFlag"]) == "Y")
                {
                    divdatetime.Visible = true;
                    txtNumber.Enabled = true;
                    txtDate.Text = String.Format("{0:dd-MM-yyyy}", DateTime.Now);
                    txtTime.Text = "00:00";
                }
                else
                {
                    txtDate.Text = String.Format("{0:dd-MM-yyyy}", DateTime.Now);
                    txtTime.Text = "00:00";
                    divdatetime.Visible = false;
                    txtNumber.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        #endregion
        
    }
}