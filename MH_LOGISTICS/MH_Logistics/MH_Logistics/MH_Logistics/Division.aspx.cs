﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MH_Logistics_BusinessAccess;
using MH_Logistics_BusinessLogic;
using MH_Logistics_BusinessObject;
using MH_Logistics_BusinessObjects;

namespace MH_Logistics
{
    public partial class Division : System.Web.UI.Page
    {
        #region Global Declaration
        CommonBL objComBL;
        CommonFunctions objCom = new CommonFunctions();
        DivisionBL objDivBL;
        DivisionBO objDivBO;
        #endregion

        #region Events
        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Jun 15,2018
        /// Desc: Check for login session and load the login page if session timeout, if not, load all the division details in a grid and the view panel with first row data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }

            int vwdivisionid;

            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
                {
                    try
                    {
                        GridBind();
                        DataTable dtGrid = new DataTable();
                        dtGrid = (DataTable)Session["dtDivision"];
                        vwdivisionid = Convert.ToInt32(dtGrid.Rows[0]["division_id"]);
                        DataRow drfirst = selectedRow(vwdivisionid);
                        LoadViewpanel(drfirst);
                    }
                    catch (Exception ex)
                    {
                        objCom.ErrorLog(ex);
                    }
                }
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : June 15,2018
        /// Desc: On click of pages in gridview, the selected page will be loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdDivision_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdDivision.PageIndex = e.NewPageIndex;
                grdDivision.DataSource = (DataTable)Session["dtDivision"];
                grdDivision.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Jun 20,2018
        /// Desc: On click of any row in gridview, the selected row details will be loaded in view panel by calling LoadViewpanel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Select(object sender, EventArgs e)
        {
            try
            {
                int vwdivisionid = Convert.ToInt32((sender as LinkButton).CommandArgument);
                DataRow drselect = selectedRow(vwdivisionid);
                LoadViewpanel(drselect);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc:On click of any row in gridview, the selected row details will be loaded in edit panel by calling LoadEditPanel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Edit(object sender, EventArgs e)
        {
            try
            {
                int vwdivisionid = Convert.ToInt32((sender as ImageButton).CommandArgument);
                DataRow drselect = selectedRow(vwdivisionid);
                LoadEditPanel(drselect);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Jun 20,2018
        /// Desc: On click of Add button, it will clear all the field from edit panel and display Add panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
                addpanl.Visible = true;
                if (string.IsNullOrEmpty(Convert.ToString(txtSection1.Text)) || string.IsNullOrEmpty(Convert.ToString(txtSection2.Text)) || string.IsNullOrEmpty(Convert.ToString(txtSection3.Text)) || string.IsNullOrEmpty(Convert.ToString(txtSection4.Text)) || string.IsNullOrEmpty(Convert.ToString(txtSection5.Text)) || string.IsNullOrEmpty(Convert.ToString(txtSection6.Text)))
                {
                    LoadSections();
                }
                viewpanel.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }


        }

        

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of Delete button, it will call the DeleteRoleBL with all the details selected row to delete from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete(object sender, EventArgs e)
        {
            try
            {
                objDivBO = new DivisionBO();
                objDivBL = new DivisionBL();
                int divisionid = Convert.ToInt32((sender as ImageButton).CommandArgument);
                objDivBO.division_id = divisionid;
                objDivBO = objDivBL.DeleteDivisionBL(objDivBO);

                if (objDivBO.error_code == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + objDivBO.error_msg + "');", true);
                    GridBind();
                    DataTable dtGrid = new DataTable();
                    dtGrid = (DataTable)Session["dtDivision"];
                    DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["division_id"]));
                    LoadViewpanel(drfirst);
                }
                else if (objDivBO.error_code == 100)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + objDivBO.error_msg + "');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('There is some error in deleting division, please try again.');", true);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of Save button, it will call the SaveRoleBL with all the details in edit panel to save to database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            DivisionBO Output;
            try
            {
                Output = new DivisionBO();
                objDivBO = new DivisionBO();
                objDivBL = new DivisionBL();
                if (!string.IsNullOrEmpty(Convert.ToString(Session["SelectedDivisionId"])))
                    objDivBO.division_id = Convert.ToInt32(Session["SelectedDivisionId"]);
                objDivBO.division_flag = Convert.ToString(txtflag.Text);
                objDivBO.division_name = Convert.ToString(txtname.Text);
                objDivBO.section1 = Convert.ToString(txtSection1.Text);
                objDivBO.section2 = Convert.ToString(txtSection2.Text);
                objDivBO.section3 = Convert.ToString(txtSection3.Text);
                objDivBO.section4 = Convert.ToString(txtSection4.Text);
                objDivBO.section5 = Convert.ToString(txtSection5.Text);
                objDivBO.section6 = Convert.ToString(txtSection6.Text);

                Output = objDivBL.SaveDivisionBL(objDivBO);

                if (Output.error_code == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + Output.error_msg + "');", true);
                    GridBind();
                    DataTable dtGrid = new DataTable();
                    dtGrid = (DataTable)Session["dtDivision"];
                    DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["division_id"]));
                    LoadViewpanel(drfirst);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('There is some error, please try again.');", true);
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods

        private void LoadEditPanel(DataRow drselect)
        {
            try
            {
                btnAdd_Click(null, null);
                Session["SelectedDivisionId"] = Convert.ToString(drselect["division_id"]);
                txtflag.Text = Convert.ToString(drselect["Division_flag"]);
                txtname.Text = Convert.ToString(drselect["Division_Name"]);
                txtSection1.Text = Convert.ToString(drselect["section1_name"]);
                txtSection2.Text = Convert.ToString(drselect["section2_name"]);
                txtSection3.Text = Convert.ToString(drselect["section3_name"]);
                txtSection4.Text = Convert.ToString(drselect["section4_name"]);
                txtSection5.Text = Convert.ToString(drselect["section5_name"]);
                txtSection6.Text = Convert.ToString(drselect["section6_name"]);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void LoadViewpanel(DataRow drselect)
        {
            try
            {
                addpanl.Visible = false;
                viewpanel.Visible = true;
                lblflag.Text = Convert.ToString(drselect["Division_flag"]);
                lblname.Text = Convert.ToString(drselect["Division_Name"]);
                lblSection1.Text = Convert.ToString(drselect["section1_name"]);
                lblSection2.Text = Convert.ToString(drselect["section2_name"]);
                lblSection3.Text = Convert.ToString(drselect["section3_name"]);
                lblSection4.Text = Convert.ToString(drselect["section4_name"]);
                lblSection5.Text = Convert.ToString(drselect["section5_name"]);
                lblSection6.Text = Convert.ToString(drselect["section6_name"]);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private DataRow selectedRow(int vwdivisionid)
        {
            DataTable dtGrid = (DataTable)Session["dtDivision"];
            DataRow drselect = (from DataRow dr in dtGrid.Rows
                                where (int)dr["division_id"] == vwdivisionid
                                select dr).FirstOrDefault();

            return drselect;
        }

        private void GridBind()
        {
            DataTable dtDivision;
            try
            {
                dtDivision = new DataTable();
                objDivBL = new DivisionBL();
                dtDivision = objDivBL.GetDivisionBL();
                if (dtDivision.Rows.Count > 0)
                {
                    Session["dtDivision"] = dtDivision;
                    grdDivision.DataSource = dtDivision;
                }
                else
                {
                    grdDivision.DataSource = null;
                }
                grdDivision.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Jun 20,2018
        /// Desc : Clears all the session related to the page and fields
        /// </summary>
        private void Clear()
        {
            txtname.Text = "";
            txtflag.Text = "";
            Session["SelectedDivisionId"] = "";
        }

        private void LoadSections()
        {
            DataTable dtSection;
            try
            {
                dtSection = new DataTable();
                objDivBL = new DivisionBL();
                dtSection = objDivBL.GetDefaultSectionsBL();
                if (dtSection.Rows.Count > 0)
                {
                    txtSection1.Text = Convert.ToString(dtSection.Rows[0]["section_name"]);
                    txtSection2.Text = Convert.ToString(dtSection.Rows[1]["section_name"]);
                    txtSection3.Text = Convert.ToString(dtSection.Rows[2]["section_name"]);
                    txtSection4.Text = Convert.ToString(dtSection.Rows[3]["section_name"]);
                    txtSection5.Text = Convert.ToString(dtSection.Rows[4]["section_name"]);
                    txtSection6.Text = Convert.ToString(dtSection.Rows[5]["section_name"]);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        #endregion
    }
}