﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MH_Logistics_BusinessLogic;
using MH_Logistics_BusinessObject;

namespace MH_Logistics
{

    public partial class Users : System.Web.UI.Page
    {
        #region GlobalDeclaration
        UsersBO objUserBO;
        CommonBL objComBL;
        DataSet dsDropDownData;
        UsersBL objUsersBL;
        CommonFunctions objCom = new CommonFunctions();
        LoginAuthenticateBL objAuth;
        #endregion

        #region Events
        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: Check for login session and load the login page if session timeout, if not, load all the user details in a grid and the view panel with first row data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            int vwuserid;
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                try
                {
                    GridBind(Convert.ToInt32(Session["UserId"]));
                    DataTable dtGrid = new DataTable();
                    dtGrid = (DataTable)Session["dtGrid"];
                    vwuserid = Convert.ToInt32(dtGrid.Rows[0]["user_id"]);
                    DataRow drfirst = selectedRow(vwuserid);
                    LoadViewpanel(drfirst);
                    txtpwd.Attributes["type"] = "password";
                    txtrepwd.Attributes["type"] = "password";
                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }

            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of Add button, it will clear all the field from edit panel and display Add panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                clear();
                addpanl.Visible = true;
                viewpanel.Visible = false;
                txtpwd.Enabled = true;
                txtrepwd.Enabled = true;
                imgbtnPassword.Visible = false;
                ViewState["PasswordChangeFlag"] = 0;
                ViewState["EditFlag"] = 0;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }


        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of Save button, it will call the SaveUserDetailsBL with all the details in edit panel to save to database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            int Output;

            string strdisplayflag = string.Empty;
            try
            {
                objAuth = new LoginAuthenticateBL();
                objUserBO = new UsersBO();
                objUsersBL = new UsersBL();
                if (chkPrivacy.Checked)
                {
                    strdisplayflag = "Y";
                }
                else
                {
                    strdisplayflag = "N";
                }
                objUserBO.UserName = Convert.ToString(txtUserID.Text);
                objUserBO.UserRole = Convert.ToString(ddlRole.SelectedItem);
                objUserBO.FirstName = Convert.ToString(txtFirstName.Text);
                objUserBO.LastName = Convert.ToString(txtLastName.Text);
                if (Convert.ToInt32(ViewState["EditFlag"]) == 1 && Convert.ToInt32(ViewState["PasswordChangeFlag"]) == 1)
                {
                    objUserBO.UserPassword = Convert.ToString(txtpwd.Text);
                }
                if (Convert.ToInt32(ViewState["EditFlag"]) == 0 && Convert.ToInt32(ViewState["PasswordChangeFlag"]) == 0)
                {
                    objUserBO.UserPassword = Convert.ToString(txtpwd.Text);
                }
                if (Convert.ToInt32(ViewState["PasswordChangeFlag"]) == 0)
                {
                    if (Convert.ToInt32(ViewState["EditFlag"]) == 0)
                    {
                        objUserBO.Password = Convert.ToString(objAuth.Encrypt(Convert.ToString(objAuth.MD5Encrypt(Convert.ToString(txtpwd.Text)))));
                    }
                        
                    else
                        objUserBO.Password = Convert.ToString(objAuth.Encrypt(Convert.ToString(txtpwd.Text)));
                }
                else
                {
                    objUserBO.Password = Convert.ToString(objAuth.Encrypt(Convert.ToString(objAuth.MD5Encrypt(Convert.ToString(txtpwd.Text)))));
                }
                
                objUserBO.PrivacySetting = Convert.ToString(strdisplayflag);
                objUserBO.MailId = Convert.ToString(txtEmailId.Text);
                objUserBO.DefaultLocation = Convert.ToInt32(ddlLocation.SelectedValue);
                if (!string.IsNullOrEmpty(Convert.ToString(Session["SelectedUserId"])))
                    objUserBO.UserId = Convert.ToInt32(Session["SelectedUserId"]);
                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;

                if (DateTime.TryParseExact(txtStartDate.Text, UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string frmdate = parsedDate.ToString(DBPattern);
                    //objUserBO.StartDate = Convert.ToDateTime(frmdate);
                    objUserBO.StartDate = DateTime.ParseExact(frmdate, "MM-dd-yyyy", null);
                }
                else
                {
                    objUserBO.StartDate = Convert.ToDateTime(txtStartDate.Text);
                }
                if (!string.IsNullOrEmpty(txtEndDate.Text))
                {

                    if (DateTime.TryParseExact(txtEndDate.Text, UIpattern, null, DateTimeStyles.None, out parsedDate))
                    {
                        string frmdate = parsedDate.ToString(DBPattern); // one extra day is adding for existing date. sql 
                        //objUserBO.EndDate = Convert.ToDateTime(frmdate);
                        objUserBO.EndDate = DateTime.ParseExact(frmdate, "MM-dd-yyyy", null);
                    }
                    else
                    {
                        objUserBO.EndDate = Convert.ToDateTime(txtEndDate.Text);
                    }
                }
                if (!string.IsNullOrEmpty(rdbAdmin.SelectedValue))
                {
                    if (Convert.ToString(rdbAdmin.SelectedIndex) == "0")
                    {
                        objUserBO.SiteAdminFlag = "Y";
                        objUserBO.GlobalAdminFlag = "N";
                    }
                    else if (Convert.ToString(rdbAdmin.SelectedIndex) == "1")
                    {
                        objUserBO.GlobalAdminFlag = "Y";
                        objUserBO.SiteAdminFlag = "N";
                    }
                    else
                    {
                        objUserBO.SiteAdminFlag = "N";
                        objUserBO.GlobalAdminFlag = "N";
                    }


                }
                objUserBO.TypeOfEdit = "Insert";
                int flag = 0;
                if (!string.IsNullOrEmpty(txtEndDate.Text))
                {
                    if (objUserBO.StartDate.Date >= objUserBO.EndDate.Date)
                    {
                        flag = 1;
                    }
                }
                if (flag == 0)
                {
                    objUserBO = objUsersBL.SaveUserDetailsBL(objUserBO);

                    if (objUserBO.Err_Code == 0)
                    {
                        if (objUserBO.Err_Message.Contains("Successfully"))
                        {
                            SendMailToUser(objUserBO);
                            if (objUserBO.UserId == 0)
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('User is added successfully and mail has been sent.');", true);
                            else
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('User details are modified successfully and mail has been sent.');", true);
                            GridBind(Convert.ToInt32(Session["UserId"]));
                            DataTable dtGrid = new DataTable();
                            dtGrid = (DataTable)Session["dtGrid"];
                            DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["user_id"]));
                            LoadViewpanel(drfirst);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('There is some error, please check all the values and try again.');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('"+objUserBO.Err_Message+"');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('End date should be greater than start date.');", true);
                }


            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of any row in gridview, the selected row details will be loaded in view panel by calling LoadViewpanel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Select(object sender, EventArgs e)
        {
            try
            {
                int userid = Convert.ToInt32((sender as LinkButton).CommandArgument);
                DataRow drselect = selectedRow(userid);
                LoadViewpanel(drselect);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }


        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc:On click of any row in gridview, the selected row details will be loaded in edit panel by calling LoadEditPanel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Edit(object sender, EventArgs e)
        {
            try
            {
                int userid = Convert.ToInt32((sender as ImageButton).CommandArgument);
                DataRow drselect = selectedRow(userid);
                LoadEditPanel(drselect);
                txtpwd.Enabled = false;
                txtrepwd.Enabled = false;
                imgbtnPassword.Visible = true;
                ViewState["EditFlag"] = 1; 
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of pages in gridview, the selected page will be loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdUsers.PageIndex = e.NewPageIndex;
                grdUsers.DataSource = (DataTable)Session["FilteredData"];
                grdUsers.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On selection of location dropdown in header of gridview, the data will be loaded according to selection and saved the data in Session["FilteredData"]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLocation_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();
                if (Session["dtGrid"] != null)
                {
                    dtData = (DataTable)Session["dtGrid"];
                }
                DropDownList ddlCountry = (DropDownList)sender;
                Session["ddlSelectedLocation_grid"] = Convert.ToString(ddlCountry.SelectedValue);
                Session["ddlSelectedLName_grid"] = "All";
                Session["ddlSelectedFName_grid"] = "All";
                Session["ddlSelectedUName_grid"] = "All";
                Session["ddlSelectedType_grid"] = "All";
                Session["ddlSelectedRole_grid"] = "All";
                if (Convert.ToString(ddlCountry.SelectedValue) == "0")
                {
                    Session["FilteredData"] = dtData;
                    Session["FilteredLocation"] = dtData;
                    grdUsers.DataSource = dtData;
                    grdUsers.DataBind();
                }
                else
                {
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("location_name") == Convert.ToString(ddlCountry.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    Session["FilteredLocation"] = dtFilterData;
                    grdUsers.DataSource = dtFilterData;
                    grdUsers.DataBind();
                }
                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["user_id"]));
                LoadViewpanel(drfirst);


            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc:On selection of last name dropdown in header of gridview, the data will be loaded according to selection and saved the data in Session["FilteredData"]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLastName_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();
                if (Session["FilteredLocation"] != null)
                {
                    dtData = (DataTable)Session["FilteredLocation"];
                }
                else if (Session["dtGrid"] != null)
                {
                    dtData = (DataTable)Session["dtGrid"];
                }
                DropDownList ddlCountry = (DropDownList)sender;
                Session["ddlSelectedLName_grid"] = Convert.ToString(ddlCountry.SelectedValue);
                Session["ddlSelectedFName_grid"] = "All";
                Session["ddlSelectedUName_grid"] = "All";
                Session["ddlSelectedType_grid"] = "All";
                Session["ddlSelectedRole_grid"] = "All";
                if (Convert.ToString(ddlCountry.SelectedValue) == "0")
                {
                    Session["FilteredData"] = dtData;
                    Session["FilteredLName"] = dtData;
                    grdUsers.DataSource = dtData;
                    grdUsers.DataBind();
                }
                else
                {
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("emp_last_name") == Convert.ToString(ddlCountry.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    Session["FilteredLName"] = dtFilterData;
                    grdUsers.DataSource = dtFilterData;
                    grdUsers.DataBind();
                }
                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["user_id"]));
                LoadViewpanel(drfirst);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc:On selection of first name dropdown in header of gridview, the data will be loaded according to selection and saved the data in Session["FilteredData"]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlFirstName_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();
                if (Session["FilteredLName"] != null)
                {
                    dtData = (DataTable)Session["FilteredLName"];
                }
                else if (Session["FilteredLocation"] != null)
                {
                    dtData = (DataTable)Session["FilteredLocation"];
                }
                else if (Session["dtGrid"] != null)
                {
                    dtData = (DataTable)Session["dtGrid"];
                }
                DropDownList ddlCountry = (DropDownList)sender;
                Session["ddlSelectedFName_grid"] = Convert.ToString(ddlCountry.SelectedValue);
                Session["ddlSelectedUName_grid"] = "All";
                Session["ddlSelectedType_grid"] = "All";
                Session["ddlSelectedRole_grid"] = "All";
                if (Convert.ToString(ddlCountry.SelectedValue) == "0")
                {
                    Session["FilteredData"] = dtData;
                    Session["FilteredFName"] = dtData;
                    grdUsers.DataSource = dtData;
                    grdUsers.DataBind();
                }
                else
                {
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("emp_first_name") == Convert.ToString(ddlCountry.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    Session["FilteredFName"] = dtFilterData;
                    grdUsers.DataSource = dtFilterData;
                    grdUsers.DataBind();
                }
                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["user_id"]));
                LoadViewpanel(drfirst);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc:On selection of user name dropdown in header of gridview, the data will be loaded according to selection and saved the data in Session["FilteredData"]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlUserName_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();
                if (Session["FilteredFName"] != null)
                {
                    dtData = (DataTable)Session["FilteredFName"];
                }
                else if (Session["FilteredLName"] != null)
                {
                    dtData = (DataTable)Session["FilteredLName"];
                }
                else if (Session["FilteredLocation"] != null)
                {
                    dtData = (DataTable)Session["FilteredLocation"];
                }
                else if (Session["dtGrid"] != null)
                {
                    dtData = (DataTable)Session["dtGrid"];
                }
                DropDownList ddlCountry = (DropDownList)sender;
                Session["ddlSelectedUName_grid"] = Convert.ToString(ddlCountry.SelectedValue);
                Session["ddlSelectedType_grid"] = "All";
                Session["ddlSelectedRole_grid"] = "All";
                if (Convert.ToString(ddlCountry.SelectedValue) == "0")
                {
                    Session["FilteredData"] = dtData;
                    Session["FilteredUName"] = dtData;
                    grdUsers.DataSource = dtData;
                    grdUsers.DataBind();
                }
                else
                {
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("username") == Convert.ToString(ddlCountry.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    Session["FilteredUName"] = dtFilterData;
                    grdUsers.DataSource = dtFilterData;
                    grdUsers.DataBind();
                }
                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["user_id"]));
                LoadViewpanel(drfirst);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc:On selection of admin type dropdown in header of gridview, the data will be loaded according to selection and saved the data in Session["FilteredData"]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlType_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();
                if (Session["FilteredUName"] != null)
                {
                    dtData = (DataTable)Session["FilteredUName"];
                }
                else if (Session["FilteredFName"] != null)
                {
                    dtData = (DataTable)Session["FilteredFName"];
                }
                else if (Session["FilteredLName"] != null)
                {
                    dtData = (DataTable)Session["FilteredLName"];
                }
                else if (Session["FilteredLocation"] != null)
                {
                    dtData = (DataTable)Session["FilteredLocation"];
                }
                else if (Session["dtGrid"] != null)
                {
                    dtData = (DataTable)Session["dtGrid"];
                }
                DropDownList ddlCountry = (DropDownList)sender;
                Session["ddlSelectedType_grid"] = Convert.ToString(ddlCountry.SelectedValue);
                Session["ddlSelectedRole_grid"] = "All";
                if (Convert.ToString(ddlCountry.SelectedValue) == "0")
                {
                    Session["FilteredData"] = dtData;
                    Session["FilteredType"] = dtData;
                    grdUsers.DataSource = dtData;
                    grdUsers.DataBind();
                }
                else
                {
                    //string admin_flag=(Convert.ToString(ddlCountry.SelectedValue)=="Global")?"Y":Convert.ToString(ddlCountry.SelectedValue)=="Local"?"N":"";
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("admin_flag") == Convert.ToString(ddlCountry.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    Session["FilteredType"] = dtFilterData;
                    grdUsers.DataSource = dtFilterData;
                    grdUsers.DataBind();
                }
                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["user_id"]));
                LoadViewpanel(drfirst);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc:On selection of role dropdown in header of gridview, the data will be loaded according to selection and saved the data in Session["FilteredData"]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlRole_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();
                if (Session["FilteredType"] != null)
                {
                    dtData = (DataTable)Session["FilteredType"];
                }
                else if (Session["FilteredUName"] != null)
                {
                    dtData = (DataTable)Session["FilteredUName"];
                }
                else if (Session["FilteredFName"] != null)
                {
                    dtData = (DataTable)Session["FilteredFName"];
                }
                else if (Session["FilteredLName"] != null)
                {
                    dtData = (DataTable)Session["FilteredLName"];
                }
                else if (Session["FilteredLocation"] != null)
                {
                    dtData = (DataTable)Session["FilteredLocation"];
                }
                else if (Session["dtGrid"] != null)
                {
                    dtData = (DataTable)Session["dtGrid"];
                }
                DropDownList ddlCountry = (DropDownList)sender;
                Session["ddlSelectedRole_grid"] = Convert.ToString(ddlCountry.SelectedValue);
                if (Convert.ToString(ddlCountry.SelectedValue) == "0")
                {
                    Session["FilteredData"] = dtData;
                    Session["FilteredRole"] = dtData;
                    grdUsers.DataSource = dtData;
                    grdUsers.DataBind();
                }
                else
                {
                    //string admin_flag=(Convert.ToString(ddlCountry.SelectedValue)=="Global")?"Y":Convert.ToString(ddlCountry.SelectedValue)=="Local"?"N":"";
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("role") == Convert.ToString(ddlCountry.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    Session["FilteredRole"] = dtFilterData;
                    grdUsers.DataSource = dtFilterData;
                    grdUsers.DataBind();
                }

                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["user_id"]));
                LoadViewpanel(drfirst);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : all the drop downs data in header will be loaded according to the gridview data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataTable dtData;
            DropDownList ddlFilter;
            DataView view;
            DataTable distinctValues;
            try
            {

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    ddlFilter = e.Row.FindControl("ddlLocation_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["dtOriginalGrid"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalGrid"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "location_name");
                        DataView dv = distinctValues.DefaultView;
                        dv.Sort = "location_name asc";
                        distinctValues = dv.ToTable();
                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "location_name";
                        ddlFilter.DataValueField = "location_name";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedLocation_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedLocation_grid"]);
                        }
                    }
                    ddlFilter = e.Row.FindControl("ddlLastName_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["FilteredLocation"] != null)
                        {
                            dtData = (DataTable)Session["FilteredLocation"];
                        }
                        else if (Session["dtOriginalGrid"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalGrid"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "emp_last_name");
                        DataView dv = distinctValues.DefaultView;
                        dv.Sort = "emp_last_name asc";
                        distinctValues = dv.ToTable();
                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "emp_last_name";
                        ddlFilter.DataValueField = "emp_last_name";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedLName_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedLName_grid"]);
                        }
                        if (ddlFilter.SelectedValue == "0" || ddlFilter.SelectedValue == "")
                        {
                            Session["FilteredLName"] = Session["FilteredLocation"];
                        }
                    }
                    ddlFilter = e.Row.FindControl("ddlFirstName_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["FilteredLName"] != null)
                        {
                            dtData = (DataTable)Session["FilteredLName"];
                        }
                        else if (Session["FilteredLocation"] != null)
                        {
                            dtData = (DataTable)Session["FilteredLocation"];
                        }
                        else if (Session["dtOriginalGrid"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalGrid"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "emp_first_name");
                        DataView dv = distinctValues.DefaultView;
                        dv.Sort = "emp_first_name asc";
                        distinctValues = dv.ToTable();
                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "emp_first_name";
                        ddlFilter.DataValueField = "emp_first_name";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedFName_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedFName_grid"]);
                        }
                        if (ddlFilter.SelectedValue == "0" || ddlFilter.SelectedValue == "")
                        {
                            Session["FilteredFName"] = Session["FilteredLocation"];
                        }
                    }

                    ddlFilter = e.Row.FindControl("ddlUserName_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["FilteredFName"] != null)
                        {
                            dtData = (DataTable)Session["FilteredFName"];
                        }
                        else if (Session["FilteredLName"] != null)
                        {
                            dtData = (DataTable)Session["FilteredLName"];
                        }
                        else if (Session["FilteredLocation"] != null)
                        {
                            dtData = (DataTable)Session["FilteredLocation"];
                        }
                        else if (Session["dtOriginalGrid"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalGrid"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "username");
                        DataView dv = distinctValues.DefaultView;
                        dv.Sort = "username asc";
                        distinctValues = dv.ToTable();
                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "username";
                        ddlFilter.DataValueField = "username";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedUName_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedUName_grid"]);
                        }
                        if (ddlFilter.SelectedValue == "0" || ddlFilter.SelectedValue == "")
                        {
                            Session["FilteredUName"] = Session["FilteredLocation"];
                        }
                    }
                    ddlFilter = e.Row.FindControl("ddlType_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["FilteredUName"] != null)
                        {
                            dtData = (DataTable)Session["FilteredUName"];
                        }
                        else if (Session["FilteredFName"] != null)
                        {
                            dtData = (DataTable)Session["FilteredFName"];
                        }
                        else if (Session["FilteredLName"] != null)
                        {
                            dtData = (DataTable)Session["FilteredLName"];
                        }
                        else if (Session["FilteredLocation"] != null)
                        {
                            dtData = (DataTable)Session["FilteredLocation"];
                        }
                        else if (Session["dtOriginalGrid"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalGrid"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "admin_flag");
                        DataView dv = distinctValues.DefaultView;
                        dv.Sort = "admin_flag asc";
                        distinctValues = dv.ToTable();
                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "admin_flag";
                        ddlFilter.DataValueField = "admin_flag";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedType_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedType_grid"]);
                        }
                    }
                    ddlFilter = e.Row.FindControl("ddlRole_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["FilteredLName"] != null)
                        {
                            dtData = (DataTable)Session["FilteredLName"];
                        }
                        else if (Session["FilteredLocation"] != null)
                        {
                            dtData = (DataTable)Session["FilteredLocation"];
                        }
                        else if (Session["dtOriginalGrid"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalGrid"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "role");
                        DataView dv = distinctValues.DefaultView;
                        dv.Sort = "role asc";
                        distinctValues = dv.ToTable();
                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "role";
                        ddlFilter.DataValueField = "role";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedRole_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedRole_grid"]);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Clears all the session related to the page and fields
        /// </summary>
        public void clear()
        {
            try
            {


                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        if (Convert.ToString(Session["SiteAdminFlag"]) == "Y" && Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                            ddlLocation.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlLocation.Enabled = true;
                    }
                    else
                    {
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[4].Rows.Count > 0)
                    {
                        ddlRole.DataSource = dsDropDownData.Tables[4];
                        ddlRole.DataTextField = "rolename";
                        ddlRole.DataValueField = "role_id";
                        ddlRole.DataBind();
                    }
                    else
                    {
                        ddlRole.DataSource = null;
                        ddlRole.DataBind();
                    }
                }
                txtUserID.Text = "";
                txtFirstName.Text = "";
                txtLastName.Text = "";
                txtEmailId.Text = "";
                txtEndDate.Text = "";
                txtpwd.Text = "";
                txtrepwd.Text = "";
                txtStartDate.Text = "";
                rdbAdmin.SelectedIndex = 2;
                chkPrivacy.Checked = false;
                Session["SelectedUserId"] = "";
                if (Convert.ToString(Session["GlobalAdminFlag"]) == "Y")
                {
                    foreach (ListItem item in rdbAdmin.Items)
                    {
                        if (item.Text == "Global Admin")
                        {
                            item.Enabled = true;
                            item.Attributes.Add("style", "color:black;");
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Calling GetUserDetailsBL function to fetch datable and load gridview with the datatable.
        /// </summary>
        /// <param name="UserId"></param>
        private void GridBind(int UserId)
        {
            DataTable dtUsers;
            try
            {
                dtUsers = new DataTable();
                objUserBO = new UsersBO();
                objUsersBL = new UsersBL();
                objUserBO.UserId = UserId;
                dtUsers = objUsersBL.GetUserDetailsBL(objUserBO);
                if (dtUsers.Rows.Count > 0)
                {
                    Session["dtGrid"] = dtUsers;
                    grdUsers.DataSource = dtUsers;

                    Session["dtOriginalGrid"] = dtUsers;
                    Session["ddlSelectedLName_grid"] = null;
                    Session["ddlSelectedFName_grid"] = null;
                    Session["ddlSelectedLocation_grid"] = null;
                    Session["ddlSelectedUName_grid"] = null;
                    Session["ddlSelectedType_grid"] = null;
                    Session["ddlSelectedRole_grid"] = null;
                    Session["FilteredData"] = dtUsers;
                    Session["FilteredLName"] = null;
                    Session["FilteredFName"] = null;
                    Session["FilteredLocation"] = null;
                    Session["FilteredUName"] = null;
                    Session["FilteredType"] = null;
                    Session["FilteredRole"] = null;
                }
                else
                {
                    grdUsers.DataSource = null;
                }
                grdUsers.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Display the add panel and load all the information with respective controls.
        /// </summary>
        /// <param name="drselect">Selected Row from the dridview</param>
        private void LoadEditPanel(DataRow drselect)
        {
            objAuth = new LoginAuthenticateBL();
            try
            {
                ViewState["EncryptedUserPassword"] = objAuth.Decrypt(Convert.ToString(drselect["passwd"]));
                btnAdd_Click(null, null);
                Session["SelectedUserId"] = Convert.ToString(drselect["user_id"]);
                txtUserID.Text = Convert.ToString(drselect["username"]);
                txtFirstName.Text = Convert.ToString(drselect["emp_first_name"]);
                txtLastName.Text = Convert.ToString(drselect["emp_last_name"]);
                txtpwd.Text = Convert.ToString(ViewState["EncryptedUserPassword"]);
                txtrepwd.Text = Convert.ToString(txtpwd.Text);
                txtEmailId.Text = Convert.ToString(drselect["email_id"]);
                ddlLocation.SelectedValue = Convert.ToString(drselect["location_id"]);
                ddlRole.SelectedValue = Convert.ToString(drselect["role_id"]);
                txtStartDate.Text = Convert.ToString(drselect["start_date"]);
                txtEndDate.Text = Convert.ToString(drselect["end_date"]);

                if (drselect["display_name_flag"].ToString().Equals("Y"))
                    chkPrivacy.Checked = true;
                else
                    chkPrivacy.Checked = false;
                if (drselect["global_admin_flag"].ToString().Equals("Y"))
                    rdbAdmin.SelectedIndex = 1;
                else if (drselect["site_admin_flag"].ToString().Equals("Y"))
                    rdbAdmin.SelectedIndex = 0;
                else
                    rdbAdmin.SelectedIndex = 2;
                if (Convert.ToString(Session["GlobalAdminFlag"]) == "Y")
                {
                    foreach (ListItem item in rdbAdmin.Items)
                    {
                        if (item.Text == "Global Admin")
                        {
                            item.Enabled = true;
                            item.Attributes.Add("style", "color:black;");
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }


        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : To fetch the complete row details with userid
        /// </summary>
        /// <param name="userid">Selected Row User id</param>
        /// <returns>Selected Row details</returns>
        private DataRow selectedRow(int userid)
        {
            DataTable dtGrid = (DataTable)Session["dtGrid"];
            DataRow drselect = (from DataRow dr in dtGrid.Rows
                                where (int)dr["user_id"] == userid
                                select dr).FirstOrDefault();
            return drselect;
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Display the view panel and load all the information with respective controls.
        /// </summary>
        /// <param name="drselect">Selected Row from the dridview</param>
        public void LoadViewpanel(DataRow drselect)
        {
            try
            {
                addpanl.Visible = false;
                viewpanel.Visible = true;
                Session["SelectedUserId"] = Convert.ToString(drselect["user_id"]);
                lblUserName.Text = Convert.ToString(drselect["username"]);

                lblFirstName.Text = Convert.ToString(drselect["emp_first_name"]);
                lblLastName.Text = Convert.ToString(drselect["emp_last_name"]);
                lblEmail.Text = Convert.ToString(drselect["email_id"]);
                lblLocation.Text = Convert.ToString(drselect["location_name"]);
                lblRole.Text = Convert.ToString(drselect["role"]);

                //DateTime dt = DateTime.ParseExact(Convert.ToString(drselect["start_date"]), "MM/dd/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);

                //string s = Convert.ToDateTime(drselect["end_date"]).ToString("dd-MM-yyyy", CultureInfo.InvariantCulture);
                lblStartDate.Text = Convert.ToDateTime(drselect["start_date"]).ToString("dd-MM-yyyy", CultureInfo.InvariantCulture);
                if (!string.IsNullOrEmpty(Convert.ToString(drselect["end_date"])))
                    lblEndDate.Text = Convert.ToDateTime(drselect["end_date"]).ToString("dd-MM-yyyy", CultureInfo.InvariantCulture);
                else
                    lblEndDate.Text = "";
                if (drselect["display_name_flag"].ToString().Equals("Y"))
                    chkPrivSett.Checked = true;
                else
                    chkPrivSett.Checked = false;
                if (drselect["global_admin_flag"].ToString().Equals("Y"))
                    rdbAdminView.SelectedIndex = 1;
                else if (drselect["site_admin_flag"].ToString().Equals("Y"))
                    rdbAdminView.SelectedIndex = 0;
                else
                    rdbAdminView.SelectedIndex = 2;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : This function will store all the desired information to EmailDetails and call the SendMail function in CommonFunction class to send mail.
        /// </summary>
        /// <param name="objUserBO"></param>
        private void SendMailToUser(UsersBO objUserBO)
        {
            EmailDetails objEmail = new EmailDetails();
            string strURL = string.Empty;
            try
            {
                strURL = Convert.ToString(ConfigurationManager.AppSettings["appURL"]);
                if (objUserBO.UserId == 0)
                {
                    objEmail.subject = "Account Created in LPA Logistics";
                    objEmail.body = "Your account has been created in LPA Logistics. <br/><br/>Please log on to " + strURL + " <br/> UserName : " + objUserBO.UserName + " and Password : " + objUserBO.UserPassword;
                }
                else
                {
                    objEmail.subject = "Account Modified in LPA Logistics";
                    if (string.IsNullOrEmpty( objUserBO.UserPassword))
                    {
                        objEmail.body = "Your account has been modified in LPA Logistics. <br/><br/>Please log on to " + strURL + " <br/> UserName : " + objUserBO.UserName + ".";
                    }
                    else
                    {
                        objEmail.body = "Your account has been modified in LPA Logistics. <br/><br/>Please log on to " + strURL + " <br/> UserName : " + objUserBO.UserName + " and Password : " + objUserBO.UserPassword;
                    }
                }
                objEmail.toMailId = objUserBO.MailId;
                objCom.SendMail(objEmail);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        #endregion

        protected void imgbtnPassword_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(ViewState["PasswordChangeFlag"]) == 0)
                {
                    ViewState["PasswordChangeFlag"] = 1;
                    txtpwd.Enabled = true;
                    txtrepwd.Enabled = true;
                    txtpwd.Text = "";
                    txtrepwd.Text = "";
                    imgbtnPassword.ToolTip = "Cancel";
                }
                else
                {
                    ViewState["PasswordChangeFlag"] = 0;
                    txtpwd.Enabled = false;
                    txtrepwd.Enabled = false;
                    txtpwd.Text = Convert.ToString(ViewState["EncryptedUserPassword"]);
                    txtrepwd.Text = Convert.ToString(ViewState["EncryptedUserPassword"]);
                    imgbtnPassword.ToolTip = "Reset Password";
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

    }
}