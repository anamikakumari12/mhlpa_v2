﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using MH_Logistics_BusinessLogic;
using MH_Logistics_BusinessObject;

namespace MH_Logistics
{
    public partial class CreatePlan : System.Web.UI.Page
    {
        #region Global Declaration
        UsersBO objUserBO;
        CommonBL objComBL;
        DataSet dsDropDownData;
        PlanBO objPlanBO;
        CommonFunctions objCom = new CommonFunctions();
        PlanBL objPlanBL;
        int j = 0;
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
                        GetMasterDropdown();
                    
                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
            }
        }

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                if (dtCountry.Rows.Count > 0)
                {
                    ddlCountry.DataSource = dtCountry;
                    ddlCountry.DataTextField = "country_name";
                    ddlCountry.DataValueField = "country_id";
                    ddlCountry.DataBind();
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }

                dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlLocation.DataSource = dtLocation;
                    ddlLocation.DataTextField = "location_name";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    Session["dtLocation"] = dtLocation;
                    ddlLocation_SelectedIndexChanged(null, null);
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlLocation.DataSource = dtLocation;
                    ddlLocation.DataTextField = "location_name";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    Session["dtLocation"] = dtLocation;
                    ddlLocation_SelectedIndexChanged(null, null);
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLocation = new DataTable();
            DataTable dtLine = new DataTable();
            DataTable dtPart = new DataTable();
            DataTable dtUsers = new DataTable();
            try
            {
                j = 0;
                objUserBO = new UsersBO();
                objComBL = new CommonBL();
                Session["SelectedLocation"] = Convert.ToString(ddlLocation.SelectedValue);
                dtLocation = (DataTable)Session["dtLocation"];
                objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                BindData();
                if (dtLine.Rows.Count > 0)
                {

                    //DataTable ds = new DataTable();
                    //ds = null;
                    //CreateplanGrid.DataSource = ds;
                    //CreateplanGrid.DataBind();
                    //for (int i = CreateplanGrid.Rows.Count - 1; i >= 0; --i)
                    //{
                    //    CreateplanGrid.Rows.dele.RemoveAt(rowToRemove[i]);
                    //}
                    CreateplanGrid.DataSource = dtLine;
                    CreateplanGrid.DataBind();
                    Session["dtLine"] = dtLine;
                }
                else
                {
                    CreateplanGrid.DataSource = null;
                    CreateplanGrid.DataBind();
                }

                //OnRowDataBound(null, null);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        
        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataTable dtEmployeeusers = new DataTable();
            DataTable dtUsers = new DataTable();
            int currentYear = Convert.ToInt32(ddlYear.SelectedValue);
            int currentyearweeks = GetWeeksInYear(currentYear);
            int weeks = currentyearweeks-1;
            int currentweek = GetIso8601WeekOfYear(DateTime.Now);
            int disabledweek = currentweek-1;
            DropDownList ddlusers1;
            string id;
            dtUsers = (DataTable)Session["dtOutput"];
            try
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Label line = e.Row.FindControl("LbllineNoId") as Label;

                        //Find the DropDownList in the Row             
                        objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                        dtEmployeeusers = objComBL.GetEmployeesBasedOnLocationBL(objUserBO);

                        for (int i = 1; i <= 52; i++)
                        {
                            id = "ddlusers" + i;
                            objCom.TestLog(id);
                            ddlusers1 = (e.Row.FindControl(id) as DropDownList);
                            ddlusers1.DataSource = dtEmployeeusers;
                            ddlusers1.DataTextField = "emp_full_name";
                            ddlusers1.DataValueField = "user_id";
                            ddlusers1.DataBind();

                            if (i <= disabledweek && currentYear == Convert.ToInt32(DateTime.Now.Year))
                            {
                                ddlusers1.Items.Insert(0, new ListItem("", "0"));
                                ddlusers1.Enabled = false;
                            }
                            else
                            {
                                ddlusers1.Items.Insert(0, new ListItem("", "0"));
                            }
                            if (dtUsers != null)
                            {
                                if (dtUsers.Rows.Count > 0)
                                {
                                    if (Convert.ToString(dtUsers.Rows[j][1]) == Convert.ToString(line.Text))
                                    {
                                        ddlusers1.SelectedValue = Convert.ToString(dtUsers.Rows[j][i + 3]);
                                    }
                                }
                            }
                        }

                        j++;
                    }
                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
                
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int currentYear = Convert.ToInt32(ddlYear.SelectedValue);
            int currentyearweeks = GetWeeksInYear(currentYear);
            objCom.TestLog("Week Number :" + Convert.ToString(currentyearweeks));
            int weeks = currentyearweeks-1;
            objCom.TestLog("Week No :" + Convert.ToString(weeks));
            objPlanBL = new PlanBL();
            objPlanBO = new PlanBO();
            DataTable SelectedUsers = new DataTable();
            SelectedUsers.Columns.Add("location_id");
            SelectedUsers.Columns.Add("line_id");
            SelectedUsers.Columns.Add("line_name");
            SelectedUsers.Columns.Add("year");
            for (int i = 1; i <= 52; i++)
            {
                SelectedUsers.Columns.Add("wk" + i);
            }
            DataSet dtMailDetails = new DataSet();
            try
            {
                if (CreateplanGrid.Rows.Count != 0)
                {
                    DropDownList Username;
                    DataRow dr;
                    Label linename;
                    Label linenum;
                    string id;
                    foreach (GridViewRow row in CreateplanGrid.Rows)
                    {
                        linename = (row.FindControl("LblLineNo") as Label);
                        linenum = (row.FindControl("LbllineNoId") as Label);
                        dr = SelectedUsers.NewRow();
                        dr["location_id"] = Convert.ToString(ddlLocation.SelectedValue);
                        dr["line_id"] = Convert.ToString(linenum.Text);
                        dr["line_name"] = Convert.ToString(linename.Text);
                        dr["year"] = Convert.ToInt32(ddlYear.SelectedValue);
                        for (int i = 1; i <= 52; i++)
                        {
                            id = "ddlusers" + i;
                            Username = (row.FindControl(id) as DropDownList);
                            dr["wk" + i] = Convert.ToString(Username.SelectedValue);
                           objCom.TestLog(Convert.ToString(linename.Text) +", week"+i+" : "+Convert.ToString(Username.SelectedItem.Value));
                        }
                        SelectedUsers.Rows.Add(dr);
                        
                    }
                    dtMailDetails = objPlanBL.SaveBulkPlanningBL(SelectedUsers);
                    if (dtMailDetails != null)
                    {
                        if (dtMailDetails.Tables[0] != null)
                        {
                            if (dtMailDetails.Tables[0].Rows.Count > 0)
                            {
                                SendInvitation(dtMailDetails.Tables[0]);
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Plans are created and mails are sent to respective employees.');", true);
                                //  BindData();
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('There is no plan saved or there is error in saving. Please try again.');", true);
                            }
                        }
                        if (dtMailDetails.Tables[1] != null)
                        {
                            if (dtMailDetails.Tables[1].Rows.Count > 0)
                            {
                                CancelInvitation(dtMailDetails.Tables[1]);
                                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Plans are created and mails are sent to respective employees.');", true);
                                //  BindData();
                            }
                            //else
                            //{
                            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('There is no plan saved or there is error in saving. Please try again.');", true);
                            //}
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void CancelInvitation(DataTable dtMailDetails)
        {
            eAppointmentMail objApptEmail;
            try
            {
                for (int i = 0; i < dtMailDetails.Rows.Count; i++)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(dtMailDetails.Rows[i]["email_id"])))
                    {
                        objApptEmail = new eAppointmentMail();
                        objApptEmail.Body = "Audit for " + Convert.ToString(dtMailDetails.Rows[i]["line_name"]) + " between " + Convert.ToString(dtMailDetails.Rows[i]["start_date"]) + " and " + Convert.ToString(dtMailDetails.Rows[i]["end_date"]) + " is cancelled. Please contact the LPA Administrator if you need to make any changes.";
                        objApptEmail.Email = Convert.ToString(dtMailDetails.Rows[i]["email_id"]);
                        //objApptEmail.EndDate = Convert.ToDateTime(dtMailDetails.Rows[i]["end_date"]);
                        objApptEmail.Location = Convert.ToString(dtMailDetails.Rows[i]["line_name"]) + ", " + Convert.ToString(dtMailDetails.Rows[i]["location_name"]);
                       // objApptEmail.StartDate = Convert.ToDateTime(dtMailDetails.Rows[i]["start_date"]);
                        objApptEmail.Subject = "Your audit planned from " + Convert.ToString(dtMailDetails.Rows[i]["start_date"]) + " to " + Convert.ToString(dtMailDetails.Rows[i]["end_date"]) + " for " + Convert.ToString(dtMailDetails.Rows[i]["line_name"])+" is cancelled";
                        objApptEmail.Name = Convert.ToString(dtMailDetails.Rows[i]["email_id"]);
                        objApptEmail.newStartDate = Convert.ToString(dtMailDetails.Rows[i]["start_date_formatted"]);
                        objApptEmail.newEndDate = Convert.ToString(dtMailDetails.Rows[i]["end_date_formatted"]);
                        objCom.CancelInvitation(objApptEmail);
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void SendInvitation(DataTable dtMailDetails)
        {
            eAppointmentMail objApptEmail;
            try
            {
                for (int i = 0; i < dtMailDetails.Rows.Count;i++ )
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(dtMailDetails.Rows[i]["email_id"])))
                    {
                        objApptEmail = new eAppointmentMail();
                        objApptEmail.Body = "Audit for " + Convert.ToString(dtMailDetails.Rows[i]["line_name"]) + " needs to be performed between " + Convert.ToString(dtMailDetails.Rows[i]["start_date"]) + " and " + Convert.ToString(dtMailDetails.Rows[i]["end_date"]) + ". Please contact the LPA Logistics Administrator if you need to make any changes.";
                        objApptEmail.Email = Convert.ToString(dtMailDetails.Rows[i]["email_id"]);
                       // objApptEmail.EndDate = Convert.ToDateTime(dtMailDetails.Rows[i]["end_date"]);
                        objApptEmail.Location = Convert.ToString(dtMailDetails.Rows[i]["line_name"]) + ", " + Convert.ToString(dtMailDetails.Rows[i]["location_name"]);
                       // objApptEmail.StartDate = Convert.ToDateTime(dtMailDetails.Rows[i]["start_date"]);
                        objApptEmail.Subject = "You have an audit planned from " + Convert.ToString(dtMailDetails.Rows[i]["start_date"]) + " to " + Convert.ToString(dtMailDetails.Rows[i]["end_date"]) + " for " + Convert.ToString(dtMailDetails.Rows[i]["line_name"]);
                        objApptEmail.Name = Convert.ToString(dtMailDetails.Rows[i]["email_id"]);
                        objApptEmail.newStartDate = Convert.ToString(dtMailDetails.Rows[i]["start_date_formatted"]);
                        objApptEmail.newEndDate = Convert.ToString(dtMailDetails.Rows[i]["end_date_formatted"]);
                        objCom.SendInvitation(objApptEmail);
                    }
                }
                    
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlLocation_SelectedIndexChanged(null, null);
        }

        #endregion

        #region Methods

        private void GetMasterDropdown()
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            DataTable dtLine = new DataTable();
            DataTable dtEmployee = new DataTable();
            DataTable dtPart = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                        if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            //     ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                            ddlRegion.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlRegion.Enabled = true;
                    }
                    else
                    {
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                }
                if (!string.IsNullOrEmpty(ddlRegion.SelectedValue))
                {
                    objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                    dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                    if (dtCountry.Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dtCountry;
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                        if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            //ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                            ddlCountry.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlCountry.Enabled = true;
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }
                if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
                {
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                    dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                    if (dtLocation.Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dtLocation;
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                        if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            //ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                            ddlLocation.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlLocation.Enabled = true;
                        Session["SelectedLocation"] = Convert.ToString(ddlLocation.SelectedValue);
                        Session["dtLocation"] = dtLocation;
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }


                ddlYear.Items.Insert(0, new ListItem(Convert.ToString(DateTime.Now.Year), Convert.ToString(DateTime.Now.Year)));
                ddlYear.Items.Insert(1, new ListItem(Convert.ToString(Convert.ToInt32(DateTime.Now.Year) + 1), Convert.ToString(Convert.ToInt32(DateTime.Now.Year) + 1)));
                ddlYear.Items.Insert(1, new ListItem(Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1), Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1)));
                BindData();

                 objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                if (dtLine.Rows.Count > 0)
                {
                    CreateplanGrid.DataSource = dtLine;
                    CreateplanGrid.DataBind();
                    Session["dtLine"] = dtLine;
                }
                else
                {
                    CreateplanGrid.DataSource = null;
                    CreateplanGrid.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        private void BindData()
        {
            try
            {
                objPlanBO = new PlanBO();
                objPlanBL = new PlanBL();
                DataTable dtOutput = new DataTable();
                objPlanBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objPlanBO.p_year = Convert.ToInt32(ddlYear.SelectedValue);
                dtOutput = objPlanBL.GetBulkPlansBL(objPlanBO);
                Session["dtOutput"] = dtOutput;
                
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        public int GetWeeksInYear(int year)
        {
            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            DateTime date1 = new DateTime(year, 12, 31);
            System.Globalization.Calendar cal = dfi.Calendar;
            return cal.GetWeekOfYear(date1, dfi.CalendarWeekRule,
                                                dfi.FirstDayOfWeek);
        }

        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        #endregion
       
    }
}
