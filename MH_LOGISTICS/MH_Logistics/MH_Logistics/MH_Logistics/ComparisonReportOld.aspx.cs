﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using MH_Logistics_BusinessLogic;
using MH_Logistics_BusinessObject;

namespace MH_Logistics
{
    public partial class ComparisonReportOld : System.Web.UI.Page
    {
        #region Global Declaration
        CommonFunctions objCom = new CommonFunctions();
        DataSet dsDropDownData;
        UsersBO objUserBO;
        DataTable dtAnswer = new DataTable();
        CommonBL objComBL;
        DataTable dtReport1;
        DataTable dtReport2;
        DataTable dtReport3;
        DashboardBO objDashBO;
        DashboardBL objDashBL;
        private readonly Random random = new Random(1234);
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    objDashBO = new DashboardBO();
                    LoadFilterDropDowns(objDashBO);
                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
            }
        }

        protected void ddlRegion1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion1.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry1.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation1.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine1.SelectedValue);
                objDashBO.selection_flag = "Region";

                BindFilters1(objDashBO);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCountry1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion1.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry1.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation1.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine1.SelectedValue);
                objDashBO.selection_flag = "Country";
                BindFilters1(objDashBO);
                ddlCountry1.SelectedValue = Convert.ToString(objDashBO.country_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLocation1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion1.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry1.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation1.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine1.SelectedValue);
                objDashBO.selection_flag = "Location";
                BindFilters1(objDashBO);
                ddlCountry1.SelectedValue = Convert.ToString(objDashBO.country_id);
                ddlLocation1.SelectedValue = Convert.ToString(objDashBO.location_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLine1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion1.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry1.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation1.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine1.SelectedValue);
                objDashBO.selection_flag = "Line";
                BindFilters1(objDashBO);
                ddlCountry1.SelectedValue = Convert.ToString(objDashBO.country_id);
                ddlLocation1.SelectedValue = Convert.ToString(objDashBO.location_id);
                ddlLine1.SelectedValue = Convert.ToString(objDashBO.line_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlRegion2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion2.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry2.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation2.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine2.SelectedValue);
                objDashBO.selection_flag = "Region";
                BindFilters2(objDashBO);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCountry2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion2.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry2.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation2.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine2.SelectedValue);
                objDashBO.selection_flag = "Country";
                BindFilters2(objDashBO);
                ddlCountry2.SelectedValue = Convert.ToString(objDashBO.country_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLocation2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion2.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry2.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation2.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine2.SelectedValue);
                objDashBO.selection_flag = "Location";
                BindFilters2(objDashBO);
                ddlCountry2.SelectedValue = Convert.ToString(objDashBO.country_id);
                ddlLocation2.SelectedValue = Convert.ToString(objDashBO.location_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLine2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion2.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry2.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation2.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine2.SelectedValue);
                objDashBO.selection_flag = "Line";
                BindFilters2(objDashBO);
                ddlCountry2.SelectedValue = Convert.ToString(objDashBO.country_id);
                ddlLocation2.SelectedValue = Convert.ToString(objDashBO.location_id);
                ddlLine2.SelectedValue = Convert.ToString(objDashBO.line_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlRegion3_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion3.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry3.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation3.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine3.SelectedValue);
                objDashBO.selection_flag = "Region";

                BindFilters3(objDashBO);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCountry3_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion3.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry3.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation3.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine3.SelectedValue);
                objDashBO.selection_flag = "Country";
                BindFilters3(objDashBO);
                ddlCountry3.SelectedValue = Convert.ToString(objDashBO.country_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLocation3_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion3.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry3.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation3.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine3.SelectedValue);
                objDashBO.selection_flag = "Location";
                BindFilters3(objDashBO);
                ddlCountry3.SelectedValue = Convert.ToString(objDashBO.country_id);
                ddlLocation3.SelectedValue = Convert.ToString(objDashBO.location_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLine3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion3.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry3.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation3.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine3.SelectedValue);
                objDashBO.selection_flag = "Line";
                BindFilters3(objDashBO);
                ddlCountry3.SelectedValue = Convert.ToString(objDashBO.country_id);
                ddlLocation3.SelectedValue = Convert.ToString(objDashBO.location_id);
                ddlLine3.SelectedValue = Convert.ToString(objDashBO.line_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                string legend_name = string.Empty;
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                dtReport1 = new DataTable();
                dtReport2 = new DataTable();
                dtReport3 = new DataTable();
                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(txtDate.Text, UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string reportdate = parsedDate.ToString(DBPattern);
                    objDashBO.reportdate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }
                #region LPA Result Monthly
                objDashBO.location_id = Convert.ToInt32(ddlLocation1.SelectedValue);
                objDashBO.region_id = Convert.ToInt32(ddlRegion1.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry1.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine1.SelectedValue);
                dtReport1 = objDashBL.getDataforLPAResultsReportBL(objDashBO);
                objDashBO.location_id = Convert.ToInt32(ddlLocation2.SelectedValue);
                objDashBO.region_id = Convert.ToInt32(ddlRegion2.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry2.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine2.SelectedValue);
                dtReport2 = objDashBL.getDataforLPAResultsReportBL(objDashBO);
                objDashBO.location_id = Convert.ToInt32(ddlLocation3.SelectedValue);
                objDashBO.region_id = Convert.ToInt32(ddlRegion3.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry3.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine3.SelectedValue);
                dtReport3 = objDashBL.getDataforLPAResultsReportBL(objDashBO);

                #region comparing all tables
                DataRow dr;
                int flag = 0;
                int index = 0;
                if (dtReport1.Rows.Count > 0 && dtReport2.Rows.Count > 0)
                {
                    foreach (DataRow row1 in dtReport1.Rows)
                    {
                        flag = 0;
                        foreach (DataRow row2 in dtReport2.Rows)
                        {
                            if ((row1["audit_period"].ToString().Equals(row2["audit_period"].ToString())))
                            {
                                flag++;
                            }
                        }
                        if (flag == 0)
                        {
                            dr = dtReport2.NewRow();
                            dr["audit_period"] = row1["audit_period"];
                            dr["average_score"] = 0;
                            dtReport2.Rows.InsertAt(dr, index);
                        }
                        index++;
                    }

                    index = 0;
                    foreach (DataRow row1 in dtReport2.Rows)
                    {
                        flag = 0;
                        foreach (DataRow row2 in dtReport1.Rows)
                        {
                            if ((row1["audit_period"].ToString().Equals(row2["audit_period"].ToString())))
                            {
                                flag++;
                            }
                        }
                        if (flag == 0)
                        {
                            dr = dtReport1.NewRow();
                            dr["audit_period"] = row1["audit_period"];
                            dr["average_score"] = 0;
                            dtReport1.Rows.InsertAt(dr, index);
                        }
                        index++;
                    }
                }
                
                if (dtReport3.Rows.Count > 0 && dtReport2.Rows.Count > 0)
                {
                    index = 0;
                    foreach (DataRow row1 in dtReport2.Rows)
                    {
                        flag = 0;
                        foreach (DataRow row2 in dtReport3.Rows)
                        {
                            if ((row1["audit_period"].ToString().Equals(row2["audit_period"].ToString())))
                            {
                                flag++;
                            }
                        }
                        if (flag == 0)
                        {
                            dr = dtReport3.NewRow();
                            dr["audit_period"] = row1["audit_period"];
                            dr["average_score"] = 0;
                            dtReport3.Rows.InsertAt(dr, index);
                        }
                        index++;
                    }

                    index = 0;
                    foreach (DataRow row1 in dtReport3.Rows)
                    {
                        flag = 0;
                        foreach (DataRow row2 in dtReport2.Rows)
                        {
                            if ((row1["audit_period"].ToString().Equals(row2["audit_period"].ToString())))
                            {
                                flag++;
                            }
                        }
                        if (flag == 0)
                        {
                            dr = dtReport2.NewRow();
                            dr["audit_period"] = row1["audit_period"];
                            dr["average_score"] = 0;
                            dtReport2.Rows.InsertAt(dr, index);
                        }
                        index++;
                    }
                }

                if (dtReport1.Rows.Count > 0 && dtReport3.Rows.Count > 0)
                {
                    index = 0;
                    foreach (DataRow row1 in dtReport3.Rows)
                    {
                        flag = 0;
                        foreach (DataRow row2 in dtReport1.Rows)
                        {
                            if ((row1["audit_period"].ToString().Equals(row2["audit_period"].ToString())))
                            {
                                flag++;
                            }
                        }
                        if (flag == 0)
                        {
                            dr = dtReport1.NewRow();
                            dr["audit_period"] = row1["audit_period"];
                            dr["average_score"] = 0;
                            dtReport1.Rows.InsertAt(dr, index);
                        }
                        index++;
                    }
                }
                #endregion


                if (dtReport1.Rows.Count > 0)
                {
                    pnlReport.Visible = true;

                    DataTable ChartData = dtReport1;
                    string[] XPointMember = new string[ChartData.Rows.Count];
                    int[] YPointMember = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["average_score"]) * 100);
                    }
                    Chart1.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                    Chart1.Series[0].ChartType = SeriesChartType.Column;
                    Chart1.Series[0].Color = Color.DarkGreen;
                    legend_name = "Choice1 : " + Convert.ToString(ddlRegion1.SelectedItem);
                    if (Convert.ToInt32(ddlCountry1.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlCountry1.SelectedItem);
                    }
                    if (Convert.ToInt32(ddlLocation1.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlLocation1.SelectedItem);
                    }
                    if (Convert.ToInt32(ddlLine1.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlLine1.SelectedItem);
                    }
                    Chart1.Series[0].Name = legend_name;
                    Chart1.Legends[0].Position.Auto = false;
                    Chart1.Legends[0].Position = new ElementPosition(0, 5, 100, 10);
                    Chart1.Legends[0].Docking = Docking.Top;
                    Chart1.Legends[0].IsDockedInsideChartArea = false;
                    Chart1.Legends[0].DockedToChartArea = "cA1";
                }
                if (dtReport2.Rows.Count > 0)
                {
                    DataTable ChartData = dtReport2;
                    string[] XPointMember = new string[ChartData.Rows.Count];
                    int[] YPointMember = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["average_score"]) * 100);
                    }
                    Chart1.Series[1].Points.DataBindXY(XPointMember, YPointMember);
                    Chart1.Series[1].ChartType = SeriesChartType.Column;
                    Chart1.Series[1].Color = Color.DarkOrange;
                    legend_name = "Choice2 : "+Convert.ToString(ddlRegion2.SelectedItem);
                    if (Convert.ToInt32(ddlCountry2.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlCountry2.SelectedItem);
                    }
                    if (Convert.ToInt32(ddlLocation2.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlLocation2.SelectedItem);
                    }
                    if (Convert.ToInt32(ddlLine2.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlLine2.SelectedItem);
                    }
                    Chart1.Series[1].Name = legend_name;
                    Chart1.Legends[1].Position.Auto = false;
                    Chart1.Legends[1].Position = new ElementPosition(0, 5, 100, 10);
                    Chart1.Legends[1].Docking = Docking.Top;
                    Chart1.Legends[1].IsDockedInsideChartArea = false;
                    Chart1.Legends[1].DockedToChartArea = "cA1";
                }
                if (dtReport3.Rows.Count > 0)
                {

                    DataTable ChartData = dtReport3;
                    string[] XPointMember = new string[ChartData.Rows.Count];
                    int[] YPointMember = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["audit_period"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["average_score"]) * 100);
                    }
                    Chart1.Series[2].Points.DataBindXY(XPointMember, YPointMember);
                    Chart1.Series[2].ChartType = SeriesChartType.Column;
                    Chart1.Series[2].Color = Color.Gray;
                    legend_name = "Choice3 : " + Convert.ToString(ddlRegion3.SelectedItem);
                    if (Convert.ToInt32(ddlCountry3.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlCountry3.SelectedItem);
                    }
                    if (Convert.ToInt32(ddlLocation3.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlLocation3.SelectedItem);
                    }
                    if (Convert.ToInt32(ddlLine3.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlLine3.SelectedItem);
                    }
                    Chart1.Series[2].Name = legend_name;
                    Chart1.Legends[2].Position.Auto = false;
                    Chart1.Legends[2].Position = new ElementPosition(0, 5, 100, 10);
                    Chart1.Legends[2].Docking = Docking.Top;
                    Chart1.Legends[2].IsDockedInsideChartArea = false;
                    Chart1.Legends[2].DockedToChartArea = "cA1";
                }
                Chart1.Series[0].IsValueShownAsLabel = true;
                Chart1.Series[1].IsValueShownAsLabel = true;
                Chart1.Series[2].IsValueShownAsLabel = true;
                Chart1.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                Chart1.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                Chart1.ChartAreas[0].AxisX.Interval = 1;
                Chart1.ChartAreas[0].AxisY.Interval = 25;
                Chart1.Series[0].LabelFormat = "{0} %";
                Chart1.Series[1].LabelFormat = "{0} %";
                Chart1.Series[2].LabelFormat = "{0} %";
                Chart1.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                Chart1.Titles[0].Text = "LPA Result";
                Chart1.ChartAreas[0].AxisY.Maximum = 120;
                #endregion

                #region LPA Result Per Section
                objDashBO.location_id = Convert.ToInt32(ddlLocation1.SelectedValue);
                objDashBO.region_id = Convert.ToInt32(ddlRegion1.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry1.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine1.SelectedValue);
                dtReport1 = objDashBL.getLPAResultPerSectionBL(objDashBO);
                objDashBO.location_id = Convert.ToInt32(ddlLocation2.SelectedValue);
                objDashBO.region_id = Convert.ToInt32(ddlRegion2.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry2.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine2.SelectedValue);
                dtReport2 = objDashBL.getLPAResultPerSectionBL(objDashBO);
                objDashBO.location_id = Convert.ToInt32(ddlLocation3.SelectedValue);
                objDashBO.region_id = Convert.ToInt32(ddlRegion3.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry3.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine3.SelectedValue);
                dtReport3 = objDashBL.getLPAResultPerSectionBL(objDashBO);

                if (dtReport1.Rows.Count > 0)
                {
                    pnlReport.Visible = true;

                    DataTable ChartData = dtReport1;
                    sec1.Text = Convert.ToString(ChartData.Rows[0]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[0]["Section_name"]);
                    sec2.Text = Convert.ToString(ChartData.Rows[1]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[1]["Section_name"]);
                    sec3.Text = Convert.ToString(ChartData.Rows[2]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[2]["Section_name"]);
                    sec4.Text = Convert.ToString(ChartData.Rows[3]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[3]["Section_name"]);
                    sec5.Text = Convert.ToString(ChartData.Rows[4]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[4]["Section_name"]);
                    sec6.Text = Convert.ToString(ChartData.Rows[5]["Sec_name_abbr"]) + " - " + Convert.ToString(ChartData.Rows[5]["Section_name"]);
                    string[] XPointMember = new string[ChartData.Rows.Count];
                    int[] YPointMember = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["Sec_name_abbr"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["Score"]) * 100);
                        //ChartPerSection.Series[0].Name = column.ColumnName;

                    }

                    ChartPerSection1.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                    ChartPerSection1.Series[0].ChartType = SeriesChartType.Radar;
                    ChartPerSection1.Series[0]["RadarDrawingStyle"] = "Area";
                    ChartPerSection1.Series[0]["AreaDrawingStyle"] = "Polygon";
                    ChartPerSection1.Series[0].Color = Color.DarkGreen;

                    ChartPerSection1.Titles[0].Text = Convert.ToString(ddlRegion1.SelectedItem);
                    ChartPerSection1.Series[0].IsValueShownAsLabel = true;
                    //ChartPerSection1.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    //ChartPerSection1.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
                    //ChartPerSection1.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
                    //ChartPerSection1.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                    ChartPerSection1.ChartAreas[0].AxisX.Interval = 1;
                    ChartPerSection1.ChartAreas[0].AxisY.Interval = 25;
                    ChartPerSection1.Series[0].LabelFormat = "{0} %";
                    ChartPerSection1.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                    ChartPerSection1.ChartAreas[0].AxisY.Maximum = 100;
                }
                if (dtReport2.Rows.Count > 0)
                {
                    pnlReport.Visible = true;

                    DataTable ChartData = dtReport2;
                    string[] XPointMember = new string[ChartData.Rows.Count];
                    int[] YPointMember = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["Sec_name_abbr"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["Score"]) * 100);
                        //ChartPerSection.Series[0].Name = column.ColumnName;

                    }

                    ChartPerSection2.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                    ChartPerSection2.Series[0].ChartType = SeriesChartType.Radar;
                    ChartPerSection2.Series[0]["RadarDrawingStyle"] = "Area";
                    ChartPerSection2.Series[0]["AreaDrawingStyle"] = "Polygon";
                    ChartPerSection2.Series[0].Color = Color.DarkOrange;

                    ChartPerSection2.Titles[0].Text = Convert.ToString(ddlRegion2.SelectedItem);
                    ChartPerSection2.Series[0].IsValueShownAsLabel = true;
                    //ChartPerSection2.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
                    //ChartPerSection2.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    //ChartPerSection2.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                    ChartPerSection2.ChartAreas[0].AxisX.Interval = 1;
                    ChartPerSection2.ChartAreas[0].AxisY.Interval = 25;
                    ChartPerSection2.Series[0].LabelFormat = "{0} %";
                    ChartPerSection2.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                    ChartPerSection2.ChartAreas[0].AxisY.Maximum = 100;
                }
                if (dtReport3.Rows.Count > 0)
                {
                    pnlReport.Visible = true;

                    DataTable ChartData = dtReport3;
                    string[] XPointMember = new string[ChartData.Rows.Count];
                    int[] YPointMember = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["Sec_name_abbr"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["Score"]) * 100);
                        //ChartPerSection.Series[0].Name = column.ColumnName;

                    }

                    ChartPerSection3.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                    ChartPerSection3.Series[0].ChartType = SeriesChartType.Radar;
                    ChartPerSection3.Series[0]["RadarDrawingStyle"] = "Area";
                    ChartPerSection3.Series[0]["AreaDrawingStyle"] = "Polygon";
                    ChartPerSection3.Series[0].Color = Color.Gray;

                    ChartPerSection3.Titles[0].Text = Convert.ToString(ddlRegion3.SelectedItem);
                    ChartPerSection3.Series[0].IsValueShownAsLabel = true;
                    //ChartPerSection3.ChartAreas[0].AxisX.MajorGrid.Enabled = false;
                    //ChartPerSection3.ChartAreas[0].AxisY.MajorGrid.Enabled = false;
                    //ChartPerSection3.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    //ChartPerSection3.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                    ChartPerSection3.ChartAreas[0].AxisX.Interval = 1;
                    ChartPerSection3.ChartAreas[0].AxisY.Interval = 25;
                    ChartPerSection3.Series[0].LabelFormat = "{0} %";
                    ChartPerSection3.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                    ChartPerSection3.ChartAreas[0].AxisY.Maximum = 100;
                }
                #endregion

                #region LPA Result Per Question
                objDashBO.location_id = Convert.ToInt32(ddlLocation1.SelectedValue);
                objDashBO.region_id = Convert.ToInt32(ddlRegion1.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry1.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine1.SelectedValue);
                dtReport1 = objDashBL.getLPAResultByQuestionBL(objDashBO);
                objDashBO.location_id = Convert.ToInt32(ddlLocation2.SelectedValue);
                objDashBO.region_id = Convert.ToInt32(ddlRegion2.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry2.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine2.SelectedValue);
                dtReport2 = objDashBL.getLPAResultByQuestionBL(objDashBO);
                objDashBO.location_id = Convert.ToInt32(ddlLocation3.SelectedValue);
                objDashBO.region_id = Convert.ToInt32(ddlRegion3.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry3.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLine3.SelectedValue);
                dtReport3 = objDashBL.getLPAResultByQuestionBL(objDashBO);

                if (dtReport1.Rows.Count > 0)
                {
                    pnlReport.Visible = true;

                    DataTable ChartData = new DataTable();
                    ChartData = dtReport1;
                    string[] XPointMember = new string[ChartData.Rows.Count];
                    int[] YPointMember = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = Convert.ToString(ChartData.Rows[count]["question_id"]).Substring(1, Convert.ToString(ChartData.Rows[count]["question_id"]).Length - 1);
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["score"]) * 100);
                    }
                    ChartPerQuestion.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                    ChartPerQuestion.Series[0].ChartType = SeriesChartType.Column;
                    ChartPerQuestion.Series[0].Color = Color.DarkGreen;
                    ChartPerQuestion.Series[0].IsValueShownAsLabel = true;
                    ChartPerQuestion.Series[0].LabelFormat = "{0} %";
                    ChartPerQuestion.Series[0].Font = new Font("Gotham-Book.ttf", 5);
                    legend_name = "Choice1 : " + Convert.ToString(ddlRegion1.SelectedItem);
                    if (Convert.ToInt32(ddlCountry1.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlCountry1.SelectedItem);
                    }
                    if (Convert.ToInt32(ddlLocation1.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlLocation1.SelectedItem);
                    }
                    if (Convert.ToInt32(ddlLine1.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlLine1.SelectedItem);
                    }
                    ChartPerQuestion.Series[0].Name = legend_name;
                    ChartPerQuestion.Legends[0].Position.Auto = false;
                    ChartPerQuestion.Legends[0].Position = new ElementPosition(0, 5, 100, 10);
                    ChartPerQuestion.Legends[0].Docking = Docking.Top;
                    ChartPerQuestion.Legends[0].IsDockedInsideChartArea = false;
                    ChartPerQuestion.Legends[0].DockedToChartArea = "ca5";
                }
                if (dtReport2.Rows.Count > 0)
                {
                    pnlReport.Visible = true;

                    DataTable ChartData = new DataTable();
                    ChartData = dtReport2;
                    string[] XPointMember = new string[ChartData.Rows.Count];
                    int[] YPointMember = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = Convert.ToString(ChartData.Rows[count]["question_id"]).Substring(1, Convert.ToString(ChartData.Rows[count]["question_id"]).Length - 1);
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["score"]) * 100);
                    }
                    ChartPerQuestion.Series[1].Points.DataBindXY(XPointMember, YPointMember);
                    ChartPerQuestion.Series[1].ChartType = SeriesChartType.Column;
                    ChartPerQuestion.Series[1].Color = Color.DarkOrange;
                    ChartPerQuestion.Series[1].IsValueShownAsLabel = true;
                    ChartPerQuestion.Series[1].LabelFormat = "{0} %";
                    ChartPerQuestion.Series[1].Font = new Font("Gotham-Book.ttf", 5);
                    legend_name = "Choice2 : " + Convert.ToString(ddlRegion2.SelectedItem);
                    if (Convert.ToInt32(ddlCountry2.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlCountry2.SelectedItem);
                    }
                    if (Convert.ToInt32(ddlLocation2.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlLocation2.SelectedItem);
                    }
                    if (Convert.ToInt32(ddlLine2.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlLine2.SelectedItem);
                    }
                    ChartPerQuestion.Series[1].Name = legend_name;
                    ChartPerQuestion.Legends[1].Position.Auto = false;
                    ChartPerQuestion.Legends[1].Position = new ElementPosition(0, 5, 100, 10);
                    ChartPerQuestion.Legends[1].Docking = Docking.Top;
                    ChartPerQuestion.Legends[1].IsDockedInsideChartArea = false;
                    ChartPerQuestion.Legends[1].DockedToChartArea = "ca5";
                }
                if (dtReport3.Rows.Count > 0)
                {
                    pnlReport.Visible = true;

                    DataTable ChartData = new DataTable();
                    ChartData = dtReport3;
                    string[] XPointMember = new string[ChartData.Rows.Count];
                    int[] YPointMember = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = Convert.ToString(ChartData.Rows[count]["question_id"]).Substring(1, Convert.ToString(ChartData.Rows[count]["question_id"]).Length - 1);
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(Convert.ToDecimal(ChartData.Rows[count]["score"]) * 100);
                    }
                    ChartPerQuestion.Series[2].Points.DataBindXY(XPointMember, YPointMember);
                    ChartPerQuestion.Series[2].ChartType = SeriesChartType.Column;
                    ChartPerQuestion.Series[2].Color = Color.Gray;
                    ChartPerQuestion.Series[2].IsValueShownAsLabel = true;
                    ChartPerQuestion.Series[2].LabelFormat = "{0} %";
                    ChartPerQuestion.Series[2].Font = new Font("Gotham-Book.ttf", 5);

                    legend_name = "Choice3 : " + Convert.ToString(ddlRegion3.SelectedItem);
                    if (Convert.ToInt32(ddlCountry3.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlCountry3.SelectedItem);
                    }
                    if (Convert.ToInt32(ddlLocation3.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlLocation3.SelectedItem);
                    }
                    if (Convert.ToInt32(ddlLine3.SelectedValue) != 0)
                    {
                        legend_name = legend_name + "-" + Convert.ToString(ddlLine3.SelectedItem);
                    }
                    ChartPerQuestion.Series[2].Name = legend_name;
                    ChartPerQuestion.Legends[2].Position.Auto = false;
                    ChartPerQuestion.Legends[2].Position = new ElementPosition(0, 5, 100, 10);
                    ChartPerQuestion.Legends[2].Docking = Docking.Top;
                    ChartPerQuestion.Legends[2].IsDockedInsideChartArea = false;
                    ChartPerQuestion.Legends[2].DockedToChartArea = "ca5";
                }

                ChartPerQuestion.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                ChartPerQuestion.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                ChartPerQuestion.ChartAreas[0].AxisX.Interval = 1;
                ChartPerQuestion.Titles[0].Text = "LPA Result per Question";
                ChartPerQuestion.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                ChartPerQuestion.ChartAreas[0].AxisY.Interval = 25;
                ChartPerQuestion.ChartAreas[0].AxisY.Maximum = 120;
                #endregion
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                LoadFilterDropDowns(objDashBO);
                pnlReport.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        #endregion

        #region Methods
        private void LoadFilterDropDowns(DashboardBO objDashBO)
        {
            try
            {
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion1.DataSource = dsDropDownData.Tables[0];
                        ddlRegion1.DataTextField = "region_name";
                        ddlRegion1.DataValueField = "region_id";
                        ddlRegion1.DataBind();
                        ddlRegion1.Items.Insert(0, new ListItem("All", "0"));
                        ddlRegion2.DataSource = dsDropDownData.Tables[0];
                        ddlRegion2.DataTextField = "region_name";
                        ddlRegion2.DataValueField = "region_id";
                        ddlRegion2.DataBind();
                        ddlRegion2.Items.Insert(0, new ListItem("All", "0"));
                        ddlRegion3.DataSource = dsDropDownData.Tables[0];
                        ddlRegion3.DataTextField = "region_name";
                        ddlRegion3.DataValueField = "region_id";
                        ddlRegion3.DataBind();
                        ddlRegion3.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlRegion1.Items.Clear();
                        ddlRegion1.DataSource = null;
                        ddlRegion1.DataBind();
                        ddlRegion2.Items.Clear();
                        ddlRegion2.DataSource = null;
                        ddlRegion2.DataBind();
                        ddlRegion3.Items.Clear();
                        ddlRegion3.DataSource = null;
                        ddlRegion3.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry1.DataSource = dsDropDownData.Tables[1];
                        ddlCountry1.DataTextField = "country_name";
                        ddlCountry1.DataValueField = "country_id";
                        ddlCountry1.DataBind();
                        ddlCountry1.Items.Insert(0, new ListItem("All", "0"));
                        ddlCountry2.DataSource = dsDropDownData.Tables[1];
                        ddlCountry2.DataTextField = "country_name";
                        ddlCountry2.DataValueField = "country_id";
                        ddlCountry2.DataBind();
                        ddlCountry2.Items.Insert(0, new ListItem("All", "0"));
                        ddlCountry3.DataSource = dsDropDownData.Tables[1];
                        ddlCountry3.DataTextField = "country_name";
                        ddlCountry3.DataValueField = "country_id";
                        ddlCountry3.DataBind();
                        ddlCountry3.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlCountry1.Items.Clear();
                        ddlCountry1.DataSource = null;
                        ddlCountry1.DataBind();
                        ddlCountry2.Items.Clear();
                        ddlCountry2.DataSource = null;
                        ddlCountry2.DataBind();
                        ddlCountry3.Items.Clear();
                        ddlCountry3.DataSource = null;
                        ddlCountry3.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation1.DataSource = dsDropDownData.Tables[2];
                        ddlLocation1.DataTextField = "location_name";
                        ddlLocation1.DataValueField = "location_id";
                        ddlLocation1.DataBind();
                        ddlLocation1.Items.Insert(0, new ListItem("All", "0"));
                        ddlLocation2.DataSource = dsDropDownData.Tables[2];
                        ddlLocation2.DataTextField = "location_name";
                        ddlLocation2.DataValueField = "location_id";
                        ddlLocation2.DataBind();
                        ddlLocation2.Items.Insert(0, new ListItem("All", "0"));
                        ddlLocation3.DataSource = dsDropDownData.Tables[2];
                        ddlLocation3.DataTextField = "location_name";
                        ddlLocation3.DataValueField = "location_id";
                        ddlLocation3.DataBind();
                        ddlLocation3.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation1.Items.Clear();
                        ddlLocation1.DataSource = null;
                        ddlLocation1.DataBind();
                        ddlLocation2.Items.Clear();
                        ddlLocation2.DataSource = null;
                        ddlLocation2.DataBind();
                        ddlLocation3.Items.Clear();
                        ddlLocation3.DataSource = null;
                        ddlLocation3.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLine1.DataSource = dsDropDownData.Tables[3];
                        ddlLine1.DataTextField = "line_name";
                        ddlLine1.DataValueField = "line_id";
                        ddlLine1.DataBind();
                        ddlLine1.Items.Insert(0, new ListItem("All", "0"));
                        ddlLine2.DataSource = dsDropDownData.Tables[3];
                        ddlLine2.DataTextField = "line_name";
                        ddlLine2.DataValueField = "line_id";
                        ddlLine2.DataBind();
                        ddlLine2.Items.Insert(0, new ListItem("All", "0"));
                        ddlLine3.DataSource = dsDropDownData.Tables[3];
                        ddlLine3.DataTextField = "line_name";
                        ddlLine3.DataValueField = "line_id";
                        ddlLine3.DataBind();
                        ddlLine3.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLine1.Items.Clear();
                        ddlLine1.DataSource = null;
                        ddlLine1.DataBind();
                        ddlLine2.Items.Clear();
                        ddlLine2.DataSource = null;
                        ddlLine2.DataBind();
                        ddlLine3.Items.Clear();
                        ddlLine3.DataSource = null;
                        ddlLine3.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void BindFilters1(DashboardBO objDashBO)
        {
            try
            {
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion1.DataSource = dsDropDownData.Tables[0];
                        ddlRegion1.DataTextField = "region_name";
                        ddlRegion1.DataValueField = "region_id";
                        ddlRegion1.DataBind();
                    }
                    else
                    {
                        ddlRegion1.Items.Clear();
                        ddlRegion1.DataSource = null;
                        ddlRegion1.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry1.DataSource = dsDropDownData.Tables[1];
                        ddlCountry1.DataTextField = "country_name";
                        ddlCountry1.DataValueField = "country_id";
                        ddlCountry1.DataBind();
                        ddlCountry1.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlCountry1.Items.Clear();
                        ddlCountry1.DataSource = null;
                        ddlCountry1.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation1.DataSource = dsDropDownData.Tables[2];
                        ddlLocation1.DataTextField = "location_name";
                        ddlLocation1.DataValueField = "location_id";
                        ddlLocation1.DataBind();
                        ddlLocation1.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation1.Items.Clear();
                        ddlLocation1.DataSource = null;
                        ddlLocation1.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLine1.DataSource = dsDropDownData.Tables[3];
                        ddlLine1.DataTextField = "line_name";
                        ddlLine1.DataValueField = "line_id";
                        ddlLine1.DataBind();
                        ddlLine1.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLine1.Items.Clear();
                        ddlLine1.DataSource = null;
                        ddlLine1.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void BindFilters2(DashboardBO objDashBO)
        {
            try
            {
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion2.DataSource = dsDropDownData.Tables[0];
                        ddlRegion2.DataTextField = "region_name";
                        ddlRegion2.DataValueField = "region_id";
                        ddlRegion2.DataBind();
                    }
                    else
                    {
                        ddlRegion2.Items.Clear();
                        ddlRegion2.DataSource = null;
                        ddlRegion2.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry2.DataSource = dsDropDownData.Tables[1];
                        ddlCountry2.DataTextField = "country_name";
                        ddlCountry2.DataValueField = "country_id";
                        ddlCountry2.DataBind();
                        ddlCountry2.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlCountry2.Items.Clear();
                        ddlCountry2.DataSource = null;
                        ddlCountry2.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation2.DataSource = dsDropDownData.Tables[2];
                        ddlLocation2.DataTextField = "location_name";
                        ddlLocation2.DataValueField = "location_id";
                        ddlLocation2.DataBind();
                        ddlLocation2.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation2.Items.Clear();
                        ddlLocation2.DataSource = null;
                        ddlLocation2.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLine2.DataSource = dsDropDownData.Tables[3];
                        ddlLine2.DataTextField = "line_name";
                        ddlLine2.DataValueField = "line_id";
                        ddlLine2.DataBind();
                        ddlLine2.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLine2.Items.Clear();
                        ddlLine2.DataSource = null;
                        ddlLine2.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void BindFilters3(DashboardBO objDashBO)
        {
            try
            {
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion3.DataSource = dsDropDownData.Tables[0];
                        ddlRegion3.DataTextField = "region_name";
                        ddlRegion3.DataValueField = "region_id";
                        ddlRegion3.DataBind();
                    }
                    else
                    {
                        ddlRegion3.Items.Clear();
                        ddlRegion3.DataSource = null;
                        ddlRegion3.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry3.DataSource = dsDropDownData.Tables[1];
                        ddlCountry3.DataTextField = "country_name";
                        ddlCountry3.DataValueField = "country_id";
                        ddlCountry3.DataBind();
                        ddlCountry3.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlCountry3.Items.Clear();
                        ddlCountry3.DataSource = null;
                        ddlCountry3.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation3.DataSource = dsDropDownData.Tables[2];
                        ddlLocation3.DataTextField = "location_name";
                        ddlLocation3.DataValueField = "location_id";
                        ddlLocation3.DataBind();
                        ddlLocation3.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation3.Items.Clear();
                        ddlLocation3.DataSource = null;
                        ddlLocation3.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLine3.DataSource = dsDropDownData.Tables[3];
                        ddlLine3.DataTextField = "line_name";
                        ddlLine3.DataValueField = "line_id";
                        ddlLine3.DataBind();
                        ddlLine3.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLine3.Items.Clear();
                        ddlLine3.DataSource = null;
                        ddlLine3.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        #endregion

    }
}