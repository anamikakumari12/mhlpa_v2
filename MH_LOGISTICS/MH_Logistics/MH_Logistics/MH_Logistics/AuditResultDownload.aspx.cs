﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MH_Logistics_BusinessLogic;
using MH_Logistics_BusinessObject;

namespace MH_Logistics
{
    public partial class AuditResultDownload : System.Web.UI.Page
    {
        #region Global Declaration
        CommonFunctions objCom = new CommonFunctions();
        DataSet dsDropDownData;
        UsersBO objUserBO;
        DataTable dtAnswer = new DataTable();
        CommonBL objComBL;
        DataTable dtReport;
        DashboardBO objDashBO;
        DashboardBL objDashBL;
        #endregion

        #region Events

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Jan 2018
        /// Desc : when the page loads, all the drop down values are loaded in this event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }

                if (!IsPostBack)
                {
                    LoadDivisionDropdown();
                    objDashBO = new DashboardBO();
                    LoadFilterDropDowns(objDashBO);
                    //btnFilter_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Jan 2018
        /// Desc : Based on selection of region value, country, location, line will be loaded accordinglyby calling getFilterDataBL method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.division_id = Convert.ToInt32(ddlDivision.SelectedValue);
                //objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                //objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Region";
                // LoadFilterDropDowns(objDashBO);


                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        //ddlRegion.DataSource = dsDropDownData.Tables[0];
                        //ddlRegion.DataTextField = "region_name";
                        //ddlRegion.DataValueField = "region_id";
                        //ddlRegion.DataBind();
                        //ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        ddlRegion.SelectedValue = Convert.ToString(objDashBO.region_id);
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Jan 2018
        /// Desc : Based on selection of country value, region, location, line will be loaded accordinglyby calling getFilterDataBL method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            DataSet dsDropDownDataCountry = new DataSet();
            DashboardBO objDashBOCountry = new DashboardBO();
            int region;
            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                objDashBO.division_id = Convert.ToInt32(ddlDivision.SelectedValue);
                if (objDashBO.country_id == 0)
                {
                    objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                }
                //objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Country";
                // LoadFilterDropDowns(objDashBO);

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        //    ddlRegion.DataSource = dsDropDownData.Tables[0];
                        //    ddlRegion.DataTextField = "region_name";
                        //    ddlRegion.DataValueField = "region_id";
                        //    ddlRegion.DataBind();
                        //    ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        //    if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        //    {
                        //        ddlRegion.SelectedValue = Convert.ToString(region);
                        //    }
                        //    else
                        //    {
                        //        ddlRegion.SelectedIndex = 1;
                        //    }
                        if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        {
                            ddlRegion.SelectedValue = Convert.ToString(region);
                        }
                        else
                        {
                            ddlRegion.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                        }
                    }
                    else
                    {
                        //    ddlRegion.Items.Clear();
                        //    ddlRegion.DataSource = null;
                        //    ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        if (ddlRegion.Items.FindByValue(Convert.ToString(region)) == null || region == 0)
                        {
                            objDashBOCountry.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                            dsDropDownDataCountry = objDashBL.getFilterDataBL(objDashBOCountry);
                            if (dsDropDownDataCountry.Tables.Count > 0)
                            {
                                if (dsDropDownDataCountry.Tables[1].Rows.Count > 0)
                                {
                                    ddlCountry.DataSource = dsDropDownDataCountry.Tables[1];
                                    ddlCountry.DataTextField = "country_name";
                                    ddlCountry.DataValueField = "country_id";
                                    ddlCountry.DataBind();
                                    ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                                    ddlCountry.SelectedValue = Convert.ToString(objDashBO.country_id);
                                }
                                else
                                {
                                    ddlCountry.Items.Clear();
                                    ddlCountry.DataSource = null;
                                    ddlCountry.DataBind();
                                }
                            }
                        }
                        else
                        {
                            ddlCountry.SelectedValue = Convert.ToString(objDashBO.country_id);
                        }
                        //else
                        //{
                        //    ddlRegion.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                        //}


                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Jan 2018
        /// Desc : Based on selection of location value, region, country, line will be loaded accordinglyby calling getFilterDataBL method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataSet dsDropDownDataCountry = new DataSet();
            DataSet dsDropDownDataLocation = new DataSet();
            DashboardBO objDashBOCountry = new DashboardBO();
            DashboardBO objDashBOlocation = new DashboardBO();
            int region, country;

            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion.SelectedValue);
                country = Convert.ToInt32(ddlCountry.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objDashBO.division_id = Convert.ToInt32(ddlDivision.SelectedValue);
                if (objDashBO.location_id == 0)
                {
                    objDashBO.country_id = country;
                    objDashBO.region_id = region;
                }
                //objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                //objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                //objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Location";
                // LoadFilterDropDowns(objDashBO);

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        //    ddlRegion.DataSource = dsDropDownData.Tables[0];
                        //    ddlRegion.DataTextField = "region_name";
                        //    ddlRegion.DataValueField = "region_id";
                        //    ddlRegion.DataBind();
                        //    ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        //    if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        //    {
                        //        ddlRegion.SelectedValue = Convert.ToString(region);
                        //    }
                        //    else
                        //    {
                        //        ddlRegion.SelectedIndex = 1;
                        //    }
                        if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        {
                            ddlRegion.SelectedValue = Convert.ToString(region);
                        }
                        else
                        {
                            ddlRegion.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                        }
                    }
                    //else
                    //{
                    //    ddlRegion.Items.Clear();
                    //    ddlRegion.DataSource = null;
                    //    ddlRegion.DataBind();
                    //}
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        //    ddlCountry.DataSource = dsDropDownData.Tables[1];
                        //    ddlCountry.DataTextField = "country_name";
                        //    ddlCountry.DataValueField = "country_id";
                        //    ddlCountry.DataBind();
                        //    ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        //    if (ddlCountry.Items.FindByValue(Convert.ToString(country)) != null && country != 0)
                        //    {
                        //        ddlCountry.SelectedValue = Convert.ToString(country);
                        //    }
                        //    else
                        //    {
                        //        ddlCountry.SelectedIndex = 1;
                        //    }

                        objDashBOCountry.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                        dsDropDownDataCountry = objDashBL.getFilterDataBL(objDashBOCountry);
                        if (dsDropDownDataCountry.Tables.Count > 0)
                        {
                            if (dsDropDownDataCountry.Tables[1].Rows.Count > 0)
                            {
                                ddlCountry.DataSource = dsDropDownDataCountry.Tables[1];
                                ddlCountry.DataTextField = "country_name";
                                ddlCountry.DataValueField = "country_id";
                                ddlCountry.DataBind();
                                ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                                if (ddlCountry.Items.FindByValue(Convert.ToString(country)) != null && country != 0)
                                {
                                    ddlCountry.SelectedValue = Convert.ToString(country);
                                }
                                else
                                {
                                    ddlCountry.SelectedValue = Convert.ToString(dsDropDownData.Tables[1].Rows[0]["country_id"]); ;
                                }
                            }
                            else
                            {
                                ddlCountry.Items.Clear();
                                ddlCountry.DataSource = null;
                                ddlCountry.DataBind();
                            }
                        }
                    }
                    //else
                    //{
                    //    ddlCountry.Items.Clear();
                    //    ddlCountry.DataSource = null;
                    //    ddlCountry.DataBind();
                    //}
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        //ddlLocation.DataSource = dsDropDownData.Tables[2];
                        //ddlLocation.DataTextField = "location_name";
                        //ddlLocation.DataValueField = "location_id";
                        //ddlLocation.DataBind();
                        //ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        objDashBOlocation.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                        dsDropDownDataLocation = objDashBL.getFilterDataBL(objDashBOlocation);
                        if (dsDropDownDataLocation.Tables.Count > 0)
                        {
                            if (dsDropDownDataLocation.Tables[2].Rows.Count > 0)
                            {
                                ddlLocation.DataSource = dsDropDownDataLocation.Tables[2];
                                ddlLocation.DataTextField = "location_name";
                                ddlLocation.DataValueField = "location_id";
                                ddlLocation.DataBind();
                                ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                                ddlLocation.SelectedValue = Convert.ToString(objDashBO.location_id);
                            }
                            else
                            {
                                ddlLocation.Items.Clear();
                                ddlLocation.DataSource = null;
                                ddlLocation.DataBind();
                            }

                        }



                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Jan 2018
        /// Desc : Based on selection of line value, region, country, location will be loaded accordinglyby calling getFilterDataBL method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLineName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();

                objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                if (objDashBO.line_id == 0)
                {
                    objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                    objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                    objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                }
                objDashBO.selection_flag = "Line";
                // LoadFilterDropDowns(objDashBO);

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        ddlRegion.SelectedIndex = 1;
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        ddlCountry.SelectedIndex = 1;
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        ddlLocation.SelectedIndex = 1;
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                        ddlLineName.SelectedValue = Convert.ToString(objDashBO.line_id);
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Jan 2018
        /// Desc : It calls GetPerformanceReportForDownloadBL by passing all the filter selection and load result to datatable. It calls ExporttoExcel to download the result as excel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPerform_Click(object sender, EventArgs e)
        {
            DataTable dtPerformReports;
            try
            {

                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                dtPerformReports = new DataTable();
                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);

                string selectedDate = reportrange.Text;

                if (selectedDate.Contains("/"))
                {
                    string DBPattern = "dd-MM-yyyy";
                    string UIpattern = "dd/MM/yyyy";
                    DateTime startDate;
                    DateTime endDate;
                    string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    if (DateTime.TryParseExact(Convert.ToString(splittedDates[0].TrimEnd().Trim()), UIpattern, null, DateTimeStyles.None, out startDate))
                    //if (DateTime.TryParse(splittedDates[0], out startDate))
                    {
                        string frmdate = startDate.ToString(DBPattern);
                        objDashBO.startdate = frmdate;
                        objDashBO.l_st_date = startDate;
                    }
                    if (DateTime.TryParseExact(Convert.ToString(splittedDates[1].TrimStart().Trim()), UIpattern, null, DateTimeStyles.None, out endDate))
                    //if (DateTime.TryParse(splittedDates[1], out endDate))
                    {
                        string Todate = endDate.ToString(DBPattern);
                        objDashBO.enddate = Todate;
                        objDashBO.l_end_date = endDate;
                    }
                }


                dtPerformReports = objDashBL.GetPerformanceReportForDownloadBL(objDashBO);

                if (dtPerformReports != null)
                {
                    ExporttoExcel(dtPerformReports, "PerformanceReport");

                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Jan 2018
        /// Desc : It calls GetAuditResultsForDownloadBL by passing all the filter selection and load result to datatable. It calls ExporttoExcel to download the result as excel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            DataTable dtAuditReports;
            try
            {

                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                dtAuditReports = new DataTable();
                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.division_id = Convert.ToInt32(ddlDivision.SelectedValue);
                string selectedDate = reportrange.Text;
                if (string.IsNullOrEmpty(selectedDate))
                {

                    selectedDate = String.Format("{0:dd/MM/yyyy}", DateTime.Now) + "-" + String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                }
                if (selectedDate.Contains("/"))
                {
                    string DBPattern = "dd-MM-yyyy";
                    string UIpattern = "dd/MM/yyyy";
                    DateTime startDate;
                    DateTime endDate;
                    string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    if (DateTime.TryParseExact(Convert.ToString(splittedDates[0].TrimEnd().Trim()), UIpattern, null, DateTimeStyles.None, out startDate))
                    //if (DateTime.TryParse(splittedDates[0], out startDate))
                    {
                        string frmdate = startDate.ToString(DBPattern);
                        objDashBO.startdate = frmdate;
                        objDashBO.l_st_date = startDate;
                    }
                    if (DateTime.TryParseExact(Convert.ToString(splittedDates[1].TrimStart().Trim()), UIpattern, null, DateTimeStyles.None, out endDate))
                    //if (DateTime.TryParse(splittedDates[1], out endDate))
                    {
                        string Todate = endDate.ToString(DBPattern);
                        objDashBO.enddate = Todate;
                        objDashBO.l_end_date = endDate;
                    }
                }


                dtAuditReports = objDashBL.GetAuditResultsForDownloadBL(objDashBO);

                if (dtAuditReports != null)
                {
                    dtAuditReports=ModifyDataTable(dtAuditReports);
                    ExporttoExcel(dtAuditReports, "AuditReport");
                    //if (dtAuditReports.Rows.Count > 0)
                    //{
                    //    grdAuditReports.DataSource = dtAuditReports;
                    //    grdAuditReports.DataBind();
                    //    grdAuditReports.Visible = true;
                    //}
                    //else
                    //{
                    //    grdAuditReports.DataSource = null;
                    //    grdAuditReports.DataBind();
                    //    grdAuditReports.Visible = false;
                    //}

                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Jan 2018
        /// Desc :  On click of Clear button, it clears all the dropdown selection to default by calling LoadFilterDropDowns method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                LoadFilterDropDowns(objDashBO);
                //grdAuditReports.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlDivision_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();

                objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                if (objDashBO.line_id == 0)
                {
                    objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                    objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                    objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                }
                objDashBO.selection_flag = "Division";
                // LoadFilterDropDowns(objDashBO);
                objDashBO.division_id = Convert.ToInt32(ddlDivision.SelectedValue);
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    //if (dsDropDownData.Tables[0].Rows.Count > 0)
                    //{
                    //    ddlRegion.DataSource = dsDropDownData.Tables[0];
                    //    ddlRegion.DataTextField = "region_name";
                    //    ddlRegion.DataValueField = "region_id";
                    //    ddlRegion.DataBind();
                    //    ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                    //    ddlRegion.SelectedIndex = 1;
                    //}
                    //else
                    //{
                    //    ddlRegion.Items.Clear();
                    //    ddlRegion.DataSource = null;
                    //    ddlRegion.DataBind();
                    //}
                    //if (dsDropDownData.Tables[1].Rows.Count > 0)
                    //{
                    //    ddlCountry.DataSource = dsDropDownData.Tables[1];
                    //    ddlCountry.DataTextField = "country_name";
                    //    ddlCountry.DataValueField = "country_id";
                    //    ddlCountry.DataBind();
                    //    ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                    //    ddlCountry.SelectedIndex = 1;
                    //}
                    //else
                    //{
                    //    ddlCountry.Items.Clear();
                    //    ddlCountry.DataSource = null;
                    //    ddlCountry.DataBind();
                    //}
                    //if (dsDropDownData.Tables[2].Rows.Count > 0)
                    //{
                    //    ddlLocation.DataSource = dsDropDownData.Tables[2];
                    //    ddlLocation.DataTextField = "location_name";
                    //    ddlLocation.DataValueField = "location_id";
                    //    ddlLocation.DataBind();
                    //    ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    //    ddlLocation.SelectedIndex = 1;
                    //}
                    //else
                    //{
                    //    ddlLocation.Items.Clear();
                    //    ddlLocation.DataSource = null;
                    //    ddlLocation.DataBind();
                    //}
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                        ddlLineName.SelectedValue = Convert.ToString(objDashBO.line_id);
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Jan 2018
        /// Desc : It calls getFilterDataBLfrom DashboardBL class, it binds all the dropdown on screen and be default loaded the user's location which are stored in session.
        /// </summary>
        /// <param name="objDashBO"></param>
        private void LoadFilterDropDowns(DashboardBO objDashBO)
        {
            try
            {
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                        ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                        //    ddlRegion.Attributes["disabled"] = "disabled";
                        //}
                        //else
                        //    ddlRegion.Enabled = true;
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    //if (dsDropDownData.Tables[1].Rows.Count > 0)
                    //{
                    //    ddlCountry.DataSource = dsDropDownData.Tables[1];
                    //    ddlCountry.DataTextField = "country_name";
                    //    ddlCountry.DataValueField = "country_id";
                    //    ddlCountry.DataBind();
                    //    ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                    //    //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                    //    //{
                    //        ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                    //        //ddlCountry.Attributes["disabled"] = "disabled";
                    //    //}
                    //   // else
                    //     //   ddlCountry.Enabled = true;
                    //}
                    //else
                    //{
                    //    ddlCountry.Items.Clear();
                    //    ddlCountry.DataSource = null;
                    //    ddlCountry.DataBind();
                    //}
                    //if (dsDropDownData.Tables[2].Rows.Count > 0)
                    //{
                    //    ddlLocation.DataSource = dsDropDownData.Tables[2];
                    //    ddlLocation.DataTextField = "location_name";
                    //    ddlLocation.DataValueField = "location_id";
                    //    ddlLocation.DataBind();
                    //    ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    //    //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                    //    //{
                    //        ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                    //       // ddlLocation.Attributes["disabled"] = "disabled";
                    //   // }
                    //   //// else
                    //      //  ddlLocation.Enabled = true;
                    //}
                    //else
                    //{
                    //    ddlLocation.Items.Clear();
                    //    ddlLocation.DataSource = null;
                    //    ddlLocation.DataBind();
                    //}
                    //if (dsDropDownData.Tables[3].Rows.Count > 0)
                    //{
                    //    ddlLineName.DataSource = dsDropDownData.Tables[3];
                    //    ddlLineName.DataTextField = "line_name";
                    //    ddlLineName.DataValueField = "line_id";
                    //    ddlLineName.DataBind();
                    //    ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    //    //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                    //    //{
                    //    //ddlLineName_SelectedIndexChanged(null, null);
                    //    //}
                    //}
                    //else
                    //{
                    //    ddlLineName.Items.Clear();
                    //    ddlLineName.DataSource = null;
                    //    ddlLineName.DataBind();
                    //}
                    //ddlRegion_SelectedIndexChanged(null, null);
                    //ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                    //ddlCountry_SelectedIndexChanged(null, null);
                    //ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                    //ddlLocation_SelectedIndexChanged(null, null);


                }
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                }
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                }

                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objDashBO.division_id = Convert.ToInt32(ddlDivision.SelectedValue);
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {

                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Jan 2018
        /// Desc : It converts the datatable to excel file with filename passed in parameter and download.
        /// </summary>
        /// <param name="table">datatable which is to be exported</param>
        /// <param name="filename">name of file which is to be downloaded</param>
        private void ExporttoExcel(DataTable table, string filename)
        {


            //table = dtReport;
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 12.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + filename + ".xls");

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
            //am getting my grid's column headers
            int columnscount = table.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(table.Columns[j].ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (DataRow row in table.Rows)
            {//write in new row
                HttpContext.Current.Response.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write(row[i].ToString());
                    HttpContext.Current.Response.Write("</Td>");
                }

                HttpContext.Current.Response.Write("</TR>");
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();


        }


        private DataTable ModifyDataTable(DataTable table)
        {
            DataTable dtReport = new DataTable();
            DataTable dtReportTemp = new DataTable();
            DataTable distinctValues;
            DataView view;
            DataRow[] dr;
            DataRow[] dr1;
            DataRow finalrow;
            string audit_date;
            int colcount = 0;
            try
            {

                dtReport.Columns.Add("audit_date");
                dtReport.Columns.Add("region_name");
                dtReport.Columns.Add("country_name");
                dtReport.Columns.Add("location_name");
                dtReport.Columns.Add("line_name");
                view = new DataView(table);
                string[] columnNames = { "region_name", "country_name", "location_name", "line_name" };
                distinctValues = view.ToTable(true, columnNames);

                if (distinctValues != null)
                {
                    if (distinctValues.Rows.Count > 0)
                    {
                        dr1 = table.Select("region_name='" + Convert.ToString(distinctValues.Rows[0][0]) + "' and  country_name='" + Convert.ToString(distinctValues.Rows[0][1]) + "' and location_name='" + Convert.ToString(distinctValues.Rows[0][2]) + "' and line_name='" + Convert.ToString(distinctValues.Rows[0][3]) + "'");
                        //dr1 = table.Select("line_name='" + Convert.ToString(distinctValues.Rows[0][3])+"'");
                        colcount = dr1.Length;
                        for (int i = 0; i < colcount; i++)
                        {
                            dtReport.Columns.Add("Q" + (i + 1));
                        }
                        for (int i = 0; i < distinctValues.Rows.Count; i++)
                        {
                            dtReportTemp = new DataTable();
                            dr = table.Select("region_name='" + Convert.ToString(distinctValues.Rows[i][0]) + "' and country_name='" + Convert.ToString(distinctValues.Rows[i][1]) + "' and location_name='" + Convert.ToString(distinctValues.Rows[i][2]) + "' and line_name='" + Convert.ToString(distinctValues.Rows[i][3]) + "'");
                            //dr = table.Select("line_name=" + Convert.ToString(distinctValues.Rows[i][3]));
                            dtReportTemp = table.Clone();
                            foreach (DataRow row in dr)
                            {
                                dtReportTemp.Rows.Add(row.ItemArray);
                            }
                            audit_date = Convert.ToString(dtReportTemp.Rows[0][0]);
                            dtReportTemp.Columns.Remove("audit_date");
                            dtReportTemp.Columns.Remove("region_name");
                            dtReportTemp.Columns.Remove("country_name");
                            dtReportTemp.Columns.Remove("location_name");
                            dtReportTemp.Columns.Remove("line_name");
                            dtReportTemp.AcceptChanges();

                            finalrow = dtReport.NewRow();
                            finalrow["audit_date"] = audit_date;
                            finalrow["region_name"] = Convert.ToString(distinctValues.Rows[i][0]);
                            finalrow["country_name"] = Convert.ToString(distinctValues.Rows[i][1]);
                            finalrow["location_name"] = Convert.ToString(distinctValues.Rows[i][2]);
                            finalrow["line_name"] = Convert.ToString(distinctValues.Rows[i][3]);
                            for (int j = 0; j < dtReportTemp.Rows.Count; j++)
                            {
                                finalrow["Q" + (j + 1)] = Convert.ToString(dtReportTemp.Rows[j][1]);
                            }
                            dtReport.Rows.Add(finalrow);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtReport;
        }
        private void LoadDivisionDropdown()
        {
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);

                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[8].Rows.Count > 0)
                    {
                        ddlDivision.DataSource = dsDropDownData.Tables[8];
                        ddlDivision.DataTextField = "Division_Name";
                        ddlDivision.DataValueField = "division_id";
                        ddlDivision.DataBind();
                    }
                    else
                    {
                        ddlDivision.DataSource = null;
                        ddlDivision.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        #endregion

        //private void LoadFilterDropDowns(DashboardBO objDashBO)
        //{
        //    try
        //    {
        //        objDashBL = new DashboardBL();
        //        dsDropDownData = new DataSet();
        //        dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
        //        if (dsDropDownData.Tables.Count > 0)
        //        {
        //            if (dsDropDownData.Tables[0].Rows.Count > 0)
        //            {
        //                ddlRegion.DataSource = dsDropDownData.Tables[0];
        //                ddlRegion.DataTextField = "region_name";
        //                ddlRegion.DataValueField = "region_id";
        //                ddlRegion.DataBind();
        //                ddlRegion.Items.Insert(0, new ListItem("All", "0"));
        //                ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
        //            }
        //            else
        //            {
        //                ddlRegion.Items.Clear();
        //                ddlRegion.DataSource = null;
        //                ddlRegion.DataBind();
        //            }
        //            if (dsDropDownData.Tables[1].Rows.Count > 0)
        //            {
        //                ddlCountry.DataSource = dsDropDownData.Tables[1];
        //                ddlCountry.DataTextField = "country_name";
        //                ddlCountry.DataValueField = "country_id";
        //                ddlCountry.DataBind();
        //                ddlCountry.Items.Insert(0, new ListItem("All", "0"));
        //                ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
        //            }
        //            else
        //            {
        //                ddlCountry.Items.Clear();
        //                ddlCountry.DataSource = null;
        //                ddlCountry.DataBind();
        //            }
        //            if (dsDropDownData.Tables[2].Rows.Count > 0)
        //            {
        //                ddlLocation.DataSource = dsDropDownData.Tables[2];
        //                ddlLocation.DataTextField = "location_name";
        //                ddlLocation.DataValueField = "location_id";
        //                ddlLocation.DataBind();
        //                ddlLocation.Items.Insert(0, new ListItem("All", "0"));
        //                ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
        //            }
        //            else
        //            {
        //                ddlLocation.Items.Clear();
        //                ddlLocation.DataSource = null;
        //                ddlLocation.DataBind();
        //            }
        //            if (dsDropDownData.Tables[3].Rows.Count > 0)
        //            {
        //                ddlLineName.DataSource = dsDropDownData.Tables[3];
        //                ddlLineName.DataTextField = "line_name";
        //                ddlLineName.DataValueField = "line_id";
        //                ddlLineName.DataBind();
        //                ddlLineName.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlLineName.Items.Clear();
        //                ddlLineName.DataSource = null;
        //                ddlLineName.DataBind();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}
        //protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtLineProduct = new DataTable();
        //    DataTable dtCountry = new DataTable();
        //    DataTable dtLocation = new DataTable();
        //    try
        //    {
        //        objDashBO = new DashboardBO();
        //        objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
        //        //objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
        //        //objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //        //objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
        //        objDashBO.selection_flag = "Region";
        //        // LoadFilterDropDowns(objDashBO);


        //        objDashBL = new DashboardBL();
        //        dsDropDownData = new DataSet();
        //        dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
        //        if (dsDropDownData.Tables.Count > 0)
        //        {
        //            if (dsDropDownData.Tables[0].Rows.Count > 0)
        //            {
        //                ddlRegion.DataSource = dsDropDownData.Tables[0];
        //                ddlRegion.DataTextField = "region_name";
        //                ddlRegion.DataValueField = "region_id";
        //                ddlRegion.DataBind();
        //                ddlRegion.Items.Insert(0, new ListItem("All", "0"));
        //                ddlRegion.SelectedValue = Convert.ToString(objDashBO.region_id);
        //            }
        //            else
        //            {
        //                ddlRegion.Items.Clear();
        //                ddlRegion.DataSource = null;
        //                ddlRegion.DataBind();
        //            }
        //            if (dsDropDownData.Tables[1].Rows.Count > 0)
        //            {
        //                ddlCountry.DataSource = dsDropDownData.Tables[1];
        //                ddlCountry.DataTextField = "country_name";
        //                ddlCountry.DataValueField = "country_id";
        //                ddlCountry.DataBind();
        //                ddlCountry.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlCountry.Items.Clear();
        //                ddlCountry.DataSource = null;
        //                ddlCountry.DataBind();
        //            }
        //            if (dsDropDownData.Tables[2].Rows.Count > 0)
        //            {
        //                ddlLocation.DataSource = dsDropDownData.Tables[2];
        //                ddlLocation.DataTextField = "location_name";
        //                ddlLocation.DataValueField = "location_id";
        //                ddlLocation.DataBind();
        //                ddlLocation.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlLocation.Items.Clear();
        //                ddlLocation.DataSource = null;
        //                ddlLocation.DataBind();
        //            }
        //            if (dsDropDownData.Tables[3].Rows.Count > 0)
        //            {
        //                ddlLineName.DataSource = dsDropDownData.Tables[3];
        //                ddlLineName.DataTextField = "line_name";
        //                ddlLineName.DataValueField = "line_id";
        //                ddlLineName.DataBind();
        //                ddlLineName.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlLineName.Items.Clear();
        //                ddlLineName.DataSource = null;
        //                ddlLineName.DataBind();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtLineProduct = new DataTable();
        //    DataTable dtCountry = new DataTable();
        //    DataTable dtLocation = new DataTable();
        //    int region;
        //    try
        //    {
        //        objDashBO = new DashboardBO();
        //        region = Convert.ToInt32(ddlRegion.SelectedValue);
        //        objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
        //        if (objDashBO.country_id == 0)
        //        {
        //            objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
        //        }
        //        //objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //        //objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
        //        objDashBO.selection_flag = "Country";
        //        // LoadFilterDropDowns(objDashBO);

        //        objDashBL = new DashboardBL();
        //        dsDropDownData = new DataSet();
        //        dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
        //        if (dsDropDownData.Tables.Count > 0)
        //        {
        //            if (dsDropDownData.Tables[0].Rows.Count > 0)
        //            {
        //                ddlRegion.DataSource = dsDropDownData.Tables[0];
        //                ddlRegion.DataTextField = "region_name";
        //                ddlRegion.DataValueField = "region_id";
        //                ddlRegion.DataBind();
        //                ddlRegion.Items.Insert(0, new ListItem("All", "0"));
        //                if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
        //                {
        //                    ddlRegion.SelectedValue = Convert.ToString(region);
        //                }
        //                else
        //                {
        //                    ddlRegion.SelectedIndex = 1;
        //                }
        //            }
        //            else
        //            {
        //                ddlRegion.Items.Clear();
        //                ddlRegion.DataSource = null;
        //                ddlRegion.DataBind();
        //            }
        //            if (dsDropDownData.Tables[1].Rows.Count > 0)
        //            {
        //                ddlCountry.DataSource = dsDropDownData.Tables[1];
        //                ddlCountry.DataTextField = "country_name";
        //                ddlCountry.DataValueField = "country_id";
        //                ddlCountry.DataBind();
        //                ddlCountry.Items.Insert(0, new ListItem("All", "0"));
        //                ddlCountry.SelectedValue = Convert.ToString(objDashBO.country_id);
        //            }
        //            else
        //            {
        //                ddlCountry.Items.Clear();
        //                ddlCountry.DataSource = null;
        //                ddlCountry.DataBind();
        //            }
        //            if (dsDropDownData.Tables[2].Rows.Count > 0)
        //            {
        //                ddlLocation.DataSource = dsDropDownData.Tables[2];
        //                ddlLocation.DataTextField = "location_name";
        //                ddlLocation.DataValueField = "location_id";
        //                ddlLocation.DataBind();
        //                ddlLocation.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlLocation.Items.Clear();
        //                ddlLocation.DataSource = null;
        //                ddlLocation.DataBind();
        //            }
        //            if (dsDropDownData.Tables[3].Rows.Count > 0)
        //            {
        //                ddlLineName.DataSource = dsDropDownData.Tables[3];
        //                ddlLineName.DataTextField = "line_name";
        //                ddlLineName.DataValueField = "line_id";
        //                ddlLineName.DataBind();
        //                ddlLineName.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlLineName.Items.Clear();
        //                ddlLineName.DataSource = null;
        //                ddlLineName.DataBind();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtLineProduct = new DataTable();
        //    int region, country;
        //    try
        //    {
        //        objDashBO = new DashboardBO();
        //        region = Convert.ToInt32(ddlRegion.SelectedValue);
        //        country = Convert.ToInt32(ddlCountry.SelectedValue);
        //        objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //        if (objDashBO.location_id == 0)
        //        {
        //            objDashBO.country_id = country;
        //            objDashBO.region_id = region;
        //        }
        //        //objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
        //        //objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
        //        //objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //        //objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
        //        objDashBO.selection_flag = "Location";
        //        // LoadFilterDropDowns(objDashBO);

        //        objDashBL = new DashboardBL();
        //        dsDropDownData = new DataSet();
        //        dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
        //        if (dsDropDownData.Tables.Count > 0)
        //        {
        //            if (dsDropDownData.Tables[0].Rows.Count > 0)
        //            {
        //                ddlRegion.DataSource = dsDropDownData.Tables[0];
        //                ddlRegion.DataTextField = "region_name";
        //                ddlRegion.DataValueField = "region_id";
        //                ddlRegion.DataBind();
        //                ddlRegion.Items.Insert(0, new ListItem("All", "0"));
        //                if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
        //                {
        //                    ddlRegion.SelectedValue = Convert.ToString(region);
        //                }
        //                else
        //                {
        //                    ddlRegion.SelectedIndex = 1;
        //                }
        //            }
        //            else
        //            {
        //                ddlRegion.Items.Clear();
        //                ddlRegion.DataSource = null;
        //                ddlRegion.DataBind();
        //            }
        //            if (dsDropDownData.Tables[1].Rows.Count > 0)
        //            {
        //                ddlCountry.DataSource = dsDropDownData.Tables[1];
        //                ddlCountry.DataTextField = "country_name";
        //                ddlCountry.DataValueField = "country_id";
        //                ddlCountry.DataBind();
        //                ddlCountry.Items.Insert(0, new ListItem("All", "0"));
        //                if (ddlCountry.Items.FindByValue(Convert.ToString(country)) != null && country != 0)
        //                {
        //                    ddlCountry.SelectedValue = Convert.ToString(country);
        //                }
        //                else
        //                {
        //                    ddlCountry.SelectedIndex = 1;
        //                }
        //            }
        //            else
        //            {
        //                ddlCountry.Items.Clear();
        //                ddlCountry.DataSource = null;
        //                ddlCountry.DataBind();
        //            }
        //            if (dsDropDownData.Tables[2].Rows.Count > 0)
        //            {
        //                ddlLocation.DataSource = dsDropDownData.Tables[2];
        //                ddlLocation.DataTextField = "location_name";
        //                ddlLocation.DataValueField = "location_id";
        //                ddlLocation.DataBind();
        //                ddlLocation.Items.Insert(0, new ListItem("All", "0"));
        //                ddlLocation.SelectedValue = Convert.ToString(objDashBO.location_id);
        //            }
        //            else
        //            {
        //                ddlLocation.Items.Clear();
        //                ddlLocation.DataSource = null;
        //                ddlLocation.DataBind();
        //            }
        //            if (dsDropDownData.Tables[3].Rows.Count > 0)
        //            {
        //                ddlLineName.DataSource = dsDropDownData.Tables[3];
        //                ddlLineName.DataTextField = "line_name";
        //                ddlLineName.DataValueField = "line_id";
        //                ddlLineName.DataBind();
        //                ddlLineName.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlLineName.Items.Clear();
        //                ddlLineName.DataSource = null;
        //                ddlLineName.DataBind();
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlLineName_SelectedIndexChanged()
        //{
        //    try
        //    {
        //        objDashBO = new DashboardBO();

        //        objDashBO.line_id = 0;
        //        if (objDashBO.line_id == 0)
        //        {
        //            objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
        //            objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
        //            objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //        }
        //        objDashBO.selection_flag = "Line";
        //        // LoadFilterDropDowns(objDashBO);

        //        objDashBL = new DashboardBL();
        //        dsDropDownData = new DataSet();
        //        dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
        //        if (dsDropDownData.Tables.Count > 0)
        //        {

        //            if (dsDropDownData.Tables[3].Rows.Count > 0)
        //            {
        //                ddlLineName.DataSource = dsDropDownData.Tables[3];
        //                ddlLineName.DataTextField = "line_name";
        //                ddlLineName.DataValueField = "line_id";
        //                ddlLineName.DataBind();
        //                ddlLineName.Items.Insert(0, new ListItem("All", "0"));
        //                ddlLineName.SelectedValue = Convert.ToString(objDashBO.line_id);
        //            }
        //            else
        //            {
        //                ddlLineName.Items.Clear();
        //                ddlLineName.DataSource = null;
        //                ddlLineName.DataBind();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }

        //}

    }
}