﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mail;
using MH_Logistics_BusinessLogic;
using MH_Logistics_BusinessObject;

namespace MH_Logistics
{
    public partial class Login : System.Web.UI.Page
    {
        #region GlobalDeclaration
        UsersBO objUserBO;
        CommonBL objComBL;
        UsersBL objUsersBL;
        CommonFunctions objCom = new CommonFunctions();
        LoginAuthenticateBL objAuth = new LoginAuthenticateBL();
        LoginBO objLogin = new LoginBO();
        EmailDetails objEmail;
        #endregion

        #region Events

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 07 Sept 2017
        /// Desc : On loading the page of login, it should clear all the session value as this page will be loaded after logout also.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string ipaddress = string.Empty;
            ipaddress = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (ipaddress == "" || ipaddress == null)
                ipaddress = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

            if (!IsPostBack)
            {
                Session["UserId"] = "";
                Session["UserFullName"] = "";
                Session["DisplayNameFlag"] = "";
                Session["EmailId"] = "";
                Session["LocationId"] = "";
                Session["Role"] = "";
                Session["GlobalAdminFlag"] = "";
                Session["SiteAdminFlag"] = "";
                Session["Location"] = "";
            }
        }

        /// <summary>
        /// Author :Anamika(KNS)
        /// Date : 07 Sept 2017
        /// Desc : On click of login button, it will validate the user credential with database entry and it will set all the required session for further use.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            DataTable dtLoginInfo = new DataTable();
            try
            {
                objLogin.username = Convert.ToString(txtusername.Text);
                    objLogin.encryptedPassword = objAuth.Encrypt(Convert.ToString(objAuth.MD5Encrypt(Convert.ToString(txtpassword.Text))));
                Session["LoggedInPassword"] = Convert.ToString(txtpassword.Text);
                dtLoginInfo = objAuth.authLoginBL(objLogin);
                if (dtLoginInfo != null)
                {
                    if (dtLoginInfo.Rows.Count > 0)
                    {
                        Session["UserId"] = Convert.ToString(dtLoginInfo.Rows[0]["user_id"]);
                        Session["UserFullName"] = Convert.ToString(dtLoginInfo.Rows[0]["emp_full_name"]);
                        Session["DisplayNameFlag"] = Convert.ToString(dtLoginInfo.Rows[0]["display_name_flag"]);
                        Session["EmailId"] = Convert.ToString(dtLoginInfo.Rows[0]["email_id"]);
                        Session["LocationId"] = Convert.ToString(dtLoginInfo.Rows[0]["location_id"]);
                        Session["Role"] = Convert.ToString(dtLoginInfo.Rows[0]["role_id"]);
                        Session["GlobalAdminFlag"] = Convert.ToString(dtLoginInfo.Rows[0]["global_admin_flag"]);
                        Session["SiteAdminFlag"] = Convert.ToString(dtLoginInfo.Rows[0]["site_admin_flag"]);
                        Session["Location"] = Convert.ToString(dtLoginInfo.Rows[0]["location_name"]);
                        Session["LoggedInRegionId"] = Convert.ToString(dtLoginInfo.Rows[0]["region_id"]);
                        Session["LoggedInCountryId"] = Convert.ToString(dtLoginInfo.Rows[0]["country_id"]);
                        Session["Username"] = Convert.ToString(txtusername.Text);
                        Response.Redirect("Planning.aspx");
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('User Credential is invalid');", true);
                    }

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('User Credential is invalid');", true);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 07 Sept 2017
        /// Desc :On click of request new password link, it will generate random password, save to database and then send a mail to 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkRequest_Click(object sender, EventArgs e)
        {
            string result = string.Empty;
            CommonBL objComBL = new CommonBL();
            try
            {
                objUserBO = new UsersBO();
                objAuth = new LoginAuthenticateBL();
                objUsersBL = new UsersBL();
                objUserBO.UserName = Convert.ToString(txtusername.Text);
                //to create a random password
                string password = CreateRandomPassword(8);
                objUserBO.Password = objAuth.Encrypt(Convert.ToString(objAuth.MD5Encrypt(password)));
                // save the encrypted password to database
                string output = objUsersBL.ChangePasswordBL(objUserBO);
                if (!string.IsNullOrEmpty(output))
                {
                    //
                    SendMail(password, output);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Password has been rest and sent to your mail ID.');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('There is some error in reseting password. Please enter valid username and try again.');", true);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Author :Anamika(KNS)
        /// Date : 07 Sept 2017
        /// Desc : This function is called for sending mail to user with new password
        /// </summary>
        /// <param name="password">auto generated password</param>
        /// <param name="emailTo">user's mail id</param>
        private void SendMail(string password, string emailTo)
        {
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            try
            {
                objEmail = new EmailDetails();
                objEmail.body = "Password has been reset for your account. <br/><br/>New Password: " + password;
                objEmail.toMailId = emailTo;
                objEmail.subject = "LPA : Password Change";//Subject for your request

                objCom.SendMail(objEmail);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Author :Anamika(KNS)
        /// Date : 07 Sept 2017
        /// Desc : This function generates the new password with allowed characters and length of passord  is  passed in parameter
        /// </summary>
        /// <param name="PasswordLength">Length of parameter</param>
        /// <returns></returns>
        public static string CreateRandomPassword(int PasswordLength)
        {
            string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }

        #endregion
    }

}
