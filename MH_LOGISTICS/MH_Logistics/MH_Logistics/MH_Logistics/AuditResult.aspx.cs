﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Text;
using System.Configuration;
using System.Web.UI.DataVisualization.Charting;
//using Microsoft.ReportingServices;
using System.Net;
using iTextSharp.tool.xml;

using MH_Logistics_BusinessLogic;
using MH_Logistics_BusinessObject;

namespace MH_Logistics
{
    public partial class AuditResult : System.Web.UI.Page
    {
        #region Global Declaration
        CommonFunctions objComm = new CommonFunctions();
        PlanBO objPlanBO;
        PlanBL objPlanBL;
        LoginAuthenticateBL objAuth = new LoginAuthenticateBL();
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                objPlanBO = new PlanBO();
                DataSet dsImage = new DataSet();
                DataSet dsAnswers = new DataSet();
                string html = string.Empty;
                string filename = string.Empty;
                string filepath = string.Empty;

                string Location = string.Empty;
                string Line = string.Empty;
                string mailIds = string.Empty;
                int audit_id = 0;
                try
                {
                    if (Session["objPlanBO"] != null)
                    {
                        objPlanBO = (PlanBO)Session["objPlanBO"];

                    }
                    mailIds = objPlanBO.mailIds;
                    objPlanBL = new PlanBL();
                    dsAnswers = objPlanBL.GetAnswers(objPlanBO.p_audit_id);
                    audit_id = objPlanBO.p_audit_id;
                    callAPI(audit_id);
                    Bindchart(audit_id);
                        filename = "Audit_Result_" + Convert.ToString(audit_id) + ".pdf";
                        filepath = ConfigurationManager.AppSettings["PDF_Folder"].ToString();
                        Session["filename"] = Convert.ToString(filepath + filename);
                    //    
                    ////dsAnswers = objPlanBL.GetAnswers(102);
                    //if (dsAnswers != null)
                    //{
                    //    dsImage = dsAnswers.Copy();
                    //    html = "<html ><head></head><body>";
                    //    //html += heading();
                    //    //html += coverpage();
                    //    for (int i = 0; i < dsAnswers.Tables.Count; i++)
                    //    {
                    //        if (i == 0)
                    //        {
                    //            // html += firstpagedetails(dsAnswers.Tables[0]);
                    //            html += newheading(dsAnswers.Tables[0]);
                    //            html += questionheading();
                    //            Location = Convert.ToString(dsAnswers.Tables[0].Rows[0]["location_name"]);
                    //            Line = Convert.ToString(dsAnswers.Tables[0].Rows[0]["line_name"]);
                    //        }
                    //        else
                    //        {
                    //            dsAnswers.Tables[i].Columns.Remove("image_file_name");
                    //            //dsAnswers.Tables[i].Columns.Remove("display_sequence");
                    //            //dsAnswers.Tables[i].Columns.Remove("region_name");
                    //            //dsAnswers.Tables[i].Columns.Remove("country_name");
                    //            //dsAnswers.Tables[i].Columns.Remove("location_name");
                    //            //dsAnswers.Tables[i].Columns.Remove("line_name");
                    //            //dsAnswers.Tables[i].Columns.Remove("product_name");

                    //            html += ExportDatatableToHtml(dsAnswers.Tables[i]);
                    //        }
                    //    }
                    //    html += getLastRow(dsAnswers.Tables[0]);

                    //    StringBuilder strHTMLBuilder = new StringBuilder();
                    //    strHTMLBuilder.Append("</table>");
                    //    html += strHTMLBuilder.ToString();
                        
                    //    strHTMLBuilder = new StringBuilder();
                    //    strHTMLBuilder.Append("<div style='height:60px;'></div>");
                    //    html += strHTMLBuilder.ToString();
                    //    html += newheading(dsAnswers.Tables[0]);
                    //    filename = "Audit_Result_" + Convert.ToString(dsAnswers.Tables[0].Rows[0]["Document_no"]) + ".pdf";
                    //    filepath = ConfigurationManager.AppSettings["PDF_Folder"].ToString();
                    //    Bindchart(audit_id);
                    //    html += BindchartData();
                    //    html += "</body></html>";
                    //    convertPDF(html, filepath, filename, dsImage);


                    //    Session["filename"] = Convert.ToString(filepath + filename);
                    //    if (!string.IsNullOrEmpty(mailIds))
                    //    {
                    //        EmailDetails objEmail = new EmailDetails();
                    //        objEmail.toMailId = mailIds;
                    //        objEmail.subject = "LPA Audit Report for " + Location + "-" + Line;
                    //        objEmail.body = "Layered Process Audit for " + Location + "-" + Line + " is completed and audit report is attached.  This audit report was also sent to Central Processing Team.";
                    //        objEmail.attachment = Convert.ToString(filepath + filename);
                    //        objComm.SendMail(objEmail);
                    //    }
                    //}
                    ////ShowReport();
                }
                catch (Exception ex)
                {
                    objComm.ErrorLog(ex);
                }
            }
        }

        protected void btnAudit_Click(object sender, EventArgs e)
        {
            Response.Redirect("Dashboard.aspx");
        }

        private void callAPI(int audit_id)
        {
            string ipaddress=string.Empty;
            try
            {
                string ReportGenerateServiceURL = Convert.ToString(ConfigurationManager.AppSettings["ReportGenerateServiceURL"]);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ReportGenerateServiceURL);
                request.Method = "POST";
                //SampleModel model = new SampleModel();
                //model.PostData = "Test";
                request.ContentType = "application/json";
               
                ipaddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (ipaddress == "" || ipaddress == null)
                    ipaddress = Request.ServerVariables["REMOTE_ADDR"];
                //JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (var sw = new StreamWriter(request.GetRequestStream()))
                {
                    //string json = serializer.Serialize(model);
                    string json = "{\"userID\":\"" + Convert.ToString(Session["UserId"]) + "\",\"password\":\"" + Convert.ToString(objAuth.MD5Encrypt(Convert.ToString(Session["LoggedInPassword"]))) + "\",\"deviceID\":\"\",\"username\":\"" + Convert.ToString(Session["Username"]) + "\",\"audit_id\": " + audit_id + "}";
                    sw.Write(json);
                    sw.Flush();
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.ClearContent();
                response.Clear();
                response.ContentType = "text/plain";
                response.AddHeader("Content-Disposition",
                                   "attachment; filename=" + Convert.ToString(Session["filename"]) + ";");
                response.TransmitFile(Convert.ToString(Session["filename"]));
                response.Flush();
                response.End();
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods

        protected string ExportDatatableToHtml(DataTable dt)
        {
            string Heading = Convert.ToString(dt.Rows[0]["section_name"]);
            StringBuilder strHTMLBuilder = new StringBuilder();
            string Htmltext = string.Empty;
            try
            {

                //strHTMLBuilder.Append("<table cellspacing='0' cellpadding='0' style='border:1px black solid; border-collapse: collapse; border-spacing:0;'>");
                strHTMLBuilder.Append("<tr height='15px' style='font-family:Gotham-Book; font-size:12px; text-align:center; background-color:#048347; color:white; border-collapse: collapse;'>");
                strHTMLBuilder.Append("<td></td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append(Heading);
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td></td>");
                strHTMLBuilder.Append("<td></td>");
                strHTMLBuilder.Append("<td></td>");
                strHTMLBuilder.Append("<td></td>");
                strHTMLBuilder.Append("<td></td>");
                strHTMLBuilder.Append("<td></td>");
                strHTMLBuilder.Append("</tr>");

                foreach (DataRow myRow in dt.Rows)
                {

                    strHTMLBuilder.Append("<tr style='background-color:white; height:30px'>");
                    foreach (DataColumn myColumn in dt.Columns)
                    {
                        if (myColumn.ColumnName.ToString() == "display_sequence")
                        {
                            strHTMLBuilder.Append("<td width='50px' style='border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:9px;'>");
                            strHTMLBuilder.Append(myRow[myColumn.ColumnName].ToString());
                            strHTMLBuilder.Append("</td>");
                        }

                        else if (myColumn.ColumnName.ToString() == "question")
                        {
                            strHTMLBuilder.Append("<td width='570px'  style='border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:9px;'>");
                            strHTMLBuilder.Append(myRow[myColumn.ColumnName].ToString());
                            strHTMLBuilder.Append("</td>");
                        }
                        else if (myColumn.ColumnName.ToString() == "answer")
                        {
                            if (myRow[myColumn.ColumnName].ToString() == "1")
                            {
                                strHTMLBuilder.Append("<td width='25px' style='background-color:red;  border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:9px;'>");
                                strHTMLBuilder.Append("&#10008;");
                                strHTMLBuilder.Append("</td>");
                                strHTMLBuilder.Append("<td width='25px' style='border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:9px;'>");
                                //strHTMLBuilder.Append("Yes");
                                strHTMLBuilder.Append("</td>");
                                if (myRow["na_flag"].ToString() == "Y")
                                {
                                    strHTMLBuilder.Append("<td width='25px' style='border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:9px;'>");
                                    // strHTMLBuilder.Append("Not Applicable");
                                    strHTMLBuilder.Append("</td>");
                                }
                                else
                                {
                                    strHTMLBuilder.Append("<td width='25px'>");
                                    // strHTMLBuilder.Append("Not Applicable");
                                    strHTMLBuilder.Append("</td>");
                                }
                            }
                            else if (myRow[myColumn.ColumnName].ToString() == "0")
                            {
                                strHTMLBuilder.Append("<td width='25px' style=' border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:9px;'>");
                                //strHTMLBuilder.Append("&#10004;");
                                strHTMLBuilder.Append("</td>");
                                strHTMLBuilder.Append("<td width='25px' style=' background-color:#048347; border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:9px;'>");
                                strHTMLBuilder.Append("&#10004;");
                                strHTMLBuilder.Append("</td>");
                                if (myRow["na_flag"].ToString() == "Y")
                                {
                                    strHTMLBuilder.Append("<td width='25px' style=' border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:9px;'>");
                                    //strHTMLBuilder.Append("Not Applicable");
                                    strHTMLBuilder.Append("</td>");
                                }
                                else
                                {
                                    strHTMLBuilder.Append("<td width='25px'>");
                                    //strHTMLBuilder.Append("Not Applicable");
                                    strHTMLBuilder.Append("</td>");
                                }
                            }
                            else
                            {
                                strHTMLBuilder.Append("<td width='25px' style=' border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:9px;'>");
                                //strHTMLBuilder.Append("&#10004;");
                                strHTMLBuilder.Append("</td>");
                                strHTMLBuilder.Append("<td width='25px' style=' border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:9px;'>");
                                //strHTMLBuilder.Append("Yes");
                                strHTMLBuilder.Append("</td>");
                                if (myRow["na_flag"].ToString() == "Y")
                                {
                                    strHTMLBuilder.Append("<td width='25px' style='background-color:gray; border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:9px;'>");
                                    strHTMLBuilder.Append("&#10004;");
                                    strHTMLBuilder.Append("</td>");
                                }
                                else
                                {
                                    strHTMLBuilder.Append("<td width='25px'>");
                                    strHTMLBuilder.Append("&#10004;");
                                    strHTMLBuilder.Append("</td>");
                                }
                            }

                        }
                        else if (myColumn.ColumnName.ToString() == "remarks")
                        {
                            strHTMLBuilder.Append("<td width='150px'  style='font-family:Gotham-Book; font-size:9px;'>");
                            strHTMLBuilder.Append(myRow[myColumn.ColumnName]);
                            strHTMLBuilder.Append("</td>");
                        }
                    }
                    strHTMLBuilder.Append("<td width='70px' style=' border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:9px;'>");
                    strHTMLBuilder.Append("");
                    strHTMLBuilder.Append("</td>");
                    strHTMLBuilder.Append("<td width='70px' style=' border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:9px;'>");
                    strHTMLBuilder.Append("");
                    strHTMLBuilder.Append("</td>");
                    strHTMLBuilder.Append("</tr>");
                }

                //Close tags.  
                //strHTMLBuilder.Append("</table>");

                //strHTMLBuilder.Append("</html>");

                Htmltext = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }
            return Htmltext;
        }

        protected string ExportSectionToHTML(DataTable dt)
        {
            StringBuilder strHTMLBuilder = new StringBuilder();
            string Htmltext = string.Empty;
            try
            {
                strHTMLBuilder.Append("<table >");
                foreach (DataRow myRow in dt.Rows)
                {

                    strHTMLBuilder.Append("<tr >");

                    strHTMLBuilder.Append("<td>");
                    strHTMLBuilder.Append(myRow["section_abbr"].ToString() + " - " + myRow["section_name"].ToString());
                    strHTMLBuilder.Append("</td>");

                    strHTMLBuilder.Append("</tr>");
                }

                //Close tags.  
                strHTMLBuilder.Append("</table>");
                strHTMLBuilder.Append("<div style='height:30px;'></div>");
                Htmltext = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }
            return Htmltext;
        }

        protected void convertPDF(string example_html, string filepath, string filename, DataSet dsImage)
        {
            try
            {
                 using (FileStream fs = new FileStream(Path.Combine(filepath,"test.htm"), FileMode.Create))
                {
                    using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                    {
                        w.WriteLine(example_html);
                    }
                }

                GeneratePdfFromHtml(filepath, filename,dsImage);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }
            //Byte[] bytes;
            //StringReader srHtml = null;
            //DataTable dtChart = new DataTable();
            //string imageURL;
            //Paragraph paragraph;
            //try
            //{
            //    var ms = new MemoryStream();
            //    var doc = new Document();
            //    var writer = PdfWriter.GetInstance(doc, ms);
            //    doc.Open();

            //    //imageURL = Convert.ToString(ConfigurationManager.AppSettings["MHLogo"]);
            //    //iTextSharp.text.Image imgLogo = iTextSharp.text.Image.GetInstance(imageURL);
            //    imageURL = string.Empty;
            //    //img.ScalePercent(75f);
            //    //imgLogo.SetAbsolutePosition((PageSize.A4.Width - imgLogo.ScaledWidth) / 2, imgLogo.ScaledHeight);
            //    //imgLogo.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
            //    //doc.Add(imgLogo);

            //    srHtml = new StringReader(example_html);
            //    iTextSharp.text.FontFactory.Register(ConfigurationManager.AppSettings["fontpath"].ToString(), "Gotham-Book");
            //    iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, srHtml);


            //    //imageURL = Convert.ToString(ConfigurationManager.AppSettings["ChartImageFile"]);
            //    //iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(imageURL);
            //    //img.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
            //    //imageURL = string.Empty;
            //    //img.ScalePercent(75f);
            //    //doc.Add(img);

            //    //if(Session["ChartData"]!=null)
            //    //{
            //    //    dtChart=(DataTable)Session["ChartData"];
            //    //    if (dtChart != null)
            //    //    {
            //    //        if (dtChart.Rows.Count > 0)
            //    //        {
            //    //            srHtml = new StringReader(ExportSectionToHTML(dtChart));
            //    //            iTextSharp.text.FontFactory.Register(ConfigurationManager.AppSettings["fontpath"].ToString(), "Gotham-Book");
            //    //            iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, srHtml);
            //    //        }
            //    //    }
            //    //}


            //    if (dsImage != null)
            //    {
            //        for (int j = 1; j < dsImage.Tables.Count; j++)
            //        {
            //            if (dsImage.Tables[j].Rows.Count > 0)
            //            {
            //                for (int i = 0; i < dsImage.Tables[j].Rows.Count; i++)
            //                {
            //                    if (!string.IsNullOrEmpty(Convert.ToString(dsImage.Tables[j].Rows[i]["image_file_name"])))
            //                    {
            //                        paragraph = new Paragraph(Convert.ToString("Question : " + Convert.ToString(dsImage.Tables[j].Rows[i]["display_sequence"])));
            //                        // imageURL = ConfigurationManager.AppSettings["ImageURL"].ToString() + "/Image_" + Convert.ToString(dsImage.Tables[j].Rows[i]["image_file_name"]) + ".Jpeg";
            //                        imageURL = Convert.ToString(dsImage.Tables[j].Rows[i]["image_file_name"]);
            //                        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(imageURL);
            //                        jpg.ScaleToFit(140f, 120f);
            //                        jpg.SpacingBefore = 10f;
            //                        jpg.SpacingAfter = 1f;
            //                        jpg.Alignment = Element.ALIGN_LEFT;
            //                        doc.Add(paragraph);
            //                        doc.Add(jpg);
            //                    }

            //                }
            //            }
            //        }
            //    }
            //    doc.Close();
            //    bytes = ms.ToArray();
            //    var testFile = Path.Combine(filepath, filename);
            //    System.IO.File.WriteAllBytes(testFile, bytes);
            //}
            //catch (Exception ex)
            //{
            //    objComm.ErrorLog(ex);
            //}
        }

        public void GeneratePdfFromHtml(string filepath, string filename, DataSet dsImage)
        {
            string outputFilename = Path.Combine(filepath, filename);
            string inputFilename = Path.Combine(filepath, "test.htm");

            using (var input = new FileStream(inputFilename, FileMode.Open))
            using (var output = new FileStream(outputFilename, FileMode.Create))
            {
                CreatePdf(filepath, filename, input, output, dsImage);
            }
        }

        public void CreatePdf(string filepath, string filename, Stream htmlInput, Stream pdfOutput, DataSet dsImage)
        {
            string[] images;
            string imageURL;
            Paragraph paragraph;
            using (var document = new Document(PageSize.A4, 30, 30, 30, 30))
            {
                var writer = PdfWriter.GetInstance(document, pdfOutput);
                var worker = XMLWorkerHelper.GetInstance();

                document.Open();
                worker.ParseXHtml(writer, document, htmlInput, null, Encoding.UTF8, new UnicodeFontFactory());
                if (dsImage != null)
                {
                    for (int j = 1; j < dsImage.Tables.Count; j++)
                    {
                        if (dsImage.Tables[j].Rows.Count > 0)
                        {
                            for (int i = 0; i < dsImage.Tables[j].Rows.Count; i++)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(dsImage.Tables[j].Rows[i]["image_file_name"])))
                                {
                                    imageURL = string.Empty;
                                    paragraph = new Paragraph(Convert.ToString("Question : " + Convert.ToString(dsImage.Tables[j].Rows[i]["display_sequence"])));
                                    images = Convert.ToString(dsImage.Tables[j].Rows[i]["image_file_name"]).Split(';');
                                    //imageURL = Convert.ToString(dsImage.Tables[j].Rows[i]["image_file_name"]);
                                    document.Add(paragraph);
                                    for (int k = 0; k < images.Length; k++)
                                    {
                                        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(images[k]);
                                        jpg.ScaleToFit(140f, 120f);
                                        jpg.SpacingBefore = 10f;
                                        jpg.SpacingAfter = 1f;
                                        jpg.Alignment = Element.ALIGN_LEFT;
                                        
                                        document.Add(jpg);
                                    }
                                }

                            }
                        }
                    }
                }
                document.Close();
            }
        }

        public class UnicodeFontFactory : FontFactoryImp
        {
            private static readonly string FontPath = ConfigurationManager.AppSettings["fontpath"].ToString();

            private readonly BaseFont _baseFont;

            public UnicodeFontFactory()
            {
                _baseFont = BaseFont.CreateFont(FontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            }

            public override Font GetFont(string fontname, string encoding, bool embedded, float size, int style, BaseColor color,
              bool cached)
            {
                return new Font(_baseFont, size, style, color);
            }
        }

        private static string AddCommentsToFile(string fileName, string userComments)
        {
            string outputFileName = Path.GetTempFileName();
            //Step 1: Create a Docuement-Object
            Document document = new Document();
            try
            {
                //Step 2: we create a writer that listens to the document
                PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(outputFileName, FileMode.Create));

                //Step 3: Open the document
                document.Open();

                PdfContentByte cb = writer.DirectContent;

                //The current file path
                string filename = fileName;

                // we create a reader for the document
                PdfReader reader = new PdfReader(filename);

                for (int pageNumber = 1; pageNumber < reader.NumberOfPages + 1; pageNumber++)
                {
                    document.SetPageSize(reader.GetPageSizeWithRotation(1));
                    document.NewPage();

                    //Insert to Destination on the first page
                    if (pageNumber == 1)
                    {
                        Chunk fileRef = new Chunk(" ");
                        fileRef.SetLocalDestination(filename);
                        document.Add(fileRef);
                    }

                    PdfImportedPage page = writer.GetImportedPage(reader, pageNumber);
                    int rotation = reader.GetPageRotation(pageNumber);
                    if (rotation == 90 || rotation == 270)
                    {
                        cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader.GetPageSizeWithRotation(pageNumber).Height);
                    }
                    else
                    {
                        cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                    }
                }

                // Add a new page to the pdf file
                document.NewPage();

                Paragraph paragraph = new Paragraph();
                Font titleFont = new Font(iTextSharp.text.Font.FontFamily.HELVETICA
                                          , 15
                                          , iTextSharp.text.Font.BOLD
                                          , BaseColor.BLACK
                    );
                Chunk titleChunk = new Chunk("Comments", titleFont);
                paragraph.Add(titleChunk);
                document.Add(paragraph);

                paragraph = new Paragraph();
                Font textFont = new Font(iTextSharp.text.Font.FontFamily.HELVETICA
                                         , 12
                                         , iTextSharp.text.Font.NORMAL
                                         , BaseColor.BLACK
                    );
                Chunk textChunk = new Chunk(userComments, textFont);
                paragraph.Add(textChunk);

                document.Add(paragraph);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                document.Close();
            }
            return outputFileName;
        }

        private string coverpage()
        {
            string output = string.Empty;
            try
            {
                StringBuilder strHTMLBuilder = new StringBuilder();
                strHTMLBuilder.Append("<table  style='font-family:Gotham-Book; margin-top: 30%; margin-left: 1%; margin-bottom: 100px; width:1000px;'>");
                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td style='text-align:center; font-size:45px; font-weight:bold'>");
                strHTMLBuilder.Append("LPA");
                strHTMLBuilder.Append("</td></tr ><tr><td style='text-align:center; font-size:25px; font-weight:bold'>");
                strHTMLBuilder.Append("conducted for");
                strHTMLBuilder.Append("</td></tr ><tr><td style='text-align:center; font-size:45px; font-weight:bold'>");
                strHTMLBuilder.Append("MHIN");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</table>");
                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }
            return output;
        }

        private string heading()
        {
            string output = string.Empty;
            StringBuilder strHTMLBuilder = new StringBuilder();
            string imageURL = string.Empty;
            try
            {
                imageURL = Convert.ToString(ConfigurationManager.AppSettings["MHLogo"]);
                strHTMLBuilder.Append("<table>");
                strHTMLBuilder.Append("<tr ><td width='800px'></td>");
                strHTMLBuilder.Append("<td width='200px'><img style='float:left; width:110px;' src='");
                strHTMLBuilder.Append(imageURL);
                strHTMLBuilder.Append("'/>");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</table>");
                strHTMLBuilder.Append("<table border='1px' cellspacing='0' style='font-family:Gotham-Book; font-size:12px; font-weight:bold'>");

                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td width='1000px' style='text-align:center; background-color:#048347; color:white;'> LAYERED PROCESS AUDIT REPORT</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</table>");
                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }
            return output;
        }

        private string firstpagedetails(DataTable dt)
        {
            string output = string.Empty;
            try
            {
                StringBuilder strHTMLBuilder = new StringBuilder();
                strHTMLBuilder.Append("<table border='1px' cellspacing='0'  style='font-family:Gotham-Book; font-size:12px; border-collapse: inherit;  border-spacing:0;'>");
                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td width='250px' style='font-weight:bold;'>");
                strHTMLBuilder.Append("Name of the auditor");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='250px'>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["conducted_by"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append("");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='250px' style='font-weight:bold;'>");
                strHTMLBuilder.Append("Date");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='250px'>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["conducted_on"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append("");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("<tr><td></td></tr>");
                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td style='font-weight:bold;'>");
                strHTMLBuilder.Append("Line Name");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["line_name"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append("");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='font-weight:bold;'>");
                strHTMLBuilder.Append("Part Number");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["product_code"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append("");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("<tr><td></td></tr>");
                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td style='font-weight:bold;'>");
                strHTMLBuilder.Append("Location");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["location_name"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append("");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='font-weight:bold;'>");
                strHTMLBuilder.Append("Score");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["Score"]) + "%");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td>");
                strHTMLBuilder.Append("");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");


                strHTMLBuilder.Append("</table>");

                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }
            return output;
        }

        private string newheading(DataTable dt)
        {
            string output = string.Empty;
            StringBuilder strHTMLBuilder = new StringBuilder();
            string imageURL = string.Empty;
            try
            {
                imageURL = Convert.ToString(ConfigurationManager.AppSettings["MHLogo"]);
                strHTMLBuilder.Append("<table cellspacing='0' style='border: 1px solid darkgray; font-family:Gotham-Book; border-collapse:collapse;'>");

                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td colspan='5' width='850px' style='text-align:center; font-size:14px; font-weight:bold; color:black;'> Layered Process Audit (LPA)</td>");
                strHTMLBuilder.Append("<td width='150px'><img style='float:left; width:80px;' src='");
                strHTMLBuilder.Append(imageURL);
                strHTMLBuilder.Append("'/>");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                //strHTMLBuilder.Append("</table>");
                //strHTMLBuilder.Append("<table style='border: 1px solid gray; border-collapse:collapse; border-spacing:0;'>");
                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td width='150px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append("0.1)Plant/Line<br/>(name/no.)");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='200px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["line_name"]) + "(" + Convert.ToString(dt.Rows[0]["location_name"]) + ")");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='100px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append("0.3)Name:");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='250px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["conducted_by"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='150px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append("0.5)Personal No.:");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='150px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["username"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");

                //  strHTMLBuilder.Append("<tr><td></td></tr>");

                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append("0.2) Current product<br/>/part-no.");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["product_code"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append("0.4)Date:");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["conducted_on"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append("0.6)Shift:");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["no_of_shifts"]));
                strHTMLBuilder.Append("</td>");

                strHTMLBuilder.Append("</tr>");

                strHTMLBuilder.Append("</table>");

                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }
            return output;
        }

        private string BindchartData()
        {
            string output = string.Empty;
            StringBuilder strHTMLBuilder = new StringBuilder();
            DataTable dtChart = new DataTable();
            string imageURL = string.Empty;
            try
            {
                imageURL = Convert.ToString(ConfigurationManager.AppSettings["ChartImageFile"]);
                dtChart = (DataTable)Session["ChartData"];
                
                strHTMLBuilder.Append("<table cellspacing='0'>");

                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td width='350px'></td>");
                strHTMLBuilder.Append("<td width='800px'><img style='float:right; width:300px;' src='");
                strHTMLBuilder.Append(imageURL);
                strHTMLBuilder.Append("'/>");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("</table>");
                //strHTMLBuilder.Append("<tr >");
               // strHTMLBuilder.Append("<td width='1000px' style='font-family:Gotham-Book; font-size:10px; text-align:left;'>");
                strHTMLBuilder.Append("<table cellspacing='0' style='font-family:Gotham-Book; font-size:10px; border: 1px solid gray;'>");
                int i = 0;
                foreach (DataRow myRow in dtChart.Rows)
                {
                    if(i%2==0)
                        strHTMLBuilder.Append("<tr >");

                    strHTMLBuilder.Append("<td width='100px' style=' border: 1px solid gray; border-collapse: collapse;'> ");
                    strHTMLBuilder.Append(Convert.ToString(myRow["section_abbr"]));
                    strHTMLBuilder.Append("</td>");
                    strHTMLBuilder.Append("<td width='400px' style=' border: 1px solid gray; border-collapse: collapse;'>");
                    strHTMLBuilder.Append(Convert.ToString(myRow["section_name"]));
                    strHTMLBuilder.Append("</td>");
                    if (i % 2 != 0)
                        strHTMLBuilder.Append("</tr>");
                    i++;
                }
                strHTMLBuilder.Append("</table>");
                //strHTMLBuilder.Append("</td>");
                //strHTMLBuilder.Append("</tr>");
                //strHTMLBuilder.Append("</table>");

                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }
            return output;
        }

        private void Bindchart(int audit_id)
        {
            objPlanBO = new PlanBO();
            objPlanBL = new PlanBL();
            DataSet ds = new DataSet();
            try
            {
                //objPlanBO.p_line_id = Convert.ToInt32(Session["chart_line"]);
                //objPlanBO.location_id = Convert.ToInt32(Session["chart_location"]);
                //objPlanBO.region_id = Convert.ToInt32(Session["chart_region"]);
                //objPlanBO.country_id = Convert.ToInt32(Session["chart_country"]);
                objPlanBO.p_audit_id = audit_id;
                ds = objPlanBL.GetAuditScore(objPlanBO);
                DataTable ChartData = ds.Tables[0];
                Session["ChartData"] = ChartData;
                if (ChartData.Rows.Count > 0)
                {
                    sec1.Text = Convert.ToString(ChartData.Rows[0]["section_abbr"]) + " - " + Convert.ToString(ChartData.Rows[0]["section_name"]);
                    sec2.Text = Convert.ToString(ChartData.Rows[1]["section_abbr"]) + " - " + Convert.ToString(ChartData.Rows[1]["section_name"]);
                    sec3.Text = Convert.ToString(ChartData.Rows[2]["section_abbr"]) + " - " + Convert.ToString(ChartData.Rows[2]["section_name"]);
                    sec4.Text = Convert.ToString(ChartData.Rows[3]["section_abbr"]) + " - " + Convert.ToString(ChartData.Rows[3]["section_name"]);
                    sec5.Text = Convert.ToString(ChartData.Rows[4]["section_abbr"]) + " - " + Convert.ToString(ChartData.Rows[4]["section_name"]);
                    sec6.Text = Convert.ToString(ChartData.Rows[5]["section_abbr"]) + " - " + Convert.ToString(ChartData.Rows[5]["section_name"]);
                    string[] XPointMember = new string[ChartData.Rows.Count];
                    int[] YPointMember = new int[ChartData.Rows.Count];

                    for (int count = 0; count < ChartData.Rows.Count; count++)
                    {
                        //storing Values for X axis  
                        XPointMember[count] = ChartData.Rows[count]["section_abbr"].ToString();
                        //storing values for Y Axis  
                        YPointMember[count] = Convert.ToInt32(ChartData.Rows[count]["avg_score"]);
                        //ChartPerSection.Series[0].Name = column.ColumnName;

                    }
                    Chart1.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                    Chart1.Series[0].ChartType = SeriesChartType.Radar;
                    Chart1.Series[0]["RadarDrawingStyle"] = "Area";
                    Chart1.Series[0]["AreaDrawingStyle"] = "Polygon";
                    Chart1.Series[0].Color = System.Drawing.Color.DarkGreen;
                    Chart1.Titles[0].Text = "Audit Score";
                    Chart1.Series[0].IsValueShownAsLabel = true;
                    Chart1.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    Chart1.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                    Chart1.ChartAreas[0].AxisX.Interval = 1;
                    Chart1.ChartAreas[0].AxisY.Interval = 25;
                    Chart1.Series[0].LabelFormat = "{0} %";
                    Chart1.Series[0].LabelForeColor = System.Drawing.Color.DarkRed;
                    Chart1.Series[0].LabelBorderWidth = 10;
                    Chart1.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                    Chart1.ChartAreas[0].AxisY.Maximum = 100;


                    Chart1.SaveImage(Convert.ToString(ConfigurationManager.AppSettings["ChartImageFile"]), ChartImageFormat.Png);

                    //string[] XPointMember1 = new string[ChartData.Rows.Count];
                    //int[] YPointMember1 = new int[ChartData.Rows.Count];

                    //for (int count = 0; count < ChartData.Rows.Count; count++)
                    //{
                    //    //storing Values for X axis  
                    //    XPointMember1[count] = ChartData.Rows[count]["section_abbr"].ToString();
                    //    //storing values for Y Axis  
                    //    YPointMember1[count] = Convert.ToInt32(ChartData.Rows[count]["avg_score"]);
                    //    //ChartPerSection.Series[0].Name = column.ColumnName;

                    //}
                    //Chart Chart2 = new Chart();
                    //Series series1 = new Series();
                    //Chart2.Series.Add(series1);
                    //Chart2.Series[0].Points.DataBindXY(XPointMember, YPointMember);
                    //Chart2.Series[0].ChartType = SeriesChartType.Radar;
                    //Chart2.Series[0]["RadarDrawingStyle"] = "Area";
                    //Chart2.Series[0]["AreaDrawingStyle"] = "Polygon";
                    //Chart2.Series[0].Color = System.Drawing.Color.DarkGreen;
                    //Title title = new System.Web.UI.DataVisualization.Charting.Title();
                    //Chart2.Titles.Add(title);
                    //Chart2.Titles[0].Text = "Audit Score";
                    //Chart2.Series[0].IsValueShownAsLabel = true;
                    //ChartArea ch1 = new ChartArea();
                    //Chart2.ChartAreas.Add(ch1);
                    //Chart2.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    //Chart2.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                    //Chart2.ChartAreas[0].AxisX.Interval = 1;
                    //Chart2.ChartAreas[0].AxisY.Interval = 25;
                    //Chart2.Series[0].LabelFormat = "{0} %";
                    //Chart2.Series[0].LabelForeColor = System.Drawing.Color.DarkRed;
                    //Chart2.Series[0].LabelBorderWidth = 10;
                    //Chart2.ChartAreas[0].AxisY.LabelStyle.Format = "{0} %";
                    //Chart2.ChartAreas[0].AxisY.Maximum = 100;
                    //Chart2.SaveImage(Convert.ToString(ConfigurationManager.AppSettings["ChartImageFile"]), ChartImageFormat.Png);
                }
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }



        }

        private string questionheading()
        {
            StringBuilder strHTMLBuilder = new StringBuilder();
            string Htmltext = string.Empty;
            try
            {
                strHTMLBuilder.Append("<table style='border-collapse: collapse; border: 1px solid darkgray;'>");
                strHTMLBuilder.Append("<tr height='20px' style='background-color:gray;'>");
                strHTMLBuilder.Append("<td width='50px' style='font-family:Gotham-Book; font-size:10px; border: 1px solid gray; border-collapse: collapse;'>");
                strHTMLBuilder.Append("Item #");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='560px' style='border: 1px solid darkgray; border-collapse: collapse; font-family:Gotham-Book; font-size:10px; text-align:center;'>");
                strHTMLBuilder.Append("Checklist");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='90px' colspan='3' style='border: 1px solid darkgray; border-collapse: collapse; font-family:Gotham-Book; font-size:10px; text-align:center;'>");
                strHTMLBuilder.Append("Evaluation<br/><table  style='border-collapse: collapse; border: 1px solid black;'><tr><td style='border: 1px solid black; border-collapse: collapse; font-family:Gotham-Book; font-size:9px; text-align:center;'>No</td><td style='border: 1px solid black; border-collapse: collapse; font-family:Gotham-Book; font-size:9px; text-align:center;'>Yes</td><td style='border: 1px solid black; border-collapse: collapse; font-family:Gotham-Book; font-size:9px; text-align:center;'>NA</td></tr></table>");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='150px' style='border: 1px solid darkgray; border-collapse: collapse; font-family:Gotham-Book; font-size:10px; text-align:center;'>");
                strHTMLBuilder.Append("Findings");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='70px' style='border: 1px solid darkgray; border-collapse: collapse; font-family:Gotham-Book; font-size:10px; text-align:center; '>");
                strHTMLBuilder.Append("Resp.");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='70px' style='border: 1px solid darkgray; border-collapse: collapse; font-family:Gotham-Book; font-size:10px; text-align:center;'>");
                strHTMLBuilder.Append("Actual Date Closed");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                //strHTMLBuilder.Append("</table>");
                Htmltext = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }
            return Htmltext;
        }

        private string getLastRow(DataTable dt)
        {
            StringBuilder strHTMLBuilder = new StringBuilder();
            string strHTML = string.Empty;
            try
            {
                //strHTMLBuilder.Append("<table border='1px' cellspacing='0'>");

                //strHTMLBuilder.Append("<tr >");
                //strHTMLBuilder.Append("<td colspan='2' style='font-family:Gotham-Book; font-size:12px; text-align:center;'>");
                //strHTMLBuilder.Append("</td>");
                //strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("<tr style='border: 1px solid gray; border-collapse: collapse;'>");
                strHTMLBuilder.Append("<td colspan='2' width='500px' style='border: 1px solid gray; border-collapse: collapse; font-family:Gotham-Book; font-size:12px; text-align:right;'>");
                strHTMLBuilder.Append("Result:");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td colspan='6' width='500px' style='border: 1px solid gray; border-collapse: collapse; font-family:Gotham-Book; font-size:12px; text-align:left;'>");
                if (dt != null)
                    if (dt.Rows.Count > 0)
                        strHTMLBuilder.Append(Convert.ToString(dt.Rows[0]["Score"]) + "%");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                // strHTMLBuilder.Append("</table>");
                strHTML = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }
            return strHTML;
        }

        #endregion
    }
}