﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MH_Logistics
{
    public partial class Site_Master : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["Role"])))
            {
                lblUserName.Text = Convert.ToString(Session["UserFullName"]);
                if (Convert.ToString(Session["GlobalAdminFlag"]) == "Y" || Convert.ToString(Session["SiteAdminFlag"]) == "Y")
                {
                    CompReport.Visible = true;
                    listSite.Visible = true;
                }
                else
                {
                    CompReport.Visible = false;
                    listSite.Visible = false;
                }
                if (Convert.ToString(Session["GlobalAdminFlag"]) == "Y" )
                {
                    //BulkQR.Visible = true;
                    role.Visible = true;
                }

                else
                {
                    //BulkQR.Visible = false;
                    role.Visible = false;
                }
            }

        }

        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            try
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Buffer = true;
                Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
                Response.Expires = -1000;
                Response.CacheControl = "no-cache";
                Response.Redirect("Login.aspx", true);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }
    }
}