﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreatePlanning.aspx.cs" Inherits="MH_Logistics.CreatePlanning" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="CalenderTheme/css/jquery.qtip-2.2.0.css" rel="stylesheet" type="text/css" />
    <link href="css/kns_calendar/kns_calendar.css" rel="stylesheet" />
    <link href="css/kns_calendar/fSelect.css" rel="stylesheet" />
    <link href="css/kns_calendar/kns_modal.css" rel="stylesheet" />
    <script src="CalenderTheme/jquery/moment-2.8.1.min.js" type="text/javascript"></script>
    <script src="CalenderTheme/jquery/jquery-ui-1.11.1.js" type="text/javascript"></script>
    <script src="CalenderTheme/jquery/jquery.qtip-2.2.0.js" type="text/javascript"></script>
    <link href="media/css/themes/south/jquery-ui.theme.min.css" rel="stylesheet" type="text/css" charset="utf-8" />
    <script src="js/fullcalendar.js"></script>
    <style type='text/css'>
        body
        {
            margin-top: 40px;
            text-align: center;
            font-size: 14px;
            font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
        }

        #calendar
        {
            width: 100%;
            margin: 0 auto;
        }
        /* css for timepicker */
        .ui-timepicker-div dl
        {
            text-align: left;
        }

        .control-label2
        {
            font-weight: normal;
            padding-top: 9px;
        }

        input#MainContent_btnFilter
        {
            margin-top: 20px;
        }

        .ui-timepicker-div dl dt
        {
            height: 25px;
        }

        .ui-timepicker-div dl dd
        {
            margin: -25px 0 10px 65px;
        }

        .style1
        {
            width: 100%;
        }

        input[type=submit]:disabled
        {
            cursor: not-allowed;
            background-color: #f9f9f9;
            color: #adadad;
            width: 18% !important;
            background-color: #419641;
            color: #333333;
            font-family: "Segoe UI", Helvetica, Arial, sans-serif;
        }

        .fc th, .fc td
        {
            border-style: 1px solid !important;
        }

        /* table fields alignment*/
        .multiple
        {
            padding: 6px 6px !important;
            height: 0px !important;
            font-size: 12px;
            border: none !important;
        }

        #calendar
        {
            margin-top: 24px;
        }

        .fs-search:before
        {
            position: absolute;
            right: 0px;
            margin-right: 15px;
            top: 12px;
        }

        .aspNetDisabled
        {
            width: 70.4% !important;
            height: auto;
            padding: 5px 5px 5px 5px;
            margin: 5px 1px 3px 0px;
        }

        .ui-datepicker-trigger
        {
            padding-top: 15px;
        }

        .col-sm-3
        {
            width: 29% !important;
        }

        .rbtn_panel
        {
            list-style: none;
            width: 240px !important;
            padding: 0px 2px 0px 9px;
            margin-bottom: 0px;
        }

        .fc-event-container
        {
            width: 100%;
        }

        .fc-time-grid-container, /* so scroll container's z-index is below all-day */
        .fc-time-grid
        { /* so slats/bg/content/etc positions get scoped within here */
            position: relative;
            z-index: 1;
            width: 100%;
        }
    </style>
    <script type="text/javascript">
        var currentUpdateEvent;
        var addStartDate;
        var addEndDate;
        var globalAllDay;
        var tempStartDate;
        var tempEnddate;
        var projectID;
        var totalhrs;
        function updateEvent(event, element, start, end) {
            alert("Update Event");
            if ($(this).data("qtip")) $(this).qtip("destroy");
            currentUpdateEvent = event;

            $('#addDialog').modal();

            $("#MainContent_addEventStartDate").val("" + event.start.format("MM-DD-YYYY hh:mm:ss T"));
            $("#MainContent_addEventEndDate").val("" + event.end.format("MM-DD-YYYY hh:mm:ss T"));

            addStartDate = event.start;
            addEndDate = event.end;
            var dt1 = (event.start.format("MM-DD-YYYY hh:mm:ss T")) + "M";
            var dt2 = (event.end.format("MM-DD-YYYY hh:mm:ss T")) + "M";
            var from = new Date(Date.parse(dt1));
            var to = new Date(Date.parse(dt2));
            totalhrs = (parseInt(((to - from) / 1000 / 3600) * 100, 10)) / 100;
            $('#txtTotalHrs').val(totalhrs);

            var flag = event.isSubmit;
            $('#MainContent_hdnValueSubmitFlag').val(flag);
            if (event.isSubmit == "1") {
                $('#addDialog').find('input, textarea, button, select').attr('disabled', 'disabled');
            }
        }

        function updateSuccess(updateResult) {
            alert(updateResult);
        }

        function deleteSuccess(deleteResult) {
            alert(deleteResult);
        }

        function addSuccess(addResult) {
            // if addresult is -1, means event was not added
            alert("added key: " + addResult);

            if (addResult != -1) {
                $('#calendar').fullCalendar('renderEvent',
                                {
                                    title: $("#addEventName").val(),
                                    start: addStartDate,
                                    end: addEndDate,
                                    id: addResult,
                                    description: $("#addEventDesc").val(),
                                    allDay: globalAllDay
                                },
                                true // make the event "stick"
                            );


                $('#calendar').fullCalendar('unselect');
            }
        }

        function UpdateTimeSuccess(updateResult) {
            alert(updateResult);
        }

        function selectDate(start, end, allDay) {
            var newstartdt = start.format("MM-DD-YYYY HH:MM:ss");
            var newenddt = end.format("MM-DD-YYYY HH:MM:ss");
            var today = new Date();
            var DD = today.getDate();
            var MM = today.getMonth() + 1; //January is 0!
            var YYYY = today.getFullYear();
            if (DD < 10) {
                DD = '0' + DD
            }
            if (MM < 10) {
                MM = '0' + MM
            }
            var hh = today.getHours();
            var mm = today.getMinutes();
            var ss = today.getSeconds();

            var todaydate = MM + '-' + DD + '-' + YYYY + ' ' + hh + ':' + mm + ':' + ss;
            var newstartdate = new Date(newstartdt);
            var today_date = new Date(todaydate);
            var newenddate = new Date(newenddt);
            if (today_date.getTime() - newstartdate.getTime() < 0 || today_date.getTime() - newenddate.getTime() < 0) {
                //if (newstartdt > todaydate || newenddt > todaydate) {
                alert("Please Select Valid Date");
                location.reload();
            }
            else {

                $('#addDialog').modal({
                    buttons: {
                        "Add": function () {
                            //alert("sent:" + addStartDate.format("dd-MM-yyyy hh:mm:ss tt") + "==" + addStartDate.toLocaleString());
                            var eventToAdd = {
                                title: $("#addEventName").val(),
                                description: $("#addEventDesc").val(),
                                start: addStartDate.toJSON(),
                                end: addEndDate.toJSON(),

                                allDay: isAllDay(addStartDate, addEndDate)
                            };

                            if (checkForSpecialChars(eventToAdd.title) || checkForSpecialChars(eventToAdd.description)) {
                                alert("please enter characters: A to Z, a to z, 0 to 9, spaces");
                            }
                            else {
                                //alert("sending " + eventToAdd.title);

                                PageMethods.addEvent(eventToAdd, addSuccess);
                                $(this).dialog("close");
                            }
                        }
                    }
                });
            }
            var newstartdt = start.format("MM-DD-YYYY hh:mm:ss T");
            //alert(newstartdt);
            //var dateEnd = new Date(end);
            var newEnddt = end.format("MM-DD-YYYY hh:mm:ss T")

            $("#MainContent_addEventStartDate").val("" + newstartdt);
            $("#MainContent_addEventEndDate").val("" + newEnddt);
            tempStartDate = "" + start.toLocaleString();
            tempEnddate = "" + end.toLocaleString();
            addStartDate = start;
            addEndDate = end;
            globalAllDay = allDay;

            var dt1 = (newstartdt) + "M";
            var dt2 = (newEnddt) + "M";
            var from = new Date(Date.parse(dt1));
            var to = new Date(Date.parse(dt2));
            totalhrs = (parseInt(((to - from) / 1000 / 3600) * 100, 10)) / 100;
            $('#txtTotalHrs').val(totalhrs);

            //alert(allDay);

        }

        function updateEventOnDropResize(event, allDay) {

            alert("allday: " + allDay);
            var eventToUpdate = {
                id: event.id,
                start: event.start
            };
            if (event.end === null) {
                eventToUpdate.end = eventToUpdate.start;
            }
            else {
                eventToUpdate.end = event.end;
            }
            var endDate;
            if (!event.allDay) {
                endDate = new Date(eventToUpdate.end + 60 * 60000);
                endDate = endDate.toJSON();
            }
            else {
                endDate = eventToUpdate.end.toJSON();
            }

            eventToUpdate.start = eventToUpdate.start.toJSON();
            eventToUpdate.end = eventToUpdate.end.toJSON(); //endDate;
            eventToUpdate.allDay = event.allDay;

            PageMethods.UpdateEventTime(eventToUpdate, UpdateTimeSuccess);
        }

        function eventDropped(event, delta, revertFunc) {

            if ($(this).data("qtip")) $(this).qtip("destroy");
            if (event.isSubmit == 0) {
                updateEventOnDropResize(event);
            } else {
                $(this).qtip("destroy");
                revertFunc();
            }
        }

        function eventResized(event, delta, revertFunc) {

            if ($(this).data("qtip")) $(this).qtip("destroy");
            if (event.isSubmit == 0) {
                updateEventOnDropResize(event);

            }
            else {
                revertFunc();
            }
        }

        function checkForSpecialChars(stringToCheck) {
            var pattern = /[^A-Za-z0-9 ]/;
            return pattern.test(stringToCheck);
        }

        function isAllDay(startDate, endDate) {
            var allDay;

            if (startDate.format("HH:mm:ss") == "00:00:00" && endDate.format("HH:mm:ss") == "00:00:00") {
                allDay = true;
                globalAllDay = true;
            }
            else {
                allDay = false;
                globalAllDay = false;
            }

            return allDay;
        }

        function qTipText(start, end, description) {
            addStartDate = start;
            addEndDate = end;
            globalAllDay = allDay;
        }

        function updateEventOnDropResize(event, allDay) {
            var eventToUpdate = {
                id: event.id,
                start: event.start
            };
            if (event.end === null) {
                eventToUpdate.end = eventToUpdate.start;
            }
            else {
                eventToUpdate.end = event.end;
            }
            var endDate;
            if (!event.allDay) {
                endDate = new Date(eventToUpdate.end + 60 * 60000);
                endDate = endDate.toJSON();
            }
            else {
                endDate = eventToUpdate.end.toJSON();
            }

            eventToUpdate.start = eventToUpdate.start.toJSON();
            eventToUpdate.end = eventToUpdate.end.toJSON(); //endDate;
            eventToUpdate.allDay = event.allDay;

            PageMethods.UpdateEventTime(eventToUpdate, UpdateTimeSuccess);
        }



        function checkForSpecialChars(stringToCheck) {
            var pattern = /[^A-Za-z0-9 ]/;
            return pattern.test(stringToCheck);
        }

        function isAllDay(startDate, endDate) {
            var allDay;

            if (startDate.format("HH:mm:ss") == "00:00:00" && endDate.format("HH:mm:ss") == "00:00:00") {
                allDay = true;
                globalAllDay = true;
            }
            else {
                allDay = false;
                globalAllDay = false;
            }

            return allDay;
        }

        //Edited by Aswini. 
        //Pop up display on mouse over of event
        function qTipText(start, end, customer_number, customer_short_name, customer_class, visited_salesengineer_name, remarks) {
            var text;

            if (end !== null)
                text = "<strong>Start:</strong> " + start.format("MM/DD/YYYY hh:mm T") +
                       "<br/><strong>End:</strong> " + end.format("MM/DD/YYYY hh:mm T") +
                       "<br/><br/><strong>Visited Eng. Name: </strong>" + visited_salesengineer_name +
                       "<br/><br/><strong>Customer Number: </strong>" + customer_number +
                       "<br/><strong>Customer Name: </strong>" + customer_short_name +
                       "<br/><strong>Customer Class: </strong>" + customer_class +
                       "<br/><strong>Remarks: </strong>" + remarks;
            else
                text = "<strong>Start:</strong> " + start.format("MM/DD/YYYY hh:mm T") +
                        "<br/><br/><strong>Visited Eng. Name: </strong>" + visited_salesengineer_name +
                        "<br/><br/><strong>Customer Number: </strong>" + customer_number +
                        "<br/><strong>Customer Name: </strong>" + customer_short_name +
                        "<br/><strong>Customer Class: </strong>" + customer_class +
                        "<br/><strong>Remarks: </strong>" + remarks;

            return text;
        }


        function eventDragStop(event, dayDelta, minuteDelta, allDay, revertFunc) {

            if (event.isSubmit == 0) {
                if ($(this).data("qtip")) $(this).qtip("destroy");

                return true;
            }
            else {
                if ($(this).data("qtip")) $(this).qtip("destroy");
                //$('.fc-bg').re
                return false;
            }
        }

        $(document).ready(function (events) {

            triggerScript();

        });
        $(window).load(function () {
            triggerScript();
        });
        function showPopUp() {
            $('#addDialog').modal();

        }
        function triggerScript() {


            var date = new Date(tempStartDate);
            var newstartdt = date.format("MM-dd-yyyy hh:mm:ss tt");
            var dateEnd = new Date(tempEnddate);
            var newEnddt = dateEnd.format("MM-dd-yyyy hh:mm:ss tt");
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var options = {
                weekday: "long", year: "numeric", month: "short",
                day: "numeric", hour: "2-digit", minute: "2-digit"
            };



            $('#txtTotalHrs').val(totalhrs);
            $('#btnclose').click(function () {
                location.reload();
            });


            $('#calendar').fullCalendar({

                theme: true,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                defaultView: 'agendaWeek',
                eventClick: updateEvent,
                selectable: true,
                selectHelper: true,
                select: selectDate,
                editable: true,
                events: [
				{
				    title: 'All Day Event',
				    start: '2017-05-01'
				},
				{
				    title: 'Long Event',
				    start: '2017-05-07',
				    end: '2017-05-10'
				},
				{
				    id: 999,
				    title: 'Repeating Event',
				    start: '2017-05-09T16:00:00'
				},
				{
				    id: 999,
				    title: 'Repeating Event',
				    start: '2017-05-16T16:00:00'
				},
				{
				    title: 'Conference',
				    start: '2017-05-11',
				    end: '2017-05-13'
				},
				{
				    title: 'Meeting',
				    start: '2017-05-12T10:30:00',
				    end: '2017-05-12T12:30:00'
				},
				{
				    title: 'Lunch',
				    start: '2017-05-12T12:00:00'
				},
				{
				    title: 'Meeting',
				    start: '2017-05-12T14:30:00'
				},
				{
				    title: 'Happy Hour',
				    start: '2017-05-12T17:30:00'
				},
				{
				    title: 'Dinner',
				    start: '2017-05-12T20:00:00'
				},
				{
				    title: 'Birthday Party',
				    start: '2017-05-13T07:00:00'
				},
				{
				    title: 'Click for Google',
				    url: 'http://google.com/',
				    start: '2017-05-28'
				}
                ],
                eventDrop: eventDropped,
                eventResize: eventResized,
                eventLimit: true,
                eventRender: function (event, element) {
                    element.addClass('sale_person_' + event.salesEngineer);
                    element.addClass('visited_person_' + event.visited_salesengineer_id);
                    //alert(event.visited_salesengineer_id);//
                    //alert(event.salesEngineer)
                    element.find('.fc-content').append("<strong>" + event.customer_short_name + "(" + event.customerNumber + ")" + "</strong>"); //+ "(" + event.salesEngineer + ")</strong>");
                    //alert(event.customer_short_name);//
                    element.qtip({
                        content: {
                            text: qTipText(event.start, event.end, event.customerNumber, event.customer_short_name, event.customer_class, event.visited_salesengineer_name, event.remarks),
                            title: '<strong>' + event.customer_short_name + "( " + event.customerNumber + " )" + '</strong>'
                        },
                        position: {
                            my: 'bottom left',
                            at: 'top right'
                        },
                        style: { classes: 'qtip-shadow qtip-rounded' }
                    });

                },


            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true" AsyncPostBackTimeout="360">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="contentpanel">
                <!--\\\\\\\ contentpanel start\\\\\\-->
                <div class="container clear_both padding_fix">
                    <div class="row table_display table_type">
                        <div class="row-info">
                            <div class="col-md-4 nopad">
                                <p>Location</p>
                            </div>
                            <div class="col-md-8 nopad">
                                <asp:DropDownList class="form-control1 mn_inp control3" runat="server" ID="ddlLocation"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="clearfix">
                        </div>
                        <div id="calendar" class="col-sm-9">
                        </div>
                        <div id="addDialog" class="modal fade">
                            <div class="modal-dialog1">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 id="H1" class="modal-title">Create Planning</h4>
                                    </div>
                                    <div id='Div2'></div>
                                    <div id="Div1" class="modal-body">
                                        <form id='plan_entry'>
                                            <table width='100%' cellspacing='2' cellpadding='3' border='0' bgcolor='#efefef'>
                                                <tr>
                                                    <td>
                                                        <table width='100%' cellspacing='3' cellpadding='1' border='0' bgcolor='#ffffff'>
                                                            <tr>
                                                                <td width='20%' style='width: 420px;' class='table_holder'>
                                                                    <table width='100%' border='0' cellpadding='1' cellspacing='1' bgcolor='#ffffff'>
                                                                        <tr>
                                                                            <td class='text_data_darker'>Employee Name</td>

                                                                            <td class='text_data'>
                                                                                <asp:DropDownList runat="server" ID="ddlusers"></asp:DropDownList>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td class='text_data_darker'>Line Name </td>
                                                                            <td class='text_data'>
                                                                                <asp:DropDownList ID="ddlLineNumber" runat="server" size='1' class='plan_select_box kns_inputText kns_inputSelectWidth'>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td class='text_data_darker'>Shift Number <span id="ccstar" class='star'>*</span></td>
                                                                            <td class='text_data'>

                                                                                <asp:TextBox runat="server" ID="txtShift" class='remark kns_inputText kns_inputSelectWidth'></asp:TextBox>
                                                                            </td>
                                                                        </tr>


                                                                        <tr>
                                                                            <td class='text_data_darker'>Planning Start:</td>
                                                                            <td class="text_data">
                                                                                <%--                                                        <asp:Label runat="server" id="lblEventStartDate"><span  runat="server" ></span></asp:Label>--%>
                                                                                <asp:TextBox ID="addEventStartDate" EnableViewState="true" ReadOnly="true" runat="server" class='remark kns_inputText kns_inputSelectWidth' size='25'></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="text_data_darker">Planning End:</td>
                                                                            <td class="text_data">
                                                                                <%--                                                        <asp:Label runat="server" id="lblEventEndDate"><span  runat="server" ></span></asp:Label>--%>
                                                                                <asp:TextBox ID="addEventEndDate" ReadOnly="true" runat="server" class='remark kns_inputText kns_inputSelectWidth' size='25'></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class='text_data_darker'>No of Hours</td>
                                                                            <td class='text_data'>
                                                                                <input type='text' readonly="true" name='contact_number' id='txtTotalHrs' class='remark kns_inputText kns_inputSelectWidth' size='25'></td>
                                                                        </tr>
                                                                    </table>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            </table>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="button" id="btnclose" class="close_visit_entry btn btn-success" value="Close" data-dismiss="modal">
                                        <asp:Button ID="btnSave" Text="Save &  Exit" class="btn btn-success" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>

