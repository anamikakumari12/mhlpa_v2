﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="Audit.aspx.cs" Inherits="MH_Logistics.Audit" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="HeadContent">
    <%--it worked
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
    --%>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
    <!--/<script src="js/jspdf.js"></script>
    <script src="js/from_html.js"></script>/-->
    <!--/<script src="js/split_text_to_size.js"></script>
    <script src="js/standard_fonts_metrics.js"></script>/-->
    <%--<script src="js/addhtml.js"></script>--%>
    <script src="js/FileSaver.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#dialog").dialog({
                modal: true,
                autoOpen: false,
                title: "Comments",
                width: 800
            });
            $("#imgComment").click(function () {
                debugger;
                $('#dialog').css('display', 'block');
                $('#dialog').dialog('open');
            });
        });
        function OpenPopUp() {
            $("#dialog").dialog({
                modal: true,
                autoOpen: false,
                title: "Comments",
                width: 800
            });
            $('#dialog').css('display', 'block');
            $('#dialog').dialog('open');
        }
        function MyFunction() {
            alert('Updated Successfully.');
        }
        function validateControls() {
            debugger
            var err_flag = 0;
            var time_format_flag = 0;
            var time_format_check = true;
            if ($('#MainContent_ddlRegion').val() == "" || $('#MainContent_ddlRegion').val() == null) {
                $('#MainContent_ddlRegion').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_ddlRegion').css('border-color', '');
            }
            if ($('#MainContent_ddlCountry').val() == "" || $('#MainContent_ddlCountry').val() == null) {
                $('#MainContent_ddlCountry').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlCountry').css('border-color', '');
            }

            if ($('#MainContent_ddlLocation').val() == "" || $('#MainContent_ddlLocation').val() == null) {
                $('#MainContent_ddlLocation').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLocation').css('border-color', '');
            }

            if ($('#MainContent_ddlLineName').val() == "" || $('#MainContent_ddlLineName').val() == null) {
                $('#MainContent_ddlLineName').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLineName').css('border-color', '');
            }
            //if (document.getElementById('txtPartNo').value == '' || document.getElementById('txtPartNo').value == null) {
            //    //if ($('#MainContent_ddlPartName').val() == "" || $('#MainContent_ddlPartName').val() == null) {
            //    //$('#MainContent_ddlPartName').css('border-color', 'red');
            //    document.getElementById('txtPartNo').style["border-color"] = "red";
            //    err_flag = 1;
            //}
            //else {
            //    //$('#MainContent_ddlPartName').css('border-color', '');
            //    document.getElementById('txtPartNo').style["border-color"] = "";
            //}

            if ($('#MainContent_txtNumber').val() == "") {
                $('#MainContent_txtNumber').css('border-color', 'red');
                err_flag = 1;
            }
            else {

                $('#MainContent_txtNumber').css('border-color', '');
            }
            if ($('#MainContent_txtDate').val() == "") {
                $('#MainContent_txtDate').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_txtDate').css('border-color', '');
            }
            if ($('#MainContent_txtTime').val() == "") {
                $('#MainContent_txtTime').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_txtTime').css('border-color', '');
                if ($('#MainContent_txtTime').is(':visible')) {
                    time_format_check = validateTime($('#MainContent_txtTime').val());
                    if (!time_format_check) {
                        time_format_flag = 1;
                        err_flag = 1;
                        $('#MainContent_txtTime').css('border-color', 'red');
                    }
                }
            }

            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return setmin();

            }
            else {
                if (time_format_flag == 0)
                    $('#MainContent_lblError').text('Please enter all the mandatory fields.');
                else
                    $('#MainContent_lblError').text('Please enter shift time in HH:MM format and enter all the mandatory fields. ');
                return false;
            }
        }
        function setmin() {
            if ($('#MainContent_txtNumber').val() <= 0) {
                $('#MainContent_txtNumber').val = 1;
                $('#MainContent_lblError').text('Shift should be greater than 0.');
                return false;
            }
            else {
                $('#MainContent_lblError').text('');
                return true;
            }
        }
        function validateComment() {
            var err_flag = 0;
            var value = $('#MainContent_rdbAnswer').find('input[type=radio]:checked').val();
            if (value == "1") {
                if ($('#MainContent_txtComment').val() == "") {
                    $('#MainContent_txtComment').css('border-color', 'red');
                    err_flag = 1;
                }
                else {

                    $('#MainContent_txtComment').css('border-color', '');
                }
            }


            if (err_flag == 0) {
                $('#MainContent_txtComment').css('border-color', '');
                return true;

            }
            else {
                $('#MainContent_txtComment').css('border-color', 'red');
                return false;
            }
        }


        function visibleComment() {
            //debugger
            var value = $('#MainContent_rdbAnswer').find('input[type=radio]:checked').val();
            if (value == 1) {
                $('#MainContent_txtComment').css('display', 'block');
                $('#MainContent_uplImage').css('display', 'block');
            }
            else {
                $('#MainContent_txtComment').css('display', 'none');
                $('#MainContent_uplImage').css('display', 'none');
            }
        }

        $("#imgComment").click(function () {
            debugger;
            $('#dialog').css('display', 'block');
            $('#dialog').dialog('open');
        });
        function CheckExtension(sender) {
            var validExts = new Array(".png", ".jpg", ".jpeg");
            var fileExt = sender.value;
            fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
            if (validExts.indexOf(fileExt) < 0) {
                $('#MainContent_lblExtError').text('Invalid file selected, valid files are of ' +
                         validExts.toString() + ' types.');
                $('#MainContent_btnExcel').attr("style", "display:none");
                return false;
            }
            else {
                $('#MainContent_lblExtError').text('');
                $('#MainContent_btnExcel').attr("style", "display:block");
                return true;
            }
        }
        $(document).ready(function () {
            debugger;
           // var part = '<%=HttpContext.Current.Session["part"]%>';
            //if (part == null || part == '' || part == undefined)
            //    BindControls();
            //else {
            //    BindControls();
                //$('#txtPartNo').val(part);
            //}

            $("#MainContent_txtDate").datepicker({
                showOn: "both",
                buttonImageOnly: true,
                buttonText: "",
                changeYear: true,
                changeMonth: true,
                yearRange: "c-20:c+50",
                // minDate: new Date(),
                dateFormat: 'dd-mm-yy',
                // buttonImage: "images/calander_icon.png"
            });

        });
        function BindControls() {
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Audit.aspx/GetPartNumbers",
                data: "{'Name':''}",
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    console.log(data.d);
                    var Countries = data.d;

                    $('#txtPartNo').autocomplete({
                        source: function (request, response) {
                            var matches = $.map(Countries, function (acItem) {
                                if (acItem.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                                    return acItem;
                                }
                            });
                            response(matches);
                        },
                        minLength: 0,
                        scroll: true,
                        autoFocus: true
                    }).focus(function () {
                        $(this).autocomplete("search", "");
                    });
                },
                error: function (result) {
                    console.log("No Match");
                }
            });


        }

        function validateHhMm(inputField) {
            debugger;
            var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(inputField.value);

            if (isValid) {
                //inputField.css('border-color', 'red');
                $('#MainContent_lblError').text('');
            } else {
                //inputField.css('border-color', '');
                $('#MainContent_lblError').text('Time should be in HH:MM format.');
            }

            return isValid;
        }
        function validateTime(timevalue) {
            var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(timevalue);
            return isValid;
        }

    </script>
    <style type="text/css">
        .btn-default
        {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            float: right;
            margin: 0px;
            color: #04522c;
            /*background-color: #04522c;*/
            border-color: #04522c;
            width: 36px;
            height: 36px;
            width: 100%!important;
            /*background: #007B3D none repeat scroll 0 0;
    color: #fff;
    float: right;
    margin: 10px 0px 0 10px;
    padding: 5px 10px;
    font-size: 14px;
    border:none*/
        }

        .btn-YesAnswered
        {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            float: right;
            margin: 0px;
            color: #fff;
            width: 36px;
            height: 36px;
            /*background-color: #e94e02;
    border-color: #e94e02;*/
            background-color: #04522c;
            border-color: #04522c;
            width: 100%!important;
        }

        .btn-NoAnswered
        {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            float: right;
            margin: 0px;
            color: black;
            width: 36px;
            height: 36px;
            /*background-color: #e94e02;
    border-color: #e94e02;*/
            background-color: red;
            border-color: red;
            width: 100%!important;
        }

        .btn-NAAnswered
        {
            width: 100%!important;
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            float: right;
            margin: 0px;
            color: black;
            width: 36px;
            height: 36px;
            /*background-color: #e94e02;
    border-color: #e94e02;*/
            background-color: gray;
            border-color: gray;
        }

        .tooltipDemo
        {
            position: relative;
            display: inline;
            text-decoration: wavy;
            left: 5px;
            top: 0px;
            text-decoration-color: aliceblue;
        }

            .tooltipDemo:hover:before
            {
                border: solid;
                border-color: transparent rgb(111, 13, 53);
                border-width: 6px 6px 6px 0px;
                bottom: 21px;
                content: "";
                left: 35px;
                top: 5px;
                position: absolute;
                z-index: 95;
                text-decoration: solid;
                text-decoration-color: aliceblue;
            }

            .tooltipDemo:hover:after
            {
                background: rgb(111, 13, 53);
                border-radius: 5px;
                color: #fff;
                width: 150px;
                left: 40px;
                top: -5px;
                content: attr(alt);
                position: absolute;
                padding: 5px 15px;
                z-index: 95;
                text-decoration-color: aliceblue;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server" EnableViewState="true">
    </asp:ScriptManager>
    <div>

        <div class='block-web' style='float: left; width: 100%; height: 36px'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Audit</a>
                        </li>
                        <li class="current">Audit Perform</li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div>

        <!-- main content start-->
        <div class="md_main">
            <div class="row">
                <div class="mannHummel" id="mannHummelId">
                    <div class="clearfix" style="width: 40px;"></div>

                    <asp:Panel runat="server" ID="pnlFilters">
                        <div class="filter_panel">
                            <div>
                                <div class="col-md-4 filter_label" style="width: 14%">
                                    <p>Region* : </p>
                                </div>
                                <div class="col-md-4" style="width: 20%">
                                    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                                <div class="col-md-4 filter_label" style="width: 13%">
                                    <p>Country* : </p>
                                </div>
                                <div class="col-md-4" style="width: 20%">
                                    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                                <div class="col-md-4 filter_label" style="width: 13%">
                                    <p>Location* : </p>
                                </div>
                                <div class="col-md-4" style="width: 20%">
                                    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <div>
                                <div class="col-md-4 filter_label" style="width: 14%">
                                    <p>Zone* :</p>
                                </div>
                                <div class="col-md-4" style="width: 20%">
                                    <asp:DropDownList AutoPostBack="true" class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLineName" OnSelectedIndexChanged="ddlLineName_SelectedIndexChanged"></asp:DropDownList>

                                </div>
                               
                                <div class="col-md-4 filter_label" style="width: 13%">
                                    <p>Shift Number* : </p>
                                </div>
                                <div class="col-md-4" style="width: 20%">
                                    <asp:TextBox class="form-control1 mn_inp control3 filter_select_control" Style="width: 100%; height: 35px; font-size: 14px; margin: 0.6em 0 0.6em;" Enabled="false" runat="server" ID="txtNumber" TextMode="Number"></asp:TextBox>
                                </div>
                                 <div class="col-md-4 filter_label" style="width: 13%; visibility:collapse;">
                                    <p>Product/Part No* :</p>
                                </div>
                                <div class="col-md-4" style="width: 20%; visibility:collapse;">
                                    <input type="text" class="form-control1 mn_inp control3 filter_select_control" id="txtPartNo" name="txtPartNo" />
                                    <asp:DropDownList Visible="false" class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlPartName"></asp:DropDownList>

                                </div>
                            </div>
                            <div runat="server" id="divdatetime">
                                <div class="col-md-4 filter_label" style="width: 14%">
                                    <p>Date* : </p>
                                </div>
                                <div class="col-md-4" style="width: 20%">
                                    <asp:TextBox class="form-control1 mn_inp control3 filter_select_control" Style="width: 100%; height: 35px; font-size: 14px; margin: 0.6em 0 0.6em;" runat="server" ID="txtDate"></asp:TextBox>
                                </div>

                                <div class="col-md-4 filter_label" style="width: 13%">
                                    <p>Time* : </p>
                                </div>
                                <div class="col-md-4" style="width: 20%">
                                    <asp:TextBox TextMode="Time" class="form-control1 mn_inp control3 filter_select_control" onchange="validateHhMm(this);" Style="width: 100%; height: 35px; font-size: 14px; margin: 0.6em 0 0.6em;" runat="server" ID="txtTime"></asp:TextBox>
                                    <%--  <div style="font-size: 12px;"> * Time format should be 24 hours. </div>--%>
                                </div>

                            </div>

                            <div>
                                <div>
                                    <div class="col-md-12">
                                        <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                                        <div style="float: right">
                                            <asp:Button runat="server" CssClass="btn-add" ID="btnClear" Style="margin-left: 4px; float: right;" Text="Clear" OnClick="btnClear_Click" />
                                        </div>
                                        <div style="float: right">
                                            <asp:Button runat="server" CssClass="btn-add" ID="btnFilter" Text="Audit" OnClick="btnFilter_Click" OnClientClick="return validateControls();" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </asp:Panel>

                    <div class="clearfix" style="width: 5px;"></div>
                    <asp:Panel runat="server" ID="pnlQuestions" Visible="false">
                    <asp:UpdateProgress runat="server" ID="PageUpdateProgress">
                        <ProgressTemplate>
                            Loading...
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <asp:UpdatePanel ID="updQuestion" runat="server">
                        <ContentTemplate>
                            <div class="mn_margin_15">
                                <div class="panel-body">
                                    <div class="row-info mn_table_1">
                                        <asp:Label runat="server" ID="lblResult"></asp:Label>
                                        <div class="col-md-10">
                                            <asp:Panel runat="server" ID="pnlQuestion" Visible="false">
                                                <div id="Div2" class="result_panel">

                                                    <div class="panel-body">
                                                        <div class="row-info mn_table_1">
                                                            <div>
                                                                <asp:Label Style="font-weight: bolder; font-style: italic;" runat="server" ID="lblSection" class="mn_top"></asp:Label>

                                                                <asp:Image Height="50px" Width="50px" CssClass="tooltipDemo" ImageAlign="Right" runat="server" ID="imgHelp" ImageUrl="images/help.png" />

                                                                <%--<asp:ImageButton Height="40px" Width="50px" ImageAlign="Right" OnClick="imgComment_Click" runat="server" ID="imgComment" ImageUrl="images/comment_icon.jpg" />--%>
                                                                <img id="imgComment" runat="server" onclick="OpenPopUp();" title="Comments" style="float: right; vertical-align: top; height: 55px; width: 50px;" src="images/comment_icon.png" />
                                                                <%--<img src="images/help.png" />--%>
                                                            </div>
                                                        </div>

                                                        <div class="row-info mn_table_1">
                                                            <div>
                                                                <%--<div class="col-md-3 nopad">
                                                            <p>Question :</p>
                                                        </div>
                                                        <div class="col-md-9 nopad">--%>
                                                                <asp:Label Style="font-weight: bolder;" class="mn_top" runat="server" ID="lblQuestion"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="row-info mn_table_1">
                                                            <div>
                                                                <%--<div class="col-md-3 nopad">
                                                            <p>Answer :</p>
                                                        </div>
                                                        <div class="col-md-9 nopad">--%>
                                                                <asp:RadioButtonList runat="server" ID="rdbAnswer" AutoPostBack="true" RepeatDirection="Horizontal" onchange="visibleComment();" OnSelectedIndexChanged="rdbAnswer_SelectedIndexChanged">
                                                                    <asp:ListItem Text="Yes" Value="0"></asp:ListItem>
                                                                    <asp:ListItem Text="No" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Not Applicable" Value="2"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                                <br />
                                                                <div id="divComment">
                                                                    <asp:TextBox runat="server" Style="width: 80%; display: none;" ID="txtComment" Rows="4" TextMode="MultiLine"></asp:TextBox>
                                                                    <div class="col-md-6">
                                                                        <asp:FileUpload runat="server" ID="uplImage" Style="width: 80%; display: none" />
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <asp:ImageButton Width="20px" runat="server" ImageUrl="images/plus.png" Visible="false" ID="imgAddImage" OnClick="imgAddImage_Click" />

                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <asp:Label ID="lblImageResult" runat="server" ForeColor="Red"></asp:Label>
                                                                    </div>
                                                                    <asp:GridView ID="grdImages" runat="server" AutoGenerateColumns="False" CellPadding="2"
                                                                        EnableModelValidation="True" ForeColor="#333333" GridLines="None"
                                                                        OnRowDeleting="grdImages_RowDeleting" DataKeyNames="Image_path">
                                                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                                                        <Columns>

                                                                            <asp:TemplateField HeaderText="Image" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:Label runat="server" Text='<%#Eval("Image_path") %>' ID="lblPath"></asp:Label>
                                                                                    <asp:HiddenField runat="server" Value='<%#Eval("DBImage_path") %>' ID="hdnPath" />
                                                                                    <%--  <img src='<%#Eval("Image_path") %>' width="120px" alt="" />             --%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>


                                                                            <asp:TemplateField HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="imgDelete" Style="width: 20px;" runat="server" CommandName="Delete" ImageUrl="~/images/delete_icon.png" OnClientClick="return confirm('Are you sure you want to delete selected image file ?')" ToolTip="Delete" CausesValidation="false" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <%-- <EditRowStyle BackColor="#999999" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />--%>
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%--<div class="row-info mn_table_1">
                                                        <div class="col-md-3 nopad">
                                                            <p>Help :</p>
                                                        </div>
                                                        <div class="col-md-9 nopad">
                                                            <asp:Label class="mn_top" runat="server" ID="lblHelp"></asp:Label>
                                                        </div>
                                                    </div>--%>

                                                        <div class="clearfix"></div>
                                                        <div>
                                                            <div class="col-md-4" style="width: 50%; align-items: flex-start;">
                                                                <asp:Button runat="server" ID="btnPrevious" Style="float: left;" Visible="false" CssClass="btn-add" Text="Previous" OnClick="btnPrevious_Click" OnClientClick="return validateComment();" />
                                                            </div>
                                                            <div class="col-md-4 nopad" style="width: 50%; align-items: flex-end;">
                                                                <asp:Button runat="server" ID="btnNext" CssClass="btn-add" Text="Next" OnClick="btnNext_Click" OnClientClick="return validateComment();" />

                                                                <asp:Button runat="server" ID="btnSubmit" Visible="false" CssClass="btn-add" Text="Submit" OnClick="btnSubmit_Click" OnClientClick="return validateComment();" />
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>


                                            </asp:Panel>
                                        </div>




                                        <div class="col-md-2">
                                            <asp:Panel runat="server" ID="pnlQNumbers" Visible="false">
                                                <div class="result_panel">

                                                    <div id="QNumDiv" runat="server" class="panel-body">
                                                        <div runat="server" id="DivQ1" class="col-md-4 nopad"></div>
                                                        <div runat="server" id="DivQ2" class="col-md-4 nopad"></div>
                                                        <div runat="server" id="DivQ3" class="col-md-4 nopad"></div>
                                                        <%-- <asp:Button runat="server" ID="Button" CssClass="numberCircle"></asp:Button>--%>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="dialog" class="mn_table" style="display: none;">
                                <asp:Label runat="server" ID="lblComments"></asp:Label>
                                <asp:Repeater ID="rptComments" runat="server" Visible="false">
                                    <HeaderTemplate>
                                        <table style="width: 750px;">
                                    </HeaderTemplate>
                                    <ItemTemplate>

                                        <tr>
                                            <td><b>Audit Comments : </b></td>
                                            <td colspan="3">
                                                <asp:Label runat="server" ID="lblComments" Text='<%#Eval("audit_comments") %>'></asp:Label></td>
                                        </tr>
                                         <tr>
                                            <td><b>Review Comments : </b></td>
                                            <td colspan="3">
                                                <asp:Label runat="server" ID="Label1" Text='<%#Eval("review_comments") %>'></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td><b>Review On : </b></td>
                                            <td>
                                                <asp:Label runat="server" ID="lblrptOn" Text='<%#Eval("review_closed_on") %>'></asp:Label></td>
                                            <td><b>Status : </b></td>
                                            <td>
                                                <asp:Label runat="server" ID="lblrptStatus" Text='<%#Eval("review_closed_status") %>'></asp:Label></td>
                                        </tr>
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>


                                </asp:Repeater>
                            </div>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="rdbAnswer" EventName="SelectedIndexChanged" />
                            <asp:PostBackTrigger ControlID="btnNext" />
                            <asp:PostBackTrigger ControlID="btnPrevious" />
                            <asp:PostBackTrigger ControlID="btnSubmit" />
                            <asp:PostBackTrigger ControlID="imgAddImage" />
                        </Triggers>
                    </asp:UpdatePanel>
                    </asp:Panel>
                </div>

            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>



