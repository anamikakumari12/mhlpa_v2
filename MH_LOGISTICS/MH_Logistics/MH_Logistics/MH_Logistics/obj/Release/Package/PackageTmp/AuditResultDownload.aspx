﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuditResultDownload.aspx.cs" MasterPageFile="~/Site.Master" Inherits="MH_Logistics.AuditResultDownload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <%--<link href="css/fonts.css" rel="stylesheet" />--%>
    <script src="js/datatables_export_lib.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <link href="css/datatables_lib_export.min.css" rel="stylesheet" />
    <link href="css/dataTables.bootstrap.css" rel="stylesheet" />
    <script src="js/moment.js"></script>
    <script src="js/daterangepicker.js"></script>
    <link href="css/daterangepicker.css" rel="stylesheet" />

    <script type="text/javascript">

        $(document).ready(function () {

            var head_content = $('#MainContent_grdAuditReports tr:first').html();
            $('#MainContent_grdAuditReports').prepend('<thead></thead>')
            $('#MainContent_grdAuditReports thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdAuditReports tbody tr:first').hide();

            $('#MainContent_grdAuditReports').dataTable({
                scrollY: '500px',
                scrollX: '100%',
                sScrollXInner: "100%",
                //"columnDefs": [
                // { "width": "200px", "targets": 6 },
                //],
                "bInfo": false

            });

        });



        $(function () {
            $("#<%= reportrange.ClientID %>").attr("readonly", "readonly");
            $("#<%= reportrange.ClientID %>").attr("unselectable", "on");
            $("#<%= reportrange.ClientID %>").daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            });
        });

        function validateControls() {
            var err_flag = 0;
            if ($('#MainContent_ddlRegion').val() == "" || $('#MainContent_ddlRegion').val() == null) {
                $('#MainContent_ddlRegion').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_ddlRegion').css('border-color', '');
            }
            if ($('#MainContent_ddlCountry').val() == "" || $('#MainContent_ddlCountry').val() == null) {
                $('#MainContent_ddlCountry').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlCountry').css('border-color', '');
            }

            if ($('#MainContent_ddlLocation').val() == "" || $('#MainContent_ddlLocation').val() == null) {
                $('#MainContent_ddlLocation').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLocation').css('border-color', '');
            }

            if ($('#MainContent_ddlLineName').val() == "" || $('#MainContent_ddlLineName').val() == null) {
                $('#MainContent_ddlLineName').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLineName').css('border-color', '');
            }


            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return setmin();

            }
            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields.');

                return false;
            }
        }



    </script>

    <style type="text/css">
        .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12
        {
                padding-right: 5px!important;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="SecriptMannager1" runat="server"></asp:ScriptManager>
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Report</a>
                        </li>
                        <li class="current">Audit Result Download</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div>
        <!-- main content start-->
        <div class="md_main">
            <div class="row">
                <div>
                    <div class="clearfix"></div>

                    <div class="filter_panel">
                        <div class="col-md-12">
                            <div class="col-md-1 filter_label">
                                <p>Division* : </p>
                            </div>
                            <div class="col-md-3">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlDivision" OnSelectedIndexChanged="ddlDivision_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-1 filter_label">
                                <p>Region* : </p>
                            </div>
                            <div class="col-md-3">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-1 filter_label">
                                <p>Country* : </p>
                            </div>
                            <div class="col-md-3">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-1 filter_label">
                                <p>Location* : </p>
                            </div>
                            <div class="col-md-3">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-1 filter_label">
                                <p>Zone* :</p>
                            </div>
                            <div class="col-md-3">
                                <asp:DropDownList OnSelectedIndexChanged="ddlLineName_SelectedIndexChanged" class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLineName" AutoPostBack="true"></asp:DropDownList>

                            </div>

                            <div class="col-md-1 filter_label">
                                <p>Date* :</p>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="reportrange" class="form-control1 mn_inp control3 filter_select_control" runat="server"></asp:TextBox>
                            </div>


                        </div>
                        <div class="col-md-12">

                            <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>

                            <asp:Button runat="server" CssClass="btn-add" ID="btnClear" Style="margin-left: 4px; float: right;" Text="Clear" OnClick="btnClear_Click" />

                            <asp:Button runat="server" CssClass="btn-add" ID="btnFilter" Text="Download Audit Result Report" OnClick="btnFilter_Click" OnClientClick="return validateControls();" />

                            <asp:Button runat="server" CssClass="btn-add" ID="btnPerform" OnClick="btnPerform_Click" Text="Download Performance Report" OnClientClick="return validateControls();" />
                        </div>
                    </div>

                    <div class="clearfix" style="height: 5px;"></div>




                </div>

            </div>
        </div>

    </div>

</asp:Content>


