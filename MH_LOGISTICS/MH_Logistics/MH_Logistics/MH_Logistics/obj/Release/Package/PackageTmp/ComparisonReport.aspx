﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ComparisonReport.aspx.cs" Inherits="MH_Logistics.ComparisonReport" MasterPageFile="~/Site.Master" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="css/calender.css" rel="stylesheet" />
    <script src="js/calender.js"></script>
    <script src="js/amcharts.js"></script>
    <script src="js/radar.js"></script>
    <script src="js/serial.js"></script>
    <script src="js/dataloader.min.js"></script>
    <script src="js/export.min.js"></script>
    <link href="css/export.css" rel="stylesheet" />
    <script src="js/light.js"></script>
    <script src="js/gray.js"></script>
    <script src="js/none.js"></script>
    <script src="js/multichart.js"></script>
    <script src="js/FileSaver.js"></script>
    <script src="js/fabric.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <style>
        .mn_mrg_left
        {
            padding-left: 0;
            box-shadow: 0px 0px 3px #aaa;
            background: #fff;
        }

        .mn_mrg_right
        {
            padding-right: 0;
            box-shadow: 0px 0px 3px #aaa;
            background: #fff;
        }

        .mn_mrg
        {
            padding: 0;
            box-shadow: 0px 0px 3px #aaa;
            background: #fff;
        }

        .mn_fetch .mn_mrg_left, .mn_fetch .mn_mrg, .mn_fetch .mn_mrg_right
        {
            box-shadow: none;
            padding: 5px;
        }

        .mn_fetch, .ma_lpa_result
        {
            width: 100%;
            float: left;
            background: #fff;
            border: solid 1px #ddd;
        }

        .ma_lpa_result
        {
            margin: 10px 0;
        }

        #divChartPerSectionMonthly
        {
            width: 100%;
            height: 500px;
        }

        #divChartPerSection
        {
            height: 500px;
        }

        #divChartPerSection2
        {
            height: 500px;
        }

        #divChartPerSection3
        {
            height: 500px;
        }

        #divChartPerQuestion
        {
            width: 100%;
            height: 500px;
        }

        .amcharts-legend-div
        {
            position: relative !important;
            left: 100px !important;
            top: 30px !important;
            width: 100% !important;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#MainContent_txtDate").datepicker({
                showOn: "both",
                buttonImageOnly: true,
                buttonText: "",
                changeYear: true,
                changeMonth: true,
                yearRange: "c-20:c+50",
                // minDate: new Date(),
                dateFormat: 'dd-mm-yy',
                //buttonImage: "images/calander_icon.png",
            });
        });
        $("#MainContent_txtDate").datepicker({
            showOn: "both",
            buttonImageOnly: true,
            buttonText: "",
            changeYear: true,
            changeMonth: true,
            yearRange: "c-20:c+50",
            // minDate: new Date(),
            dateFormat: 'dd-mm-yy',
            //buttonImage: "images/calander_icon.png",
        });

        function ValidateControls() {
            var err_flag = 0;
            if ($('#MainContent_ddlRegion1').val() == "" || $('#MainContent_ddlRegion1').val() == null) {
                $('#MainContent_ddlRegion1').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_ddlRegion1').css('border-color', '');
            }
            if ($('#MainContent_ddlCountry1').val() == "" || $('#MainContent_ddlCountry1').val() == null) {
                $('#MainContent_ddlCountry1').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlCountry1').css('border-color', '');
            }

            if ($('#MainContent_ddlLocation1').val() == "" || $('#MainContent_ddlLocation1').val() == null) {
                $('#MainContent_ddlLocation1').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLocation1').css('border-color', '');
            }

            if ($('#MainContent_ddlLine1').val() == "" || $('#MainContent_ddlLine1').val() == null) {
                $('#MainContent_ddlLine1').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLine1').css('border-color', '');
            }
            if ($('#MainContent_ddlRegion2').val() == "" || $('#MainContent_ddlRegion2').val() == null) {
                $('#MainContent_ddlRegion2').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_ddlRegion2').css('border-color', '');
            }
            if ($('#MainContent_ddlCountry2').val() == "" || $('#MainContent_ddlCountry2').val() == null) {
                $('#MainContent_ddlCountry2').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlCountry2').css('border-color', '');
            }

            if ($('#MainContent_ddlLocation2').val() == "" || $('#MainContent_ddlLocation2').val() == null) {
                $('#MainContent_ddlLocation2').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLocation2').css('border-color', '');
            }

            if ($('#MainContent_ddlLine2').val() == "" || $('#MainContent_ddlLine2').val() == null) {
                $('#MainContent_ddlLine2').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLine2').css('border-color', '');
            }
            if ($('#MainContent_ddlRegion3').val() == "" || $('#MainContent_ddlRegion3').val() == null) {
                $('#MainContent_ddlRegion3').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_ddlRegion3').css('border-color', '');
            }
            if ($('#MainContent_ddlCountry3').val() == "" || $('#MainContent_ddlCountry3').val() == null) {
                $('#MainContent_ddlCountry3').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlCountry3').css('border-color', '');
            }

            if ($('#MainContent_ddlLocation3').val() == "" || $('#MainContent_ddlLocation3').val() == null) {
                $('#MainContent_ddlLocation3').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLocation3').css('border-color', '');
            }

            if ($('#MainContent_ddlLine3').val() == "" || $('#MainContent_ddlLine3').val() == null) {
                $('#MainContent_ddlLine3').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLine3').css('border-color', '');
            }

            if ($('#MainContent_txtDate').val() == "") {
                $('#MainContent_txtDate').css('border-color', 'red');
                err_flag = 1;
            }
            else {

                $('#MainContent_txtDate').css('border-color', '');
            }
            if ($('#MainContent_ddlDivision1').val() == "" || $('#MainContent_ddlDivision1').val() == null) {
                $('#MainContent_ddlDivision1').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlDivision1').css('border-color', '');
            }
            if ($('#MainContent_ddlDivision2').val() == "" || $('#MainContent_ddlDivision2').val() == null) {
                $('#MainContent_ddlDivision2').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlDivision2').css('border-color', '');
            }
            if ($('#MainContent_ddlDivision3').val() == "" || $('#MainContent_ddlDivision3').val() == null) {
                $('#MainContent_ddlDivision3').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlDivision3').css('border-color', '');
            }
            if (err_flag == 0) {

                if ($('#MainContent_ddlDivision3').val() == $('#MainContent_ddlDivision2').val() && $('#MainContent_ddlDivision3').val() == $('#MainContent_ddlDivision1').val()) {
                    $('#MainContent_lblError').text('');
                    return true;
                }
                else {
                    $('#MainContent_lblError').text('All the division value should be same.');
                    return false;
                }
            }
            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields.');
                return false;
            }
        }

        function LoadCharts() {
            LoadChartPerMonthly();
            LoadChartPerSection();
            LoadChartPerQuestion();
        }

        function LoadChartPerMonthly() {

            var tmp = null;
            $.ajax({
                type: "POST",
                url: 'ComparisonReport.aspx/returnLPAResultMonthly',
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    console.log(msg.d);

                    var choice1;
                    if ($('#MainContent_ddlRegion1 :selected').val() == 0) {
                        choice1 = $('#MainContent_ddlRegion1 :selected').text();
                    }
                    else {
                        if ($('#MainContent_ddlCountry1 :selected').val() != 0) {

                            //choice1 = $('#MainContent_ddlRegion1 :selected').text() + '-' + $('#MainContent_ddlCountry1 :selected').text();
                            if ($('#MainContent_ddlLocation1 :selected').val() != 0) {
                                choice1 = $('#MainContent_ddlLocation1 :selected').text();
                                if ($('#MainContent_ddlLine1 :selected').val() != 0)
                                    choice1 = $('#MainContent_ddlLocation1 :selected').text() + '-' + $('#MainContent_ddlLine1 :selected').text();
                            }
                            else
                                choice1 = $('#MainContent_ddlCountry1 :selected').text();

                        }
                        else {
                            choice1 = $('#MainContent_ddlRegion1 :selected').text();
                        }


                    }
                    console.log('Choice1');
                    console.log(choice1);
                    var choice2;
                    if ($('#MainContent_ddlRegion2 :selected').val() == 0) {
                        choice2 = $('#MainContent_ddlRegion2 :selected').text();
                    }
                    else {
                        if ($('#MainContent_ddlCountry2 :selected').val() != 0) {

                            //choice1 = $('#MainContent_ddlRegion1 :selected').text() + '-' + $('#MainContent_ddlCountry1 :selected').text();
                            if ($('#MainContent_ddlLocation2 :selected').val() != 0) {
                                choice2 = $('#MainContent_ddlLocation2 :selected').text();
                                if ($('#MainContent_ddlLine2 :selected').val() != 0)
                                    choice2 = $('#MainContent_ddlLocation2 :selected').text() + '-' + $('#MainContent_ddlLine2 :selected').text();
                            }
                            else
                                choice2 = $('#MainContent_ddlCountry2 :selected').text();

                        }
                        else {
                            choice2 = $('#MainContent_ddlRegion2 :selected').text();
                        }


                    }

                    var choice3;
                    if ($('#MainContent_ddlRegion3 :selected').val() == 0) {
                        choice3 = $('#MainContent_ddlRegion3 :selected').text();
                    }
                    else {
                        if ($('#MainContent_ddlCountry3 :selected').val() != 0) {

                            //choice1 = $('#MainContent_ddlRegion1 :selected').text() + '-' + $('#MainContent_ddlCountry1 :selected').text();
                            if ($('#MainContent_ddlLocation3 :selected').val() != 0) {
                                choice3 = $('#MainContent_ddlLocation3 :selected').text();
                                if ($('#MainContent_ddlLine3 :selected').val() != 0)
                                    choice3 = $('#MainContent_ddlLocation3 :selected').text() + '-' + $('#MainContent_ddlLine3 :selected').text();
                            }
                            else
                                choice3 = $('#MainContent_ddlCountry3 :selected').text();

                        }
                        else {
                            choice3 = $('#MainContent_ddlRegion3 :selected').text();
                        }


                    }
                    //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];

                    AmCharts.addInitHandler(function (chart) {

                        //method to handle removing/adding columns when the marker is toggled
                        function handleCustomMarkerToggle(legendEvent) {
                            var dataProvider = legendEvent.chart.dataProvider;
                            var itemIndex; //store the location of the removed item

                            //Set a custom flag so that the dataUpdated event doesn't fire infinitely, in case you have
                            //a dataUpdated event of your own
                            legendEvent.chart.toggleLegend = true;
                            // The following toggles the markers on and off.
                            // The only way to "hide" a column and reserved space on the axis is to remove it
                            // completely from the dataProvider. You'll want to use the hidden flag as a means
                            // to store/retrieve the object as needed and then sort it back to its original location
                            // on the chart using the dataIdx property in the init handler
                            if (undefined !== legendEvent.dataItem.hidden && legendEvent.dataItem.hidden) {
                                legendEvent.dataItem.hidden = false;
                                dataProvider.push(legendEvent.dataItem.storedObj);
                                legendEvent.dataItem.storedObj = undefined;
                                //re-sort the array by dataIdx so it comes back in the right order.
                                dataProvider.sort(function (lhs, rhs) {
                                    return lhs.dataIdx - rhs.dataIdx;
                                });
                            } else {
                                // toggle the marker off
                                legendEvent.dataItem.hidden = true;
                                //get the index of the data item from the data provider, using the 
                                //dataIdx property.
                                for (var i = 0; i < dataProvider.length; ++i) {
                                    if (dataProvider[i].dataIdx === legendEvent.dataItem.dataIdx) {
                                        itemIndex = i;
                                        break;
                                    }
                                }
                                //store the object into the dataItem
                                legendEvent.dataItem.storedObj = dataProvider[itemIndex];
                                //remove it
                                dataProvider.splice(itemIndex, 1);
                            }
                            legendEvent.chart.validateData(); //redraw the chart
                        }

                        //check if legend is enabled and custom generateFromData property
                        //is set before running
                        if (!chart.legend || !chart.legend.enabled || !chart.legend.generateFromData) {
                            return;
                        }

                        var categoryField = chart.categoryField;
                        var colorField = chart.graphs[0].lineColorField || chart.graphs[0].fillColorsField || chart.graphs[0].colorField;
                        var legendData = chart.dataProvider.map(function (data, idx) {
                            var markerData = {
                                "title": data[categoryField] + ": " + data[chart.graphs[0].valueField],
                                "color": data[colorField],
                                "dataIdx": idx //store a copy of the index of where this appears in the dataProvider array for ease of removal/re-insertion
                            };
                            if (!markerData.color) {
                                markerData.color = chart.graphs[0].lineColor;
                            }
                            data.dataIdx = idx; //also store it in the dataProvider object itself
                            return markerData;
                        });

                        chart.legend.data = legendData;

                        //make the markers toggleable
                        chart.legend.switchable = true;
                        chart.legend.addListener("clickMarker", handleCustomMarkerToggle);

                    }, ["serial"]);

                    var chart = AmCharts.makeChart("divChartPerSectionMonthly", {
                        "type": "serial",
                        "theme": "multichart",
                        "titles": [{
                            "text": "LPA Results"
                        }, {
                            "text": "(average)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "gridColor": "#FFFFFF",
                            "gridAlpha": 0,
                            "dashLength": 0,
                            "minimum": 0,
                            "maximum": 100,
                            "minMaxMultiplier": 1.5,
                            "gridCount": 25
                        }],
                        "gridAboveGraphs": true,
                        "startDuration": 1,
                        "graphs": [{
                            "type": "column",
                            "title": choice1,
                            "balloonText": "[[title]]: <b>[[value]]</b>",
                            "bullet": "round",
                            "bulletSize": 10,
                            "bulletBorderColor": "#ffffff",
                            "bulletBorderAlpha": 1,
                            "bulletBorderThickness": 2,
                            "fillAlphas": 1,
                            "lineAlpha": 0.2,
                            "fillColorsField": "color1",
                            "labelText": "[[value]]%",
                            "labelPosition": "left",
                            "valueField": "average_score"
                        },
                        {
                            "type": "column",
                            "title": choice2,
                            "theme": "none",
                            "balloonText": "[[title]]: <b>[[value]]</b>",
                            "bullet": "round",
                            "bulletSize": 10,
                            "bulletBorderColor": "#ffffff",
                            "bulletBorderAlpha": 1,
                            "bulletBorderThickness": 2,
                            "fillAlphas": 1,
                            "lineAlpha": 0.2,
                            "fillColorsField": "color2",

                            "labelText": "[[value]]%",
                            "labelPosition": "left",
                            "valueField": "average_score_2"
                        }, {
                            "type": "column",
                            "title": choice3,
                            "balloonText": "[[title]]: <b>[[value]]</b>",
                            "bullet": "round",
                            "bulletSize": 10,
                            "bulletBorderColor": "#ffffff",
                            "bulletBorderAlpha": 1,
                            "bulletBorderThickness": 2,
                            "fillAlphas": 1,
                            "lineAlpha": 0.2,
                            "fillColorsField": "color3",
                            "labelText": "[[value]]%",
                            "labelPosition": "left",
                            "valueField": "average_score_3"
                        }],
                        "balloon": {
                            "fillAlpha": 1
                        },
                        //"chartCursor": {
                        //    "categoryBalloonEnabled": false,
                        //    "cursorAlpha": 0,
                        //    "zoomable": false
                        //},
                        "categoryField": "audit_period",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "gridAlpha": 0
                        },
                        "legend": {
                            "position": "absolute",
                            "top": 10,
                            "left": 300,
                            // "fill":"#eb690f",
                            "useGraphSettings": true
                        },
                        "export": {
                            "enabled": true,
                            "menu": [{
                                "class": "export-main",
                                "menu": [{
                                    "label": "Download",
                                    "menu": ["PNG", "JPG", "CSV", {
                                        "format": "PDF",
                                        "content": [
                                          {
                                              "image": "reference",
                                              "fit": [500.28, 749.89] // fit image to A4
                                          }
                                        ]
                                    }]
                                }, {
                                    "label": "Save",
                                    "menu": ["CSV", "JSON"]
                                },
                                {
                                    "label": "Annotate",
                                    "action": "draw",
                                    "menu": [{
                                        "class": "export-drawing",
                                        "menu": ["PNG", "JPG"]
                                    }]
                                }, {
                                    "format": "PRINT",
                                    "label": "PRINT"
                                }
                                ]
                            }]
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }

        function LoadChartPerSection() {
            var return_first = function () {
                var tmp = null;
                $.ajax({
                    type: "POST",
                    url: 'ComparisonReport.aspx/returnLPAResultPerSection',
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        tmp = msg.d;
                        //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];
                        var chart = AmCharts.makeChart("divChartPerSection", {
                            "type": "radar",
                            "theme": "light",
                            "dataProvider": tmp,
                            "valueAxes": [{
                                "axisTitleOffset": 20,
                                "minimum": 0,
                                "maximum": 100,
                                "axisAlpha": 0.15
                            }],
                            "startDuration": 2,
                            "graphs": [{
                                "balloonText": "[[category]]: <b>[[value]]</b>",
                                "bullet": "round",
                                "lineThickness": 2,
                                "bulletBorderColor": "#ffffff",
                                "fillColorsField": "color1",
                                "labelText": "[[value]]%",
                                "labelPosition": "left",
                                "valueField": "Score"
                            }],
                            "balloon": {
                                "fillAlpha": 1
                            },
                            "categoryField": "Sec_name_abbr",

                            "export": {
                                "enabled": true,
                                "menu": [{
                                    "class": "export-main",
                                    "menu": [{
                                        "label": "Download",
                                        "menu": ["PNG", "JPG", "CSV", {
                                            "format": "PDF",
                                            "content": [
                                              {
                                                  "image": "reference",
                                                  "fit": [500.28, 749.89] // fit image to A4
                                              }
                                            ]
                                        }]
                                    }, {
                                        "label": "Save",
                                        "menu": ["CSV", "JSON"]
                                    },
                                    {
                                        "label": "Annotate",
                                        "action": "draw",
                                        "menu": [{
                                            "class": "export-drawing",
                                            "menu": ["PNG", "JPG"]
                                        }]
                                    }, {
                                        "format": "PRINT",
                                        "label": "PRINT"
                                    }
                                    ]
                                }]
                            }
                        });
                        chart.dataProvider = AmCharts.parseJSON(tmp);
                        chart.validateData();
                        var chart1 = AmCharts.makeChart("divChartPerSection2", {
                            "type": "radar",
                            "theme": "none",
                            "titles": [{
                                "text": "LPA Results per Section"
                            }, {
                                "text": "(12 months average)",
                                "bold": false,
                                "size": 10

                            }],
                            "dataProvider": tmp,
                            "valueAxes": [{
                                "axisTitleOffset": 20,
                                "minimum": 0,
                                "axisAlpha": 0.15
                            }],
                            "startDuration": 2,
                            "graphs": [{
                                "balloonText": "[[category]]: <b>[[value]]</b>",
                                "bullet": "round",
                                "lineThickness": 2,
                                "bulletBorderColor": "#ffffff",
                                "labelText": "[[value]]%",
                                "labelPosition": "left",
                                "valueField": "Score_2"
                            }],
                            "balloon": {
                                "fillAlpha": 1
                            },
                            "categoryField": "Sec_name_abbr",

                            "export": {
                                "enabled": true,
                                "menu": [{
                                    "class": "export-main",
                                    "menu": [{
                                        "label": "Download",
                                        "menu": ["PNG", "JPG", "CSV", {
                                            "format": "PDF",
                                            "content": [
                                              {
                                                  "image": "reference",
                                                  "fit": [500.28, 749.89] // fit image to A4
                                              }
                                            ]
                                        }]
                                    }, {
                                        "label": "Save",
                                        "menu": ["CSV", "JSON"]
                                    },
                                    {
                                        "label": "Annotate",
                                        "action": "draw",
                                        "menu": [{
                                            "class": "export-drawing",
                                            "menu": ["PNG", "JPG"]
                                        }]
                                    }, {
                                        "format": "PRINT",
                                        "label": "PRINT"
                                    }
                                    ]
                                }]
                            }
                        });
                        chart1.dataProvider = AmCharts.parseJSON(tmp);
                        chart1.validateData();
                        var chart2 = AmCharts.makeChart("divChartPerSection3", {
                            "type": "radar",
                            "theme": "gray",
                            "dataProvider": tmp,
                            "valueAxes": [{
                                "axisTitleOffset": 20,
                                "minimum": 0,
                                "axisAlpha": 0.15
                            }],
                            "startDuration": 2,
                            "graphs": [{
                                "balloonText": "[[category]]: <b>[[value]]</b>",
                                "bullet": "round",
                                "lineThickness": 2,
                                "bulletBorderColor": "#ffffff",
                                "labelText": "[[value]]%",
                                "labelPosition": "left",
                                "valueField": "Score_3"
                            }],
                            "balloon": {
                                "fillAlpha": 1
                            },
                            "categoryField": "Sec_name_abbr",

                            "export": {
                                "enabled": true,
                                "menu": [{
                                    "class": "export-main",
                                    "menu": [{
                                        "label": "Download",
                                        "menu": ["PNG", "JPG", "CSV", {
                                            "format": "PDF",
                                            "content": [
                                              {
                                                  "image": "reference",
                                                  "fit": [500.28, 749.89] // fit image to A4
                                              }
                                            ]
                                        }]
                                    }, {
                                        "label": "Save",
                                        "menu": ["CSV", "JSON"]
                                    },
                                    {
                                        "label": "Annotate",
                                        "action": "draw",
                                        "menu": [{
                                            "class": "export-drawing",
                                            "menu": ["PNG", "JPG"]
                                        }]
                                    }, {
                                        "format": "PRINT",
                                        "label": "PRINT"
                                    }
                                    ]
                                }]
                            }
                        });
                        chart2.dataProvider = AmCharts.parseJSON(tmp);
                        chart2.validateData();
                    },
                    error: function (e) {
                        //console(e);
                    }
                });


                console.log("tmp2 : " + tmp);
                return tmp;
            }();
            console.log("return_first : " + return_first);

        }

        function LoadChartPerQuestion() {
            var return_first = function () {
                var tmp = null;
                $.ajax({
                    type: "POST",
                    url: 'ComparisonReport.aspx/returnLPAResultPerQuestion',
                    data: "",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        tmp = msg.d;

                        var choice1;
                        if ($('#MainContent_ddlRegion1 :selected').val() == 0) {
                            choice1 = $('#MainContent_ddlRegion1 :selected').text();
                        }
                        else {
                            if ($('#MainContent_ddlCountry1 :selected').val() != 0) {

                                //choice1 = $('#MainContent_ddlRegion1 :selected').text() + '-' + $('#MainContent_ddlCountry1 :selected').text();
                                if ($('#MainContent_ddlLocation1 :selected').val() != 0) {
                                    choice1 = $('#MainContent_ddlLocation1 :selected').text();
                                    if ($('#MainContent_ddlLine1 :selected').val() != 0)
                                        choice1 = $('#MainContent_ddlLocation1 :selected').text() + '-' + $('#MainContent_ddlLine1 :selected').text();
                                }
                                else
                                    choice1 = $('#MainContent_ddlCountry1 :selected').text();

                            }
                            else {
                                choice1 = $('#MainContent_ddlRegion1 :selected').text();
                            }


                        }
                        console.log('Choice1');
                        console.log(choice1);
                        var choice2;
                        if ($('#MainContent_ddlRegion2 :selected').val() == 0) {
                            choice2 = $('#MainContent_ddlRegion2 :selected').text();
                        }
                        else {
                            if ($('#MainContent_ddlCountry2 :selected').val() != 0) {

                                //choice1 = $('#MainContent_ddlRegion1 :selected').text() + '-' + $('#MainContent_ddlCountry1 :selected').text();
                                if ($('#MainContent_ddlLocation2 :selected').val() != 0) {
                                    choice2 = $('#MainContent_ddlLocation2 :selected').text();
                                    if ($('#MainContent_ddlLine2 :selected').val() != 0)
                                        choice2 = $('#MainContent_ddlLocation2 :selected').text() + '-' + $('#MainContent_ddlLine2 :selected').text();
                                }
                                else
                                    choice2 = $('#MainContent_ddlCountry2 :selected').text();

                            }
                            else {
                                choice2 = $('#MainContent_ddlRegion2 :selected').text();
                            }


                        }

                        var choice3;
                        if ($('#MainContent_ddlRegion3 :selected').val() == 0) {
                            choice3 = $('#MainContent_ddlRegion3 :selected').text();
                        }
                        else {
                            if ($('#MainContent_ddlCountry3 :selected').val() != 0) {

                                //choice1 = $('#MainContent_ddlRegion1 :selected').text() + '-' + $('#MainContent_ddlCountry1 :selected').text();
                                if ($('#MainContent_ddlLocation3 :selected').val() != 0) {
                                    choice3 = $('#MainContent_ddlLocation3 :selected').text();
                                    if ($('#MainContent_ddlLine3 :selected').val() != 0)
                                        choice3 = $('#MainContent_ddlLocation3 :selected').text() + '-' + $('#MainContent_ddlLine3 :selected').text();
                                }
                                else
                                    choice3 = $('#MainContent_ddlCountry3 :selected').text();

                            }
                            else {
                                choice3 = $('#MainContent_ddlRegion3 :selected').text();
                            }


                        }
                        //tmp = [{"Section_number":"1","section_name":"General Topics","Score":"0.65893470790378","Sec_name_abbr":"GP"},{"Section_number":"2","section_name":"A. People Development","Score":"0.954602368866329","Sec_name_abbr":"PD"},{"Section_number":"3","section_name":"B. Safe, Organized, Clean Work Area","Score":"0.704974619289341","Sec_name_abbr":"SOCWA"},{"Section_number":"4","section_name":"C. Robust Processes and Equipment","Score":"0.918781725888325","Sec_name_abbr":"RPE"},{"Section_number":"5","section_name":"D. Standardized Work","Score":"0.922944162436547","Sec_name_abbr":"SW"},{"Section_number":"6","section_name":"E. Rapid Problem Solving / 8D","Score":"0.82186440677966","Sec_name_abbr":"RPS"}];
                        var chart = AmCharts.makeChart("divChartPerQuestion", {
                            "type": "serial",
                            "theme": "multichart",
                            //"balloon": {
                            //    "fixedPosition": true
                            //},
                            //"chartCursor": {
                            //    "cursorColor": "#55bb76",
                            //    "valueBalloonsEnabled": false,
                            //    "cursorAlpha": 0,
                            //    "valueLineAlpha": 0.5,
                            //    "valueLineBalloonEnabled": true,
                            //    "valueLineEnabled": true,
                            //    "zoomable": false,
                            //    "valueZoomable": true
                            //},
                            "titles": [{
                                "text": "LPA Results per Question"
                            }, {
                                "text": "(12 months average)",
                                "bold": false,
                                "size": 10
                            }],
                            "dataProvider": tmp,
                            "valueAxes": [{
                                "gridColor": "#FFFFFF",
                                "gridAlpha": 0,
                                "dashLength": 0,
                                "minimum": 0,
                                "maximum": 100,
                                "minMaxMultiplier": 1.5,
                                //"gridCount": 25
                            }],
                            "gridAboveGraphs": true,
                            "startDuration": 1,
                            "graphs": [{
                                "type": "column",
                                "title": choice1,
                                "balloonText": "[[question]]",
                                "bullet": "round",
                                "bulletSize": 10,
                                "bulletBorderColor": "#ffffff",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 2,
                                "fillAlphas": 1,
                                "lineAlpha": 0.2,
                                "fillColorsField": "color1",
                                "labelText": "[[value]]%",
                                "labelPosition": "bottom",
                                "valueField": "score"
                            },
                            {
                                "type": "column",
                                "title": choice2,
                                "balloonText": "[[question]]",
                                "bullet": "round",
                                "bulletSize": 10,
                                "bulletBorderColor": "#ffffff",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 2,
                                "fillAlphas": 1,
                                "lineAlpha": 0.2,
                                "fillColorsField": "color2",
                                "labelText": "[[value]]%",
                                "labelPosition": "left",
                                "valueField": "score_2"

                            },
                            {
                                "type": "column",
                                "title": choice3,
                                "balloonText": "[[question]]",
                                "bullet": "round",
                                "bulletSize": 10,
                                "bulletBorderColor": "#ffffff",
                                "bulletBorderAlpha": 1,
                                "bulletBorderThickness": 2,
                                "fillAlphas": 1,
                                "lineAlpha": 0.2,
                                "fillColorsField": "color3",
                                "labelText": "[[value]]%",
                                "labelPosition": "right",
                                "valueField": "score_3"

                            }],
                            "balloon": {
                                "fillAlpha": 1
                            },
                            //"chartCursor": {
                            //    "categoryBalloonEnabled": false,
                            //    "cursorAlpha": 0,

                            //    "zoomable": false
                            //},
                            "categoryField": "question_id",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "gridAlpha": 0
                            },
                            "legend": {
                                //"position": "absolute",
                                "position": "bottom",
                                "marginTop": 0,
                                "left": 100,
                                "autoMargins": false,
                                "marginBottom": 20,
                                //"align":"right",
                                "useGraphSettings": false
                            },
                            "export": {
                                "enabled": true,
                                "menu": [{
                                    "class": "export-main",
                                    "menu": [{
                                        "label": "Download",
                                        "menu": ["PNG", "JPG", "CSV", {
                                            "format": "PDF",
                                            "content": [
                                              {
                                                  "image": "reference",
                                                  "fit": [500.28, 749.89] // fit image to A4
                                              }
                                            ]
                                        }]
                                    }, {
                                        "label": "Save",
                                        "menu": ["CSV", "JSON"]
                                    },
                                    {
                                        "label": "Annotate",
                                        "action": "draw",
                                        "menu": [{
                                            "class": "export-drawing",
                                            "menu": ["PNG", "JPG"]
                                        }]
                                    }, {
                                        "format": "PRINT",
                                        "label": "PRINT"
                                    }
                                    ]
                                }]
                            }
                        });
                        chart.dataProvider = AmCharts.parseJSON(tmp);
                        chart.validateData();
                    },
                    error: function (e) {
                        //console(e);
                    }
                });
                return tmp;
            }();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="col-md-12">
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Reports</a>
                        </li>
                        <li class="current">Comparison Report</li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
    <%--from--%>
    <div class="md_main">
        <div class="row">
            <div class="mannHummel" id="mannHummelId">
                <%--<div class="clearfix" style="width: 40px;"></div>--%>
                <asp:Panel runat="server" ID="pnlFilters">
                    <div class="filter_panel">
                        <div>
                            <div class="col-md-2"></div>
                            <div class="col-md-2 filter_label">
                                <p>Division* : </p>
                            </div>
                            <div class="col-md-2 filter_label">
                                <p>Region* : </p>
                            </div>
                            <div class="col-md-2 filter_label">
                                <p>Country* : </p>
                            </div>
                            <div class="col-md-2 filter_label">
                                <p>Location* : </p>
                            </div>
                            
                            <div class="col-md-2 filter_label">
                                <p>Zone* : </p>
                            </div>
                        </div>

                        <div>
                            <div class="col-md-2 filter_label">
                                <p style="float: right;">Select 1st Comparison*:</p>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlDivision1" OnSelectedIndexChanged="ddlDivision1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion1" OnSelectedIndexChanged="ddlRegion1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry1" OnSelectedIndexChanged="ddlCountry1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation1" OnSelectedIndexChanged="ddlLocation1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                             
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLine1" OnSelectedIndexChanged="ddlLine1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                            </div>
                        </div>

                        <div>
                            <div class="col-md-2 filter_label">
                                <p style="float: right;">Select 2nd Comparison*:</p>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlDivision2" OnSelectedIndexChanged="ddlDivision2_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion2" OnSelectedIndexChanged="ddlRegion2_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry2" OnSelectedIndexChanged="ddlCountry2_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation2" OnSelectedIndexChanged="ddlLocation2_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLine2" OnSelectedIndexChanged="ddlLine2_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                            </div>
                        </div>

                        <div>
                            <div class="col-md-2 filter_label">
                                <p style="float: right;">Select 3rd Comparison*:</p>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlDivision3" OnSelectedIndexChanged="ddlDivision3_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion3" OnSelectedIndexChanged="ddlRegion3_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry3" OnSelectedIndexChanged="ddlCountry3_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation3" OnSelectedIndexChanged="ddlLocation3_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLine3" OnSelectedIndexChanged="ddlLine3_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                            </div>
                        </div>

                        <div>
                            <div class="col-md-2 filter_label">
                                <p style="float: right;">Date* : </p>
                            </div>
                            <div class="col-md-2">
                                <asp:TextBox class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="txtDate"></asp:TextBox>
                            </div>

                            <div class="col-md-4 filter_label">
                                <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                            </div>
                            <div class="col-md-4">
                                <div>
                                    <asp:Button runat="server" CssClass="btn-add" ID="btnClear" Style="margin-left: 4px;" OnClick="btnClear_Click" Text="Clear" />
                                </div>
                                <div>
                                    <asp:Button runat="server" CssClass="btn-add" ID="btnFilter" Style="margin-left: 4px;" Text="Fetch Report" OnClick="btnFilter_Click" OnClientClick="return ValidateControls();" />
                                </div>
                            </div>
                        </div>

                    </div>

                </asp:Panel>
                <div class="clearfix" style="height: 5px;"></div>
                <div class="col-md-12">
                    <div id="divChartPerSectionMonthly"></div>
                </div>
                <div class="col-md-12">
                    <div class="col-md-4" id="divChartPerSection"></div>
                    <div class="col-md-4" id="divChartPerSection2"></div>
                    <div class="col-md-4" id="divChartPerSection3"></div>
                </div>
                <div class="mn_fetch">
                    <div class="col-md-4 mn_mrg_left" style="background-color: white;">
                        <asp:Label runat="server" ID="sec1"></asp:Label>
                    </div>
                    <div class="col-md-4 mn_mrg" style="background-color: white;">
                        <asp:Label runat="server" ID="sec2"></asp:Label>
                    </div>
                    <div class="col-md-4 mn_mrg_right" style="background-color: white;">
                        <asp:Label runat="server" ID="sec3"></asp:Label>
                    </div>
                    <div class="col-md-4 mn_mrg_left" style="background-color: white;">
                        <asp:Label runat="server" ID="sec4"></asp:Label>
                    </div>
                    <div class="col-md-4 mn_mrg" style="background-color: white;">
                        <asp:Label runat="server" ID="sec5"></asp:Label>
                    </div>
                    <div class="col-md-4 mn_mrg_right" style="background-color: white;">
                        <asp:Label runat="server" ID="sec6"></asp:Label>
                    </div>
                </div>
                <div class="col-md-12">
                    <div id="divChartPerQuestion"></div>
                </div>

                <asp:Panel runat="server" Visible="false" ID="pnlReport">
                    <div class="ma_lpa_result">
                        <asp:Chart ID="Chart1" runat="server" Style="margin: 0 auto; display: block; height: auto;"
                            BackGradientStyle="LeftRight" Palette="None"
                            PaletteCustomColors="green" Width="1000px">
                            <Titles>
                                <asp:Title Font="16pt, style=Bold" Name="LPA Result"></asp:Title>
                            </Titles>
                            <Series>
                                <asp:Series ChartType="Area"></asp:Series>
                                <asp:Series ChartType="Area"></asp:Series>
                                <asp:Series ChartType="Area"></asp:Series>
                            </Series>
                            <Legends>
                                <asp:Legend ShadowColor="DarkGreen"></asp:Legend>
                                <asp:Legend ShadowColor="DarkOrange"></asp:Legend>
                                <asp:Legend ShadowColor="Gray"></asp:Legend>
                            </Legends>
                            <ChartAreas>
                                <asp:ChartArea Name="cA1"></asp:ChartArea>
                            </ChartAreas>

                        </asp:Chart>
                    </div>
                    <div class="clearfix" style="height: 5px;"></div>
                    <div class="col-md-4 mn_mrg_left">
                        <asp:Chart ID="ChartPerSection1" runat="server" Style="border-width: 0px; margin: 0 auto; display: block;"
                            BackGradientStyle="LeftRight" Palette="None"
                            PaletteCustomColors="green">
                            <Titles>
                                <asp:Title Font="16pt, style=Bold" Name="t2"></asp:Title>
                            </Titles>


                            <Series>
                                <asp:Series Name="Series1" ChartType="Radar"></asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ca2"></asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>
                    </div>
                    <div class="col-md-4 mn_mrg">
                        <asp:Chart ID="ChartPerSection2" runat="server" Style="border-width: 0px; margin: 0 auto; display: block;"
                            BackGradientStyle="LeftRight" Palette="None"
                            PaletteCustomColors="green">
                            <Titles>
                                <asp:Title Font="16pt, style=Bold" Name="t2"></asp:Title>
                            </Titles>
                            <Series>
                                <asp:Series Name="Series1" ChartType="Radar"></asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ca2"></asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>
                    </div>
                    <div class="col-md-4 mn_mrg_right">
                        <asp:Chart ID="ChartPerSection3" runat="server" Style="border-width: 0px; margin: 0 auto; display: block;"
                            BackGradientStyle="LeftRight" Palette="None"
                            PaletteCustomColors="green">
                            <Titles>
                                <asp:Title Font="16pt, style=Bold" Name="t2"></asp:Title>
                            </Titles>
                            <Series>
                                <asp:Series Name="Series1" ChartType="Radar"></asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ca2"></asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>
                    </div>

                    <div class="clearfix" style="height: 5px;"></div>


                    <div class="ma_lpa_result">
                        <asp:Chart ID="ChartPerQuestion" runat="server" Style="margin: 0 auto; display: block; height: auto;"
                            BackGradientStyle="LeftRight" Palette="None"
                            PaletteCustomColors="green" Width="1000px">
                            <Titles>
                                <asp:Title Font="16pt, style=Bold" Name="t5"></asp:Title>
                            </Titles>
                            <Series>
                                <asp:Series ChartType="Column" Name="Category1"></asp:Series>
                                <asp:Series ChartType="Column" Name="Category2"></asp:Series>
                                <asp:Series ChartType="Column" Name="Category3"></asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ca5"></asp:ChartArea>
                            </ChartAreas>
                            <Legends>
                                <asp:Legend Alignment="Far" HeaderSeparator="Line" Name="category1" ShadowColor="DarkGreen"></asp:Legend>
                                <asp:Legend Alignment="Center" Name="category2" ShadowColor="DarkOrange"></asp:Legend>
                                <asp:Legend Alignment="Center" Name="category3" ShadowColor="Gray"></asp:Legend>
                            </Legends>
                        </asp:Chart>
                    </div>
                </asp:Panel>
            </div>
        </div>

    </div>
</asp:Content>
