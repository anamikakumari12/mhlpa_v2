﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Division.aspx.cs" Inherits="MH_Logistics.Division" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function validateControls() {
            //debugger
            var err_flag = 0;
            if ($('#MainContent_txtflag').val() == "") {
                $('#MainContent_txtflag').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_txtflag').css('border-color', '');
            }
            if ($('#MainContent_txtname').val() == "") {
                $('#MainContent_txtname').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtname').css('border-color', '');
            }
            if ($('#MainContent_txtSection1').val() == "") {
                $('#MainContent_txtSection1').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtSection1').css('border-color', '');
            }
            if ($('#MainContent_txtSection2').val() == "") {
                $('#MainContent_txtSection2').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtSection2').css('border-color', '');
            }
            if ($('#MainContent_txtSection3').val() == "") {
                $('#MainContent_txtSection3').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtSection3').css('border-color', '');
            }
            if ($('#MainContent_txtSection4').val() == "") {
                $('#MainContent_txtSection4').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtSection4').css('border-color', '');
            }
            if ($('#MainContent_txtSection5').val() == "") {
                $('#MainContent_txtSection5').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtSection5').css('border-color', '');
            }
            if ($('#MainContent_txtSection6').val() == "") {
                $('#MainContent_txtSection6').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtSection6').css('border-color', '');
            }
            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return setmin();

            }
            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields.');
                return false;
            }
        }
       
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">SetUp</a>
                        </li>
                        <li class="current">Division</li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div>
        <!-- main content start-->
        <div>
            <div class="main-page">
                <div class="row">
                    <div class="col-md-8">
                        <div class="col-md-12 nopad">
                            <div class="panel panel-default panel-table">
                                <div class="panel-group tool-tips widget-shadow" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="col-md-12 nopad">
                                        <h4 class="title2">Division Flag</h4>
                                    </div>
                                    <hr />
                                    <div class="col-md-12 nopad mn_table">
                                        <asp:GridView ID="grdDivision" runat="server" AllowPaging="true" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="division_id" CssClass="dictionary" Width="100%" HeaderStyle-BackColor="#ff6c52" AlternatingRowStyle-BackColor="#cccccc" OnPageIndexChanging="grdDivision_PageIndexChanging">

                                            <Columns>

                                                <asp:TemplateField HeaderText="Division Flag" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkflag" runat="server" Text='<%# Bind("Division_flag") %>' OnClick="Select" CommandArgument='<%# Bind("division_id") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkname" runat="server" Visible="true" Text='<%# Bind("Division_Name") %>' OnClick="Select" CommandArgument='<%# Bind("division_id") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                               
                                                <asp:TemplateField HeaderText="Action" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="50px">
                                                    <ItemTemplate>
                                                        <asp:ImageButton runat="server" Width="20px" ToolTip="Edit" ImageUrl="images/edit_icon.jpg" ID="imgAction" OnClick="Edit" CommandArgument='<%# Bind("division_id") %>' />
                                                        <asp:ImageButton runat="server" Width="20px" ToolTip="Delete" ImageUrl="images/delete_icon.png" ID="imgDelete" OnClick="Delete" CommandArgument='<%# Bind("division_id") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>

                                        </asp:GridView>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel-group tool-tips widget-shadow">
                            <div class="col-md-12 nopad">
                                <asp:Button runat="server" CssClass="btn-add" ID="btnAdd" Text="Add" OnClick="btnAdd_Click" />
                            </div>
                            <hr />
                            <asp:Panel runat="server" ID="viewpanel" Visible="true">
                                <div class="col-md-12 nopad">
                                    <div class="row-info">
                                        <div class="col-md-6 nopad">
                                            <p>Division Flag: </p>
                                        </div>
                                        <div class="col-md-6 nopad">
                                            <asp:Label runat="server" ID="lblflag"></asp:Label></div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-6 nopad">
                                            <p>Name: </p>
                                        </div>
                                        <div class="col-md-6 nopad">
                                            <asp:Label runat="server" ID="lblname"></asp:Label></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-6 nopad">
                                            <p>Section 1: </p>
                                        </div>
                                        <div class="col-md-6 nopad">
                                            <asp:Label runat="server" ID="lblSection1"></asp:Label></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-6 nopad">
                                            <p>Section 2: </p>
                                        </div>
                                        <div class="col-md-6 nopad">
                                            <asp:Label runat="server" ID="lblSection2"></asp:Label></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-6 nopad">
                                            <p>Section 3: </p>
                                        </div>
                                        <div class="col-md-6 nopad">
                                            <asp:Label runat="server" ID="lblSection3"></asp:Label></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-6 nopad">
                                            <p>Section 4: </p>
                                        </div>
                                        <div class="col-md-6 nopad">
                                            <asp:Label runat="server" ID="lblSection4"></asp:Label></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-6 nopad">
                                            <p>Section 5: </p>
                                        </div>
                                        <div class="col-md-6 nopad">
                                            <asp:Label runat="server" ID="lblSection5"></asp:Label></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-6 nopad">
                                            <p>Section 6: </p>
                                        </div>
                                        <div class="col-md-6 nopad">
                                            <asp:Label runat="server" ID="lblSection6"></asp:Label></div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="addpanl" Visible="false">
                                <div class="col-md-12 nopad mn_mar_5">
                                    <div class="row-info ">
                                        <div class="col-md-5 nopad">
                                            <p>Division Flag* : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="txtflag"></asp:TextBox>
                                         </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p style="line-height: 18px!important;">Name* : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="txtname"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p style="line-height: 18px!important;">Section 1* : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="txtSection1"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p style="line-height: 18px!important;">Section 2* : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="txtSection2"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p style="line-height: 18px!important;">Section 3* : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="txtSection3"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p style="line-height: 18px!important;">Section 4* : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="txtSection4"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p style="line-height: 18px!important;">Section 5* : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="txtSection5"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="row-info">
                                        <div class="col-md-5 nopad">
                                            <p style="line-height: 18px!important;">Section 6* : </p>
                                        </div>
                                        <div class="col-md-7 nopad">
                                            <asp:TextBox class="form-control1 mn_inp control3" runat="server" ID="txtSection6"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                                <asp:Button runat="server" ID="btnSave" CssClass="btn-add" Text="Save" OnClick="btnSave_Click" OnClientClick="return validateControls();" />
                            </asp:Panel>


                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>

        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>

