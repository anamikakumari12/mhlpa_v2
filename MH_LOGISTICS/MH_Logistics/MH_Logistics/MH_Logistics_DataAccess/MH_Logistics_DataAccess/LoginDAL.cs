﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH_Logistics_BusinessObject;

namespace MH_Logistics_DataAccess
{
    public class LoginDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataTable authLoginDAL(LoginBO objLogin)
        {
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            DataTable dtOutput = new DataTable();
            SqlDataAdapter sqlAd;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.getLoginInfoProcedure, sqlconn);
                sqlconn.Open();
                
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.user_name, SqlDbType.VarChar, 100).Value = objLogin.username;
                sqlcmd.Parameters.Add(ResourceFileDAL.user_pwd, SqlDbType.VarChar, -1).Value = objLogin.encryptedPassword;
                sqlAd = new SqlDataAdapter(sqlcmd);
                sqlAd.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public string RequestPasswordDAL(LoginBO objLogin)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("InsProfileChangeReq", connection);
                command.CommandType = CommandType.StoredProcedure;


                command.Parameters.Add("@p_user_id", SqlDbType.Int).Value = 0;
                command.Parameters.Add("@p_username", SqlDbType.VarChar, 100).Value = objLogin.username;
                command.Parameters.Add("@p_comments", SqlDbType.VarChar, 100).Value = objLogin.p_comments;
                Output = Convert.ToString(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return Output;
        }
    }
}
