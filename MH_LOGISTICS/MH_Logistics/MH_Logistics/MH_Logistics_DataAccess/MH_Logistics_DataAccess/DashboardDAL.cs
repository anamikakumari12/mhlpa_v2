﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH_Logistics_BusinessObject;

namespace MH_Logistics_DataAccess
{
    public class DashboardDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataTable getDataforLPAResultsReportDAL(DashboardBO objDashBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand("sp_getLPAResultMonthly", sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_division_id, SqlDbType.Int).Value = objDashBO.division_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_region_id, SqlDbType.Int).Value = objDashBO.region_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_id, SqlDbType.Int).Value = objDashBO.country_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objDashBO.location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objDashBO.line_id;
                sqlcmd.Parameters.Add("@p_date", SqlDbType.Date).Value = objDashBO.reportdate;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }


        public DataTable getLPAResultPerSectionDAL(DashboardBO objDashBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand("sp_getLPAResultPerSection", sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_division_id, SqlDbType.Int).Value = objDashBO.division_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_region_id, SqlDbType.Int).Value = objDashBO.region_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_id, SqlDbType.Int).Value = objDashBO.country_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objDashBO.location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objDashBO.line_id;
                sqlcmd.Parameters.Add("@p_date", SqlDbType.Date).Value = objDashBO.reportdate;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }
        public DataTable getLPAResultPerSectionMonthlyDAL(DashboardBO objDashBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand("sp_getLPAResultPerSectionMonthly", sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_division_id, SqlDbType.Int).Value = objDashBO.division_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_region_id, SqlDbType.Int).Value = objDashBO.region_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_id, SqlDbType.Int).Value = objDashBO.country_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objDashBO.location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objDashBO.line_id;
                sqlcmd.Parameters.Add("@p_date", SqlDbType.Date).Value = objDashBO.reportdate;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable getLPAResultByLineDAL(DashboardBO objDashBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand("sp_getLPAResultByLine", sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_division_id, SqlDbType.Int).Value = objDashBO.division_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_region_id, SqlDbType.Int).Value = objDashBO.region_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_id, SqlDbType.Int).Value = objDashBO.country_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objDashBO.location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objDashBO.line_id;
                sqlcmd.Parameters.Add("@p_date", SqlDbType.Date).Value = objDashBO.reportdate;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable getLPAResultByQuestionDAL(DashboardBO objDashBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand("sp_getLPAResultByQuestion", sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_division_id, SqlDbType.Int).Value = objDashBO.division_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_region_id, SqlDbType.Int).Value = objDashBO.region_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_id, SqlDbType.Int).Value = objDashBO.country_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objDashBO.location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objDashBO.line_id;
                sqlcmd.Parameters.Add("@p_date", SqlDbType.Date).Value = objDashBO.reportdate;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataSet getFilterDataDAL(DashboardBO objDashBO)
        {
            DataSet dsOutput = new DataSet();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand("sp_getDropDownDetailsForReport", sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_region_id, SqlDbType.Int).Value = objDashBO.region_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_id, SqlDbType.Int).Value = objDashBO.country_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objDashBO.location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_division_id, SqlDbType.Int).Value = objDashBO.division_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objDashBO.line_id;
                sqlcmd.Parameters.Add("@p_Current_field", SqlDbType.VarChar,100).Value = objDashBO.selection_flag;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dsOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dsOutput;
        }

        public DataTable getDataforLPAComparisionReportDAL(DashboardBO objDashBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand("sp_getLPAPlanVsActuals", sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_division_id, SqlDbType.Int).Value = objDashBO.division_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_region_id, SqlDbType.Int).Value = objDashBO.region_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_id, SqlDbType.Int).Value = objDashBO.country_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objDashBO.location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objDashBO.line_id;
                sqlcmd.Parameters.Add("@p_date", SqlDbType.Date).Value = objDashBO.reportdate;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetAuditReportsDAL(DashboardBO objDashBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getAuditReportPaths, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_region_id, SqlDbType.Int).Value = objDashBO.region_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_id, SqlDbType.Int).Value = objDashBO.country_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objDashBO.location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objDashBO.line_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.Auditfromdate, SqlDbType.VarChar, 100).Value = objDashBO.startdate;
                sqlcmd.Parameters.Add(ResourceFileDAL.Audittodate, SqlDbType.VarChar, 100).Value = objDashBO.enddate;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetAuditResultsForDownloadDAL(DashboardBO objDashBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.AuditResultsForDownload, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_division_id, SqlDbType.Int).Value = objDashBO.division_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_region_id, SqlDbType.Int).Value = objDashBO.region_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_id, SqlDbType.Int).Value = objDashBO.country_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objDashBO.location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objDashBO.line_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_st_date, SqlDbType.Date).Value = objDashBO.l_st_date;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_end_date, SqlDbType.Date).Value =objDashBO.l_end_date;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetPerformanceReportForDownloadDAL(DashboardBO objDashBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.AuditPerformanceForDownload, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_region_id, SqlDbType.Int).Value = objDashBO.region_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_id, SqlDbType.Int).Value = objDashBO.country_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objDashBO.location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objDashBO.line_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_st_date, SqlDbType.Date).Value = objDashBO.l_st_date;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_end_date, SqlDbType.Date).Value = objDashBO.l_end_date;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }
    }
}
