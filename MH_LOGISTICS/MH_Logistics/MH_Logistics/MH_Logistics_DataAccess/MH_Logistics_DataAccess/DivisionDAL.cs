﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MH_Logistics_BusinessObjects;
using MH_Logistics_BusinessObject;
using System.Data.SqlClient;
using System.Configuration;

namespace MH_Logistics_DataAccess
{
    public class DivisionDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataTable GetDivisionDAL()
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlada;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getDivisions, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlada = new SqlDataAdapter(sqlcmd);
                sqlada.Fill(dtOutput);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtOutput;
        }

        public DivisionBO DeleteDivisionDAL(DivisionBO objDivBO)
        {
            DivisionBO objOutput = new DivisionBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_DelDivision, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_division_id, SqlDbType.Int).Value = objDivBO.division_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_message, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.l_err_code].Value);
                objOutput.error_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_err_message].Value);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        public DivisionBO SaveDivisionDAL(DivisionBO objDivBO)
        {
            DivisionBO objOutput = new DivisionBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_saveDivisions, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_division_id, SqlDbType.Int).Value = objDivBO.division_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_division_flag, SqlDbType.VarChar, 10).Value = objDivBO.division_flag;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_division_name, SqlDbType.VarChar, 200).Value = objDivBO.division_name;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_section1_text, SqlDbType.VarChar, 100).Value = objDivBO.section1;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_section2_text, SqlDbType.VarChar, 100).Value = objDivBO.section2;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_section3_text, SqlDbType.VarChar, 100).Value = objDivBO.section3;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_section4_text, SqlDbType.VarChar, 100).Value = objDivBO.section4;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_section5_text, SqlDbType.VarChar, 100).Value = objDivBO.section5;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_section6_text, SqlDbType.VarChar, 100).Value = objDivBO.section6;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_message, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.l_err_code].Value);
                objOutput.error_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_err_message].Value);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        public DataTable GetDefaultSectionsDAL()
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlada;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getDefault_sections, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlada = new SqlDataAdapter(sqlcmd);
                sqlada.Fill(dtOutput);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtOutput;
        }
    }
}
