﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MH_Logistics_BusinessObjects
{
    public class DivisionBO
    {
        public string division_flag { get; set; }
        public string division_name { get; set; }
        public int division_id { get; set; }
        public string section1 { get; set; }
        public string section2 { get; set; }
        public string section3 { get; set; }
        public string section4 { get; set; }
        public string section5 { get; set; }
        public string section6 { get; set; }
        public int error_code { get; set; }
        public string error_msg { get; set; }
    }
}
