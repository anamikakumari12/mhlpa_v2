﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MH_Logistics_BusinessObject
{
    public class RoleBO
    {
        public int role_id { get; set; }
        public string rolename { get; set; }
        public string frequency { get; set; }
        public int audits_no { get; set; }
        public int error_code { get; set; }
        public string error_msg { get; set; }
    }
}
