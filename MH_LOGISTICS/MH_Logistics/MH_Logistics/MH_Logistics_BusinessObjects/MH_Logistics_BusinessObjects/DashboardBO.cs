﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MH_Logistics_BusinessObject
{
    public class DashboardBO
    {
        public int region_id { get; set; }
        public int country_id { get; set; }
        public int location_id { get; set; }
        public int line_id { get; set; }
        public string selection_flag { get; set; }
        public DateTime reportdate { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
        public DateTime l_st_date { get; set; }
        public DateTime l_end_date { get; set; }
        public int division_id { get; set; }
    }
}
