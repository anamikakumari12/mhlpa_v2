﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MH_Logistics_BusinessObject
{
    public class EmailDetails
    {
        public string toMailId { get; set; }
        public string ccMailId { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public string attachment { get; set; }
    }
    public class eAppointmentMail
    {
        public string Name { set; get; }
        public string Email { set; get; }
        public string Location { set; get; }
        public string StartDate { set; get; }
        public string EndDate { set; get; }
        public string Subject { set; get; }
        public string Body { set; get; }
        public string newStartDate { get; set; }
        public string newEndDate { get; set; }
    }
}
