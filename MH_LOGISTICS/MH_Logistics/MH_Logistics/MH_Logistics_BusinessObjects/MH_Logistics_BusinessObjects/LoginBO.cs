﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MH_Logistics_BusinessObject
{
    public class LoginBO
    {
        public string password { get; set; }
        public string username { get; set; }
        public string encryptedPassword { get; set; }
        public string decryptedPassword { get; set; }
        public int userid { get; set; }
        public int locationid { get; set; }
        public string locationName { get; set; }
        public string user_full_name { get; set; }
        public string p_comments { get; set; }
        
    }
   
}
