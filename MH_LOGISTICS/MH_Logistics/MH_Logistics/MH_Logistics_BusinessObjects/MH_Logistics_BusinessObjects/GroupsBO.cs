﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MH_Logistics_BusinessObject
{
    public class GroupsBO
    {
        public string  p_location_name { get; set; }
        public int p_location_id { get; set; }
        public string p_group_name { get; set; }
        public int p_group_id { get; set; }
        public int p_group_member_id { get; set; }
        public int p_user_id { get; set; }
    }
}
