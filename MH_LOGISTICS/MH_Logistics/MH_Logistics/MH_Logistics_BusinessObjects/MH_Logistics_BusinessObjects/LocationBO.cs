﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MH_Logistics_BusinessObject
{
    public class LocationBO
    {
        public int UserId { get; set; }
        public int line_product_rel_id { get; set; }
        public int location_id { get; set; }
        public int line_id { get; set; }
        public string region_name { get; set; }
        public string location_name { get; set; }
        public string location_code { get; set; }
        public string country_name { get; set; }
        public string part_name { get; set; }
        public string part_number { get; set; }
        public string line_name { get; set; }
        public string line_number { get; set; }
        public int no_of_shifts { get; set; }
        public TimeSpan shift1start { get; set; }
        public TimeSpan shift2start { get; set; }
        public TimeSpan shift3start { get; set; }
        public TimeSpan shift1end { get; set; }
        public TimeSpan shift2end { get; set; }
        public TimeSpan shift3end { get; set; }
        public DateTime? startdate { get; set; }
        public DateTime? enddate { get; set; }
        public int error_code { get; set; }
        public string error_msg { get; set; }
        public string p_location_id { get; set; }
        public string p_timezone_code { get; set; }
        public string p_timezone_desc { get; set; }
        public string mailIds { get; set; }
        public string  DistributionList { get; set; }
        public string division_flag { get; set; }
    }
}
