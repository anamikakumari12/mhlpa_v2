﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MH_Logistics_DataAccess;
using MH_Logistics_BusinessObjects;

namespace MH_Logistics_BusinessAccess
{
    public class DivisionBL
    {
        DivisionDAL objDivDAL = new DivisionDAL();
        public DataTable GetDivisionBL()
        {
            return objDivDAL.GetDivisionDAL();
        }

        public DivisionBO DeleteDivisionBL(DivisionBO objDivBO)
        {
            return objDivDAL.DeleteDivisionDAL(objDivBO);
        }

        public DivisionBO SaveDivisionBL(DivisionBO objDivBO)
        {
            return objDivDAL.SaveDivisionDAL(objDivBO);
        }

        public DataTable GetDefaultSectionsBL()
        {
            return objDivDAL.GetDefaultSectionsDAL();
        }
    }
}
