﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH_Logistics_DataAccess;
using MH_Logistics_BusinessObject;
using System.Data;

namespace MH_Logistics_BusinessLogic
{
    public class LocationBL
    {
        LocationDAL objLocDAL = new LocationDAL();
        public DataTable GetLinProductBL(LocationBO objLoc)
        {
            return objLocDAL.GetLinProductDAL(objLoc);
        }

        public LocationBO SaveLineProductBL(LocationBO objLocBO)
        {
            return objLocDAL.SaveLineProductBL(objLocBO);
        }

        public DataTable GetLocationDetailsBL(LocationBO objLocBO)
        {
            return objLocDAL.GetLocationDetailsDAL(objLocBO);
        }

        public LocationBO SaveLocationBL(LocationBO objLocBO)
        {
            return objLocDAL.SaveLocationDAL(objLocBO);
        }

        public DataTable GetLineDetailsBL(LocationBO objLocBO)
        {
            return objLocDAL.GetLineDetailsDAL(objLocBO);
        }

        public LocationBO SaveLineDetailsBL(LocationBO objLocBO)
        {
            return objLocDAL.SaveLineDetailsDAL(objLocBO);
        }

        public LocationBO DeleteLineDetailsBL(LocationBO objLocBO)
        {
            return objLocDAL.DeleteLineDetailsDAL(objLocBO);
        }

        public DataTable SavePartsBL(DataTable Exceldt, string flag)
        {
            return objLocDAL.SavePartsDAL(Exceldt, flag);
        }

        public LocationBO DeletePartBL(LocationBO objLocBO)
        {
            return objLocDAL.DeletePartDAL(objLocBO);
        }
    }
}
