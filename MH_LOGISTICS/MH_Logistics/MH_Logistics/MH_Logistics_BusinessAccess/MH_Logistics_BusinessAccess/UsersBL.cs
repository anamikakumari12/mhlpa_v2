﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH_Logistics_DataAccess;
using MH_Logistics_BusinessObject;
using System.Data;

namespace MH_Logistics_BusinessLogic
{
    public class UsersBL
    {
        UsersDAL objUserDAL = new UsersDAL();
        public UsersBO SaveUserDetailsBL(UsersBO objUserBO)
        {
            return objUserDAL.SaveUserDetailsDAL(objUserBO);
        }

        public DataTable GetUserDetailsBL(UsersBO objUserBO)
        {
            return objUserDAL.GetUserDetailsDAL(objUserBO);
        }

        public string ChangePasswordBL(UsersBO objUserBO)
        {
            return objUserDAL.ChangePasswordDAL(objUserBO);
        }
    }
}
