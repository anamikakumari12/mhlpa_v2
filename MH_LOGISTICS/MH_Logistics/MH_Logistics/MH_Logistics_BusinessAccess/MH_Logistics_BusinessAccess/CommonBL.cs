﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH_Logistics_DataAccess;
using MH_Logistics_BusinessObject;
using System.Data;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Web.Script.Serialization;

namespace MH_Logistics_BusinessLogic
{
    public class CommonBL
    {
        CommonDAL objComDAL = new CommonDAL();
        public DataSet GetAllMasterBL(UsersBO objUserBO)
        {
            return objComDAL.GetAllMasterDAL(objUserBO);
        }


        public IEnumerable<ImproperCalendarEvent> getEventsBL(LocationBO objLocBO)
        {
            return objComDAL.getEventsDAL(objLocBO);
        }

        public DataTable GetLineListDropDownBL(UsersBO objUserBO)
        {
            return objComDAL.GetLineListDropDownDAL(objUserBO);
        }

        public DataTable GetCountryBasedOnRegionBL(UsersBO objUserBO)
        {
            return objComDAL.GetCountryBasedOnRegionDAL(objUserBO);
        }
        public DataTable GetLocationBasedOnCountryBL(UsersBO objUserBO)
        {
            return objComDAL.GetLocationBasedOnCountryDAL(objUserBO);
        }
        /// <summary>
        /// JSON Serialization
        /// </summary>
        public string JsonSerializer<T>(T t)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream();
            ser.WriteObject(ms, t);
            string jsonString = Encoding.UTF8.GetString(ms.ToArray());
            ms.Close();
            return jsonString;
        }
        /// <summary>
        /// JSON Deserialization
        /// </summary>
        public T JsonDeserialize<T>(string jsonString)
        {
            //DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            //MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            //T obj = (T)ser.ReadObject(ms);

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            T obj =(T)json_serializer.DeserializeObject(jsonString);
            return obj;
        }

        public DataTable GetPartBasedOnLineBL(UsersBO objUserBO)
        {
            return objComDAL.GetPartBasedOnLineDAL(objUserBO);
        }

        public DataTable GetEmployeesBasedOnLocationBL(UsersBO objUserBO)
        {
            return objComDAL.GetEmployeesBasedOnLocationDAL(objUserBO);
        }

        public DataTable GetEmailListBL(UsersBO objUserBO)
        {
            return objComDAL.GetEmailListDAL(objUserBO);
        }

        public DataTable GetPartBasedOnLocBL(UsersBO objUserBO)
        {
            return objComDAL.GetPartBasedOnLocDAL(objUserBO);
        }

        public string getShiftBL(int location)
        {
            return objComDAL.getShiftDAL(location);
        }
    }
}
