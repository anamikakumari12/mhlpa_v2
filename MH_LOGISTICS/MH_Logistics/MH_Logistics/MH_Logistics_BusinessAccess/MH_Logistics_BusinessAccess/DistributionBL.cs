﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH_Logistics_DataAccess;
using MH_Logistics_BusinessObject;

namespace MH_Logistics_BusinessLogic
{
    
    public class DistributionBL
    {
        DistributionDAL objDAL = new DistributionDAL();

        public DataTable GetDistributionListBL(UsersBO objUserBO)
        {
            return objDAL.GetDistributionListDAL(objUserBO);
        }

        public DistributionBO SaveDistributionBL(DistributionBO objDistBO)
        {
            return objDAL.SaveDistributionDAL(objDistBO);
        }
    }
}
