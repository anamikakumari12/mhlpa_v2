﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH_Logistics_DataAccess;
using MH_Logistics_BusinessObject;

namespace MH_Logistics_BusinessLogic
{
    public class DashboardBL
    {
        DashboardDAL objDAL = new DashboardDAL();
        public DataTable getDataforLPAResultsReportBL(DashboardBO objDashBO)
        {
            return objDAL.getDataforLPAResultsReportDAL(objDashBO);
        }

        public DataTable getLPAResultPerSectionBL(DashboardBO objDashBO)
        {
            return objDAL.getLPAResultPerSectionDAL(objDashBO);
        }
        public DataTable getLPAResultPerSectionMonthlyBL(DashboardBO objDashBO)
        {
            return objDAL.getLPAResultPerSectionMonthlyDAL(objDashBO);
        }

        public DataTable getLPAResultByLineBL(DashboardBO objDashBO)
        {
            return objDAL.getLPAResultByLineDAL(objDashBO);
        }

        public DataTable getLPAResultByQuestionBL(DashboardBO objDashBO)
        {
            return objDAL.getLPAResultByQuestionDAL(objDashBO);
        }

        public DataSet getFilterDataBL(DashboardBO objDashBO)
        {
            return objDAL.getFilterDataDAL(objDashBO);
        }

        public DataTable getDataforLPAComparisionReportBL(DashboardBO objDashBO)
        {
            return objDAL.getDataforLPAComparisionReportDAL(objDashBO);
        }

        public DataTable GetAuditReportsBL(DashboardBO objDashBO)
        {
           return objDAL.GetAuditReportsDAL(objDashBO);
        }

        public DataTable GetAuditResultsForDownloadBL(DashboardBO objDashBO)
        {
            return objDAL.GetAuditResultsForDownloadDAL(objDashBO);
        }

        public DataTable GetPerformanceReportForDownloadBL(DashboardBO objDashBO)
        {
            return objDAL.GetPerformanceReportForDownloadDAL(objDashBO);
        }
    }
}
