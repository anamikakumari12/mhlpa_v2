﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH_Logistics_DataAccess;
using MH_Logistics_BusinessObject;

namespace MH_Logistics_BusinessLogic
{
    
    public class GroupsBL
    {
        GroupsDAL objGrpDAL = new GroupsDAL();
        public DataSet GetGroupMemberListBL(GroupsBO objGrpBO)
        {
            return objGrpDAL.GetGroupMemberListDAL(objGrpBO);
        }

        public int SaveGroupMemberBL(GroupsBO objGrpBO, DataTable dt)
        {
            return objGrpDAL.SaveGroupMemberDAL(objGrpBO,dt);
        }

    }
}
