﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH_Logistics_DataAccess;
using MH_Logistics_BusinessObject;

namespace MH_Logistics_BusinessLogic
{
    public class RoleBL
    {
        RoleDAL objRoleDAL = new RoleDAL();
        public DataTable GetRoleBL()
        {
            return objRoleDAL.GetRoleDAL();
        }
        public RoleBO SaveRoleBL(RoleBO objRoleBL)
        {
            return objRoleDAL.SaveRoleDAL(objRoleBL);
        }

        public RoleBO DeleteRoleBL(RoleBO objRoleBO)
        {
            return objRoleDAL.DeleteRoleDAL(objRoleBO);
        }
    }
}
