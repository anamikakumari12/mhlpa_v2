﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MH_Logistics_DataAccess;
using MH_Logistics_BusinessObject;

namespace MH_Logistics_BusinessLogic
{
    public class QuestionsBL
    {
        QuestionsDAL objQuesDAL = new QuestionsDAL();
        public DataTable getQuestionListforEditBL(QuestionsBO objQuesBO)
        {
            return objQuesDAL.getQuestionListforEditDAL(objQuesBO);
        }

        public QuestionsBO SaveQuestionsBL(QuestionsBO objQuesBO)
        {
            return objQuesDAL.SaveQuestionsDAL(objQuesBO);
        }

        public QuestionsBO SaveLocalQuestionsBL(QuestionsBO objQuesBO)
        {
            return objQuesDAL.SaveLocalQuestionsDAL(objQuesBO);
        }
    }
}
