USE [MH_LOGISTICS_TEST]
GO
/****** Object:  StoredProcedure [dbo].[InsQuestion]    Script Date: 9/26/2018 3:29:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[InsQuestion](	@p_question_id			int,
								@p_section_name			VARCHAR(100),
								@p_question				VARCHAR(1000),
								@p_na_flag				VARCHAR(1),
								@p_help_text			VARCHAR(1000),
								@p_how_to_check			VARCHAR(1000),
								@p_display_seq			int,
								@p_location_id			int,
								@p_division_flag		VARCHAR(10),
								@l_err_code				int OUTPUT,
								@l_err_message			varchar(100) OUTPUT
								)
AS
	SET NOCOUNT ON;
	
BEGIN
	DECLARE @l_count 		INT;
	DECLARE @l_count1 		INT;
	DECLARE @l_section_id 		INT;
	DECLARE @l_question_id		INT;
	DECLARE @l_old_seq_no		INT;
	DECLARE @l_old_seq_no_lq	INT;
	DECLARE @l_division_id		INT;


 SELECT @l_division_id=ID FROM mh_log_division_master WHERE division_flag=@p_division_flag
	EXEC getSectionId  @p_section_name, @l_section_id = @l_section_id OUTPUT;

	IF @l_section_id IS NULL 
		BEGIN
			SET @l_err_code = 2001;
			SET @l_err_message = 'Invalid Section';
		END;
	ELSE
		BEGIN
			

			IF @p_question_id IS NOT NULL AND @p_question_id != 0 
			BEGIN

				SELECT @l_old_seq_no = display_sequence
				FROM   mh_lpa_question_master
				WHERE question_id = @p_question_id;
	
				IF @l_old_seq_no < @p_display_seq
					UPDATE mh_lpa_question_master
					SET    display_sequence = display_sequence-1
					WHERE  question_id != @p_question_id
					AND    display_sequence BETWEEN @l_old_seq_no AND @p_display_seq
					AND    division_id = @l_division_id;
				ELSE
					IF @l_old_seq_no > @p_display_seq
						UPDATE mh_lpa_question_master
						SET    display_sequence = display_sequence+1
						WHERE  question_id != @p_question_id
						AND    display_sequence BETWEEN @p_display_seq AND @l_old_seq_no 
						AND    division_id = @l_division_id;
								


				UPDATE mh_lpa_question_master
				SET display_sequence = @p_display_seq,
					question = @p_question,
					help_text = @p_help_text,
					how_to_check = @p_how_to_check,
					section_id = @l_section_id,
					na_flag = @p_na_flag,
					division_id=@l_division_id
				WHERE question_id = @p_question_id;
				

				UPDATE mh_lpa_local_questions SET section_id=@l_section_id WHERE question_id = @p_question_id;

				SET @l_err_code = 0;
				SET @l_err_message = 'Updated Successfully';
				
			END;
			ELSE
				BEGIN	
					SELECT @l_count = count(*)  
					FROM   mh_lpa_question_master
					WHERE  UPPER(question) = UPPER(@p_question)
					AND    division_id = @l_division_id;
			
				IF @l_count = 0 
					IF @p_question IS NOT NULL AND @p_question != '' 
					BEGIN
						/* first move all the existing questions to below the new question */
						UPDATE mh_lpa_question_master
						SET    display_sequence = display_sequence+1
						WHERE  display_sequence >= @p_display_seq
						AND    division_id = @l_division_id;

						INSERT INTO mh_lpa_question_master (
							section_id,
							question,
							na_flag,
							help_text,
							how_to_check,
							display_sequence,
							division_id,
							max_no_of_pics_allowed
						) VALUES (
							@l_section_id,
							@p_question,
							@p_na_flag,
							@p_help_text,
							@p_how_to_check,
							@p_display_seq,
							@l_division_id,
							1
						);

						SELECT @l_question_id = question_id
						FROM   mh_lpa_question_master
						WHERE  UPPER(question) = UPPER(@p_question)
						AND    division_id = @l_division_id;

						SELECT @l_count1 = count(*)
						FROM   mh_lpa_local_questions
						WHERE  division_id = @l_division_id;

						IF @l_count1 = 0 
							INSERT INTO mh_lpa_local_questions(
								question_id,
								location_id,
								section_id,
								display_sequence,
								division_id
							)
							SELECT @l_question_id, location_id, @l_section_id, @p_display_seq, @l_division_id
							FROM   mh_lpa_location_master;
						ELSE

							INSERT INTO mh_lpa_local_questions(
								question_id,
								location_id,
								section_id,
								display_sequence,
								division_id
							)
							SELECT @l_question_id, location_id, @l_section_id, ISNULL(MAX(display_sequence)+1,1), @l_division_id
							FROM   mh_lpa_local_questions
							WHERE  division_id = @l_division_id
							GROUP BY location_id, division_id;


						SET @l_err_code = 0;
						SET @l_err_message = 'Inserted Successfully';
					END;
				END;
		END;
	
END





--SELECT distinct * FROM  mh_lpa_local_questions lq JOIN mh_lpa_question_master qm on lq.question_id=qm.question_id
