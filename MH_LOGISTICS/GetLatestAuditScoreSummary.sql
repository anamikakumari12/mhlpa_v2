USE [MH_LOGISTICS_TEST]
GO
/****** Object:  UserDefinedFunction [dbo].[GetLatestAuditScoreSummary]    Script Date: 9/25/2018 3:27:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select [dbo].[GetLatestAuditScoreSummary](0,8,11,20,5)
ALTER FUNCTION [dbo].[GetLatestAuditScoreSummary](
								@p_division_id		INT,
								@p_region_id		INT,
								@p_country_id		INT,
								@p_location_id		INT,
								@p_line_id int )  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  

	DECLARE @l_line_id		INT;
	DECLARE @l_audit_id		INT;
	DECLARE @l_region_id	INT;
	DECLARE @l_country_id	INT;
	DECLARE @l_location_id	INT;


	IF @p_line_id IS NOT NULL AND @p_line_id != 0 
		BEGIN
			SET @l_line_id = @p_line_id;
	--IF @p_division_id IS NULL OR @p_division_id = 0 
	--BEGIN
	SELECT @p_division_id=division_id,@p_region_id=region_id, @p_country_id=country_id,@p_location_id=location_id   FROM mh_lpa_line_master where line_id = @l_line_id;
	--END

			SELECT @l_audit_id = MAX(audit_id) 
			FROM   mh_lpa_local_answers
			WHERE line_id = @l_line_id;
		END;
	ELSE
		BEGIN
			SET @l_line_id = NULL;
			SET @l_audit_id = NULL;
		END;
		

		 
	IF @p_region_id IS NOT NULL AND @p_region_id != 0 
		SET @l_region_id = @p_region_id;
	ELSE
		SET @l_region_id = NULL;
		
	IF @p_country_id IS NOT NULL AND @p_country_id != 0 
		SET @l_country_id = @p_country_id;
	ELSE
		SET @l_country_id = NULL;
		
	IF @p_location_id IS NOT NULL AND @p_location_id != 0 
		SET @l_location_id = @p_location_id;
	ELSE
		SET @l_location_id = NULL;
		
	RETURN(
	SELECT sec_id, avg_score, section_name, section_abbr
	FROM (
		SELECT  Sec_id, 
				avg_score,
				section_name,
				CASE
							WHEN section_id = 1 THEN 'GT'
							WHEN section_id = 2 THEN 'PD'
							WHEN section_id = 3 THEN 'SOCWA'
							WHEN section_id = 4 THEN 'RPE'
							WHEN section_id = 5 THEN 'SW'
							WHEN section_id = 6 THEN 'RPS'
							ELSE 'OTHERS'
/*
							WHEN section_name = 'General Topics' THEN 'GP'
							WHEN section_name = 'People Development' THEN 'PD'
							WHEN section_name = 'Safe, Organized, Clean Work Area' THEN 'SOCWA'
							WHEN section_name = 'Robust Processes and Equipment' THEN 'RPE'
							WHEN section_name = 'Standardized Work' THEN 'SW'
							WHEN section_name = 'Rapid Problem Solving / 8D' THEN 'RPS'
							ELSE 'OTHERS'
*/
				END section_abbr
		FROM (
			SELECT 1 as Sec_id, 
					AVG(sec1_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  audit_id = ISNULL(@l_audit_id, audit_id)
			AND    line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    division_id = @p_division_id
			AND    sec1_score IS NOT NULL
			UNION
			SELECT 2 as Sec_id, 
					AVG(sec2_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  audit_id = ISNULL(@l_audit_id, audit_id)
			AND    line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    division_id = @p_division_id
			AND    sec2_score IS NOT NULL
			UNION
			SELECT 3 as Sec_id, 
					AVG(sec3_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  audit_id = ISNULL(@l_audit_id, audit_id)
			AND    line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    division_id = @p_division_id
			AND    sec3_score IS NOT NULL
			UNION
			SELECT 4 as Sec_id, 
					AVG(sec4_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  audit_id = ISNULL(@l_audit_id, audit_id)
			AND    line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    division_id = @p_division_id
			AND    sec4_score IS NOT NULL
			UNION
			SELECT 5 as Sec_id, 
					AVG(sec5_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  audit_id = ISNULL(@l_audit_id, audit_id)
			AND    line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    division_id = @p_division_id
			AND    sec5_score IS NOT NULL
			UNION
			SELECT 6 as Sec_id, 
					AVG(sec6_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  audit_id = ISNULL(@l_audit_id, audit_id)
			AND    line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    division_id = @p_division_id
			AND    sec6_score IS NOT NULL
			) t,
			mh_lpa_section_master sm
			WHERE t.sec_id = sm.section_id
			AND    division_id = @p_division_id
		) tmp
		FOR JSON AUTO)
/*
   RETURN (
   SELECt sec_id,
			section_name,
			section_abbr,
			avg_score,
			tot_answers
	FROM (
			SELECT Sec_id,
					section_name,
					section_abbr,
					CAST(AVG( CAST(yes_answer AS FLOAT)/CAST(tot_answers AS FLOAT)*100) as numeric(10,2) ) avg_score,
					CAST(AVG(tot_answers) as numeric(10,2) ) tot_answers
			FROM (
				SELECT la.audit_id,
						la.audit_date,
						la.Shift_No,
						sm.section_id as sec_id,
						sm.section_name,
						CASE
							WHEN section_name = 'General Topics' THEN 'GP'
							WHEN section_name = 'People Development' THEN 'PD'
							WHEN section_name = 'Safe, Organized, Clean Work Area' THEN 'SOCWA'
							WHEN section_name = 'Robust Processes and Equipment' THEN 'RPE'
							WHEN section_name = 'Standardized Work' THEN 'SW'
							WHEN section_name = 'Rapid Problem Solving / 8D' THEN 'RPS'
							ELSE 'OTHERS'
							END section_abbr,
						SUM(CASE 
							WHEN answer = 0 THEN 1
							ELSE 0
							END) yes_answer,
						SUM(CASE	
							WHEN answer = 0 THEN 1
							WHEN answer = 1 THEN 1
							ELSE 0
							END) tot_answers
				FROM    mh_lpa_local_answers la,
						mh_lpa_section_master sm
				WHERE  la.section_id = sm.section_id
				--and    la.section_id != 1
--				AND    audit_id = (select max(audit_id) from mh_lpa_local_answers where line_product_rel_id = @p_line_product_rel_id)
				AND   answer != 2
				AND    ( 
						(ISNULL(@p_line_id,0) != 0 AND  audit_id = (select max(audit_id) from mh_lpa_local_answers loc, mh_lpa_line_product_relationship rel 
																				where loc.line_product_rel_id = rel.line_product_rel_id
																				and   rel.line_id = @p_line_id)) OR
						ISNULL(@p_line_id,0) = 0
						)
*
				AND    ( 
						(ISNULL(@p_line_product_rel_id,0) != 0 AND la.line_product_rel_id = @p_line_product_rel_id) OR
						ISNULL(@p_line_product_rel_id,0) = 0
						)
*
				AND    ( 
						(ISNULL(@p_region_id,0) != 0 AND la.region_id = @p_region_id) OR
						ISNULL(@p_region_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_country_id,0) != 0 AND la.country_id = @p_country_id) OR
						ISNULL(@p_country_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_location_id,0) != 0 AND la.location_id = @p_location_id) OR
						ISNULL(@p_location_id,0) = 0
						)
				-- AND    FORMAT(la.audit_date,'MM-YYYY') = FORMAT(CURRENT_TIMESTAMP,'MM-YYYY')
				GROUP BY la.audit_id,
						la.audit_date,
						la.Shift_No,
						sm.section_id,
						section_name
				HAVING SUM(CASE	
							WHEN answer = 0 THEN 1
							WHEN answer = 1 THEN 1
							ELSE 0
							END) != 0
				) tmp	
		GROUP BY 
			sec_id, section_name, section_abbr
			) t	
		FOR JSON AUTO) 
		
*/ 
END




GO

ALTER FUNCTION [dbo].[GetAuditScoreSummary](
								@p_division_id		INT,
								@p_region_id		INT,
								@p_country_id		INT,
								@p_location_id		INT,
								@p_line_id int )  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  

	DECLARE @l_line_id		INT;
	DECLARE @l_audit_id		INT;
	DECLARE @l_region_id	INT;
	DECLARE @l_country_id	INT;
	DECLARE @l_location_id	INT;


	IF @p_line_id IS NOT NULL AND @p_line_id != 0 
		BEGIN
			SET @l_line_id = @p_line_id;
		END;
	ELSE
		BEGIN
			SET @l_line_id = NULL;
		END;
		
	IF @p_region_id IS NOT NULL AND @p_region_id != 0 
		SET @l_region_id = @p_region_id;
	ELSE
		SET @l_region_id = NULL;
		
	IF @p_country_id IS NOT NULL AND @p_country_id != 0 
		SET @l_country_id = @p_country_id;
	ELSE
		SET @l_country_id = NULL;
		
	IF @p_location_id IS NOT NULL AND @p_location_id != 0 
		SET @l_location_id = @p_location_id;
	ELSE
		SET @l_location_id = NULL;
		
	RETURN(
	SELECT sec_id, avg_score, section_name, section_abbr
	FROM (
		SELECT  Sec_id, 
				avg_score,
				section_name,
				CASE
							WHEN section_id = 1 THEN 'GT'
							WHEN section_id = 2 THEN 'PD'
							WHEN section_id = 3 THEN 'SOCWA'
							WHEN section_id = 4 THEN 'RPE'
							WHEN section_id = 5 THEN 'SW'
							WHEN section_id = 6 THEN 'RPS'
							ELSE 'OTHERS'
				END section_abbr
		FROM (
			SELECT 1 as Sec_id, 
					AVG(sec1_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    division_id = @p_division_id
			AND    sec1_score IS NOT NULL
			UNION
			SELECT 2 as Sec_id, 
					AVG(sec2_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    division_id = @p_division_id
			AND    sec2_score IS NOT NULL
			UNION
			SELECT 3 as Sec_id, 
					AVG(sec3_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    division_id = @p_division_id
			AND    sec3_score IS NOT NULL
			UNION
			SELECT 4 as Sec_id, 
					AVG(sec4_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    division_id = @p_division_id
			AND    sec4_score IS NOT NULL
			UNION
			SELECT 5 as Sec_id, 
					AVG(sec5_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    division_id = @p_division_id
			AND    sec5_score IS NOT NULL
			UNION
			SELECT 6 as Sec_id, 
					AVG(sec6_score)*100 as avg_score
			FROM   mh_lpa_score_summary
			WHERE  line_id = ISNULL(@l_line_id, line_id)
			AND    location_id = ISNULL(@l_location_id, location_id)
			AND    country_id = ISNULL(@l_country_id, country_id)
			AND    region_id = ISNULL(@l_region_id, region_id)
			AND    division_id = @p_division_id
			AND    sec6_score IS NOT NULL
			) t,
			mh_lpa_section_master sm
			WHERE t.sec_id = sm.section_id
			AND   sm.division_id = @p_division_id
		) tmp
		FOR JSON AUTO
		)



/*
   RETURN (
   SELECt sec_id,
			section_name,
			section_abbr,
			avg_score,
			tot_answers
	FROM (
			SELECT Sec_id,
					section_name,
					section_abbr,
					CAST(AVG( CAST(yes_answer AS FLOAT)/CAST(tot_answers AS FLOAT)*100) as numeric(10,2) ) avg_score,
					CAST(AVG(tot_answers) as numeric(10,2) ) tot_answers
			FROM (
				SELECT la.audit_id,
						la.audit_date,
						la.Shift_No,
						sm.section_id as sec_id,
						sm.section_name,
						CASE
							WHEN section_name = 'General Topics' THEN 'GP'
							WHEN section_name = 'People Development' THEN 'PD'
							WHEN section_name = 'Safe, Organized, Clean Work Area' THEN 'SOCWA'
							WHEN section_name = 'Robust Processes and Equipment' THEN 'RPE'
							WHEN section_name = 'Standardized Work' THEN 'SW'
							WHEN section_name = 'Rapid Problem Solving / 8D' THEN 'RPS'
							ELSE 'OTHERS'
							END section_abbr,
						SUM(CASE 
							WHEN answer = 0 THEN 1
							ELSE 0
							END) yes_answer,
						SUM(CASE	
							WHEN answer = 0 THEN 1
							WHEN answer = 1 THEN 1
							ELSE 0
							END) tot_answers
				FROM    mh_lpa_local_answers la,
						mh_lpa_section_master sm
				WHERE  la.section_id = sm.section_id
			--	AND   la.section_id != 1
			--	AND    audit_id = dbo.GetLowScoreAuditIdbyDate(line_product_rel_id,audit_date)
				AND   answer != 2
				AND    ( 
						(ISNULL(@p_line_product_rel_id,0) != 0 AND la.line_product_rel_id = @p_line_product_rel_id) OR
						ISNULL(@p_line_product_rel_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_region_id,0) != 0 AND la.region_id = @p_region_id) OR
						ISNULL(@p_region_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_country_id,0) != 0 AND la.country_id = @p_country_id) OR
						ISNULL(@p_country_id,0) = 0
						)
				AND    ( 
						(ISNULL(@p_location_id,0) != 0 AND la.location_id = @p_location_id) OR
						ISNULL(@p_location_id,0) = 0
						)
				AND    FORMAT(la.audit_date,'MM-YYYY') = FORMAT(CURRENT_TIMESTAMP,'MM-YYYY')
				GROUP BY la.audit_id,
						la.audit_date,
						la.Shift_No,
						sm.section_id,
						section_name
				HAVING SUM(CASE	
							WHEN answer = 0 THEN 1
							WHEN answer = 1 THEN 1
							ELSE 0
							END) != 0
				) tmp	
		GROUP BY 
			sec_id, section_name, section_abbr
			) t	
		FOR JSON AUTO)  
*/
END



GO


ALTER PROCEDURE [dbo].[UpdUserPassword]
(
                @p_user_id               	INT,
                @PASSWORD               	VARCHAR(max)
)
AS
BEGIN

	DECLARE  @l_err_code 		INT;
	DECLARE  @l_err_message 	VARCHAR(100);
 
--exec InsUserDetails 1, 'ab', 'abc', 'kns',1,'Y','Manager','06-01-2017','06-25-2017',null,'N','Y','Insert'
                -- SET NOCOUNT ON added to prevent extra result sets from
                -- interfering with SELECT statements.
                SET NOCOUNT ON;
 
 	IF @p_user_id IS NOT NULL AND @p_user_id != 0
 
		UPDATE mh_lpa_user_master
		SET   passwd = @PASSWORD
		WHERE user_id = @p_user_id

	SET @l_err_code = 0;
	SET @l_err_message = 'Password Reset Successfully';

	SELECT * FROM (SELECT @l_err_code err_code, @l_err_message err_message) a FOR JSON AUTO

	
END



GO

ALTER PROCEDURE [dbo].[ResetUserPassword]
(
                @USERNAME               	VARCHAR(100),
                @PASSWORD               	VARCHAR(max)
)
AS
BEGIN

	DECLARE  @l_err_code 		INT;
	DECLARE  @l_err_message 	VARCHAR(100);
 
--exec InsUserDetails 1, 'ab', 'abc', 'kns',1,'Y','Manager','06-01-2017','06-25-2017',null,'N','Y','Insert'
                -- SET NOCOUNT ON added to prevent extra result sets from
                -- interfering with SELECT statements.
                SET NOCOUNT ON;
 
 	IF @USERNAME IS NOT NULL AND @USERNAME != ''
 
		UPDATE mh_lpa_user_master
		SET   passwd = @PASSWORD
		WHERE username = @USERNAME
		SET @l_err_code = 0;
		SET @l_err_message = 'Password Reset Successfully';
		SELECT email_id,(SELECT * FROM (SELECT @l_err_code err_code, @l_err_message err_message) a FOR JSON AUTO) FROM mh_lpa_user_master WHERE username = @USERNAME AND passwd = @PASSWORD

	
END




GO