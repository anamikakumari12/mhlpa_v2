
-- DECLARE @l_audit_invite  AuditPlanInviteTableType;
 DECLARE @l_year  INT;
 DECLARE @l_emp_id INT;
 DECLARE @l_line_id INT;
 DECLARE @l_location_id   INT;
 DECLARE @l_line_name   VARCHAR(100);
 DECLARE @l_invite_to_list  varchar(max);
 DECLARE @l_del_invite_to_list varchar(max);
 DECLARE @l_err_code    int;
 DECLARE @l_err_message   varchar(100);
 DECLARE @l_yr     VARCHAR(4);
 DECLARE @l_start_date   DATE;
 DECLARE @l_end_date    DATE;
 DECLARE @l_week     INT;
  DECLARE @l_timezone_code varchar(100);
  DECLARE @l_time_offset	FLOAT;
  DECLARE @l_location_name	VARCHAR(100);

  --Select * INTO temp_audit_plan_interface_table from @p_audit_plan_interface_table


 DECLARE @l_wk1 VARCHAR(100), @l_wk2 VARCHAR(100), @l_wk3 VARCHAR(100), @l_wk4 VARCHAR(100), @l_wk5 VARCHAR(100), 
         @l_wk6 VARCHAR(100), @l_wk7 VARCHAR(100), @l_wk8 VARCHAR(100), @l_wk9 VARCHAR(100), @l_wk10 VARCHAR(100),
   @l_wk11 VARCHAR(100), @l_wk12 VARCHAR(100), @l_wk13 VARCHAR(100), @l_wk14 VARCHAR(100), @l_wk15 VARCHAR(100),
   @l_wk16 VARCHAR(100), @l_wk17 VARCHAR(100), @l_wk18 VARCHAR(100), @l_wk19 VARCHAR(100), @l_wk20 VARCHAR(100),
   @l_wk21 VARCHAR(100), @l_wk22 VARCHAR(100), @l_wk23 VARCHAR(100), @l_wk24 VARCHAR(100), @l_wk25 VARCHAR(100),
   @l_wk26 VARCHAR(100), @l_wk27 VARCHAR(100), @l_wk28 VARCHAR(100), @l_wk29 VARCHAR(100), @l_wk30 VARCHAR(100),
   @l_wk31 VARCHAR(100), @l_wk32 VARCHAR(100), @l_wk33 VARCHAR(100), @l_wk34 VARCHAR(100), @l_wk35 VARCHAR(100),
   @l_wk36 VARCHAR(100), @l_wk37 VARCHAR(100), @l_wk38 VARCHAR(100), @l_wk39 VARCHAR(100), @l_wk40 VARCHAR(100),
   @l_wk41 VARCHAR(100), @l_wk42 VARCHAR(100), @l_wk43 VARCHAR(100), @l_wk44 VARCHAR(100), @l_wk45 VARCHAR(100),
   @l_wk46 VARCHAR(100), @l_wk47 VARCHAR(100), @l_wk48 VARCHAR(100), @l_wk49 VARCHAR(100), @l_wk50 VARCHAR(100),
   @l_wk51 VARCHAR(100), @l_wk52 VARCHAR(100);
   
   DECLARE @l_orig_wk1 VARCHAR(100), @l_orig_wk2 VARCHAR(100), @l_orig_wk3 VARCHAR(100), @l_orig_wk4 VARCHAR(100), @l_orig_wk5 VARCHAR(100), 
         @l_orig_wk6 VARCHAR(100), @l_orig_wk7 VARCHAR(100), @l_orig_wk8 VARCHAR(100), @l_orig_wk9 VARCHAR(100), @l_orig_wk10 VARCHAR(100),
   @l_orig_wk11 VARCHAR(100), @l_orig_wk12 VARCHAR(100), @l_orig_wk13 VARCHAR(100), @l_orig_wk14 VARCHAR(100), @l_orig_wk15 VARCHAR(100),
   @l_orig_wk16 VARCHAR(100), @l_orig_wk17 VARCHAR(100), @l_orig_wk18 VARCHAR(100), @l_orig_wk19 VARCHAR(100), @l_orig_wk20 VARCHAR(100),
   @l_orig_wk21 VARCHAR(100), @l_orig_wk22 VARCHAR(100), @l_orig_wk23 VARCHAR(100), @l_orig_wk24 VARCHAR(100), @l_orig_wk25 VARCHAR(100),
   @l_orig_wk26 VARCHAR(100), @l_orig_wk27 VARCHAR(100), @l_orig_wk28 VARCHAR(100), @l_orig_wk29 VARCHAR(100), @l_orig_wk30 VARCHAR(100),
   @l_orig_wk31 VARCHAR(100), @l_orig_wk32 VARCHAR(100), @l_orig_wk33 VARCHAR(100), @l_orig_wk34 VARCHAR(100), @l_orig_wk35 VARCHAR(100),
   @l_orig_wk36 VARCHAR(100), @l_orig_wk37 VARCHAR(100), @l_orig_wk38 VARCHAR(100), @l_orig_wk39 VARCHAR(100), @l_orig_wk40 VARCHAR(100),
   @l_orig_wk41 VARCHAR(100), @l_orig_wk42 VARCHAR(100), @l_orig_wk43 VARCHAR(100), @l_orig_wk44 VARCHAR(100), @l_orig_wk45 VARCHAR(100),
   @l_orig_wk46 VARCHAR(100), @l_orig_wk47 VARCHAR(100), @l_orig_wk48 VARCHAR(100), @l_orig_wk49 VARCHAR(100), @l_orig_wk50 VARCHAR(100),
   @l_orig_wk51 VARCHAR(100), @l_orig_wk52 VARCHAR(100);

 DECLARE l_audit_plan_cur CURSOR FOR SELECT * FROM temp_audit_plan_interface_table;
 
 SELECT TOP 1 @l_year = year,
  @l_location_id = location_id,
  @l_line_id = line_id,
  @l_line_name = line_name
 FROM  temp_audit_plan_interface_table;
 
 
 DELETE FROM mh_lpa_audit_plan_interface
 WHERE  year = @l_year
 AND    location_id = @l_location_id;
 
 INSERT INTO mh_lpa_audit_plan_interface
 SELECT *
 FROM   temp_audit_plan_interface_table;
 
 
 DELETE FROM  mh_lpa_AuditPlan_Del_Invite
 WHERE  year = @l_year
 AND    location_id = @l_location_id;
 
 DELETE FROM  mh_lpa_AuditPlanInvite
 WHERE  year = @l_year
 AND    location_id = @l_location_id;
  
 
 SET @l_yr = CAST(@l_year AS VARCHAR);
 
 /*
  * Now for each cell from the above table, create or update the plan in the mh_lpa_audit_plan table
  */
  
 OPEN l_audit_plan_cur
 FETCH NEXT FROM l_audit_plan_cur INTO @l_location_id,@l_line_id,@l_line_name,@l_year,
           @l_wk1, @l_wk2, @l_wk3, @l_wk4, @l_wk5, 
           @l_wk6, @l_wk7, @l_wk8, @l_wk9, @l_wk10,
           @l_wk11, @l_wk12, @l_wk13, @l_wk14, @l_wk15,
           @l_wk16, @l_wk17, @l_wk18, @l_wk19, @l_wk20,
           @l_wk21, @l_wk22, @l_wk23, @l_wk24, @l_wk25,
           @l_wk26, @l_wk27, @l_wk28, @l_wk29, @l_wk30,
           @l_wk31, @l_wk32, @l_wk33, @l_wk34, @l_wk35,
           @l_wk36, @l_wk37, @l_wk38, @l_wk39, @l_wk40,
           @l_wk41, @l_wk42, @l_wk43, @l_wk44, @l_wk45,
           @l_wk46, @l_wk47, @l_wk48, @l_wk49, @l_wk50,
           @l_wk51, @l_wk52;
 WHILE @@FETCH_STATUS = 0 
 BEGIN
 
	SET @l_orig_wk1 = 0; SET @l_orig_wk2 = 0; SET @l_orig_wk3 = 0; SET @l_orig_wk4 = 0; SET @l_orig_wk5 = 0; 
	SET @l_orig_wk6 = 0; SET @l_orig_wk7 = 0; SET @l_orig_wk8 = 0; SET @l_orig_wk9 = 0; SET @l_orig_wk10 = 0; 
	
	SET @l_orig_wk11 = 0; SET @l_orig_wk12 = 0; SET @l_orig_wk13 = 0; SET @l_orig_wk14 = 0; SET @l_orig_wk15 = 0; 
	SET @l_orig_wk16 = 0; SET @l_orig_wk17 = 0; SET @l_orig_wk18 = 0; SET @l_orig_wk19 = 0; SET @l_orig_wk20 = 0; 
	
	SET @l_orig_wk21 = 0; SET @l_orig_wk22 = 0; SET @l_orig_wk23 = 0; SET @l_orig_wk24 = 0; SET @l_orig_wk25 = 0; 
	SET @l_orig_wk26 = 0; SET @l_orig_wk27 = 0; SET @l_orig_wk28 = 0; SET @l_orig_wk29 = 0; SET @l_orig_wk30 = 0; 

	SET @l_orig_wk31 = 0; SET @l_orig_wk32 = 0; SET @l_orig_wk33 = 0; SET @l_orig_wk34 = 0; SET @l_orig_wk35 = 0; 
	SET @l_orig_wk36 = 0; SET @l_orig_wk37 = 0; SET @l_orig_wk38 = 0; SET @l_orig_wk39 = 0; SET @l_orig_wk40 = 0; 

	SET @l_orig_wk41 = 0; SET @l_orig_wk42 = 0; SET @l_orig_wk43 = 0; SET @l_orig_wk44 = 0; SET @l_orig_wk45 = 0; 
	SET @l_orig_wk46 = 0; SET @l_orig_wk47 = 0; SET @l_orig_wk48 = 0; SET @l_orig_wk49 = 0; SET @l_orig_wk50 = 0; 

	SET @l_orig_wk51 = 0; SET @l_orig_wk52 = 0; 

	/*  SET @l_week = 1;
  SET @l_start_date = DATEADD(day, 1, DATEADD(wk, DATEDIFF(wk, 6, '1/1/' + @l_yr) + (@l_week-1), 6));
  SET @l_end_date = DATEADD(day, -1, DATEADD(wk, DATEDIFF(wk, 5, '1/1/' + @l_yr) + (@l_week-1), 5));
*/
  
  SELECT @l_timezone_code = timezone_code,
		@l_location_name = location_name
  FROM   mh_lpa_location_master
  WHERE  location_id = @l_location_id;
  
  

  EXEC dbo.get_st_end_date @l_yr, 1,@l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk1 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
   
  IF @l_wk1 = '0' OR @l_wk1 != @l_orig_wk1
  BEGIN
  Print @l_wk1
  print @l_orig_wk1
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk1 IS NOT NULL AND @l_wk1 != '0'
  BEGIN
   print @l_start_date
   print @l_end_date
   print @l_line_id
   EXEC dbo.InsAuditPlan NULL, @l_wk1, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 2,  @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;   
	SELECT @l_orig_wk2 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk2 = 0 OR @l_wk2 != @l_orig_wk2
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk2 IS NOT NULL AND @l_wk2 != 0
  BEGIN
   EXEC dbo.InsAuditPlan NULL, @l_wk2, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 3,  @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk3 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk3 = 0 OR @l_wk3 != @l_orig_wk3
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk3 IS NOT NULL AND @l_wk3 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk3, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 4,  @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk4 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk4 = 0 OR @l_wk4 != @l_orig_wk4
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk4 IS NOT NULL AND @l_wk4 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk4, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 5,  @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk5 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk5 = 0 OR @l_wk5 != @l_orig_wk5
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk5 IS NOT NULL AND @l_wk5 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk5, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 6,  @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk6 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk6 = 0 OR @l_wk6 != @l_orig_wk6
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk6 IS NOT NULL AND @l_wk6 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk6, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 7,  @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk7 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk7 = 0 OR @l_wk7 != @l_orig_wk7
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk7 IS NOT NULL AND @l_wk7 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk7, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 8,  @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk8 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk8 = 0 OR @l_wk8 != @l_orig_wk8
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk8 IS NOT NULL AND @l_wk8 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk8, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 9,  @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk9 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk9 = 0 OR @l_wk9 != @l_orig_wk9
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk9 IS NOT NULL AND @l_wk9 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk9, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 10, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk10 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk10 = 0 OR @l_wk10 != @l_orig_wk10
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk10 IS NOT NULL AND @l_wk10 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk10, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 11, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk11 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk11 = 0 OR @l_wk11 != @l_orig_wk11
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk11 IS NOT NULL AND @l_wk11 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk11, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 12, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk12 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk12 = 0 OR @l_wk12 != @l_orig_wk12
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk12 IS NOT NULL AND @l_wk12 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk12, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 13, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk13 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk13 = 0 OR @l_wk13 != @l_orig_wk13
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk13 IS NOT NULL AND @l_wk13 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk13, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 14, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk14 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk14 = 0 OR @l_wk14 != @l_orig_wk14
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk14 IS NOT NULL AND @l_wk14 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk14, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 15, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk15 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk15 = 0 OR @l_wk15 != @l_orig_wk15
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk15 IS NOT NULL AND @l_wk15 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk15, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 16, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk16 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
   
  IF @l_wk16 = 0 OR @l_wk16 != @l_orig_wk16
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk16 IS NOT NULL AND @l_wk16 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk16, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 17, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk17 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
   
  IF @l_wk17 = 0 OR @l_wk17 != @l_orig_wk17
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk17 IS NOT NULL AND @l_wk17 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk17, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 18, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk18 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
   
  IF @l_wk18 = 0 OR @l_wk18 != @l_orig_wk18
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk18 IS NOT NULL AND @l_wk18 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk18, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 19, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
	SELECT @l_orig_wk19 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
   
  IF @l_wk19 = 0 OR @l_wk19 != @l_orig_wk19
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk19 IS NOT NULL AND @l_wk19 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk19, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  /* Week 20 to 29 */
  
  
  EXEC dbo.get_st_end_date @l_yr, 20, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk20 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk20 = 0 OR @l_wk20 != @l_orig_wk20
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk20 IS NOT NULL AND @l_wk20 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk20, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 21, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk21 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk21 = 0 OR @l_wk21 != @l_orig_wk21
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk21 IS NOT NULL AND @l_wk21 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk21, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 22, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk22 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk22 = 0 OR @l_wk22 != @l_orig_wk22
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk22 IS NOT NULL AND @l_wk22 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk22, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 23, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk23 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk23 = 0 OR @l_wk23 != @l_orig_wk23
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk23 IS NOT NULL AND @l_wk23 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk23, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 24, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk24 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk24 = 0 OR @l_wk24 != @l_orig_wk24
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk24 IS NOT NULL AND @l_wk24 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk24, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 25, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk25 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk25 = 0 OR @l_wk25 != @l_orig_wk25
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk25 IS NOT NULL AND @l_wk25 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk25, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 26, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk26 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk26 = 0 OR @l_wk26 != @l_orig_wk26
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk26 IS NOT NULL AND @l_wk26 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk26, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 27, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk27 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk27 = 0 OR @l_wk27 != @l_orig_wk27
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk27 IS NOT NULL AND @l_wk27 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk27, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 28, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
 	SELECT @l_orig_wk28 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk28 = 0 OR @l_wk28 != @l_orig_wk28
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk28 IS NOT NULL AND @l_wk28 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk28, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 29, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk29 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk29 = 0 OR @l_wk29 != @l_orig_wk29
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk29 IS NOT NULL AND @l_wk29 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk29, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  

  /* Week 30 to 39 */
  
  
  EXEC dbo.get_st_end_date @l_yr, 30, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk30 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk30 = 0 OR @l_wk30 != @l_orig_wk30
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk30 IS NOT NULL AND @l_wk30 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk30, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 31, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk31 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk31 = 0 OR @l_wk31 != @l_orig_wk31
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk31 IS NOT NULL AND @l_wk31 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk31, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 32, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk32 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk32 = 0 OR @l_wk32 != @l_orig_wk32
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk32 IS NOT NULL AND @l_wk32 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk32, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 33, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk33 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk33 = 0 OR @l_wk33 != @l_orig_wk33
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk33 IS NOT NULL AND @l_wk33 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk33, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 34, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk34 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk34 = 0 OR @l_wk34 != @l_orig_wk34
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;

  IF @l_wk34 IS NOT NULL AND @l_wk34 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk34, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 35, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk35 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk35 = 0 OR @l_wk35 != @l_orig_wk35
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk35 IS NOT NULL AND @l_wk35 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk35, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 36, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk36 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk36 = 0 OR @l_wk36 != @l_orig_wk36
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk36 IS NOT NULL AND @l_wk36 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk36, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 37, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk37 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk37 = 0 OR @l_wk37 != @l_orig_wk37
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk37 IS NOT NULL AND @l_wk37 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk37, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 38, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk38 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk38 = 0 OR @l_wk38 != @l_orig_wk38
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk38 IS NOT NULL AND @l_wk38 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk38, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 39, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk39 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk39 = 0 OR @l_wk39 != @l_orig_wk39
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk39 IS NOT NULL AND @l_wk39 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk39, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  

  
  /* Week 40 to 49 */
  
  
  EXEC dbo.get_st_end_date @l_yr, 40, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk40 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk40 = 0 OR @l_wk40 != @l_orig_wk40
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk40 IS NOT NULL AND @l_wk40 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk40, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;
 
  
  
  
  EXEC dbo.get_st_end_date @l_yr, 41, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk41 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk41 = 0 OR @l_wk41 != @l_orig_wk41
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk41 IS NOT NULL AND @l_wk41 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk41, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 42, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk42 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk42 = 0 OR @l_wk42 != @l_orig_wk42
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk42 IS NOT NULL AND @l_wk42 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk42, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 43, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk43 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk43 = 0 OR @l_wk43 != @l_orig_wk43
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk43 IS NOT NULL AND @l_wk43 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk43, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 44, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk44 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk44 = 0 OR @l_wk44 != @l_orig_wk44
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk44 IS NOT NULL AND @l_wk44 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk44, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 45, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk45 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk45 = 0 OR @l_wk45 != @l_orig_wk45
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk45 IS NOT NULL AND @l_wk45 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk45, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 46, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk46 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk46 = 0 OR @l_wk46 != @l_orig_wk46
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk46 IS NOT NULL AND @l_wk46 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk46, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 47, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk47 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk47 = 0 OR @l_wk47 != @l_orig_wk47
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk47 IS NOT NULL AND @l_wk47 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk47, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 48, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
  	SELECT @l_orig_wk48 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk48 = 0 OR @l_wk48 != @l_orig_wk48
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk48 IS NOT NULL AND @l_wk48 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk48, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 49, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk49 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk49 = 0 OR @l_wk49 != @l_orig_wk49
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk49 IS NOT NULL AND @l_wk49 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk49, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  /* Week 50 to 52 */
  
  
  EXEC dbo.get_st_end_date @l_yr, 50, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk50 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk50 = 0 OR @l_wk50 != @l_orig_wk50
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk50 IS NOT NULL AND @l_wk50 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk50, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  
  EXEC dbo.get_st_end_date @l_yr, 51, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
    SELECT @l_orig_wk51 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
 IF @l_wk51 = 0 OR @l_wk51 != @l_orig_wk51
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;
  
  IF @l_wk51 IS NOT NULL AND @l_wk51 != 0
  BEGIN
   
   EXEC dbo.InsAuditPlan NULL, @l_wk51, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  END;

  
  
  EXEC dbo.get_st_end_date @l_yr, 52, @l_start_date = @l_start_date OUTPUT ,@l_end_date = @l_end_date OUTPUT;
   	SELECT @l_orig_wk52 = to_be_audited_by_user_id
	FROM   mh_lpa_audit_plan p
	WHERE  planned_date = @l_start_date
	AND    planned_date_end = @l_end_date
	AND    p.line_id = @l_line_id;   
  IF @l_wk52 = 0 OR @l_wk52 != @l_orig_wk52
  BEGIN
   EXEC dbo.[DelAuditPlan_byDate] @l_line_id, @l_start_date, @l_end_date, @l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
  
   IF @l_del_invite_to_list IS NOT NULL
   BEGIN
    INSERT INTO mh_lpa_AuditPlan_Del_Invite VALUES (@l_location_id, @l_year, @l_del_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
    SET @l_del_invite_to_list = NULL;
   END;
  END;

  IF @l_wk52 IS NOT NULL AND @l_wk52 != 0
  BEGIN
-- select * from (select @l_wk52 wk52, @l_start_date st_date, @l_end_date end_date, @l_line_id line_id ) tmp
   EXEC dbo.InsAuditPlan NULL, @l_wk52, @l_start_date, @l_end_date, @l_line_id, 
        @l_invite_to_list = @l_invite_to_list OUTPUT, 
        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
            
   IF @l_invite_to_list IS NOT NULL AND @l_invite_to_list != ''
    INSERT INTO mh_lpa_AuditPlanInvite VALUES (@l_location_id, @l_year, @l_invite_to_list, @l_start_date, @l_end_date, @l_line_name);
  
  END;

  
  FETCH NEXT FROM l_audit_plan_cur INTO @l_location_id,@l_line_id,@l_line_name,@l_year,
           @l_wk1, @l_wk2, @l_wk3, @l_wk4, @l_wk5, 
           @l_wk6, @l_wk7, @l_wk8, @l_wk9, @l_wk10,
           @l_wk11, @l_wk12, @l_wk13, @l_wk14, @l_wk15,
           @l_wk16, @l_wk17, @l_wk18, @l_wk19, @l_wk20,
           @l_wk21, @l_wk22, @l_wk23, @l_wk24, @l_wk25,
           @l_wk26, @l_wk27, @l_wk28, @l_wk29, @l_wk30,
           @l_wk31, @l_wk32, @l_wk33, @l_wk34, @l_wk35,
           @l_wk36, @l_wk37, @l_wk38, @l_wk39, @l_wk40,
           @l_wk41, @l_wk42, @l_wk43, @l_wk44, @l_wk45,
           @l_wk46, @l_wk47, @l_wk48, @l_wk49, @l_wk50,
           @l_wk51, @l_wk52;
 END;

 CLOSE l_audit_plan_cur;
 DEALLOCATE l_audit_plan_cur;
 --SELECT * from (select @l_err_code err_code, @l_err_message err_message) a;
 --select  location_id,year,email_id, Convert(VARCHAR(100),start_date) +' 08:00:00.000' start_date,
 --Convert(VARCHAR(100),start_date) +' 09:00:00.000' end_date,
 --line_name  from mh_lpa_AuditPlanInvite where location_id=@l_location_id and year=@l_year


	-- SET  @l_time_offset	= CAST(REPLACE(@l_timezone_code,':','.') as FLOAT);
	-- SET  @l_time_offset = @l_time_offset * -1.0;
 	SET  @l_timezone_code	= REPLACE(@l_timezone_code,':','.');
	SET  @l_time_offset	= CAST(REPLACE(@l_timezone_code,'.30','.50') as FLOAT);
	SET  @l_time_offset = @l_time_offset * -60.0;
	-- SET  @l_time_offset = @l_time_offset * 60.0;

  select  i.location_id,
		@l_location_name location_name,
		year,
		email_id,
		Convert(VARCHAR(100),start_date) start_date,
		Convert(VARCHAR(100),end_date) end_date,
		FORMAT(DATEADD(minute,@l_time_offset, CAST(FORMAT(start_date,'yyyy-MM-dd'+' 08:00:00') as datetime) ),'yyyyMMddTHHmmssZ') start_date_formatted,
		FORMAT(DATEADD(minute,@l_time_offset, CAST(FORMAT(start_date,'yyyy-MM-dd'+' 09:00:00') as datetime) ),'yyyyMMddTHHmmssZ') end_date_formatted,
--		TODATETIMEOFFSET(CAST(start_date as datetime), @l_timezone_code) end_date,
--		Convert(VARCHAR(100),start_date) +' 08:00:00.000' start_date,
--		Convert(VARCHAR(100),start_date) +' 09:00:00.000' end_date,
 line_name  
 from mh_lpa_AuditPlanInvite i
 where i.location_id=@l_location_id and year=@l_year



 SELECT location_id, @l_location_name location_name, year,email_id, 
 		Convert(VARCHAR(100),start_date) start_date,
		Convert(VARCHAR(100),end_date) end_date,
		FORMAT(DATEADD(minute,@l_time_offset, CAST(FORMAT(start_date,'yyyy-MM-dd'+' 08:00:00') as datetime) ),'yyyyMMddTHHmmssZ') start_date_formatted,
		FORMAT(DATEADD(minute,@l_time_offset, CAST(FORMAT(start_date,'yyyy-MM-dd'+' 09:00:00') as datetime) ),'yyyyMMddTHHmmssZ') end_date_formatted,
line_name 
 from mh_lpa_AuditPlan_Del_Invite where location_id=@l_location_id and year=@l_year
