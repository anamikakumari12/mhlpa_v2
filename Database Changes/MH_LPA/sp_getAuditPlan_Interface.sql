USE [mhlpa_db]
GO
/****** Object:  StoredProcedure [dbo].[sp_getAuditPlan_Interface]    Script Date: 5/29/2019 11:57:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec sp_getAuditPlan_Interface 30,2019
ALTER PROCEDURE [dbo].[sp_getAuditPlan_Interface](@p_location_id  int,
     @p_year    int)
AS
BEGIN
 SELECT int.location_id
      ,int.line_id
      ,lm.line_name
      ,[year]
      ,[wk1]
      ,[wk2]
      ,[wk3]
      ,[wk4]
      ,[wk5]
      ,[wk6]
      ,[wk7]
      ,[wk8]
      ,[wk9]
      ,[wk10]
      ,[wk11]
      ,[wk12]
      ,[wk13]
      ,[wk14]
      ,[wk15]
      ,[wk16]
      ,[wk17]
      ,[wk18]
      ,[wk19]
      ,[wk20]
      ,[wk21]
      ,[wk22]
      ,[wk23]
      ,[wk24]
      ,[wk25]
      ,[wk26]
      ,[wk27]
      ,[wk28]
      ,[wk29]
      ,[wk30]
      ,[wk31]
      ,[wk32]
      ,[wk33]
      ,[wk34]
      ,[wk35]
      ,[wk36]
      ,[wk37]
      ,[wk38]
      ,[wk39]
      ,[wk40]
      ,[wk41]
      ,[wk42]
      ,[wk43]
      ,[wk44]
      ,[wk45]
      ,[wk46]
      ,[wk47]
      ,[wk48]
      ,[wk49]
      ,[wk50]
      ,[wk51]
      ,[wk52] 
 FROM   mh_lpa_audit_plan_interface int,
   mh_lpa_line_master lm
 WHERE  int.location_id = @p_location_id
 AND    year = @p_year
 AND   int.line_id = lm.line_id
 AND  isnull(end_date, dateadd(day, 1,getdate())) > getdate()
 UNION
 SELECT [location_id]
      ,[line_id]
      ,[line_name]
   ,@p_year
      , '0' wk1
      , '0' wk2
      , '0' wk3
      , '0' wk4
      , '0' wk5
      , '0' wk6
      , '0' wk7
      , '0' wk8
      , '0' wk9
      , '0' wk10
      , '0' wk11
      , '0' wk12
      , '0' wk13
      , '0' wk14
      , '0' wk15
      , '0' wk16
      , '0' wk17
      , '0' wk18
      , '0' wk19
      , '0' wk20
      , '0' wk21
      , '0' wk22
      , '0' wk23
      , '0' wk24
      , '0' wk25
      , '0' wk26
      , '0' wk27
      , '0' wk28
      , '0' wk29
      , '0' wk30
      , '0' wk31
      , '0' wk32
      , '0' wk33
      , '0' wk34
      , '0' wk35
      , '0' wk36
      , '0' wk37
      , '0' wk38
      , '0' wk39
      , '0' wk40
      , '0' wk41
      , '0' wk42
      , '0' wk43
      , '0' wk44
      , '0' wk45
      , '0' wk46
      , '0' wk47
      , '0' wk48
      , '0' wk49
      , '0' wk50
      , '0' wk51
      , '0' wk52 
 FROM  mh_lpa_line_master lm
 WHERE lm.line_id NOT IN (select line_id from mh_lpa_audit_plan_interface
       WHERE  location_id = @p_location_id
       AND    year = @p_year)
 AND  lm.location_id = @p_location_id
 AND  isnull(end_date, dateadd(day, 1,getdate())) > getdate()
 order by line_name
END;