USE [mhlpa_db]
GO
/****** Object:  StoredProcedure [dbo].[DelAuditPlan_byDate]    Script Date: 5/29/2019 11:26:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DECLARE @l_del_invite_to_list  VARCHAR(MAX),
--        @l_err_code    int,
--        @l_err_message   varchar(100)
--exec DelAuditPlan_byDate 2487,'1751,1802,1750','2018-12-31','2019-01-04',@l_del_invite_to_list = @l_del_invite_to_list OUTPUT,
--        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;

--select @l_del_invite_to_list,@l_err_code,@l_err_message
ALTER  PROCEDURE [dbo].[DelAuditPlan_byDate](@p_line_id   int,
		@p_userlist		VARCHAR(MAX),
        @l_st_date    DATE,
        @l_end_date    DATE,
        @l_del_invite_to_list  VARCHAR(MAX) OUTPUT,
        @l_err_code    int OUTPUT,
        @l_err_message   varchar(100) OUTPUT
        )
AS
 SET NOCOUNT ON;
 
BEGIN
/* This procedure needs date overlap validation still */
 DECLARE  @l_auditor_id   INT;
 DECLARE  @l_audit_plan_id   INT;
 DECLARE @count INT
 
 SET @l_err_code = 0;
 SET @l_err_message = 'Initializing';
 
 SELECT @count=Count(*) FROM   mh_lpa_audit_plan p
 WHERE  planned_date = @l_st_date
 AND    planned_date_end = @l_end_date
 AND    p.line_id = @p_line_id
 AND to_be_audited_by_user_id IN (SElECT item from SplitString(@p_userlist,','))
 print @count
 IF @count=0
 BEGIN
 SELECT @l_audit_plan_id = audit_plan_id,
   @l_auditor_id = to_be_audited_by_user_id
 FROM   mh_lpa_audit_plan p
 WHERE  planned_date = @l_st_date
 AND    planned_date_end = @l_end_date
 AND    p.line_id = @p_line_id
 
 IF @l_audit_plan_id IS NOT NULL
 BEGIN
  EXEC dbo.[DelAuditPlan] @l_audit_plan_id, @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
 END;
 
 IF @l_auditor_id IS NOT NULL
  SELECT @l_del_invite_to_list = email_id
  FROM   mh_lpa_user_master
  WHERE  user_id = @l_auditor_id;
  END
ELSE
BEGIN
SET @l_del_invite_to_list='';
 SELECT audit_plan_id,
   to_be_audited_by_user_id INTO #TEMP
 FROM   mh_lpa_audit_plan p
 WHERE  planned_date = @l_st_date
 AND    planned_date_end = @l_end_date
 AND    p.line_id = @p_line_id
  AND to_be_audited_by_user_id NOT IN (SElECT item from SplitString(@p_userlist,','))

   DECLARE l_audit_plan_cursor CURSOR FOR SELECT * FROM #TEMP;
    OPEN l_audit_plan_cursor
	FETCH NEXT FROM l_audit_plan_cursor INTO @l_audit_plan_id,@l_auditor_id
	 WHILE @@FETCH_STATUS = 0 
		BEGIN
		 IF @l_audit_plan_id IS NOT NULL
 BEGIN
  EXEC dbo.[DelAuditPlan] @l_audit_plan_id, @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;
 END;
 
 IF @l_auditor_id IS NOT NULL
  SELECT @l_del_invite_to_list =@l_del_invite_to_list+';'+ email_id
  FROM   mh_lpa_user_master
  WHERE  user_id = @l_auditor_id;

		FETCH NEXT FROM l_audit_plan_cursor INTO @l_audit_plan_id,@l_auditor_id
		END
		CLOSE l_audit_plan_cursor;
 DEALLOCATE l_audit_plan_cursor;
 IF(LEN(@l_del_invite_to_list)>0)
	SET @l_del_invite_to_list= Substring(@l_del_invite_to_list,1,LEN(@l_del_invite_to_list)-1)
END

END






