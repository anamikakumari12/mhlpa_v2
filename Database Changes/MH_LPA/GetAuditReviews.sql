USE [mhlpa_db]
GO
/****** Object:  StoredProcedure [dbo].[GetAuditReviews]    Script Date: 6/3/2019 10:51:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec GetAuditReviews 2487
ALTER PROCEDURE [dbo].[GetAuditReviews](@p_line_id int)  
 
AS  
BEGIN  
   SELECT loc_answer_id,
		audit_id,
		section_id,
		section_name,
		section_display_sequence,
		question_id,
		question,
		help_text,
		question_display_sequence,
		remarks,
		image_file_name,
		review_comments,
		audit_date,
		audited_by_user_id,
		mhu.emp_full_name
		INTO #TEMP
	FROM (
		SELECT  la.loc_answer_id,
				la.audit_id,
				sm.section_id,
				sm.section_name,
				sm.display_sequence  section_display_sequence,
				qm.question_id,
				qm.question,
				qm.help_text,
				lq.display_sequence  question_display_sequence,
				la.remarks,
				la.image_file_name,
				la.review_comments,
				la.audit_date,
				la.audited_by_user_id
		FROM    mh_lpa_local_answers la,
				mh_lpa_question_master qm,
				mh_lpa_local_questions lq,
				mh_lpa_section_master sm
		WHERE  la.answer = 1
		--AND    la.audit_id = (select max(audit_id) 
		--						from mh_lpa_local_answers la1
		--						where la1.line_id = @p_line_id)
		AND	   la.line_id=@p_line_id
		AND    la.question_id = qm.question_id
		AND    la.question_id < 10000
		AND    la.location_id = lq.location_id
		AND    lq.question_id = qm.question_id
		AND    lq.section_id = sm.section_id
		AND    ISNULL(la.review_closed_status,0) = 0
		UNION
		SELECT  la.loc_answer_id,
				la.audit_id,
				sm.section_id,
				sm.section_name,
				sm.display_sequence  section_display_sequence,
				qm.local_question_id,
				qm.local_question,
				qm.local_help_text,
				lq.display_sequence  question_display_sequence,
				la.remarks,
				la.image_file_name,
				la.review_comments,
				la.audit_date,
				la.audited_by_user_id
		FROM    mh_lpa_local_answers la,
				mh_lpa_local_question_master qm,
				mh_lpa_local_questions lq,
				mh_lpa_section_master sm
		WHERE  la.answer = 1
		--AND    la.audit_id = (select max(audit_id) 
		--						from mh_lpa_local_answers la1
		--						where la1.line_id = @p_line_id)
		AND	   la.line_id=@p_line_id
		AND    la.question_id = qm.local_question_id
		AND    la.question_id >= 10000
		AND    lq.question_id = qm.local_question_id
		AND    la.location_id = lq.location_id
		AND    lq.section_id = sm.section_id
		AND    ISNULL(la.review_closed_status,0) = 0
		 ) tmp
		 LEFT JOIN mh_lpa_user_master mhu ON tmp.audited_by_user_id=mhu.user_id
	ORDER BY audit_id desc,  section_display_sequence, question_display_sequence

	--SELECT distinct audited_by_user_id, emp_full_name FROM #TEMP 
	SELECT distinct audit_id,CONVERT(VARCHAR(100),audit_date)+'('+CONVERT(VARCHAR(100),emp_full_name)+')' audit_date FROM #TEMP order by audit_id desc
	SELECT loc_answer_id,
		audit_id,
		section_id,
		section_name,
		section_display_sequence,
		question_id,
		question,
		help_text,
		question_display_sequence,
		remarks,
		image_file_name,
		review_comments,
		audited_by_user_id
		FROM #TEMP
    
END




