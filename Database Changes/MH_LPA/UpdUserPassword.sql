USE [mhlpa_db]
GO
/****** Object:  StoredProcedure [dbo].[UpdUserPassword]    Script Date: 5/31/2019 11:48:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[UpdUserPassword]
(
                @p_user_id               	INT,
                @PASSWORD               	VARCHAR(MAX)
)
AS
BEGIN

	DECLARE  @l_err_code 		INT;
	DECLARE  @l_err_message 	VARCHAR(100);
 
--exec InsUserDetails 1, 'ab', 'abc', 'kns',1,'Y','Manager','06-01-2017','06-25-2017',null,'N','Y','Insert'
                -- SET NOCOUNT ON added to prevent extra result sets from
                -- interfering with SELECT statements.
                SET NOCOUNT ON;
 
 	IF @p_user_id IS NOT NULL AND @p_user_id != 0
 
		UPDATE mh_lpa_user_master
		SET   passwd = @PASSWORD
		WHERE user_id = @p_user_id

	SET @l_err_code = 0;
	SET @l_err_message = 'Password Reset Successfully';

	SELECT * FROM (SELECT @l_err_code err_code, @l_err_message err_message) a FOR JSON AUTO

	
END



