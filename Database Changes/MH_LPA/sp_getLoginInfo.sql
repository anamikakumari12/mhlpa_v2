USE [mhlpa_db]
GO
ALTER TABLE mh_lpa_user_master ALTER COLUMN  passwd VARCHAR(MAX)
GO
/****** Object:  StoredProcedure [dbo].[sp_getLoginInfo]    Script Date: 5/31/2019 11:44:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[sp_getLoginInfo](
@user_name VARCHAR(100),
@PASSWORD VARCHAR(MAX)
)
AS
BEGIN
SELECT user_id
,username
,passwd
,emp_full_name
,display_name_flag
,email_id
,location_id
,(select location_name from [dbo].[mh_lpa_location_master] where location_id=um.location_id) location_name
,role
,(select role_id from [dbo].[mh_lpa_role_master] where rolename=um.role) role_id
,global_admin_flag
,site_admin_flag
,start_date
,end_date
,region_id
,country_id
 FROM [dbo].[mh_lpa_user_master] um where username=@user_name AND passwd=@PASSWORD
 AND  ISNULL(end_date, getdate())  >= getdate()
END



