USE [mhlpa_db]
GO

/****** Object:  UserDefinedTableType [dbo].[AuditPlanInterfaceTableType]    Script Date: 5/28/2019 4:56:48 PM ******/
DROP TYPE [dbo].[AuditPlanInterfaceTableType]
GO

/****** Object:  UserDefinedTableType [dbo].[AuditPlanInterfaceTableType]    Script Date: 5/28/2019 4:56:48 PM ******/
CREATE TYPE [dbo].[AuditPlanInterfaceTableType] AS TABLE(
	[location_id] [int] NULL,
	[line_id] [int] NULL,
	[line_name] [varchar](100) NULL,
	[year] [varchar](100) NULL,
	[wk1] [varchar](100) NULL,
	[wk2] [varchar](100) NULL,
	[wk3] [varchar](100) NULL,
	[wk4] [varchar](100) NULL,
	[wk5] [varchar](100) NULL,
	[wk6] [varchar](100) NULL,
	[wk7] [varchar](100) NULL,
	[wk8] [varchar](100) NULL,
	[wk9] [varchar](100) NULL,
	[wk10] [varchar](100) NULL,
	[wk11] [varchar](100) NULL,
	[wk12] [varchar](100) NULL,
	[wk13] [varchar](100) NULL,
	[wk14] [varchar](100) NULL,
	[wk15] [varchar](100) NULL,
	[wk16] [varchar](100) NULL,
	[wk17] [varchar](100) NULL,
	[wk18] [varchar](100) NULL,
	[wk19] [varchar](100) NULL,
	[wk20] [varchar](100) NULL,
	[wk21] [varchar](100) NULL,
	[wk22] [varchar](100) NULL,
	[wk23] [varchar](100) NULL,
	[wk24] [varchar](100) NULL,
	[wk25] [varchar](100) NULL,
	[wk26] [varchar](100) NULL,
	[wk27] [varchar](100) NULL,
	[wk28] [varchar](100) NULL,
	[wk29] [varchar](100) NULL,
	[wk30] [varchar](100) NULL,
	[wk31] [varchar](100) NULL,
	[wk32] [varchar](100) NULL,
	[wk33] [varchar](100) NULL,
	[wk34] [varchar](100) NULL,
	[wk35] [varchar](100) NULL,
	[wk36] [varchar](100) NULL,
	[wk37] [varchar](100) NULL,
	[wk38] [varchar](100) NULL,
	[wk39] [varchar](100) NULL,
	[wk40] [varchar](100) NULL,
	[wk41] [varchar](100) NULL,
	[wk42] [varchar](100) NULL,
	[wk43] [varchar](100) NULL,
	[wk44] [varchar](100) NULL,
	[wk45] [varchar](100) NULL,
	[wk46] [varchar](100) NULL,
	[wk47] [varchar](100) NULL,
	[wk48] [varchar](100) NULL,
	[wk49] [varchar](100) NULL,
	[wk50] [varchar](100) NULL,
	[wk51] [varchar](100) NULL,
	[wk52] [varchar](100) NULL
)
GO


