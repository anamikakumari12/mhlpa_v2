--select [dbo].[GetAuditInfo](2487)

ALTER FUNCTION [dbo].[GetAuditInfo](@p_line_id int)  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
		SELECT distinct
				la.audit_id,
		CONVERT(VARCHAR(100),audit_date)+'('+CONVERT(VARCHAR(100),mhu.emp_full_name)+')' audit_date
		FROM    mh_lpa_local_answers la LEFT JOIN mh_lpa_user_master mhu ON la.audited_by_user_id=mhu.user_id
		WHERE la.line_id=@p_line_id
	ORDER BY audit_id desc
    FOR JSON AUTO)  
END