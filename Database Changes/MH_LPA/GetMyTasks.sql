USE [mhlpa_db]
GO
/****** Object:  UserDefinedFunction [dbo].[GetMyTasks]    Script Date: 6/3/2019 12:18:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[GetMyTasks](@p_line_id int, @p_audit_id int)  
 RETURNS NVARCHAR(MAX)  
AS  
BEGIN  
   RETURN (
   SELECT loc_answer_id,
		audit_id,
		Shift_No,
		section_id,
		section_name,
		section_display_sequence,
		question_id,
		question,
		help_text,
		question_display_sequence,
		remarks,
		image_file_name
	FROM (
		SELECT  la.loc_answer_id,
				la.audit_id,
				la.Shift_No,
				sm.section_id,
				sm.section_name,
				sm.display_sequence  section_display_sequence,
				qm.question_id,
				qm.question,
				qm.help_text,
				lq.display_sequence  question_display_sequence,
				la.remarks,
				la.image_file_name
		FROM    mh_lpa_local_answers la,
				mh_lpa_question_master qm,
				mh_lpa_local_questions lq,
				mh_lpa_section_master sm
		WHERE  la.answer = 1
		--AND    la.audit_id = (select max(audit_id) from mh_lpa_local_answers loc 
		--					where loc.line_id = @p_line_id)
		AND    la.audit_id = @p_audit_id
		AND	   la.line_id=@p_line_id
		AND    la.question_id = qm.question_id
		AND    la.question_id < 10000
		AND    lq.question_id = qm.question_id
		AND    lq.location_id = la.location_id
		AND    lq.section_id = sm.section_id
		AND    ISNULL(la.review_closed_status,0) = 0
		UNION
		SELECT  la.loc_answer_id,
				la.audit_id,
				la.Shift_No,
				sm.section_id,
				sm.section_name,
				sm.display_sequence  section_display_sequence,
				qm.local_question_id,
				qm.local_question,
				qm.local_help_text,
				lq.display_sequence  question_display_sequence,
				la.remarks,
				la.image_file_name
		FROM    mh_lpa_local_answers la,
				mh_lpa_local_question_master qm,
				mh_lpa_local_questions lq,
				mh_lpa_section_master sm
		WHERE  la.answer = 1
		--AND    la.audit_id = (select max(audit_id) 
		--					from mh_lpa_local_answers la1 
		--					where line_id = @p_line_id)
		AND    la.audit_id = @p_audit_id
		AND	   la.line_id=@p_line_id
		AND    la.question_id = qm.local_question_id
		AND    la.question_id >= 10000
		AND    lq.question_id = qm.local_question_id
		AND    lq.location_id = la.location_id
		AND    lq.section_id = sm.section_id
		AND    ISNULL(la.review_closed_status,0) = 0
		 ) tmp
	ORDER BY section_display_sequence, question_display_sequence
    FOR JSON AUTO)  
END

