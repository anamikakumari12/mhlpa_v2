

--exec SP_GetMyQuestionListForWeb 1,'French'
ALTER PROCEDURE [dbo].[SP_GetMyQuestionListForWeb ](@p_location_id int, @language VARCHAR(100))  

AS  
BEGIN  

   SELECT section_id,
		section_name,
		section_display_sequence,
		question_id,
		question,
		na_flag,
		max_no_of_pics_allowed,
		help_text,
		question_display_sequence,
		how_to_check
		INTO #temp1
		FROM (
				SELECT  sm.section_id,
						sm.section_name,
						sm.display_sequence  section_display_sequence,
						qm.question_id,
						qm.question,
						qm.na_flag,
						max_no_of_pics_allowed,
						qm.help_text,
						lq.display_sequence  question_display_sequence,
						qm.how_to_check
				FROM    mh_lpa_question_master qm,
						mh_lpa_local_questions lq,
						mh_lpa_section_master sm
				WHERE  lq.location_id = @p_location_id
				AND    lq.question_id < 10000
				AND    lq.question_id = qm.question_id
				AND    lq.section_id = sm.section_id
				UNION
				SELECT  sm.section_id,
						sm.section_name,
						sm.display_sequence  section_display_sequence,
						qm.local_question_id,
						qm.local_question,
						qm.na_flag,
						max_no_of_pics_allowed,
						qm.local_help_text,
						lq.display_sequence  question_display_sequence,
						qm.how_to_check
				FROM    mh_lpa_local_question_master qm,
						mh_lpa_local_questions lq,
						mh_lpa_section_master sm
				WHERE  lq.location_id = @p_location_id
				AND    lq.question_id = qm.local_question_id
				AND    lq.section_id = sm.section_id
				 ) tmp
		ORDER BY question_display_sequence


		Declare @col VARCHAR(MAX), @Query VARCHAR(MAX)
		select @col=default_language FROM [dbo].[mh_lpa_location_master] where location_id=@p_location_id
		IF(@col is null)
			SET @col='English'

		SET @Query ='
         SELECT t.section_id,
		CASE WHEN t2.'+@language+' IS NULL THEN t.section_name ELSE t2.'+@language+' END section_name,
		section_display_sequence,
		t.question_id,
		CASE WHEN t1.'+@language+' IS NULL THEN t.question ELSE t1.'+@language+' END question,
		na_flag,
		max_no_of_pics_allowed,
		help_text,
		question_display_sequence,
		how_to_check
		FROM  #temp1 t LEFT JOIN [dbo].[mh_lpa_Question_language_master] t1 ON t1.Question_id=t.question_id
		LEFT JOIN mh_lpa_section_language_master t2 ON t2.section_id=t.section_id
		ORDER BY section_display_sequence, question_display_sequence
		'

		PRINT @Query
		EXEC(@Query)
END



