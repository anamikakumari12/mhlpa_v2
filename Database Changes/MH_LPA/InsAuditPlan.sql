USE [mhlpa_db]
GO
/****** Object:  StoredProcedure [dbo].[InsAuditPlan]    Script Date: 5/29/2019 9:29:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DECLARE  @l_invite_to_list  varchar(max),
--        @l_err_code    int,
--        @l_err_message   varchar(100)
--exec InsAuditPlan null,'1751,1799','2018-12-31','2019-01-04',2487,@l_invite_to_list = @l_invite_to_list OUTPUT, 
--        @l_err_code = @l_err_code OUTPUT, @l_err_message = @l_err_message OUTPUT;

ALTER PROCEDURE [dbo].[InsAuditPlan](@p_audit_plan_id   int,
        @p_user_id    VARCHAR(MAX), /* Auditor's User Id */ 
        @p_planned_date   date,
        @p_planned_date_end  date,
        @p_line_id    int,
        @l_invite_to_list  varchar(max) OUTPUT,
        @l_err_code    int OUTPUT,
        @l_err_message   varchar(100) OUTPUT
        )
AS
 SET NOCOUNT ON;
 
BEGIN
/* This procedure needs date overlap validation still */
 DECLARE @l_send_note_to_owner VARCHAR(10);
 DECLARE @l_location_id   INT;
 DECLARE @l_count    INT;
 DECLARE @l_count1    INT;
 
 SET @l_err_code = 0;
 SET @l_err_message = 'Initializing';

 DECLARE @QUERY VARCHAR(MAX)
 DECLARE @TEMP TABLE(User_ID INT)
 DECLARE @RowsToProcess  int
DECLARE @CurrentRow     int
SET @QUERY='select user_id from mh_lpa_user_Master where convert(varchar(10),user_id) in ('+@p_user_id+')'
PRINT(@QUERY)
INSERT INTO @TEMP
EXEC(@QUERY)


DECLARE @USER INT
IF EXISTS(SELECT * FROM @TEMP)
BEGIN
DECLARE EMP_CURSOR CURSOR  FOR  
SELECT * FROM @TEMP  
OPEN EMP_CURSOR  
FETCH NEXT FROM EMP_CURSOR INTO  @USER
WHILE @@FETCH_STATUS = 0  
	BEGIN
	print @USER
	SELECT @l_location_id = location_id
 FROM   mh_lpa_line_master
 WHERE  line_id = @p_line_id;
 SELECT @l_invite_to_list = email_id
 FROM   mh_lpa_user_master
 WHERE  user_id = @USER;

 IF @p_audit_plan_id IS NOT NULL AND @p_audit_plan_id != 0 
 
  BEGIN
 
   UPDATE mh_lpa_audit_plan
   SET    to_be_audited_by_user_id = @USER,
     planned_date = @p_planned_date,
     planned_date_end = @p_planned_date_end,
     line_id = @p_line_id,
     location_id = @l_location_id
   WHERE audit_plan_id = @p_audit_plan_id;
   
   SET @l_err_code = 0;
   SET @l_err_message = 'Updated Successfully';
  END;  
 ELSE
  BEGIN
   SELECT @l_count = count(*)
   FROM   mh_lpa_audit_plan
   WHERE  FORMAT(ISNULL(planned_date,getdate()),'dd-MM-yyyy') = FORMAT(@p_planned_date,'dd-MM-yyyy')
   AND    FORMAT(ISNULL(planned_date_end,getdate()),'dd-MM-yyyy') = FORMAT(@p_planned_date_end,'dd-MM-yyyy')
   AND    line_id = @p_line_id

   print @l_count 
   IF @l_count > 0
   BEGIN
    SELECT @l_count1 = count(*)
    FROM   mh_lpa_audit_plan
    WHERE  to_be_audited_by_user_id = @USER
    AND    FORMAT(ISNULL(planned_date,getdate()),'dd-MM-yyyy') = FORMAT(@p_planned_date,'dd-MM-yyyy')
    AND    FORMAT(ISNULL(planned_date_end,getdate()),'dd-MM-yyyy') = FORMAT(@p_planned_date_end,'dd-MM-yyyy')
    AND    line_id = @p_line_id
    AND    location_id = @l_location_id;
   print @l_count1
    IF @l_count1 = 0 
	INSERT INTO mh_lpa_audit_plan
    (
     to_be_audited_by_user_id,
     planned_date,
     planned_date_end,
     line_id,
     location_id
    ) VALUES (
     @USER,
     @p_planned_date,
     @p_planned_date_end,
     @p_line_id,
     @l_location_id
    );
     --UPDATE mh_lpa_audit_plan
     --SET    to_be_audited_by_user_id = @USER
     --WHERE  FORMAT(ISNULL(planned_date,getdate()),'dd-MM-yyyy') = FORMAT(@p_planned_date,'dd-MM-yyyy')
     --AND    FORMAT(ISNULL(planned_date_end,getdate()),'dd-MM-yyyy') = FORMAT(@p_planned_date_end,'dd-MM-yyyy')
     --AND    line_id = @p_line_id
     --AND    location_id = @l_location_id;
    ELSE
     SET @l_invite_to_list = NULL;
   END;
   ELSE
    INSERT INTO mh_lpa_audit_plan
    (
     to_be_audited_by_user_id,
     planned_date,
     planned_date_end,
     line_id,
     location_id
    ) VALUES (
     @USER,
     @p_planned_date,
     @p_planned_date_end,
     @p_line_id,
     @l_location_id
    );
 
   
   SET @l_err_code = 0;
   SET @l_err_message = 'Inserted Successfully';
  END;

 FETCH  FROM EMP_CURSOR INTO  @USER
END  
CLOSE EMP_CURSOR  
DEALLOCATE EMP_CURSOR 
  END

END
