﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MHLPA_BusinessObject;
using MHLPA_DataAccess;

namespace MHLPA_BusinessLogic
{
    
    public class DistributionBL
    {
        DistributionDAL objDAL = new DistributionDAL();

        public DataTable GetDistributionListBL(UsersBO objUserBO)
        {
            return objDAL.GetDistributionListDAL(objUserBO);
        }

        public DistributionBO SaveDistributionBL(DistributionBO objDistBO)
        {
            return objDAL.SaveDistributionDAL(objDistBO);
        }
    }
}
