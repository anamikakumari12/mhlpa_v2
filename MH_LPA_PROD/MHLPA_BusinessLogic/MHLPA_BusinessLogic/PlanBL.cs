﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MHLPA_BusinessObject;
using MHLPA_DataAccess;

namespace MHLPA_BusinessLogic
{
    public class PlanBL
    {
        PlanDAL objPlanDAL = new PlanDAL();

        public PlanBO SavePlanningBL(PlanBO objPlanBO)
        {
            return objPlanDAL.SavePlanningDAL(objPlanBO);
        }

        public DataSet getMyTaskBL(PlanBO objPlanBO)
        {
            return objPlanDAL.getMyTaskDAL(objPlanBO);
        }

        public PlanBO SaveAuditBL(PlanBO objPlanBO, DataTable dtAnswer)
        {
            
            return objPlanDAL.SaveAuditDAL(objPlanBO, dtAnswer);
        }

        public DataTable GetQuestionsForAuditBL(PlanBO objPlanBO)
        {
            return objPlanDAL.GetQuestionsForAuditDAL(objPlanBO); 
        }

        public PlanBO SaveAuditReviewsBL(PlanBO objPlanBO, DataTable dtReview)
        {
            return objPlanDAL.SaveAuditReviewsDAL(objPlanBO, dtReview);
        }

        public DataTable GetCommentsBL(PlanBO objPlanBO)
        {
            return objPlanDAL.GetCommentsDAL(objPlanBO);
        }

        public PlanBO DeletePlanningBL(PlanBO objPlanBO)
        {
            return objPlanDAL.DeletePlanningDAL(objPlanBO);
        }

        public DataSet GetAnswers(int p)
        {
            return objPlanDAL.GetAnswersDAL(p);
        }

        public DataSet GetAuditScore(PlanBO objPlanBO)
        {
            return objPlanDAL.GetAuditScoreDAL(objPlanBO);
        }

        public object getDataforLPAResultsReportBL(PlanBO objPlanBO)
        {
            throw new NotImplementedException();
        }

        public DataSet SaveBulkPlanningBL(DataTable SelectedUsers)
        {
            return objPlanDAL.SaveBulkPlanningDAL(SelectedUsers);
        }

        public DataTable GetBulkPlansBL(PlanBO objPlanBO)
        {
            return objPlanDAL.GetBulkPlanDAL(objPlanBO); ;
        }

        public PlanBO GetMaxAuditId()
        {
            return objPlanDAL.GetMaxAuditDAL();
        }

        public DataTable GetAllTasks(PlanBO objPlanBO)
        {
            return objPlanDAL.GetAllTasks(objPlanBO);            
        }
    }
}
