﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Questions.aspx.cs" Inherits="MHLPA_Application.Questions" MasterPageFile="~/Site.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        function ExpandSelection(newdiv, olddiv, firstdiv) {
            debugger
            console.log(newdiv);
            firstdiv.className = "panel-collapse collapse";
            olddiv.className = "panel-collapse collapse";
            newdiv.className = "panel-collapse collapse in";

        }
        $('#MainContent_txtSequence').bind('keypress', function (e) {
            debugger;
            var valid = (e.which >= 48 && e.which <= 57);
            if (!valid) {
                e.preventDefault();
            }
        });
        function validateControls() {
            debugger
            var err_flag = 0;
            if ($('#MainContent_txtSequence').val() == "") {
                $('#MainContent_txtSequence').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_txtSequence').css('border-color', '');
            }
            if ($('#MainContent_txtQuestion').val() == "") {
                $('#MainContent_txtQuestion').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtQuestion').css('border-color', '');
            }
            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return setmin();

            }
            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields.');
                return false;
            }
        }
        function setmin() {
            if ($('#MainContent_txtSequence').val() < 0) {
                $('#MainContent_txtSequence').val = 0;
                $('#MainContent_lblError').text('Display Sequence should be greater than and equal to 0.');
                return false;
            }
            else {
                $('#MainContent_lblError').text('');
                return true;
            }
        }

        function isNumberKey(evt, obj) {

            debugger;
            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                     // Number of digits right of decimal point.
                     (match[1] ? match[1].length : 0)
                     // Adjust for scientific notation.
                     - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;

                if (charCode == 46) return false;
            }
            else {
                if (value.length > 2) {
                    if (charCode == 46) return true;
                    else return false;
                }

            }
            if (charCode == 46) return false;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div class="col-md-12">
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">SetUp</a>
                        </li>
                        <li class="current">Questions</li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div>
        <!-- main content start-->
        <div>
            <div class="main-page">
                <div class="row">
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlData">
                                <div class="col-md-8">
                                    <div class="col-md-12 nopad">
                                        <div class="panel panel-default panel-table">
                                            <div class="panel-group tool-tips widget-shadow">
                                                <h4 class="title2">Questions</h4>
                                                <hr />

                                                <div class="col-md-12 nopad">
                                                    <div class="panel-group" id="accordion">
                                                        <div class="panel panel-default">
                                                            <div id="div1" runat="server" class="panel-heading">
                                                                <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="panel-title expand">
                                                                    <div class="right-arrow pull-right">&nbsp;</div>
                                                                    <asp:Label runat="server" ID="lblSec1"></asp:Label>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse1" class="panel-collapse collapse in">
                                                                <div class="panel-body">
                                                                    <div class="col-md-12 nopad mn_table_1">
                                                                        <asp:GridView runat="server" ID="grdSection1" AutoGenerateColumns="false" CssClass="dictionary" Width="100%" HeaderStyle-BackColor="#ff6c52" AlternatingRowStyle-BackColor="#cccccc" AllowPaging="true" AllowSorting="True" OnPageIndexChanging="grdSection1_PageIndexChanging">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Display Sequence" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkseq1" runat="server" Text='<%# Bind("question_display_sequence") %>' OnClick="Select" CommandArgument='<%# Bind("question_id") %>'></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Question" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="75%">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkquestions1" runat="server" Visible="true" Text='<%# Bind("question") %>' OnClick="Select" CommandArgument='<%# Bind("question_id") %>'></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-ForeColor="#04522c">
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ToolTip="Edit" runat="server" Width="50%" ImageUrl="images/edit_icon.jpg" ID="imgAction1" OnClick="Edit" CommandArgument='<%# Bind("question_id") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div id="div2" runat="server" class="panel-heading">
                                                                <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="panel-title expand">
                                                                    <div class="right-arrow pull-right">&nbsp;</div>
                                                                    <asp:Label runat="server" ID="lblSec2"></asp:Label></h4>
                                                            </div>
                                                            <div id="collapse2" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div class="col-md-12 nopad mn_table_1">
                                                                        <asp:GridView runat="server" ID="grdSection2" AutoGenerateColumns="false" CssClass="dictionary" Width="100%" HeaderStyle-BackColor="#ff6c52" AlternatingRowStyle-BackColor="#cccccc" AllowPaging="true" AllowSorting="True" OnPageIndexChanging="grdSection2_PageIndexChanging">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Display Sequence" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkseq2" runat="server" Text='<%# Bind("question_display_sequence") %>' OnClick="Select" CommandArgument='<%# Bind("question_id") %>'></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Question" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="75%">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkquestions2" runat="server" Visible="true" Text='<%# Bind("question") %>' OnClick="Select" CommandArgument='<%# Bind("question_id") %>'></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton runat="server" ToolTip="Edit" Style="width: 50%;" ImageUrl="images/edit_icon.jpg" ID="imgAction2" OnClick="Edit" CommandArgument='<%# Bind("question_id") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="panel panel-default">
                                                            <div id="div3" runat="server" class="panel-heading">
                                                                <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="panel-title expand">
                                                                    <div class="right-arrow pull-right">
                                                                        &nbsp;
                                                                    </div>
                                                                    <asp:Label runat="server" ID="lblSec3"></asp:Label></h4>
                                                            </div>
                                                            <div id="collapse3" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div class="col-md-12 nopad mn_table_1">
                                                                        <asp:GridView runat="server" ID="grdSection3" AutoGenerateColumns="false" CssClass="dictionary" Width="100%" HeaderStyle-BackColor="#ff6c52" AlternatingRowStyle-BackColor="#cccccc" AllowPaging="true" AllowSorting="True" OnPageIndexChanging="grdSection3_PageIndexChanging">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Display Sequence" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkseq3" runat="server" Text='<%# Bind("question_display_sequence") %>' OnClick="Select" CommandArgument='<%# Bind("question_id") %>'></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Question" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="75%">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkquestions3" runat="server" Visible="true" Text='<%# Bind("question") %>' OnClick="Select" CommandArgument='<%# Bind("question_id") %>'></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton runat="server" ToolTip="Edit" Style="width: 50%;" ImageUrl="images/edit_icon.jpg" ID="imgAction3" OnClick="Edit" CommandArgument='<%# Bind("question_id") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div id="div4" runat="server" class="panel-heading">
                                                                <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="panel-title expand">
                                                                    <div class="right-arrow pull-right">&nbsp;</div>
                                                                    <asp:Label runat="server" ID="lblSec4"></asp:Label></h4>
                                                            </div>
                                                            <div id="collapse4" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div class="col-md-12 nopad mn_table_1">
                                                                        <asp:GridView runat="server" ID="grdSection4" AutoGenerateColumns="false" CssClass="dictionary" Width="100%" HeaderStyle-BackColor="#ff6c52" AlternatingRowStyle-BackColor="#cccccc" AllowPaging="true" AllowSorting="True" OnPageIndexChanging="grdSection4_PageIndexChanging">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Display Sequence" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkseq4" runat="server" Text='<%# Bind("question_display_sequence") %>' OnClick="Select" CommandArgument='<%# Bind("question_id") %>'></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Question" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="75%">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkquestions4" runat="server" Visible="true" Text='<%# Bind("question") %>' OnClick="Select" CommandArgument='<%# Bind("question_id") %>'></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton runat="server" ToolTip="Edit" Style="width: 50%;" ImageUrl="images/edit_icon.jpg" ID="imgAction4" OnClick="Edit" CommandArgument='<%# Bind("question_id") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div id="div5" runat="server" class="panel-heading">
                                                                <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse5" class="panel-title expand">
                                                                    <div class="right-arrow pull-right">&nbsp;</div>
                                                                    <asp:Label runat="server" ID="lblSec5"></asp:Label></h4>
                                                            </div>
                                                            <div id="collapse5" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div class="col-md-12 nopad mn_table_1">
                                                                        <asp:GridView runat="server" ID="grdSection5" AutoGenerateColumns="false" CssClass="dictionary" Width="100%" HeaderStyle-BackColor="#ff6c52" AlternatingRowStyle-BackColor="#cccccc" AllowPaging="true" AllowSorting="True" OnPageIndexChanging="grdSection5_PageIndexChanging">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Display Sequence" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkseq5" runat="server" Text='<%# Bind("question_display_sequence") %>' OnClick="Select" CommandArgument='<%# Bind("question_id") %>'></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Question" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="75%">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkquestions5" runat="server" Visible="true" Text='<%# Bind("question") %>' OnClick="Select" CommandArgument='<%# Bind("question_id") %>'></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton runat="server" ToolTip="Edit" Style="width: 50%;" ImageUrl="images/edit_icon.jpg" ID="imgAction5" OnClick="Edit" CommandArgument='<%# Bind("question_id") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div id="div6" runat="server" class="panel-heading">
                                                                <h4 data-toggle="collapse" data-parent="#accordion" href="#collapse6" class="panel-title expand">
                                                                    <div class="right-arrow pull-right">&nbsp;</div>
                                                                    <asp:Label runat="server" ID="lblSec6"></asp:Label></h4>
                                                            </div>
                                                            <div id="collapse6" class="panel-collapse collapse">
                                                                <div class="panel-body">
                                                                    <div class="col-md-12 nopad mn_table_1">
                                                                        <asp:GridView runat="server" ID="grdSection6" AutoGenerateColumns="false" CssClass="dictionary" Width="100%" HeaderStyle-BackColor="#ff6c52" AlternatingRowStyle-BackColor="#cccccc" AllowPaging="true" AllowSorting="True" OnPageIndexChanging="grdSection6_PageIndexChanging">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Display Sequence" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="15%">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkseq5" runat="server" Text='<%# Bind("question_display_sequence") %>' OnClick="Select" CommandArgument='<%# Bind("question_id") %>'></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Question" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="75%">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkquestions5" runat="server" Visible="true" Text='<%# Bind("question") %>' OnClick="Select" CommandArgument='<%# Bind("question_id") %>'></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Action" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton runat="server" ToolTip="Edit" Style="width: 50%;" ImageUrl="images/edit_icon.jpg" ID="imgAction5" OnClick="Edit" CommandArgument='<%# Bind("question_id") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>



                            <div class="col-md-4">
                                <div class="panel-group tool-tips widget-shadow">
                                    <div class="col-md-12 nopad">
                                        <%--<h4><a href="add_question.html">+ Add Question</a></h4>--%>
                                        <asp:Button runat="server" CssClass="btn-add" ID="btnAdd" Text="Add Question" OnClick="btnAdd_Click" />
                                    </div>
                                    <hr />
                                    <asp:Panel runat="server" ID="viewpanel" Visible="true">
                                        <div class="col-md-12 nopad mn_mar_5">
                                            <div class="panel-group wrap" id="bs-collapse">
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <%--<a data-toggle="collapse" data-parent="#bs-collapse" href="#one"> Section 1 </a> --%>
                                                            <asp:Label runat="server" ID="lblSectionNumber"></asp:Label>
                                                        </h4>
                                                    </div>
                                                    <div id="one" class="panel-collapse collapse in">
                                                        <div class="panel-body">
                                                            <div class="row-info mn_table_1">
                                                                <div class="col-md-3 nopad">
                                                                    <p style="line-height: 18px!important;">Display Sequence :</p>
                                                                </div>
                                                                <div class="col-md-9 nopad">
                                                                    <asp:Label runat="server" ID="lblSequence"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="row-info mn_table_1">
                                                                <div class="col-md-3 nopad">
                                                                    <p>Question :</p>
                                                                </div>
                                                                <div class="col-md-9 nopad">
                                                                    <asp:Label runat="server" ID="lblQuestion"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="row-info mn_table_1">
                                                                <div class="col-md-3 nopad">
                                                                    <p style="line-height: 18px!important;">What should be in place? :</p>
                                                                </div>
                                                                <div class="col-md-9 nopad">
                                                                    <asp:Label runat="server" ID="lblHelp"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="row-info mn_table_1">
                                                                <div class="col-md-3 nopad">
                                                                    <p style="line-height: 18px!important;">How to Check? :</p>
                                                                </div>
                                                                <div class="col-md-9 nopad">
                                                                    <asp:Label runat="server" ID="lblcheck"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="row-info mn_table_1">
                                                                <div class="col-md-3 nopad">
                                                                    <p style="line-height: 18px!important;">Display N/A option :</p>
                                                                </div>
                                                                <div class="col-md-9 nopad">
                                                                    <asp:CheckBox class="mn_inp control3" runat="server" Enabled="true" ID="chkDisplay_option" />
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end of panel -->
                                            </div>
                                            <!-- end of #bs-collapse  -->
                                        </div>
                                        <div class="clearfix"></div>
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="addpanel" Visible="false">
                                        <div class="col-md-12 nopad mn_mar_5">
                                            <%--<div class="panel-group wrap" id="Div1">
                                        <div class="panel">
                                            <div id="Div2" class="panel-collapse collapse in">
                                                <div class="panel-body">--%>
                                            <div class="row-info mn_table_1">
                                                <div class="col-md-4 nopad">
                                                    <p style="line-height: 18px!important;">Section Name : </p>
                                                </div>
                                                <div class="col-md-8 nopad">
                                                    <asp:DropDownList runat="server" ID="ddlSection" class="form-control1 mn_inp control3"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row-info mn_table_1">

                                                <div class="col-md-4 nopad">
                                                    <p style="line-height: 18px!important;">Display Sequence :</p>
                                                </div>
                                                <div class="col-md-8 nopad">
                                                    <asp:TextBox class="form-control1 mn_inp control3" onkeypress="return isNumberKey(event,this);" runat="server" ID="txtSequence"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row-info mn_table_1">
                                                <div class="col-md-4 nopad">
                                                    <p>Question :</p>
                                                </div>
                                                <div class="col-md-8 nopad">
                                                    <asp:TextBox Rows="4" Style="width: 100%;" TextMode="MultiLine" runat="server" ID="txtQuestion"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row-info mn_table_1">
                                                <div class="col-md-4 nopad">
                                                    <p style="line-height: 18px!important;">What should be in place?  :</p>
                                                </div>
                                                <div class="col-md-8 nopad">
                                                    <asp:TextBox Rows="4" Style="width: 100%;" TextMode="MultiLine" runat="server" ID="txtHelp"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="row-info mn_table_1">
                                                <div class="col-md-4 nopad">
                                                    <p style="line-height: 18px!important;">How to Check :</p>
                                                </div>
                                                <div class="col-md-8 nopad">
                                                    <asp:TextBox Rows="4" Style="width: 100%;" TextMode="MultiLine" runat="server" ID="txtCheck"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="row-info mn_table_1">
                                                <div class="col-md-4 nopad">
                                                    <label class="mnq_label" style="line-height: 18px!important;">Display N/A option : </label>
                                                </div>
                                                <div class="col-md-8 nopad">
                                                    <asp:CheckBox class="mn_inp control3" runat="server" ID="chkDisplay" />
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <asp:HiddenField runat="server" ID="hdnQuestionId" />
                                            <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                                            <asp:Button runat="server" ID="btnSave" CssClass="btn-add" Text="Save" OnClick="btnSave_Click" OnClientClick="return validateControls();" />
                                        </div>
                                        <%--  </div>
                                        </div>
                                        <!-- end of panel -->
                                    </div>
                                    <!-- end of #bs-collapse  -->
                                </div>--%>
                                        <div class="clearfix"></div>
                                    </asp:Panel>

                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>


