﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuditReports.aspx.cs" Inherits="MHLPA_Application.AuditReports" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <%--<link href="css/fonts.css" rel="stylesheet" />--%>
    <script src="js/datatables_export_lib.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <link href="css/datatables_lib_export.min.css" rel="stylesheet" />
    <link href="css/dataTables.bootstrap.css" rel="stylesheet" />
    <script src="js/moment.js"></script>
    <script src="js/date-euro.js"></script>
    <script src="js/daterangepicker.js"></script>
    <link href="css/daterangepicker.css" rel="stylesheet" />

    <script type="text/javascript">

        $(document).ready(function () {

            var head_content = $('#MainContent_grdAuditReports tr:first').html();
            $('#MainContent_grdAuditReports').prepend('<thead></thead>')
            $('#MainContent_grdAuditReports thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdAuditReports tbody tr:first').hide();
            //$.fn.dataTable.moment('HH:mm MMM D, YY');
            //$.fn.dataTable.moment('dddd, MMMM Do, YYYY');
            $('#MainContent_grdAuditReports').dataTable({
                scrollY: '500px',
                scrollX: '100%',
                sScrollXInner: "100%",
                columnDefs: [
       { type: 'date-euro', targets: 5 }
                ],
                "bInfo": false

            });

        });



        $(function () {
            $("#<%= reportrange.ClientID %>").attr("readonly", "readonly");
            $("#<%= reportrange.ClientID %>").attr("unselectable", "on");
            $("#<%= reportrange.ClientID %>").daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            });
        });

        function validateControls() {
            var err_flag = 0;
            if ($('#MainContent_ddlRegion').val() == "" || $('#MainContent_ddlRegion').val() == null) {
                $('#MainContent_ddlRegion').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_ddlRegion').css('border-color', '');
            }
            if ($('#MainContent_ddlCountry').val() == "" || $('#MainContent_ddlCountry').val() == null) {
                $('#MainContent_ddlCountry').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlCountry').css('border-color', '');
            }

            if ($('#MainContent_ddlLocation').val() == "" || $('#MainContent_ddlLocation').val() == null) {
                $('#MainContent_ddlLocation').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLocation').css('border-color', '');
            }

            if ($('#MainContent_ddlLineName').val() == "" || $('#MainContent_ddlLineName').val() == null) {
                $('#MainContent_ddlLineName').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLineName').css('border-color', '');
            }


            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return setmin();

            }
            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields.');

                return false;
            }
        }



    </script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="SecriptMannager1" runat="server"></asp:ScriptManager>
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Report</a>
                        </li>
                        <li class="current">Audit Report Download</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div>
        <!-- main content start-->
        <div class="md_main">
            <div class="row">
                <div>
                    <div class="clearfix"></div>

                    <div class="filter_panel">
                        <div class="col-md-12">
                            <div class="col-md-2 filter_label">
                                <p><%=Resources.Resource.Region %>* : </p>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-1 filter_label">
                                <p><%=Resources.Resource.Country %>* : </p>
                            </div>
                            <div class="col-md-3">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-1 filter_label" style="width: 12%">
                                <p><%=Resources.Resource.Location %>* : </p>
                            </div>
                            <div class="col-md-3" style="width: 21%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-2 filter_label">
                                <p><%=Resources.Resource.LineNameNo %>* :</p>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLineName" OnSelectedIndexChanged="ddlLineName_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                            </div>

                            <div class="col-md-1 filter_label">
                                <p><%=Resources.Resource.Date %>* :</p>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="reportrange" class="form-control1 mn_inp control3 filter_select_control" runat="server"></asp:TextBox>
                            </div>


                        </div>
                        <div class="col-md-12">

                            <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>

                            <asp:Button runat="server" CssClass="btn-add" ID="btnClear" Style="margin-left: 4px; float: right;" Text="Clear" OnClick="btnClear_Click" />

                            <asp:Button runat="server" CssClass="btn-add" ID="btnFilter" Text="Fetch Reports" OnClick="btnFilter_Click" OnClientClick="return validateControls();" />

                        </div>
                    </div>

                    <div class="clearfix" style="height: 5px;"></div>


                    <div class="result_panel_1">
                        <asp:GridView ID="grdAuditReports" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="mn_th">
                            <Columns>
                                <asp:TemplateField HeaderText="<%$Resources:Resource, Region %>" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label ID="LdlRegion" runat="server" Text='<%# Eval("region_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="<%$Resources:Resource, Country %>" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label ID="LblCountry" runat="server" Text='<%# Eval("country_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="<%$Resources:Resource, Location %>" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label ID="Lbllocation" runat="server" Text='<%# Eval("location_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="<%$Resources:Resource, Line %>" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label ID="LblLine" runat="server" Text='<%# Eval("line_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Audit Date(Central European Time)" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label ID="LblAuditDate" runat="server" Text='<%# Eval("audit_date") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Action" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="50px">
                                    <ItemTemplate>
                                        <asp:ImageButton runat="server" ToolTip="Download As PDF" Width="20px" ImageUrl="images/pdf-download.png" ID="imgAction" OnClick="imgAction_Click" CommandArgument='<%# Bind("audit_id") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>

                </div>

            </div>
        </div>

    </div>

</asp:Content>


