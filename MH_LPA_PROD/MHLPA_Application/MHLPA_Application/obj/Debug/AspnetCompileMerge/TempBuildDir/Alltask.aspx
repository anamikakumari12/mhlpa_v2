﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Alltask.aspx.cs" Inherits="MHLPA_Application.Alltask" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">    <%--<link href="css/fonts.css" rel="stylesheet" />--%>
    <script src="js/datatables_export_lib.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <link href="css/datatables_lib_export.min.css" rel="stylesheet" />
    <link href="css/dataTables.bootstrap.css" rel="stylesheet" />
    <script src="js/moment.js"></script>
    <script src="js/daterangepicker.js"></script>
    <link href="css/daterangepicker.css" rel="stylesheet" />

    <script type="text/javascript">

        $(document).ready(function () {

            var head_content = $('#MainContent_AllTasksGrid tr:first').html();
            $('#MainContent_AllTasksGrid').prepend('<thead></thead>')
            $('#MainContent_AllTasksGrid thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_AllTasksGrid tbody tr:first').hide();

            $('#MainContent_AllTasksGrid').dataTable({                
                scrollY: '500px',
                scrollX: '100%',
                sScrollXInner: "100%",
                "columnDefs": [
                 { "width": "200px", "targets": 6 },
                ],
                "bInfo": false

            });
         
        });



        $(function () {
            $("#<%= reportrange.ClientID %>").attr("readonly", "readonly");
            $("#<%= reportrange.ClientID %>").attr("unselectable", "on");
            $("#<%= reportrange.ClientID %>").daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            });
        });

        function validateControls() {
            var err_flag = 0;
            if ($('#MainContent_ddlRegion').val() == "" || $('#MainContent_ddlRegion').val() == null) {
                $('#MainContent_ddlRegion').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_ddlRegion').css('border-color', '');
            }
            if ($('#MainContent_ddlCountry').val() == "" || $('#MainContent_ddlCountry').val() == null) {
                $('#MainContent_ddlCountry').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlCountry').css('border-color', '');
            }

            if ($('#MainContent_ddlLocation').val() == "" || $('#MainContent_ddlLocation').val() == null) {
                $('#MainContent_ddlLocation').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLocation').css('border-color', '');
            }

            if ($('#MainContent_ddlLineName').val() == "" || $('#MainContent_ddlLineName').val() == null) {
                $('#MainContent_ddlLineName').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLineName').css('border-color', '');
            }


            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return setmin();

            }
            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields.');
                return false;
            }
        }

        function myFunction() {
            var imgurl_server = $("#MainContent_myImg").attr('src');
            $("#imgonpop").attr("src", imgurl_server);
        }
        function myFunction(a) {
            debugger;
            var imgurl_server = $(a).attr('src');
            $("#imgonpop").attr("src", imgurl_server);
        }

    </script>

    <style>
        
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="SecriptMannager1" runat="server"></asp:ScriptManager>
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Audit</a>
                        </li>
                        <li class="current">Audit Review</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div>
        <!-- main content start-->
        <div class="md_main">
            <div class="row">
                <div>
                    <div class="clearfix"></div>

                    <div class="filter_panel">
                        <div class="col-md-12">
                            <div class="col-md-2 filter_label">
                                <p>Region* : </p>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-1 filter_label">
                                <p>Country* : </p>
                            </div>
                            <div class="col-md-3">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-1 filter_label" style="width: 12%">
                                <p>Location* : </p>
                            </div>
                            <div class="col-md-3" style="width: 21%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-2 filter_label">
                                <p>Line Name/No* :</p>
                            </div>
                            <div class="col-md-2">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLineName" OnSelectedIndexChanged="ddlLineName_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                            </div>

                            <div class="col-md-1 filter_label">
                                <p>Date* :</p>
                            </div>
                            <div class="col-md-3">
                                <asp:TextBox ID="reportrange" class="form-control1 mn_inp control3 filter_select_control" runat="server" OnTextChanged="reportrange_TextChanged" ></asp:TextBox>
                            </div>

                            <div class="col-md-1 filter_label" style="width: 12%">
                                <p>Status* : </p>
                            </div>
                            <div class="col-md-2" style="width: 21%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlStatus" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Text="All" Value="0" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Open" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Closed" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Not Reviewed" Value="3"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-12">

                            <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>

                            <asp:Button runat="server" CssClass="btn-add" ID="btnClear" Style="margin-left: 4px; float: right;" Text="Clear" OnClick="btnClear_Click" />

                            <asp:Button runat="server" CssClass="btn-add" ID="btnFilter" Text="Review" OnClick="btnFilter_Click" OnClientClick="return validateControls();" />

                        </div>
                    </div>

                    <div class="clearfix" style="height: 5px;"></div>


                    <div class="result_panel_1">
                        <asp:GridView OnRowDataBound="AllTasksGrid_RowDataBound" ID="AllTasksGrid" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="mn_th">
                            <Columns>
                                <asp:TemplateField HeaderText="Region" HeaderStyle-ForeColor="#ffffff" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="LdlID" runat="server" Text='<%# Eval("audit_id") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Region" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label ID="LdlRegion" runat="server" Text='<%# Eval("region_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Country" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label ID="LblCountry" runat="server" Text='<%# Eval("country_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Location" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label ID="Lbllocation" runat="server" Text='<%# Eval("location_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Line" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label ID="LblLine" runat="server" Text='<%# Eval("line_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Audit Date" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label ID="LblAuditDate" runat="server" Text='<%# Eval("audit_date") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Question Number" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label ID="LblQuenum" runat="server" Text='<%# Eval("question_display_sequence") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Question" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label ID="LblQuestion" runat="server" Text='<%# Eval("question") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Answer" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label ID="LblAnswer" runat="server" Text='<%# Convert.ToString(Eval("answer"))== "1"? "NO" : "YES" %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Audit Comments" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label ID="LblAuditcomments" runat="server" Text='<%# Eval("remarks") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Current Status" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label ID="Lblcurrentstatus" runat="server" Text='<%# !String.IsNullOrEmpty(Convert.ToString(Eval("review_closed_status")))?(Convert.ToString(Eval("review_closed_status"))=="0"? "Open":"Closed"):"Not Reviewed" %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Review Comment" HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                        <asp:Label ID="LblReviewcomment" runat="server" Text='<%# Eval("review_comments") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Audit Image"  HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                         <%--<asp:Image ID="myImg" runat="server"
                                                    Width="50px" Height="50px" ImageUrl='<%# Eval("image_file_name") %>' Style="cursor: pointer" onclick="myFunction();" data-toggle="modal" data-target="#myModal1" />                                         --%>
                                        <asp:Panel runat="server" ID="pnlImages"></asp:Panel>

                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Review Image"  HeaderStyle-ForeColor="#ffffff">
                                    <ItemTemplate>
                                         <asp:Image ID="myImg" runat="server"
                                                    Width="50px" Height="50px" ImageUrl='<%# Eval("review_image_file_name") %>' Style="cursor: pointer" onclick="myFunction();" data-toggle="modal" data-target="#myModal1" />                                         

                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                         <div id="myModal1" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Image</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img src="" alt="image" class="img-responsive" id="imgonpop">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                    </div>

                </div>

            </div>
        </div>

    </div>

</asp:Content>
