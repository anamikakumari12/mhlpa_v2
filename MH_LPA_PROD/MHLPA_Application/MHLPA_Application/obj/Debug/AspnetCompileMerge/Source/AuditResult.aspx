﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuditResult.aspx.cs" Inherits="MHLPA_Application.AuditResult" MasterPageFile="~/Site.Master" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>   

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="HeadContent">
    </asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
      <asp:Panel runat="server" ID="pnlQuestion">
          <div class="container">
                                                            <div id="Div2" class="result_panel">
                               
                                                <div class="panel-body">
                                                    
                                                    <div class="row-info mn_table_1">
                                                        <div>
                                                            <asp:Label style="font-weight:bolder;" class="mn_top" runat="server" ID="lblResult" Text="The Audit is submitted Successfully.The Audit result is sent to respective auditors. If you want to download the audit result then please click on Download as PDF button."></asp:Label>
                                                        </div>
                                                    </div>
                                                   
                                                    
                                                    <div class="clearfix"></div>
                                                     <div>
                                                   <div class="col-md-4"  style="width:50%; align-items:flex-start;">
                                                        <%--<asp:Button runat="server" ID="btnAudit" style="float:right;" CssClass="btn-add" Text="Go To Home" OnClick="btnAudit_Click"/>--%>
                                                    </div>
                                                        <div class="col-md-4 nopad"  style="width:50%; align-items:flex-end;">
                                                            <asp:Button runat="server" ID="btnAudit" style="float:right;" CssClass="btn-add" Text="Go To Home" OnClick="btnAudit_Click"/>
                                                        <asp:Button runat="server" ID="btnDownload" CssClass="btn-add" Text="Download As PDF" OnClick="btnDownload_Click"/>
                                                    
                                                    </div>
                                                    </div>

                                                </div>
                            
                                   </div>
              <div class="clearfix" style="height:5px;"></div>
              <div>
                  
                  <asp:Chart ID="Chart1" runat="server" style="height: 50%; width: 50%; border-width: 0px; margin-left: 25%;"
        BackGradientStyle="LeftRight" Height="400px" Palette="None"   
        PaletteCustomColors="green" Width="400px" ImageLocation="C:\MannHummelLog\Chart_Images">  
        <Series>  
            <asp:Series Name="Series1" ChartType="Radar">  
            </asp:Series>  
        </Series>  
                      <Titles>
                          <asp:Title Name="t1"></asp:Title>
                      </Titles>
        <ChartAreas>  
            <asp:ChartArea Name="ChartArea1">  
            </asp:ChartArea>  
        </ChartAreas>  
                      
        <BorderSkin BackColor="" PageColor="192, 64, 0" />  
    </asp:Chart>   
              </div>
                 <div class="clearfix" style="height:5px;"></div>
                 <div>
                            <div class="col-md-4" style="background-color:white; width:30%">
                               <asp:Label runat="server" ID="sec1"></asp:Label>
                            </div>
                            <div class="col-md-4" style="background-color:white; width:30%">
                               <asp:Label runat="server" ID="sec2"></asp:Label>
                            </div>
                            <div class="col-md-4" style="background-color:white; width:40%">
                               <asp:Label runat="server" ID="sec3"></asp:Label>
                            </div>
                            <div class="col-md-4" style="background-color:white; width:30%">
                               <asp:Label runat="server" ID="sec4"></asp:Label>
                            </div>
                            <div class="col-md-4" style="background-color:white; width:30%">
                               <asp:Label runat="server" ID="sec5"></asp:Label>
                            </div>
                            <div class="col-md-4" style="background-color:white; width:40%">
                               <asp:Label runat="server" ID="sec6"></asp:Label>
                            </div>
                        </div>
                         <div class="clearfix" style="height:5px;"></div>
              <div>
                 
              </div>                            
           </div>    
                    </asp:Panel>
    </asp:Content>
