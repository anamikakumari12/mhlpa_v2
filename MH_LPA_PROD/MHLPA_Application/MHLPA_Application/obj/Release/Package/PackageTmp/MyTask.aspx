﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyTask.aspx.cs" Inherits="MHLPA_Application.MyTask" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="HeadContent">
    <style>
        .aspNetDisabled td{
            padding:5px 10px 5px 0;
        }
    </style>
    <script type="text/javascript">

        $(function () {
            $("#dialog").dialog({
                modal: true,
                autoOpen: false,
                title: "Comments",
                width: 500,
                height: 500
            });
            $("#imgComment").click(function () {
                $('#dialog').dialog('open');
            });
        });

        function myFunction(a) {
            debugger;
            var imageurl = $(a).attr('src');
            //var imgurl_server = $("#MainContent_myImg").attr('src');
            $("#imgonpop").attr("src", imageurl);
        }

        function validateControls() {
            var err_flag = 0;
            if ($('#MainContent_ddlRegion').val() == "" || $('#MainContent_ddlRegion').val() == null) {
                $('#MainContent_ddlRegion').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_ddlRegion').css('border-color', '');
            }
            if ($('#MainContent_ddlCountry').val() == "" || $('#MainContent_ddlCountry').val() == null) {
                $('#MainContent_ddlCountry').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlCountry').css('border-color', '');
            }

            if ($('#MainContent_ddlLocation').val() == "" || $('#MainContent_ddlLocation').val() == null) {
                $('#MainContent_ddlLocation').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLocation').css('border-color', '');
            }

            if ($('#MainContent_ddlLineName').val() == "" || $('#MainContent_ddlLineName').val() == null) {
                $('#MainContent_ddlLineName').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLineName').css('border-color', '');
            }
            //if ($('#MainContent_ddlPartName').val() == "" || $('#MainContent_ddlPartName').val() == null) {
            //    $('#MainContent_ddlPartName').css('border-color', 'red');

            //    err_flag = 1;
            //}
            //else {
            //    $('#MainContent_ddlPartName').css('border-color', '');
            //}

            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return setmin();

            }
            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields.');
                return false;
            }
        }

        function validateComment() {
            debugger;

            var err_flag = 0;
            var value = $('#MainContent_rdbStatus').find('input[type=radio]:checked').val();

            

            if (value == null || value == 'undefined') {
                err_flag = 1;
                $('#MainContent_rdbStatus').css('border-color', 'red');
                $('#MainContent_lblmessage').text('Status is mandatory.');
            }
            else {
                $('#MainContent_rdbStatus').css('border-color', '');
                $('#MainContent_lblmessage').text('');
                if (value == '1') {
                    if ($('#MainContent_txtComment').val() == "") {
                        $('#MainContent_txtComment').css('border-color', 'red');
                        $('#MainContent_lblmessage').text('Comment is mandatory.');
                        err_flag = 1;
                    }
                    else {
                        $('#MainContent_lblmessage').text('');
                        $('#MainContent_txtComment').css('border-color', '');
                    }
                }
            }
            if (err_flag == 0) {
                // $('#MainContent_txtComment').css('border-color', '');
                return true;

            }
            else {
                //$('#MainContent_txtComment').css('border-color', 'red');
                return false;
            }
        }

        function validateCloseComment() {
            debugger;

            var err_flag = 0;
            var value = $('#MainContent_rdbStatus').find('input[type=radio]:checked').val();



            if (value != null || value != 'undefined') {
                
                if (value == '1') {
                    if ($('#MainContent_txtComment').val() == "") {
                        $('#MainContent_txtComment').css('border-color', 'red');
                        $('#MainContent_lblmessage').text('Comment is mandatory.');
                        err_flag = 1;
                    }
                    else {
                        err_flag = 0;
                        $('#MainContent_lblmessage').text('');
                        $('#MainContent_txtComment').css('border-color', '');
                    }
                }
            }
            else {
                err_flag = 0;
                $('#MainContent_rdbStatus').css('border-color', '');
                $('#MainContent_lblmessage').text('');
               
            }
            if (err_flag == 0) {
                // $('#MainContent_txtComment').css('border-color', '');
                return true;

            }
            else {
                //$('#MainContent_txtComment').css('border-color', 'red');
                return false;
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="MainContent">
    <asp:ScriptManager ID="SecriptMannager1" runat="server"></asp:ScriptManager>
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb">Audit</a>
                        </li>
                        <li class="current">Audit Review</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div>
        <!-- main content start-->
        <div>
            <div>
                <div class="row">
                    <div class="clearfix" style="width: 40px;"></div>
                    <asp:Panel runat="server" ID="pnlFilters">
                        <div class="filter_panel">
                            <div>
                                <div class="col-md-4 filter_label" style="width: 14%">
                                    <p>Region* : </p>
                                </div>
                                <div class="col-md-4" style="width: 20%">
                                    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                                <div class="col-md-4 filter_label" style="width: 13%">
                                    <p>Country* : </p>
                                </div>
                                <div class="col-md-4" style="width: 20%">
                                    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                                <div class="col-md-4 filter_label" style="width: 13%">
                                    <p>Location* : </p>
                                </div>
                                <div class="col-md-4" style="width: 20%">
                                    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <%-- <div>
                                <div class="col-md-4 filter_label" style="width:14%"><p>Line Part Name* :</p></div> 
                                <div class="col-md-4" style="width:20%">
                                    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLineProduct"></asp:DropDownList>

                            </div>--%>
                            <div>
                                <div class="col-md-4 filter_label" style="width: 14%">
                                    <p>Line Name/No* :</p>
                                </div>
                                <div class="col-md-4" style="width: 20%">
                                    <asp:DropDownList  class="form-control1 mn_inp control3 filter_select_control"  runat="server" ID="ddlLineName"></asp:DropDownList>

                                </div>
                                <%-- <div class="col-md-4 filter_label" style="width:13%"><p>Product/Part No* :</p></div> 
                                <div class="col-md-4" style="width:20%">
                                    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlPartName"></asp:DropDownList>

                            </div>--%>
                                <div>
                                    <div class="col-md-4" style="width: 66%">
                                        <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                                        <div style="float: right">
                                    <asp:Button runat="server" CssClass="btn-add" ID="btnClear" Style="margin-left: 4px; float: right;" Text="Clear" OnClick="btnClear_Click" />
                                </div>
                                        <div style="float: right">
                                        <asp:Button runat="server" CssClass="btn-add" ID="btnFilter" Text="Review" OnClick="btnFilter_Click" OnClientClick="return validateControls();" />
                                            </div>
                                    </div>
                                    <%--<asp:Button runat="server" ID="btnFilter" CssClass="btn-add" Text="Select" OnClick="btnFilter_Click"/>--%>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="clearfix" style="width: 5px;"></div>
                    <asp:Label runat="server" ID="lblResult"></asp:Label>
                    <asp:UpdatePanel ID="updReview" runat="server">
                        <ContentTemplate>

                            <asp:Panel runat="server" ID="pnlQuestion" Visible="false">
                                <div id="Div2" class="result_panel">
                                    <div class="panel-body">
                                        <div class="row-info mn_table_1">
                                            <div class="col-md-3 nopad">
                                                <p>Section Name : </p>
                                            </div>
                                            <div class="col-md-9 nopad">
                                                <asp:Label runat="server" ID="lblSection" class="mn_top"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="row-info mn_table_1">
                                            <div class="col-md-3 nopad">
                                                <p>Question :</p>
                                            </div>
                                            <div class="col-md-9 nopad">
                                                <asp:Label class="mn_top" runat="server" ID="lblQuestion"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row-info mn_table_1">
                                            <div class="col-md-3 nopad">
                                                <p>Answer :</p>
                                            </div>
                                            <div class="col-md-9 nopad" style="border: 0;">
                                                <asp:RadioButtonList runat="server" ID="rdbAnswer" RepeatDirection="Horizontal" Enabled="false">
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>No</asp:ListItem>
                                                    <asp:ListItem>Not Applicable</asp:ListItem>
                                                </asp:RadioButtonList>
                                                <br />
                                                <asp:TextBox Style="width: 80%" runat="server" ID="txtRemarks" Rows="3" TextMode="MultiLine" Enabled="false"></asp:TextBox>
                                           
                                                <br />
                                                <%--<asp:Image ID="myImg" runat="server"
                                                    Width="50px" Height="50px" Style="cursor: pointer" onclick="myFunction();" data-toggle="modal" data-target="#myModal1" />                                         
                                             --%>   <asp:Panel runat="server" ID="pnlImages">

                                                </asp:Panel>
                                                 </div>
                                          
                                                

                                            <!-- Modal Popup for Image -->
                                            <div id="myModal1" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Image</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img src="" alt="image" class="img-responsive" id="imgonpop">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                     

                                                                                     

                                        </div>
                                        <div class="row-info mn_table_1">
                                            <div class="col-md-3 nopad">
                                                <p>Help :</p>
                                            </div>
                                            <div class="col-md-9 nopad">
                                                <asp:Label class="mn_top" runat="server" ID="lblHelp"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="row-info mn_table_1">
                                            <div class="col-md-3 nopad">
                                                <p>Status* :</p>
                                            </div>
                                            <div class="col-md-9 nopad">
                                                <asp:RadioButtonList runat="server" ID="rdbStatus" RepeatDirection="Horizontal" OnSelectedIndexChanged="rdbStatus_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Text="Open" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Close" Value="1"></asp:ListItem>
                                                </asp:RadioButtonList>
                                                <br />
                                                <asp:TextBox runat="server" ID="txtComment" Style="width: 80%" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                                <asp:FileUpload runat="server" ID="uplImage" style="width:80%; display:block;" />

                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div>
                                            <div class="col-md-4" style="width: 50%; align-items: flex-start;">
                                                <asp:Button runat="server" ID="btnPrevious" Style="float: left;" Visible="false" CssClass="btn-add" Text="Previous" OnClientClick="return validateCloseComment();"  OnClick="btnPrevious_Click" />
                                            </div>
                                            <div class="col-md-4" style="width: 50%; align-items: flex-end;">
                                                <asp:Label runat="server" ID="lblmessage" ForeColor="Red"></asp:Label>
                                                <asp:HiddenField runat="server" ID="lblAuditId" />
                                                <asp:Button runat="server" ID="btnNext" CssClass="btn-add" Text="Next" OnClientClick="return validateCloseComment();" OnClick="btnNext_Click"  />
                                                <asp:Button runat="server" ID="btnSubmit" Visible="false" CssClass="btn-add" Text="Submit" OnClick="btnSubmit_Click" OnClientClick="return validateComment();" />
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </asp:Panel>

                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="rdbStatus" EventName="SelectedIndexChanged" />
                            <asp:PostBackTrigger ControlID="btnNext" />
                            <asp:PostBackTrigger ControlID="btnPrevious" />
                            <asp:PostBackTrigger ControlID="btnSubmit" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
