﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Location.aspx.cs" Inherits="MHLPA_Application.Location" MasterPageFile="~/Site.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1"  ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
             
        function validateControls()
        {
            var err_flag = 0;
            var time_format_flag = 0;
            var time_format_check = true;
            if ($('#MainContent_txtRegion').val() == "") {
                $('#MainContent_txtRegion').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_txtRegion').css('border-color', '');
            }
            if ($('#MainContent_txtCountry').val() == "") {
                $('#MainContent_txtCountry').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtCountry').css('border-color', '');
            }

            if ($('#MainContent_txtLocation').val() == "") {
                $('#MainContent_txtLocation').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtLocation').css('border-color', '');
            }
            if ($('#MainContent_txtNoOfShift').val() == "1") {
                if ($('#MainContent_txtIstart').val() == "") {
                    $('#MainContent_txtIstart').css('border-color', 'red');
                    err_flag = 1;
                }
                else {
                    $('#MainContent_txtIstart').css('border-color', '');
                    time_format_check = validateTime($('#MainContent_txtIstart').val());
                    if (!time_format_check) {
                        time_format_flag = 1;
                        err_flag = 1;
                        $('#MainContent_txtIstart').css('border-color', 'red');
                    }
                     
                }
                if ($('#MainContent_txtIend').val() == "") {
                    $('#MainContent_txtIend').css('border-color', 'red');
                    err_flag = 1;
                }
                else {
                    $('#MainContent_txtIend').css('border-color', '');
                    time_format_check = validateTime($('#MainContent_txtIend').val());
                    if (!time_format_check) {
                        time_format_flag = 1;
                        err_flag = 1;
                        $('#MainContent_txtIend').css('border-color', 'red');
                    }
                }
            }
            else if ($('#MainContent_txtNoOfShift').val() == "2") {
                if ($('#MainContent_txtIstart').val() == "") {
                    $('#MainContent_txtIstart').css('border-color', 'red');
                    err_flag = 1;
                }
                else {
                    $('#MainContent_txtIstart').css('border-color', '');
                    time_format_check = validateTime($('#MainContent_txtIstart').val());
                    if (!time_format_check) {
                        time_format_flag = 1;
                        err_flag = 1;
                        $('#MainContent_txtIstart').css('border-color', 'red');
                    }
                }
                if ($('#MainContent_txtIend').val() == "") {
                    $('#MainContent_txtIend').css('border-color', 'red');
                    err_flag = 1;
                }
                else {
                    $('#MainContent_txtIend').css('border-color', '');
                }
                if ($('#MainContent_txtIIstart').val() == "") {
                    $('#MainContent_txtIIstart').css('border-color', 'red');
                    err_flag = 1;
                }
                else {
                    $('#MainContent_txtIIstart').css('border-color', '');
                    time_format_check = validateTime($('#MainContent_txtIIstart').val());
                    if (!time_format_check) {
                        time_format_flag = 1;
                        err_flag = 1;
                        $('#MainContent_txtIIstart').css('border-color', 'red');
                    }
                }
                if ($('#MainContent_txtIIend').val() == "") {
                    $('#MainContent_txtIIend').css('border-color', 'red');
                    err_flag = 1;
                }
                else {
                    $('#MainContent_txtIIend').css('border-color', '');
                    time_format_check = validateTime($('#MainContent_txtIIend').val());
                    if (!time_format_check) {
                        time_format_flag = 1;
                        err_flag = 1;
                        $('#MainContent_txtIIend').css('border-color', 'red');
                    }
                }
            }
            else if ($('#MainContent_txtNoOfShift').val() == "3") {
                if ($('#MainContent_txtIstart').val() == "") {
                    $('#MainContent_txtIstart').css('border-color', 'red');
                    err_flag = 1;
                    time_format_check = validateTime($('#MainContent_txtIstart').val());
                    if (!time_format_check) {
                        time_format_flag = 1;
                        err_flag = 1;
                        $('#MainContent_txtIstart').css('border-color', 'red');
                    }
                }
                else {
                    $('#MainContent_txtIstart').css('border-color', '');
                }
                if ($('#MainContent_txtIend').val() == "") {
                    $('#MainContent_txtIend').css('border-color', 'red');
                    err_flag = 1;
                }
                else {
                    $('#MainContent_txtIend').css('border-color', '');
                }
                if ($('#MainContent_txtIIstart').val() == "") {
                    $('#MainContent_txtIIstart').css('border-color', 'red');
                    err_flag = 1;
                }
                else {
                    $('#MainContent_txtIIstart').css('border-color', '');
                    time_format_check = validateTime($('#MainContent_txtIIstart').val());
                    if (!time_format_check) {
                        time_format_flag = 1;
                        err_flag = 1;
                        $('#MainContent_txtIIstart').css('border-color', 'red');
                    }
                }
                if ($('#MainContent_txtIIend').val() == "") {
                    $('#MainContent_txtIIend').css('border-color', 'red');
                    err_flag = 1;
                }
                else {
                    $('#MainContent_txtIIend').css('border-color', '');
                }
                if ($('#MainContent_txtIIIstart').val() == "") {
                    $('#MainContent_txtIIIstart').css('border-color', 'red');
                    err_flag = 1;
                }
                else {
                    $('#MainContent_txtIIIstart').css('border-color', '');
                    time_format_check = validateTime($('#MainContent_txtIIIstart').val());
                    if (!time_format_check) {
                        time_format_flag = 1;
                        err_flag = 1;
                        $('#MainContent_txtIIIstart').css('border-color', 'red');
                    }
                }
                if ($('#MainContent_txtIIIend').val() == "") {
                    $('#MainContent_txtIIIend').css('border-color', 'red');
                    err_flag = 1;
                }
                else {
                    $('#MainContent_txtIIIend').css('border-color', '');
                    time_format_check = validateTime($('#MainContent_txtIIIend').val());
                    if (!time_format_check) {
                        time_format_flag = 1;
                        err_flag = 1;
                        $('#MainContent_txtIIIend').css('border-color', 'red');
                    }

                }
            }
            
            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return setmin() ;

            }
            else {
                if(time_format_flag==0)
                    $('#MainContent_lblError').text('Please enter all the mandatory fields.');
                else
                    $('#MainContent_lblError').text('Please enter shift time in HH:MM format and enter all the mandatory fields. ');
                return false;
            } 
        }
        function setmin() {
            if ($('#MainContent_txtNoOfShift').val() <= 0) {
                $('#MainContent_txtNoOfShift').val = 1;
                $('#MainContent_lblError').text('Audit number should be greater than 0.');
                return false;
            }
            else {
                $('#MainContent_lblError').text('');
                return true;
            }
        }
        function showEditShiftTime() {
            if ($('#MainContent_txtNoOfShift').val() == 1) {
                document.getElementById("editshift1").style.display = 'block';
                document.getElementById("editshift2").style.display = 'none';
                document.getElementById("editshift3").style.display = 'none';
                $('#MainContent_txtIend').removeAttr("readonly");
            }
            else if ($('#MainContent_txtNoOfShift').val() == 2) {
                document.getElementById("editshift1").style.display = 'block';
                document.getElementById("editshift2").style.display = 'block';
                document.getElementById("editshift3").style.display = 'none';
                $('#MainContent_txtIend').attr("readonly", "readonly");
                $('#MainContent_txtIIend').removeAttr("readonly");
            }
            else if ($('#MainContent_txtNoOfShift').val() == 3) {
                document.getElementById("editshift1").style.display = 'block';
                document.getElementById("editshift2").style.display = 'block';
                document.getElementById("editshift3").style.display = 'block';
                $('#MainContent_txtIend').attr("readonly", "readonly");
                $('#MainContent_txtIIend').attr("readonly", "readonly");
                $('#MainContent_txtIIIend').removeAttr("readonly");
            }
        }
        function showViewShiftTime() {
            debugger
            if ($('#MainContent_lblshift').text() == '1') {
                document.getElementById("viewshift1").style.display = 'block';
                document.getElementById("viewshift2").style.display = 'none';
                document.getElementById("viewshift3").style.display = 'none';
            }
            else if ($('#MainContent_lblshift').text() == '2') {
                document.getElementById("viewshift1").style.display = 'block';
                document.getElementById("viewshift2").style.display = 'block';
                document.getElementById("viewshift3").style.display = 'none';
            }
            else if ($('#MainContent_lblshift').text() == '3') {
                document.getElementById("viewshift1").style.display = 'block';
                document.getElementById("viewshift2").style.display = 'block';
                document.getElementById("viewshift3").style.display = 'block';
            }
        }
        function validateHhMm(inputField) {
            debugger;
            var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(inputField.value);

            if (isValid) {
                //inputField.css('border-color', 'red');
                $('#MainContent_lblError').text('');
                LoadEndTime();
            } else {
                //inputField.css('border-color', '');
                $('#MainContent_lblError').text('Time should be in HH:MM format.');
            }

            return isValid;
        }
        function validateTime(timevalue) {
            var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(timevalue);
            return isValid;
        }
        function LoadEndTime()
        {
            if ($('#MainContent_txtNoOfShift').val() == "2") {
                if ($('#MainContent_txtIIstart').val() != null && $('#MainContent_txtIIstart').val() != undefined) {
                    $('#MainContent_txtIend').val($('#MainContent_txtIIstart').val());
                }
            }
            else if ($('#MainContent_txtNoOfShift').val() == "3") {
                if ($('#MainContent_txtIIstart').val() != null && $('#MainContent_txtIIstart').val() != undefined) {
                    $('#MainContent_txtIend').val( $('#MainContent_txtIIstart').val());
                }
                if ($('#MainContent_txtIIIstart').val() != null && $('#MainContent_txtIIIstart').val() != undefined) {
                    $('#MainContent_txtIIend').val($('#MainContent_txtIIIstart').val());
                }
            }
        }
    </script>
    <style>
        
        .mn_margin_none
            {
                padding:0
            }
            .mn_margin_none option
            {
                font-size:13px;
                line-height:24px;
                padding:5px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager EnablePageMethods="true" ID="ToolkitScriptManager1" runat="server" EnablePartialRendering="true">
   </ajaxToolkit:ToolkitScriptManager>
    <%--<asp:ScriptManager ID="sc" runat="server" EnablePartialRendering="true"></asp:ScriptManager>--%>
     <div class="col-md-12">
                            <div class='block-web' style='float: left; width: 100%; height:36px;'>
                                <div class="header">
                                    <div class="crumbs">
                                        <!-- Start : Breadcrumbs -->
                                        <ul id="breadcrumbs" class="breadcrumb">
                                            <li>
                                                <a class="mn_breadcrumb">Setup</a>
                                            </li>
                                            <li class="current">Location</li>

                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
    <div>
<!-- main content start-->
		<div>
			<div class="main-page">
			<div class="row">
			<div class="col-md-8">
				<div class="col-md-12 nopad">
            <div class="panel panel-default panel-table">
                   <div class="panel-group tool-tips widget-shadow" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="clearfix"></div>
                       <div class="col-md-12 nopad">
   	                        <div class="row-info"> 
											<div class="col-md-4 nopad"><h4 class="title2"> Location</h4></div> 
											<div class="col-md-8 nopad"></div> 
										</div>
                           </div>
                       
                      <div class="clearfix"></div>
					
					<hr/>
				  <div class="col-md-12 nopad mn_table">
				   <asp:GridView OnRowDataBound="grdLocation_RowDataBound" ID="grdLocation" runat="server" AllowPaging="true" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="location_id" CssClass="dictionary" Width="100%" HeaderStyle-BackColor="#ff6c52" AlternatingRowStyle-BackColor="#cccccc" OnPageIndexChanging="grdLineProduct_PageIndexChanging">

                                        <Columns>                                           
                                         <%--   <asp:TemplateField HeaderText="ID" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="50px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkID" runat="server" OnClick="Select" CommandArgument='<%# Bind("location_id") %>' Text='<%# Bind("location_id") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Region" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                <HeaderTemplate>Region
            <asp:DropDownList ID="ddlRegion_grid" style="width: 150px;" runat="server" OnSelectedIndexChanged="ddlRegion_grid_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems = "true">
            </asp:DropDownList>
        </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lblRegion" runat="server" Visible="true" OnClick="Select" CommandArgument='<%# Bind("location_id") %>' Text='<%# Bind("region_name") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Country" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                <HeaderTemplate>Country
            <asp:DropDownList ID="ddlCountry_grid" style="width: 150px;" runat="server" OnSelectedIndexChanged="ddlCountry_grid_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems = "true">
            </asp:DropDownList>
        </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lblCountry" runat="server" Visible="true" OnClick="Select" CommandArgument='<%# Bind("location_id") %>' Text='<%# Bind("country_name") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                               <HeaderTemplate>Location:
            <asp:DropDownList ID="ddlLocation_grid" style="width: 150px;" runat="server" OnSelectedIndexChanged="ddlLocation_grid_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems = "true">
            </asp:DropDownList>
        </HeaderTemplate>
                                                 <ItemTemplate>
                                                    <asp:LinkButton ID="lblLocation" runat="server" Visible="true" OnClick="Select" CommandArgument='<%# Bind("location_id") %>' Text='<%# Bind("location_name") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                                
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="No of Shifts" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkShifts" runat="server" Visible="true" OnClick="Select" CommandArgument='<%# Bind("location_id") %>' Text='<%# Bind("no_of_shifts") %>'></asp:LinkButton>
                                                 </ItemTemplate>
                                               
                                            </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" HeaderStyle-ForeColor="#04522c" ControlStyle-Width="50px">
                                                <ItemTemplate>
                                                    <asp:ImageButton runat="server" ToolTip="Edit" Width="20px" ImageUrl="images/edit_icon.jpg" ID="imgAction" OnClick="Edit" CommandArgument='<%# Bind("location_id") %>' />                                                
                                                </ItemTemplate> 
                                            </asp:TemplateField>
                                        </Columns>
                                        
                                    </asp:GridView>

				</div>
                </div>
            </div>
        </div>
				</div>
                
				<div class="col-md-4">
				<div class="panel-group tool-tips widget-shadow">
				<div class="col-md-12 nopad">
                     
            <asp:Button runat="server" CssClass="btn-add" ID="btnAdd" Text="Add" OnClick="btnAdd_Click"/>
   	</div>
   	<hr/>
        <asp:Panel runat="server" ID="viewpanel" Visible="true">
   	<div class="col-md-12 nopad">
   	<div class="row-info mn_line_he"> 
           
											<div class="col-md-4 nopad"><p>Region : </p></div> 
											<div class="col-md-8 nopad"><asp:Label ID="lblregion" runat="server"></asp:Label></div> 
										</div>
										<div class="clearfix"></div>
										<div class="row-info">
											<div class="col-md-4 nopad"><p> 	Country : </p></div> 
											<div class="col-md-8 nopad"><asp:Label ID="lblCountry" runat="server"></asp:Label></div> 
										</div>
										<div class="clearfix"></div>
										<div class="row-info">
											<div class="col-md-4 nopad"><p> 	Location : </p></div> 
											<div class="col-md-8 nopad"><asp:Label ID="lblLocation" runat="server"></asp:Label></div> 
										</div>
										
                    <div class="clearfix"></div>
										<div class="row-info">
											<div class="col-md-4 nopad"><p> No of Shift : </p></div> 
											<div class="col-md-8 nopad"><asp:Label ID="lblshift" runat="server"></asp:Label></div> </div> 
										</div>

            <div class="clearfix"></div>
             <div id="viewshift1" class="row-info" style="display:none;">
               <div class="col-md-7 nopad"><p>Shift I Start Time: <asp:Label ID="lblIstart" runat="server"></asp:Label></p></div> 
                <div class="col-md-5 nopad"><p>End Time: <asp:Label ID="lblIend" runat="server"></asp:Label></p></div> 

           </div> 
										
                 
                        <div class="clearfix"></div>
           <div id="viewshift2" class="row-info" style="display:none;"><div class="col-md-7 nopad"><p>Shift II Start Time: <asp:Label ID="lblIIstart" runat="server"></asp:Label></p></div> 
                <div class="col-md-5 nopad"><p>End Time: <asp:Label ID="lblIIend" runat="server"></asp:Label></p></div> 

           </div> 
										
                 
                        <div class="clearfix"></div>
           <div id="viewshift3" class="row-info" style="display:none;"><div class="col-md-7 nopad"><p>Shift III Start Time: <asp:Label ID="lblIIIstart" runat="server"></asp:Label></p></div> 
                <div class="col-md-5 nopad"><p>End Time: <asp:Label ID="lblIIIend" runat="server"></asp:Label></p></div> 

           </div> 
										
                 
                        <div class="clearfix"></div>
            
            </asp:Panel>
                    <asp:Panel runat="server" ID="editpanel" Visible="false">
   	<div class="col-md-12 nopad mn_mar_5">
   	<div class="row-info"> 
											<div class="col-md-4 nopad"><p>Region : </p></div> 
											<div class="col-md-8 nopad"><asp:TextBox style="width:100%;" runat="server" ID="txtRegion" class="form-control1 mn_inp control3"></asp:TextBox>
                                               
                                                <asp:Panel ID="pnlRegion" Style="display: none;" runat="server" class="form-control1 mn_inp control4 mn_margin_none">
       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:ListBox ID="ddlRegion" runat="server" class="form-control1 mn_inp control4" AutoPostBack="true" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged">
                        </asp:ListBox>
                    </ContentTemplate>
           <Triggers>
               <asp:AsyncPostBackTrigger ControlID="ddlRegion" EventName="SelectedIndexChanged" />
           </Triggers>
                    </asp:UpdatePanel>
                    </asp:Panel>
                                                <%--<asp:DropDownList class="form-control1 mn_inp control4" runat="server" ID="ddlRegion"></asp:DropDownList>											--%>

											</div> 
										</div>
           <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" PopupControlID="pnlRegion" TargetControlID="txtRegion"
            Position="Bottom" runat="server">
        </ajaxToolkit:PopupControlExtender>
										<div class="clearfix"></div>
										<div class="row-info">
											<div class="col-md-4 nopad"><p> 	Country : </p></div> 
											<div class="col-md-8 nopad"><asp:TextBox style="width:100%;" runat="server" ID="txtCountry" class="form-control1 mn_inp control3"></asp:TextBox>
                                               
                                                <asp:Panel ID="pnlCountry" Style="display: none;" runat="server" class="form-control1 mn_inp control4 mn_margin_none">
       <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:ListBox ID="ddlCountry" runat="server" class="form-control1 mn_inp control4" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                        </asp:ListBox>
                    </ContentTemplate>
            <Triggers>
               <asp:AsyncPostBackTrigger ControlID="ddlCountry" EventName="SelectedIndexChanged" />
           </Triggers>
                    </asp:UpdatePanel>
                    </asp:Panel>
											</div> 
										</div>
           <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2" PopupControlID="pnlCountry" TargetControlID="txtCountry"
            Position="Bottom" runat="server">
        </ajaxToolkit:PopupControlExtender>
										<div class="clearfix"></div>
										<div class="row-info">
											<div class="col-md-4 nopad"><p> 	Location : </p></div> 
											<div class="col-md-8 nopad"><asp:TextBox style="width:100%;" runat="server" ID="txtLocation" class="form-control1 mn_inp control3"></asp:TextBox>
                                               
                                               <%-- <asp:Panel ID="pnlLocation" Style="display: none;" runat="server" class="form-control1 mn_inp control4 mn_margin_none">
       <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:ListBox ID="ddlLocation" runat="server" class="form-control1 mn_inp control4" AutoPostBack="true" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged">
                        </asp:ListBox>
                    </ContentTemplate>
                    </asp:UpdatePanel>
                    </asp:Panel>--%>
											</div> 
										</div>
<%--           <ajaxToolkit:PopupControlExtender ID="PopupControlExtender3" PopupControlID="pnlLocation" TargetControlID="txtLocation"
            Position="Bottom" runat="server">
        </ajaxToolkit:PopupControlExtender>--%>
										<div class="clearfix"></div>
										
										<div class="row-info">
											<div class="col-md-4 nopad"><p> No of Shift : </p></div> 
											<div class="col-md-8 nopad">
                                                <asp:DropdownList class="form-control1 mn_inp control3" runat="server" ID="txtNoOfShift" onchange="showEditShiftTime();">
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                </asp:DropdownList>
											</div> </div> 
										
                 
                        <div class="clearfix"></div>
           <div class="row-info">
											<div class="col-md-4 nopad"><p> Time Zone : </p></div> 
											<div class="col-md-8 nopad">
                                                <asp:DropdownList class="form-control1 mn_inp control3" runat="server" ID="ddlTimeZone">
                                                </asp:DropdownList>
											</div> </div> 
										
                 
                        <div class="clearfix"></div>
           <div id="editshift1" class="row-info">
               <div class="col-md-7 nopad"><p>Shift I Start Time: <asp:TextBox style="width:75px; height:35px;" runat="server" ID="txtIstart" onchange="validateHhMm(this);"></asp:TextBox></p></div> 
                <div class="col-md-5 nopad"><p>End Time: <asp:TextBox runat="server" style="width:75px; height:35px;" ID="txtIend" onchange="validateHhMm(this);"></asp:TextBox></p></div> 

           </div> 
										
                 
                        <div class="clearfix"></div>
           <div id="editshift2" class="row-info" style="display:none;"><div class="col-md-7 nopad"><p>Shift II Start Time: <asp:TextBox style="width:75px;  height:35px;" onchange="validateHhMm(this);" runat="server" ID="txtIIstart"></asp:TextBox></p></div> 
                <div class="col-md-5 nopad"><p>End Time: <asp:TextBox runat="server" style="width:75px; height:35px;" ID="txtIIend" onchange="validateHhMm(this);"></asp:TextBox></p></div> 

           </div> 
										
                 
                        <div class="clearfix"></div>
           <div id="editshift3" class="row-info" style="display:none"><div class="col-md-7 nopad"><p>Shift III Start Time: <asp:TextBox  style="width:75px; height:35px;" runat="server" ID="txtIIIstart" onchange="validateHhMm(this);"></asp:TextBox></p></div> 
                <div class="col-md-5 nopad"><p>End Time: <asp:TextBox runat="server" ID="txtIIIend" style="width:75px; height:35px;" onchange="validateHhMm(this);"></asp:TextBox></p></div> 

           </div> 
										
                 
                        <div class="clearfix"></div>
           <div> * Shift Time format should be 24 hours. </div>
           </div>
                        <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
										<asp:Button runat="server" ID="btnSave" CssClass="btn-add" Text="Save" OnClick="btnSave_Click" OnClientClick="return validateControls();"/>
            </asp:Panel>
										
   		
   	</div>
				</div>
				</div>
				</div>
				<div class="clearfix"> </div>
            
			</div>
			<div class="clearfix"> </div>
		</div>
</asp:Content>

