﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using MHLPA_BusinessLogic;
using MHLPA_BusinessObject;


namespace MHLPA_Application
{
    public partial class ComparisonReport : System.Web.UI.Page
    {

        #region Global Declaration
        CommonFunctions objCom = new CommonFunctions();
        DataSet dsDropDownData;
        UsersBO objUserBO;
        DataTable dtAnswer = new DataTable();
        CommonBL objComBL;
        DataTable dtReport1;
        DataTable dtReport2;
        DataTable dtReport3;
        DashboardBO objDashBO;
        DashboardBL objDashBL;
        private readonly Random random = new Random(1234);
        #endregion

        #region Events
        protected override void InitializeCulture()
        {
            string language = Convert.ToString(Session["language"]);
            
            //Set the Culture.
            Thread.CurrentThread.CurrentCulture = new CultureInfo(language);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                try
                {
                    objDashBO = new DashboardBO();
                    LoadFilterDropDowns(objDashBO);
                    txtDate.Text = String.Format("{0:dd-MM-yyyy}", DateTime.Now);
                    btnFilter_Click(null, null);
                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
            }
        }

        protected void ddlRegion1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion1.SelectedValue);
                //objDashBO.country_id = Convert.ToInt32(ddlCountry1.SelectedValue);
                //objDashBO.location_id = Convert.ToInt32(ddlLocation1.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLine1.SelectedValue);
                objDashBO.selection_flag = "Region";

                BindFilters1(objDashBO);
                ddlRegion1.SelectedValue = Convert.ToString(objDashBO.region_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCountry1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            int region;
            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion1.SelectedValue);
                
                objDashBO.country_id = Convert.ToInt32(ddlCountry1.SelectedValue);
                if (objDashBO.country_id == 0)
                {
                    objDashBO.region_id = Convert.ToInt32(ddlRegion1.SelectedValue);
                }
                //objDashBO.location_id = Convert.ToInt32(ddlLocation1.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLine1.SelectedValue);
                objDashBO.selection_flag = "Country";
                BindFilters1(objDashBO);
                //if (ddlRegion1.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                ////if (region != 0)
                //{
                //   ddlRegion1.SelectedValue = Convert.ToString(region);
                //}
                //else
                //{
                //    ddlRegion1.SelectedIndex = 1;
                //}
                ddlCountry1.SelectedValue = Convert.ToString(objDashBO.country_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLocation1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            int region, country;
            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion1.SelectedValue);
                country = Convert.ToInt32(ddlCountry1.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation1.SelectedValue);
                if (objDashBO.location_id == 0)
                {
                    objDashBO.country_id = country;
                    objDashBO.region_id = region;
                }
                //objDashBO.line_id = Convert.ToInt32(ddlLine1.SelectedValue);
                objDashBO.selection_flag = "Location";
                BindFilters1(objDashBO);
                //if (ddlCountry1.Items.FindByValue(Convert.ToString(country)) != null && country!=0)
                //{
                //    ddlCountry1.SelectedValue = Convert.ToString(country);
                //}
                //else
                //{
                //    ddlCountry1.SelectedIndex = 1;
                //}
                //if (ddlRegion1.Items.FindByValue(Convert.ToString(region)) != null && region!=0)
                //{
                //    ddlRegion1.SelectedValue = Convert.ToString(region);
                //}
                //else
                //{
                //    ddlRegion1.SelectedIndex = 1;
                //}
                ddlLocation1.SelectedValue = Convert.ToString(objDashBO.location_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLine1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
               
                objDashBO.line_id = Convert.ToInt32(ddlLine1.SelectedValue);
                if (objDashBO.line_id == 0)
                {
                    objDashBO.region_id = Convert.ToInt32(ddlRegion1.SelectedValue);
                    objDashBO.country_id = Convert.ToInt32(ddlCountry1.SelectedValue);
                    objDashBO.location_id = Convert.ToInt32(ddlLocation1.SelectedValue);
                }
                objDashBO.selection_flag = "Line";
                BindFilters1(objDashBO);
                //ddlCountry1.SelectedValue = Convert.ToString(objDashBO.country_id);
                //ddlLocation1.SelectedValue = Convert.ToString(objDashBO.location_id);
                ddlLine1.SelectedValue = Convert.ToString(objDashBO.line_id);
                ddlRegion1.SelectedIndex = 1;
                ddlCountry1.SelectedIndex = 1;
                ddlLocation1.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlRegion2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion2.SelectedValue);
                //objDashBO.country_id = Convert.ToInt32(ddlCountry2.SelectedValue);
                //objDashBO.location_id = Convert.ToInt32(ddlLocation2.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLine2.SelectedValue);
                objDashBO.selection_flag = "Region";
                BindFilters2(objDashBO);
                ddlRegion2.SelectedValue = Convert.ToString(objDashBO.region_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCountry2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            int region;
            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion2.SelectedValue);

                objDashBO.country_id = Convert.ToInt32(ddlCountry2.SelectedValue);
                if (objDashBO.country_id == 0)
                {
                    objDashBO.region_id = Convert.ToInt32(ddlRegion2.SelectedValue);
                }

                //objDashBO.region_id = Convert.ToInt32(ddlRegion2.SelectedValue);

                //objDashBO.country_id = Convert.ToInt32(ddlCountry2.SelectedValue);
                //objDashBO.location_id = Convert.ToInt32(ddlLocation2.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLine2.SelectedValue);
                objDashBO.selection_flag = "Country";
                BindFilters2(objDashBO);
                //if (ddlRegion2.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                ////if (region != 0)
                //{
                //    ddlRegion2.SelectedValue = Convert.ToString(region);
                //}
                //else
                //{
                //    ddlRegion2.SelectedIndex = 1;
                //}
                ddlCountry2.SelectedValue = Convert.ToString(objDashBO.country_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLocation2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            int region, country;
            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion2.SelectedValue);
                country = Convert.ToInt32(ddlCountry2.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation2.SelectedValue);
                if (objDashBO.location_id == 0)
                {
                    objDashBO.country_id = country;
                    objDashBO.region_id = region;
                }

                //objDashBO.region_id = Convert.ToInt32(ddlRegion2.SelectedValue);
                //objDashBO.country_id = Convert.ToInt32(ddlCountry2.SelectedValue);
                //objDashBO.location_id = Convert.ToInt32(ddlLocation2.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLine2.SelectedValue);
                objDashBO.selection_flag = "Location";
                BindFilters2(objDashBO);
                //if (ddlCountry2.Items.FindByValue(Convert.ToString(country)) != null && country != 0)
                //{
                //    ddlCountry2.SelectedValue = Convert.ToString(country);
                //}
                //else
                //{
                //    ddlCountry2.SelectedIndex = 1;
                //}
                //if (ddlRegion2.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                //{
                //    ddlRegion2.SelectedValue = Convert.ToString(region);
                //}
                //else
                //{
                //    ddlRegion2.SelectedIndex = 1;
                //}
                ddlLocation2.SelectedValue = Convert.ToString(objDashBO.location_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLine2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.line_id = Convert.ToInt32(ddlLine2.SelectedValue);
                if (objDashBO.line_id == 0)
                {
                    objDashBO.region_id = Convert.ToInt32(ddlRegion2.SelectedValue);
                    objDashBO.country_id = Convert.ToInt32(ddlCountry2.SelectedValue);
                    objDashBO.location_id = Convert.ToInt32(ddlLocation2.SelectedValue);
                }
                
                objDashBO.selection_flag = "Line";
                BindFilters2(objDashBO);
                //ddlCountry2.SelectedValue = Convert.ToString(objDashBO.country_id);
                //ddlLocation2.SelectedValue = Convert.ToString(objDashBO.location_id);
                ddlLine2.SelectedValue = Convert.ToString(objDashBO.line_id);
                ddlRegion2.SelectedIndex = 1;
                ddlCountry2.SelectedIndex = 1;
                ddlLocation2.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlRegion3_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion3.SelectedValue);
                //objDashBO.country_id = Convert.ToInt32(ddlCountry3.SelectedValue);
                //objDashBO.location_id = Convert.ToInt32(ddlLocation3.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLine3.SelectedValue);
                objDashBO.selection_flag = "Region";

                BindFilters3(objDashBO);
                ddlRegion3.SelectedValue = Convert.ToString(objDashBO.region_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCountry3_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            int region;
            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion3.SelectedValue);

                objDashBO.country_id = Convert.ToInt32(ddlCountry3.SelectedValue);
                if (objDashBO.country_id == 0)
                {
                    objDashBO.region_id = Convert.ToInt32(ddlRegion3.SelectedValue);
                }
                //objDashBO.region_id = Convert.ToInt32(ddlRegion3.SelectedValue);
                //objDashBO.country_id = Convert.ToInt32(ddlCountry3.SelectedValue);
                //objDashBO.location_id = Convert.ToInt32(ddlLocation3.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLine3.SelectedValue);
                objDashBO.selection_flag = "Country";
                BindFilters3(objDashBO);
                //if (ddlRegion3.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                ////if (region != 0)
                //{
                //    ddlRegion3.SelectedValue = Convert.ToString(region);
                //}
                //else
                //{
                //    ddlRegion3.SelectedIndex = 1;
                //}
                ddlCountry3.SelectedValue = Convert.ToString(objDashBO.country_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLocation3_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            int region, country;
            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion3.SelectedValue);
                country = Convert.ToInt32(ddlCountry3.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation3.SelectedValue);
                if (objDashBO.location_id == 0)
                {
                    objDashBO.country_id = country;
                    objDashBO.region_id = region;
                }
                //objDashBO.region_id = Convert.ToInt32(ddlRegion3.SelectedValue);
                //objDashBO.country_id = Convert.ToInt32(ddlCountry3.SelectedValue);
                //objDashBO.location_id = Convert.ToInt32(ddlLocation3.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLine3.SelectedValue);
                objDashBO.selection_flag = "Location";
                BindFilters3(objDashBO);
                //if (ddlCountry3.Items.FindByValue(Convert.ToString(country)) != null && country != 0)
                //{
                //    ddlCountry3.SelectedValue = Convert.ToString(country);
                //}
                //else
                //{
                //    ddlCountry3.SelectedIndex = 1;
                //}
                //if (ddlRegion3.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                //{
                //    ddlRegion3.SelectedValue = Convert.ToString(region);
                //}
                //else
                //{
                //    ddlRegion3.SelectedIndex = 1;
                //}
                ddlLocation3.SelectedValue = Convert.ToString(objDashBO.location_id);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLine3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.line_id = Convert.ToInt32(ddlLine3.SelectedValue);
                if (objDashBO.line_id == 0)
                {
                    objDashBO.region_id = Convert.ToInt32(ddlRegion3.SelectedValue);
                    objDashBO.country_id = Convert.ToInt32(ddlCountry3.SelectedValue);
                    objDashBO.location_id = Convert.ToInt32(ddlLocation3.SelectedValue);
                }
                objDashBO.selection_flag = "Line";
                BindFilters3(objDashBO);
                //ddlCountry3.SelectedValue = Convert.ToString(objDashBO.country_id);
                //ddlLocation3.SelectedValue = Convert.ToString(objDashBO.location_id);
                ddlLine3.SelectedValue = Convert.ToString(objDashBO.line_id);
                ddlRegion3.SelectedIndex = 1;
                ddlCountry3.SelectedIndex = 1;
                ddlLocation3.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                string legend_name = string.Empty;
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();
                dtReport1 = new DataTable();
                dtReport2 = new DataTable();
                dtReport3 = new DataTable();
                Session["location1"] = Convert.ToInt32(ddlLocation1.SelectedValue);
                Session["region1"] = Convert.ToInt32(ddlRegion1.SelectedValue);
                Session["country1"] = Convert.ToInt32(ddlCountry1.SelectedValue);
                Session["line1"] = Convert.ToInt32(ddlLine1.SelectedValue);

                Session["location2"] = Convert.ToInt32(ddlLocation2.SelectedValue);
                Session["region2"] = Convert.ToInt32(ddlRegion2.SelectedValue);
                Session["country2"] = Convert.ToInt32(ddlCountry2.SelectedValue);
                Session["line2"] = Convert.ToInt32(ddlLine2.SelectedValue);

                Session["location3"] = Convert.ToInt32(ddlLocation3.SelectedValue);
                Session["region3"] = Convert.ToInt32(ddlRegion3.SelectedValue);
                Session["country3"] = Convert.ToInt32(ddlCountry3.SelectedValue);
                Session["line3"] = Convert.ToInt32(ddlLine3.SelectedValue);

                Session["date"] = Convert.ToString(txtDate.Text);
                LoadSecLabels();
                Page.ClientScript.RegisterStartupScript(this.GetType(), "LoadChart", "LoadCharts()", true);

               
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                LoadFilterDropDowns(objDashBO);
                txtDate.Text = String.Format("{0:dd-MM-yyyy}", DateTime.Now);
                pnlReport.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods
        private void LoadFilterDropDowns(DashboardBO objDashBO)
        {
            try
            {
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion1.DataSource = dsDropDownData.Tables[0];
                        ddlRegion1.DataTextField = "region_name";
                        ddlRegion1.DataValueField = "region_id";
                        ddlRegion1.DataBind();
                        ddlRegion1.Items.Insert(0, new ListItem("All", "0"));
                        //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                            ddlRegion1.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                        //    ddlRegion1.Attributes["disabled"] = "disabled";
                        //}
                        //else
                        //    ddlRegion1.Enabled = true;

                        ddlRegion2.DataSource = dsDropDownData.Tables[0];
                        ddlRegion2.DataTextField = "region_name";
                        ddlRegion2.DataValueField = "region_id";
                        ddlRegion2.DataBind();
                        ddlRegion2.Items.Insert(0, new ListItem("All", "0"));
                        //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                            ddlRegion2.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                        //    ddlRegion2.Attributes["disabled"] = "disabled";
                        //}
                        //else
                        //    ddlRegion2.Enabled = true;
                        ddlRegion3.DataSource = dsDropDownData.Tables[0];
                        ddlRegion3.DataTextField = "region_name";
                        ddlRegion3.DataValueField = "region_id";
                        ddlRegion3.DataBind();
                        ddlRegion3.Items.Insert(0, new ListItem("All", "0"));
                        //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                            ddlRegion3.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                        //    ddlRegion3.Attributes["disabled"] = "disabled";
                        //}
                        //else
                        //    ddlRegion3.Enabled = true;
                    }
                    else
                    {
                        ddlRegion1.Items.Clear();
                        ddlRegion1.DataSource = null;
                        ddlRegion1.DataBind();
                        ddlRegion2.Items.Clear();
                        ddlRegion2.DataSource = null;
                        ddlRegion2.DataBind();
                        ddlRegion3.Items.Clear();
                        ddlRegion3.DataSource = null;
                        ddlRegion3.DataBind();
                    }
                    //if (dsDropDownData.Tables[1].Rows.Count > 0)
                    //{
                    //    ddlCountry1.DataSource = dsDropDownData.Tables[1];
                    //    ddlCountry1.DataTextField = "country_name";
                    //    ddlCountry1.DataValueField = "country_id";
                    //    ddlCountry1.DataBind();
                    //    ddlCountry1.Items.Insert(0, new ListItem("All", "0"));
                    //    //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                    //    //{
                    //        ddlCountry1.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                    //    //    ddlCountry1.Attributes["disabled"] = "disabled";
                    //    //}
                    //    //else
                    //    //    ddlCountry1.Enabled = true;
                    //    ddlCountry2.DataSource = dsDropDownData.Tables[1];
                    //    ddlCountry2.DataTextField = "country_name";
                    //    ddlCountry2.DataValueField = "country_id";
                    //    ddlCountry2.DataBind();
                    //    ddlCountry2.Items.Insert(0, new ListItem("All", "0"));
                    //    //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                    //    //{
                    //        ddlCountry2.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                    //    //    ddlCountry2.Attributes["disabled"] = "disabled";
                    //    //}
                    //    //else
                    //    //    ddlCountry2.Enabled = true;
                    //    ddlCountry3.DataSource = dsDropDownData.Tables[1];
                    //    ddlCountry3.DataTextField = "country_name";
                    //    ddlCountry3.DataValueField = "country_id";
                    //    ddlCountry3.DataBind();
                    //    ddlCountry3.Items.Insert(0, new ListItem("All", "0"));
                    //    //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                    //    //{
                    //        ddlCountry3.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                    //    //    ddlCountry3.Attributes["disabled"] = "disabled";
                    //    //}
                    //    //else
                    //    //    ddlCountry3.Enabled = true;
                    //}
                    //else
                    //{
                    //    ddlCountry1.Items.Clear();
                    //    ddlCountry1.DataSource = null;
                    //    ddlCountry1.DataBind();
                    //    ddlCountry2.Items.Clear();
                    //    ddlCountry2.DataSource = null;
                    //    ddlCountry2.DataBind();
                    //    ddlCountry3.Items.Clear();
                    //    ddlCountry3.DataSource = null;
                    //    ddlCountry3.DataBind();
                    //}
                    //if (dsDropDownData.Tables[2].Rows.Count > 0)
                    //{
                    //    ddlLocation1.DataSource = dsDropDownData.Tables[2];
                    //    ddlLocation1.DataTextField = "location_name";
                    //    ddlLocation1.DataValueField = "location_id";
                    //    ddlLocation1.DataBind();
                    //    ddlLocation1.Items.Insert(0, new ListItem("All", "0"));
                    //    //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                    //    //{
                    //        ddlLocation1.SelectedValue = Convert.ToString(Session["LocationId"]);
                    //    //    ddlLocation1.Attributes["disabled"] = "disabled";
                    //    //}
                    //    //else
                    //    //    ddlLocation1.Enabled = true;
                    //    ddlLocation2.DataSource = dsDropDownData.Tables[2];
                    //    ddlLocation2.DataTextField = "location_name";
                    //    ddlLocation2.DataValueField = "location_id";
                    //    ddlLocation2.DataBind();
                    //    ddlLocation2.Items.Insert(0, new ListItem("All", "0"));
                    //    //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                    //    //{
                    //        ddlLocation2.SelectedValue = Convert.ToString(Session["LocationId"]);
                    //    //    ddlLocation2.Attributes["disabled"] = "disabled";
                    //    //}
                    //    //else
                    //    //    ddlLocation2.Enabled = true;
                    //    ddlLocation3.DataSource = dsDropDownData.Tables[2];
                    //    ddlLocation3.DataTextField = "location_name";
                    //    ddlLocation3.DataValueField = "location_id";
                    //    ddlLocation3.DataBind();
                    //    ddlLocation3.Items.Insert(0, new ListItem("All", "0"));
                    //    //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                    //    //{
                    //        ddlLocation3.SelectedValue = Convert.ToString(Session["LocationId"]);
                    //    //    ddlLocation3.Attributes["disabled"] = "disabled";
                    //    //}
                    //    //else
                    //    //    ddlLocation3.Enabled = true;
                    //}
                    //else
                    //{
                    //    ddlLocation1.Items.Clear();
                    //    ddlLocation1.DataSource = null;
                    //    ddlLocation1.DataBind();
                    //    ddlLocation2.Items.Clear();
                    //    ddlLocation2.DataSource = null;
                    //    ddlLocation2.DataBind();
                    //    ddlLocation3.Items.Clear();
                    //    ddlLocation3.DataSource = null;
                    //    ddlLocation3.DataBind();
                    //}
                    //if (dsDropDownData.Tables[3].Rows.Count > 0)
                    //{
                    //    ddlLine1.DataSource = dsDropDownData.Tables[3];
                    //    ddlLine1.DataTextField = "line_name";
                    //    ddlLine1.DataValueField = "line_id";
                    //    ddlLine1.DataBind();
                    //    ddlLine1.Items.Insert(0, new ListItem("All", "0"));
                    //    //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                    //    //{
                    //      ///  ddlLocation1_SelectedIndexChanged(null, null);
                    //    //}
                    //    ddlLine2.DataSource = dsDropDownData.Tables[3];
                    //    ddlLine2.DataTextField = "line_name";
                    //    ddlLine2.DataValueField = "line_id";
                    //    ddlLine2.DataBind();
                    //    ddlLine2.Items.Insert(0, new ListItem("All", "0"));
                    //    //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                    //    //{
                    //       // ddlLocation2_SelectedIndexChanged(null, null);
                    //    //}
                    //    ddlLine3.DataSource = dsDropDownData.Tables[3];
                    //    ddlLine3.DataTextField = "line_name";
                    //    ddlLine3.DataValueField = "line_id";
                    //    ddlLine3.DataBind();
                    //    ddlLine3.Items.Insert(0, new ListItem("All", "0"));
                    //    //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                    //    //{
                    //       // ddlLocation3_SelectedIndexChanged(null, null);
                    //   // }
                    //}
                    //else
                    //{
                    //    ddlLine1.Items.Clear();
                    //    ddlLine1.DataSource = null;
                    //    ddlLine1.DataBind();
                    //    ddlLine2.Items.Clear();
                    //    ddlLine2.DataSource = null;
                    //    ddlLine2.DataBind();
                    //    ddlLine3.Items.Clear();
                    //    ddlLine3.DataSource = null;
                    //    ddlLine3.DataBind();
                    //}
                }
                objDashBO.region_id = Convert.ToInt32(ddlRegion1.SelectedValue);
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry1.DataSource = dsDropDownData.Tables[1];
                        ddlCountry1.DataTextField = "country_name";
                        ddlCountry1.DataValueField = "country_id";
                        ddlCountry1.DataBind();
                        ddlCountry1.Items.Insert(0, new ListItem("All", "0"));
                        //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                        ddlCountry1.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                        //    ddlCountry1.Attributes["disabled"] = "disabled";
                        //}
                        //else
                        //    ddlCountry1.Enabled = true;
                        ddlCountry2.DataSource = dsDropDownData.Tables[1];
                        ddlCountry2.DataTextField = "country_name";
                        ddlCountry2.DataValueField = "country_id";
                        ddlCountry2.DataBind();
                        ddlCountry2.Items.Insert(0, new ListItem("All", "0"));
                        //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                        ddlCountry2.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                        //    ddlCountry2.Attributes["disabled"] = "disabled";
                        //}
                        //else
                        //    ddlCountry2.Enabled = true;
                        ddlCountry3.DataSource = dsDropDownData.Tables[1];
                        ddlCountry3.DataTextField = "country_name";
                        ddlCountry3.DataValueField = "country_id";
                        ddlCountry3.DataBind();
                        ddlCountry3.Items.Insert(0, new ListItem("All", "0"));
                        //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                        ddlCountry3.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                        //    ddlCountry3.Attributes["disabled"] = "disabled";
                        //}
                        //else
                        //    ddlCountry3.Enabled = true;
                    }
                    else
                    {
                        ddlCountry1.Items.Clear();
                        ddlCountry1.DataSource = null;
                        ddlCountry1.DataBind();
                        ddlCountry2.Items.Clear();
                        ddlCountry2.DataSource = null;
                        ddlCountry2.DataBind();
                        ddlCountry3.Items.Clear();
                        ddlCountry3.DataSource = null;
                        ddlCountry3.DataBind();
                    }
                }
                objDashBO.country_id = Convert.ToInt32(ddlCountry1.SelectedValue);
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation1.DataSource = dsDropDownData.Tables[2];
                        ddlLocation1.DataTextField = "location_name";
                        ddlLocation1.DataValueField = "location_id";
                        ddlLocation1.DataBind();
                        ddlLocation1.Items.Insert(0, new ListItem("All", "0"));
                        //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                        ddlLocation1.SelectedValue = Convert.ToString(Session["LocationId"]);
                        //    ddlLocation1.Attributes["disabled"] = "disabled";
                        //}
                        //else
                        //    ddlLocation1.Enabled = true;
                        ddlLocation2.DataSource = dsDropDownData.Tables[2];
                        ddlLocation2.DataTextField = "location_name";
                        ddlLocation2.DataValueField = "location_id";
                        ddlLocation2.DataBind();
                        ddlLocation2.Items.Insert(0, new ListItem("All", "0"));
                        //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                        ddlLocation2.SelectedValue = Convert.ToString(Session["LocationId"]);
                        //    ddlLocation2.Attributes["disabled"] = "disabled";
                        //}
                        //else
                        //    ddlLocation2.Enabled = true;
                        ddlLocation3.DataSource = dsDropDownData.Tables[2];
                        ddlLocation3.DataTextField = "location_name";
                        ddlLocation3.DataValueField = "location_id";
                        ddlLocation3.DataBind();
                        ddlLocation3.Items.Insert(0, new ListItem("All", "0"));
                        //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                        ddlLocation3.SelectedValue = Convert.ToString(Session["LocationId"]);
                        //    ddlLocation3.Attributes["disabled"] = "disabled";
                        //}
                        //else
                        //    ddlLocation3.Enabled = true;
                    }
                    else
                    {
                        ddlLocation1.Items.Clear();
                        ddlLocation1.DataSource = null;
                        ddlLocation1.DataBind();
                        ddlLocation2.Items.Clear();
                        ddlLocation2.DataSource = null;
                        ddlLocation2.DataBind();
                        ddlLocation3.Items.Clear();
                        ddlLocation3.DataSource = null;
                        ddlLocation3.DataBind();
                    }
                }

                objDashBO.location_id = Convert.ToInt32(ddlLocation1.SelectedValue);
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLine1.DataSource = dsDropDownData.Tables[3];
                        ddlLine1.DataTextField = "line_name";
                        ddlLine1.DataValueField = "line_id";
                        ddlLine1.DataBind();
                        ddlLine1.Items.Insert(0, new ListItem("All", "0"));
                        //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                        ///  ddlLocation1_SelectedIndexChanged(null, null);
                        //}
                        ddlLine2.DataSource = dsDropDownData.Tables[3];
                        ddlLine2.DataTextField = "line_name";
                        ddlLine2.DataValueField = "line_id";
                        ddlLine2.DataBind();
                        ddlLine2.Items.Insert(0, new ListItem("All", "0"));
                        //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                        // ddlLocation2_SelectedIndexChanged(null, null);
                        //}
                        ddlLine3.DataSource = dsDropDownData.Tables[3];
                        ddlLine3.DataTextField = "line_name";
                        ddlLine3.DataValueField = "line_id";
                        ddlLine3.DataBind();
                        ddlLine3.Items.Insert(0, new ListItem("All", "0"));
                        //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                        // ddlLocation3_SelectedIndexChanged(null, null);
                        // }
                    }
                    else
                    {
                        ddlLine1.Items.Clear();
                        ddlLine1.DataSource = null;
                        ddlLine1.DataBind();
                        ddlLine2.Items.Clear();
                        ddlLine2.DataSource = null;
                        ddlLine2.DataBind();
                        ddlLine3.Items.Clear();
                        ddlLine3.DataSource = null;
                        ddlLine3.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void BindFilters1(DashboardBO objDashBO)
        {

            DataSet dsDropDownDataCountry = new DataSet();
            DataSet dsDropDownDataLocation = new DataSet();
            DashboardBO objDashBOCountry = new DashboardBO();
            DashboardBO objDashBOlocation = new DashboardBO();
            int region;
            int country;
            try
            {
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (objDashBO.selection_flag == "Region")
                {
                    if (dsDropDownData.Tables.Count > 0)
                    {
                        
                        if (dsDropDownData.Tables[1].Rows.Count > 0)
                        {
                            ddlCountry1.DataSource = dsDropDownData.Tables[1];
                            ddlCountry1.DataTextField = "country_name";
                            ddlCountry1.DataValueField = "country_id";
                            ddlCountry1.DataBind();
                            ddlCountry1.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlCountry1.Items.Clear();
                            ddlCountry1.DataSource = null;
                            ddlCountry1.DataBind();
                        }
                        if (dsDropDownData.Tables[2].Rows.Count > 0)
                        {
                            ddlLocation1.DataSource = dsDropDownData.Tables[2];
                            ddlLocation1.DataTextField = "location_name";
                            ddlLocation1.DataValueField = "location_id";
                            ddlLocation1.DataBind();
                            ddlLocation1.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLocation1.Items.Clear();
                            ddlLocation1.DataSource = null;
                            ddlLocation1.DataBind();
                        }
                        if (dsDropDownData.Tables[3].Rows.Count > 0)
                        {
                            ddlLine1.DataSource = dsDropDownData.Tables[3];
                            ddlLine1.DataTextField = "line_name";
                            ddlLine1.DataValueField = "line_id";
                            ddlLine1.DataBind();
                            ddlLine1.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLine1.Items.Clear();
                            ddlLine1.DataSource = null;
                            ddlLine1.DataBind();
                        }
                    }
                }

                else if (objDashBO.selection_flag == "Country")
                {
                    
                    region = Convert.ToInt32(ddlRegion1.SelectedValue);
                    if (dsDropDownData.Tables.Count > 0)
                    {
                        if (dsDropDownData.Tables[0].Rows.Count > 0)
                        {
                            if (ddlRegion1.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                            //if (region != 0)
                            {
                                ddlRegion1.SelectedValue = Convert.ToString(region);
                            }
                            else
                            {
                                ddlRegion1.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                            }
                        }
                        else
                        {
                        }
                        if (dsDropDownData.Tables[1].Rows.Count > 0)
                        {
                            if (ddlRegion1.Items.FindByValue(Convert.ToString(region)) == null || region == 0)
                            {
                                objDashBOCountry.region_id = Convert.ToInt32(ddlRegion1.SelectedValue);
                                dsDropDownDataCountry = objDashBL.getFilterDataBL(objDashBOCountry);
                                if (dsDropDownDataCountry.Tables.Count > 0)
                                {
                                    if (dsDropDownDataCountry.Tables[1].Rows.Count > 0)
                                    {
                                        ddlCountry1.DataSource = dsDropDownDataCountry.Tables[1];
                                        ddlCountry1.DataTextField = "country_name";
                                        ddlCountry1.DataValueField = "country_id";
                                        ddlCountry1.DataBind();
                                        ddlCountry1.Items.Insert(0, new ListItem("All", "0"));
                                        ddlCountry1.SelectedValue = Convert.ToString(objDashBO.country_id);
                                    }
                                    else
                                    {
                                        ddlCountry1.Items.Clear();
                                        ddlCountry1.DataSource = null;
                                        ddlCountry1.DataBind();
                                    }
                                }
                            }
                            else
                            {
                                ddlCountry1.SelectedValue = Convert.ToString(objDashBO.country_id);
                            }
                        }
                        else
                        {
                            ddlCountry1.Items.Clear();
                            ddlCountry1.DataSource = null;
                            ddlCountry1.DataBind();
                        }
                        if (dsDropDownData.Tables[2].Rows.Count > 0)
                        {
                            ddlLocation1.DataSource = dsDropDownData.Tables[2];
                            ddlLocation1.DataTextField = "location_name";
                            ddlLocation1.DataValueField = "location_id";
                            ddlLocation1.DataBind();
                            ddlLocation1.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLocation1.Items.Clear();
                            ddlLocation1.DataSource = null;
                            ddlLocation1.DataBind();
                        }
                        if (dsDropDownData.Tables[3].Rows.Count > 0)
                        {
                            ddlLine1.DataSource = dsDropDownData.Tables[3];
                            ddlLine1.DataTextField = "line_name";
                            ddlLine1.DataValueField = "line_id";
                            ddlLine1.DataBind();
                            ddlLine1.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLine1.Items.Clear();
                            ddlLine1.DataSource = null;
                            ddlLine1.DataBind();
                        }
                    }
                }
                else if (objDashBO.selection_flag == "Location")
                {
                    region = Convert.ToInt32(ddlRegion1.SelectedValue);
                    country = Convert.ToInt32(ddlCountry1.SelectedValue);
                    if (dsDropDownData.Tables.Count > 0)
                    {
                        if (dsDropDownData.Tables[0].Rows.Count > 0)
                        {
                            if (ddlRegion1.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                            //if (region != 0)
                            {
                                ddlRegion1.SelectedValue = Convert.ToString(region);
                            }
                            else
                            {
                                ddlRegion1.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                            }
                        }
                        else
                        {
                        }
                        if (dsDropDownData.Tables[1].Rows.Count > 0)
                        {
                            objDashBOCountry.region_id = Convert.ToInt32(ddlRegion1.SelectedValue);
                            dsDropDownDataCountry = objDashBL.getFilterDataBL(objDashBOCountry);
                            if (dsDropDownDataCountry.Tables.Count > 0)
                            {
                                if (dsDropDownDataCountry.Tables[1].Rows.Count > 0)
                                {
                                    ddlCountry1.DataSource = dsDropDownDataCountry.Tables[1];
                                    ddlCountry1.DataTextField = "country_name";
                                    ddlCountry1.DataValueField = "country_id";
                                    ddlCountry1.DataBind();
                                    ddlCountry1.Items.Insert(0, new ListItem("All", "0"));
                                    if (ddlCountry1.Items.FindByValue(Convert.ToString(country)) != null && country != 0)
                                    {
                                        ddlCountry1.SelectedValue = Convert.ToString(country);
                                    }
                                    else
                                    {
                                        ddlCountry1.SelectedValue = Convert.ToString(dsDropDownData.Tables[1].Rows[0]["country_id"]); ;
                                    }
                                }
                                else
                                {
                                    ddlCountry1.Items.Clear();
                                    ddlCountry1.DataSource = null;
                                    ddlCountry1.DataBind();
                                }
                            }
                        }
                        else
                        {
                            ddlCountry1.Items.Clear();
                            ddlCountry1.DataSource = null;
                            ddlCountry1.DataBind();
                        }
                        if (dsDropDownData.Tables[2].Rows.Count > 0)
                        {
                            objDashBOlocation.country_id = Convert.ToInt32(ddlCountry1.SelectedValue);
                            dsDropDownDataLocation = objDashBL.getFilterDataBL(objDashBOlocation);
                            if (dsDropDownDataLocation.Tables.Count > 0)
                            {
                                if (dsDropDownDataLocation.Tables[2].Rows.Count > 0)
                                {
                                    ddlLocation1.DataSource = dsDropDownDataLocation.Tables[2];
                                    ddlLocation1.DataTextField = "location_name";
                                    ddlLocation1.DataValueField = "location_id";
                                    ddlLocation1.DataBind();
                                    ddlLocation1.Items.Insert(0, new ListItem("All", "0"));
                                    ddlLocation1.SelectedValue = Convert.ToString(objDashBO.location_id);
                                }
                                else
                                {
                                    ddlLocation1.Items.Clear();
                                    ddlLocation1.DataSource = null;
                                    ddlLocation1.DataBind();
                                }

                            }
                        }
                        else
                        {
                            ddlLocation1.Items.Clear();
                            ddlLocation1.DataSource = null;
                            ddlLocation1.DataBind();
                        }
                        if (dsDropDownData.Tables[3].Rows.Count > 0)
                        {
                            ddlLine1.DataSource = dsDropDownData.Tables[3];
                            ddlLine1.DataTextField = "line_name";
                            ddlLine1.DataValueField = "line_id";
                            ddlLine1.DataBind();
                            ddlLine1.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLine1.Items.Clear();
                            ddlLine1.DataSource = null;
                            ddlLine1.DataBind();
                        }
                    }
                }
                else
                {
                    if (dsDropDownData.Tables.Count > 0)
                    {
                        if (dsDropDownData.Tables[0].Rows.Count > 0)
                        {
                            ddlRegion1.DataSource = dsDropDownData.Tables[0];
                            ddlRegion1.DataTextField = "region_name";
                            ddlRegion1.DataValueField = "region_id";
                            ddlRegion1.DataBind();
                            ddlRegion1.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlRegion1.Items.Clear();
                            ddlRegion1.DataSource = null;
                            ddlRegion1.DataBind();
                        }
                        if (dsDropDownData.Tables[1].Rows.Count > 0)
                        {
                            ddlCountry1.DataSource = dsDropDownData.Tables[1];
                            ddlCountry1.DataTextField = "country_name";
                            ddlCountry1.DataValueField = "country_id";
                            ddlCountry1.DataBind();
                            ddlCountry1.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlCountry1.Items.Clear();
                            ddlCountry1.DataSource = null;
                            ddlCountry1.DataBind();
                        }
                        if (dsDropDownData.Tables[2].Rows.Count > 0)
                        {
                            ddlLocation1.DataSource = dsDropDownData.Tables[2];
                            ddlLocation1.DataTextField = "location_name";
                            ddlLocation1.DataValueField = "location_id";
                            ddlLocation1.DataBind();
                            ddlLocation1.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLocation1.Items.Clear();
                            ddlLocation1.DataSource = null;
                            ddlLocation1.DataBind();
                        }
                        if (dsDropDownData.Tables[3].Rows.Count > 0)
                        {
                            ddlLine1.DataSource = dsDropDownData.Tables[3];
                            ddlLine1.DataTextField = "line_name";
                            ddlLine1.DataValueField = "line_id";
                            ddlLine1.DataBind();
                            ddlLine1.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLine1.Items.Clear();
                            ddlLine1.DataSource = null;
                            ddlLine1.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void BindFilters2(DashboardBO objDashBO)
        {
           
            DataSet dsDropDownDataCountry = new DataSet();
            DataSet dsDropDownDataLocation = new DataSet();
            DashboardBO objDashBOCountry = new DashboardBO();
            DashboardBO objDashBOlocation = new DashboardBO();
            int region;
            int country;
            try
            {
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (objDashBO.selection_flag == "Region")
                {
                    if (dsDropDownData.Tables.Count > 0)
                    {
                        
                        if (dsDropDownData.Tables[1].Rows.Count > 0)
                        {
                            ddlCountry2.DataSource = dsDropDownData.Tables[1];
                            ddlCountry2.DataTextField = "country_name";
                            ddlCountry2.DataValueField = "country_id";
                            ddlCountry2.DataBind();
                            ddlCountry2.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlCountry2.Items.Clear();
                            ddlCountry2.DataSource = null;
                            ddlCountry2.DataBind();
                        }
                        if (dsDropDownData.Tables[2].Rows.Count > 0)
                        {
                            ddlLocation2.DataSource = dsDropDownData.Tables[2];
                            ddlLocation2.DataTextField = "location_name";
                            ddlLocation2.DataValueField = "location_id";
                            ddlLocation2.DataBind();
                            ddlLocation2.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLocation2.Items.Clear();
                            ddlLocation2.DataSource = null;
                            ddlLocation2.DataBind();
                        }
                        if (dsDropDownData.Tables[3].Rows.Count > 0)
                        {
                            ddlLine2.DataSource = dsDropDownData.Tables[3];
                            ddlLine2.DataTextField = "line_name";
                            ddlLine2.DataValueField = "line_id";
                            ddlLine2.DataBind();
                            ddlLine2.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLine2.Items.Clear();
                            ddlLine2.DataSource = null;
                            ddlLine2.DataBind();
                        }
                    }
                }

                else if (objDashBO.selection_flag == "Country")
                {
                    
                    region = Convert.ToInt32(ddlRegion2.SelectedValue);
                    if (dsDropDownData.Tables.Count > 0)
                    {
                        if (dsDropDownData.Tables[0].Rows.Count > 0)
                        {
                            if (ddlRegion2.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                            //if (region != 0)
                            {
                                ddlRegion2.SelectedValue = Convert.ToString(region);
                            }
                            else
                            {
                                ddlRegion2.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                            }
                        }
                        else
                        {
                        }
                        if (dsDropDownData.Tables[1].Rows.Count > 0)
                        {
                            if (ddlRegion2.Items.FindByValue(Convert.ToString(region)) == null || region == 0)
                            {
                                objDashBOCountry.region_id = Convert.ToInt32(ddlRegion2.SelectedValue);
                                dsDropDownDataCountry = objDashBL.getFilterDataBL(objDashBOCountry);
                                if (dsDropDownDataCountry.Tables.Count > 0)
                                {
                                    if (dsDropDownDataCountry.Tables[1].Rows.Count > 0)
                                    {
                                        ddlCountry2.DataSource = dsDropDownDataCountry.Tables[1];
                                        ddlCountry2.DataTextField = "country_name";
                                        ddlCountry2.DataValueField = "country_id";
                                        ddlCountry2.DataBind();
                                        ddlCountry2.Items.Insert(0, new ListItem("All", "0"));
                                        ddlCountry2.SelectedValue = Convert.ToString(objDashBO.country_id);
                                    }
                                    else
                                    {
                                        ddlCountry2.Items.Clear();
                                        ddlCountry2.DataSource = null;
                                        ddlCountry2.DataBind();
                                    }
                                }
                            }
                            else
                            {
                                ddlCountry2.SelectedValue = Convert.ToString(objDashBO.country_id);
                            }
                        }
                        else
                        {
                            ddlCountry2.Items.Clear();
                            ddlCountry2.DataSource = null;
                            ddlCountry2.DataBind();
                        }
                        if (dsDropDownData.Tables[2].Rows.Count > 0)
                        {
                            ddlLocation2.DataSource = dsDropDownData.Tables[2];
                            ddlLocation2.DataTextField = "location_name";
                            ddlLocation2.DataValueField = "location_id";
                            ddlLocation2.DataBind();
                            ddlLocation2.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLocation2.Items.Clear();
                            ddlLocation2.DataSource = null;
                            ddlLocation2.DataBind();
                        }
                        if (dsDropDownData.Tables[3].Rows.Count > 0)
                        {
                            ddlLine2.DataSource = dsDropDownData.Tables[3];
                            ddlLine2.DataTextField = "line_name";
                            ddlLine2.DataValueField = "line_id";
                            ddlLine2.DataBind();
                            ddlLine2.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLine2.Items.Clear();
                            ddlLine2.DataSource = null;
                            ddlLine2.DataBind();
                        }
                    }
                }
                else if (objDashBO.selection_flag == "Location")
                {
                    region = Convert.ToInt32(ddlRegion2.SelectedValue);
                    country = Convert.ToInt32(ddlCountry2.SelectedValue);
                    if (dsDropDownData.Tables.Count > 0)
                    {
                        if (dsDropDownData.Tables[0].Rows.Count > 0)
                        {
                            if (ddlRegion2.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                            //if (region != 0)
                            {
                                ddlRegion2.SelectedValue = Convert.ToString(region);
                            }
                            else
                            {
                                ddlRegion2.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                            }
                        }
                        else
                        {
                        }
                        if (dsDropDownData.Tables[1].Rows.Count > 0)
                        {
                            objDashBOCountry.region_id = Convert.ToInt32(ddlRegion2.SelectedValue);
                            dsDropDownDataCountry = objDashBL.getFilterDataBL(objDashBOCountry);
                            if (dsDropDownDataCountry.Tables.Count > 0)
                            {
                                if (dsDropDownDataCountry.Tables[1].Rows.Count > 0)
                                {
                                    ddlCountry2.DataSource = dsDropDownDataCountry.Tables[1];
                                    ddlCountry2.DataTextField = "country_name";
                                    ddlCountry2.DataValueField = "country_id";
                                    ddlCountry2.DataBind();
                                    ddlCountry2.Items.Insert(0, new ListItem("All", "0"));
                                    if (ddlCountry2.Items.FindByValue(Convert.ToString(country)) != null && country != 0)
                                    {
                                        ddlCountry2.SelectedValue = Convert.ToString(country);
                                    }
                                    else
                                    {
                                        ddlCountry2.SelectedValue = Convert.ToString(dsDropDownData.Tables[1].Rows[0]["country_id"]); ;
                                    }
                                }
                                else
                                {
                                    ddlCountry2.Items.Clear();
                                    ddlCountry2.DataSource = null;
                                    ddlCountry2.DataBind();
                                }
                            }
                        }
                        else
                        {
                            ddlCountry2.Items.Clear();
                            ddlCountry2.DataSource = null;
                            ddlCountry2.DataBind();
                        }
                        if (dsDropDownData.Tables[2].Rows.Count > 0)
                        {
                            objDashBOlocation.country_id = Convert.ToInt32(ddlCountry2.SelectedValue);
                            dsDropDownDataLocation = objDashBL.getFilterDataBL(objDashBOlocation);
                            if (dsDropDownDataLocation.Tables.Count > 0)
                            {
                                if (dsDropDownDataLocation.Tables[2].Rows.Count > 0)
                                {
                                    ddlLocation2.DataSource = dsDropDownDataLocation.Tables[2];
                                    ddlLocation2.DataTextField = "location_name";
                                    ddlLocation2.DataValueField = "location_id";
                                    ddlLocation2.DataBind();
                                    ddlLocation2.Items.Insert(0, new ListItem("All", "0"));
                                    ddlLocation2.SelectedValue = Convert.ToString(objDashBO.location_id);
                                }
                                else
                                {
                                    ddlLocation2.Items.Clear();
                                    ddlLocation2.DataSource = null;
                                    ddlLocation2.DataBind();
                                }

                            }
                        }
                        else
                        {
                            ddlLocation2.Items.Clear();
                            ddlLocation2.DataSource = null;
                            ddlLocation2.DataBind();
                        }
                        if (dsDropDownData.Tables[3].Rows.Count > 0)
                        {
                            ddlLine2.DataSource = dsDropDownData.Tables[3];
                            ddlLine2.DataTextField = "line_name";
                            ddlLine2.DataValueField = "line_id";
                            ddlLine2.DataBind();
                            ddlLine2.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLine2.Items.Clear();
                            ddlLine2.DataSource = null;
                            ddlLine2.DataBind();
                        }
                    }
                }
                else
                {
                    if (dsDropDownData.Tables.Count > 0)
                    {
                        if (dsDropDownData.Tables[0].Rows.Count > 0)
                        {
                            ddlRegion2.DataSource = dsDropDownData.Tables[0];
                            ddlRegion2.DataTextField = "region_name";
                            ddlRegion2.DataValueField = "region_id";
                            ddlRegion2.DataBind();
                            ddlRegion2.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlRegion2.Items.Clear();
                            ddlRegion2.DataSource = null;
                            ddlRegion2.DataBind();
                        }
                        if (dsDropDownData.Tables[1].Rows.Count > 0)
                        {
                            ddlCountry2.DataSource = dsDropDownData.Tables[1];
                            ddlCountry2.DataTextField = "country_name";
                            ddlCountry2.DataValueField = "country_id";
                            ddlCountry2.DataBind();
                            ddlCountry2.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlCountry2.Items.Clear();
                            ddlCountry2.DataSource = null;
                            ddlCountry2.DataBind();
                        }
                        if (dsDropDownData.Tables[2].Rows.Count > 0)
                        {
                            ddlLocation2.DataSource = dsDropDownData.Tables[2];
                            ddlLocation2.DataTextField = "location_name";
                            ddlLocation2.DataValueField = "location_id";
                            ddlLocation2.DataBind();
                            ddlLocation2.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLocation2.Items.Clear();
                            ddlLocation2.DataSource = null;
                            ddlLocation2.DataBind();
                        }
                        if (dsDropDownData.Tables[3].Rows.Count > 0)
                        {
                            ddlLine2.DataSource = dsDropDownData.Tables[3];
                            ddlLine2.DataTextField = "line_name";
                            ddlLine2.DataValueField = "line_id";
                            ddlLine2.DataBind();
                            ddlLine2.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLine2.Items.Clear();
                            ddlLine2.DataSource = null;
                            ddlLine2.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void BindFilters3(DashboardBO objDashBO)
        {
            
            DataSet dsDropDownDataCountry = new DataSet();
            DataSet dsDropDownDataLocation = new DataSet();
            DashboardBO objDashBOCountry = new DashboardBO();
            DashboardBO objDashBOlocation = new DashboardBO();
            int region;
            int country;
            try
            {
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (objDashBO.selection_flag == "Region")
                {
                    if (dsDropDownData.Tables.Count > 0)
                    {
                        
                        if (dsDropDownData.Tables[1].Rows.Count > 0)
                        {
                            ddlCountry3.DataSource = dsDropDownData.Tables[1];
                            ddlCountry3.DataTextField = "country_name";
                            ddlCountry3.DataValueField = "country_id";
                            ddlCountry3.DataBind();
                            ddlCountry3.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlCountry3.Items.Clear();
                            ddlCountry3.DataSource = null;
                            ddlCountry3.DataBind();
                        }
                        if (dsDropDownData.Tables[2].Rows.Count > 0)
                        {
                            ddlLocation3.DataSource = dsDropDownData.Tables[2];
                            ddlLocation3.DataTextField = "location_name";
                            ddlLocation3.DataValueField = "location_id";
                            ddlLocation3.DataBind();
                            ddlLocation3.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLocation3.Items.Clear();
                            ddlLocation3.DataSource = null;
                            ddlLocation3.DataBind();
                        }
                        if (dsDropDownData.Tables[3].Rows.Count > 0)
                        {
                            ddlLine3.DataSource = dsDropDownData.Tables[3];
                            ddlLine3.DataTextField = "line_name";
                            ddlLine3.DataValueField = "line_id";
                            ddlLine3.DataBind();
                            ddlLine3.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLine3.Items.Clear();
                            ddlLine3.DataSource = null;
                            ddlLine3.DataBind();
                        }
                    }
                }

                else if (objDashBO.selection_flag == "Country")
                {
                    
                    region = Convert.ToInt32(ddlRegion3.SelectedValue);
                    if (dsDropDownData.Tables.Count > 0)
                    {
                        if (dsDropDownData.Tables[0].Rows.Count > 0)
                        {
                            if (ddlRegion3.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                            //if (region != 0)
                            {
                                ddlRegion3.SelectedValue = Convert.ToString(region);
                            }
                            else
                            {
                                ddlRegion3.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                            }
                        }
                        else
                        {
                        }
                        if (dsDropDownData.Tables[1].Rows.Count > 0)
                        {
                            if (ddlRegion3.Items.FindByValue(Convert.ToString(region)) == null || region == 0)
                            {
                                objDashBOCountry.region_id = Convert.ToInt32(ddlRegion3.SelectedValue);
                                dsDropDownDataCountry = objDashBL.getFilterDataBL(objDashBOCountry);
                                if (dsDropDownDataCountry.Tables.Count > 0)
                                {
                                    if (dsDropDownDataCountry.Tables[1].Rows.Count > 0)
                                    {
                                        ddlCountry3.DataSource = dsDropDownDataCountry.Tables[1];
                                        ddlCountry3.DataTextField = "country_name";
                                        ddlCountry3.DataValueField = "country_id";
                                        ddlCountry3.DataBind();
                                        ddlCountry3.Items.Insert(0, new ListItem("All", "0"));
                                        ddlCountry3.SelectedValue = Convert.ToString(objDashBO.country_id);
                                    }
                                    else
                                    {
                                        ddlCountry3.Items.Clear();
                                        ddlCountry3.DataSource = null;
                                        ddlCountry3.DataBind();
                                    }
                                }
                            }
                            else
                            {
                                ddlCountry3.SelectedValue = Convert.ToString(objDashBO.country_id);
                            }
                        }
                        else
                        {
                            ddlCountry3.Items.Clear();
                            ddlCountry3.DataSource = null;
                            ddlCountry3.DataBind();
                        }
                        if (dsDropDownData.Tables[2].Rows.Count > 0)
                        {
                            ddlLocation3.DataSource = dsDropDownData.Tables[2];
                            ddlLocation3.DataTextField = "location_name";
                            ddlLocation3.DataValueField = "location_id";
                            ddlLocation3.DataBind();
                            ddlLocation3.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLocation3.Items.Clear();
                            ddlLocation3.DataSource = null;
                            ddlLocation3.DataBind();
                        }
                        if (dsDropDownData.Tables[3].Rows.Count > 0)
                        {
                            ddlLine3.DataSource = dsDropDownData.Tables[3];
                            ddlLine3.DataTextField = "line_name";
                            ddlLine3.DataValueField = "line_id";
                            ddlLine3.DataBind();
                            ddlLine3.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLine3.Items.Clear();
                            ddlLine3.DataSource = null;
                            ddlLine3.DataBind();
                        }
                    }
                }
                else if (objDashBO.selection_flag == "Location")
                {
                    region = Convert.ToInt32(ddlRegion3.SelectedValue);
                    country = Convert.ToInt32(ddlCountry3.SelectedValue);
                    if (dsDropDownData.Tables.Count > 0)
                    {
                        if (dsDropDownData.Tables[0].Rows.Count > 0)
                        {
                            if (ddlRegion3.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                            //if (region != 0)
                            {
                                ddlRegion3.SelectedValue = Convert.ToString(region);
                            }
                            else
                            {
                                ddlRegion3.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                            }
                        }
                        else
                        {
                        }
                        if (dsDropDownData.Tables[1].Rows.Count > 0)
                        {
                            objDashBOCountry.region_id = Convert.ToInt32(ddlRegion3.SelectedValue);
                            dsDropDownDataCountry = objDashBL.getFilterDataBL(objDashBOCountry);
                            if (dsDropDownDataCountry.Tables.Count > 0)
                            {
                                if (dsDropDownDataCountry.Tables[1].Rows.Count > 0)
                                {
                                    ddlCountry3.DataSource = dsDropDownDataCountry.Tables[1];
                                    ddlCountry3.DataTextField = "country_name";
                                    ddlCountry3.DataValueField = "country_id";
                                    ddlCountry3.DataBind();
                                    ddlCountry3.Items.Insert(0, new ListItem("All", "0"));
                                    if (ddlCountry3.Items.FindByValue(Convert.ToString(country)) != null && country != 0)
                                    {
                                        ddlCountry3.SelectedValue = Convert.ToString(country);
                                    }
                                    else
                                    {
                                        ddlCountry3.SelectedValue = Convert.ToString(dsDropDownData.Tables[1].Rows[0]["country_id"]); ;
                                    }
                                }
                                else
                                {
                                    ddlCountry3.Items.Clear();
                                    ddlCountry3.DataSource = null;
                                    ddlCountry3.DataBind();
                                }
                            }
                        }
                        else
                        {
                            ddlCountry3.Items.Clear();
                            ddlCountry3.DataSource = null;
                            ddlCountry3.DataBind();
                        }
                        if (dsDropDownData.Tables[2].Rows.Count > 0)
                        {
                            objDashBOlocation.country_id = Convert.ToInt32(ddlCountry3.SelectedValue);
                            dsDropDownDataLocation = objDashBL.getFilterDataBL(objDashBOlocation);
                            if (dsDropDownDataLocation.Tables.Count > 0)
                            {
                                if (dsDropDownDataLocation.Tables[2].Rows.Count > 0)
                                {
                                    ddlLocation3.DataSource = dsDropDownDataLocation.Tables[2];
                                    ddlLocation3.DataTextField = "location_name";
                                    ddlLocation3.DataValueField = "location_id";
                                    ddlLocation3.DataBind();
                                    ddlLocation3.Items.Insert(0, new ListItem("All", "0"));
                                    ddlLocation3.SelectedValue = Convert.ToString(objDashBO.location_id);
                                }
                                else
                                {
                                    ddlLocation3.Items.Clear();
                                    ddlLocation3.DataSource = null;
                                    ddlLocation3.DataBind();
                                }

                            }
                        }
                        else
                        {
                            ddlLocation3.Items.Clear();
                            ddlLocation3.DataSource = null;
                            ddlLocation3.DataBind();
                        }
                        if (dsDropDownData.Tables[3].Rows.Count > 0)
                        {
                            ddlLine3.DataSource = dsDropDownData.Tables[3];
                            ddlLine3.DataTextField = "line_name";
                            ddlLine3.DataValueField = "line_id";
                            ddlLine3.DataBind();
                            ddlLine3.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLine3.Items.Clear();
                            ddlLine3.DataSource = null;
                            ddlLine3.DataBind();
                        }
                    }
                }
                else
                {
                    if (dsDropDownData.Tables.Count > 0)
                    {
                        if (dsDropDownData.Tables[0].Rows.Count > 0)
                        {
                            ddlRegion3.DataSource = dsDropDownData.Tables[0];
                            ddlRegion3.DataTextField = "region_name";
                            ddlRegion3.DataValueField = "region_id";
                            ddlRegion3.DataBind();
                            ddlRegion3.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlRegion3.Items.Clear();
                            ddlRegion3.DataSource = null;
                            ddlRegion3.DataBind();
                        }
                        if (dsDropDownData.Tables[1].Rows.Count > 0)
                        {
                            ddlCountry3.DataSource = dsDropDownData.Tables[1];
                            ddlCountry3.DataTextField = "country_name";
                            ddlCountry3.DataValueField = "country_id";
                            ddlCountry3.DataBind();
                            ddlCountry3.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlCountry3.Items.Clear();
                            ddlCountry3.DataSource = null;
                            ddlCountry3.DataBind();
                        }
                        if (dsDropDownData.Tables[2].Rows.Count > 0)
                        {
                            ddlLocation3.DataSource = dsDropDownData.Tables[2];
                            ddlLocation3.DataTextField = "location_name";
                            ddlLocation3.DataValueField = "location_id";
                            ddlLocation3.DataBind();
                            ddlLocation3.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLocation3.Items.Clear();
                            ddlLocation3.DataSource = null;
                            ddlLocation3.DataBind();
                        }
                        if (dsDropDownData.Tables[3].Rows.Count > 0)
                        {
                            ddlLine3.DataSource = dsDropDownData.Tables[3];
                            ddlLine3.DataTextField = "line_name";
                            ddlLine3.DataValueField = "line_id";
                            ddlLine3.DataBind();
                            ddlLine3.Items.Insert(0, new ListItem("All", "0"));
                        }
                        else
                        {
                            ddlLine3.Items.Clear();
                            ddlLine3.DataSource = null;
                            ddlLine3.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        [WebMethod]
        public static string returnLPAResultMonthly()
        {
            CommonFunctions objCom = new CommonFunctions();
            DashboardBO objDashBO;
            DashboardBL objDashBL;
            DataTable dtReport1 = new DataTable();
            DataTable dtReport2 = new DataTable();
            DataTable dtReport3 = new DataTable();
            string output = string.Empty;
            try
            {
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location1"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region1"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country1"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line1"]);


                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(Convert.ToString(HttpContext.Current.Session["date"]), UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string reportdate = parsedDate.ToString(DBPattern);
                    // objPlanBO.p_planned_start_date = Convert.ToDateTime(plannedstartdate);
                    // objDashBO.reportdate = DateTime.Parse(reportdate);
                    objDashBO.reportdate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }

                dtReport1 = objDashBL.getDataforLPAResultsReportBL(objDashBO);

                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location2"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region2"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country2"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line2"]);
                dtReport2 = objDashBL.getDataforLPAResultsReportBL(objDashBO);

                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location3"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region3"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country3"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line3"]);
                dtReport3 = objDashBL.getDataforLPAResultsReportBL(objDashBO);

                dtReport1.Columns.Add("average_score_2");
                dtReport1.Columns.Add("previous_score_2");
                dtReport1.Columns.Add("average_score_3");
                dtReport1.Columns.Add("previous_score_3");
                dtReport1.Columns.Add("color1");
                dtReport1.Columns.Add("color2");
                dtReport1.Columns.Add("color3");

                for (int i = 0; i < dtReport1.Rows.Count; i++)
                {
                    dtReport1.Rows[i]["color1"] = "#00732d";
                    dtReport1.Rows[i]["color2"] = "#eb690f";
                    dtReport1.Rows[i]["color3"] = "#7f7f7f";
                    dtReport1.Rows[i]["average_score"] = Convert.ToInt32(Convert.ToDecimal(dtReport1.Rows[i]["average_score"]) * 100);
                    dtReport1.Rows[i]["previous_score"] = Convert.ToInt32(Convert.ToDecimal(dtReport1.Rows[i]["previous_score"]) * 100);
                    dtReport1.Rows[i]["average_score_2"] = Convert.ToInt32(Convert.ToDecimal(dtReport2.Rows[i]["average_score"]) * 100);
                    dtReport1.Rows[i]["previous_score_2"] = Convert.ToInt32(Convert.ToDecimal(dtReport2.Rows[i]["previous_score"]) * 100);
                    dtReport1.Rows[i]["average_score_3"] = Convert.ToInt32(Convert.ToDecimal(dtReport3.Rows[i]["average_score"]) * 100);
                    dtReport1.Rows[i]["previous_score_3"] = Convert.ToInt32(Convert.ToDecimal(dtReport3.Rows[i]["previous_score"]) * 100);
                }

                output = DataTableToJSONWithStringBuilder(dtReport1);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }

        [WebMethod]
        public static string returnLPAResultPerSection()
        {
            CommonFunctions objCom = new CommonFunctions();
            DashboardBO objDashBO;
            DashboardBL objDashBL;
            DataTable dtReport1 = new DataTable();
            DataTable dtReport2 = new DataTable();
            DataTable dtReport3 = new DataTable();
            string output = string.Empty;

            try
            {
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location1"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region1"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country1"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line1"]);

                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(Convert.ToString(HttpContext.Current.Session["date"]), UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string reportdate = parsedDate.ToString(DBPattern);
                    objDashBO.reportdate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }
                dtReport1 = objDashBL.getLPAResultPerSectionBL(objDashBO);

                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location2"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region2"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country2"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line2"]);
                dtReport2 = objDashBL.getLPAResultPerSectionBL(objDashBO);

                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location3"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region3"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country3"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line3"]);
                dtReport3 = objDashBL.getLPAResultPerSectionBL(objDashBO);

                dtReport1.Columns.Add("Score_2");
                dtReport1.Columns.Add("Score_3");
                dtReport1.Columns.Add("color1");
                dtReport1.Columns.Add("color2");
                dtReport1.Columns.Add("color3");

                for (int i = 0; i < dtReport1.Rows.Count; i++)
                {
                    dtReport1.Rows[i]["color1"] = "#00732d";
                    dtReport1.Rows[i]["color2"] = "#eb690f";
                    dtReport1.Rows[i]["color3"] = "#7f7f7f";
                    dtReport1.Rows[i]["Score"] = Convert.ToInt32(Convert.ToDecimal(dtReport1.Rows[i]["Score"]) * 100);
                    dtReport1.Rows[i]["Score_2"] = Convert.ToInt32(Convert.ToDecimal(dtReport2.Rows[i]["Score"]) * 100);
                    dtReport1.Rows[i]["Score_3"] = Convert.ToInt32(Convert.ToDecimal(dtReport3.Rows[i]["Score"]) * 100);

                }

                output = DataTableToJSONWithStringBuilder(dtReport1);
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;

        }

        [WebMethod]
        public static string returnLPAResultPerQuestion()
        {

            CommonFunctions objCom = new CommonFunctions();
            DashboardBO objDashBO;
            DashboardBL objDashBL;
            DataTable dtReport1 = new DataTable();
            DataTable dtReport2 = new DataTable();
            DataTable dtReport3 = new DataTable();
            string output = string.Empty;

            try
            {
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location1"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region1"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country1"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line1"]);

                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(Convert.ToString(HttpContext.Current.Session["date"]), UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string reportdate = parsedDate.ToString(DBPattern);
                    objDashBO.reportdate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }
                dtReport1 = objDashBL.getLPAResultByQuestionBL(objDashBO);

                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location2"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region2"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country2"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line2"]);
                dtReport2 = objDashBL.getLPAResultByQuestionBL(objDashBO);

                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location3"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region3"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country3"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line3"]);
                dtReport3 = objDashBL.getLPAResultByQuestionBL(objDashBO);

                dtReport1.Columns.Add("score_2");
                dtReport1.Columns.Add("score_3");
                dtReport1.Columns.Add("color1");
                dtReport1.Columns.Add("color2");
                dtReport1.Columns.Add("color3");

                for (int i = 0; i < dtReport1.Rows.Count; i++)
                {
                    dtReport1.Rows[i]["color1"] = "#00732d";
                    dtReport1.Rows[i]["color2"] = "#eb690f";
                    dtReport1.Rows[i]["color3"] = "#7f7f7f";
                    dtReport1.Rows[i]["score"] = Convert.ToInt32(Convert.ToDecimal(dtReport1.Rows[i]["score"]) * 100);
                    dtReport1.Rows[i]["score_2"] = Convert.ToInt32(Convert.ToDecimal(dtReport2.Rows[i]["score"]) * 100);
                    dtReport1.Rows[i]["score_3"] = Convert.ToInt32(Convert.ToDecimal(dtReport3.Rows[i]["score"]) * 100);
                    dtReport1.Rows[i]["question_id"] = Convert.ToString(dtReport1.Rows[i]["question_id"]).Substring(1, Convert.ToString(dtReport1.Rows[i]["question_id"]).Length - 1);
                }
                output = DataTableToJSONWithStringBuilder(dtReport1);
            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }

        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                        }
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }

        public void LoadSecLabels()
        {
            CommonFunctions objCom = new CommonFunctions();
            DashboardBO objDashBO;
            DashboardBL objDashBL;
            DataTable dtReport1 = new DataTable();
            DataTable dtReport2 = new DataTable();
            DataTable dtReport3 = new DataTable();
            string output = string.Empty;

            try
            {
                objDashBO = new DashboardBO();
                objDashBL = new DashboardBL();

                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location1"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region1"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country1"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line1"]);

                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                if (DateTime.TryParseExact(Convert.ToString(HttpContext.Current.Session["date"]), UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string reportdate = parsedDate.ToString(DBPattern);
                    objDashBO.reportdate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }
                dtReport1 = objDashBL.getLPAResultPerSectionBL(objDashBO);

                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location2"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region2"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country2"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line2"]);
                dtReport2 = objDashBL.getLPAResultPerSectionBL(objDashBO);

                objDashBO.location_id = Convert.ToInt32(HttpContext.Current.Session["location3"]);
                objDashBO.region_id = Convert.ToInt32(HttpContext.Current.Session["region3"]);
                objDashBO.country_id = Convert.ToInt32(HttpContext.Current.Session["country3"]);
                objDashBO.line_id = Convert.ToInt32(HttpContext.Current.Session["line3"]);
                dtReport3 = objDashBL.getLPAResultPerSectionBL(objDashBO);

                sec1.Text = Convert.ToString(dtReport1.Rows[0]["Sec_name_abbr"]) + " - " + Convert.ToString(dtReport1.Rows[0]["section_name"]);
                sec2.Text = Convert.ToString(dtReport1.Rows[1]["Sec_name_abbr"]) + " - " + Convert.ToString(dtReport1.Rows[1]["section_name"]);
                sec3.Text = Convert.ToString(dtReport1.Rows[2]["Sec_name_abbr"]) + " - " + Convert.ToString(dtReport1.Rows[2]["section_name"]);
                sec4.Text = Convert.ToString(dtReport1.Rows[3]["Sec_name_abbr"]) + " - " + Convert.ToString(dtReport1.Rows[3]["section_name"]);
                sec5.Text = Convert.ToString(dtReport1.Rows[4]["Sec_name_abbr"]) + " - " + Convert.ToString(dtReport1.Rows[4]["section_name"]);
                sec6.Text = Convert.ToString(dtReport1.Rows[5]["Sec_name_abbr"]) + " - " + Convert.ToString(dtReport1.Rows[5]["section_name"]);


            }

            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        #endregion

    }
}