﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Planning.aspx.cs" Inherits="MHLPA_Application.Planning" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="css/calender.css" rel="stylesheet" />
    <script src="js/calender.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            // The one that fires the event is always the
            // checked one; you don't need to test for this
            PageLoadScript();

        });

        function PageLoadScript() {


            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            $('#external-events div.external-event').each(function () {
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };
                $(this).data('eventObject', eventObject);
                $(this).draggable({
                    zIndex: 999,
                    revert: true,      // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });

            });

            var calender = $('#calendar').fullCalendar({
                weekNumbers: true,
                header: {
                    left: 'title',
                    center: 'agendaDay,agendaWeek,month',
                    right: 'prev,next today'
                },
                editable: true,
                firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
                selectable: true,
                defaultView: 'month',

                axisFormat: 'h:mm',
                columnFormat: {
                    month: 'ddd',    // Mon
                    week: 'ddd d', // Mon 7
                    day: 'dddd M/d',  // Monday 9/7
                    agendaDay: 'dddd d'
                },
                titleFormat: {
                    month: 'MMMM yyyy', // September 2009
                    week: "MMMM yyyy", // September 2009
                    day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
                },
                allDaySlot: false,
                //eventClick: updateEvent,
                selectHelper: true,
                //select: selectDate,
                droppable: true, // this allows things to be dropped onto the calendar !!!
                drop: function (date, allDay) { // this function is called when something is dropped

                    var originalEventObject = $(this).data('eventObject');
                    var copiedEventObject = $.extend({}, originalEventObject);
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                    if ($('#drop-remove').is(':checked')) {
                        $(this).remove();
                    }
                },

                events:
                    "CalendarEvent.ashx",
                //[{ "audit_plan_id": 114, "planned_date": "07-31-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "Group1", "allDay": false, "title": "Group1", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 0, "Userflag": "N" }, { "audit_plan_id": 112, "planned_date": "07-19-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "Group1", "allDay": false, "title": "Group1", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 0, "Userflag": "N" }, { "audit_plan_id": 113, "planned_date": "07-12-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "Group1", "allDay": false, "title": "Group1", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 0, "Userflag": "N" }, { "audit_plan_id": 115, "planned_date": "07-12-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 118, "planned_date": "07-12-2017 00:00:00 AM", "shift_no": 3, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 129, "planned_date": "07-11-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "Bakta Salla", "allDay": false, "title": "Bakta Salla", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 12, "Userflag": "Y" }, { "audit_plan_id": 127, "planned_date": "07-11-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 128, "planned_date": "07-10-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 126, "planned_date": "07-10-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 123, "planned_date": "07-09-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 124, "planned_date": "07-09-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 125, "planned_date": "07-09-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 120, "planned_date": "07-08-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 121, "planned_date": "07-08-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 122, "planned_date": "07-08-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 119, "planned_date": "07-07-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 116, "planned_date": "07-05-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 117, "planned_date": "07-05-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "Bakta Salla", "allDay": false, "title": "Bakta Salla", "line_product_rel_id": 4, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 12, "Userflag": "Y" }, { "audit_plan_id": 111, "planned_date": "06-23-2017 00:00:00 AM", "shift_no": 3, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 1, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 110, "planned_date": "06-22-2017 00:00:00 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 1, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 109, "planned_date": "06-21-2017 11:52:40 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 1, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 108, "planned_date": "06-20-2017 11:52:35 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 1, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 107, "planned_date": "06-19-2017 11:52:30 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 1, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 106, "planned_date": "06-18-2017 11:52:25 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 1, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 105, "planned_date": "06-17-2017 11:52:21 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 1, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 104, "planned_date": "06-16-2017 11:52:17 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 1, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 103, "planned_date": "06-15-2017 11:52:13 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 1, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 102, "planned_date": "06-14-2017 11:52:08 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 1, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 101, "planned_date": "06-13-2017 11:52:02 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 1, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }, { "audit_plan_id": 100, "planned_date": "06-12-2017 11:51:24 AM", "shift_no": 1, "Emp_name": "akhila", "allDay": false, "title": "akhila", "line_product_rel_id": 1, "line_id": 1, "product_id": 1, "to_be_audited_by_user_id": 6, "Userflag": "Y" }]

            });
        }
        function updateEvent(event, element, start, end) {
            //alert("Update Event");
            //debugger
            $('#MainContent_lblError').text('');
            if ($(this).data("qtip")) $(this).qtip("destroy");
            currentUpdateEvent = event;

            $('#addDialog').modal();
            //var shiftno = event.shift_no;
            var emp_name = event.to_be_audited_by_user_id;
            var line_id = event.line_id;
            //var line_product_rel_id = event.line_product_rel_id;
            //var product_id = event.product_id;
            var audit_id = event.audit_plan_id;
            var flag = event.Userflag;
            var planned_date = event.planned_date;
            var formated_date = new Date(planned_date);
            var planned_end_date = event.planned_end_date;
            var formated_end_date = new Date(planned_end_date);
            var DD = formated_date.getDate();
            var MM = formated_date.getMonth() + 1; //January is 0!
            var YYYY = formated_date.getFullYear();
            if (DD < 10) {
                DD = '0' + DD
            }
            if (MM < 10) {
                MM = '0' + MM
            }
            var newdt = DD + '-' + MM + '-' + YYYY;

            DD = formated_end_date.getDate();
            MM = formated_end_date.getMonth() + 1; //January is 0!
            YYYY = formated_end_date.getFullYear();
            if (DD < 10) {
                DD = '0' + DD
            }
            if (MM < 10) {
                MM = '0' + MM
            }
            var newenddt = DD + '-' + MM + '-' + YYYY;
            $('#MainContent_hdnLineProdRelId').val(line_id);
            $('#MainContent_ddlusers').val(emp_name);
            $('#MainContent_ddlLineNumber').val(line_id);
            //$('#MainContent_ddlProduct').val(product_id);
            //$('#MainContent_ddlShift').val(shiftno);
            $('#MainContent_hdnPlanId').val(audit_id);
            $('#MainContent_txtPlannedStartDate').val(newdt);
            $('#MainContent_txtPlannedEndDate').val(newenddt);

            //if (flag == 'Y') {
            //    MainContent_rdbOwnerType_0.checked = true;
            //    $("#MainContent_ddlusers").show();
            //    $("#MainContent_ddlGroup").hide();
            //}
            //if (flag == 'N') {
            //    MainContent_rdbOwnerType_1.checked = true;
            //    $("#MainContent_ddlusers").hide();
            //    $("#MainContent_ddlGroup").show();
            //}
            //alert(new Date());
            var diff = daydiff(formated_date, new Date());
            if (diff < 7) {
                $('#MainContent_btnSave').css('display', 'none');
                $('#MainContent_btnDelete').css('display', 'none');
            }
            else {
                $('#MainContent_btnSave').css('display', 'block');
                $('#MainContent_btnDelete').css('display', 'block');
            }
            //$("#MainContent_txtPlannedStartDate").datepicker({
            //    showOn: "both",
            //    buttonImageOnly: true,
            //    buttonText: "",
            //    changeYear: true,
            //    changeMonth: true,
            //    yearRange: "c-20:c+50",
            //    //minDate: new Date(),
            //    dateFormat: 'dd-mm-yy',
            //    //buttonImage: "images/calander_icon.png",
            //});
            //$("#MainContent_txtPlannedEndDate").datepicker({
            //    showOn: "both",
            //    buttonImageOnly: true,
            //    buttonText: "",
            //    changeYear: true,
            //    changeMonth: true,
            //    yearRange: "c-20:c+50",
            //    // minDate: new Date(),
            //    dateFormat: 'dd-mm-yy',
            //    //buttonImage: "images/calander_icon.png",
            //});
            $("#MainContent_txtPlannedStartDate").attr('disabled', 'disabled');
            $("#MainContent_txtPlannedEndDate").attr('disabled', 'disabled');
        }
        function selectDate(start, end, allDay) {
            $('#MainContent_lblError').text('');
            var newstartdt = [start.getMonth(),
               start.getDate(),
               start.getFullYear()].join('-') + ' ' +
              [start.getHours(),
               start.getMinutes(),
               start.getSeconds()].join(':');
            var newenddt = [end.getMonth(),
               end.getDate(),
               end.getFullYear()].join('-') + ' ' +
              [end.getHours(),
               end.getMinutes(),
               end.getSeconds()].join(':');
            var today = new Date();
            var DD = today.getDate();
            var MM = today.getMonth() + 1; //January is 0!
            var YYYY = today.getFullYear();
            if (DD < 10) {
                DD = '0' + DD
            }
            if (MM < 10) {
                MM = '0' + MM
            }
            var hh = today.getHours();
            var mm = today.getMinutes();
            var ss = today.getSeconds();

            var todaydate = MM + '-' + DD + '-' + YYYY + ' ' + hh + ':' + mm + ':' + ss;
            var newstartdate = new Date(newstartdt);
            var today_date = new Date(todaydate);
            var newenddate = new Date(newenddt);

            end = getFriday(start);
            start = getMonday(start);
            var diff = daydiff(start, new Date());
            if (diff > -6) {
                $('#addDialog').modal({
                    buttons: {
                        "Add": function () {
                            //alert("sent:" + addStartDate.format("dd-MM-yyyy hh:mm:ss tt") + "==" + addStartDate.toLocaleString());
                            var eventToAdd = {
                                title: $("#addEventName").val(),
                                description: $("#addEventDesc").val(),
                                start: addStartDate.toJSON(),
                                end: addEndDate.toJSON(),

                                allDay: isAllDay(addStartDate, addEndDate)
                            };

                            if (checkForSpecialChars(eventToAdd.title) || checkForSpecialChars(eventToAdd.description)) {
                                alert("please enter characters: A to Z, a to z, 0 to 9, spaces");
                            }
                            else {
                                //alert("sending " + eventToAdd.title);

                                PageMethods.addEvent(eventToAdd, addSuccess);
                                $(this).dialog("close");
                            }
                        }
                    }
                });
            }

            var DD = start.getDate();
            var MM = start.getMonth() + 1; //January is 0!
            var YYYY = start.getFullYear();
            if (DD < 10) {
                DD = '0' + DD
            }
            if (MM < 10) {
                MM = '0' + MM
            }
            var newstartdt = DD + '-' + MM + '-' + YYYY;
            //start.format("DD-MM-YYYY");
            ////alert(newstartdt);
            ////var dateEnd = new Date(end);
            //var newEnddt = end.format("MM-DD-YYYY hh:mm:ss T")
            $("#MainContent_txtPlannedStartDate").val("" + newstartdt);
            var DD = end.getDate();
            var MM = end.getMonth() + 1; //January is 0!
            var YYYY = end.getFullYear();
            if (DD < 10) {
                DD = '0' + DD
            }
            if (MM < 10) {
                MM = '0' + MM
            }
            var newenddt = DD + '-' + MM + '-' + YYYY;
            $("#MainContent_txtPlannedEndDate").val("" + newenddt);
            $("#MainContent_addEventEndDate").val("" + newenddt);
            $("#MainContent_txtPlannedStartDate").attr('disabled', 'disabled');
            $("#MainContent_txtPlannedEndDate").attr('disabled', 'disabled');

            tempStartDate = "" + start.toLocaleString();
            tempEnddate = "" + end.toLocaleString();
            addStartDate = start;
            addEndDate = end;
            globalAllDay = allDay;

            $('#MainContent_hdnLineProdRelId').val('');
            $('#MainContent_ddlusers').val('');
            $('#MainContent_ddlLineNumber').val('');
            //$('#MainContent_ddlProduct').val('');
            //$('#MainContent_ddlShift').val('');
            $('#MainContent_hdnPlanId').val('');
            //var radioButtons = $('#MainContent_rdbOwnerType');
            //MainContent_rdbOwnerType_0.checked = true;
            //if (MainContent_rdbOwnerType_0.checked) {
            //    $("#MainContent_ddlusers").show();
            //    $("#MainContent_ddlGroup").hide();
            //}
            //if (MainContent_rdbOwnerType_1.checked) {
            //    $("#MainContent_ddlusers").hide();
            //    $("#MainContent_ddlGroup").show();
            //}

            var diff = daydiff(new Date(), start);
            if (diff > 7) {
                $('#MainContent_btnSave').css('display', 'none');
                $('#MainContent_btnDelete').css('display', 'none');
            }
            else {
                $('#MainContent_btnSave').css('display', 'block');
            }
            //$("#MainContent_txtPlannedStartDate").datepicker({
            //    showOn: "both",
            //    buttonImageOnly: true,
            //    buttonText: "",
            //    changeYear: true,
            //    changeMonth: true,
            //    yearRange: "c-20:c+50",
            //    //minDate: new Date(),
            //    dateFormat: 'dd-mm-yy',
            //    //buttonImage: "images/calander_icon.png",
            //});
            //$("#MainContent_txtPlannedEndDate").datepicker({
            //    showOn: "both",
            //    buttonImageOnly: true,
            //    buttonText: "",
            //    changeYear: true,
            //    changeMonth: true,
            //    yearRange: "c-20:c+50",
            //    // minDate: new Date(),
            //    dateFormat: 'dd-mm-yy',
            //    //buttonImage: "images/calander_icon.png",
            //});

        }
        function getMonday(d) {
            //////debugger
            d = new Date(d);
            var day = d.getDay(),
                diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
            return new Date(d.setDate(diff));
        }
        function getFriday(d) {
            ////debugger
            d = new Date(d);
            var day = d.getDay(),
                diff = d.getDate() - day + (day == 0 ? 0 : 5); // adjust when day is sunday
            return new Date(d.setDate(diff));
        }
        function daydiff(second, first) {
            return Math.round((second - first) / (1000 * 60 * 60 * 24));
        }
        function validateControls() {
            ////debugger
            var err_flag = 0;
            //if (MainContent_rdbOwnerType_0.checked == true) {
            if ($('#MainContent_ddlusers').val() == "" || $('#MainContent_ddlusers').val() == null) {
                $('#MainContent_ddlusers').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_ddlusers').css('border-color', '');
            }
            //}
            //else {
            //    if ($('#MainContent_ddlGroup').val() == "" || $('#MainContent_ddlGroup').val() == null) {
            //        $('#MainContent_ddlGroup').css('border-color', 'red');
            //        err_flag = 1;
            //    }
            //    else {
            //        $('#MainContent_ddlGroup').css('border-color', '');
            //    }
            //}
            if ($('#MainContent_ddlLineNumber').val() == "" || $('#MainContent_ddlLineNumber').val() == null) {
                $('#MainContent_ddlLineNumber').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#MainContent_ddlLineNumber').css('border-color', '');
            }
            //if ($('#MainContent_ddlProduct').val() == "" || $('#MainContent_ddlProduct').val() == null) {
            //    $('#MainContent_ddlProduct').css('border-color', 'red');

            //    err_flag = 1;
            //}
            //else {
            //    $('#MainContent_ddlProduct').css('border-color', '');
            //}

            //if ($('#MainContent_ddlShift').val() == "" || $('#MainContent_ddlShift').val() == null) {
            //    $('#MainContent_ddlShift').css('border-color', 'red');

            //    err_flag = 1;
            //}
            //else {
            //    $('#MainContent_ddlShift').css('border-color', '');
            //}
            if ($('#MainContent_txtPlannedStartDate').val() == "" || $('#MainContent_txtPlannedStartDate').val() == null) {
                $('#MainContent_txtPlannedStartDate').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#MainContent_txtPlannedStartDate').css('border-color', '');
                //return validateDates();
            }
            if ($('#MainContent_txtPlannedEndDate').val() == "" || $('#MainContent_txtPlannedStartDate').val() == null) {
                $('#MainContent_txtPlannedEndDate').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                // return validateDates();
                $('#MainContent_txtPlannedEndDate').css('border-color', '');
            }
            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return true;

            }
            else {
                $('#MainContent_lblError').text('Please enter all the mandatory fields.');
                return false;
            }
        }
        //function validateDates() {
        //    ////debugger
        //    var myDate = new Date($('#MainContent_txtPlannedStartDate').val());
        //    var today = new Date();
        //    if (myDate < today) {
        //        $('#MainContent_lblError').text('Start date should be greater than current date.');
        //        return false;
        //    }
        //    var myEndDate = new Date($('#MainContent_txtPlannedEndDate').val());
        //    if (myEndDate < today) {
        //        $('#MainContent_lblError').text('End date should be greater than current date.');
        //        return false;
        //    }

        //    if (myEndDate < myDate)
        //    {
        //        $('#MainContent_lblError').text('End date should be greater than start date.');
        //        return false;
        //    }

        //    //if (Date.parse($('#MainContent_txtPlannedStartDate').val()) <= Date.parse(new Date)) {
        //    //    $('#MainContent_lblError').text('Start date should be greater than current date.');
        //    //    return false;
        //    //}
        //    if (Date.parse($('#MainContent_txtPlannedEndDate').val()) <= Date.parse(new Date)) {
        //        $('#MainContent_lblError').text('End date should be greater than current date.');
        //        return false;
        //    }
        //    if (Date.parse($('#MainContent_txtPlannedEndDate').val()) <= Date.parse($('#MainContent_txtPlannedStartDate').val())) {
        //        //if ($.datepicker.parseDate('dd/mm/yy',$('#MainContent_txtEndDate').val()) < $.datepicker.parseDate('dd/mm/yy',$("#MainContent_txtStartDate").val())) {
        //        $('#MainContent_lblError').text('End date should be greater than start date.');
        //        return false;
        //    }

        //}
        //$("#MainContent_txtPlannedStartDate").datepicker({
        //    showOn: "both",
        //    buttonImageOnly: true,
        //    buttonText: "",
        //    changeYear: true,
        //    changeMonth: true,
        //    yearRange: "c-20:c+50",
        //    //minDate: new Date(),
        //    dateFormat: 'dd-mm-yy',
        //    //buttonImage: "images/calander_icon.png",
        //});
        //$("#MainContent_txtPlannedEndDate").datepicker({
        //    showOn: "both",
        //    buttonImageOnly: true,
        //    buttonText: "",
        //    changeYear: true,
        //    changeMonth: true,
        //    yearRange: "c-20:c+50",
        //    // minDate: new Date(),
        //    dateFormat: 'dd-mm-yy',
        //    //buttonImage: "images/calander_icon.png",
        //});
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb"><%=Resources.Resource.Audit %></a>
                        </li>
                        <li class="current">Planning</li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div>
        <!-- main content start-->
        <div class="md_main">

            <div class="row">
                <div>
                    <div class="clearfix"></div>
                    <div class="filter_panel">
                        <div>
                            <div class="col-md-4 filter_label" style="width: 14%">
                                <p><%=Resources.Resource.Region %>* : </p>
                            </div>
                            <div class="col-md-4" style="width: 20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-4 filter_label" style="width: 13%">
                                <p><%=Resources.Resource.Country %>* : </p>
                            </div>
                            <div class="col-md-4" style="width: 20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-4 filter_label" style="width: 13%">
                                <p><%=Resources.Resource.Location %>* : </p>
                            </div>
                            <div class="col-md-4" style="width: 20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </div>
                    </div>


                    <div class="clearfix" style="height: 5px;"></div>
                    <div id='calendar'></div>
                </div>
            </div>

            <div style='clear: both'></div>
            <div id="addDialog" class="modal fade">
                <div class="mn_popup">
                    <asp:UpdatePanel ID="updModel" runat="server">
                        <ContentTemplate>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 id="H1" class="modal-title" style="margin-left: 40%;">Create Plan</h4>
                                </div>
                                <div id='Div2'></div>
                                .
                                    <div id="Div1" class="modal-body">
                                        <div class="filter_panel">

                                            <div>

                                                <div class="col-md-4 filter_label" style="width: 40%">
                                                    <p>Employee :</p>
                                                </div>
                                                <div class="col-md-4" style="width: 60%">
                                                    <asp:DropDownList CssClass="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlusers"></asp:DropDownList>
                                                </div>

                                            </div>
                                            <div>
                                                <div class="col-md-4 filter_label" style="width: 40%">
                                                    <p>Line Name/No. :</p>
                                                </div>
                                                <div class="col-md-4" style="width: 60%">
                                                    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLineNumber"></asp:DropDownList>

                                                </div>
                                            </div>
                                            <%--<div>
                                                <div class="col-md-4 filter_label" style="width: 40%">
                                                    <p>Product/Part No. :</p>
                                                </div>
                                                <div class="col-md-4" style="width: 60%">
                                                    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlProduct"></asp:DropDownList>

                                                </div>
                                            </div>
                                            <div>
                                                <div class="col-md-4 filter_label" style="width: 40%">
                                                    <p>Shift : </p>
                                                </div>
                                                <div class="col-md-4" style="width: 60%">
                                                    <asp:DropDownList runat="server" ID="ddlShift" class="form-control1 mn_inp control3 filter_select_control">
                                                        <asp:ListItem>1</asp:ListItem>
                                                        <asp:ListItem>2</asp:ListItem>
                                                        <asp:ListItem>3</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>--%>
                                            <div>
                                                <div class="col-md-4 filter_label" style="width: 40%">
                                                    <p>Planned Start Date : </p>
                                                </div>
                                                <div class="col-md-4" style="width: 60%">
                                                    <asp:TextBox runat="server" ID="txtPlannedStartDate" class="form-control1 mn_inp control3 filter_select_control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="col-md-4 filter_label" style="width: 40%">
                                                    <p>Planned End Date : </p>
                                                </div>
                                                <div class="col-md-4" style="width: 60%">
                                                    <asp:TextBox runat="server" ID="txtPlannedEndDate" class="form-control1 mn_inp control3 filter_select_control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="clearfix"></div>
                                <div class="modal-footer">

                                    <div style="width: 30%">
                                        <asp:HiddenField ID="hdnLineProdRelId" runat="server" />
                                        <asp:HiddenField ID="hdnPlanId" runat="server" />
                                        <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
                                    </div>
                                    <div style="width: 70%">

                                        <asp:Button ID="btnDelete" Text="Delete" class="btn btn-success" runat="server" Style="display: none; margin-left: 4px; float: right" OnClick="btnDelete_Click" />
                                        <asp:Button ID="btnSave" Text="Save &  Exit" class="btn btn-success" runat="server" Style="float: right; margin-left: 4px;" OnClick="btnSave_Click" OnClientClick="return validateControls();" />
                                        <input type="button" id="btnclose" class="close_visit_entry btn btn-success" value="Close" data-dismiss="modal" style="float: right; margin-left: 4px;" />
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <%--<asp:PostBackTrigger ControlID="ddlLineNumber" />--%>
                            <asp:AsyncPostBackTrigger ControlID="ddlLineNumber" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>

            </div>
        </div>
    </div>
</asp:Content>
