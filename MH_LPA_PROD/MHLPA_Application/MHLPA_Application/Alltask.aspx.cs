﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using MHLPA_BusinessLogic;
using MHLPA_BusinessObject;
using System.Globalization;
using System.Threading;

namespace MHLPA_Application
{
    public partial class Alltask : System.Web.UI.Page
    {
        #region GlobalDeclaration

        CommonBL objComBL;
        CommonFunctions objCom = new CommonFunctions();
        PlanBO objPlanBO;
        PlanBL objPlanBL;
        DataSet dsDropDownData;
        UsersBO objUserBO;
        DashboardBO objDashBO;
        DashboardBL objDashBL;

        #endregion

        #region Events
        protected override void InitializeCulture()
        {
            string language = Convert.ToString(Session["language"]);

            //Detect User's Language.
            //if (Request.UserLanguages != null)
            //{
            //    //Set the Language.
            //    language = Request.UserLanguages[0];
            //}

            //Set the Culture.
            Thread.CurrentThread.CurrentCulture = new CultureInfo(language);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 02 Jan 2018
        /// Desc : when the page loads, all the drop down values are loaded in this event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }

                if (!IsPostBack)
                {
                    objDashBO = new DashboardBO();
                    LoadFilterDropDowns(objDashBO);
                    btnFilter_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        //protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtLineProduct = new DataTable();
        //    DataTable dtCountry = new DataTable();
        //    DataTable dtLocation = new DataTable();
        //    try
        //    {
        //        objDashBO = new DashboardBO();
        //        objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
        //        objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
        //        objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //        objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
        //        objDashBO.selection_flag = "Region";
        //        // LoadFilterDropDowns(objDashBO);


        //        objDashBL = new DashboardBL();
        //        dsDropDownData = new DataSet();
        //        dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
        //        if (dsDropDownData.Tables.Count > 0)
        //        {
        //            if (dsDropDownData.Tables[0].Rows.Count > 0)
        //            {
        //                ddlRegion.DataSource = dsDropDownData.Tables[0];
        //                ddlRegion.DataTextField = "region_name";
        //                ddlRegion.DataValueField = "region_id";
        //                ddlRegion.DataBind();
        //            }
        //            else
        //            {
        //                ddlRegion.Items.Clear();
        //                ddlRegion.DataSource = null;
        //                ddlRegion.DataBind();
        //            }
        //            if (dsDropDownData.Tables[1].Rows.Count > 0)
        //            {
        //                ddlCountry.DataSource = dsDropDownData.Tables[1];
        //                ddlCountry.DataTextField = "country_name";
        //                ddlCountry.DataValueField = "country_id";
        //                ddlCountry.DataBind();
        //                ddlCountry.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlCountry.Items.Clear();
        //                ddlCountry.DataSource = null;
        //                ddlCountry.DataBind();
        //            }
        //            if (dsDropDownData.Tables[2].Rows.Count > 0)
        //            {
        //                ddlLocation.DataSource = dsDropDownData.Tables[2];
        //                ddlLocation.DataTextField = "location_name";
        //                ddlLocation.DataValueField = "location_id";
        //                ddlLocation.DataBind();
        //                ddlLocation.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlLocation.Items.Clear();
        //                ddlLocation.DataSource = null;
        //                ddlLocation.DataBind();
        //            }
        //            if (dsDropDownData.Tables[3].Rows.Count > 0)
        //            {
        //                ddlLineName.DataSource = dsDropDownData.Tables[3];
        //                ddlLineName.DataTextField = "line_name";
        //                ddlLineName.DataValueField = "line_id";
        //                ddlLineName.DataBind();
        //                ddlLineName.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlLineName.Items.Clear();
        //                ddlLineName.DataSource = null;
        //                ddlLineName.DataBind();
        //            }
        //            btnFilter_Click(null, null);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtLineProduct = new DataTable();
        //    DataTable dtCountry = new DataTable();
        //    DataTable dtLocation = new DataTable();
        //    try
        //    {
        //        objDashBO = new DashboardBO();
        //        objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
        //        objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
        //        objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //        objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
        //        objDashBO.selection_flag = "Country";
        //        // LoadFilterDropDowns(objDashBO);

        //        objDashBL = new DashboardBL();
        //        dsDropDownData = new DataSet();
        //        dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
        //        if (dsDropDownData.Tables.Count > 0)
        //        {
        //            if (dsDropDownData.Tables[0].Rows.Count > 0)
        //            {
        //                ddlRegion.DataSource = dsDropDownData.Tables[0];
        //                ddlRegion.DataTextField = "region_name";
        //                ddlRegion.DataValueField = "region_id";
        //                ddlRegion.DataBind();
        //            }
        //            else
        //            {
        //                ddlRegion.Items.Clear();
        //                ddlRegion.DataSource = null;
        //                ddlRegion.DataBind();
        //            }
        //            if (dsDropDownData.Tables[1].Rows.Count > 0)
        //            {
        //                ddlCountry.DataSource = dsDropDownData.Tables[1];
        //                ddlCountry.DataTextField = "country_name";
        //                ddlCountry.DataValueField = "country_id";
        //                ddlCountry.DataBind();
        //            }
        //            else
        //            {
        //                ddlCountry.Items.Clear();
        //                ddlCountry.DataSource = null;
        //                ddlCountry.DataBind();
        //            }
        //            if (dsDropDownData.Tables[2].Rows.Count > 0)
        //            {
        //                ddlLocation.DataSource = dsDropDownData.Tables[2];
        //                ddlLocation.DataTextField = "location_name";
        //                ddlLocation.DataValueField = "location_id";
        //                ddlLocation.DataBind();
        //                ddlLocation.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlLocation.Items.Clear();
        //                ddlLocation.DataSource = null;
        //                ddlLocation.DataBind();
        //            }
        //            if (dsDropDownData.Tables[3].Rows.Count > 0)
        //            {
        //                ddlLineName.DataSource = dsDropDownData.Tables[3];
        //                ddlLineName.DataTextField = "line_name";
        //                ddlLineName.DataValueField = "line_id";
        //                ddlLineName.DataBind();
        //                ddlLineName.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlLineName.Items.Clear();
        //                ddlLineName.DataSource = null;
        //                ddlLineName.DataBind();
        //            }
        //        }
        //        btnFilter_Click(null, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dtLineProduct = new DataTable();
        //    try
        //    {
        //        objDashBO = new DashboardBO();
        //        objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
        //        objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
        //        objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
        //        objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
        //        objDashBO.selection_flag = "Location";
        //        // LoadFilterDropDowns(objDashBO);

        //        objDashBL = new DashboardBL();
        //        dsDropDownData = new DataSet();
        //        dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
        //        if (dsDropDownData.Tables.Count > 0)
        //        {
        //            if (dsDropDownData.Tables[0].Rows.Count > 0)
        //            {
        //                ddlRegion.DataSource = dsDropDownData.Tables[0];
        //                ddlRegion.DataTextField = "region_name";
        //                ddlRegion.DataValueField = "region_id";
        //                ddlRegion.DataBind();
        //            }
        //            else
        //            {
        //                ddlRegion.Items.Clear();
        //                ddlRegion.DataSource = null;
        //                ddlRegion.DataBind();
        //            }
        //            if (dsDropDownData.Tables[1].Rows.Count > 0)
        //            {
        //                ddlCountry.DataSource = dsDropDownData.Tables[1];
        //                ddlCountry.DataTextField = "country_name";
        //                ddlCountry.DataValueField = "country_id";
        //                ddlCountry.DataBind();
        //            }
        //            else
        //            {
        //                ddlCountry.Items.Clear();
        //                ddlCountry.DataSource = null;
        //                ddlCountry.DataBind();
        //            }
        //            if (dsDropDownData.Tables[2].Rows.Count > 0)
        //            {
        //                ddlLocation.DataSource = dsDropDownData.Tables[2];
        //                ddlLocation.DataTextField = "location_name";
        //                ddlLocation.DataValueField = "location_id";
        //                ddlLocation.DataBind();
        //            }
        //            else
        //            {
        //                ddlLocation.Items.Clear();
        //                ddlLocation.DataSource = null;
        //                ddlLocation.DataBind();
        //            }
        //            if (dsDropDownData.Tables[3].Rows.Count > 0)
        //            {
        //                ddlLineName.DataSource = dsDropDownData.Tables[3];
        //                ddlLineName.DataTextField = "line_name";
        //                ddlLineName.DataValueField = "line_id";
        //                ddlLineName.DataBind();
        //                ddlLineName.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlLineName.Items.Clear();
        //                ddlLineName.DataSource = null;
        //                ddlLineName.DataBind();
        //            }
        //        }
        //        btnFilter_Click(null, null);
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 02 Jan 2018
        /// Desc : Based on selection of Region value, country, location, line will be loaded accordingly and it will load the result in gridview.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                objDashBO = new DashboardBO();
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                //objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                //objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Region";
                // LoadFilterDropDowns(objDashBO);


                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        //ddlRegion.DataSource = dsDropDownData.Tables[0];
                        //ddlRegion.DataTextField = "region_name";
                        //ddlRegion.DataValueField = "region_id";
                        //ddlRegion.DataBind();
                        //ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        ddlRegion.SelectedValue = Convert.ToString(objDashBO.region_id);
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
                btnFilter_Click(null, null);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 02 Jan 2018
        /// Desc : Based on selection of country value, region, location, line will be loaded accordingly and it will load the result in gridview.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            DataSet dsDropDownDataCountry = new DataSet();
            DashboardBO objDashBOCountry = new DashboardBO();
            int region;
            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion.SelectedValue);
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                if (objDashBO.country_id == 0)
                {
                    objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                }
                //objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Country";
                // LoadFilterDropDowns(objDashBO);

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        //    ddlRegion.DataSource = dsDropDownData.Tables[0];
                        //    ddlRegion.DataTextField = "region_name";
                        //    ddlRegion.DataValueField = "region_id";
                        //    ddlRegion.DataBind();
                        //    ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        //    if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        //    {
                        //        ddlRegion.SelectedValue = Convert.ToString(region);
                        //    }
                        //    else
                        //    {
                        //        ddlRegion.SelectedIndex = 1;
                        //    }
                        if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        {
                            ddlRegion.SelectedValue = Convert.ToString(region);
                        }
                        else
                        {
                            ddlRegion.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                        }
                    }
                    else
                    {
                        //    ddlRegion.Items.Clear();
                        //    ddlRegion.DataSource = null;
                        //    ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        if (ddlRegion.Items.FindByValue(Convert.ToString(region)) == null || region == 0)
                        {
                            objDashBOCountry.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                            dsDropDownDataCountry = objDashBL.getFilterDataBL(objDashBOCountry);
                            if (dsDropDownDataCountry.Tables.Count > 0)
                            {
                                if (dsDropDownDataCountry.Tables[1].Rows.Count > 0)
                                {
                                    ddlCountry.DataSource = dsDropDownDataCountry.Tables[1];
                                    ddlCountry.DataTextField = "country_name";
                                    ddlCountry.DataValueField = "country_id";
                                    ddlCountry.DataBind();
                                    ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                                    ddlCountry.SelectedValue = Convert.ToString(objDashBO.country_id);
                                }
                                else
                                {
                                    ddlCountry.Items.Clear();
                                    ddlCountry.DataSource = null;
                                    ddlCountry.DataBind();
                                }
                            }
                        }
                        else
                        {
                            ddlCountry.SelectedValue = Convert.ToString(objDashBO.country_id);
                        }
                        //else
                        //{
                        //    ddlRegion.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                        //}


                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
                btnFilter_Click(null, null);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 02 Jan 2018
        /// Desc :Based on selection of location value, region, country, line will be loaded accordingly and it will load the result in gridview.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataSet dsDropDownDataCountry = new DataSet();
            DataSet dsDropDownDataLocation = new DataSet();
            DashboardBO objDashBOCountry = new DashboardBO();
            DashboardBO objDashBOlocation = new DashboardBO();
            int region, country;

            try
            {
                objDashBO = new DashboardBO();
                region = Convert.ToInt32(ddlRegion.SelectedValue);
                country = Convert.ToInt32(ddlCountry.SelectedValue);
                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                if (objDashBO.location_id == 0)
                {
                    objDashBO.country_id = country;
                    objDashBO.region_id = region;
                }
                //objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                //objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                //objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                objDashBO.selection_flag = "Location";
                // LoadFilterDropDowns(objDashBO);

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        //    ddlRegion.DataSource = dsDropDownData.Tables[0];
                        //    ddlRegion.DataTextField = "region_name";
                        //    ddlRegion.DataValueField = "region_id";
                        //    ddlRegion.DataBind();
                        //    ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        //    if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        //    {
                        //        ddlRegion.SelectedValue = Convert.ToString(region);
                        //    }
                        //    else
                        //    {
                        //        ddlRegion.SelectedIndex = 1;
                        //    }
                        if (ddlRegion.Items.FindByValue(Convert.ToString(region)) != null && region != 0)
                        {
                            ddlRegion.SelectedValue = Convert.ToString(region);
                        }
                        else
                        {
                            ddlRegion.SelectedValue = Convert.ToString(dsDropDownData.Tables[0].Rows[0]["region_id"]);
                        }
                    }
                    //else
                    //{
                    //    ddlRegion.Items.Clear();
                    //    ddlRegion.DataSource = null;
                    //    ddlRegion.DataBind();
                    //}
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        //    ddlCountry.DataSource = dsDropDownData.Tables[1];
                        //    ddlCountry.DataTextField = "country_name";
                        //    ddlCountry.DataValueField = "country_id";
                        //    ddlCountry.DataBind();
                        //    ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        //    if (ddlCountry.Items.FindByValue(Convert.ToString(country)) != null && country != 0)
                        //    {
                        //        ddlCountry.SelectedValue = Convert.ToString(country);
                        //    }
                        //    else
                        //    {
                        //        ddlCountry.SelectedIndex = 1;
                        //    }

                        objDashBOCountry.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                        dsDropDownDataCountry = objDashBL.getFilterDataBL(objDashBOCountry);
                        if (dsDropDownDataCountry.Tables.Count > 0)
                        {
                            if (dsDropDownDataCountry.Tables[1].Rows.Count > 0)
                            {
                                ddlCountry.DataSource = dsDropDownDataCountry.Tables[1];
                                ddlCountry.DataTextField = "country_name";
                                ddlCountry.DataValueField = "country_id";
                                ddlCountry.DataBind();
                                ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                                if (ddlCountry.Items.FindByValue(Convert.ToString(country)) != null && country != 0)
                                {
                                    ddlCountry.SelectedValue = Convert.ToString(country);
                                }
                                else
                                {
                                    ddlCountry.SelectedValue = Convert.ToString(dsDropDownData.Tables[1].Rows[0]["country_id"]); ;
                                }
                            }
                            else
                            {
                                ddlCountry.Items.Clear();
                                ddlCountry.DataSource = null;
                                ddlCountry.DataBind();
                            }
                        }
                    }
                    //else
                    //{
                    //    ddlCountry.Items.Clear();
                    //    ddlCountry.DataSource = null;
                    //    ddlCountry.DataBind();
                    //}
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        //ddlLocation.DataSource = dsDropDownData.Tables[2];
                        //ddlLocation.DataTextField = "location_name";
                        //ddlLocation.DataValueField = "location_id";
                        //ddlLocation.DataBind();
                        //ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        objDashBOlocation.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                        dsDropDownDataLocation = objDashBL.getFilterDataBL(objDashBOlocation);
                        if (dsDropDownDataLocation.Tables.Count > 0)
                        {
                            if (dsDropDownDataLocation.Tables[2].Rows.Count > 0)
                            {
                                ddlLocation.DataSource = dsDropDownDataLocation.Tables[2];
                                ddlLocation.DataTextField = "location_name";
                                ddlLocation.DataValueField = "location_id";
                                ddlLocation.DataBind();
                                ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                                ddlLocation.SelectedValue = Convert.ToString(objDashBO.location_id);
                            }
                            else
                            {
                                ddlLocation.Items.Clear();
                                ddlLocation.DataSource = null;
                                ddlLocation.DataBind();
                            }

                        }



                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
                btnFilter_Click(null, null);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 02 Jan 2018
        /// Desc : It calls GetAllTasks function to fetch  all tasks based on filter selection region, country,location and line
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            DataTable dtDataAlltasks;
            objPlanBO = new PlanBO();
            objPlanBL = new PlanBL();
            dtDataAlltasks = new DataTable();
            try
            {
                objPlanBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                objPlanBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                objPlanBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objPlanBO.p_line_id = Convert.ToInt32(ddlLineName.SelectedValue);

                string selectedDate = reportrange.Text;

                if (selectedDate.Contains("/"))
                {
                    string DBPattern = "dd-MM-yyyy";
                    string UIpattern = "dd/MM/yyyy";
                    DateTime startDate;
                    DateTime endDate;
                    string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    if (DateTime.TryParseExact(Convert.ToString(splittedDates[0].TrimEnd().Trim()), UIpattern, null, DateTimeStyles.None, out startDate))
                    //if (DateTime.TryParse(splittedDates[0], out startDate))
                    {
                        string frmdate = startDate.ToString(DBPattern);
                        objPlanBO.Auditfromdate = frmdate;
                        //objPlanBO.p_planned_start_date = DateTime.ParseExact(frmdate, "MM-dd-yyyy", null);
                    }
                    if (DateTime.TryParseExact(Convert.ToString(splittedDates[1].TrimStart().Trim()), UIpattern, null, DateTimeStyles.None, out endDate))
                    //if (DateTime.TryParse(splittedDates[1], out endDate))
                    {
                        string Todate = endDate.ToString(DBPattern);
                        objPlanBO.Audittodate = Todate;
                        // objPlanBO.p_planned_end_date = DateTime.ParseExact(Todate, "MM-dd-yyyy", null);
                    }
                }

                if (ddlStatus.SelectedValue == "0")
                {
                    objPlanBO.Closed_status = "2";
                }
                if (ddlStatus.SelectedValue == "1")
                {
                    objPlanBO.Closed_status = "0";
                }
                if (ddlStatus.SelectedValue == "2")
                {
                    objPlanBO.Closed_status = "1";
                }
                if (ddlStatus.SelectedValue == "3")
                {
                    objPlanBO.Closed_status = "3";
                }

                dtDataAlltasks = objPlanBL.GetAllTasks(objPlanBO);
                Session["dtDataAlltasks"] = dtDataAlltasks;
                if (dtDataAlltasks != null)
                {
                    if (dtDataAlltasks.Rows.Count > 0)
                    {
                        AllTasksGrid.DataSource = dtDataAlltasks;
                        AllTasksGrid.DataBind();
                        AllTasksGrid.Visible = true;
                    }
                    else
                    {
                        AllTasksGrid.DataSource = null;
                        AllTasksGrid.DataBind();
                        AllTasksGrid.Visible = false;
                    }

                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 02 Jan 2018
        /// Desc : On click of Clear button, it clears all the dropdown selection to default by calling LoadFilterDropDowns method and the gridview will be not visible to user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();
                LoadFilterDropDowns(objDashBO);
                AllTasksGrid.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 02 Jan 2018
        /// Desc : On selection change of Status, this event calls btnFilter_Click and load the gridview with the selected filters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnFilter_Click(null, null);
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 02 Jan 2018
        /// Desc : On selection of dates, this event calls btnFilter_Click and load the gridview with the selected filters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void reportrange_TextChanged(object sender, EventArgs e)
        {
            btnFilter_Click(null, null);
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 02 Jan 2018
        /// Desc : Based on selection of line value, region, country, location will be loaded accordingly and it will load the result in gridview.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLineName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                objDashBO = new DashboardBO();

                objDashBO.line_id = Convert.ToInt32(ddlLineName.SelectedValue);
                if (objDashBO.line_id == 0)
                {
                    objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                    objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                    objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                }
                objDashBO.selection_flag = "Line";
                // LoadFilterDropDowns(objDashBO);

                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        ddlRegion.SelectedIndex = 1;
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        ddlCountry.SelectedIndex = 1;
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        ddlLocation.SelectedIndex = 1;
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                        ddlLineName.SelectedValue = Convert.ToString(objDashBO.line_id);
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
                btnFilter_Click(null, null);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 02 Jan 2018
        /// Desc : For every task, there can be image for audit and review. This event is call to bind the image panel with image path 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AllTasksGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Panel pnlImages = e.Row.FindControl("pnlImages") as Panel;
                    if (Session["dtDataAlltasks"] != null)
                    {
                        DataTable dt = new DataTable();
                        dt = (DataTable)Session["dtDataAlltasks"];
                        if (!String.IsNullOrEmpty(Convert.ToString(dt.Rows[e.Row.RowIndex]["image_file_name"])))
                        {
                            string[] imageArray = Convert.ToString(dt.Rows[e.Row.RowIndex]["image_file_name"]).Split(';');
                            for (int i = 0; i < imageArray.Length; i++)
                            {
                                Image img = new Image();
                                img.ID = "img" + i;
                                img.ImageUrl = Convert.ToString(imageArray[i]);
                                img.Width = 50;
                                img.Height = 50;
                                img.Attributes["style"] = "cursor: pointer";
                                img.Attributes["onclick"] = "myFunction(this);";
                                img.Attributes["data-toggle"] = "modal";
                                img.Attributes["data-target"] = "#myModal1";
                                pnlImages.Controls.Add(img);
                            }
                        }
                    }

                    Panel pnlRevImages = e.Row.FindControl("pnlRevImages") as Panel;
                    if (Session["dtDataAlltasks"] != null)
                    {
                        DataTable dt = new DataTable();
                        dt = (DataTable)Session["dtDataAlltasks"];
                        if (!String.IsNullOrEmpty(Convert.ToString(dt.Rows[e.Row.RowIndex]["review_image_file_name"])))
                        {
                            string[] imageArray = Convert.ToString(dt.Rows[e.Row.RowIndex]["review_image_file_name"]).Split(';');
                            for (int i = 0; i < imageArray.Length; i++)
                            {
                                Image img = new Image();
                                img.ID = "img" + i;
                                img.ImageUrl = Convert.ToString(imageArray[i]);
                                img.Width = 50;
                                img.Height = 50;
                                img.Attributes["style"] = "cursor: pointer";
                                img.Attributes["onclick"] = "myFunction(this);";
                                img.Attributes["data-toggle"] = "modal";
                                img.Attributes["data-target"] = "#myModal1";
                                pnlRevImages.Controls.Add(img);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 02 Jan 2018
        /// Desc : It calls GetAllMasterBL method with userid as parameter from CommonFunctionBL class to fetch region.country, location and line data and it bind to respective dropdowns.(Not In use as requirement might get changed)
        /// </summary>
        private void GetMasterDropdown()
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);

                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        //ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                        ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ddlRegion.SelectedValue)))
                {
                    objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                    dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                    if (dtCountry.Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dtCountry;
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        //ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                        ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ddlCountry.SelectedValue)))
                {
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                    dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                    if (dtLocation.Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dtLocation;
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        //ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ddlLocation.SelectedValue)))
                {
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLineProduct.Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dtLineProduct;
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                        //BindPart();
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
                else
                {
                    ddlLineName.Items.Clear();
                    ddlLineName.DataSource = null;
                    ddlLineName.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 02 Jan 2018
        /// Desc : It calls getFilterDataBLfrom DashboardBL class, it binds all the dropdown on screen and be default loaded the user's location which are stored in session.
        /// </summary>
        /// <param name="objDashBO"></param>
        private void LoadFilterDropDowns(DashboardBO objDashBO)
        {
            try
            {
                objDashBL = new DashboardBL();
                dsDropDownData = new DataSet();
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.Items.Insert(0, new ListItem("All", "0"));
                        //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                        ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                        //    ddlRegion.Attributes["disabled"] = "disabled";
                        //}
                        //else
                        //    ddlRegion.Enabled = true;
                    }
                    else
                    {
                        ddlRegion.Items.Clear();
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    //if (dsDropDownData.Tables[1].Rows.Count > 0)
                    //{
                    //    ddlCountry.DataSource = dsDropDownData.Tables[1];
                    //    ddlCountry.DataTextField = "country_name";
                    //    ddlCountry.DataValueField = "country_id";
                    //    ddlCountry.DataBind();
                    //    ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                    //    //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                    //    //{
                    //        ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                    //        //ddlCountry.Attributes["disabled"] = "disabled";
                    //    //}
                    //   // else
                    //     //   ddlCountry.Enabled = true;
                    //}
                    //else
                    //{
                    //    ddlCountry.Items.Clear();
                    //    ddlCountry.DataSource = null;
                    //    ddlCountry.DataBind();
                    //}
                    //if (dsDropDownData.Tables[2].Rows.Count > 0)
                    //{
                    //    ddlLocation.DataSource = dsDropDownData.Tables[2];
                    //    ddlLocation.DataTextField = "location_name";
                    //    ddlLocation.DataValueField = "location_id";
                    //    ddlLocation.DataBind();
                    //    ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                    //    //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                    //    //{
                    //        ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                    //       // ddlLocation.Attributes["disabled"] = "disabled";
                    //   // }
                    //   //// else
                    //      //  ddlLocation.Enabled = true;
                    //}
                    //else
                    //{
                    //    ddlLocation.Items.Clear();
                    //    ddlLocation.DataSource = null;
                    //    ddlLocation.DataBind();
                    //}
                    //if (dsDropDownData.Tables[3].Rows.Count > 0)
                    //{
                    //    ddlLineName.DataSource = dsDropDownData.Tables[3];
                    //    ddlLineName.DataTextField = "line_name";
                    //    ddlLineName.DataValueField = "line_id";
                    //    ddlLineName.DataBind();
                    //    ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    //    //if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                    //    //{
                    //    //ddlLineName_SelectedIndexChanged(null, null);
                    //    //}
                    //}
                    //else
                    //{
                    //    ddlLineName.Items.Clear();
                    //    ddlLineName.DataSource = null;
                    //    ddlLineName.DataBind();
                    //}
                    //ddlRegion_SelectedIndexChanged(null, null);
                    //ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                    //ddlCountry_SelectedIndexChanged(null, null);
                    //ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                    //ddlLocation_SelectedIndexChanged(null, null);


                }
                objDashBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.Items.Insert(0, new ListItem("All", "0"));
                        ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                }
                objDashBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.Items.Insert(0, new ListItem("All", "0"));
                        ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                }

                objDashBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
                if (dsDropDownData.Tables.Count > 0)
                {

                    if (dsDropDownData.Tables[3].Rows.Count > 0)
                    {
                        ddlLineName.DataSource = dsDropDownData.Tables[3];
                        ddlLineName.DataTextField = "line_name";
                        ddlLineName.DataValueField = "line_id";
                        ddlLineName.DataBind();
                        ddlLineName.Items.Insert(0, new ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLineName.Items.Clear();
                        ddlLineName.DataSource = null;
                        ddlLineName.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        //private void LoadFilterDropDowns(DashboardBO objDashBO)
        //{
        //    try
        //    {
        //        objDashBL = new DashboardBL();
        //        dsDropDownData = new DataSet();
        //        dsDropDownData = objDashBL.getFilterDataBL(objDashBO);
        //        if (dsDropDownData.Tables.Count > 0)
        //        {
        //            if (dsDropDownData.Tables[0].Rows.Count > 0)
        //            {
        //                ddlRegion.DataSource = dsDropDownData.Tables[0];
        //                ddlRegion.DataTextField = "region_name";
        //                ddlRegion.DataValueField = "region_id";
        //                ddlRegion.DataBind();
        //                ddlRegion.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlRegion.Items.Clear();
        //                ddlRegion.DataSource = null;
        //                ddlRegion.DataBind();
        //            }
        //            if (dsDropDownData.Tables[1].Rows.Count > 0)
        //            {
        //                ddlCountry.DataSource = dsDropDownData.Tables[1];
        //                ddlCountry.DataTextField = "country_name";
        //                ddlCountry.DataValueField = "country_id";
        //                ddlCountry.DataBind();
        //                ddlCountry.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlCountry.Items.Clear();
        //                ddlCountry.DataSource = null;
        //                ddlCountry.DataBind();
        //            }
        //            if (dsDropDownData.Tables[2].Rows.Count > 0)
        //            {
        //                ddlLocation.DataSource = dsDropDownData.Tables[2];
        //                ddlLocation.DataTextField = "location_name";
        //                ddlLocation.DataValueField = "location_id";
        //                ddlLocation.DataBind();
        //                ddlLocation.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlLocation.Items.Clear();
        //                ddlLocation.DataSource = null;
        //                ddlLocation.DataBind();
        //            }
        //            if (dsDropDownData.Tables[3].Rows.Count > 0)
        //            {
        //                ddlLineName.DataSource = dsDropDownData.Tables[3];
        //                ddlLineName.DataTextField = "line_name";
        //                ddlLineName.DataValueField = "line_id";
        //                ddlLineName.DataBind();
        //                ddlLineName.Items.Insert(0, new ListItem("All", "0"));
        //            }
        //            else
        //            {
        //                ddlLineName.Items.Clear();
        //                ddlLineName.DataSource = null;
        //                ddlLineName.DataBind();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        #endregion
    }
}