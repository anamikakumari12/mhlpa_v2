﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BulkQRDetails.aspx.cs" Inherits="MHLPA_Application.BulkQRDetails" MasterPageFile="~/Site_Master.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function PrintPanel() {
            debugger;
            var panel = document.getElementById("<%=pnlPrintBulkQRDetails.ClientID%>");

            panel.style.visibility = "visible";
            //PageMethods.SetDownloadPath("yes");
            window.print();

        }

    </script>
    <%--    <style media="print">
        .mn_vertical
        {
            float: left;
            width: 100%;
            border: none;
        }

        .feedback-title
        {
            /*-webkit-transform: rotate(-90deg);
                -moz-transform: rotate(-90deg);
                -ms-transform: rotate(-90deg);
                -o-transform: rotate(-90deg);
                transform: rotate(-90deg);
                position: absolute;
                margin: 0;
            width: 146px;
            height: 100px;
            font-size: 10px;*/
            margin: 20px auto;
            width: 150px;
            height: 30px;
            font-size: 10px;
            display: block;
            text-align: center;
        }

        .mn_vertical .feedback-list
        {
            float: left;
        }

        .mn_feedback
        {
            width: 100%;
            float: left;
            padding: 25px 50px;
        }

        .feedback-list p
        {
            text-align: center;
        }

        .colQR1
        {
            width: 50%;
            float: left;
            padding: 15px;
            border-style: groove;
        }

        .mn_location_span
        {
            display: block;
            text-align: center;
            margin: 0 0 10px 0;
        }

        .colQR1 img
        {
            margin: 10px auto;
            display: block;
        }

        .qr-title
        {
            margin: 0 auto;
            font-size: 16px;
            font-weight: bold;
            display: block;
            text-align: center;
        }

        .qr-title1
        {
            margin: 0 auto;
            font-size: 16px;
            font-weight: bold;
            display: block;
            text-align: center;
        }


        .mn_vertical
        {
            float: left;
            width: 100%;
            border: none;
        }

        .mnhu_floright
        {
            float: right;
        }

        .feedback-title
        {
            /*-webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            transform: rotate(-90deg);
            position: absolute;
            margin: 0;
            width: 146px;
            height: 100px;
            font-size: 10px;*/
            margin: 20px auto;
            width: 150px;
            height: 30px;
            display: block;
            font-size: 10px;
            text-align: center;
        }

        .mn_vertical .feedback-list
        {
            float: left;
        }

        .mn_feedback
        {
            width: 100%;
            float: left;
            padding: 25px 50px;
        }

        .feedback-list p
        {
            text-align: center;
        }

        .colQR1
        {
            width: 50%;
            float: left;
            padding: 15px;
            border-style: groove;
        }

        .mn_location_span
        {
            display: block;
            text-align: center;
            margin: 0 0 10px 0;
        }

        .colQR1 img
        {
            margin: 10px auto;
            display: block;
        }

        .qr-title
        {
            margin: 0 auto;
            display: block;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
        }

        .qr-title1
        {
            margin: 0 auto;
            display: block;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
        }

        .row
        {
            visibility: hidden;
            height: 1px;
        }

        .filter_panel
        {
            visibility: hidden;
        }

        .col-md-4 filter_label
        {
            visibility: hidden;
        }
        #MainContent_pnlBulkQRDetails, .divBulkQR
        {
             width:100%;
             float:left
        }
        .divBulkQR table
        {
            vertical-align: top;
        }
    </style>--%>
    <style>
        @media print
        {
            .col-md-8
            {
                visibility: hidden;
            }

            .col-md-4
            {
                visibility: hidden;
            }

            .btn-add
            {
                visibility: hidden;
            }

            .col-md-12
            {
                visibility: hidden;
            }

            hr
            {
                visibility: hidden;
            }

            .divPrint
            {
                visibility: visible;
                height: 100%;
            }

            .row
            {
                visibility: hidden;
            }

            .divBulkQR
            {
                visibility: hidden;
            }
        }

        .mn_vertical
        {
            float: left;
            width: 100%;
            border: none;
        }

        .feedback-title
        {
            /*-webkit-transform: rotate(-90deg);
                -moz-transform: rotate(-90deg);
                -ms-transform: rotate(-90deg);
                -o-transform: rotate(-90deg);
                transform: rotate(-90deg);
                position: absolute;
                margin: 0;
            width: 146px;
            height: 100px;
            font-size: 10px;*/
            margin: 20px auto;
            width: 150px;
            height: 30px;
            display: block;
            font-size: 10px;
            text-align: center;
        }

        .mn_vertical .feedback-list
        {
            float: left;
        }

        .mn_feedback
        {
            width: 100%;
            float: left;
            padding: 25px 50px;
        }

        .feedback-list p
        {
            text-align: center;
        }

        .colQR1
        {
            width: 50%;
            float: left;
            padding: 15px;
            border-style: groove;
            height: 33%;
        }

        .mn_location_span
        {
            display: block;
            text-align: center;
            margin: 0 0 10px 0;
        }

        .colQR1 img
        {
            margin: 10px auto;
            display: block;
        }

        .qr-title
        {
            margin: 0 auto;
            display: block;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
        }

        .qr-title1
        {
            margin: 0 auto;
            display: block;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
        }


        .mn_vertical
        {
            float: left;
            width: 100%;
            border: none;
        }

        .feedback-title
        {
            /*-webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            transform: rotate(-90deg);
            position: absolute;
            margin: 0;
            width: 146px;
            height: 100px;
            font-size: 10px;*/
            margin: 20px auto 0;
            width: 150px;
            height: 30px;
            font-size: 10px;
            display: block;
            text-align: center;
        }

        .mn_vertical .feedback-list
        {
            float: left;
            width: 100%;
        }

        .mn_feedback
        {
            width: 100%;
            float: left;
            padding: 25px 50px;
        }

        .feedback-list p
        {
            text-align: center;
        }

        .colQR1
        {
            width: 50%;
            float: left;
            padding: 15px;
            border-style: groove;
            height: 33%;
        }

        .mn_location_span
        {
            display: block;
            text-align: center;
            margin: 0 0 10px 0;
        }

        .colQR1 img
        {
            margin: 0px auto;
            display: block;
        }

        .qr-title
        {
            margin: 0 auto;
            display: block;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
        }

        .qr-title1
        {
            margin: 0 auto;
            display: block;
            text-align: center;
            font-size: 16px;
            font-weight: bold;
        }

        pnlPrintBulkQRDetails
        {
            visibility: hidden;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <ajaxToolkit:ToolkitScriptManager EnablePageMethods="true" ID="ToolkitScriptManager1" runat="server" EnablePartialRendering="true">
    </ajaxToolkit:ToolkitScriptManager>
    <div class="divPrint" style="height: 1px;">
        <asp:Panel runat="server" ID="pnlPrintBulkQRDetails" Style="visibility: hidden;" Visible="true">
            <div class="divPrintBulkQR" id="divPrintBulkQR">
                <asp:Table runat="server" Style="width: 100%" ID="tblPrintBulkQR">
                    <asp:TableRow>
                        <asp:TableCell>
                            <div class="mn_vertical">
                                <div class="mn_feedback">
                                    <div class="feedback-list">
                                        <asp:Panel ID="pnlPrintBulkQR1" runat="server">
                                        </asp:Panel>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>

                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>

            </div>
        </asp:Panel>
    </div>
    <div class="row">
        <div>
            <div class="clearfix"></div>
            <div class="filter_panel">
                <div>
                    <div class="col-md-4 filter_label" style="width: 14%">
                        <p>Region* : </p>
                    </div>
                    <div class="col-md-4" style="width: 20%">
                        <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="col-md-4 filter_label" style="width: 13%">
                        <p>Country* : </p>
                    </div>
                    <div class="col-md-4" style="width: 20%">
                        <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="col-md-4 filter_label" style="width: 13%">
                        <p>Location* : </p>
                    </div>
                    <div class="col-md-4" style="width: 20%">
                        <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>

                </div>
                <div>
                    <div class="col-md-4 filter_label" style="width: 14%">
                        <p>Line Name/No* : </p>
                    </div>
                    <div class="col-md-4" style="width: 20%">
                        <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLine" OnSelectedIndexChanged="ddlLine_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>

                    <div>
                        <div class="col-md-4" style="width: 66%">
                            <asp:Button runat="server" CssClass="btn-add" ID="download" Text="Download QRCodes For Print" OnClick="download_Click" />
                            <asp:Button runat="server" CssClass="btn-add" ID="btnPrint" Text="Print QRCodes" OnClientClick="PrintPanel();" Visible="false" />
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <asp:Panel runat="server" ID="pnlBulkQRDetails">
        <div class="divBulkQR" id="divBulkQR">
            <asp:Table runat="server" Style="width: 100%" ID="tblBulkQR">
                <asp:TableRow>
                    <asp:TableCell>
                        <div class="mn_vertical">
                            <%-- <div id="heading" style="margin: 0 0 24px;">
                            <p>Layered Process Audit QR Code</p>
                        </div>--%>
                            <div class="mn_feedback">
                                <div class="feedback-list">
                                    <asp:Panel ID="pnlBulkQR1" runat="server">
                                        <%-- <div id="divQR1" runat="server"></div>--%>
                                    </asp:Panel>
                                    <div class="clearfix"></div>
                                </div>
                                <%--<div>
                                <img style="height: 30px; width: 40px;" src="images/logo-neu_1.png" />
                            </div>--%>
                            </div>
                        </div>

                    </asp:TableCell>
                    <%-- <asp:TableCell>
                    <div class="mn_vertical">
                        <div id="Div1">
                            <p>Layered Process Audit QR Code</p>
                        </div>
                        <div class="mn_feedback">
                            <div class="feedback-list">
                                <asp:Panel ID="pnlBulkQR2" runat="server"></asp:Panel>
                            </div>
                            <div>
                                <img style="height: 30px; width: 50px;" src="images/logo-neu_1.png" />
                            </div>
                        </div>
                    </div>

                </asp:TableCell>
                <asp:TableCell>
                    <div class="mn_vertical">
                        <div id="Div3">
                            <p>Layered Process Audit QR Code</p>
                        </div>
                        <div class="mn_feedback">
                            <div class="feedback-list">
                                <asp:Panel ID="pnlBulkQR3" runat="server"></asp:Panel>
                            </div>
                            <div>
                                <img style="height: 30px; width: 50px;" src="images/logo-neu_1.png" />
                            </div>
                        </div>
                    </div>
                </asp:TableCell>--%>
                </asp:TableRow>
            </asp:Table>

        </div>
    </asp:Panel>
</asp:Content>
