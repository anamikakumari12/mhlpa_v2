﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MHLPA_BusinessLogic;
using MHLPA_BusinessObject;

namespace MHLPA_Application
{
    public partial class Part : System.Web.UI.Page
    {
        #region Global Declaration
        LocationBO objLocBO;
        LocationBL objLocBL;
        DataSet dsDropDownData;
        UsersBO objUserBO;
        CommonBL objComBL;
        CommonFunctions objCom = new CommonFunctions();
        #endregion

        #region Events
        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: Check for login session and load the login page if session timeout, if not, load all the part details in a grid and the view panel with first row data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int UserId;
                int vwlinprod_id;
                if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }
                else
                {
                    UserId = Convert.ToInt32(Session["UserId"]);
                }
                if (!Page.IsPostBack)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
                    {
                        GridBind();
                        DataTable dtGrid = new DataTable();
                        dtGrid = (DataTable)Session["dtPart"];

                        vwlinprod_id = Convert.ToInt32(dtGrid.Rows[0]["line_id"]);
                        Session["line_id"] = vwlinprod_id;
                        DataRow drfirst = selectedRow(vwlinprod_id);
                        LoadViewpanel(drfirst);
                        Session["PrintFlag"] = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of Add button, it will clear all the field from edit panel and display Add panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAdd_Click(object sender, EventArgs e)
        {

            try
            {
                Clear();
                GetMasterDropDown();
                Session["SaveFlag"] = "add";
                editpanel.Visible = true;
                viewpanel.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }


        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of Save button, it will call the SavePartsBL with all the details in edit panel to save to database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            LocationBO Output = new LocationBO();
            DataTable dtOutput = new DataTable();
            string flag=string.Empty;
            try
            {
                objLocBO = new LocationBO();
                objLocBL = new LocationBL();
                objLocBO.UserId = Convert.ToInt32(Session["UserId"]);
                if (!string.IsNullOrEmpty(Convert.ToString(Session["line_id"])))
                    objLocBO.line_product_rel_id = Convert.ToInt32(Session["line_id"]);
                objLocBO.region_name = Convert.ToString(ddlRegion.SelectedItem);
                objLocBO.country_name = Convert.ToString(ddlCountry.SelectedItem);
                objLocBO.location_name = Convert.ToString(ddlLocation.SelectedItem);
                //objLocBO.line_number = Convert.ToString(ddlLineNumber.SelectedItem);
                objLocBO.line_name = Convert.ToString(ddlLineName.SelectedItem);
                //objLocBO.part_name = Convert.ToString(txtPartName.Text);
                objLocBO.part_number = Convert.ToString(txtPartNumber.Text);
                objLocBO.startdate = null;
                objLocBO.enddate = null;
                DataTable dtSave = new DataTable();
                dtSave.Columns.Add("Region");
                dtSave.Columns.Add("Country");
                dtSave.Columns.Add("Location");
                dtSave.Columns.Add("LineName");
                dtSave.Columns.Add("PartNumber");
                DataRow dr= dtSave.NewRow();
                dr["Region"] = objLocBO.region_name;
                dr["Country"] = objLocBO.country_name;
                dr["Location"] = objLocBO.location_name;
                dr["LineName"] = objLocBO.line_name;
                dr["PartNumber"] = objLocBO.part_number;
                dtSave.Rows.Add(dr);
                flag=Convert.ToString(Session["SaveFlag"]);
                dtOutput = objLocBL.SavePartsBL(dtSave, flag);
                
                if (dtOutput != null)
                {
                    if (dtOutput.Rows.Count > 0)
                    {
                        Output.error_code = Convert.ToInt32(dtOutput.Rows[0][0]);
                        Output.error_msg = Convert.ToString(dtOutput.Rows[0][1]);
                        if (Output.error_code == 0)
                        {
                            if(objLocBO.line_product_rel_id==0)
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('A new part is added successfully.');", true);
                            else
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Part is modified successfully.');", true);
                            GridBind();
                            DataTable dtGrid = new DataTable();
                            dtGrid = (DataTable)Session["dtPart"];
                            DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["line_id"]));
                            LoadViewpanel(drfirst);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + Output.error_msg + "');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('There is error in saving parts. Please try again.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('There is error in saving parts. Please try again.');", true);
                }
                //Output = objLocBL.SaveLineProductBL(objLocBO);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        //protected void btnDelete_Click(object sender, EventArgs e)
        //{
        //    LocationBO Output = new LocationBO();
        //    try
        //    {
        //        objLocBO = new LocationBO();
        //        objLocBL = new LocationBL();
        //        objLocBO.UserId = Convert.ToInt32(Session["UserId"]);
        //        if (!string.IsNullOrEmpty(Convert.ToString(Session["line_id"])))
        //            objLocBO.line_id = Convert.ToInt32(Session["line_id"]);
        //        objLocBO.region_name = Convert.ToString(ddlRegion.SelectedItem);
        //        objLocBO.country_name = Convert.ToString(ddlCountry.SelectedItem);
        //        objLocBO.location_name = Convert.ToString(ddlLocation.SelectedItem);
        //        //objLocBO.line_number = Convert.ToString(ddlLineNumber.SelectedItem);
        //        objLocBO.line_name = Convert.ToString(ddlLineName.SelectedItem);
        //        //objLocBO.part_name = Convert.ToString(txtPartName.Text);
        //        objLocBO.part_number = Convert.ToString(txtPartNumber.Text);
        //        objLocBO.startdate = null;
        //        objLocBO.enddate = DateTime.Now;
        //        Output = objLocBL.SaveLineProductBL(objLocBO);

        //        if (Output.error_code == 0)
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + Output.error_msg + "');", true);
        //            GridBind();
        //            DataTable dtGrid = new DataTable();
        //            dtGrid = (DataTable)Session["dtPart"];
        //            DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["line_id"]));
        //            LoadViewpanel(drfirst);
        //        }
        //        else
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + Output.error_msg + "');", true);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of any row in gridview, the selected row details will be loaded in view panel by calling LoadViewpanel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Select(object sender, EventArgs e)
        {
            try
            {
                int line_id = Convert.ToInt32((sender as LinkButton).CommandArgument);
                Session["line_id"] = line_id;
                DataRow drselect = selectedRow(line_id);
                LoadViewpanel(drselect);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc:On click of any row in gridview, the selected row details will be loaded in edit panel by calling LoadEditPanel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Edit(object sender, EventArgs e)
        {
            try
            {
                int line_id = Convert.ToInt32((sender as ImageButton).CommandArgument);
                Session["line_id"] = line_id;
                DataRow drselect = selectedRow(line_id);
                LoadEditPanel(drselect);
                Session["SaveFlag"] = "edit";
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of Delete button, it will call the DeletePartBL with all the details in edit panel to delete from database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete(object sender, EventArgs e)
        {
            try
            {
                objLocBO = new LocationBO();
                objLocBL = new LocationBL();
                int line_id = Convert.ToInt32((sender as ImageButton).CommandArgument);
                objLocBO.line_id=line_id;
                objLocBO = objLocBL.DeletePartBL(objLocBO);
                if (objLocBO.error_code == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + objLocBO.error_msg + "');", true);
                    GridBind();
                    DataTable dtGrid = new DataTable();
                    dtGrid = (DataTable)Session["dtPart"];
                    DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["line_id"]));
                    LoadViewpanel(drfirst);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('There is error in delete part. Please try again.');", true);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On selection of region, the country, location and line drop down will be loaded accordingly by calling GetCountryBasedOnRegionBL,GetLocationBasedOnCountryBL and GetLineListDropDownBL respectively.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                if (dtCountry.Rows.Count > 0)
                {
                    ddlCountry.DataSource = dtCountry;
                    ddlCountry.DataTextField = "country_name";
                    ddlCountry.DataValueField = "country_id";
                    ddlCountry.DataBind();
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }

                dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlLocation.DataSource = dtLocation;
                    ddlLocation.DataTextField = "location_name";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }

                dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
                if (dtLineProduct.Rows.Count > 0)
                {
                    ddlLineName.DataSource = dtLineProduct;
                    ddlLineName.DataTextField = "line_name";
                    ddlLineName.DataValueField = "line_id";
                    ddlLineName.DataBind();
                    Session["dtLineProduct"] = dtLineProduct;
                    //BindPart();
                }
                else
                {
                    ddlLineName.Items.Clear();
                    ddlLineName.DataSource = null;
                    ddlLineName.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// 
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On selection of country, the location and line drop down will be loaded accordingly by calling GetLocationBasedOnCountryBL and GetLineListDropDownBL respectively.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlLocation.DataSource = dtLocation;
                    ddlLocation.DataTextField = "location_name";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }
                objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
                if (dtLineProduct.Rows.Count > 0)
                {
                    ddlLineName.DataSource = dtLineProduct;
                    ddlLineName.DataTextField = "line_name";
                    ddlLineName.DataValueField = "line_id";
                    ddlLineName.DataBind();
                    Session["dtLineProduct"] = dtLineProduct;
                    //BindPart();
                }
                else
                {
                    ddlLineName.Items.Clear();
                    ddlLineName.DataSource = null;
                    ddlLineName.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// 
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On selection of location, the line drop down will be loaded accordingly by calling  GetLineListDropDownBL.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLineProduct = new DataTable();
            try
            {
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                dtLineProduct = objComBL.GetLineListDropDownBL(objUserBO);
                if (dtLineProduct.Rows.Count > 0)
                {
                    ddlLineName.DataSource = dtLineProduct;
                    ddlLineName.DataTextField = "line_name";
                    ddlLineName.DataValueField = "line_id";
                    ddlLineName.DataBind();

                    //ddlLineNumber.DataSource = dtLineProduct;
                    //ddlLineNumber.DataTextField = "line_code";
                    //ddlLineNumber.DataValueField = "line_id";
                    //ddlLineNumber.DataBind();
                    Session["dtLineProduct"] = dtLineProduct;
                    //BindPart();
                }
                else
                {
                    ddlLineName.Items.Clear();
                    ddlLineName.DataSource = null;
                    ddlLineName.DataBind();
                    //ddlLineNumber.Items.Clear();
                    //ddlLineNumber.DataSource = null;
                    //ddlLineNumber.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        //protected void ddlLineName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        //string line_id = Convert.ToString(ddlLineName.SelectedValue);
        //        //ddlLineNumber.SelectedValue = line_id;
        //        BindPart();
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}
        
        //protected void ddlPartNumber_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        txtPartNumber.Text = Convert.ToString(ddlPartNumber.SelectedItem);
        //        PopupControlExtender1.Commit(Convert.ToString(ddlPartNumber.SelectedItem));
        //        //ddlPartName.SelectedValue = Convert.ToString(ddlPartNumber.SelectedValue);
        //        //txtPartName.Text = Convert.ToString(ddlPartName.SelectedItem);
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        //protected void ddlPartName_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        txtPartName.Text = Convert.ToString(ddlPartName.SelectedItem);
        //        PopupControlExtender4.Commit(Convert.ToString(ddlPartName.SelectedItem));
        //        ddlPartNumber.SelectedValue = Convert.ToString(ddlPartName.SelectedValue);
        //        txtPartNumber.Text = Convert.ToString(ddlPartNumber.SelectedItem);
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of pages in gridview, the selected page will be loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdLineProduct_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdLineProduct.PageIndex = e.NewPageIndex;
                grdLineProduct.DataSource = (DataTable)Session["FilteredData"];
                grdLineProduct.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : On click of Excel upload button, it will save the file in Report folder and call InsertExcelRecords function for loading the data from excel to datable and saving to database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExcel_Click(object sender, EventArgs e)
        {
            if (updFile.PostedFile != null && updFile.PostedFile.ContentLength > 0)
            {

                string fileName = Path.GetFileName(updFile.PostedFile.FileName);
                string folder = Server.MapPath("~/Reports/");
                Directory.CreateDirectory(folder);
                string CurrentFilePath = Path.Combine(folder, fileName);
                updFile.PostedFile.SaveAs(Path.Combine(folder, fileName));
                try
                {
                    InsertExcelRecords(CurrentFilePath);
                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : all the drop downs data in header will be loaded according to the gridview data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdLineProduct_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataTable dtData;
            DropDownList ddlFilter;
            DataView view;
            DataTable distinctValues;
            try
            {

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    ddlFilter = e.Row.FindControl("ddlRegion_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["dtOriginalPart"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalPart"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "region_name");
                        DataView dv = distinctValues.DefaultView;
                        dv.Sort = "region_name asc";
                        distinctValues = dv.ToTable();
                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "region_name";
                        ddlFilter.DataValueField = "region_name";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedRegion_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedRegion_grid"]);
                        }
                    }
                    ddlFilter = e.Row.FindControl("ddlCountry_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["FilteredRegionData"] != null)
                        {
                            dtData = (DataTable)Session["FilteredRegionData"];
                        }
                        else if (Session["dtOriginalPart"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalPart"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "country_name");
                        DataView dv = distinctValues.DefaultView;
                        dv.Sort = "country_name asc";
                        distinctValues = dv.ToTable();
                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "country_name";
                        ddlFilter.DataValueField = "country_name";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedCountry_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedCountry_grid"]);
                        }
                        if (ddlFilter.SelectedValue == "0" || ddlFilter.SelectedValue == "")
                        {
                            Session["FilteredCountryData"] = Session["FilteredRegionData"];
                        }
                    }
                    ddlFilter = e.Row.FindControl("ddlLocation_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["FilteredCountryData"] != null)
                        {
                            dtData = (DataTable)Session["FilteredCountryData"];
                        }
                        else if (Session["FilteredRegionData"] != null)
                        {
                            dtData = (DataTable)Session["FilteredRegionData"];
                        }
                        else if (Session["dtOriginalPart"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalPart"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "location_name");
                        DataView dv = distinctValues.DefaultView;
                        dv.Sort = "location_name asc";
                        distinctValues = dv.ToTable();
                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "location_name";
                        ddlFilter.DataValueField = "location_name";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedLocation_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedLocation_grid"]);
                        }
                    }
                    ddlFilter = e.Row.FindControl("ddlLine_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["FilteredLocationData"] != null)
                        {
                            dtData = (DataTable)Session["FilteredLocationData"];
                        }
                        else if (Session["FilteredCountryData"] != null)
                        {
                            dtData = (DataTable)Session["FilteredCountryData"];
                        }
                        else if (Session["FilteredRegionData"] != null)
                        {
                            dtData = (DataTable)Session["FilteredRegionData"];
                        }
                        else if (Session["dtOriginalPart"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalPart"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "line_name");
                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "line_name";
                        ddlFilter.DataValueField = "line_name";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedLine_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedLine_grid"]);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On selection of region dropdown in header of gridview, the data will be loaded according to selection and saved the data in Session["FilteredData"]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlRegion_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();
                if (Session["dtPart"] != null)
                {
                    dtData = (DataTable)Session["dtPart"];
                }
                DropDownList ddlCountry = (DropDownList)sender;
                Session["ddlSelectedRegion_grid"] = Convert.ToString(ddlCountry.SelectedValue);
                Session["ddlSelectedLocation_grid"] = "All";
                Session["ddlSelectedCountry_grid"] = "All";
                Session["ddlSelectedLine_grid"] = "All";
                if (Convert.ToString(ddlCountry.SelectedValue) == "0")
                {
                    Session["FilteredData"] = dtData;
                    Session["FilteredRegionData"] = dtData;
                    grdLineProduct.DataSource = dtData;
                    grdLineProduct.DataBind();
                }
                else
                {
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("region_name") == Convert.ToString(ddlCountry.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    Session["FilteredRegionData"] = dtFilterData;
                    grdLineProduct.DataSource = dtFilterData;
                    grdLineProduct.DataBind();
                }

                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["line_id"]));
                LoadViewpanel(drfirst);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On selection of country dropdown in header of gridview, the data will be loaded according to selection and saved the data in Session["FilteredData"]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCountry_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();

                if (Session["FilteredRegionData"] != null)
                {
                    dtData = (DataTable)Session["FilteredRegionData"];
                }
                else if (Session["dtPart"] != null)
                {
                    dtData = (DataTable)Session["dtPart"];
                }
                DropDownList ddlCountry = (DropDownList)sender;
                Session["ddlSelectedCountry_grid"] = Convert.ToString(ddlCountry.SelectedValue);
                Session["ddlSelectedLocation_grid"] = "All";
                Session["ddlSelectedLine_grid"] = "All";
                if (Convert.ToString(ddlCountry.SelectedValue) == "0")
                {
                    Session["FilteredData"] = dtData;
                    Session["FilteredCountryData"] = dtData;
                    grdLineProduct.DataSource = dtData;
                    grdLineProduct.DataBind();
                }
                else
                {
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("country_name") == Convert.ToString(ddlCountry.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    Session["FilteredCountryData"] = dtFilterData;
                    grdLineProduct.DataSource = dtFilterData;
                    grdLineProduct.DataBind();
                }

                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["line_id"]));
                LoadViewpanel(drfirst);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On selection of location dropdown in header of gridview, the data will be loaded according to selection and saved the data in Session["FilteredData"]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLocation_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();

                if (Session["FilteredCountryData"] != null)
                {
                    dtData = (DataTable)Session["FilteredCountryData"];
                }
                else if (Session["FilteredRegionData"] != null)
                {
                    dtData = (DataTable)Session["FilteredRegionData"];
                }
                else if (Session["dtPart"] != null)
                {
                    dtData = (DataTable)Session["dtPart"];
                }
                DropDownList ddlCountry = (DropDownList)sender;
                Session["ddlSelectedLocation_grid"] = Convert.ToString(ddlCountry.SelectedValue);
                Session["ddlSelectedLine_grid"] = "All";
                if (Convert.ToString(ddlCountry.SelectedValue) == "0")
                {
                    Session["FilteredLocationData"] = dtData;
                    Session["FilteredData"] = dtData;
                    grdLineProduct.DataSource = dtData;
                    grdLineProduct.DataBind();
                }
                else
                {
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("location_name") == Convert.ToString(ddlCountry.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    Session["FilteredLocationData"] = dtFilterData;
                    grdLineProduct.DataSource = dtFilterData;
                    grdLineProduct.DataBind();
                }

                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["line_id"]));
                LoadViewpanel(drfirst);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On selection of line dropdown in header of gridview, the data will be loaded according to selection and saved the data in Session["FilteredData"]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLine_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();

                if (Session["FilteredLocationData"] != null)
                {
                    dtData = (DataTable)Session["FilteredLocationData"];
                }
                else if (Session["FilteredCountryData"] != null)
                {
                    dtData = (DataTable)Session["FilteredCountryData"];
                }
                else if (Session["FilteredRegionData"] != null)
                {
                    dtData = (DataTable)Session["FilteredRegionData"];
                }
                else if (Session["dtPart"] != null)
                {
                    dtData = (DataTable)Session["dtPart"];
                }
                DropDownList ddlCountry = (DropDownList)sender;
                Session["ddlSelectedLine_grid"] = Convert.ToString(ddlCountry.SelectedValue);
                if (Convert.ToString(ddlCountry.SelectedValue) == "0")
                {
                    Session["FilteredData"] = dtData;
                    grdLineProduct.DataSource = dtData;
                    grdLineProduct.DataBind();
                }
                else
                {
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("line_name") == Convert.ToString(ddlCountry.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    grdLineProduct.DataSource = dtFilterData;
                    grdLineProduct.DataBind();
                }

                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["line_id"]));
                LoadViewpanel(drfirst);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : It will load all the data to dropdowns in edit panel by calling GetAllMasterBL unction and binding the output to different dropdowns.
        /// </summary>
        private void GetMasterDropDown()
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        if (Convert.ToString(Session["SiteAdminFlag"]) == "Y" && Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                            ddlRegion.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlRegion.Enabled = true;
                    }
                    else
                    {
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    //ddlRegion_SelectedIndexChanged(null, null);
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        if (Convert.ToString(Session["SiteAdminFlag"]) == "Y" && Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                            ddlCountry.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlCountry.Enabled = true;
                    }
                    else
                    {
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        if (Convert.ToString(Session["SiteAdminFlag"]) == "Y" && Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                            ddlLocation.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlLocation.Enabled = true;
                    }
                    else
                    {
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                ddlLocation_SelectedIndexChanged(null, null);
                    //if (dsDropDownData.Tables[3].Rows.Count > 0)
                    //{
                    //    ddlLineNumber.DataSource = dsDropDownData.Tables[3];
                    //    ddlLineNumber.DataTextField = "line_code";
                    //    ddlLineNumber.DataValueField = "line_id";
                    //    ddlLineNumber.DataBind();
                    //}
                    //else
                    //{
                    //    ddlLineNumber.DataSource = null;
                    //    ddlLineNumber.DataBind();
                    //}
                    //if (dsDropDownData.Tables[3].Rows.Count > 0)
                    //{
                    //    ddlLineName.DataSource = dsDropDownData.Tables[3];
                    //    ddlLineName.DataTextField = "line_name";
                    //    ddlLineName.DataValueField = "line_id";
                    //    ddlLineName.DataBind();
                    //}
                    //else
                    //{
                    //    ddlLineName.DataSource = null;
                    //    ddlLineName.DataBind();
                    //}
                    //if (dsDropDownData.Tables[4].Rows.Count > 0)
                    //{
                    //    //ddlPartName.DataSource = dsDropDownData.Tables[4];
                    //    //ddlPartName.DataTextField = "product_name";
                    //    //ddlPartName.DataValueField = "product_id";
                    //    //ddlPartName.DataBind();
                    //    ddlPartNumber.DataSource = dsDropDownData.Tables[4];
                    //    ddlPartNumber.DataTextField = "product_code";
                    //    ddlPartNumber.DataValueField = "product_id";
                    //    ddlPartNumber.DataBind();
                    //}
                    //else
                    //{
                    //    //ddlPartName.DataSource = null;
                    //    //ddlPartName.DataBind();
                    //    ddlPartNumber.DataSource = null;
                    //    ddlPartNumber.DataBind();
                    //}

                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        //public void BindPart()
        //{
        //    DataTable dtPart = new DataTable();
        //    DataRow[] dr;
        //    DataTable dtLine = new DataTable();
        //    try
        //    {
        //        dtLine = (DataTable)Session["dtLineProduct"];
        //        dr = dtLine.Select("line_name='" + Convert.ToString(ddlLineName.SelectedItem) + "'");
        //        dtPart = dtLine.Clone();
        //        foreach (DataRow row in dr)
        //        {
        //            dtPart.Rows.Add(row.ItemArray);
        //        }
        //        if (dtPart.Rows.Count > 0)
        //        {
        //            //ddlPartName.DataSource = dtPart;
        //            //ddlPartName.DataTextField = "product_name";
        //            //ddlPartName.DataValueField = "product_id";
        //            ddlPartNumber.DataSource = dtPart;
        //            ddlPartNumber.DataTextField = "product_code";
        //            ddlPartNumber.DataValueField = "product_id";
        //            //ddlPartName.DataBind();
        //            ddlPartNumber.DataBind();
        //            //ddlPartName.SelectedIndex = 0;
        //            //ddlPartNumber.SelectedValue = ddlPartName.SelectedValue;
        //            //txtPartName.Text = Convert.ToString(ddlPartName.SelectedItem);
        //            txtPartNumber.Text = Convert.ToString(ddlPartNumber.SelectedItem);
        //        }
        //        else
        //        {
        //            //ddlPartName.DataSource = null;
        //            ddlPartNumber.DataSource = null;
        //            //ddlPartName.DataBind();
        //            ddlPartNumber.DataBind();
        //            //txtPartName.Text = "";
        //            txtPartNumber.Text = "";
        //        }
                    
               
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }

        //}

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Calling GetLinProductBL function to fetch datable and load gridview with the datatable.
        /// </summary>
        /// <param name="UserId"></param>
        private void GridBind()
        {
            DataTable dtResult;
            try
            {
                objLocBO = new LocationBO();
                objLocBL = new LocationBL();
                dtResult = new DataTable();
                objLocBO.UserId = Convert.ToInt32(Session["UserId"]);
                dtResult = objLocBL.GetLinProductBL(objLocBO);
                if (dtResult.Rows.Count > 0)
                {
                    grdLineProduct.DataSource = dtResult;
                    Session["dtPart"] = dtResult;
                    Session["dtOriginalPart"] = dtResult;
                    Session["ddlSelectedRegion_grid"] = null;
                    Session["ddlSelectedCountry_grid"] = null;
                    Session["ddlSelectedLocation_grid"] = null;
                    Session["ddlSelectedLine_grid"] = null;
                    Session["FilteredData"] = dtResult;
                    Session["FilteredRegionData"] = null;
                    Session["FilteredCountryData"] = null;
                    Session["FilteredLocationData"] = null;
                }
                else
                {
                    grdLineProduct.DataSource = null;
                }
                grdLineProduct.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Display the view panel and load all the information with respective controls.
        /// </summary>
        /// <param name="drselect">Selected Row from the dridview</param>
        private void LoadViewpanel(DataRow drselect)
        {
            try
            {
                lblregion.Text = Convert.ToString(drselect["region_name"]);
                lblCountry.Text = Convert.ToString(drselect["country_name"]);
                lblLocation.Text = Convert.ToString(drselect["location_name"]);
                lblLineName.Text = Convert.ToString(drselect["line_name"]);
                //lblLineNumber.Text = Convert.ToString(drselect["line_code"]);
                //lblPartName.Text = Convert.ToString(drselect["product_name"]);
                lblPartNumber.Text = Convert.ToString(drselect["product_code"]);
                viewpanel.Visible = true;
                editpanel.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : To fetch the complete row details with lineid
        /// </summary>
        /// <param name="userid">Selected Row Line id</param>
        /// <returns>Selected Row details</returns>
        private DataRow selectedRow(int line_id)
        {
            DataTable dtGrid = (DataTable)Session["dtPart"];
            DataRow drselect = (from DataRow dr in dtGrid.Rows
                                where (int)dr["line_id"] == line_id
                                select dr).FirstOrDefault();

            return drselect;
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Display the add panel and load all the information with respective controls.
        /// </summary>
        /// <param name="drselect">Selected Row from the dridview</param>
        private void LoadEditPanel(DataRow drselect)
        {
            try
            {
                btnAdd_Click(null, null);

                Session["line_id"] = Convert.ToString(drselect["line_id"]);
                ddlRegion.SelectedValue = Convert.ToString(drselect["region_id"]);
                ddlRegion_SelectedIndexChanged(null, null);
                ddlCountry.SelectedValue = Convert.ToString(drselect["country_id"]);
                ddlCountry_SelectedIndexChanged(null, null);
                ddlLocation.SelectedValue = Convert.ToString(drselect["location_id"]);
                ddlLocation_SelectedIndexChanged(null, null);
                ddlLineName.SelectedValue = Convert.ToString(drselect["line_id"]);
                //ddlLineNumber.SelectedValue = Convert.ToString(drselect["line_id"]);
                //ddlPartName.SelectedValue = Convert.ToString(drselect["product_id"]);
                //txtPartName.Text = Convert.ToString(ddlPartName.SelectedItem);
                //ddlPartNumber.SelectedValue = Convert.ToString(drselect["product_id"]);
                txtPartNumber.Text = Convert.ToString(drselect["product_code"]);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Clears all the session related to the page and fields
        /// </summary>
        private void Clear()
        {
            //txtPartName.Text = "";
            txtPartNumber.Text = "";
            Session["line_id"] = "";
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : It loads the excel data to datatable and calls SavePartsBL function to save the part details in database.
        /// </summary>
        /// <param name="CurrentFilePath">path where the excel file upload has been saved</param>
        private void InsertExcelRecords(string CurrentFilePath)
        {
            try
            {
                LocationBO Output = new LocationBO();
                objLocBL = new LocationBL();
                DataTable dtOutput = new DataTable();
                OleDbConnection Econ;
                string constr = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0 Xml;HDR=YES;""", CurrentFilePath);
                Econ = new OleDbConnection(constr);
                string Query = string.Format("Select [Region],[Country],[Location],[LineName],[PartNumber] FROM [{0}]", "Part_Mass_Upload$");
                OleDbCommand Ecom = new OleDbCommand(Query, Econ);
                //Econ.Open();

                DataSet ds = new DataSet();
                OleDbDataAdapter oda = new OleDbDataAdapter(Query, Econ);
                //Econ.Close();
                oda.Fill(ds);
                DataTable Exceldt = ds.Tables[0];
                dtOutput = objLocBL.SavePartsBL(Exceldt,"add");
                if (dtOutput != null)
                {
                    if (dtOutput.Rows.Count > 0)
                    {
                        Output.error_code = Convert.ToInt32(dtOutput.Rows[0][0]);
                        Output.error_msg = Convert.ToString(dtOutput.Rows[0][1]);
                        if (Output.error_code == 0)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + Output.error_msg + "');", true);
                            GridBind();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + Output.error_msg + "');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('There is error in saving parts. Please check all the columns added in excel sheet and try again..');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('There is error in saving parts. Please check all the columns added in excel sheet and try again..');", true);
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Please check all the columns added in excel sheet and try again.');", true);
            }

        }

        #endregion

    }
}