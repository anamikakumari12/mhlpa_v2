﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreatePlan.aspx.cs" Inherits="MHLPA_Application.CreatePlan" MasterPageFile="~/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
<%-- <script src="js/datatables_export_lib.min.js"></script>--%>
   <script src=" https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.bootstrap.min.js"></script>
    <link href="css/datatables_lib_export.min.css" rel="stylesheet" />
    <link href="css/dataTables.bootstrap.css" rel="stylesheet" />
    <script src="js/DatatblefixedColumns.js"></script>
    <%--<script src="https://cdn.datatables.net/scroller/1.4.4/js/dataTables.scroller.min.js"></script>--%>
    <style>
        .lst-pad
        {
            padding:10px;
        }
        #MainContent_CreateplanGridWrapper
        {
            float: left;
        }

        #MainContent_CreateplanGridWrapper, #MainContent_CreateplanGridPanelHeader, #MainContent_CreateplanGridPanelHeaderContent, #MainContent_CreateplanGridPanelItemContent, #MainContent_CreateplanGridPanelItem
        {
            width: 100% !important;
        }

            #MainContent_CreateplanGridWrapper th, #MainContent_CreateplanGridWrapper td
            {
                padding: 5px 15px;
                font-size: 13px;
            }

        .ma_table
        {
            margin: 10px 0 0 0;
            width: 100%;
            float: left;
        }

        #MainContent_CreateplanGridPanelHeaderContent
        {
            background: none !important;
        }

        #MainContent_CreateplanGridPanelHeader, #MainContent_CreateplanGridPanelHeaderContentFreeze
        {
            background: #008244 !important;
        }

        #MainContent_CreateplanGridFreeze
        {
            background: #008244;
            color: #ffffff;
        }

        .DTFC_Clone
        {
            background: #008244;
            color: #ffffff;
        }

        div.DTFC_LeftHeadWrapper table, div.DTFC_LeftFootWrapper table, div.DTFC_RightHeadWrapper table, div.DTFC_RightFootWrapper table, table.DTFC_Cloned tr.even
        {
            background: #008244;
            color: #ffffff;
            padding: 0 5px;
        }

        div.DTFC_LeftHeadWrapper table, div.DTFC_LeftFootWrapper table, div.DTFC_RightHeadWrapper table, div.DTFC_RightFootWrapper table, table.DTFC_Cloned tr.odd
        {
            background: #008244;
            color: #ffffff;
            padding: 0 5px;
        }

        table.dataTable thead .sorting
        {
            border: none;
        }

        th, td
        {
            white-space: nowrap;
        }

        table.DTFC_Cloned thead, table.DTFC_Cloned tfoot
        {
            background-color: white;
        }

        div.DTFC_Blocker
        {
            background-color: white;
        }

        div.DTFC_LeftWrapper table.dataTable, div.DTFC_RightWrapper table.dataTable
        {
            margin-bottom: 0;
            z-index: 2;
        }

            div.DTFC_LeftWrapper table.dataTable.no-footer, div.DTFC_RightWrapper table.dataTable.no-footer
            {
                border-bottom: none;
            }

        .divWaiting
        {
            position: absolute;
            background-color: #FAFAFA;
            z-index: 2147483647 !important;
            opacity: 0.8;
            overflow: hidden;
            text-align: center;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            padding-top: 20%;
        }
    </style>
    <script type="text/javascript">
        function LoadGrid() {
            var head_content = $('#MainContent_CreateplanGrid tr:first').html();
            $('#MainContent_CreateplanGrid').prepend('<thead></thead>')
            $('#MainContent_CreateplanGrid thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_CreateplanGrid tbody tr:first').hide();

            var table = $('#MainContent_CreateplanGrid').dataTable({
                scrollY: '500px',
                scrollX: '100%',
                sScrollXInner: "100%",
                paging: false,
                sorting: false,
                fixedColumns: {
                    leftColumns: 1

                },
                "columnDefs": [
            //{
            //    "targets": [ 2 ],
            //    "visible": false,
            //    "searchable": false
            //},
            //{
            //    "targets": [ 3 ],
            //    "visible": false
            //},
            //{
            //    "targets": [4],
            //    "visible": false
            //},
            //{
            //    "targets": [5],
            //    "visible": false
            //},
            //{
            //    "targets": [6],
            //    "visible": false
            //},
            //{
            //    "targets": [7],
            //    "visible": false
            //},
                //{ "width": "200px", "targets": 0 },
                ],
                // "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]]
            });
        }
        $(document).ready(function () {
            //gridviewScroll();
            //Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            var head_content = $('#MainContent_CreateplanGrid tr:first').html();
            $('#MainContent_CreateplanGrid').prepend('<thead></thead>')
            $('#MainContent_CreateplanGrid thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_CreateplanGrid tbody tr:first').hide();

            var table = $('#MainContent_CreateplanGrid').dataTable({
                scrollY: '500px',
                scrollX: '100%',
                sScrollXInner: "100%",
                paging: false,
                sorting: false,
                fixedColumns: {
                    leftColumns: 1

                },
                "columnDefs": [
            //{
            //    "targets": [2],
            //    "visible": false,
            //    "searchable": false
            //},
            //{
            //    "targets": [3],
            //    "visible": false
            //},
            //{
            //    "targets": [4],
            //    "visible": false
            //},
            //{
            //    "targets": [5],
            //    "visible": false
            //},
            //{
            //    "targets": [6],
            //    "visible": false
            //},
            //{
            //    "targets": [7],
            //    "visible": false
            //},
                //{ "width": "200px", "targets": 0 },
                ],
                // "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "All"]]
            });
            //$('#MainContent_CreateplanGrid').scroll(5);
            //var left = $('#MainContent_CreateplanGrid').scrollLeft;
            //$('#MainContent_CreateplanGrid').scrollLeft = 1000;
        });
        $(function () {
            $("#divpopup").dialog({
                modal: true,
                autoOpen: false,
                title: "Auditors",
                width: 700,
                height: 500
            });
        });

        function loadgrid() {
            debugger;
            var col = document.getElementById("MainContent_hdnweek").value;
            var line = document.getElementById("MainContent_hdnLine").value;
            var row;
            var tbl = document.getElementById('MainContent_CreateplanGrid');
            //var tbl_row = tbl.rows[row];
            for (var i = 2; i < tbl.rows.length; i++)
            {
                var lineid = tbl.rows[i].cells[0].children[0].value;
                if (lineid == line)
                {
                    row = i;
                    break;
                }
                
            }
            var tbl_Cell = tbl.rows[row].cells[col];
            var users="";
            var x = document.getElementById("MainContent_lstSelectedEmployees");
            var spans = tbl_Cell.getElementsByTagName("span");
            var inputs = tbl_Cell.getElementsByTagName("input");
           
            if (spans.length != undefined || spans.length != '') {
                var container;
                while (spans.length > 0)
                {
                    container = spans[0].parentNode;
                    container.removeChild(spans[0]);
                    container.removeChild(inputs[0]);
                }
               
            }
            var brs = tbl_Cell.getElementsByTagName('br');
            if (brs.length != undefined || brs.length != '') {
                var container;
                while (brs.length > 0) {
                    container = brs[0].parentNode;
                    container.removeChild(brs[0]);
                }

            }
            for (var i = 0; i < x.options.length; i++) {
                //if (x.options[i].selected) {
                users = users.toString() + "," + x.options[i].value.toString();

                //if (!tbl_Cell.innerHTML.toString().includes(x.options[i].value.toString())) {
                    
                    tbl_Cell.innerHTML = tbl_Cell.innerHTML.toString() + "<br/><span>" + x.options[i].text + "</span>";
                    tbl_Cell.innerHTML = tbl_Cell.innerHTML.toString() + "<input type=\"hidden\" value=\"" + x.options[i].value + "\" />";
                //}
                //}
            }
            users = users.substring(1, users.length);
            tbl.rows[row].cells[col].getElementsByTagName("input")[0].value = users;
            $('#divpopup').dialog('close');
        }

        function ShowPopup(id,name,line,week)
        {
            $("#divpopup").dialog({
                modal: true,
                autoOpen: false,
                title: "Employees",
                width: 700,
                height: 500
            });


            $('#divpopup').css('display', 'block');
            $('#divpopup').dialog('open');
            $('[id*=MainContent_lstSelectedEmployees]').empty();

            debugger;
            var col = week;
            var row;
            var tbl = document.getElementById('MainContent_CreateplanGrid');
            //var tbl_row = tbl.rows[row];
            for (var i = 2; i < tbl.rows.length; i++) {
                var lineid = tbl.rows[i].cells[0].children[0].value;
                if (lineid == line) {
                    row = i;
                    break;
                }

            }
            var tbl_Cell = tbl.rows[row].cells[col];
            var x = document.getElementById("MainContent_lstSelectedEmployees");
            var option;

            var inputs = tbl_Cell.getElementsByTagName("input");
            var spans = tbl_Cell.getElementsByTagName("span");
            if (inputs.length != undefined || inputs.length != '') {
                for (i = 1; i < inputs.length; i++) {
                    option = document.createElement("option");
                    option.text = spans[i-1].innerHTML;
                    option.value = inputs[i].value;
                    x.add(option);
                }
            }
            else if (name != undefined || name != '') {
                option = document.createElement("option");
                option.text = name;
                option.value = id;
                x.add(option);
            }

            $.ajax({
                type: "POST",
                url: "CreatePlan.aspx/LoadAllEmployees",
                data: '{}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });

            //$('#lstSelectedEmployees > option').appendTo('#lstEmployees');
           
            

            document.getElementById("MainContent_hdnLine").value = line;
            document.getElementById("MainContent_hdnweek").value = week;
            return false;
            //document.getElementById("divpopup").style["display"] = "block";
            //alert(name);
        }

        function OnSuccess(response) {
            var data = response.d;
            $('[id*=MainContent_lstEmployees]').empty();
            var listBox = $('[id*=MainContent_lstEmployees]');
            $.each($.parseJSON(response.d), function () {
                $('[id*=MainContent_lstEmployees]').append($("<option></option>").val(this['user_id']).html(this['emp_full_name']));
            });

            for (var i = 0; i < $('[id*=MainContent_lstSelectedEmployees]')[0].length; i++) {
                value = $('[id*=MainContent_lstSelectedEmployees]')[0].options[i].value;
                for (var j = 0; j < $('[id*=MainContent_lstEmployees]')[0].length; j++) {
                    if ($('[id*=MainContent_lstEmployees]')[0].options[j].value == $('[id*=MainContent_lstSelectedEmployees]')[0].options[i].value) {
                        $('[id*=MainContent_lstEmployees]')[0].remove(j);
                    }
                }
            }

        }
        function Add(e) {
            $('#MainContent_lstEmployees > option:selected').appendTo('#MainContent_lstSelectedEmployees');
            e.preventDefault();
        }
        function AddAll(e) {
            $('#MainContent_lstEmployees > option').appendTo('#MainContent_lstSelectedEmployees');
            e.preventDefault();
        }
        function Remove(e) {
            $('#MainContent_lstSelectedEmployees > option:selected').appendTo('#MainContent_lstEmployees');
            e.preventDefault();
        }
        function RemoveAll(e) {
            $('#MainContent_lstSelectedEmployees > option').appendTo('#MainContent_lstEmployees');
            e.preventDefault();
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>

    <div>
        <div class='block-web' style='float: left; width: 100%; height: 36px;'>
            <div class="header">
                <div class="crumbs">
                    <!-- Start : Breadcrumbs -->
                    <ul id="breadcrumbs" class="breadcrumb">
                        <li>
                            <a class="mn_breadcrumb"><%=Resources.Resource.Audit %></a>
                        </li>
                        <li class="current">Create Plan</li>

                    </ul>
                </div>
            </div>

        </div>
    </div>
    <div>
        <!-- main content start-->

        <div class="md_main">
            <%-- <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                        <span style="border-width: 0px; position: fixed; padding: 50px; background-color: #FFFFFF; font-size: 36px; left: 40%; top: 40%;">Loading ...</span>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>--%>
            <asp:UpdatePanel runat="server" ID="updpnlGrid">
                <ContentTemplate>
                    <div class="row">
                        <div>
                            <div class="clearfix"></div>
                            <div class="filter_panel">
                                <div>
                                    <div class="col-md-4 filter_label" style="width: 14%">
                                        <p><%=Resources.Resource.Region %>* : </p>
                                    </div>
                                    <div class="col-md-4" style="width: 20%">
                                        <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-md-4 filter_label" style="width: 13%">
                                        <p><%=Resources.Resource.Country %>* : </p>
                                    </div>
                                    <div class="col-md-4" style="width: 20%">
                                        <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-md-4 filter_label" style="width: 13%">
                                        <p><%=Resources.Resource.Location %>* : </p>
                                    </div>
                                    <div class="col-md-4" style="width: 20%">
                                        <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>
                                <div>
                                    <div class="col-md-4 filter_label" style="width: 14%">
                                        <p>Year* : </p>
                                    </div>
                                    <div class="col-md-4" style="width: 20%">
                                        <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlYear" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>

                                    </div>
                                    <div class="col-md-4 filter_label" style="width: 66%">
                                        <div id="save">

                                            <asp:Button runat="server" CssClass="btn-add" ID="btnSave" Text="SAVE" OnClick="btnSave_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix" style="height: 5px;"></div>


                            <asp:UpdateProgress ID="UpdateProgress" runat="server">
                <ProgressTemplate>
                     <div class="divWaiting">
                    <asp:Image ID="Image1" ImageUrl="images/loader.gif" AlternateText="Please Wait....."
                        runat="server" /> 
                    </div>
                </ProgressTemplate>
                   
            </asp:UpdateProgress>

                            <div class="result_panel_1">

                                <%--<asp:SqlDataSource ConnectionString="<%$ ConnectionStrings:csMHLPA %>" SelectCommand="select user_id, emp_full_name from mh_lpa_user_master" ID="SqlDataSource1" runat="server"></asp:SqlDataSource>--%>
                                <asp:GridView ID="CreateplanGrid" runat="server" AutoGenerateColumns="false" Width="100%" OnRowDataBound="OnRowDataBound" OnRowCommand="CreateplanGrid_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="<%$Resources:Resource, LineNameNo %>" HeaderStyle-ForeColor="#ffffff" ItemStyle-Height="100px" ItemStyle-Width="300px">
                                            <ItemTemplate>
                                                <%--<%# Eval("line_id") %>--%>
                                                 <asp:HiddenField ID="hdnlineId" runat="server" Value='<%# Eval("line_id") %>'/>
                                                <asp:Label ID="LbllineNoId" runat="server" Text='<%# Eval("line_id") %>' Visible="false"></asp:Label>
                                                <asp:Label Width="300px" ID="LblLineNo" runat="server" Text='<%# Eval("line_name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week1 %>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers1" runat="server" />
                                               <%-- <asp:DetailsView ID="DetailsView1" runat="server" Height="50px" Width="125px">
                                                    <Fields>
                                                        <asp:BoundField DataField="emp_full_name" />
                                                    </Fields>
                                                </asp:DetailsView>--%>
                                               <%-- <asp:DropDownList ID="ddlusers1" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week2 %>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers2" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers2" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week3 %>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers3" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers3" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week4 %>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers4" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers4" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week5 %>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers5" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers5" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week6%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers6" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers6" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week7%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers7" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers7" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week8%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers8" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers8" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week9%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers9" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers9" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week10%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers10" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers10" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week11%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers11" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers11" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week12%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers12" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers12" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week13%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers13" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers13" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week14%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers14" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers14" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week15%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers15" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers15" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week16%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers16" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers16" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week17%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers17" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers17" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week18%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers18" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers18" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week19%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers19" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers19" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week20%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers20" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers20" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week21%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers21" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers21" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week22%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers22" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers22" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week23%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers23" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers23" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week24%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers24" runat="server" />
                                              <%--  <asp:DropDownList ID="ddlusers24" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week25%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers25" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers25" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week26%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers26" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers26" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week27%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers27" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers27" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week28%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers28" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers28" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week29%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers29" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers29" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week30%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers30" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers30" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week31%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers31" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers31" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week32%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers32" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers32" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week33%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers33" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers33" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week34%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers34" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers34" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week35%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers35" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers35" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week36%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers36" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers36" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week37%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers37" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers37" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week38%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers38" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers38" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week39%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers39" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers39" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week40%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers40" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers40" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week41%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers41" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers41" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week42%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers42" runat="server" />
                                              <%--  <asp:DropDownList ID="ddlusers42" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week43%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers43" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers43" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week44%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers44" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers44" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week45%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers45" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers45" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week46%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers46" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers46" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week47%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers47" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers47" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week48%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers48" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers48" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week49%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers49" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers49" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week50%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers50" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers50" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week51%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers51" runat="server" />
                                                <%--<asp:DropDownList ID="ddlusers51" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="<%$Resources:Resource, Week52%>" HeaderStyle-ForeColor="#ffffff">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnSelectedUsers52" runat="server" />
                                               <%-- <asp:DropDownList ID="ddlusers52" runat="server">
                                                </asp:DropDownList>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>


                            </div>

                            <div id="divpopup" style="display:none;">
                                <div class="col-md-12">
                                <div class="col-md-5">
                                   <%-- <asp:CheckBoxList runat="server" ID="chklstEmployees" Height="50px"></asp:CheckBoxList>--%>
                                <asp:ListBox Rows="10" ID="lstEmployees" runat="server"></asp:ListBox>
                                    </div>
                                <div class="col-md-2" style="height:100%;">
                                    <div class="col-md-12 lst-pad">
                                        <input type="button" name="btnAdd" onclick="javascript: Add(this);" value=">" class="btn-add" />
                                    </div>
                                    <div class="col-md-12 lst-pad">
                                        <input type="button" name="btnAddAll" onclick="javascript: AddAll(this);" value=">>" class="btn-add" />
                                    </div>
                                    <div class="col-md-12 lst-pad">

                                        <input type="button" name="btnRemove" onclick="javascript: Remove(this);" value="<" class="btn-add" />
                                    </div>
                                    <div class="col-md-12 lst-pad">
                                        <input type="button" name="btnRemoveAll" onclick="javascript: RemoveAll(this);" value="<<" class="btn-add" />
                                    </div>

                                </div>
                                    
                                <div class="col-md-5">
                                <asp:ListBox ID="lstSelectedEmployees" Rows="10"  runat="server"></asp:ListBox>
                                    </div>
                                    </div>
                                <asp:HiddenField ID="hdnLine" runat="server" />
                                <asp:HiddenField ID="hdnweek" runat="server" />
                                <asp:Button ID="btnAddEmployee"  CssClass="btn-add" Text="Save Auditors" runat="server" OnClientClick="loadgrid();" />
                            </div>

                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlRegion" EventName="SelectedIndexChanged"/>
                    <asp:AsyncPostBackTrigger ControlID="ddlCountry" EventName="SelectedIndexChanged"/>
                    <asp:AsyncPostBackTrigger ControlID="ddlLocation" EventName="SelectedIndexChanged"/>
                    <asp:AsyncPostBackTrigger ControlID="btnSave"  EventName="Click"/>
                </Triggers>

            </asp:UpdatePanel>
            

        </div>
    </div>
    <%--</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="ddlRegion" />
            <asp:PostBackTrigger ControlID="ddlCountry" />
            <asp:PostBackTrigger ControlID="ddlRegion" />
        </Triggers>
</asp:UpdatePanel>--%>
</asp:Content>
