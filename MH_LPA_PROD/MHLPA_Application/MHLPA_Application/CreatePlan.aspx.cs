﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using MHLPA_BusinessLogic;
using MHLPA_BusinessObject;
using System.Threading;
using System.Web.Services;
using System.Text;


namespace MHLPA_Application
{
    public partial class CreatePlan : System.Web.UI.Page
    {
        #region Global Declaration
        UsersBO objUserBO;
        CommonBL objComBL;
        DataSet dsDropDownData;
        PlanBO objPlanBO;
        CommonFunctions objCom = new CommonFunctions();
        PlanBL objPlanBL;
        int j = 0;
        #endregion

        #region Events
        protected override void InitializeCulture()
        {
            string language = Convert.ToString(Session["language"]);

            Thread.CurrentThread.CurrentCulture = new CultureInfo(language);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
        }
        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
                        GetMasterDropdown();
                    
                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                if (dtCountry.Rows.Count > 0)
                {
                    ddlCountry.DataSource = dtCountry;
                    ddlCountry.DataTextField = "country_name";
                    ddlCountry.DataValueField = "country_id";
                    ddlCountry.DataBind();
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }

                dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlLocation.DataSource = dtLocation;
                    ddlLocation.DataTextField = "location_name";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    Session["dtLocation"] = dtLocation;
                    ddlLocation_SelectedIndexChanged(null, null);
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlLocation.DataSource = dtLocation;
                    ddlLocation.DataTextField = "location_name";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    Session["dtLocation"] = dtLocation;
                    ddlLocation_SelectedIndexChanged(null, null);
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLocation = new DataTable();
            DataTable dtLine = new DataTable();
            DataTable dtPart = new DataTable();
            DataTable dtUsers = new DataTable();
            try
            {
                j = 0;
                objUserBO = new UsersBO();
                objComBL = new CommonBL();
                Session["SelectedLocation"] = Convert.ToString(ddlLocation.SelectedValue);
                dtLocation = (DataTable)Session["dtLocation"];
                objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                BindData();
                if (dtLine.Rows.Count > 0)
                {

                    //DataTable ds = new DataTable();
                    //ds = null;
                    //CreateplanGrid.DataSource = ds;
                    //CreateplanGrid.DataBind();
                    //for (int i = CreateplanGrid.Rows.Count - 1; i >= 0; --i)
                    //{
                    //    CreateplanGrid.Rows.dele.RemoveAt(rowToRemove[i]);
                    //}
                    CreateplanGrid.DataSource = dtLine;
                    CreateplanGrid.DataBind();
                    Session["dtLine"] = dtLine;
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "LoadGrid();", true);
                }
                else
                {
                    CreateplanGrid.DataSource = null;
                    CreateplanGrid.DataBind();
                }

                //OnRowDataBound(null, null);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        
        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataTable dtEmployeeusers = new DataTable();
            DataTable dtUsers = new DataTable();
            int currentYear = Convert.ToInt32(ddlYear.SelectedValue);
            int currentyearweeks = GetWeeksInYear(currentYear);
            int weeks = currentyearweeks-1;
            int currentweek = GetIso8601WeekOfYear(DateTime.Now);
            int disabledweek = currentweek + 1;
            string lineid;
            dtUsers = (DataTable)Session["dtOutput"];
            try
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        
                        Label line = e.Row.FindControl("LbllineNoId") as Label;
                        lineid = Convert.ToString(line.Text);
                        //Find the DropDownList in the Row             
                        objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                        dtEmployeeusers = objComBL.GetEmployeesBasedOnLocationBL(objUserBO);

                        for (int i = 1; i <= 52; i++)
                        {
                            //Label lbl = new Label();
                            HiddenField hdnselected = e.Row.FindControl("hdnSelectedUsers"+i) as HiddenField;
                            hdnselected.Value = Convert.ToString(dtUsers.Rows[j][i + 3]);
                            if (!string.IsNullOrEmpty(Convert.ToString(dtUsers.Rows[j][i + 3])) && Convert.ToString(dtUsers.Rows[j][i + 3]) != "0")
                            {
                                List<int> lstUsers = Convert.ToString(dtUsers.Rows[j][i + 3]).Split(',').Select(int.Parse).ToList();
                                var name = dtEmployeeusers.AsEnumerable().Where(dr => lstUsers.Contains(dr.Field<int>("user_id"))).ToList();
                                if (name.Count > 0)
                                {
                                    for (int x = 0; x < name.Count; x++)
                                    {
                                        Label lbl = new Label();
                                        lbl.Text = Convert.ToString(name[x]["emp_full_name"]);
                                        e.Row.Cells[i].Controls.Add(lbl);
                                        e.Row.Cells[i].Controls.Add(new Literal() { ID = "row", Text = "<br/>" });
                                        HiddenField hdn = new HiddenField();
                                        hdn.ID = "hdn_" + j + "_" + i+"_"+x;
                                        hdn.Value = Convert.ToString(name[x]["user_id"]);
                                        e.Row.Cells[i].Controls.Add(hdn);
                                    }

                                    //selectedname = Convert.ToString(name[0]["user_id"]) + "-" + Convert.ToString(name[0]["emp_full_name"]);

                                    e.Row.Cells[i].Attributes.Add("onClick", "ShowPopup('" + Convert.ToString(name[0]["user_id"]) + "','" + Convert.ToString(name[0]["emp_full_name"]) + "','" + lineid + "','" + Convert.ToString(i) + "');");
                                }
                                else
                                {
                                    e.Row.Cells[i].Attributes.Add("onClick", "ShowPopup('','', '" + lineid + "', '" + Convert.ToString(i) + "');");
                                }
                            }
                            else
                            {
                                e.Row.Cells[i].Attributes.Add("onClick", "ShowPopup('','', '" + lineid + "', '" + Convert.ToString(i) + "');");
                            }
                        }

                        //DetailsView dv = e.Row.FindControl("DetailsView1") as DetailsView;
                        //DataTable dt = new DataTable();
                        //dt.Columns.AddRange(new DataColumn[1] { new DataColumn("emp_full_name", typeof(string)) });
                        //dt.Rows.Add("Test User");
                        //dv.DataSource = dt;
                        //dv.DataBind();
                        //for (int i = 1; i <= 52; i++)
                        //{
                        //    id = "ddlusers" + i;
                        //    objCom.TestLog(id);
                        //    ddlusers1 = (e.Row.FindControl(id) as DropDownList);
                        //    ddlusers1.DataSource = dtEmployeeusers;
                        //    ddlusers1.DataTextField = "emp_full_name";
                        //    ddlusers1.DataValueField = "user_id";
                        //    ddlusers1.DataBind();

                        //    if (i <= disabledweek && currentYear == Convert.ToInt32(DateTime.Now.Year))
                        //    {
                        //        ddlusers1.Items.Insert(0, new ListItem("", "0"));
                        //        ddlusers1.Enabled = false;
                        //    }
                        //    else
                        //    {
                        //        ddlusers1.Items.Insert(0, new ListItem("", "0"));
                        //    }
                        //    if (dtUsers != null)
                        //    {
                        //        if (dtUsers.Rows.Count > 0)
                        //        {
                        //            if (Convert.ToString(dtUsers.Rows[j][1]) == Convert.ToString(line.Text))
                        //            {
                        //                ddlusers1.SelectedValue = Convert.ToString(dtUsers.Rows[j][i + 3]);
                        //            }
                        //        }
                        //    }
                        //}

                        j++;
                    }
                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
                
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc :
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            int currentYear = Convert.ToInt32(ddlYear.SelectedValue);
            int currentyearweeks = GetWeeksInYear(currentYear);
            objCom.TestLog("Week Number :" + Convert.ToString(currentyearweeks));
            int weeks = currentyearweeks-1;
            objCom.TestLog("Week No :" + Convert.ToString(weeks));
            objPlanBL = new PlanBL();
            objPlanBO = new PlanBO();
            DataTable SelectedUsers = new DataTable();
            SelectedUsers.Columns.Add("location_id");
            SelectedUsers.Columns.Add("line_id");
            SelectedUsers.Columns.Add("line_name");
            SelectedUsers.Columns.Add("year");
            for (int i = 1; i <= 52; i++)
            {
                SelectedUsers.Columns.Add("wk" + i);
            }
            DataSet dtMailDetails = new DataSet();
            try
            {
                if (CreateplanGrid.Rows.Count != 0)
                {
                    HiddenField Username;
                    DataRow dr;
                    Label linename;
                    Label linenum;
                    string id;
                    foreach (GridViewRow row in CreateplanGrid.Rows)
                    {
                        linename = (row.FindControl("LblLineNo") as Label);
                        linenum = (row.FindControl("LbllineNoId") as Label);
                        dr = SelectedUsers.NewRow();
                        dr["location_id"] = Convert.ToString(ddlLocation.SelectedValue);
                        dr["line_id"] = Convert.ToString(linenum.Text);
                        dr["line_name"] = Convert.ToString(linename.Text);
                        dr["year"] = Convert.ToInt32(ddlYear.SelectedValue);
                        for (int i = 1; i <= 52; i++)
                        {
                            id = "hdnSelectedUsers" + i;
                            Username = (row.FindControl(id) as HiddenField);
                            dr["wk" + i] = Convert.ToString(Username.Value);
                           objCom.TestLog(Convert.ToString(linename.Text) +", week"+i+" : "+Convert.ToString(Username.Value));
                        }
                        SelectedUsers.Rows.Add(dr);
                        
                    }
                    dtMailDetails = objPlanBL.SaveBulkPlanningBL(SelectedUsers);
                    if (dtMailDetails != null)
                    {
                        if (dtMailDetails.Tables[0] != null)
                        {
                            if (dtMailDetails.Tables[0].Rows.Count > 0)
                            {
                                SendInvitation(dtMailDetails.Tables[0]);
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Plans are created and mails are sent to respective employees.'); LoadGrid();", true);
                                //  BindData();
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('There is no plan saved or there is error in saving. Please try again.'); LoadGrid();", true);
                            }
                        }
                        if (dtMailDetails.Tables[1] != null)
                        {
                            if (dtMailDetails.Tables[1].Rows.Count > 0)
                            {
                                CancelInvitation(dtMailDetails.Tables[1]);
                                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Plans are created and mails are sent to respective employees.');", true);
                                //  BindData();
                            }
                            //else
                            //{
                            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('There is no plan saved or there is error in saving. Please try again.');", true);
                            //}
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc :
        /// </summary>
        /// <param name="dtMailDetails"></param>
        private void CancelInvitation(DataTable dtMailDetails)
        {
            eAppointmentMail objApptEmail;
            try
            {
                for (int i = 0; i < dtMailDetails.Rows.Count; i++)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(dtMailDetails.Rows[i]["email_id"])))
                    {
                        objApptEmail = new eAppointmentMail();
                        objApptEmail.Body = "Audit for " + Convert.ToString(dtMailDetails.Rows[i]["line_name"]) + " between " + Convert.ToString(dtMailDetails.Rows[i]["start_date"]) + " and " + Convert.ToString(dtMailDetails.Rows[i]["end_date"]) + " is cancelled. Please contact the LPA Administrator if you need to make any changes.";
                        objApptEmail.Email = Convert.ToString(dtMailDetails.Rows[i]["email_id"]);
                        //objApptEmail.EndDate = Convert.ToDateTime(dtMailDetails.Rows[i]["end_date"]);
                        objApptEmail.Location = Convert.ToString(dtMailDetails.Rows[i]["line_name"]) + ", " + Convert.ToString(dtMailDetails.Rows[i]["location_name"]);
                       // objApptEmail.StartDate = Convert.ToDateTime(dtMailDetails.Rows[i]["start_date"]);
                        objApptEmail.Subject = "Your audit planned from " + Convert.ToString(dtMailDetails.Rows[i]["start_date"]) + " to " + Convert.ToString(dtMailDetails.Rows[i]["end_date"]) + " for " + Convert.ToString(dtMailDetails.Rows[i]["line_name"])+" is cancelled";
                        objApptEmail.Name = Convert.ToString(dtMailDetails.Rows[i]["email_id"]);
                        objApptEmail.newStartDate = Convert.ToString(dtMailDetails.Rows[i]["start_date_formatted"]);
                        objApptEmail.newEndDate = Convert.ToString(dtMailDetails.Rows[i]["end_date_formatted"]);
                        objCom.CancelInvitation(objApptEmail);
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc : It will call SendInvitation function by passing email body, subject, location for sending mails to respective users. The details are available in datatable passed in parameter.
        /// </summary>
        /// <param name="dtMailDetails">Details for sending mail</param>
        private void SendInvitation(DataTable dtMailDetails)
        {
            eAppointmentMail objApptEmail;
            try
            {
                for (int i = 0; i < dtMailDetails.Rows.Count;i++ )
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(dtMailDetails.Rows[i]["email_id"])))
                    {
                        objApptEmail = new eAppointmentMail();
                        objApptEmail.Body = "Audit for " + Convert.ToString(dtMailDetails.Rows[i]["line_name"]) + " needs to be performed between " + Convert.ToString(dtMailDetails.Rows[i]["start_date"]) + " and " + Convert.ToString(dtMailDetails.Rows[i]["end_date"]) + ". Please contact the LPA Administrator if you need to make any changes.";
                        objApptEmail.Email = Convert.ToString(dtMailDetails.Rows[i]["email_id"]);
                       // objApptEmail.EndDate = Convert.ToDateTime(dtMailDetails.Rows[i]["end_date"]);
                        objApptEmail.Location = Convert.ToString(dtMailDetails.Rows[i]["line_name"]) + ", " + Convert.ToString(dtMailDetails.Rows[i]["location_name"]);
                       // objApptEmail.StartDate = Convert.ToDateTime(dtMailDetails.Rows[i]["start_date"]);
                        objApptEmail.Subject = "You have an audit planned from " + Convert.ToString(dtMailDetails.Rows[i]["start_date"]) + " to " + Convert.ToString(dtMailDetails.Rows[i]["end_date"]) + " for " + Convert.ToString(dtMailDetails.Rows[i]["line_name"]);
                        objApptEmail.Name = Convert.ToString(dtMailDetails.Rows[i]["email_id"]);
                        objApptEmail.newStartDate = Convert.ToString(dtMailDetails.Rows[i]["start_date_formatted"]);
                        objApptEmail.newEndDate = Convert.ToString(dtMailDetails.Rows[i]["end_date_formatted"]);
                        objCom.SendInvitation(objApptEmail);
                    }
                }
                    
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc : On changing the year value, it calls ddlLocation_SelectedIndexChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlLocation_SelectedIndexChanged(null, null);
        }

        #endregion

        #region Methods
        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc : It calls GetAllMasterBL to fetch details of region,c ountry and location and it binds with the dropdowns.
        /// </summary>
        private void GetMasterDropdown()
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            DataTable dtLine = new DataTable();
            DataTable dtEmployee = new DataTable();
            DataTable dtPart = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                        if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            //     ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                            ddlRegion.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlRegion.Enabled = true;
                    }
                    else
                    {
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                }
                if (!string.IsNullOrEmpty(ddlRegion.SelectedValue))
                {
                    objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                    dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                    if (dtCountry.Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dtCountry;
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                        if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            //ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                            ddlCountry.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlCountry.Enabled = true;
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }
                if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
                {
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                    dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                    if (dtLocation.Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dtLocation;
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                        if (Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            //ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                            ddlLocation.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlLocation.Enabled = true;
                        Session["SelectedLocation"] = Convert.ToString(ddlLocation.SelectedValue);
                        Session["dtLocation"] = dtLocation;
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }


                ddlYear.Items.Insert(0, new ListItem(Convert.ToString(DateTime.Now.Year), Convert.ToString(DateTime.Now.Year)));
                ddlYear.Items.Insert(1, new ListItem(Convert.ToString(Convert.ToInt32(DateTime.Now.Year) + 1), Convert.ToString(Convert.ToInt32(DateTime.Now.Year) + 1)));
                ddlYear.Items.Insert(1, new ListItem(Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1), Convert.ToString(Convert.ToInt32(DateTime.Now.Year) - 1)));
                BindData();

                 objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                if (dtLine.Rows.Count > 0)
                {
                    CreateplanGrid.DataSource = dtLine;
                    CreateplanGrid.DataBind();
                    Session["dtLine"] = dtLine;
                }
                else
                {
                    CreateplanGrid.DataSource = null;
                    CreateplanGrid.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc : It calls GetBulkPlansBL function based on location and year and the output is stored in session.
        /// </summary>
        private void BindData()
        {
            try
            {
                objPlanBO = new PlanBO();
                objPlanBL = new PlanBL();
                DataTable dtOutput = new DataTable();
                objPlanBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                objPlanBO.p_year = Convert.ToInt32(ddlYear.SelectedValue);
                dtOutput = objPlanBL.GetBulkPlansBL(objPlanBO);
                Session["dtOutput"] = dtOutput;
                objUserBO = new UsersBO();
                DataTable dtAllEmps = new DataTable();
                objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                dtAllEmps = objComBL.GetEmployeesBasedOnLocationBL(objUserBO);
                Session["dtAllEmployees"] = dtAllEmps;
                //if (lstEmployees.Items.Count > 0)
                //{
                //    for (int i = lstEmployees.Items.Count-1; i >= 0; i--)
                //    {
                //        lstEmployees.Items.RemoveAt(i);
                //    }
                //}
                //    lstEmployees.Items.Clear();
                //lstSelectedEmployees.Items.Clear();
                //lstEmployees.DataSource = dtAllEmps;
                //lstEmployees.DataTextField = "emp_full_name";
                //lstEmployees.DataValueField = "user_id";
                //lstEmployees.DataBind();
                //chklstEmployees.Items.Clear();
                //chklstEmployees.DataSource = dtAllEmps;
                //chklstEmployees.DataTextField = "emp_full_name";
                //chklstEmployees.DataValueField = "user_id";
                //chklstEmployees.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc : It will return the week count for year passed in parameter
        /// </summary>
        /// <param name="year">year</param>
        /// <returns>week count</returns>
        public int GetWeeksInYear(int year)
        {
            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            DateTime date1 = new DateTime(year, 12, 31);
            System.Globalization.Calendar cal = dfi.Calendar;
            return cal.GetWeekOfYear(date1, dfi.CalendarWeekRule,
                                                dfi.FirstDayOfWeek);
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 23 Oct 2017
        /// Desc : This function returns the week for the passed datetime.
        /// </summary>
        /// <param name="time"></param>
        /// <returns>week number</returns>
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        #endregion

        protected void CreateplanGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int rowIndex = Convert.ToInt32(e.CommandArgument);

            //Reference the GridView Row.
            GridViewRow row = CreateplanGrid.Rows[rowIndex];

            Label lnk = e.CommandSource as Label;
            if (lnk != null)
            {
                DataControlFieldCell cell = lnk.Parent as DataControlFieldCell;
                //cell.BackColor = Color.Red;

            }

            //Label line = e.Row.FindControl("LbllineNoId") as Label;
           
            objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
            //dtEmployeeusers = objComBL.GetEmployeesBasedOnLocationBL(objUserBO);
        }
       

        [WebMethod]
        public static string LoadAllEmployees()
        {
            DataTable dt = new DataTable();
            dt =(DataTable)HttpContext.Current.Session["dtAllEmployees"];
            return DataTableToJSONWithStringBuilder(dt);
        }

        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                        }
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }  
    }
}
