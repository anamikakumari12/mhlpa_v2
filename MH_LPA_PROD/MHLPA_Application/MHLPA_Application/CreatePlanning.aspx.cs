﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MHLPA_BusinessLogic;
using MHLPA_BusinessObject;

namespace MHLPA_Application
{
    public partial class CreatePlanning : System.Web.UI.Page
    {
        UsersBO objUserBO;
        CommonBL objComBL;
        DataSet dsDropDownData;
        protected void Page_Load(object sender, EventArgs e)
        {
            //int userid = 4;
            //Session["UserId"] = userid;
            if (!IsPostBack)
            {
                GetMasterDropdown();
            }

        }

        private void GetMasterDropdown()
        {
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[2].Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dsDropDownData.Tables[2];
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                    }
                    else
                    {
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerScript();", true);
            }
            catch (Exception)
            {
                
                throw;
            }
            
        }
    }
}