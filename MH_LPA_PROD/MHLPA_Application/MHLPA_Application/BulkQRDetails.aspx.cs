﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using MHLPA_BusinessLogic;
using MHLPA_BusinessObject;
using QRCoder;

namespace MHLPA_Application
{
    public partial class BulkQRDetails : System.Web.UI.Page
    {
        #region Global Declaration
        LocationBO objLocBO;
        LocationBL objLocBL;
        DataSet dsDropDownData;
        UsersBO objUserBO;
        CommonBL objComBL;
        CommonFunctions objCom = new CommonFunctions();
        #endregion

        #region Events

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Dec 2017
        /// Desc : When page loads, it calls GetMasterDropdown to load dropdown values, it then calls GridBind to set session value for lines and then it calls BulkQRCode method to generate QR Codes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int UserId;
                if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }
                else
                {
                    UserId = Convert.ToInt32(Session["UserId"]);
                }
                if (!Page.IsPostBack)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
                    {
                        GetMasterDropdown();
                        GridBind();
                        BulkQRCode();
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Dec 2017
        /// Desc : Based on selection of location value,it calls GetLineListDropDownBL for line dropdown binding
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLocation = new DataTable();
            DataTable dtLine = new DataTable();
            DataTable dtPart = new DataTable();
            DataTable dtUsers = new DataTable();
            try
            {
                objUserBO = new UsersBO();
                objComBL = new CommonBL();
                Session["SelectedLocation"] = Convert.ToString(ddlLocation.SelectedValue);
                dtLocation = (DataTable)Session["dtLocation"];
                objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);

                //objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                if (dtLine.Rows.Count > 0)
                {
                    ddlLine.DataSource = dtLine;
                    ddlLine.DataTextField = "line_name";
                    ddlLine.DataValueField = "line_id";
                    ddlLine.DataBind();
                    ddlLine.Items.Insert(0, new System.Web.UI.WebControls.ListItem("All", "0"));
                }
                else
                {
                    ddlLine.Items.Clear();
                    ddlLine.DataSource = null;
                    ddlLine.DataBind();
                }
                GridBind();
                BulkQRCode();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Dec 2017
        /// Desc : Based on selection of region value,it calls GetCountryBasedOnRegionBL for country dropdown binding and  GetLocationBasedOnCountryBL method for location dropdown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                if (dtCountry.Rows.Count > 0)
                {
                    ddlCountry.DataSource = dtCountry;
                    ddlCountry.DataTextField = "country_name";
                    ddlCountry.DataValueField = "country_id";
                    ddlCountry.DataBind();
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }

                dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlLocation.DataSource = dtLocation;
                    ddlLocation.DataTextField = "location_name";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    Session["dtLocation"] = dtLocation;
                    ddlLocation_SelectedIndexChanged(null, null);
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Dec 2017
        /// Desc : Based on selection of country value,it calls GetLocationBasedOnCountryBL for location dropdown binding and  ddlLocation_SelectedIndexChanged method for line dropdown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlLocation.DataSource = dtLocation;
                    ddlLocation.DataTextField = "location_name";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    Session["dtLocation"] = dtLocation;
                    ddlLocation_SelectedIndexChanged(null, null);
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Dec 2017
        /// Desc : it calls BulkQRCode method for generating qr code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLine_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLine = new DataTable();
            try
            {
                objUserBO = new UsersBO();
                objComBL = new CommonBL();
                Session["SelectedLine"] = Convert.ToString(ddlLine.SelectedValue);
                string selectedline = Session["SelectedLine"].ToString();
                objUserBO.line_id = Convert.ToInt32(ddlLine.SelectedValue);
                BulkQRCode();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Dec 2017
        /// Desc : It calls ConvertToHTML method to convert the qr details in HTML code and converts to PDF by calling ConvertPDF and download. then it binds the QR details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void download_Click(object sender, EventArgs e)
        {
            string html = string.Empty;
            try
            {

                //string html = "<html><head>    <title>Insert Page Breaks Before and After HTML Elements Using CSS</title></head><body style=\"width: 1010px; font-family: 'Times New Roman'; font-size: 20px; margin: 5px\">    <div style=\"width: 100%; height: 500px; background-color: aliceblue; border: 2px solid gray; text-align: center\">        <div style=\"width: 100%; height: 200px\"></div>        A block <b>without any page break</b> style<br />        <br />        [ Follows a block with <i>page-break-before : always</i> and <i>page-break-after : always</i> styles ]    </div>    <div style=\"page-break-before: always; page-break-after: always; width: 100%; height: 500px; background-color: gainsboro; border: 2px solid gray; text-align: center\">        <div style=\"width: 100%; height: 200px\"></div>        A block with <b>page-break-before : always</b> and <b>page-break-after : always</b> styles<br />        <br />        <b>This block will be always rendered alone in a PDF page</b><br />        <br />        [ Follows a block with <i>page-break-after : always</i> style ]    </div>    <div style=\"page-break-after: always; width: 100%; height: 500px; background-color: beige; border: 2px solid gray; text-align: center\">        <div style=\"width: 100%; height: 200px\"></div>        A block with <b>page-break-after : always</b> style<br />        <br />        <b>Nothing will be rendered after this block in PDF page</b>        <br />        <br />        [ Follows a block <i>without any page break</i> style ]    </div>    <div style=\"width: 100%; height: 500px; background-color: aliceblue; border: 2px solid gray; text-align: center\">        <div style=\"width: 100%; height: 200px\"></div>        A block <b>without any page break</b> style<br />        <br />        [ Follows a block with <i>page-break-before : always</i> style ]    </div>    <div style=\"page-break-before: always; width: 100%; height: 500px; background-color: lightgray; border: 2px solid gray; text-align: center\">        <div style=\"width: 100%; height: 200px\"></div>        A block with <b>page-break-before : always</b> style<br />        <br />        <b>This block will always be rendered at the top of a PDF page</b>    </div></body></html>";
                html = "<html><head><title>Test</title></head><body>";
                html += ConvertToHTML();
                html += "</body></html>";
                ConvertPDF(html);
                GridBind();
                BulkQRCode();
                //Document pdfDoc = new Document(PageSize.A4, 25, 10, 25, 10);
                //PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                //pdfDoc.Open();
                //Paragraph Text = new Paragraph("This is test file");
                //pdfDoc.Add(Text);
                //Paragraph Text1 = new Paragraph("This is test file line 2");
                //pdfDoc.Add(Text1);
                //pdfWriter.CloseStream = false;
                //pdfDoc.Close();
                //Response.Buffer = true;
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("content-disposition", "attachment;filename=Example.pdf");
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.Write(pdfDoc);
                //Response.End();
            }
            catch (Exception ex)
            { Response.Write(ex.Message); }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Dec 2017
        /// Desc : It calls GetLineListDropDownBL method and save the result in session.
        /// </summary>
        private void GridBind()
        {
            DataTable dtResult;
            try
            {
                objUserBO = new UsersBO();
                dtResult = new DataTable();
                objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                dtResult = objComBL.GetLineListDropDownBL(objUserBO);
                //dtResult = objLocBL.GetLineDetailsBL(objLocBO);
                if (dtResult.Rows.Count > 0)
                {
                    Session["dtLine"] = dtResult;
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Dec 2017
        /// Desc : It generate the QR code for line stored in session and display on screen in formatted way.
        /// </summary>
        public void BulkQRCode()
        {
            string code;
            QRCodeGenerator qrGenerator;
            QRCodeGenerator.QRCode qrCode;
            try
            {
                DataTable dtLines = new DataTable();
                dtLines = (DataTable)Session["dtLine"];
                int top = 0;
                int j = 0;
                if (ddlLine.SelectedValue == "0")
                {
                    for (int i = 0; i < dtLines.Rows.Count; i++)
                    {
                        Panel divQR1 = new Panel();
                        divQR1.CssClass = "colQR1";
                        code = Convert.ToString(dtLines.Rows[i]["line_id"]);
                        System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
                        qrGenerator = new QRCodeGenerator();
                        qrCode = qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);
                        imgBarCode.Height = 113;
                        imgBarCode.Width = 113;
                        imgBarCode.ID = "image" + i;

                        Label lbl_title = new Label();
                        lbl_title.Text = "Layered Process Audit";
                        lbl_title.CssClass = "qr-title";
                        divQR1.Controls.Add(lbl_title);
                        Label lbl_title1 = new Label();
                        lbl_title1.Text = "QR Code";
                        lbl_title1.CssClass = "qr-title1";
                        divQR1.Controls.Add(lbl_title1);
                        divQR1.Controls.Add(new LiteralControl("<br/>"));


                        Label lbl_loc = new Label();
                        lbl_loc.Text = Convert.ToString(ddlLocation.SelectedItem);
                        lbl_loc.CssClass = "mn_location_span";
                        divQR1.Controls.Add(lbl_loc);
                        divQR1.Controls.Add(imgBarCode);

                        Label lbl_line = new Label();
                        lbl_line.Text = Convert.ToString(dtLines.Rows[i]["line_name"]);
                        lbl_line.CssClass = "feedback-title";
                        divQR1.Controls.Add(lbl_line);

                        using (Bitmap bitMap = qrCode.GetGraphic(20))
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                                byte[] byteImage = ms.ToArray();
                                imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                            }
                        }

                        divQR1.Controls.Add(new LiteralControl("<img style=\"width: 60px;float:right; margin: -40px 0 0 0;\" src=\"images/logo-neu_1.png\" />"));

                        pnlBulkQR1.Controls.Add(divQR1);

                        Panel divPrintQR1 = new Panel();
                        divPrintQR1.CssClass = "colQR1";
                        code = Convert.ToString(dtLines.Rows[i]["line_id"]);
                        System.Web.UI.WebControls.Image imgBarCodePrint = new System.Web.UI.WebControls.Image();
                        qrGenerator = new QRCodeGenerator();
                        qrCode = qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);
                        if (i <= 5)
                        {
                            imgBarCodePrint.Height = 114;
                            imgBarCodePrint.Width = 114;
                        }
                        else
                        {
                            imgBarCodePrint.Height = 122;
                            imgBarCodePrint.Width = 122;
                        }
                        imgBarCodePrint.ID = "imagePrint" + i;

                        Label lbl_printtitle = new Label();
                        lbl_printtitle.Text = "Layered Process Audit";
                        lbl_printtitle.CssClass = "qr-title";
                        divPrintQR1.Controls.Add(lbl_printtitle);
                        Label lbl_printtitle1 = new Label();
                        lbl_printtitle1.Text = "QR Code";
                        lbl_printtitle1.CssClass = "qr-title1";
                        divPrintQR1.Controls.Add(lbl_printtitle1);
                        divPrintQR1.Controls.Add(new LiteralControl("<br/>"));


                        Label lbl_printloc = new Label();
                        lbl_printloc.Text = Convert.ToString(ddlLocation.SelectedItem);
                        lbl_printloc.CssClass = "mn_location_span";
                        divPrintQR1.Controls.Add(lbl_printloc);
                        divPrintQR1.Controls.Add(imgBarCodePrint);

                        Label lbl_printline = new Label();
                        lbl_printline.Text = Convert.ToString(dtLines.Rows[i]["line_name"]);
                        lbl_printline.CssClass = "feedback-title";
                        divPrintQR1.Controls.Add(lbl_printline);

                        using (Bitmap bitMap = qrCode.GetGraphic(20))
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                                byte[] byteImage = ms.ToArray();
                                imgBarCodePrint.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                            }
                        }

                        divPrintQR1.Controls.Add(new LiteralControl("<img style=\"width: 60px;float:right; margin: -40px 0 0 0;\" src=\"images/logo-neu_1.png\" />"));
                        //<div style=\"height:60px;\"></div>

                        //Bitmap bm = new Bitmap(400, 400);
                        //divPrintQR1.DrawToBitmap(bm, new System.Drawing.Rectangle(0, 0, 400, 400));



                        pnlPrintBulkQR1.Controls.Add(divPrintQR1);
                        if (i % 4 == 0)
                        {
                            pnlPrintBulkQR1.Controls.Add(new LiteralControl("<div style=\"page-break-before: always; page-break-after: always; \">"));
                        }

                    }
                }

                else
                {

                    Panel divQR1 = new Panel();
                    divQR1.CssClass = "colQR1";
                    code = Convert.ToString(Session["SelectedLine"]);
                    // code = Convert.ToString(dtLines.Rows[i]["line_id"]);
                    System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
                    qrGenerator = new QRCodeGenerator();
                    qrCode = qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);
                    imgBarCode.Height = 113;
                    imgBarCode.Width = 113;
                    imgBarCode.ID = "image1";

                    Label lbl_title = new Label();
                    lbl_title.Text = "Layered Process Audit";
                    lbl_title.CssClass = "qr-title";
                    divQR1.Controls.Add(lbl_title);
                    Label lbl_title1 = new Label();
                    lbl_title1.Text = "QR Code";
                    lbl_title1.CssClass = "qr-title1";
                    divQR1.Controls.Add(lbl_title1);
                    divQR1.Controls.Add(new LiteralControl("<br/>"));


                    Label lbl_loc = new Label();
                    lbl_loc.Text = Convert.ToString(ddlLocation.SelectedItem);
                    lbl_loc.CssClass = "mn_location_span";
                    divQR1.Controls.Add(lbl_loc);
                    divQR1.Controls.Add(imgBarCode);

                    Label lbl_line = new Label();
                    lbl_line.Text = Convert.ToString(ddlLine.SelectedItem.Text);
                    lbl_line.CssClass = "feedback-title";
                    divQR1.Controls.Add(lbl_line);

                    using (Bitmap bitMap = qrCode.GetGraphic(20))
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                            byte[] byteImage = ms.ToArray();
                            imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                        }
                    }

                    divQR1.Controls.Add(new LiteralControl("<img style=\"width: 60px;float:right; margin: -40px 0 0 0;\" src=\"images/logo-neu_1.png\" />"));

                    pnlBulkQR1.Controls.Add(divQR1);

                    Panel divPrintQR1 = new Panel();
                    divPrintQR1.CssClass = "colQR1";
                    code = Convert.ToString(Session["SelectedLine"]);
                    //  code = Convert.ToString(dtLines.Rows[i]["line_id"]);
                    System.Web.UI.WebControls.Image imgBarCodePrint = new System.Web.UI.WebControls.Image();
                    qrGenerator = new QRCodeGenerator();
                    qrCode = qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);

                    imgBarCodePrint.Height = 138;
                    imgBarCodePrint.Width = 138;

                    imgBarCodePrint.ID = "imagePrint1";

                    Label lbl_printtitle = new Label();
                    lbl_printtitle.Text = "Layered Process Audit";
                    lbl_printtitle.CssClass = "qr-title";
                    divPrintQR1.Controls.Add(lbl_printtitle);
                    Label lbl_printtitle1 = new Label();
                    lbl_printtitle1.Text = "QR Code";
                    lbl_printtitle1.CssClass = "qr-title1";
                    divPrintQR1.Controls.Add(lbl_printtitle1);
                    divPrintQR1.Controls.Add(new LiteralControl("<br/>"));


                    Label lbl_printloc = new Label();
                    lbl_printloc.Text = Convert.ToString(ddlLocation.SelectedItem);
                    lbl_printloc.CssClass = "mn_location_span";
                    divPrintQR1.Controls.Add(lbl_printloc);
                    divPrintQR1.Controls.Add(imgBarCodePrint);

                    Label lbl_printline = new Label();
                    lbl_printline.Text = Convert.ToString(ddlLine.SelectedItem.Text);
                    lbl_printline.CssClass = "feedback-title";
                    divPrintQR1.Controls.Add(lbl_printline);

                    using (Bitmap bitMap = qrCode.GetGraphic(20))
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                            byte[] byteImage = ms.ToArray();
                            imgBarCodePrint.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
                        }
                    }

                    divPrintQR1.Controls.Add(new LiteralControl("<img style=\"width: 60px;float:right; margin: -40px 0 0 0;\" src=\"images/logo-neu_1.png\" />"));

                    pnlPrintBulkQR1.Controls.Add(divPrintQR1);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Dec 2017
        /// Desc :
        /// </summary>
        private void GetMasterDropdown()
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            DataTable dtLine = new DataTable();
            DataTable dtEmployee = new DataTable();
            DataTable dtPart = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        //if (Convert.ToString(Session["SiteAdminFlag"]) == "Y" && Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                        ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                        //    ddlRegion.Attributes["disabled"] = "disabled";
                        //}
                        //else
                        //    ddlRegion.Enabled = true;
                    }
                    else
                    {
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                }
                if (!string.IsNullOrEmpty(ddlRegion.SelectedValue))
                {
                    objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                    dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                    if (dtCountry.Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dtCountry;
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        //if (Convert.ToString(Session["SiteAdminFlag"]) == "Y" && Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                        ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                        //    ddlCountry.Attributes["disabled"] = "disabled";
                        //}
                        //else
                        //    ddlCountry.Enabled = true;
                    }
                    else
                    {
                        ddlCountry.Items.Clear();
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }
                if (!string.IsNullOrEmpty(ddlCountry.SelectedValue))
                {
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                    dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                    if (dtLocation.Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dtLocation;
                        ddlLocation.DataTextField = "location_name";
                        ddlLocation.DataValueField = "location_id";
                        ddlLocation.DataBind();
                        //if (Convert.ToString(Session["SiteAdminFlag"]) == "Y" && Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        //{
                        ddlLocation.SelectedValue = Convert.ToString(Session["LocationId"]);
                        //ddlLocation.Attributes["disabled"] = "disabled";
                        //}
                        //else
                        //    ddlLocation.Enabled = true;
                        Session["SelectedLocation"] = Convert.ToString(ddlLocation.SelectedValue);
                        Session["dtLocation"] = dtLocation;
                    }
                    else
                    {
                        ddlLocation.Items.Clear();
                        ddlLocation.DataSource = null;
                        ddlLocation.DataBind();
                    }
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }

                if (!string.IsNullOrEmpty(Convert.ToString(ddlLocation.SelectedValue)))
                {
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                    dtLine = objComBL.GetLineListDropDownBL(objUserBO);
                    if (dtLine.Rows.Count > 0)
                    {
                        ddlLine.DataSource = dtLine;
                        ddlLine.DataTextField = "line_name";
                        ddlLine.DataValueField = "line_id";
                        ddlLine.DataBind();
                        ddlLine.Items.Insert(0, new System.Web.UI.WebControls.ListItem("All", "0"));
                    }
                    else
                    {
                        ddlLine.Items.Clear();
                        ddlLine.DataSource = null;
                        ddlLine.DataBind();
                    }
                }
                else
                {
                    ddlLine.Items.Clear();
                    ddlLine.DataSource = null;
                    ddlLine.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Dec 2017
        /// Desc : It converts all the qr details in html elements in formatted way.
        /// </summary>
        /// <returns></returns>
        private string ConvertToHTML()
        {
            string html = string.Empty;
            string code;
            QRCodeGenerator qrGenerator;
            QRCodeGenerator.QRCode qrCode;
            string outputFileName = string.Empty;
            string imageURL = string.Empty;

            StringBuilder strHTMLBuilder = new StringBuilder();
            try
            {
                imageURL = Convert.ToString(ConfigurationManager.AppSettings["MHLogoForQR"]);
                DataTable dtLines = new DataTable();
                dtLines = (DataTable)Session["dtLine"];
                int j = 0;
                if (ddlLine.SelectedValue == "0")
                {
                    for (int i = 0; i < dtLines.Rows.Count; i++)
                    {
                        if (i % 6 == 0)
                        {
                            strHTMLBuilder.Append("<table style='border-collapse: collapse; border: 1px solid darkgray;'>");
                        }
                        if (i % 2 == 0)
                        {
                            strHTMLBuilder.Append("<tr height=300px>");
                        }
                        strHTMLBuilder.Append("<td width=500px style=' border: 1px solid darkgray; border-collapse: collapse; font-family:Gotham-Book; font-size:10px; text-align:center;'>");
                        strHTMLBuilder.Append("<div style='float: left; padding: 15px; border-style: groove;'>");
                        strHTMLBuilder.Append("<span style=' margin: 0 auto; display: block; text-align: center; font-size: 16px; font-weight: bold;'>Layered Process Audit</span><br/>");
                        strHTMLBuilder.Append("<span style='margin: 0 auto;display: block; text-align: center; font-size: 16px; font-weight: bold;'>QR Code</span><br/>");
                        strHTMLBuilder.Append("<span style='display: block; text-align: center; margin: 0 0 10px 0;'>" + Convert.ToString(ddlLocation.SelectedItem) + "</span><br/>");

                        code = Convert.ToString(dtLines.Rows[i]["line_id"]);
                        System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
                        qrGenerator = new QRCodeGenerator();
                        qrCode = qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);
                        imgBarCode.Height = 113;
                        imgBarCode.Width = 113;
                        imgBarCode.ID = "image1";
                        outputFileName = Convert.ToString(ConfigurationManager.AppSettings["QR_Images"]) + "qr_" + i + ".png";
                        using (Bitmap bitMap = qrCode.GetGraphic(20))
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                using (FileStream fs = new FileStream(outputFileName, FileMode.Create, FileAccess.ReadWrite))
                                {
                                    bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                                    byte[] bytes = ms.ToArray();
                                    fs.Write(bytes, 0, bytes.Length);
                                }
                                strHTMLBuilder.Append("<img style='width: 113px;' src='");
                                strHTMLBuilder.Append(outputFileName);
                                strHTMLBuilder.Append("' /><br/>");
                            }
                        }
                        strHTMLBuilder.Append("<span style=' margin: 20px auto; width: 150px; height: 30px; display: block; font-size: 10px; text-align: center;'>" + Convert.ToString(dtLines.Rows[i]["line_name"]) + "</span><br/>");
                        strHTMLBuilder.Append("<img src='");
                        strHTMLBuilder.Append(imageURL);
                        strHTMLBuilder.Append("' />");

                        strHTMLBuilder.Append("</div>");
                        strHTMLBuilder.Append("</td>");
                        if ((i + 1) % 2 == 0 || i == dtLines.Rows.Count - 1)
                        {
                            strHTMLBuilder.Append("</tr>");

                        }
                        if ((i + 1) % 6 == 0 || i == dtLines.Rows.Count - 1)
                        {
                            strHTMLBuilder.Append("</table>");
                        }
                        if ((i + 1) % 6 == 0)
                        {

                            strHTMLBuilder.Append("<div style='page-break-after: always; '></div>");

                        }

                    }
                }

                else
                {
                    strHTMLBuilder.Append("<table><tr>");

                    strHTMLBuilder.Append("<td width=500px style=' border: 1px solid darkgray; border-collapse: collapse; font-family:Gotham-Book; font-size:10px; text-align:center;'>");
                    strHTMLBuilder.Append("<div style='float: left; padding: 15px; border-style: groove;'>");
                    strHTMLBuilder.Append("<span style=' margin: 0 auto; display: block; text-align: center; font-size: 16px; font-weight: bold;'>Layered Process Audit</span><br/>");
                    strHTMLBuilder.Append("<span style='margin: 0 auto;display: block; text-align: center; font-size: 16px; font-weight: bold;'>QR Code</span><br/>");
                    strHTMLBuilder.Append("<span style='display: block; text-align: center; margin: 0 0 10px 0;'>" + Convert.ToString(ddlLocation.SelectedItem) + "</span><br/>");


                    code = Convert.ToString(Session["SelectedLine"]);
                    System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
                    qrGenerator = new QRCodeGenerator();
                    qrCode = qrGenerator.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);
                    imgBarCode.Height = 113;
                    imgBarCode.Width = 113;
                    imgBarCode.ID = "image1";
                    outputFileName = Convert.ToString(ConfigurationManager.AppSettings["QR_Images"]) + "qr_1.png";
                    using (Bitmap bitMap = qrCode.GetGraphic(20))
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (FileStream fs = new FileStream(outputFileName, FileMode.Create, FileAccess.ReadWrite))
                            {
                                bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                                byte[] bytes = ms.ToArray();
                                fs.Write(bytes, 0, bytes.Length);
                            }

                            strHTMLBuilder.Append("<img style='width: 113px;' src='");
                            strHTMLBuilder.Append(outputFileName);
                            strHTMLBuilder.Append("' /><br/>");
                        }
                    }
                    strHTMLBuilder.Append("<span style=' margin: 20px auto; width: 150px; height: 30px; display: block; font-size: 10px; text-align: center;\">" + Convert.ToString(ddlLine.SelectedItem.Text) + "</span><br/>");
                    strHTMLBuilder.Append("<img style='width: 60px;float:right; margin: -40px 0 0 400px;' src='");
                    strHTMLBuilder.Append(imageURL);
                    strHTMLBuilder.Append("' />");
                    strHTMLBuilder.Append("</div>");
                    strHTMLBuilder.Append("</td></tr></table>");
                }
                html = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

            return html;
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Dec 2017
        /// Desc :This function calls GeneratePdfFromHtml with the html data passed as parameter.
        /// </summary>
        /// <param name="example_html">html string</param>
        private void ConvertPDF(string example_html)
        {
            string filename = "QR_Details_" + Convert.ToString(Session["UserId"]) + ".pdf";
            string filepath = ConfigurationManager.AppSettings["QR_Folder"].ToString();
            var testFile = Path.Combine(filepath, filename);
            try
            {
                using (FileStream fs = new FileStream(Path.Combine(filepath, "test2.htm"), FileMode.Create))
                {
                    using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                    {
                        w.WriteLine(example_html);
                    }
                }

                GeneratePdfFromHtml(filepath, filename);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Dec 2017
        /// Desc : It converts the html page to pdf page by calling CreatePDF.
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="filename"></param>
        private void GeneratePdfFromHtml(string filepath, string filename)
        {
            string outputFilename = Path.Combine(filepath, filename);
            string inputFilename = Path.Combine(filepath, "test2.htm");
            try
            {
                using (var input = new FileStream(inputFilename, FileMode.Open))
                using (var output = new FileStream(outputFilename, FileMode.Create))
                {
                    using (var document = new Document(PageSize.A4, 30, 30, 30, 30))
                    {
                        CreatePdf(filepath, filename, input, output);
                        DownloadQRCodes(filepath, filename);
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Dec 2017
        /// Desc : The created pdf will be download on click on Download button
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="filename"></param>
        private void DownloadQRCodes(string filepath, string filename)
        {
            try
            {
                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.ClearContent();
                response.Clear();
                response.ContentType = "text/plain";
                response.AddHeader("Content-Disposition",
                                   "attachment; filename=" + Convert.ToString(filepath + filename) + ";");
                response.TransmitFile(Convert.ToString(filepath + filename));
                response.Flush();
                response.End();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 11 Dec 2017
        /// Desc : It creates pdf bt ParseXHtml.
        /// </summary>
        /// <param name="filepath">the pdf file path</param>
        /// <param name="filename">pdf file name</param>
        /// <param name="htmlInput">html format stream</param>
        /// <param name="pdfOutput">pdf stream</param>
        public void CreatePdf(string filepath, string filename, Stream htmlInput, Stream pdfOutput)
        {
            using (var document = new Document(PageSize.A4, 30, 30, 30, 30))
            {
                var writer = PdfWriter.GetInstance(document, pdfOutput);
                var worker = XMLWorkerHelper.GetInstance();

                document.Open();
                //document.Add(new Paragraph("QR Details"));
                worker.ParseXHtml(writer, document, htmlInput, null, Encoding.UTF8, new MHLPA_Application.AuditResult.UnicodeFontFactory());
                document.Close();
            }
        }
        #endregion

    }
}