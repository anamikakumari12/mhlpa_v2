﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DashboardOld.aspx.cs" Inherits="MHLPA_Application.DashboardOld" MasterPageFile="~/Site.Master" %>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %> --%>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"  Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>   

<asp:Content ID="Content1"  ContentPlaceHolderID="HeadContent" runat="server">
        <link href="css/calender.css" rel="stylesheet" />
    <script src="js/calender.js"></script>

     <script type="text/javascript">

         $(document).ready(function () {
             $("#MainContent_txtDate").datepicker({
                 showOn: "both",
                 buttonImageOnly: true,
                 buttonText: "",
                 changeYear: true,
                 changeMonth: true,
                 yearRange: "c-20:c+50",
                 // minDate: new Date(),
                 dateFormat: 'dd-mm-yy',
                 //buttonImage: "images/calander_icon.png",
             });
         });
         $("#MainContent_txtDate").datepicker({
             showOn: "both",
             buttonImageOnly: true,
             buttonText: "",
             changeYear: true,
             changeMonth: true,
             yearRange: "c-20:c+50",
             // minDate: new Date(),
             dateFormat: 'dd-mm-yy',
             //buttonImage: "images/calander_icon.png",
         });
         function ValidateControls() {
             var err_flag = 0;
             if ($('#MainContent_ddlRegion').val() == "" || $('#MainContent_ddlRegion').val() == null) {
                 $('#MainContent_ddlRegion').css('border-color', 'red');
                 err_flag = 1;
             }
             else {
                 $('#MainContent_ddlRegion').css('border-color', '');
             }
             if ($('#MainContent_ddlCountry').val() == "" || $('#MainContent_ddlCountry').val() == null) {
                 $('#MainContent_ddlCountry').css('border-color', 'red');

                 err_flag = 1;
             }
             else {
                 $('#MainContent_ddlCountry').css('border-color', '');
             }

             if ($('#MainContent_ddlLocation').val() == "" || $('#MainContent_ddlLocation').val() == null) {
                 $('#MainContent_ddlLocation').css('border-color', 'red');

                 err_flag = 1;
             }
             else {
                 $('#MainContent_ddlLocation').css('border-color', '');
             }

             if ($('#MainContent_ddlLineName').val() == "" || $('#MainContent_ddlLineName').val() == null) {
                 $('#MainContent_ddlLineName').css('border-color', 'red');

                 err_flag = 1;
             }
             else {
                 $('#MainContent_ddlLineName').css('border-color', '');
             }
             
             if ($('#MainContent_txtDate').val() == "") {
                 $('#MainContent_txtDate').css('border-color', 'red');
                 err_flag = 1;
             }
             else {

                 $('#MainContent_txtDate').css('border-color', '');
             }

             if (err_flag == 0) {
                 $('#MainContent_lblError').text('');
                 return setmin();

             }
             else {
                 $('#MainContent_lblError').text('Please enter all the mandatory fields.');
                 return false;
             }
         }
    </script>
    <style>
        .mn_mrg_left
        {
            padding-left:0;
            box-shadow:0px 0px 3px #aaa;
            background:#fff;
        }
        .mn_mrg_right
        {
            padding-right:0;
            box-shadow:0px 0px 3px #aaa;
            background:#fff;
        }
        .mn_mrg
        {
            padding: 0;
            box-shadow:0px 0px 3px #aaa;
            background:#fff;
        }
        .mn_fetch .mn_mrg_left, .mn_fetch .mn_mrg, .mn_fetch .mn_mrg_right 
        {
            box-shadow:none;
            padding: 5px;
        }
        .mn_fetch, .ma_lpa_result
        {
            width: 100%;
            float: left;
            background: #fff;
            border: solid 1px #ddd;
        }
        .ma_lpa_result
        {
            margin:10px 0;
        }
        .mn_none_mar
        {
            margin:0 0 10px 0
        }
        #MainContent_pnlReport
        {
            width:100%;
            float:left
        }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="col-md-12">
                            <div class='block-web' style='float: left; width: 100%; height:36px;'>
                                <div class="header">
                                    <div class="crumbs">
                                        <!-- Start : Breadcrumbs -->
                                        <ul id="breadcrumbs" class="breadcrumb">
                                            <li>
                                                <a class="mn_breadcrumb">Reports</a>
                                            </li>
                                            <li class="current">Dashboard</li>

                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
    <%--from--%>
        <div class="md_main">
            <div class="row">
                <div class="mannHummel" id="mannHummelId">
                    <div class="clearfix" style="width:40px;"></div>
                     <asp:Panel runat="server" ID="pnlFilters">
                        <div class="filter_panel">
                            <div>
                                <div class="col-md-4 filter_label" style="width:14%"><p>Region* : </p></div> 
                            <div class="col-md-4" style="width:20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlRegion" OnSelectedIndexChanged="ddlRegion_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                                <div class="col-md-4 filter_label" style="width:13%"><p>Country* : </p></div> 
                            <div class="col-md-4" style="width:20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlCountry" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-md-4 filter_label" style="width:13%"><p>Location* : </p></div> 
                            <div class="col-md-4" style="width:20%">
                                <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLocation" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                                </div>
                            <div>
                                <div class="col-md-4 filter_label" style="width:14%"><p>Line Name/No* :</p></div> 
                                <div class="col-md-4" style="width:20%">
                                    <asp:DropDownList class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="ddlLineName" AutoPostBack="true" OnSelectedIndexChanged="ddlLineName_SelectedIndexChanged"></asp:DropDownList>

                            </div>
                               
                            <div class="col-md-4 filter_label" style="width:13%"><p>Date* : </p></div> 
                            <div class="col-md-4" style="width:20%">
                                <asp:TextBox class="form-control1 mn_inp control3 filter_select_control" runat="server" ID="txtDate"></asp:TextBox>
                            </div>

                                <div class="col-md-4 filter_label" style="width:12%"> <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label></div> 
                            <div class="col-md-4"  style="width:21%">
                                <div class="col-md-5" style="float:left">
                                <asp:Button runat="server" CssClass="btn-add" ID="btnClear" style="margin-left: 4px; float:right;" Text="Clear" OnClick="btnClear_Click" />
                                </div><div class="col-md-7">
                                <asp:Button runat="server" CssClass="btn-add" ID="btnFilter" style="margin-left: 4px; float:right;" Text="Fetch Report" OnClick="btnFilter_Click" OnClientClick="return ValidateControls();"/>
                            </div></div>
                                </div>
                            
                            </div>
                            
                            
                      
                    </asp:Panel>
                    <div class="clearfix" style="height:5px;"></div>
                    <asp:Panel runat="server" Visible="false" ID="pnlReport">
                        <div class="ma_lpa_result">
                        <asp:Chart ID="Chart1" runat="server" style="margin:0 auto; display:block; height:auto"
        BackGradientStyle="LeftRight" Palette="None"   
        PaletteCustomColors="green" Width="1000px">
                            <Titles>
                                <asp:Title Font="16pt, style=Bold" Name="LPA Result"></asp:Title>
                            </Titles>
                            <Series>
                               
                                <asp:Series ChartType="Area" Name="Previous Year"></asp:Series>
                                 <asp:Series ChartType="Column" Name="Actual Year"></asp:Series>
                            </Series>
                            <Legends>
                                <asp:Legend Name="Actual year" ShadowColor="DarkGreen"></asp:Legend>
                                <asp:Legend Name="Previous Year" ShadowColor="LightGray"></asp:Legend>
                            </Legends>
                            <ChartAreas>
                                <asp:ChartArea Name="cA1"></asp:ChartArea>
                            </ChartAreas>

                        </asp:Chart>
                    </div>
                         <div class="clearfix" style="height:5px;"></div>
                        <div class="col-md-4 mn_mrg_left">
                        <asp:Chart ID="ChartPerSection" runat="server" style="margin:0 auto; display:block"
        BackGradientStyle="LeftRight" Palette="None"   
        PaletteCustomColors="green" Width="400px">
                            <Titles>
                                <asp:Title Font="16pt, style=Bold" Name="t2"></asp:Title>
                            </Titles>
                            <Series>
                                <asp:Series Name="Series1"  ChartType="Radar"></asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ca2"></asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>
                            </div>
                        <div class="col-md-8 mn_mrg_right">
                        <asp:Chart ID="ChartPerSectionMonthly" runat="server" style="margin:0 auto; display:block"
        BackGradientStyle="LeftRight" Palette="None"   
        PaletteCustomColors="green" Width="800px">
                            <Titles>
                                <asp:Title Font="16pt, style=Bold" Name="t3"></asp:Title>
                            </Titles>
                            <Series>
                                <asp:Series ChartType="Line"></asp:Series>
                                <asp:Series ChartType="Line"></asp:Series>
                                <asp:Series ChartType="Line"></asp:Series>
                                <asp:Series ChartType="Line"></asp:Series>
                                <asp:Series ChartType="Line"></asp:Series>
                                <asp:Series ChartType="Line"></asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ca3"></asp:ChartArea>
                            </ChartAreas>
                            <Legends>
                               
                                   <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" LegendStyle="Row" />

                            </Legends>
                        </asp:Chart>
                        </div>
                       <div class="mn_fetch">
                            <div class="col-md-4 mn_mrg_left" style="background-color:white;">
                               <asp:Label runat="server" ID="sec1"></asp:Label>
                            </div>
                            <div class="col-md-4 mn_mrg" style="background-color:white;">
                               <asp:Label runat="server" ID="sec2"></asp:Label>
                            </div>
                            <div class="col-md-4 mn_mrg_right" style="background-color:white;">
                               <asp:Label runat="server" ID="sec3"></asp:Label>
                            </div>
                            <div class="col-md-4 mn_mrg_left" style="background-color:white;">
                               <asp:Label runat="server" ID="sec4"></asp:Label>
                            </div>
                            <div class="col-md-4 mn_mrg" style="background-color:white;">
                               <asp:Label runat="server" ID="sec5"></asp:Label>
                            </div>
                            <div class="col-md-4 mn_mrg_right" style="background-color:white;">
                               <asp:Label runat="server" ID="sec6"></asp:Label>
                            </div>
                        </div>
                         <div class="clearfix" style="height:5px;"></div>
                        <div class="ma_lpa_result">
                        <asp:Chart ID="ChartPerLine" runat="server" style="margin:0 auto; display:block"
        BackGradientStyle="LeftRight" Palette="None"   
        PaletteCustomColors="green" Width="1000px">
                            <Titles>
                                <asp:Title Font="16pt, style=Bold" Name="t4"></asp:Title>
                            </Titles>
                            <Series>
                                <asp:Series ChartType="Column"  Name="Average Score"></asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ca4"></asp:ChartArea>
                            </ChartAreas>
                            <Legends>
                                 <asp:Legend Name="Average Score" ShadowColor="DarkGreen"></asp:Legend>
                            </Legends>
                        </asp:Chart>
                        </div>
                         <div class="clearfix"></div>
                        <div class="ma_lpa_result mn_none_mar">
                        <asp:Chart ID="ChartPerQuestion" runat="server" style="margin:0 auto; display:block"
        BackGradientStyle="LeftRight" Palette="None"   
        PaletteCustomColors="green" Width="1000px">
                            <Titles>
                                <asp:Title Font="16pt, style=Bold" Name="t5"></asp:Title>
                            </Titles>
                            <Series>
                                <asp:Series ChartType="Column" Name="Average Score"></asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ca5"></asp:ChartArea>
                            </ChartAreas>
                            <%--<Legends>
                                 <asp:Legend Name="Average Score" ShadowColor="DarkGreen"></asp:Legend>
                            </Legends>--%>
                        </asp:Chart>
                    </div>
                        <div class="clearfix"></div>
                     <div class="ma_lpa_result">
                        <asp:Chart ID="chartComparision" runat="server" style="margin:0 auto; display:block; height:auto"
        BackGradientStyle="LeftRight" Palette="None"   
        PaletteCustomColors="green" Width="1000px">
                            <Titles>
                                <asp:Title Font="16pt, style=Bold" Name="LPA Result"></asp:Title>
                            </Titles>
                            <Series>
                               
                                <asp:Series ChartType="Area" Name="Previous Year"></asp:Series>
                                 <asp:Series ChartType="Column" Name="Actual Year"></asp:Series>
                            </Series>
                            <Legends>
                                <asp:Legend Name="Actual year" ShadowColor="DarkGreen"></asp:Legend>
                                <asp:Legend Name="Previous Year" ShadowColor="LightGray"></asp:Legend>
                            </Legends>
                            <ChartAreas>
                                <asp:ChartArea Name="cA1"></asp:ChartArea>
                            </ChartAreas>

                        </asp:Chart>
                    </div>
                    </asp:Panel>
</div>
                </div>
            <%--end--%>
       <%-- <asp:ScriptManager ID="scriptManagerReport" runat="server"> 
         </asp:ScriptManager> 
  
        <rsweb:ReportViewer  runat ="server" ProcessingMode="Remote" Width="99.9%" Height="100%"  id="rvSiteMapping" > 
            <ServerReport DisplayName="MainReport" Timeout="600000" />
        </rsweb:ReportViewer>  --%>                 
    </div> 
     </asp:Content>