﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MHLPA_BusinessObject;
using MHLPA_BusinessLogic;
using System.Data;
using System.Globalization;
using QRCoder;
using System.Drawing;
using System.IO;
using AjaxControlToolkit;

namespace MHLPA_Application
{
    public partial class Location : System.Web.UI.Page
    {
        #region Global Declaration
        LocationBO objLocBO;
        LocationBL objLocBL;
        DataSet dsDropDownData;
        UsersBO objUserBO;
        CommonBL objComBL;
        CommonFunctions objCom = new CommonFunctions();
        #endregion

        #region Events

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: Check for login session and load the login page if session timeout, if not, load all the location details in a grid and the view panel with first row data.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int UserId;
                int vwlinprod_id;
                if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }
                else
                {
                    UserId = Convert.ToInt32(Session["UserId"]);
                }
                if (!Page.IsPostBack)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
                    {
                        GridBind();
                        DataTable dtGrid = new DataTable();
                        dtGrid = (DataTable)Session["dtLocation"];

                        vwlinprod_id = Convert.ToInt32(dtGrid.Rows[0]["location_id"]);
                        Session["location_id"] = vwlinprod_id;
                        DataRow drfirst = selectedRow(vwlinprod_id);
                        LoadViewpanel(drfirst);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "showViewShiftTime();", true);
                        Session["PrintFlag"] = 0;
                        if (Convert.ToString(Session["GlobalAdminFlag"]) == "Y")
                            btnAdd.Visible = true;
                        else
                            btnAdd.Visible = false;
                    }
                }
                else
                {
                    //if (Convert.ToString(Session["Print"]) == "yes")
                    //{
                    //    Session["Print"] = "No";
                    //    System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
                    //    imgBarCode = (System.Web.UI.WebControls.Image)pnlQRCode.FindControl("image");
                    //    if (imgBarCode == null)
                    //    {
                    //        GridBind();
                    //        DataTable dtGrid = new DataTable();
                    //        dtGrid = (DataTable)Session["dtLocation"];
                    //        vwlinprod_id = Convert.ToInt32(Session["location_id"]);
                    //        DataRow drfirst = selectedRow(vwlinprod_id);
                    //        LoadViewpanel(drfirst);
                    //    }
                    //}
                    
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of Add button, it will clear all the field from edit panel and display Add panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAdd_Click(object sender, EventArgs e)
        {

            try
            {
                Clear();
                GetMasterDropdown();
                editpanel.Visible = true;
                viewpanel.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }


        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of Save button, it will call the SaveLocationBL with all the details in edit panel to save to database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            LocationBO Output = new LocationBO();
            try
            {
                objLocBO = new LocationBO();
                objLocBL = new LocationBL();
                objLocBO.UserId = Convert.ToInt32(Session["UserId"]);
                if (!string.IsNullOrEmpty(Convert.ToString(Session["location_id"])))
                    objLocBO.location_id = Convert.ToInt32(Session["location_id"]);
                objLocBO.region_name = Convert.ToString(txtRegion.Text);
                objLocBO.country_name = Convert.ToString(txtCountry.Text);
                objLocBO.location_name = Convert.ToString(txtLocation.Text);
                objLocBO.no_of_shifts = Convert.ToInt32(txtNoOfShift.SelectedValue);
                if (objLocBO.no_of_shifts == 1)
                {
                    objLocBO.shift1start = TimeSpan.Parse(Convert.ToString(txtIstart.Text));
                    objLocBO.shift1end = TimeSpan.Parse(Convert.ToString(txtIend.Text));
                }
                else if (objLocBO.no_of_shifts == 2)
                {
                    objLocBO.shift1start = TimeSpan.Parse(Convert.ToString(txtIstart.Text));
                    objLocBO.shift1end = TimeSpan.Parse(Convert.ToString(txtIend.Text));
                    objLocBO.shift2start = TimeSpan.Parse(Convert.ToString(txtIIstart.Text));
                    objLocBO.shift2end = TimeSpan.Parse(Convert.ToString(txtIIend.Text));
                }
                else if (objLocBO.no_of_shifts == 3)
                {
                    objLocBO.shift1start = TimeSpan.Parse(Convert.ToString(txtIstart.Text));
                    objLocBO.shift1end = TimeSpan.Parse(Convert.ToString(txtIend.Text));
                    objLocBO.shift2start = TimeSpan.Parse(Convert.ToString(txtIIstart.Text));
                    objLocBO.shift2end = TimeSpan.Parse(Convert.ToString(txtIIend.Text));
                    objLocBO.shift3start = TimeSpan.Parse(Convert.ToString(txtIIIstart.Text));
                    objLocBO.shift3end = TimeSpan.Parse(Convert.ToString(txtIIIend.Text));
                }
                
                string timezone1 = Convert.ToString(ddlTimeZone.SelectedValue);
                if(timezone1.Substring(0,1)=="-")
                timezone1 = timezone1.Substring(0, 6);
                else
                timezone1=string.Concat("+",timezone1.Substring(0, 5));
                objLocBO.p_timezone_code = Convert.ToString(timezone1);
                objLocBO.p_timezone_desc = Convert.ToString(ddlTimeZone.SelectedItem);
                Output = objLocBL.SaveLocationBL(objLocBO);

                if (Output.error_code == 0)
                {
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + Output.error_msg + "');", true);
                    int loc_id = objLocBO.location_id;
                    GridBind();
                    DataTable dtGrid = new DataTable();
                    dtGrid = (DataTable)Session["dtLocation"];
                    DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["location_id"]));
                    LoadViewpanel(drfirst);
                    if (loc_id== 0)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "showViewShiftTime();alert('A new location is added successfully.');", true);
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "showViewShiftTime();alert('Location is modified successfully.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('" + Output.error_msg + "');", true);
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of any row in gridview, the selected row details will be loaded in view panel by calling LoadViewpanel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Select(object sender, EventArgs e)
        {
            try
            {
                int location_id = Convert.ToInt32((sender as LinkButton).CommandArgument);
                Session["location_id"] = location_id;
                DataRow drselect = selectedRow(location_id);
                LoadViewpanel(drselect);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "showViewShiftTime();", true);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc:On click of any row in gridview, the selected row details will be loaded in edit panel by calling LoadEditPanel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Edit(object sender, EventArgs e)
        {
            try
            {
                int location_id = Convert.ToInt32((sender as ImageButton).CommandArgument);
                Session["location_id"] = location_id;
                DataRow drselect = selectedRow(location_id);
                LoadEditPanel(drselect);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On selection of region, the country down will be loaded accordingly by calling GetCountryBasedOnRegionBL.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                txtRegion.Text = Convert.ToString(ddlRegion.SelectedItem);
                PopupControlExtender1.Commit(Convert.ToString(ddlRegion.SelectedItem));
                PopupControlExtender p = PopupControlExtender.GetProxyForCurrentPopup(this);
                p.Cancel();
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                if (dtCountry.Rows.Count > 0)
                {
                    ddlCountry.DataSource = dtCountry;
                    ddlCountry.DataTextField = "country_name";
                    ddlCountry.DataValueField = "country_id";
                    ddlCountry.DataBind();
                    //ddlCountry.SelectedIndex = 0;
                    //txtCountry.Text = Convert.ToString(ddlCountry.SelectedItem);
                    //objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }

                //dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                //if (dtLocation.Rows.Count > 0)
                //{
                //    ddlLocation.DataSource = dtLocation;
                //    ddlLocation.DataTextField = "location_name";
                //    ddlLocation.DataValueField = "location_id";
                //    ddlLocation.DataBind();
                //    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //}
                //else
                //{
                //    ddlLocation.Items.Clear();
                //    ddlLocation.DataSource = null;
                //    ddlLocation.DataBind();
                //}
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// 
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtLocation = new DataTable();
            try
            {
                txtCountry.Text = Convert.ToString(ddlCountry.SelectedItem);
                PopupControlExtender2.Commit(Convert.ToString(ddlCountry.SelectedItem));
                PopupControlExtender p = PopupControlExtender.GetProxyForCurrentPopup(this);
                p.Cancel();
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                
                //dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                //if (dtLocation.Rows.Count > 0)
                //{
                //    ddlLocation.DataSource = dtLocation;
                //    ddlLocation.DataTextField = "location_name";
                //    ddlLocation.DataValueField = "location_id";
                //    ddlLocation.DataBind();
                //  //  objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                //}
                //else
                //{
                //    ddlLocation.Items.Clear();
                //    ddlLocation.DataSource = null;
                //    ddlLocation.DataBind();
                //}
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        //protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        txtLocation.Text = Convert.ToString(ddlLocation.SelectedItem);
        //        PopupControlExtender3.Commit(Convert.ToString(ddlLocation.SelectedItem));
        //        PopupControlExtender p = PopupControlExtender.GetProxyForCurrentPopup(this);
        //        p.Cancel();
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of pages in gridview, the selected page will be loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdLineProduct_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdLocation.PageIndex = e.NewPageIndex;
                grdLocation.DataSource = (DataTable)Session["FilteredData"];
                grdLocation.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void grdLocation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataTable dtData;
            DropDownList ddlFilter;
            DataView view;
            DataTable distinctValues;
            try
            {

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    ddlFilter = e.Row.FindControl("ddlRegion_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["dtOriginalLocation"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalLocation"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "region_name");
                        DataView dv = distinctValues.DefaultView;
                        dv.Sort = "region_name asc";
                        distinctValues = dv.ToTable();
                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "region_name";
                        ddlFilter.DataValueField = "region_name";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedRegion_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedRegion_grid"]);
                        }
                    }
                    ddlFilter = e.Row.FindControl("ddlCountry_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["FilteredRegionData"] != null)
                        {
                            dtData = (DataTable)Session["FilteredRegionData"];
                        }
                        else if (Session["dtOriginalLocation"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalLocation"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "country_name");
                        DataView dv = distinctValues.DefaultView;
                        dv.Sort = "country_name asc";
                        distinctValues = dv.ToTable();
                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "country_name";
                        ddlFilter.DataValueField = "country_name";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedCountry_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedCountry_grid"]);
                        }
                        if (ddlFilter.SelectedValue == "0"||ddlFilter.SelectedValue == "")
                        {
                            Session["FilteredCountryData"] = Session["FilteredRegionData"];
                        }
                    }
                    ddlFilter = e.Row.FindControl("ddlLocation_grid") as DropDownList;
                    if (ddlFilter != null)
                    {
                        dtData = new DataTable();
                        if (Session["FilteredCountryData"] != null)
                        {
                            dtData = (DataTable)Session["FilteredCountryData"];
                        }
                        else if (Session["FilteredRegionData"] != null)
                        {
                            dtData = (DataTable)Session["FilteredRegionData"];
                        }
                        else if (Session["dtOriginalLocation"] != null)
                        {
                            dtData = (DataTable)Session["dtOriginalLocation"];
                        }
                        view = new DataView(dtData);
                        distinctValues = view.ToTable(true, "location_name");
                        DataView dv = distinctValues.DefaultView;
                        dv.Sort = "location_name asc";
                        distinctValues = dv.ToTable();
                        ddlFilter.DataSource = distinctValues;
                        ddlFilter.DataTextField = "location_name";
                        ddlFilter.DataValueField = "location_name";
                        ddlFilter.DataBind();
                        ddlFilter.Items.Insert(0, new ListItem("All", "0"));
                        if (Session["ddlSelectedLocation_grid"] != null)
                        {
                            ddlFilter.SelectedValue = Convert.ToString(Session["ddlSelectedLocation_grid"]);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlRegion_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();
                if (Session["dtLocation"] != null)
                {
                    dtData = (DataTable)Session["dtLocation"];
                }
                DropDownList ddlCountry = (DropDownList)sender;
                Session["ddlSelectedRegion_grid"] = Convert.ToString(ddlCountry.SelectedValue);
                Session["ddlSelectedLocation_grid"] = "All";
                Session["ddlSelectedCountry_grid"] = "All";
                if (Convert.ToString(ddlCountry.SelectedValue) == "0")
                {
                    Session["FilteredData"] = dtData;
                    Session["FilteredRegionData"] = dtData;
                    grdLocation.DataSource = dtData;
                    grdLocation.DataBind();
                }
                else
                {
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("region_name") == Convert.ToString(ddlCountry.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    Session["FilteredRegionData"] = dtFilterData;
                    grdLocation.DataSource = dtFilterData;
                    grdLocation.DataBind();
                }
                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["location_id"]));
                LoadViewpanel(drfirst);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "showViewShiftTime();", true);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLocation_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();

                if (Session["FilteredCountryData"] != null)
                {
                    dtData = (DataTable)Session["FilteredCountryData"];
                }
                else if (Session["FilteredRegionData"] != null)
                {
                    dtData = (DataTable)Session["FilteredRegionData"];
                }
                else if (Session["dtLocation"] != null)
                {
                    dtData = (DataTable)Session["dtLocation"];
                }
                DropDownList ddlCountry = (DropDownList)sender;
                Session["ddlSelectedLocation_grid"] = Convert.ToString(ddlCountry.SelectedValue);
                if (Convert.ToString(ddlCountry.SelectedValue) == "0")
                {
                    Session["FilteredData"] = dtData;
                    grdLocation.DataSource = dtData;
                    grdLocation.DataBind();
                }
                else
                {
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("location_name") == Convert.ToString(ddlCountry.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    grdLocation.DataSource = dtFilterData;
                    grdLocation.DataBind();
                }
                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["location_id"]));
                LoadViewpanel(drfirst);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "showViewShiftTime();", true);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCountry_grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtData;
            DataTable dtFilterData = new DataTable();
            try
            {
                dtData = new DataTable();

                if (Session["FilteredRegionData"] != null)
                {
                    dtData = (DataTable)Session["FilteredRegionData"];
                }
                else if (Session["dtLocation"] != null)
                {
                    dtData = (DataTable)Session["dtLocation"];
                }
                DropDownList ddlCountry = (DropDownList)sender;
                Session["ddlSelectedCountry_grid"] = Convert.ToString(ddlCountry.SelectedValue);
                Session["ddlSelectedLocation_grid"] = "All";
                if (Convert.ToString(ddlCountry.SelectedValue) == "0")
                {
                    Session["FilteredData"] = dtData;
                    Session["FilteredCountryData"] = dtData;
                    grdLocation.DataSource = dtData;
                    grdLocation.DataBind();
                }
                else
                {
                    var rows = from row in dtData.AsEnumerable()
                               where row.Field<string>("country_name") == Convert.ToString(ddlCountry.SelectedValue)
                               select row;
                    DataRow[] rowsArray = rows.ToArray();
                    dtFilterData = rows.CopyToDataTable();
                    Session["FilteredData"] = dtFilterData;
                    Session["FilteredCountryData"] = dtFilterData;
                    grdLocation.DataSource = dtFilterData;
                    grdLocation.DataBind();
                }
                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["FilteredData"];
                DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["location_id"]));
                LoadViewpanel(drfirst);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "showViewShiftTime();", true);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods
        public void BindTime()
        {
            ddlTimeZone.DataSource = TimeZoneInfo.GetSystemTimeZones();//binding time zone list in drop down list
            ddlTimeZone.DataValueField = "BaseUtcOffset";
            ddlTimeZone.DataTextField = "DisplayName";
            ddlTimeZone.DataBind();
            TimeZone zone = TimeZone.CurrentTimeZone;
            TimeSpan offset = zone.GetUtcOffset(DateTime.Now);
            ddlTimeZone.SelectedValue = Convert.ToString(offset);
            //DateTime start = DateTime.ParseExact("00:00", "HH:mm", null);
            //DateTime end = DateTime.ParseExact("23:30", "HH:mm", null);
            //int interval = 30;
            //List<string> lstTimeIntervals = new List<string>();
            //for (DateTime i = start; i < end; i = i.AddMinutes(interval))
            //    lstTimeIntervals.Add(i.ToString("HH:mm"));

            //txtIIstart.DataSource = lstTimeIntervals;
            //txtIIstart.DataBind();
            //txtIIstart.Items.Insert(0,new ListItem("--Select--","0"));
            //txtIstart.DataSource = lstTimeIntervals;
            //txtIstart.DataBind();
            //txtIstart.Items.Insert(0, new ListItem("--Select--","0"));
            //txtIIIstart.DataSource = lstTimeIntervals;
            //txtIIIstart.DataBind();
            //txtIIIstart.Items.Insert(0, new ListItem("--Select--","0"));
            //txtIIend.DataSource = lstTimeIntervals;
            //txtIIend.DataBind();
            //txtIIend.Items.Insert(0, new ListItem("--Select--", "0"));
            //txtIend.DataSource = lstTimeIntervals;
            //txtIend.DataBind();
            //txtIend.Items.Insert(0, new ListItem("--Select--", "0"));
            //txtIIIend.DataSource = lstTimeIntervals;
            //txtIIIend.DataBind();
            //txtIIIend.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        private void GridBind()
        {
            DataTable dtResult;
            try
            {
                objLocBO = new LocationBO();
                objLocBL = new LocationBL();
                dtResult = new DataTable();
                objLocBO.UserId = Convert.ToInt32(Session["UserId"]);
                dtResult = objLocBL.GetLocationDetailsBL(objLocBO);
                if (dtResult.Rows.Count > 0)
                {
                    grdLocation.DataSource = dtResult;
                    Session["dtLocation"] = dtResult;
                    Session["dtOriginalLocation"] = dtResult;
                    Session["ddlSelectedRegion_grid"] = null;
                    Session["ddlSelectedCountry_grid"] = null;
                    Session["ddlSelectedLocation_grid"] = null;
                    Session["FilteredData"] = dtResult;
                    Session["FilteredRegionData"] = null;
                    Session["FilteredCountryData"] = null;
                }
                else
                {
                    grdLocation.DataSource = null;
                }
                grdLocation.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void LoadViewpanel(DataRow drselect)
        {
            try
            {
                lblregion.Text = Convert.ToString(drselect["region_name"]);
                lblCountry.Text = Convert.ToString(drselect["country_name"]);
                lblLocation.Text = Convert.ToString(drselect["location_name"]);
                lblshift.Text = Convert.ToString(drselect["no_of_shifts"]);
                //lblIstart.Text = Convert.ToString(drselect["shift1_start_time"]);
                //lblIend.Text = Convert.ToString(drselect["shift1_end_time"]);
                //lblIIstart.Text = Convert.ToString(drselect["shift2_start_time"]);
                //lblIIend.Text = Convert.ToString(drselect["shift2_end_time"]);
                //lblIIIstart.Text = Convert.ToString(drselect["shift3_start_time"]);
                //lblIIIend.Text = Convert.ToString(drselect["shift3_end_time"]);

                lblIstart.Text = TimeSpan.Parse(Convert.ToString(drselect["shift1_start_time"])).ToString(@"hh\:mm");
                lblIend.Text = TimeSpan.Parse(Convert.ToString(drselect["shift1_end_time"])).ToString(@"hh\:mm");
                lblIIstart.Text = TimeSpan.Parse(Convert.ToString(drselect["shift2_start_time"])).ToString(@"hh\:mm");
                lblIIend.Text = TimeSpan.Parse(Convert.ToString(drselect["shift2_end_time"])).ToString(@"hh\:mm");
                lblIIIstart.Text = TimeSpan.Parse(Convert.ToString(drselect["shift3_start_time"])).ToString(@"hh\:mm");
                lblIIIend.Text = TimeSpan.Parse(Convert.ToString(drselect["shift3_end_time"])).ToString(@"hh\:mm");

                //if (lblshift.Text == "1")
                //{
                //    viewshift1.Attributes["display"] = "block";
                //}


                viewpanel.Visible = true;
                editpanel.Visible = false;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "showViewShiftTime();", true);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private DataRow selectedRow(int location_id)
        {
                DataTable dtGrid = (DataTable)Session["dtLocation"];
                DataRow drselect = (from DataRow dr in dtGrid.Rows
                                    where (int)dr["location_id"] == location_id
                            select dr).FirstOrDefault();
            
            return drselect;
        }

        private void LoadEditPanel(DataRow drselect)
        {
            try
            {
                btnAdd_Click(null, null);
                Session["location_id"] = Convert.ToString(drselect["location_id"]);
                ddlRegion.SelectedValue = Convert.ToString(drselect["region_id"]);
                txtRegion.Text = Convert.ToString(ddlRegion.SelectedItem);
                ddlCountry.SelectedValue = Convert.ToString(drselect["country_id"]);
                txtCountry.Text = Convert.ToString(ddlCountry.SelectedItem);
                //ddlLocation.SelectedValue = Convert.ToString(drselect["location_id"]);
                //txtLocation.Text = Convert.ToString(ddlLocation.SelectedItem);
                txtLocation.Text = Convert.ToString(drselect["location_name"]);
                txtNoOfShift.SelectedValue = Convert.ToString(drselect["no_of_shifts"]);
                ddlTimeZone.SelectedValue = ddlTimeZone.Items.FindByText(Convert.ToString(drselect["timezone_desc"])).Value;
                //txtIstart.SelectedIndex = txtIstart.Items.IndexOf(txtIstart.Items.FindByValue((Convert.ToString(drselect["shift1_start_time"])).Substring(0,5)));
                //txtIend.SelectedIndex = txtIend.Items.IndexOf(txtIend.Items.FindByValue((Convert.ToString(drselect["shift1_end_time"])).Substring(0, 5)));
                //txtIIstart.SelectedIndex = txtIIstart.Items.IndexOf(txtIIstart.Items.FindByValue((Convert.ToString(drselect["shift2_start_time"])).Substring(0, 5)));
                //txtIIend.SelectedIndex = txtIIend.Items.IndexOf(txtIIend.Items.FindByValue((Convert.ToString(drselect["shift2_end_time"])).Substring(0, 5)));
                //txtIIIstart.SelectedIndex = txtIIIstart.Items.IndexOf(txtIIIstart.Items.FindByValue((Convert.ToString(drselect["shift3_start_time"])).Substring(0, 5)));
                //txtIIIend.SelectedIndex = txtIIIend.Items.IndexOf(txtIIIend.Items.FindByValue((Convert.ToString(drselect["shift3_end_time"])).Substring(0, 5)));
                txtIstart.Text = TimeSpan.Parse(Convert.ToString(drselect["shift1_start_time"])).ToString(@"hh\:mm");
                txtIend.Text = TimeSpan.Parse(Convert.ToString(drselect["shift1_end_time"])).ToString(@"hh\:mm");
                txtIIstart.Text = TimeSpan.Parse(Convert.ToString(drselect["shift2_start_time"])).ToString(@"hh\:mm");
                txtIIend.Text = TimeSpan.Parse(Convert.ToString(drselect["shift2_end_time"])).ToString(@"hh\:mm");
                txtIIIstart.Text = TimeSpan.Parse(Convert.ToString(drselect["shift3_start_time"])).ToString(@"hh\:mm");
                txtIIIend.Text = TimeSpan.Parse(Convert.ToString(drselect["shift3_end_time"])).ToString(@"hh\:mm");

                if (Convert.ToString(Session["GlobalAdminFlag"]) == "Y")
                {
                    txtRegion.Enabled = true;
                    txtCountry.Enabled = true;

                    txtLocation.Enabled = true;
                }
                else
                {
                    txtRegion.Enabled = false;
                    txtCountry.Enabled = false;
                    txtLocation.Enabled = false;
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "showEditShiftTime();", true);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void PrepareControlForExport(Control control)
        {
            try
            {
                for (int i = 0; i < control.Controls.Count; i++)
                {
                    Control current = control.Controls[i];
                    if (current is LinkButton)
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                    }
                    else if (current is ImageButton)
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                    }
                    else if (current is HyperLink)
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                    }

                    else if (current is TextBox)
                    {
                        control.Controls.Remove(current);
                        control.Controls.AddAt(i, new LiteralControl((current as TextBox).Text));
                    }
                    else if (current is DropDownList)
                    {
                        control.Controls.Remove(current);
                        //control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                    }
                    else if (current is HiddenField)
                    {
                        control.Controls.Remove(current);
                    }
                    else if (current is CheckBox)
                    {
                        control.Controls.Remove(current);
                        // control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                    }

                    else if (current is System.Web.UI.WebControls.Image)
                    {
                        control.Controls.Remove(current);
                        // control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                    }

                    else if (current is Button)
                    {
                        control.Controls.Remove(current);
                        // control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                    }
                    if (current.HasControls())
                    {
                        PrepareControlForExport(current);
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void Clear()
        {
            txtRegion.Text = "";
            txtCountry.Text = "";
            txtLocation.Text = "";
            txtNoOfShift.SelectedIndex = 0;
            txtIend.Text = "";
            txtIIend.Text = "";
            txtIIIend.Text = "";
            txtIstart.Text = "";
            txtIIstart.Text = "";
            txtIIIstart.Text = "";
            Session["location_id"] = "";
        }

        private void GetMasterDropdown()
        {
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                        if (Convert.ToString(Session["SiteAdminFlag"]) == "Y" && Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            ddlRegion.SelectedValue = Convert.ToString(Session["LoggedInRegionId"]);
                            ddlRegion.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlRegion.Enabled = true;
                    }
                    else
                    {
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[1].Rows.Count > 0)
                    {
                        ddlCountry.DataSource = dsDropDownData.Tables[1];
                        ddlCountry.DataTextField = "country_name";
                        ddlCountry.DataValueField = "country_id";
                        ddlCountry.DataBind();
                        if (Convert.ToString(Session["SiteAdminFlag"]) == "Y" && Convert.ToString(Session["GlobalAdminFlag"]) == "N")
                        {
                            ddlCountry.SelectedValue = Convert.ToString(Session["LoggedInCountryId"]);
                            ddlCountry.Attributes["disabled"] = "disabled";
                        }
                        else
                            ddlCountry.Enabled = true;
                    }
                    else
                    {
                        ddlCountry.DataSource = null;
                        ddlCountry.DataBind();
                    }
                    //if (dsDropDownData.Tables[2].Rows.Count > 0)
                    //{
                    //    ddlLocation.DataSource = dsDropDownData.Tables[2];
                    //    ddlLocation.DataTextField = "location_name";
                    //    ddlLocation.DataValueField = "location_id";
                    //    ddlLocation.DataBind();
                    //}
                    //else
                    //{
                    //    ddlLocation.DataSource = null;
                    //    ddlLocation.DataBind();
                    //}
                }
                BindTime();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        public void showViewPanelAfterFilter()
        {
            int vwlinprod_id;
            DataTable dtGrid = new DataTable();
            dtGrid = (DataTable)Session["FilteredData"];

            vwlinprod_id = Convert.ToInt32(dtGrid.Rows[0]["location_id"]);
            Session["location_id"] = vwlinprod_id;
            DataRow drfirst = selectedRow(vwlinprod_id);
            LoadViewpanel(drfirst);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "showViewShiftTime();", true);
                        
        }

        #endregion
    }
}