﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="MHLPA_Application.Login"  UICulture="auto" Culture="auto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Login ::-Mann+ Hummel-::</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <link href="css/Main.css" rel="stylesheet" type="text/css" />

    <!-- js-->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.js"> </script>
    <script src="js/modernizr.custom.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <%--<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.js"></script> --%>
    <%-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>--%>
    <script type="text/javascript">
        $(window).load(function () {
            $('.form-group-1 input').on('focus blur', function (e) {
                $(this).parents('.form-group-1').toggleClass('active', (e.type === 'focus' || this.value.length > 0));
            }).trigger('blur');
        });
        function validateControls() {
            //debugger
            var err_flag = 0;
            if ($('#MainContent_txtusername').val() == "") {
                err_flag = 1;
            }
            if (err_flag == 0) {
                $('#MainContent_lblError').text('');
                return true;

            }
            else {
                $('#MainContent_lblError').text('Please enter username.');
                return false;
            }
        }
        function message() {
            return "Username is required.";
        }
    </script>
    <style type="text/css">
        .login-form
        {
            overflow: hidden;
            position: relative;
            padding: 40px;
        }

        .logo_heading
        {
            color: #008244;
            font-size: 19.54px;
            margin: 16px 0 0;
            font-weight: 800;
        }
    </style>
</head>
<body class="mn_background">
    <div class="form-box">
        <div class="head">
            <img src="images/logo-neu_1.png" alt="" /><br />
            <p class="logo_heading"><%=Resources.Resource.Title %></p>
        </div>
        <form action="#" class="login-form" runat="server">
            <div class="form-group-1">
                <label class="label-control">
                    <span class="label-text"><%=Resources.Resource.Username %></span>
                </label>
                <asp:TextBox runat="server" ID="txtusername" CssClass="form-control-1"></asp:TextBox>
                <asp:RequiredFieldValidator ValidationGroup="VGLogin" ControlToValidate="txtusername" Display="None" runat="server" ID="rfvUsername" SetFocusOnError="true" ErrorMessage="Enter Username" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RequiredFieldValidator ValidationGroup="VGRequest" ControlToValidate="txtusername" Display="None" runat="server" ID="rfvforrequest" SetFocusOnError="true" ErrorMessage="Enter Username" ForeColor="Red"></asp:RequiredFieldValidator>

            </div>
            <div class="form-group-1">
                <label class="label-control">
                    <span class="label-text"><%=Resources.Resource.Password %></span>
                </label>
                <asp:TextBox runat="server" ID="txtpassword" TextMode="Password" CssClass="form-control-1"></asp:TextBox>
                <asp:RequiredFieldValidator ValidationGroup="VGLogin" Display="None" ControlToValidate="txtpassword" runat="server" ID="rfvpwd" SetFocusOnError="true" ErrorMessage="Password is required." ForeColor="Red"></asp:RequiredFieldValidator>

            </div>
            <div class="form-group-1">
                <%--<asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>--%>
                <asp:ValidationSummary ID="valSum" runat="server" ValidationGroup="VGLogin" ForeColor="Red" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="VGRequest" />
                <asp:LinkButton ValidationGroup="VGRequest" Text="<%$Resources:Resource, ResetPassword %>" OnClick="lnkRequest_Click" ID="lnkRequest" runat="server"></asp:LinkButton>
                <%-- <a href="#" onclick="Request();">Request Password</a>--%>
            </div>
            <asp:Button ValidationGroup="VGLogin" runat="server" ID="btnLogin" CssClass="btn_Login" Text="<%$Resources:Resource, Login %>" OnClick="btnLogin_Click" />
        </form>
    </div>
    <%--	<div id="page-wrapper mn_none" class="mn_logn">
			<div class="main-page login-page ">
				
				<div class="widget-shadow" style="margin-top: 35%; margin-bottom:35%">
					<div class="login-top">
                        <h2 class="title1"><img src="images/logo-neu.png"/></h2>
					</div>
					<div class="login-body">
						<form runat="server">
                            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                            <asp:TextBox runat="server" ID="txtusername" CssClass="user" placeholder="Enter username" style="background:url(../images/user.png)no-repeat 8px 10px #fff;"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="txtusername" runat="server" ID="rfvUsername" SetFocusOnError="true" ErrorMessage="Username is required." ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:TextBox runat="server" ID="txtpassword" TextMode="Password" CssClass="lock" placeholder="Enter password" style="background:url(../images/lock.png)no-repeat 8px 10px #fff;"></asp:TextBox>
                            <asp:RequiredFieldValidator ControlToValidate="txtpassword" runat="server" ID="rfvpwd" SetFocusOnError="true" ErrorMessage="Password is required." ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:Button runat="server" ID="btnLogin" CssClass="btn_Login" Text="Sign In" OnClick="btnLogin_Click" />
							<div class="forgot-grid">
								<div class="forgot">
									<a href="#">Forgot Password?</a>
								</div>
								<div class="clearfix"> </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>--%>
    <%--	</div>--%>
    <!--footer-->
    <div class="footer mn_footer">
        <p>&copy; 2017 MANN+HUMMEL. All Rights Reserved | Designed by <a href="#" target="_blank">KNS Technologies</a></p>
    </div>
</body>
</html>
