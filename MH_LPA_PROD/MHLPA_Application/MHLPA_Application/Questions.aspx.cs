﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MHLPA_BusinessLogic;
using MHLPA_BusinessObject;

namespace MHLPA_Application
{
    public partial class Questions : System.Web.UI.Page
    {
        #region GlobalDeclaration

        CommonBL objComBL;
        CommonFunctions objCom = new CommonFunctions();
        QuestionsBO objQuesBO;
        QuestionsBL objQuesBL;
        DataSet dsDropDownData;
        UsersBO objUserBO;
        #endregion

        #region Events
        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: Check for login session and load the login page if session timeout, if not, call GridBind() for binding the gridview, LoadDropdown() for loading the dropdown data in edit panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                //if (!string.IsNullOrEmpty(Convert.ToString(Session["GlobalAdminFlag"])))
                //{
                //    if (Convert.ToString(Session["GlobalAdminFlag"]) == "Y")
                //        btnAdd.Visible = false;
                //    else
                //        btnAdd.Visible = true;
                //}
                //else
                //    btnAdd.Visible = true;
                
                GridBind();
                LoadDropdown();
                LoadViewpanelFirst();
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of Add button, it will clear all the field from edit panel and display Add panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                addpanel.Visible = true;
                viewpanel.Visible = false;
                ddlSection.SelectedValue = "1";
                txtQuestion.Text = "";
                txtSequence.Text = "";
                txtHelp.Text = "";
                txtCheck.Text = "";
                hdnQuestionId.Value = "";
                    txtQuestion.Enabled = true;
                    txtHelp.Enabled = true;
                    txtCheck.Enabled = true;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of Save button, it will call the SaveQuestionsBL with all the details in edit panel to save to database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                objQuesBO = new QuestionsBO();
                objQuesBL = new QuestionsBL();
                objQuesBO.p_display_seq = Convert.ToInt32(txtSequence.Text);
                objQuesBO.p_help_text = Convert.ToString(txtHelp.Text);
                objQuesBO.p_question = Convert.ToString(txtQuestion.Text);
                objQuesBO.p_section_name = Convert.ToString(ddlSection.SelectedItem);
                if (!string.IsNullOrEmpty(Convert.ToString(hdnQuestionId.Value)))
                    objQuesBO.p_question_id = Convert.ToInt32(hdnQuestionId.Value);
                objQuesBO.p_how_to_check = Convert.ToString(txtCheck.Text);
                objQuesBO.p_na_flag=Convert.ToBoolean(chkDisplay.Checked)?"Y":"N";
                if (!string.IsNullOrEmpty(Convert.ToString(Session["GlobalAdminFlag"])))
                {
                    if (Convert.ToString(Session["GlobalAdminFlag"]) == "Y")
                    {
                        objQuesBO.p_location_id = 0;
                        objQuesBO = objQuesBL.SaveQuestionsBL(objQuesBO);
                    }
                    else
                    {
                        objQuesBO.p_location_id = Convert.ToInt32(Session["LocationId"]);
                        objQuesBO = objQuesBL.SaveLocalQuestionsBL(objQuesBO);
                    }
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(Session["SiteAdminFlag"])))
                {
                    if (Convert.ToString(Session["SiteAdminFlag"]) == "Y")
                    {
                        objQuesBO.p_location_id = Convert.ToInt32(Session["LocationId"]);
                        objQuesBO = objQuesBL.SaveLocalQuestionsBL(objQuesBO);
                    }
                }
               
                if (objQuesBO.error_code == 0)
                {
                    string msg = objQuesBO.error_msg;
                    GridBind();
                    LoadDropdown();
                    LoadViewpanelFirst();
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('" + msg + "');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('" + objQuesBO.error_msg + "');", true);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of any row in gridview, the selected row details will be loaded in view panel by calling LoadViewpanel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Select(object sender, EventArgs e)
        {
            try
            {
                int questionid = Convert.ToInt32((sender as LinkButton).CommandArgument);
                DataRow drselect = selectedRow(questionid);
                if (Session["SelectedSection"] != null)
                {
                    Session["PreviousSelectedSection"] = Session["SelectedSection"];
                }
                
                LoadViewpanel(drselect);
                if (Session["SelectedSection"] != null)
                {
                    Session["CurrentSelectedSection"] = Session["SelectedSection"];
                }
                ExpandCollapse();

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc:On click of any row in gridview, the selected row details will be loaded in edit panel by calling LoadEditPanel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Edit(object sender, EventArgs e)
        {
            try
            {
                int questionid = Convert.ToInt32((sender as ImageButton).CommandArgument);
                DataRow drselect = selectedRow(questionid);
                if (Session["SelectedSection"] != null)
                {
                    Session["PreviousSelectedSection"] = Session["SelectedSection"];
                }

                LoadEditPanel(drselect);
                if (Session["SelectedSection"] != null)
                {
                    Session["CurrentSelectedSection"] = Session["SelectedSection"];
                }
                ExpandCollapse();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        //protected void grdSection2_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    try
        //    {
        //        if (e.CommandName == "Select")
        //        {
        //            int questionid = Convert.ToInt32(e.CommandArgument);
        //            DataRow drselect = selectedRow(questionid);
        //            LoadViewpanel(drselect);

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of pages in gridview for Section 3, the selected page will be loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdSection3_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdSection3.PageIndex = e.NewPageIndex;
                grdSection3.DataSource = (DataTable)Session["GrdSection3"];
                grdSection3.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of pages in gridview for Section 1, the selected page will be loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdSection1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdSection1.PageIndex = e.NewPageIndex;
                grdSection1.DataSource = (DataTable)Session["GrdSection1"];
                grdSection1.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of pages in gridview for Section 2, the selected page will be loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdSection2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdSection2.PageIndex = e.NewPageIndex;
                grdSection2.DataSource = (DataTable)Session["GrdSection2"];
                grdSection2.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of pages in gridview for Section 4, the selected page will be loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdSection4_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdSection4.PageIndex = e.NewPageIndex;
                grdSection4.DataSource = (DataTable)Session["GrdSection4"];
                grdSection4.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of pages in gridview for Section 5, the selected page will be loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdSection5_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdSection5.PageIndex = e.NewPageIndex;
                grdSection5.DataSource = (DataTable)Session["GrdSection5"];
                grdSection5.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc: On click of pages in gridview for Section 6, the selected page will be loaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdSection6_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdSection6.PageIndex = e.NewPageIndex;
                grdSection6.DataSource = (DataTable)Session["GrdSection6"];
                grdSection6.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc :
        /// </summary>
        private void LoadViewpanelFirst()
        {
            try
            {
                int vwquestionId;
                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["dtQuestions"];
                vwquestionId = Convert.ToInt32(dtGrid.Rows[0]["question_id"]);
                DataRow drfirst = selectedRow(vwquestionId);
                LoadViewpanel(drfirst);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Calling GetAllMasterBL function for loading all the dropdowns in edit panel
        /// </summary>
        private void LoadDropdown()
        {
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[8].Rows.Count > 0)
                    {
                        ddlSection.DataSource = dsDropDownData.Tables[8];
                        ddlSection.DataTextField = "section_name";
                        ddlSection.DataValueField = "section_id";
                        ddlSection.DataBind();
                    }
                    else
                    {
                        ddlSection.DataSource = null;
                        ddlSection.DataBind();
                    }

                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }


        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Calling getQuestionListforEditBL function to fetch datable and load gridview with the datatable for each section.
        /// </summary>
        /// <param name="UserId"></param>
        private void GridBind()
        {
            DataTable dtQuestions;
            DataTable dtSec1;
            DataView view;
            DataTable distinctValues;
            DataRow[] dr;
            try
            {
                objQuesBO = new QuestionsBO();
                objQuesBL = new QuestionsBL();
                dtQuestions = new DataTable();
                dtSec1 = new DataTable();
                objQuesBO.p_user_id = Convert.ToInt32(Session["UserId"]); ;
                dtQuestions = objQuesBL.getQuestionListforEditBL(objQuesBO);
                Session["dtQuestions"] = dtQuestions;
                if (dtQuestions != null)
                {
                    if (dtQuestions.Rows.Count > 0)
                    {
                        view = new DataView(dtQuestions);
                        distinctValues = view.ToTable(true, "section_id");
                        dr = dtQuestions.Select("section_id=" + Convert.ToString(distinctValues.Rows[0][0]));
                        dtSec1 = dtQuestions.Clone();
                        foreach (DataRow row in dr)
                        {
                            dtSec1.Rows.Add(row.ItemArray);
                        }
                        if (dtSec1.Rows.Count > 0)
                        {
                            Session["GrdSection1"] = dtSec1;
                            grdSection1.DataSource = dtSec1;
                            lblSec1.Text = Convert.ToString(dtSec1.Rows[0]["section_name"]);
                        }
                        else
                            grdSection1.DataSource = null;
                        grdSection1.DataBind();

                        view = new DataView(dtQuestions);
                        distinctValues = view.ToTable(true, "section_id");
                        dr = dtQuestions.Select("section_id=" + Convert.ToString(distinctValues.Rows[1][0]));
                        dtSec1 = dtQuestions.Clone();
                        foreach (DataRow row in dr)
                        {
                            dtSec1.Rows.Add(row.ItemArray);
                        }
                        if (dtSec1.Rows.Count > 0)
                        {
                            Session["GrdSection2"] = dtSec1;
                            grdSection2.DataSource = dtSec1;
                            lblSec2.Text = Convert.ToString(dtSec1.Rows[0]["section_name"]);
                        }
                        else
                            grdSection2.DataSource = null;
                        grdSection2.DataBind();

                        view = new DataView(dtQuestions);
                        distinctValues = view.ToTable(true, "section_id");
                        dr = dtQuestions.Select("section_id=" + Convert.ToString(distinctValues.Rows[2][0]));
                        dtSec1 = dtQuestions.Clone();
                        foreach (DataRow row in dr)
                        {
                            dtSec1.Rows.Add(row.ItemArray);
                        }
                        if (dtSec1.Rows.Count > 0)
                        {
                            Session["GrdSection3"] = dtSec1;
                            grdSection3.DataSource = dtSec1;
                            lblSec3.Text = Convert.ToString(dtSec1.Rows[0]["section_name"]);
                        }
                        else
                            grdSection3.DataSource = null;
                        grdSection3.DataBind();

                        view = new DataView(dtQuestions);
                        distinctValues = view.ToTable(true, "section_id");
                        dr = dtQuestions.Select("section_id=" + Convert.ToString(distinctValues.Rows[3][0]));
                        dtSec1 = dtQuestions.Clone();
                        foreach (DataRow row in dr)
                        {
                            dtSec1.Rows.Add(row.ItemArray);
                        }
                        if (dtSec1.Rows.Count > 0)
                        {
                            Session["GrdSection4"] = dtSec1;
                            grdSection4.DataSource = dtSec1;
                            lblSec4.Text = Convert.ToString(dtSec1.Rows[0]["section_name"]);
                        }
                        else
                            grdSection4.DataSource = null;
                        grdSection4.DataBind();
                        view = new DataView(dtQuestions);
                        distinctValues = view.ToTable(true, "section_id");
                        dr = dtQuestions.Select("section_id=" + Convert.ToString(distinctValues.Rows[4][0]));
                        dtSec1 = dtQuestions.Clone();
                        foreach (DataRow row in dr)
                        {
                            dtSec1.Rows.Add(row.ItemArray);
                        }
                        if (dtSec1.Rows.Count > 0)
                        {
                            Session["GrdSection5"] = dtSec1;
                            grdSection5.DataSource = dtSec1;
                            lblSec5.Text = Convert.ToString(dtSec1.Rows[0]["section_name"]);
                        }
                        else
                            grdSection5.DataSource = null;
                        grdSection5.DataBind();

                        view = new DataView(dtQuestions);
                        distinctValues = view.ToTable(true, "section_id");
                        dr = dtQuestions.Select("section_id=" + Convert.ToString(distinctValues.Rows[5][0]));
                        dtSec1 = dtQuestions.Clone();
                        foreach (DataRow row in dr)
                        {
                            dtSec1.Rows.Add(row.ItemArray);
                        }
                        if (dtSec1.Rows.Count > 0)
                        {
                            Session["GrdSection6"] = dtSec1;
                            grdSection6.DataSource = dtSec1;
                            lblSec6.Text = Convert.ToString(dtSec1.Rows[0]["section_name"]);
                        }
                        else
                            grdSection6.DataSource = null;
                        grdSection6.DataBind();
                    }
                    else
                    {
                        grdSection1.DataSource = null;
                        grdSection1.DataBind();
                        grdSection2.DataSource = null;
                        grdSection2.DataBind();
                        grdSection3.DataSource = null;
                        grdSection3.DataBind();
                        grdSection4.DataSource = null;
                        grdSection4.DataBind();
                        grdSection5.DataSource = null;
                        grdSection5.DataBind();
                    }
                }
                else
                {
                    grdSection1.DataSource = null;
                    grdSection1.DataBind();
                    grdSection2.DataSource = null;
                    grdSection2.DataBind();
                    grdSection3.DataSource = null;
                    grdSection3.DataBind();
                    grdSection4.DataSource = null;
                    grdSection4.DataBind();
                    grdSection5.DataSource = null;
                    grdSection5.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Display the add panel and load all the information with respective controls.
        /// </summary>
        /// <param name="drselect">Selected Row from the dridview</param>
        private void LoadEditPanel(DataRow drselect)
        {
            try
            {
                ddlSection.SelectedValue = Convert.ToString(drselect["section_id"]);
                txtQuestion.Text = Convert.ToString(drselect["question"]);
                txtSequence.Text = Convert.ToString(drselect["question_display_sequence"]);
                txtHelp.Text = Convert.ToString(drselect["help_text"]);
                txtCheck.Text = Convert.ToString(drselect["how_to_check"]);
                hdnQuestionId.Value = Convert.ToString(drselect["question_id"]);
                chkDisplay.Checked = Convert.ToString(drselect["na_flag"]) == "Y" ? true : false;
                addpanel.Visible = true;
                viewpanel.Visible = false;
                if (!string.IsNullOrEmpty(Convert.ToString(Session["GlobalAdminFlag"])) && Convert.ToString(Session["GlobalAdminFlag"]) == "Y")
                {
                        txtQuestion.Enabled = true;
                        txtHelp.Enabled = true;
                        txtCheck.Enabled = true;
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(Session["SiteAdminFlag"])))
                {
                    if (Convert.ToString(Session["SiteAdminFlag"]) == "Y")
                    {
                        if (Convert.ToInt32(hdnQuestionId.Value) >= 10000)
                        {
                            txtQuestion.Enabled = true;
                            txtHelp.Enabled = true;
                            txtCheck.Enabled = true;
                        }
                        else
                        {
                            txtQuestion.Enabled = false;
                            txtHelp.Enabled = false;
                            txtCheck.Enabled = false;
                        }
                        
                    }
                    else
                    {
                        txtQuestion.Enabled = false;
                        txtHelp.Enabled = false;
                        txtCheck.Enabled = false;
                    }
                }
                else
                {
                    txtQuestion.Enabled = false;
                    txtHelp.Enabled = false;
                    txtCheck.Enabled = false;
                }
                Session["SelectedSection"] = Convert.ToString(drselect["section_name"]);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : To fetch the complete row details with questionid
        /// </summary>
        /// <param name="userid">Selected Row Question id</param>
        /// <returns>Selected Row details</returns>
        private DataRow selectedRow(int questionid)
        {
                DataTable dtGrid = (DataTable)Session["dtQuestions"];
                DataRow drselect = (from DataRow dr in dtGrid.Rows
                            where (int)dr["question_id"] == questionid
                            select dr).FirstOrDefault();
            
            return drselect;
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : Display the view panel and load all the information with respective controls.
        /// </summary>
        /// <param name="drselect">Selected Row from the dridview</param>
        public void LoadViewpanel(DataRow drselect)
        {
            try
            {
                addpanel.Visible = false;
                viewpanel.Visible = true;
                lblSectionNumber.Text = Convert.ToString(drselect["section_name"]);
                lblSequence.Text = Convert.ToString(drselect["question_display_sequence"]);
                lblQuestion.Text = Convert.ToString(drselect["question"]);
                lblHelp.Text = Convert.ToString(drselect["help_text"]);
                lblcheck.Text=Convert.ToString(drselect["how_to_check"]);
                chkDisplay_option.Checked = Convert.ToString(drselect["na_flag"]) == "Y" ? true : false;
                Session["SelectedSection"] = Convert.ToString(drselect["section_name"]);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Anamika(KNS)
        /// Date : Feb 26,2018
        /// Desc : To fetch the previously openned Section so that the old section must be expanded
        /// </summary>
        public void ExpandCollapse()
        {
            string oldDiv;

            switch (Convert.ToString(Session["PreviousSelectedSection"]))
            {
                case "General Topics":
                    oldDiv = "collapse1";
                    break;
                case "A. People Development":
                    oldDiv = "collapse2";
                    break;
                case "B. Safe, Organized, Clean Work Area":
                    oldDiv = "collapse3";
                    break;
                case "C. Robust Processes and Equipment":
                    oldDiv = "collapse4";
                    break;
                case "D. Standardized Work":
                    oldDiv = "collapse5";
                    break;
                case "E. Rapid Problem Solving / 8D":
                    oldDiv = "collapse6";
                    break;
                default:
                    oldDiv = "collapse1";
                    break;
            }

            string newDiv;

            switch (Convert.ToString(Session["CurrentSelectedSection"]))
            {
                case "General Topics":
                    newDiv = "collapse1";
                    break;
                case "A. People Development":
                    newDiv = "collapse2";
                    break;
                case "B. Safe, Organized, Clean Work Area":
                    newDiv = "collapse3";
                    break;
                case "C. Robust Processes and Equipment":
                    newDiv = "collapse4";
                    break;
                case "D. Standardized Work":
                    newDiv = "collapse5";
                    break;
                case "E. Rapid Problem Solving / 8D":
                    newDiv = "collapse6";
                    break;
                default:
                    newDiv = "collapse1";
                    break;
            }

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "ExpandSelection(" + newDiv + "," + oldDiv + ",collapse1);", true);

        }

        #endregion        


    }
}