﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MHLPA_BusinessLogic;
using MHLPA_BusinessObject;

namespace MHLPA_Application
{
    public partial class Distribution_List : System.Web.UI.Page
    {
        #region Global Declaration
        DistributionBL objDistBL;
        DistributionBO objDistBO;
        UsersBO objUserBO;
        CommonFunctions objCom = new CommonFunctions();
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserId"]))) { Response.Redirect("Login.aspx"); return; }
            if (!IsPostBack)
            {
                BindGrid();
                LoadViewpanelFirst();

            }

        }
        
        protected void Select(object sender, EventArgs e)
        {
            try
            {
                int dist_list_id = Convert.ToInt32((sender as LinkButton).CommandArgument);
                string old = Convert.ToString(Session["dist_list_id"]);
                Session["dist_list_id"] = dist_list_id;
                DataRow drselect = selectedRow(dist_list_id);
                LoadViewpanel(drselect);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void Edit(object sender, EventArgs e)
        {
            try
            {
                int dist_list_id = Convert.ToInt32((sender as ImageButton).CommandArgument);
                Session["dist_list_id"] = dist_list_id;
                DataRow drselect = selectedRow(dist_list_id);
                LoadEditPanel(drselect);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                objDistBO = new DistributionBO();
                objDistBL = new DistributionBL();
                objDistBO.distribution_type = Convert.ToString(txtdistType.Text);
                objDistBO.mail_list = Convert.ToString(txtList.Text);
                if (Convert.ToString(Session["GlobalAdminFlag"]) == "Y")
                    objDistBO.location_id = 0;
                else
                    objDistBO.location_id = Convert.ToInt32(Session["LocationId"]);
                if (chkNotOwner.Checked)
                    objDistBO.notification = "Y";
                else
                    objDistBO.notification = "N";
                objDistBO = objDistBL.SaveDistributionBL(objDistBO);
                if (objDistBO.err_code == 0)
                {

                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('" + objDistBO.err_msg + "');", true);
                    BindGrid();
                    LoadViewpanelFirst();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('" + objDistBO.err_msg + "');", true);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods

        private void LoadViewpanel(DataRow drselect)
        {
            try
            {
                lbltype.Text = Convert.ToString(drselect["list_type"]);
                lbllist.Text = Convert.ToString(drselect["email_list"]);
                if (Convert.ToString(drselect["send_notification_to_owner"]) == "Y")
                    chkNot.Checked = true;
                else
                    chkNot.Checked = false;

                viewpanel.Visible = true;
                addpanel.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void LoadEditPanel(DataRow drselect)
        {
            try
            {
                txtdistType.Text = Convert.ToString(drselect["list_type"]);
                txtList.Text = Convert.ToString(drselect["email_list"]);
                if (Convert.ToString(drselect["send_notification_to_owner"]) == "Y")
                    chkNot.Checked = true;
                else
                    chkNot.Checked = false;

                viewpanel.Visible = false;
                addpanel.Visible = true;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private DataRow selectedRow(int dist_list_id)
        {
            DataTable dtGrid = (DataTable)Session["Distribution"];
            DataRow drselect = (from DataRow dr in dtGrid.Rows
                                where (int)dr["dist_list_id"] == dist_list_id
                                select dr).FirstOrDefault();

            return drselect;
        }

        private void BindGrid()
        {
            objDistBL = new DistributionBL();
            DataTable dtDistributionList = new DataTable();
            objUserBO = new UsersBO();
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["GlobalAdminFlag"])))
                {
                    objUserBO.GlobalAdminFlag = Convert.ToString(Session["GlobalAdminFlag"]);
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(Session["SiteAdminFlag"])))
                {
                    objUserBO.SiteAdminFlag = Convert.ToString(Session["SiteAdminFlag"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationId"])))
                {
                    objUserBO.location_id = Convert.ToInt32(Session["LocationId"]);
                }
                dtDistributionList = objDistBL.GetDistributionListBL(objUserBO);
                if (dtDistributionList != null)
                {
                    if (dtDistributionList.Rows.Count > 0)
                    {
                        grdDistribution.DataSource = dtDistributionList;
                        grdDistribution.DataBind();
                        Session["Distribution"] = dtDistributionList;
                    }
                    else
                    {
                        grdDistribution.DataSource = null;
                        grdDistribution.DataBind();
                        Session["Distribution"] = null;
                    }
                }
                else
                {
                    grdDistribution.DataSource = null;
                    grdDistribution.DataBind();
                    Session["Distribution"] = null;
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }



        }

        private void LoadViewpanelFirst()
        {
            try
            {
                int vwdistId;
                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["Distribution"];
                vwdistId = Convert.ToInt32(dtGrid.Rows[0]["dist_list_id"]);
                DataRow drfirst = selectedRow(vwdistId);
                LoadViewpanel(drfirst);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        #endregion
    }
}