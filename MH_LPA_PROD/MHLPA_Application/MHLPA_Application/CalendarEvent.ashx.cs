﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MHLPA_BusinessObject;
using MHLPA_BusinessLogic;
using System.Web.SessionState;

namespace MHLPA_Application
{
    /// <summary>
    /// Summary description for CalendarEvent
    /// </summary>
    public class CalendarEvent : IHttpHandler, IRequiresSessionState 
    {
        /// <summary>
        /// Author : Anamika(KNS)
        /// Date : 14 Aug 2017
        /// Desc :It will return all the plan details in json format based on Location.
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            CommonFunctions objComm = new CommonFunctions();
            CommonBL objCom = new CommonBL();
            LocationBO objLocBO = new LocationBO();
            context.Response.ContentType = "text/plain";
            try
            {
                List<int> idList = new List<int>();
                List<ImproperCalendarEvent> tasksList = new List<ImproperCalendarEvent>();
                if (!string.IsNullOrEmpty(Convert.ToString(context.Session["SelectedLocation"])))
                    objLocBO.p_location_id = Convert.ToString(context.Session["SelectedLocation"]);
                //objLocBO.p_location_id = "1";
                //Generate JSON serializable events
                foreach (ImproperCalendarEvent cevent in objCom.getEventsBL(objLocBO))
                {

                    tasksList.Add(new ImproperCalendarEvent
                    {
                        audit_plan_id = cevent.audit_plan_id,
                        planned_date = cevent.planned_date,
                        Emp_name = cevent.Emp_name,
                        line_name=cevent.line_name,
                        allDay = false,
                        line_id = cevent.line_id,
                        to_be_audited_by_user_id = cevent.to_be_audited_by_user_id,
                        title = Convert.ToString(cevent.Emp_name) + "(" + Convert.ToString(cevent.line_name) + ")",
                        planned_end_date=Convert.ToString(cevent.planned_end_date),
                        Audit_Status=Convert.ToInt32(cevent.Audit_Status)
                    }
                    );
                    idList.Add(cevent.audit_plan_id);
                }

                //Serialize events to string
                System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string sJSON = oSerializer.Serialize(tasksList);

                //Write JSON to response object
                context.Response.Write(sJSON);
            }
            catch (Exception ex)
            {
                objComm.ErrorLog(ex);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}