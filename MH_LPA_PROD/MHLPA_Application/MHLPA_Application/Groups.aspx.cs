﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MHLPA_BusinessLogic;
using MHLPA_BusinessObject;

namespace MHLPA_Application
{
    public partial class Groups : System.Web.UI.Page
    {
        #region GlobalDeclaration
        CommonBL objComBL;
        DataSet dsDropDownData;
        CommonFunctions objCom = new CommonFunctions();
        GroupsBO objGrpBO;
        GroupsBL objGrpBL;
        UsersBO objUserBO;
        ArrayList arraylist1 = new ArrayList();
        ArrayList arraylist2 = new ArrayList();
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                int vwuserid;
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

                string strLocation;
                if (!IsPostBack)
                {
                    LoadDropDowns();
                    if (string.IsNullOrEmpty(Convert.ToString(Session["Location"])))
                    {
                        strLocation = Convert.ToString(ddlLocation.SelectedItem);
                    }
                    else
                    {
                        strLocation = Convert.ToString(Session["Location"]);
                    }
                    if (Convert.ToString(Session["GlobalAdminFlag"]) == "Y")
                    {
                        ddlLocation.Enabled = true;
                    }
                    else
                    {
                        ddlLocation.Enabled = false;
                    }
                    GridBind(strLocation);
                    DataTable dtGrid = new DataTable();
                    dtGrid = (DataTable)Session["dtGroups"];
                    if (dtGrid != null)
                    {
                        vwuserid = Convert.ToInt32(dtGrid.Rows[0]["group_id"]);
                        DataRow drfirst = selectedRow(vwuserid);
                        LoadViewpanel(drfirst);
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

            try
            {
                LoadDropDowns();

                addpanl.Visible = true;
                viewpanel.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }


        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataTable dtUserId;
            int Output;
            try
            {
                objGrpBO = new GroupsBO();
                objGrpBL = new GroupsBL();
                dtUserId = new DataTable();
                objGrpBO.p_group_name = txtGroup.Text;
                objGrpBO.p_location_name = Convert.ToString(Session["Location"]);
                dtUserId.Columns.Add("UserId");
                for (int i = 0; i < lstSelectedEmployees.Items.Count; i++)
                {
                    dtUserId.Rows.Add(lstSelectedEmployees.Items[i].Value);
                }
                Output = objGrpBL.SaveGroupMemberBL(objGrpBO, dtUserId);
                if (Output == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Updated Successfully.');", true);
                    LoadDropDowns();
                    if (string.IsNullOrEmpty(Convert.ToString(Session["Location"])))
                    {
                        GridBind(Convert.ToString(ddlLocation.SelectedItem));
                    }
                    else
                    {
                         GridBind(Convert.ToString(Session["Location"]));
                    }
                    if (Convert.ToString(Session["GlobalAdminFlag"]) == "Y")
                    {
                        ddlLocation.Enabled = true;
                    }
                    else
                    {
                        ddlLocation.Enabled = false;
                    }
                    DataTable dtGrid = new DataTable();
                    dtGrid = (DataTable)Session["dtGroups"];
                    if (dtGrid != null)
                    {
                        DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["group_id"]));
                        LoadViewpanel(drfirst);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('There is some issue in updating. Please try Again');", true);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void Select(object sender, EventArgs e)
        {
            try
            {
                int group_member_id = Convert.ToInt32((sender as LinkButton).CommandArgument);
                DataRow drselect = selectedRow(group_member_id);
                LoadViewpanel(drselect);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void Edit(object sender, EventArgs e)
        {
            try
            {
                int group_id = Convert.ToInt32((sender as ImageButton).CommandArgument);
                Session["GroupId"] = group_id;
                DataTable dtGrid = (DataTable)Session["dtGroupsDetail"];
                LoadEditPanel(dtGrid);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnAddSelected_Click(object sender, EventArgs e)
        {
            try
            {
                lbltxt.Visible = false;
                if (lstSelectEmployees.SelectedIndex >= 0)
                {
                    for (int i = 0; i < lstSelectEmployees.Items.Count; i++)
                    {
                        if (lstSelectEmployees.Items[i].Selected)
                        {
                            if (!arraylist1.Contains(lstSelectEmployees.Items[i]))
                            {
                                arraylist1.Add(lstSelectEmployees.Items[i]);

                            }
                        }
                    }
                    for (int i = 0; i < arraylist1.Count; i++)
                    {
                        if (!lstSelectedEmployees.Items.Contains(((ListItem)arraylist1[i])))
                        {
                            lstSelectedEmployees.Items.Add(((ListItem)arraylist1[i]));
                        }
                        lstSelectEmployees.Items.Remove(((ListItem)arraylist1[i]));
                    }
                    lstSelectedEmployees.SelectedIndex = -1;
                }
                else
                {
                    lbltxt.Visible = true;
                    lbltxt.Text = "Please select atleast one in Listbox1 to move";
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnAddAll_Click(object sender, EventArgs e)
        {
            try
            {
                lbltxt.Visible = false;
                while (lstSelectEmployees.Items.Count != 0)
                {
                    for (int i = 0; i < lstSelectEmployees.Items.Count; i++)
                    {
                        lstSelectedEmployees.Items.Add(lstSelectEmployees.Items[i]);
                        lstSelectEmployees.Items.Remove(lstSelectEmployees.Items[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnRemoveSelected_Click(object sender, EventArgs e)
        {
            try
            {
                lbltxt.Visible = false;
                if (lstSelectedEmployees.SelectedIndex >= 0)
                {
                    for (int i = 0; i < lstSelectedEmployees.Items.Count; i++)
                    {
                        if (lstSelectedEmployees.Items[i].Selected)
                        {
                            if (!arraylist2.Contains(lstSelectedEmployees.Items[i]))
                            {
                                arraylist2.Add(lstSelectedEmployees.Items[i]);
                            }
                        }
                    }
                    for (int i = 0; i < arraylist2.Count; i++)
                    {
                        if (!lstSelectEmployees.Items.Contains(((ListItem)arraylist2[i])))
                        {
                            lstSelectEmployees.Items.Add(((ListItem)arraylist2[i]));
                        }
                        lstSelectedEmployees.Items.Remove(((ListItem)arraylist2[i]));
                    }
                    lstSelectEmployees.SelectedIndex = -1;
                }
                else
                {
                    lbltxt.Visible = true;
                    lbltxt.Text = "Please select atleast one in Listbox2 to move";
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void btnRemoveAll_Click(object sender, EventArgs e)
        {
            try
            {
                lbltxt.Visible = false;
                while (lstSelectedEmployees.Items.Count != 0)
                {
                    for (int i = 0; i < lstSelectedEmployees.Items.Count; i++)
                    {
                        lstSelectEmployees.Items.Add(lstSelectedEmployees.Items[i]);
                        lstSelectedEmployees.Items.Remove(lstSelectedEmployees.Items[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridBind(Convert.ToString(ddlLocation.SelectedItem));
                DataTable dtGrid = new DataTable();
                dtGrid = (DataTable)Session["dtGroups"];
                if (dtGrid != null)
                {
                    DataRow drfirst = selectedRow(Convert.ToInt32(dtGrid.Rows[0]["group_id"]));
                    LoadViewpanel(drfirst);
                }
                else
                {
                    viewpanel.Visible = false;
                    addpanl.Visible = false;
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }

        protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txtGroup.Text = Convert.ToString(ddlGroup.SelectedItem);

                PopupControlExtender2.Commit(Convert.ToString(ddlGroup.SelectedItem));
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                if (dtCountry.Rows.Count > 0)
                {
                    ddlCountry.DataSource = dtCountry;
                    ddlCountry.DataTextField = "country_name";
                    ddlCountry.DataValueField = "country_id";
                    ddlCountry.DataBind();
                    objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                }
                else
                {
                    ddlCountry.Items.Clear();
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }

                dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlLocation.DataSource = dtLocation;
                    ddlLocation.DataTextField = "location_name";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();

                objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlLocation.DataSource = dtLocation;
                    ddlLocation.DataTextField = "location_name";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();
                    objUserBO.location_id = Convert.ToInt32(ddlLocation.SelectedValue);
                }
                else
                {
                    ddlLocation.Items.Clear();
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void grdGroups_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                grdGroups.PageIndex = e.NewPageIndex;
                grdGroups.DataSource = (DataTable)Session["dtGroups"];
                grdGroups.DataBind();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        #endregion

        #region Methods
        private void GridBind(string strLocation)
        {
            DataSet dtGroups;
            try
            {
                dtGroups = new DataSet();
                objGrpBO = new GroupsBO();
                objGrpBL = new GroupsBL();
                objGrpBO.p_location_name = strLocation;
                dtGroups = objGrpBL.GetGroupMemberListBL(objGrpBO);
                if (dtGroups.Tables.Count > 0)
                {
                    if (dtGroups.Tables[1].Rows.Count > 0)
                    {
                        Session["dtGroups"] = dtGroups.Tables[1];
                        grdGroups.DataSource = dtGroups.Tables[1];
                    }
                    else
                    {
                        grdGroups.DataSource = null;
                    }
                    grdGroups.DataBind();
                    if (dtGroups.Tables[0].Rows.Count > 0)
                    {
                        Session["dtGroupsDetail"] = dtGroups.Tables[0];
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void LoadEditPanel(DataTable dtSelectedGroup)
        {
            //DataTable dtSelectedGroup = new DataTable();
            DataView view;
            DataRow[] dr;
            DataTable dtusers = new DataTable();
            try
            {
                btnAdd_Click(null, null);
                ////dtSelectedGroup = (DataTable)Session["dtGroupsDetail"];
                if (dtSelectedGroup != null)
                {
                    if (dtSelectedGroup.Rows.Count > 0)
                    {

                        view = new DataView(dtSelectedGroup);
                        dr = dtSelectedGroup.Select("group_id=" + Convert.ToInt32(Session["GroupId"]));
                        dtusers = dtSelectedGroup.Clone();
                        foreach (DataRow row in dr)
                        {
                            dtusers.Rows.Add(row.ItemArray);
                        }
                        if (dtusers.Rows.Count > 0)
                        {

                            for (int j = 0; j < lstSelectEmployees.Items.Count; j++)
                            {
                                for (int i = 0; i < dtusers.Rows.Count; i++)
                                {
                                    if (lstSelectEmployees.Items[j].Value == Convert.ToString(dtusers.Rows[i]["user_id"]))
                                    {
                                        if (!arraylist1.Contains(lstSelectEmployees.Items[j]))
                                        {
                                            arraylist1.Add(lstSelectEmployees.Items[j]);

                                        }
                                        lstSelectEmployees.Items[j].Selected = true;
                                        lstSelectEmployees.SelectedIndex = i;
                                    }
                                }
                            }
                            for (int i = 0; i < arraylist1.Count; i++)
                            {
                                if (!lstSelectedEmployees.Items.Contains(((ListItem)arraylist1[i])))
                                {
                                    lstSelectedEmployees.Items.Add(((ListItem)arraylist1[i]));
                                }
                                lstSelectEmployees.Items.Remove(((ListItem)arraylist1[i]));
                            }
                            lstSelectedEmployees.SelectedIndex = -1;
                            //btnAddSelected_Click(null, null);

                        }

                    }
                }
                DataRow drselect = (from DataRow dr1 in dtusers.Rows
                                    where (int)dr1["group_id"] == Convert.ToInt32(dtusers.Rows[0]["group_id"])
                                    select dr1).FirstOrDefault();
                txtGroup.Text = Convert.ToString(drselect["group_name"]);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private DataRow selectedRow(int group_id)
        {
                DataTable dtGrid = (DataTable)Session["dtGroups"];
                DataRow drselect = (from DataRow dr in dtGrid.Rows
                            where (int)dr["group_id"] == group_id
                            select dr).FirstOrDefault();
           
            return drselect;
        }

        public void LoadViewpanel(DataRow drselect)
        {
            try
            {
                addpanl.Visible = false;
                viewpanel.Visible = true;
                lblgroup.Text = Convert.ToString(drselect["group_name"]);
                lblemployee.Text = Convert.ToString(drselect["emp_full_name"]);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private void LoadDropDowns()
        {
            DataTable dtCountry = new DataTable();
            DataTable dtLocation = new DataTable();
            try
            {
                dsDropDownData = new DataSet();
                objUserBO = new UsersBO();
                objUserBO.UserId = Convert.ToInt32(Session["UserId"]);
                objComBL = new CommonBL();
                dsDropDownData = objComBL.GetAllMasterBL(objUserBO);
                if (dsDropDownData.Tables.Count > 0)
                {
                    if (dsDropDownData.Tables[0].Rows.Count > 0)
                    {
                        ddlRegion.DataSource = dsDropDownData.Tables[0];
                        ddlRegion.DataTextField = "region_name";
                        ddlRegion.DataValueField = "region_id";
                        ddlRegion.DataBind();
                    }
                    else
                    {
                        ddlRegion.DataSource = null;
                        ddlRegion.DataBind();
                    }
                    if (dsDropDownData.Tables[6].Rows.Count > 0)
                    {
                        ddlGroup.DataSource = dsDropDownData.Tables[6];
                        ddlGroup.DataTextField = "group_name";
                        ddlGroup.DataValueField = "group_id";
                        ddlGroup.DataBind();
                    }
                    else
                    {
                        ddlGroup.DataSource = null;
                        ddlGroup.DataBind();
                    }
                    if (dsDropDownData.Tables[7].Rows.Count > 0)
                    {
                        lstSelectEmployees.DataSource = dsDropDownData.Tables[7];
                        lstSelectEmployees.DataTextField = "emp_full_name";
                        lstSelectEmployees.DataValueField = "user_id";
                        lstSelectEmployees.DataBind();

                    }
                    else
                    {
                        lstSelectEmployees.DataSource = null;
                        lstSelectEmployees.DataBind();
                    }
                }
                objUserBO.region_id = Convert.ToInt32(ddlRegion.SelectedValue);
                dtCountry = objComBL.GetCountryBasedOnRegionBL(objUserBO);

                if (dtCountry.Rows.Count > 0)
                {
                    ddlCountry.DataSource = dtCountry;
                    ddlCountry.DataTextField = "country_name";
                    ddlCountry.DataValueField = "country_id";
                    ddlCountry.DataBind();
                }
                else
                {
                    ddlCountry.DataSource = null;
                    ddlCountry.DataBind();
                }
                objUserBO.country_id = Convert.ToInt32(ddlCountry.SelectedValue);
                dtLocation = objComBL.GetLocationBasedOnCountryBL(objUserBO);
                if (dtLocation.Rows.Count > 0)
                {
                    ddlLocation.DataSource = dtLocation;
                    ddlLocation.DataTextField = "location_name";
                    ddlLocation.DataValueField = "location_id";
                    ddlLocation.DataBind();
                }
                else
                {
                    ddlLocation.DataSource = null;
                    ddlLocation.DataBind();
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }

        }
        #endregion

        

    }
}