﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MHLPA_BusinessObject;

namespace MHLPA_DataAccess
{
    public class DistributionDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataTable GetDistributionListDAL(UsersBO objUserBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getDistributionList, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamGlobalAdminFlag, SqlDbType.VarChar).Value = objUserBO.GlobalAdminFlag;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamSiteAdminFlag, SqlDbType.VarChar).Value = objUserBO.SiteAdminFlag;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.VarChar).Value = objUserBO.location_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtOutput;
        }

        public DistributionBO SaveDistributionDAL(DistributionBO objDistBO)
        {
            DistributionBO objOutput = new DistributionBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.InsDistListProcedure, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = Convert.ToInt32(objDistBO.location_id);
                sqlcmd.Parameters.Add(ResourceFileDAL.p_list_type, SqlDbType.VarChar, 100).Value = objDistBO.distribution_type;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_email_list, SqlDbType.VarChar, 100).Value = objDistBO.mail_list;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_send_to_owner, SqlDbType.VarChar, 1).Value = objDistBO.notification;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_message, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.err_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.l_err_code].Value);
                objOutput.err_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_err_message].Value);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }
    }
}
