﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MHLPA_BusinessObject;

namespace MHLPA_DataAccess
{
    public class RoleDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public DataTable GetRoleDAL()
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlada;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.GetRoleListProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlada = new SqlDataAdapter(sqlcmd);
                sqlada.Fill(dtOutput);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtOutput;
        }


        public RoleBO SaveRoleDAL(RoleBO objRoleBL)
        {
            RoleBO objOutput = new RoleBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.InsRoleProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_role_id, SqlDbType.Int).Value = objRoleBL.role_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_rolename, SqlDbType.VarChar, 100).Value = objRoleBL.rolename;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_no_of_audits_req, SqlDbType.VarChar, 1000).Value = objRoleBL.audits_no;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_frequency, SqlDbType.VarChar, 1000).Value = objRoleBL.frequency;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_code, SqlDbType.VarChar, 1).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_message, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.l_err_code].Value);
                objOutput.error_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_err_message].Value);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        public RoleBO DeleteRoleDAL(RoleBO objRoleBO)
        {
            RoleBO objOutput = new RoleBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.DelRole, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_role_id, SqlDbType.Int).Value = objRoleBO.role_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_message, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.l_err_code].Value);
                objOutput.error_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_err_message].Value);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }
    }
}
