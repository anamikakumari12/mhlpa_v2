﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MHLPA_BusinessObject;

namespace MHLPA_DataAccess
{
    public class UsersDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public UsersBO SaveUserDetailsDAL(UsersBO objUserBO)
        {
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.InsUserDetailsProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamUserId, SqlDbType.Int).Value = objUserBO.UserId;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamUserName, SqlDbType.VarChar,100).Value = objUserBO.UserName;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_first_name, SqlDbType.VarChar, 1000).Value = objUserBO.FirstName;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_last_name, SqlDbType.VarChar, 1000).Value = objUserBO.LastName;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamPassword, SqlDbType.VarChar,1000).Value = objUserBO.Password;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamLocationId, SqlDbType.Int).Value = objUserBO.DefaultLocation;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamPrivacySetting, SqlDbType.VarChar,1).Value = objUserBO.PrivacySetting;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamUserRole, SqlDbType.VarChar,100).Value = objUserBO.UserRole;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamStartDate, SqlDbType.Date).Value = objUserBO.StartDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamEndDate, SqlDbType.Date).Value = objUserBO.EndDate;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamEmailId, SqlDbType.VarChar,100).Value = objUserBO.MailId;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamSiteAdminFlag, SqlDbType.VarChar,1).Value = objUserBO.SiteAdminFlag;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamGlobalAdminFlag, SqlDbType.VarChar,1).Value = objUserBO.GlobalAdminFlag;

                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_message, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objUserBO.Err_Code=Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.l_err_code].Value);
                objUserBO.Err_Message= Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_err_message].Value);


                //sqlDa = new SqlDataAdapter(sqlcmd);
                //sqlDa.Fill(output);
               // output=Convert.ToInt32(sqlcmd.ExecuteScalar());

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objUserBO;
        }

        public DataTable GetUserDetailsDAL(UsersBO objUserBO)
        {
            DataTable dtoutput=new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlada;
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.getUserListforEditProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamUserId, SqlDbType.Int).Value = objUserBO.UserId;

                sqlada = new SqlDataAdapter(sqlcmd);
                sqlada.Fill(dtoutput);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtoutput;
        }

        public string ChangePasswordDAL(UsersBO objUserBO)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(ResourceFileDAL.ResetUserPassword, connection);
                command.CommandType = CommandType.StoredProcedure;


                command.Parameters.Add(ResourceFileDAL.ParamUserName, SqlDbType.VarChar, 100).Value = objUserBO.UserName;
                command.Parameters.Add(ResourceFileDAL.ParamPassword, SqlDbType.VarChar, -1).Value = objUserBO.Password;
                Output = Convert.ToString(command.ExecuteScalar());
                //if (Output.Contains("Successfully"))
                //{
                //    Output = "Password updated successfully.";
                //}
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return Output;
        }
    }
}
