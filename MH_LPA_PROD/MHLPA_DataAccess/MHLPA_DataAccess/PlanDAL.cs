﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MHLPA_BusinessObject;

namespace MHLPA_DataAccess
{
    public class PlanDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        public PlanBO SavePlanningDAL(PlanBO objPlanBO)
        {
            PlanBO objOutput = new PlanBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                //sqlcmd = new SqlCommand(ResourceFileDAL.getRelationIdProcedure, sqlconn);
                //sqlcmd.CommandType = CommandType.StoredProcedure;
                //sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objPlanBO.p_line_id;
                //sqlcmd.Parameters.Add(ResourceFileDAL.p_product_id, SqlDbType.VarChar, 100).Value = objPlanBO.p_product_id;
                //sqlAd = new SqlDataAdapter(sqlcmd);
                //sqlAd.Fill(dtLineProdId);
                //if (dtLineProdId != null)
                //{
                //    if (dtLineProdId.Rows.Count > 0)
                //    {
                //        objPlanBO.p_line_product_rel_id = Convert.ToInt32(dtLineProdId.Rows[0][0]);
                //    }
                //}

                sqlcmd = new SqlCommand(ResourceFileDAL.InsAuditPlanProcedure, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_audit_plan_id, SqlDbType.Int).Value = objPlanBO.p_audit_plan_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.ParamUserId, SqlDbType.Int).Value = objPlanBO.p_user_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_planned_date, SqlDbType.Date).Value = objPlanBO.p_planned_start_date;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_planned_date_end, SqlDbType.Date).Value = objPlanBO.p_planned_end_date;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objPlanBO.p_line_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_code, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_message, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;

                sqlcmd.Parameters.Add(ResourceFileDAL.l_invite_to_list, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.l_err_code].Value);
                objOutput.error_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_err_message].Value);
                objOutput.invitationMailIds = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_invite_to_list].Value);
                objOutput.user_name = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_invite_to_list].Value);
                objOutput.p_planned_start_date = objPlanBO.p_planned_start_date;
                objOutput.p_planned_end_date = objPlanBO.p_planned_end_date;
                //objOutput.invitationMailIds = "anamika@knstek.com";
                //objOutput.mailIds = "anamika@knstek.com";
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        public DataSet getMyTaskDAL(PlanBO objPlanBO)
        {

            DataSet dtOutput = new DataSet();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            DataTable dtLineProdId = new DataTable();
            try
            {
                
                sqlcmd = new SqlCommand(ResourceFileDAL.GetAuditReviewsProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objPlanBO.p_line_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        
        }

        public PlanBO SaveAuditDAL(PlanBO objPlanBO, DataTable dtAnswer)
        {
            PlanBO objOutput = new PlanBO();
            DataSet dsOutput = new DataSet();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            DataTable dtLineProdId = new DataTable();
            if (dtAnswer.Columns.Contains("RowCount"))
            {
                dtAnswer.Columns.Remove("RowCount");
            }
            if (dtAnswer.Columns.Contains("display_seq"))
            {
                dtAnswer.Columns.Remove("display_seq");
            }
            if (dtAnswer.Columns.Contains("Image_path"))
            {
                dtAnswer.Columns.Remove("Image_path");
            }
            try
            {
                objCom.TestLog(Convert.ToString("p_audit_plan_id : " + objPlanBO.p_audit_plan_id + ", p_line_id: " + objPlanBO.p_line_id + ", p_part_number: " + objPlanBO.part_number + ",  p_audited_by_user_id" + objPlanBO.p_user_id + ", p_shift_no" + objPlanBO.p_shift_no));
                string str = string.Empty;
                for (int i = 0; i < dtAnswer.Rows.Count; i++)
                {
                    str += Convert.ToString("question_id : " + Convert.ToString(dtAnswer.Rows[i]["question_id"]) + "-- answer: " + Convert.ToString(dtAnswer.Rows[i]["answer"]) + ",  remarks : " + Convert.ToString(dtAnswer.Rows[i]["remarks"]) + ",  imageFile : " + Convert.ToString(dtAnswer.Rows[i]["image_file_name"]) + "; ");
                }
                objCom.TestLog("Answer Table : " + str);
                if (objPlanBO.GlobalAdminFlag == "Y")
                {
                    sqlcmd = new SqlCommand(ResourceFileDAL.InsAuditResultsDateProcedure, sqlconn);
                }
                else
                {
                    sqlcmd = new SqlCommand(ResourceFileDAL.InsAuditResultsProcedure, sqlconn);
                }
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_audit_plan_id, SqlDbType.Int).Value = objPlanBO.p_audit_plan_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objPlanBO.p_line_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_part_number, SqlDbType.VarChar,100).Value = objPlanBO.part_number;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_audited_by_user_id, SqlDbType.Int).Value = objPlanBO.p_user_id;
                if (objPlanBO.GlobalAdminFlag == "Y")
                {
                    sqlcmd.Parameters.Add(ResourceFileDAL.p_audit_date, SqlDbType.DateTime).Value = objPlanBO.audit_datetime;
                }
                sqlcmd.Parameters.Add(ResourceFileDAL.p_shift_no, SqlDbType.Int).Value = objPlanBO.p_shift_no;
                sqlcmd.Parameters.AddWithValue(ResourceFileDAL.p_AuditResultsTable, dtAnswer);

                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dsOutput);
                if (dsOutput != null)
                {
                    if (dsOutput.Tables[0].Rows.Count > 0)
                    {

                        if (Convert.ToString(dsOutput.Tables[0].Rows[0][0]).Contains("Inserted Successfully"))
                        {
                            objOutput.error_msg = "Audit performance updated successfully.";
                            objOutput.error_code = 0;
                        }
                        else
                        {
                            objOutput.error_msg = "There is error in submitting the audit. Please try again.";
                            objOutput.error_code = 1;
                        }
                       
                    }
                    if (dsOutput.Tables[1].Rows.Count > 0)
                    {
                        objOutput.mailIds = Convert.ToString(dsOutput.Tables[1].Rows[0][0]);
                        objOutput.p_audit_id = Convert.ToInt32(dsOutput.Tables[1].Rows[0][1]);
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        public DataTable GetQuestionsForAuditDAL(PlanBO objPlanBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
               
                sqlcmd = new SqlCommand(ResourceFileDAL.SP_GetMyQuestionListForWeb, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objPlanBO.location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.language, SqlDbType.VarChar, 100).Value = objPlanBO.language;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        
        }

        public PlanBO SaveAuditReviewsDAL(PlanBO objPlanBO, DataTable dtReview)
        {
            PlanBO objOutput = new PlanBO();
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            DataTable dtLineProdId = new DataTable();
            dtReview.Columns.Remove("RowCount");
            try
            {
                sqlcmd = new SqlCommand(ResourceFileDAL.InsAuditReviewsProcedure, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_audit_id, SqlDbType.Int).Value = objPlanBO.p_audit_plan_id;
                //sqlcmd.Parameters.Add(ResourceFileDAL.p_line_product_rel_id, SqlDbType.Int).Value = objPlanBO.p_line_product_rel_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_reviewed_by_user_id, SqlDbType.Int).Value = objPlanBO.p_reviewed_by_user_id;
                sqlcmd.Parameters.AddWithValue(ResourceFileDAL.p_AuditReviewTable, dtReview);
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
                if (dtOutput != null)
                {
                    if (dtOutput.Rows.Count > 0)
                    {
                        if (Convert.ToString(dtOutput.Rows[0][0]).Contains("Successfully"))
                            objOutput.error_msg = "Audit review completed successfully.";
                        else
                            objOutput.error_msg = "";
                        //objOutput.error_msg = Convert.ToString(dtOutput.Rows[0][0]);
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        public DataTable GetCommentsDAL(PlanBO objPlanBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.GetReviewHistoryForWeb, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objPlanBO.p_line_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_question_id, SqlDbType.Int).Value = objPlanBO.question_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public PlanBO DeletePlanningDAL(PlanBO objPlanBO)
        {
            PlanBO objOutput = new PlanBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.DelAuditPlanProcedure, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_audit_plan_id, SqlDbType.Int).Value = objPlanBO.p_audit_plan_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_code, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceFileDAL.l_err_message, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.l_err_code].Value);
                objOutput.error_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_err_message].Value);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        public DataSet GetAnswersDAL(int p)
        {
            DataSet dsOutput = new DataSet();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand("getAuditResultsForReport", sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add("@p_audit_id", SqlDbType.Int).Value = p;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dsOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dsOutput;
        }

        public DataSet GetAuditScoreDAL(PlanBO objPlanBO)
        {
            DataSet dsOutput = new DataSet();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.GetAuditScoreSummaryWeb, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_region_id, SqlDbType.Int).Value = objPlanBO.region_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_id, SqlDbType.Int).Value = objPlanBO.country_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objPlanBO.location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objPlanBO.p_line_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_audit_id, SqlDbType.Int).Value = objPlanBO.p_audit_id;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dsOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dsOutput;
        }

        public DataSet SaveBulkPlanningDAL(DataTable SelectedUsers)
        {
            DataSet dtOutput = new DataSet();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlda = null;
            try
            {
                sqlconn.Open();
                objCom.TestLog("Column count : " + Convert.ToString(SelectedUsers.Columns.Count));
                sqlcmd = new SqlCommand(ResourceFileDAL.InsAuditPlanInterface, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue(ResourceFileDAL.p_audit_plan_interface_table, SelectedUsers);
                //sqlcmd.Parameters.Add(ResourceFileDAL.l_err_code, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                //sqlcmd.Parameters.Add(ResourceFileDAL.l_err_message, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                //sqlcmd.ExecuteNonQuery();
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
                //objOutput.error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.l_err_code].Value);
                //objOutput.error_msg = Convert.ToString(sqlcmd.Parameters[ResourceFileDAL.l_err_message].Value);
               // objOutput.error_code = Convert.ToInt32(0);
               // objOutput.error_msg = Convert.ToString("All data updated successfully.");

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetBulkPlanDAL(PlanBO objPlanBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getAuditPlan_Interface, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                //sqlcmd.CommandType = CommandType.Text;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objPlanBO.location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_year, SqlDbType.Int).Value = objPlanBO.p_year;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public PlanBO GetMaxAuditDAL()
        {
            PlanBO objOutput = new PlanBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceFileDAL.GetMaxAuditId, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_audit_id, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();

                objOutput.p_audit_id = Convert.ToInt32(sqlcmd.Parameters[ResourceFileDAL.p_audit_id].Value);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return objOutput;
        }

        public DataTable GetAllTasks(PlanBO objPlanBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csMHLPA"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {

                sqlcmd = new SqlCommand(ResourceFileDAL.sp_getAllTasks, sqlconn);
                sqlconn.Open();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_region_id, SqlDbType.Int).Value = objPlanBO.region_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_country_id, SqlDbType.Int).Value = objPlanBO.country_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_location_id, SqlDbType.Int).Value = objPlanBO.location_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.p_line_id, SqlDbType.Int).Value = objPlanBO.p_line_id;
                sqlcmd.Parameters.Add(ResourceFileDAL.Closed_Status, SqlDbType.VarChar,50).Value = objPlanBO.Closed_status;
                sqlcmd.Parameters.Add(ResourceFileDAL.Auditfromdate, SqlDbType.VarChar,100).Value = objPlanBO.Auditfromdate;
                sqlcmd.Parameters.Add(ResourceFileDAL.Audittodate, SqlDbType.VarChar,100).Value = objPlanBO.Audittodate;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }
    }
}
