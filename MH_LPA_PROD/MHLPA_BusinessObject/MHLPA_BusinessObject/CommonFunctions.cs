﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;

namespace MHLPA_BusinessObject
{
    public class CommonFunctions
    {
        public void ErrorLog(Exception ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;


            StreamWriter log;
            DateTime dateTime = DateTime.UtcNow.Date;
            string ErrorLogFile = Convert.ToString(ConfigurationManager.AppSettings["ErrorLogFile"]) + "_" + dateTime.ToString("dd_MM_yyyy") + ".txt";
            string ErrorFile = Convert.ToString(ConfigurationManager.AppSettings["ErrorFile"])+"_" + dateTime.ToString("dd_MM_yyyy")+".txt";
            if (!File.Exists(ErrorFile))
            {
                log = new StreamWriter(ErrorLogFile, true);
            }
            else
            {
                log = File.AppendText(ErrorLogFile);
            }

            // Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(message);
            // Close the stream:
            log.Close();
        }

        public void TestLog(string a)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: Test Log");
            message += Environment.NewLine;
            message += string.Format("Message Value: {0}", a);
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;


            StreamWriter log;
            DateTime dateTime = DateTime.UtcNow.Date;
            string ErrorLogFile = Convert.ToString(ConfigurationManager.AppSettings["ErrorLogFile"]) + "_" + dateTime.ToString("dd_MM_yyyy") + ".txt";
            string ErrorFile = Convert.ToString(ConfigurationManager.AppSettings["ErrorFile"]) + "_" + dateTime.ToString("dd_MM_yyyy") + ".txt";
            if (!File.Exists(ErrorFile))
            {
                log = new StreamWriter(ErrorLogFile, true);
            }
            else
            {
                log = File.AppendText(ErrorLogFile);
            }

            // Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(message);
            // Close the stream:
            log.Close();
        }

        public void SendMail(EmailDetails objEmail)
        {
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            MailMessage email;
            SmtpClient smtpc;
            try
            {
                email = new MailMessage();
                if (objEmail.toMailId.Contains(";"))
                {
                    List<string> names = objEmail.toMailId.Split(';').ToList<string>();
                    for (int i = 0; i < names.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(names[i].TrimStart().TrimEnd()))
                            email.To.Add(names[i]);
                    }
                }
                else
                {
                    email.To.Add(objEmail.toMailId);
                }

                   // email.To.Add(objEmail.toMailId); //Destination Recipient e-mail address.
                if (!string.IsNullOrEmpty(objEmail.ccMailId))
                    email.CC.Add(objEmail.ccMailId);
                email.Subject = objEmail.subject;
                email.Body = objEmail.body;
                email.IsBodyHtml = true;
                if (!string.IsNullOrEmpty(objEmail.attachment))
                    email.Attachments.Add(new Attachment(objEmail.attachment));
                smtpc = new SmtpClient();
                smtpc.Send(email);
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            finally
            {
                email = new MailMessage();
            }
        }

        public void SendInvitation(eAppointmentMail objApptEmail)
        {
            try
            {

                TestLog("Invite Mail Id : " + objApptEmail.Email);
                SmtpClient sc = new SmtpClient();
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                msg.From = new MailAddress("LPA_Admin@mann-hummel.com");
                msg.To.Add(new MailAddress(objApptEmail.Email, objApptEmail.Name));
                msg.Subject = objApptEmail.Subject;
                msg.Body = objApptEmail.Body;
                msg.Headers.Add("Content-class", "urn:content-classes:calendarmessage");
                StringBuilder str = new StringBuilder();
                str.AppendLine("BEGIN:VCALENDAR");
                str.AppendLine("PRODID:-//" + objApptEmail.Email);
                str.AppendLine("VERSION:2.0");
                str.AppendLine("METHOD:REQUEST");
                str.AppendLine("BEGIN:VEVENT");

                //str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", objApptEmail.StartDate.ToLocalTime().ToString("yyyyMMddTHHmmssZ")));
                //str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", objApptEmail.EndDate.ToLocalTime().ToString("yyyyMMddTHHmmssZ")));

                str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", objApptEmail.newStartDate));
                str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", objApptEmail.newEndDate));

                //str.AppendLine(string.Format("DTSTART:20180205T080000"));
                //str.AppendLine(string.Format("DTSTAMP:20180205T080000"));
                //str.AppendLine(string.Format("DTEND:20180205T090000"));
                //str.AppendLine(string.Format("RRULE:FREQ=DAILY;COUNT=5"));

                str.AppendLine("LOCATION:" + objApptEmail.Location);
                str.AppendLine(string.Format("UID:{0}", Guid.NewGuid()));
                str.AppendLine(string.Format("DESCRIPTION:{0}", objApptEmail.Body));
                str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", objApptEmail.Body));
                str.AppendLine(string.Format("SUMMARY:{0}", objApptEmail.Subject));
                str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", "LPA_Admin@mann-hummel.com"));
                str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", msg.To[0].DisplayName, msg.To[0].Address));
                str.AppendLine("BEGIN:VALARM");
                str.AppendLine("STATUS:FREE");
                str.AppendLine("TRIGGER:-PT15M");
                str.AppendLine("ACTION:DISPLAY");
                str.AppendLine("DESCRIPTION:Reminder");
                str.AppendLine("END:VALARM");
                str.AppendLine("END:VEVENT");
                str.AppendLine("END:VCALENDAR");
                TestLog(str.ToString());
                System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType("text/calendar");
                ct.Parameters.Add("method", "REQUEST");
                ct.Parameters.Add("name", "Meeting.ics");
                AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), ct);
                msg.AlternateViews.Add(avCal);
                
                try
                {
                    sc.Send(msg);
                }
                catch (Exception ex)
                {
                    ErrorLog(ex);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
        }

        //public string MakeDayEvent(string subject, string location, DateTime startDate, DateTime endDate)
        //{
        //    StreamWriter writer;
        //    string filePath = string.Empty;
        //    try
        //    {
        //        string path = HttpContext.Current.Server.MapPath(@"\iCal\");
        //        filePath = path + subject + ".ics";
        //        writer = new StreamWriter(filePath);
        //        writer.WriteLine("BEGIN:VCALENDAR");
        //        writer.WriteLine("VERSION:2.0");
        //        writer.WriteLine("PRODID:-//hacksw/handcal//NONSGML v1.0//EN");
        //        writer.WriteLine("BEGIN:VEVENT");
        //        string startDay = "VALUE=DATE:" + GetFormatedDate(startDate);
        //        string endDay = "VALUE=DATE:" + GetFormatedDate(endDate);
        //        writer.WriteLine("DTSTART;" + startDay);
        //        writer.WriteLine("DTEND;" + endDay);
        //        writer.WriteLine("SUMMARY:" + subject);
        //        writer.WriteLine("LOCATION:" + location);
        //        writer.WriteLine("END:VEVENT");
        //        writer.WriteLine("END:VCALENDAR");
        //        writer.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog(ex);
        //    }
            
        //    return filePath;
        //}

        public void CancelInvitation(eAppointmentMail objApptEmail)
        {
            try
            {
                TestLog("Cancel Mail Id : " + objApptEmail.Email);
                SmtpClient sc = new SmtpClient();
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                msg.From = new MailAddress("LPA_Admin@mann-hummel.com");
                msg.To.Add(new MailAddress(objApptEmail.Email, objApptEmail.Name));
                msg.Subject = objApptEmail.Subject;
                msg.Body = objApptEmail.Body;
                msg.Headers.Add("Content-class", "urn:content-classes:calendarmessage");
                StringBuilder str = new StringBuilder();
                str.AppendLine("BEGIN:VCALENDAR");
                str.AppendLine("PRODID:-//" + objApptEmail.Email);
                str.AppendLine("VERSION:2.0");
                str.AppendLine("METHOD:CANCEL");
                str.AppendLine("BEGIN:VEVENT");
                str.AppendLine("STATUS:CANCELLED");
                //str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", objApptEmail.StartDate.ToUniversalTime().ToString("yyyyMMddTHHmmssZ")));
                //str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                //str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", objApptEmail.EndDate.ToUniversalTime().ToString("yyyyMMddTHHmmssZ")));
                //str.AppendLine(string.Format("RRULE:FREQ=DAILY;COUNT=5"));

                str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", objApptEmail.newStartDate));
                str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", objApptEmail.newEndDate));

                str.AppendLine("LOCATION:" + objApptEmail.Location);
                str.AppendLine(string.Format("UID:{0}", Guid.NewGuid()));
                str.AppendLine(string.Format("DESCRIPTION:{0}", objApptEmail.Body));
                str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", objApptEmail.Body));
                str.AppendLine(string.Format("SUMMARY:{0}", objApptEmail.Subject));
                str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", "LPA_Admin@mann-hummel.com"));
                str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", msg.To[0].DisplayName, msg.To[0].Address));
                str.AppendLine("BEGIN:VALARM");
                str.AppendLine("TRIGGER:-PT15M");
                str.AppendLine("ACTION:DISPLAY");
                str.AppendLine("DESCRIPTION:Reminder");
                str.AppendLine("END:VALARM");
                str.AppendLine("END:VEVENT");
                str.AppendLine("END:VCALENDAR");
                TestLog(str.ToString());
                System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType("text/calendar");
                ct.Parameters.Add("method", "CANCEL");
                ct.Parameters.Add("name", "Meeting.ics");
                AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), ct);
                msg.AlternateViews.Add(avCal);

                try
                {
                    sc.Send(msg);
                }
                catch (Exception ex)
                {
                    ErrorLog(ex);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
        }

        public string LanguageDesc(string lang)
        {
            switch(lang){
                case "bs-Latn-BA": return "Bosnian";
                case "cs-CZ": return "czech";
                case "de-DE": return "German";
                case "es-MX": return "Spanish";
                case "fr-FR": return "French";
                case "ko-KR": return "Korean";
                case "pt-BR": return "Portuguese";
                case "th-TH": return "Thai";
                case "zh-CN": return "Chinese";
                default: return "English";
            }
                
        }
    }
    public class JSONOutput
    {
        public int err_code { get; set; }
        public string err_message { get; set; }
    }
}
