﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MHLPA_BusinessObject
{
    public class UsersBO
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string   FullName { get; set; }
        public int DefaultLocation { get; set; }
        public string Password { get; set; }
        public string PrivacySetting { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string UserRole { get; set; }
        public string SiteAdminFlag { get; set; }
        public string GlobalAdminFlag { get; set; }
        public string MailId { get; set; }
        public string TypeOfEdit { get; set; }
        public int location_id { get; set; }
        public int line_id { get; set; }
        public int region_id { get; set; }
        public int country_id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string UserPassword { get; set; }
        public int Err_Code { get; set; }
        public string Err_Message { get; set; }
    }
}
