﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MHLPA_BusinessObject
{
    public class DistributionBO
    {
        public string distribution_type { get; set; }
        public string mail_list { get; set; }
        public string notification { get; set; }
        public int location_id { get; set; }
        public int err_code { get; set; }
        public string err_msg { get; set; }
    }
}
