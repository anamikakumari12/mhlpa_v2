﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MHLPA_BusinessObject
{
    public class ImproperCalendarEvent
    {
        public int audit_plan_id { get; set; }
        public string planned_date { get; set; }
        public string planned_end_date { get; set; }
        public string Emp_name { get; set; }
        public bool allDay { get; set; }
        public string title { get; set; }
        public int line_product_rel_id { get; set; }
        public int line_id { get; set; }
        public int to_be_audited_by_user_id { get; set; }
        public string line_name { get; set; }
        public int Audit_Status { get; set; }
    }
}
