﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MHLPA_BusinessObject
{
    public class PlanBO
    {
        public int p_audit_plan_id { get; set; }
        public int p_audit_id { get; set; }
        public int p_user_id { get; set; }
        public DateTime p_planned_start_date { get; set; }
        public DateTime p_planned_end_date { get; set; }
        public int p_shift_no { get; set; }
        public int p_line_product_rel_id { get; set; }
        public int p_line_id { get; set; }
        public int p_product_id { get; set; }
        public int error_code { get; set; }
        public string error_msg { get; set; }
        public int to_be_audited_by_user_id { get; set; }
        public int location_id { get; set; }
        public int p_reviewed_by_user_id { get; set; }
        public int p_group_id { get; set; }
        public int question_id { get; set; }
        public string invitationMailIds { get; set; }
        public string mailIds { get; set; }
        public int region_id { get; set; }
        public int country_id { get; set; }
        public string user_name { get; set; }
        public int p_year { get; set; }
        public string Closed_status { get; set; }
        public string Auditfromdate { get; set; }
        public string Audittodate { get; set; }
        public string part_number { get; set; }
        public DateTime audit_datetime { get; set; }
        public string GlobalAdminFlag { get; set; }
        public string language { get; set; }
    }

    public class AnsResultsTableType
    {
        public string question_id { get; set; }
        public string answer { get; set; }
        public string remarks { get; set; }
        public string image_file_name { get; set; }
    }

    public class AnsResultsTableObject
    {
        public AnsResultsTableType records { get; set; }
    }

    public class AnsReviewsTableType
    {
        public int loc_answer_id { get; set; }
        public int question_id { get; set; }
        public string review_closed_on { get; set; }
        public int review_closed_status { get; set; }
        public string review_comments { get; set; }
        public string review_image_file_name { get; set; }
    }

    public class AnsReviewsTableObject
    {
        public AnsReviewsTableType records { get; set; }
    }
}
